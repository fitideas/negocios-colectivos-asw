package com.accionfiduciaria.enumeradores;

import java.util.HashMap;
import java.util.Map;

public enum Estados {
	ACTIVO("1", "ACTIVO"),
	INACTIVO("0", "INACTIVO");

	private final String estado;
	private final String homologacion;
	
	private static final Map<String, Estados> MAP_ESTADOS = new HashMap<>();

	static {
		for (Estados estado : values()) {
			MAP_ESTADOS.put(estado.estado, estado);
		}
	}

	
	/**
	 * @param operacion
	 * @param homologacion
	 */
	private Estados(String estado, String homologacion) {
		this.estado = estado;
		this.homologacion = homologacion;
	}

	/**
	 * @return the operacion
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @return the homologacion
	 */
	public String getHomologacion() {
		return homologacion;
	}
	
	public static Estados buscarEstado(String estado) {
		return MAP_ESTADOS.get(estado);
	}
}
