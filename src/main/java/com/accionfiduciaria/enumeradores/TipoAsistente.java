package com.accionfiduciaria.enumeradores;

public enum TipoAsistente {
	EXPOSITOR, DIRECTIVO;
}
