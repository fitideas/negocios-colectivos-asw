/**
 * 
 */
package com.accionfiduciaria.enumeradores;

import java.util.HashMap;
import java.util.Map;

/**
 * @author efarias
 *
 */
public enum EstadosDistribucion {

	INGRESOS_INSUFICIENTES("Ingresos insuficientes", "INGRESOS_INSUFICIENTES",
			"Este estado aplica cuando las obligaciones son mayores a los ingresos o cuando no existen ingresos para un periodo y tipo de negocio especificado."),
	DISTRIBUCION_DISPONIBLE("Distribucion disponible", "DISTRIBUCION_DISPONIBLE",
			"Este estado aplica cuando los ingresos cubren las obligaciones para un periodo y tipo de negocio especificado"),
	DISTRIBUIDO("Distribuido", "DISTRIBUIDO",
			"Este estado aplica cuando se realiza la distribución de pago a los beneficieros asociados."),
	DISTRIBUCION_CANCELADA("Distribucion Cancelada", "DISTRIBUCION_CANCELADA",
			"Este estado aplica cuando se realiza nuevamente el proceso de calcular obligaciones y el proceso de calcular distribucion para unperiodo y tipo de negocio especificado.");

	private static final Map<String, EstadosDistribucion> MAP_VALOR = new HashMap<>();

	static {
		for (EstadosDistribucion estado : values()) {
			MAP_VALOR.put(estado.valor, estado);
		}
	}

	private final String nombre;
	private final String valor;
	private final String descripcion;

	/**
	 * @param nombre
	 * @param valor
	 * @param descripcion
	 */
	private EstadosDistribucion(String nombre, String valor, String descripcion) {
		this.nombre = nombre;
		this.valor = valor;
		this.descripcion = descripcion;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Busca un estado por valor, para acceder a sus propiedades.
	 * 
	 * @author efarias
	 * @param valor
	 * @return EstadosEnum
	 */
	public static EstadosDistribucion buscarPorValor(String valor) {
		return MAP_VALOR.get(valor);
	}

}
