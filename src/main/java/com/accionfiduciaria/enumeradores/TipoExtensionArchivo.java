package com.accionfiduciaria.enumeradores;

public enum TipoExtensionArchivo {
	EXCEL("xlsx"), PDF("pdf"), WORD("docx");
	
	private String valor;
	
	TipoExtensionArchivo(String valor){
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return this.valor;
	}
}
