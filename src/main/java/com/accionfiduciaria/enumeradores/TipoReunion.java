package com.accionfiduciaria.enumeradores;

public enum TipoReunion {
ASAMBLEA("asamblea"), COMITE("comite");
	
	private String valor;
	
	TipoReunion(String valor){
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return this.valor;
	}
}
