/**
 * 
 */
package com.accionfiduciaria.enumeradores;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum el cual contiene todos los tipos de operaciones que se realizan en la
 * ejecución de auditoria.
 * 
 * @author efarias
 *
 */
public enum OperacionesAuditoria {

	INSERT("INSERT", "CREACIÓN DE REGISTRO"), 
	UPDATE("UPDATE", "ACTUALIZACIÓN"),
	DELETE("DELETE", "ELIMINACIÓN DE REGISTRO");

	private final String operacion;
	private final String homologacion;

	private static final Map<String, OperacionesAuditoria> MAP_OPERACIONES = new HashMap<>();

	static {
		for (OperacionesAuditoria operacion : values()) {
			MAP_OPERACIONES.put(operacion.operacion, operacion);
		}
	}

	/**
	 * @param operacion
	 * @param homologacion
	 */
	private OperacionesAuditoria(String operacion, String homologacion) {
		this.operacion = operacion;
		this.homologacion = homologacion;
	}

	/**
	 * @return the operacion
	 */
	public String getOperacion() {
		return operacion;
	}

	/**
	 * @return the homologacion
	 */
	public String getHomologacion() {
		return homologacion;
	}

	/**
	 * Busca una homologación por operación.
	 * 
	 * @author efarias
	 * @param valor
	 * @return EstadosEnum
	 */
	public static OperacionesAuditoria buscarOperacion(String operacion) {
		return MAP_OPERACIONES.get(operacion);
	}

}
