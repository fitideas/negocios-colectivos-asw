package com.accionfiduciaria.enumeradores;

public enum TiposArchivo {
	VALIDO("VALIDO"),INVALIDO("INVALIDO");
	
	private String valor;
	
	TiposArchivo(String valor){
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return this.valor;
	}
}
