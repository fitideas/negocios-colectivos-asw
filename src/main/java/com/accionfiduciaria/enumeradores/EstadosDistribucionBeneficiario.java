/**
 * 
 */
package com.accionfiduciaria.enumeradores;

/**
 * @author efarias
 *
 */
public enum EstadosDistribucionBeneficiario {

	ACEPTADO("ACEPTADO"), 
	RECHAZADO("RECHAZADO"), 
	REPROCESADO("REPROCESADO");

	private final String valor;

	/**
	 * @param valor
	 */
	private EstadosDistribucionBeneficiario(String valor) {
		this.valor = valor;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

}
