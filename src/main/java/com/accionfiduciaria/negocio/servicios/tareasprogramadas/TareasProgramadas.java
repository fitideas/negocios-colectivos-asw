/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.tareasprogramadas;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.negocio.servicios.IServicioNotificacion;

/**
 * Esta clase ejecuta tareas programadas en un tiempo determinado el formato
 * utilizado para especificar la frecuencia en que se ejecutará esta dado por:
 * Segundo | minuto | hora | día | mes | día de la semana
 * ('MON','TUE','WED','THU','FRI','SAT','SUN').
 * 
 * @see <a href=
 *      "https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html">Class
 *      CronSequenceGenerator</a>
 * @author efarias
 */
@Component
@EnableScheduling
public class TareasProgramadas {

	@Autowired
	private IServicioNotificacion servicioNotificacionPago;

	private static final Logger log = LogManager.getLogger(TareasProgramadas.class);
	private SimpleDateFormat formatDate = new SimpleDateFormat("HH:mm:ss");

	/**
	 * Tarea programada que permite enviar notificaciones vía email a todos los
	 * interesados del negocio que se encuentran parametrizados en el servicio de
	 * acción back 'Buscar Equipo Responsable' por medio de las distintas fechas
	 * clave parametrizadas para cada negocio.
	 * 
	 * @author efarias
	 * @throws BusquedadSinResultadosException 
	 * 
	 */
	@Scheduled(cron = "#{obtenerValorCron}") //Se lee la expresión cron desde la tabla parametros.
	public void notificacionNegocio() throws BusquedadSinResultadosException {
		String fecha = formatDate.format(new Date());
		log.info("Fecha de ejecución de la tarea programda: {}", fecha);
		servicioNotificacionPago.notificarInteresadosNegocio();
	}

}
