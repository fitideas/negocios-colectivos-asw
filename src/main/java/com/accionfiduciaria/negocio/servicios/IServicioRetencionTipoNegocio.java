package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.RetencionTipoNegocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

public interface IServicioRetencionTipoNegocio {

	@Autowired
	public List<RetencionTipoNegocio> buscarRetencionesTipoNegocio(TipoNegocio tipoNegocio);

	@Autowired
	public RetencionTipoNegocio guardarRetencionTipoNegocio(RetencionTipoNegocio retencionTipoNegocio);
	
	@Autowired
	public List<RetencionTipoNegocio> buscarRetencionesTipoNegocioPorNegocio(Negocio negocio);

	@Autowired
	public List<RetencionTipoNegocio> buscarPorIdTipoNegocio(Integer idTipoNegocio);

	@Autowired
	public List<RetencionTipoNegocio> buscarPorIdRetencion(Integer idRetencion);

}
