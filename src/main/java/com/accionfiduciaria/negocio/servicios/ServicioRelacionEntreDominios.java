package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.entidad.RelacionEntreDominios;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioRelacionEntreDominios;
import com.accionfiduciaria.negocio.repositorios.RepositorioValorDominioNegocio;

@Service
@PropertySource("classpath:propiedades.properties")
@Transactional
public class ServicioRelacionEntreDominios implements IServicioRelacionEntreDominios {
	
	@Value("${cod.dominio.retenciones}")
	private String codigoDominioRetencion;
	
	@Value("${valor.true}")
	private String valorTrue;
	@Value("${valor.false}")
	private String valorFalse;
	
	@Autowired
	private RepositorioValorDominioNegocio repositorioValorDominioNegocio;
	
	@Autowired
	private RepositorioRelacionEntreDominios repositorioRelacionEntreDominios;
	
	@Override
	public void agregarRelacionRetencion(List<ValorDominioNegocio> retenciones) {
		List<ValorDominioNegocio> valoresConRetencion = repositorioValorDominioNegocio.buscarValoresQueUsanDominio(codigoDominioRetencion);
		for(ValorDominioNegocio retencion: retenciones){
			if(!repositorioRelacionEntreDominios.existsRelacionEntreDominiosByValorDominioNegocioIdValorDominio(retencion.getIdValorDominio())) {
				valoresConRetencion.forEach(valor ->
					repositorioRelacionEntreDominios.save((new RelacionEntreDominios(valor.getIdValorDominio(), retencion.getIdValorDominio(), valorFalse)))
				);
			}
		}
	}

	@Override
	public void eliminarRelacionDominio(Integer idValorDominio) {
		repositorioRelacionEntreDominios.eliminarRelacionDominio(idValorDominio);
	}

	@Override
	public List<RelacionEntreDominios> buscarRelacionesValorDominio(Integer idValorDominio) {
		return repositorioRelacionEntreDominios.buscarRelacionesValorDominio(idValorDominio);
	}

	@Override
	public void guardarRelacionDominio(RelacionEntreDominios relacion) {
		repositorioRelacionEntreDominios.save(relacion);
	}

	@Override
	public List<RelacionEntreDominios> buscarValoresAplicanRelacion(Integer idValorDominio) {
		return repositorioRelacionEntreDominios.findByValorDominioNegocio1IdValorDominioAndValor(idValorDominio, valorTrue);
	}

	@Override
	public List<RelacionEntreDominios> buscarRelacionDominioPorIdValorDominioYCodigoDominio(Integer idValorDominio,
			String codDominio) {
		return repositorioRelacionEntreDominios.buscarRelacionDominioPorIdValorDominioYCodigoDominio(idValorDominio,
				codDominio);
	}

}
