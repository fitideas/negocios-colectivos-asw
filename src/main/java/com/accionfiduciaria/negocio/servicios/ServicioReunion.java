package com.accionfiduciaria.negocio.servicios;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.enumeradores.TipoReunion;
import com.accionfiduciaria.modelo.dtos.AsambleasDTO;
import com.accionfiduciaria.modelo.dtos.AsistenteDTO;
import com.accionfiduciaria.modelo.dtos.ComiteDTO;
import com.accionfiduciaria.modelo.dtos.ComiteRegistroDTO;
import com.accionfiduciaria.modelo.dtos.DecisionComiteDTO;
import com.accionfiduciaria.modelo.dtos.DecisionDTO;
import com.accionfiduciaria.modelo.dtos.DecisionResponsableDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoReunionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAsambleasDTO;
import com.accionfiduciaria.modelo.dtos.PeticionHistorialReunionesDTO;
import com.accionfiduciaria.modelo.entidad.Asistente;
import com.accionfiduciaria.modelo.entidad.Decision;
import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.MiembroMesaDirectiva;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.Reunion;
import com.accionfiduciaria.modelo.entidad.ReunionPK;
import com.accionfiduciaria.modelo.entidad.Usuario;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.especificaciones.ReunionEspecificacion;
import com.accionfiduciaria.negocio.repositorios.RepositorioDecision;
import com.accionfiduciaria.negocio.repositorios.RepositorioMiembroMesaDirectiva;
import com.accionfiduciaria.negocio.repositorios.RepositorioReunion;
import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioUsuario;

@Service
public class ServicioReunion implements IServicioReunion {

	@Autowired
	private RepositorioReunion repositorioreunion;
	
	@Autowired
	private IServicioPersona servicioPersona;
	
	@Autowired
	private IServicioUsuario servicioUsuario;
	
	@Autowired
	private IServicioArchivo servicioArchivo;
	
	@Autowired
	private  IServicioDominio servicioDominio;
	
	@Autowired
	private RepositorioMiembroMesaDirectiva repositorioMiembroMesaDirectiva;
	
	@Autowired
	private RepositorioDecision repositorioDecision;
	
	

	
	@Autowired
	private Utilidades utilidades;
	
	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;
	
	@Override
	public Reunion guardarReunion(Reunion reunion) {
		return repositorioreunion.save(reunion);
	}

	@Override
	public void guardarMiembroDirectivo(MiembroMesaDirectiva miembro) {
		repositorioMiembroMesaDirectiva.save(miembro);
	}

	@Override
	public void guardarMiembrosMesaDirectiva(List<MiembroMesaDirectiva> miembros) {
		repositorioMiembroMesaDirectiva.saveAll(miembros);
	}

	@Override
	public void guardarDecision(Decision decision) {
		repositorioDecision.save(decision);
	}
	
	@Override
	public void guardarDecisiones(List<Decision> decisiones) {
		repositorioDecision.saveAll(decisiones);
	}

	@Override
	public Reunion guardarReunion(PeticionAsambleasDTO asambleasDTO, FechaClave fechaClave,
			List<MiembroMesaDirectiva> directivos, List<Decision> decisiones, List<AsistenteDTO> listaPersonas,
			String token) {
		Reunion reunion = new Reunion();
		ReunionPK reunionPk = new ReunionPK();
		LocalDateTime fechaTime = utilidades.convierteDateToLocalDateTime(fechaClave.getFechaClavePK().getFecha());

		// Consulta la lista de directivos, y los tranforma en lista de Personas
		List<Persona> personas = listaPersonas.stream().map(directivo -> new Persona(directivo.getTipoDocumento(),
				directivo.getNumeroDocumento(), directivo.getNombreCompleto())).collect(Collectors.toList());

		// Consulta la lista de personas y verifica cuales se debe crear.
		personas = personas.stream()
				.filter(persona -> servicioPersona.buscarPersona(persona.getPersonaPK().getTipoDocumento(),
						persona.getPersonaPK().getNumeroDocumento()) == null)
				.collect(Collectors.toList());

		if (!personas.isEmpty())
			servicioPersona.guardarPersonas(personas);
		reunionPk.setCodigoSfc(asambleasDTO.getCodigoSFC());
		reunionPk.setFecha(fechaTime);
		reunion.setReunionPk(reunionPk);
		reunion.setOrdenDia(asambleasDTO.getOrdenDia());
		reunion.setDireccion(asambleasDTO.getDireccionReunion());
		reunion.setPresentacion(asambleasDTO.getNombreArchivo());
		reunion.setQuorum(asambleasDTO.getNumeroAsistentes());
		reunion.setIdActa(asambleasDTO.getNumeroRadicado());
		reunion.setFechaReunion(fechaTime);
		reunion.setTipo(TipoReunion.ASAMBLEA.toString());
		reunion.setDecisiones(decisiones);
		reunion.setMiembros(directivos);
		reunion.setResumenReunion(asambleasDTO.getOrdenDia());
		Usuario usuario = servicioUsuario.buscarUsuarioPorToken(token);
		if (usuario != null) {
			reunion.setTipoDocumentoUsuario(usuario.getPersona().getPersonaPK().getTipoDocumento());
			reunion.setUsuario(usuario.getLogin());
			reunion.setNumeroDocumentoUsuario(usuario.getPersona().getPersonaPK().getNumeroDocumento());
		}
		guardarReunion(reunion);
		return reunion;
	}

	@Override
	public Reunion guardarReunionComite(ComiteRegistroDTO comiteDTO, FechaClave fechaClave, String token) {
			Reunion reunion = new Reunion();
			ReunionPK reunionPk = new ReunionPK();
			LocalDateTime fechaTime =  utilidades.convierteDateToLocalDateTime(fechaClave.getFechaClavePK().getFecha());
			reunionPk.setCodigoSfc(comiteDTO.getCodigoSFC());
			reunionPk.setFecha(fechaTime);
			reunion.setReunionPk(reunionPk);
			reunion.setOrdenDia(comiteDTO.getOrdenDia());
			reunion.setDireccion(comiteDTO.getDireccionReunion());
			reunion.setPresentacion(comiteDTO.getNombreArchivo());
			reunion.setQuorum(null);
			reunion.setIdActa(comiteDTO.getNumeroRadicado());
			reunion.setFechaReunion(fechaTime);
			reunion.setTipo(TipoReunion.COMITE.toString());
			reunion.setResumenReunion(comiteDTO.getTemasTratados());
			Usuario usuario = servicioUsuario.buscarUsuarioPorToken(token);
			if(usuario != null) {
				reunion.setTipoDocumentoUsuario(usuario.getPersona().getPersonaPK().getTipoDocumento());
				reunion.setUsuario(usuario.getLogin());
				reunion.setNumeroDocumentoUsuario(usuario.getPersona().getPersonaPK().getNumeroDocumento());
			}
			return guardarReunion(reunion);
		
	}
	
	@Override
	public List<Reunion> consultaHistoricoReunion(String codigoSFC, String tipoReunion) {
		return repositorioreunion.findByNegocio_codSfcAndTipo(codigoSFC,tipoReunion);
	}

	
	private List<Reunion> consultaReunionesPorFiltros(PeticionHistorialReunionesDTO peticion) {
		List<String> usuarios = null;
		if(!peticion.getUsuario().isEmpty())
			usuarios = peticion.getUsuario().stream()
				.map(persona -> persona.getTipoDocumento() + persona.getNumeroDocumento()).collect(Collectors.toList());
		
		Specification<Reunion> especificacionCombinada = Specification.
				where(ReunionEspecificacion.filtrarPorTipoReunion(peticion.getTipoReunion()))
				.and(peticion.getCodigoSFC().isEmpty() ? null : ReunionEspecificacion.filtrarPorNegocio(peticion.getCodigoSFC()))
				.and(peticion.getRangoFechas().isEmpty() ? null : ReunionEspecificacion.filtrarPorFechaInicial(utilidades.convierteSimpleDateToLocalDateTime(peticion.getRangoFechas().get(0), formatoddmmyyyy,00,00,00)))
				.and(peticion.getRangoFechas().isEmpty() ? null : ReunionEspecificacion.filtrarPorFechaFinal(utilidades.convierteSimpleDateToLocalDateTime(peticion.getRangoFechas().get(1), formatoddmmyyyy,23,59,59)))
				.and(peticion.getNumeroRadicado().isEmpty() ? null : ReunionEspecificacion.filtrarPorRadicado(peticion.getNumeroRadicado()))
				.and(peticion.getDecision().isEmpty() ? null : ReunionEspecificacion.filtrarPorDecision(peticion.getDecision()))
				.and(peticion.getTareaPendiente().isEmpty() ? null : ReunionEspecificacion.filtrarPorPendientes(peticion.getTareaPendiente()))
				.and(usuarios == null ? null: ReunionEspecificacion.filtrarPorUsuario(usuarios));
		
		return repositorioreunion.findAll(especificacionCombinada);
	}

	@Override
	public List<HistoricoReunionDTO> consultaHistoricoReuniones(PeticionHistorialReunionesDTO peticion) {
		List<Reunion> reuniones = consultaReunionesPorFiltros(peticion);
		return reuniones.stream().map(reunion -> convierteReunionAHistoricoDTO(reunion))
				.collect(Collectors.toList());
	}
	
	private  HistoricoReunionDTO convierteReunionAHistoricoDTO(Reunion reunion) {
		HistoricoReunionDTO historico = new HistoricoReunionDTO();
		historico.setCodigoSFC(reunion.getReunionPk().getCodigoSfc());
		historico.setFechaReunion(utilidades.convierteLocalDateTimeToString(reunion.getFechaReunion(), formatoddmmyyyy));
		historico.setNumeroRadicado(reunion.getIdActa());
		historico.setUsuario(reunion.getUsuario());
		return historico;
	}

	@Override
	public AsambleasDTO obtenerReunionAsamblea(String codigoSFC, String fechaHora, String numeroRadicado) {
		LocalDateTime dateTime = utilidades.convierteSimpleDateToLocalDateTime(fechaHora, formatoddmmyyyy,00,00,00);
		Reunion reunion = repositorioreunion.findFirstByIdActaAndTipoAndReunionPk_CodigoSfcAndReunionPk_FechaGreaterThanEqual(numeroRadicado, TipoReunion.ASAMBLEA.toString(), codigoSFC, dateTime);
		return convierteReunionAAsambleaDTO(reunion);
		
	}

	@Override
	public ComiteDTO obtenerReunionComite(String codigoSFC, String fechaHora, String numeroRadicado) {
		LocalDateTime dateTime = utilidades.convierteSimpleDateToLocalDateTime(fechaHora, formatoddmmyyyy,00,00,00);
		Reunion reunion = repositorioreunion.findFirstByIdActaAndTipoAndReunionPk_CodigoSfcAndReunionPk_FechaGreaterThanEqual(numeroRadicado, TipoReunion.COMITE.toString(), codigoSFC, dateTime);
		return convierteReunionAComiteDTO(reunion);
	}
	
	private AsambleasDTO convierteReunionAAsambleaDTO(Reunion reunion) {
		AsambleasDTO asambleasDTO = new AsambleasDTO();
		if(reunion == null)
			return asambleasDTO;
		asambleasDTO.setCodigoSFC(reunion.getReunionPk().getCodigoSfc());
		asambleasDTO.setDireccionReunion(reunion.getDireccion());
		asambleasDTO.setOrdenDia(reunion.getOrdenDia());
		asambleasDTO.setNumeroRadicado(reunion.getIdActa());
		asambleasDTO.setNumeroAsistentes(reunion.getQuorum());
		asambleasDTO.setDecision(reunion.getDecisiones().stream().map(ServicioReunion::convierteDecisionADecisionDTO).collect(Collectors.toList()));
		asambleasDTO.setMesaDirectiva(reunion.getMiembros().stream().map(m-> convierteMiembroAAsistenteDTO(m)).collect(Collectors.toList()));
		asambleasDTO.setUsuario(reunion.getUsuario());
		asambleasDTO.setNombreArchivo(reunion.getPresentacion());
		asambleasDTO.setFechaReunion(reunion.getFechaReunion().toLocalDate().format(DateTimeFormatter.BASIC_ISO_DATE));
		asambleasDTO.setArchivo(servicioArchivo.obtenerArchivoBase64(reunion.getPresentacion()));
		return asambleasDTO;
	}
	
	private ComiteDTO convierteReunionAComiteDTO(Reunion reunion) {
		ComiteDTO comiteDTO = new ComiteDTO();
		if(reunion == null)
			return comiteDTO;
		comiteDTO.setCodigoSFC(reunion.getReunionPk().getCodigoSfc());
		comiteDTO.setDireccionReunion(reunion.getDireccion());
		comiteDTO.setOrdenDia(reunion.getOrdenDia());
		comiteDTO.setNumeroRadicado(reunion.getIdActa());
		comiteDTO.setTemasTratados(reunion.getResumenReunion());
		comiteDTO.setNombreArchivo(reunion.getPresentacion());
		comiteDTO.setDecisiones(reunion.getDecisiones().stream().map(ServicioReunion::convierteDecisionADecisionComiteDTO).collect(Collectors.toList()));
		comiteDTO.setFechaHoraReunion(reunion.getFechaReunion().toLocalDate().format(DateTimeFormatter.BASIC_ISO_DATE));
		comiteDTO.setArchivo(servicioArchivo.obtenerArchivoBase64(reunion.getPresentacion()));
		comiteDTO.setAsistentes(reunion.getAsistentesReunion().stream().map(asistente -> convierteAsistenteAAsitenteDTO(asistente)).collect(Collectors.toList()));
		return comiteDTO;
	}
	
	private static DecisionDTO convierteDecisionADecisionDTO(Decision decision) {
		DecisionDTO decisionDTO = new DecisionDTO();
		decisionDTO.setDescripcion(decision.getDescripcion());
		decisionDTO.setQuorumAprobatorio(decision.getQuorumAprobatorio());
		decisionDTO.setResponsable(decision.getResponsable());
		decisionDTO.setTipo(decision.isEstado());
		return decisionDTO;
	}
	
	private static DecisionComiteDTO convierteDecisionADecisionComiteDTO(Decision decision) {
		DecisionComiteDTO decisionDTO = new DecisionComiteDTO();
		decisionDTO.setDescripcion(decision.getDescripcion());
		List<DecisionResponsableDTO> responsables = decision.getTomadorDecision().stream().map(tomador -> {
			DecisionResponsableDTO responsable = new DecisionResponsableDTO();
			responsable.setNomResponsable(tomador.getNomResponsable());
			responsable.setNumeroDocumento(tomador.getComitePK().getNumeroDocumento());
			responsable.setTipoDocumento(tomador.getComitePK().getTipoDocumento());
			return responsable;
			}).collect(Collectors.toList());
		decisionDTO.setResponsables(responsables);
		decisionDTO.setTipo(decision.isEstado());
		return decisionDTO;
	}
	
	private AsistenteDTO convierteMiembroAAsistenteDTO(MiembroMesaDirectiva miembro) {
		AsistenteDTO asistente = new AsistenteDTO();
		asistente.setNumeroDocumento(miembro.getNumeroDocumento());
		asistente.setTipoDocumento(miembro.getTipoDocumento());
		
		String descripcionRol = servicioDominio
						.buscarValorDominio(Integer.parseInt(miembro.getIdRol()))
						.getDescripcion();
		asistente.setRol(descripcionRol);
		
		asistente.setTipo(miembro.isDirectivo());
		asistente.setNombreCompleto(miembro.getPersona().getNombreCompleto());
		return asistente;
	}
	
	private  AsistenteDTO convierteAsistenteAAsitenteDTO(Asistente asistente){
		AsistenteDTO asistenteDTO = new AsistenteDTO();
		asistenteDTO.setNumeroDocumento(asistente.getAsistentePK().getNumeroDocumento());
		asistenteDTO.setTipoDocumento(asistente.getAsistentePK().getTipoDocumento());
		Persona persona = servicioPersona.buscarPersona(asistenteDTO.getTipoDocumento(), asistenteDTO.getNumeroDocumento());
		asistenteDTO.setNombreCompleto(persona != null ? persona.getNombreCompleto():"");
		return asistenteDTO;
	}
	
	

}
