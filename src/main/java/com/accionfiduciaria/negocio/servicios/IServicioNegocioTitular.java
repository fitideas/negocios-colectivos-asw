package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.DatosDistribucionEnvioDTO;
import com.accionfiduciaria.modelo.dtos.DatosDistribucionRecepcionDTO;
import com.accionfiduciaria.modelo.dtos.ListaNegociosBeneficiarioDTO;

/**
 * Clase encargada de manejar la logica de la relación entre los negocios y sus titulares.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioNegocioTitular {
	
	/**
	 * Consulta el listado de negocios de las que una persona es beneficiaria.
	 * 
	 * @param tipoDocumento El tipo de documento d el apersona. 
	 * @param documento El número de documento de la persona.
	 * @param pagina la página en la que se ubicara la lista de resultados. 
	 * @param elementos El número de elementos que se mostrara por página. 
	 * @param tokenAutorizacion El token de autenticación asignado al usuario.
	 * @return Un listado con los códigos y nombres de los negocios encontrados.
	 * @throws Exception
	 */
	@Autowired
	public ListaNegociosBeneficiarioDTO obtenerNegocios(String tipoDocumento, String documento, int pagina, int elementos,
			String tokenAutorizacion) throws Exception;

	/**
	 * Obtiene los datos de de distribucción de un titular o un cotitular de un negocio especifico.
	 * 
	 * @param tipoDocumentoVinculado El tipo de documento del titular o el cotitular.
	 * @param documentoVinculado El número de documento del titular o el cotitular.
	 * @param codigoNegocio El código SFC del negocio.
	 * @param tokenAutorizacion El token de autorización asignado al usuario.
	 * @return Un DTO con los datos de distribucción del negocio para el titular o el cotitular.
	 */
	@Autowired
	public DatosDistribucionEnvioDTO obtenerDatosDistribucion(String tipoDocumentoVinculado, String documentoVinculado,
			String codigoNegocio, String tokenAutorizacion) throws Exception;

	/**
	 * Guarda los datos de distribución de un titular o cotitular de un negocio.
	 * 
	 * @param datosDistribucionRecepcionDTO DTO con los datos a guardar.
	 */
	@Autowired
	public void guardarDatosDistribucion(DatosDistribucionRecepcionDTO datosDistribucionRecepcionDTO) throws Exception;

}
