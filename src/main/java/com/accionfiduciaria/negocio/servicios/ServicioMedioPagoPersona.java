package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.ReprocesoPagoBeneficiarioDTO;
import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersona;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersonaPK;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.negocio.repositorios.RepositorioMedioPagoPersona;

@Service
public class ServicioMedioPagoPersona implements IServicioMedioPagoPersona {

	@Autowired
	private RepositorioMedioPagoPersona repositorioMedioPagoPersona;

	@Override
	public MedioPagoPersona guardarMedioPagoPersona(MedioPagoPersona medioPagoPersona) {
		return repositorioMedioPagoPersona.save(medioPagoPersona);
	}

	@Override
	public MedioPagoPersona buscarMedioPagoPersona(MedioPagoPersona medioPagoPersona) {
		return repositorioMedioPagoPersona.findById(medioPagoPersona.getMedioPagoPersonaPK()).orElse(null);
	}

	/**
	 * Este metodo inactiva el anterior MedioPagoPersona y retorna el nuevo
	 * MedioPagoPersona especificado en el dto de datos financieros del
	 * beneficiarios.
	 * 
	 * @author efarias
	 * @param datosBeneficiario
	 * @param medioPago
	 * @return MedioPagoPersona
	 */
	@Transactional
	@Override
	public MedioPagoPersona verificarCrearMedioPagoPersona(ReprocesoPagoBeneficiarioDTO datosBeneficiario,
			MedioPago medioPago) {
		MedioPagoPersonaPK medioPagoPersonaOldPK = new MedioPagoPersonaPK(
				datosBeneficiario.getDatoAnterior().getNumeroCuenta(),
				datosBeneficiario.getNumeroDocumentoBeneficiario(), datosBeneficiario.getTipoDocumentoBeneficiario(),
				datosBeneficiario.getDatoAnterior().getCodigoBancario(),
				datosBeneficiario.getDatoAnterior().getTipoCuenta());

		// Inactivar medio de pago anterior.
		inactivarMedioPagoPersona(medioPagoPersonaOldPK);

		MedioPagoPersonaPK medioPagoPersonaNewPK = new MedioPagoPersonaPK(medioPago.getMedioPagoPK().getCuenta(),
				datosBeneficiario.getNumeroDocumentoBeneficiario(), datosBeneficiario.getTipoDocumentoBeneficiario(),
				medioPago.getMedioPagoPK().getBanco(), medioPago.getMedioPagoPK().getTipoCuenta());

		// Valida, crea y devuelve un MedioPagoPersona
		return verificarCrearMedioPagoPersona(medioPagoPersonaNewPK);
	}

	/**
	 * Este método inactiva un medio de pago por su clave primaria.
	 * 
	 * @author efarias
	 * @param medioPagoPersonaPK
	 */
	@Transactional
	public void inactivarMedioPagoPersona(MedioPagoPersonaPK medioPagoPersonaPK) {
		MedioPagoPersona medioPagoPersona;
		medioPagoPersona = repositorioMedioPagoPersona.findById(medioPagoPersonaPK).orElse(null);
		if (medioPagoPersona != null) {
			medioPagoPersona.setEstado(Boolean.FALSE);
			repositorioMedioPagoPersona.saveAndFlush(medioPagoPersona);
		}
	}

	/**
	 * Este método valida si existe medio de pago persona de acuerdo a su clave
	 * primaria, si existe actualiza su estado a 'activo' y lo retorna, de lo
	 * contrario si no existe, crea un nuevo registro en base de datos.
	 * 
	 * @author efarias
	 * @param medioPagoPersonaPK
	 * @return MedioPagoPersona -> Nuevo si no existe, de lo conrtarrio retorna la
	 *         búsqueda con estado activo.
	 */
	@Transactional
	public MedioPagoPersona verificarCrearMedioPagoPersona(MedioPagoPersonaPK medioPagoPersonaPK) {
		MedioPagoPersona medioPagoPersona;
		medioPagoPersona = repositorioMedioPagoPersona.findById(medioPagoPersonaPK).orElse(null);

		if (medioPagoPersona != null) {
			medioPagoPersona.setEstado(Boolean.TRUE);
			return repositorioMedioPagoPersona.saveAndFlush(medioPagoPersona);
		} else {
			MedioPagoPersona crearMedioPagoPersona = new MedioPagoPersona(medioPagoPersonaPK);
			crearMedioPagoPersona.setEstado(Boolean.TRUE);
			return repositorioMedioPagoPersona.saveAndFlush(crearMedioPagoPersona);
		}
	}

	@Override
	public List<MedioPagoPersona> buscarTodosMediosPagoPersona(Persona persona) {
		return repositorioMedioPagoPersona.findByPersona(persona);
	}

}
