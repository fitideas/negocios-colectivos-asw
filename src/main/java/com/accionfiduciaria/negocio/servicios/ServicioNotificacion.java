/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.modelo.dtos.DatosCorreoNotificacionDTO;
import com.accionfiduciaria.modelo.dtos.ResponsableNotificacionDTO;
import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.HistorialNotificaciones;
import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;

/**
 * @author efarias
 *
 */

@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:propiedades.properties")
@Service
@Transactional
public class ServicioNotificacion implements IServicioNotificacion {
	
	@Autowired
	private IServicioFechaClave servicioFechaClave;

	@Autowired
	private IServicioParametroSistema servicioParametroSistema;

	@Autowired
	private IServicioEnvioCorreo servicioEnvioCorreo;

	@Autowired
	private IServicioHistorialNotificaciones servicioHistorialNotificaciones;

	@Autowired
	private Utilidades utilidades;

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	@Value("${param.id.url.servicio.accion.buscar.equipo.negocio}")
	private Integer paramIdUrlServicioAccionBusquedadEquipoNegocio;

	@Value("${param.id.asunto.correo.notificacion}")
	private Integer paramAsuntoCorreoNotificacion;

	@Value("${param.id.cuerpo.correo.notificacion}")
	private Integer paramCuerpoCorreoNotificacion;

	@Value("${param.id.direccion.correo.notificacion.fallida}")
	private Integer paramDireccionCorreoNotificacionFallida;
	
	@Value("${param.id.cuerpo.correo.error.notificacion}")
	private Integer paramIdCuerpoCorreoErrorNotificacion;
	
	@Value("${param.id.asunto.correo.error.notificacion}")
	private Integer paramIdAsuntoCorreoErrorNotificacion;

	// Constantes notificaciones
	private static final String ERROR_CORREO_NOTIFICACION = "Ha ocurrido un error al intentar el correo de notificación";
	private static final String EMPTY = "";
	private static final String GUION = " - ";
	private static final String SALTO_LINEA_HTML = "<BR>";
	private static final Logger log = Logger.getLogger(ServicioNotificacion.class.getSimpleName());

	private static final String KEY_RESPONSABLE = "@responsable";
	private static final String KEY_LISTA_FECHAS_CLAVES = "@listaFechasClaves";
	private static final String KEY_LISTA_NOTIFICACIONES_FALLIDAS = "@listaNotificacionesFallidas";
	private static final String GUIONES = "--------------------------------------";

	/**
	 * Este método permite enviar notificaciones vía email a todos los interesados
	 * del negocio que se encuentran parametrizados en el servicio de acción back
	 * 'Buscar Equipo Responsable' por medio de las distintas fechas clave
	 * parametrizadas para cada negocio.
	 * 
	 * @author efarias
	 * @throws BusquedadSinResultadosException
	 */
	@Override
	public void notificarInteresadosNegocio() throws BusquedadSinResultadosException {

		List<FechaClave> fechasClavesFuturas = servicioFechaClave
				.buscarProximasFechasClaves(java.sql.Date.valueOf(LocalDate.now()));

		List<FechaClave> fechasClavesProximasACumplirse = fechasClavesFuturas.stream()
				.filter(fecha -> validarFechaClave(fecha)).collect(Collectors.toList());

		procesarEnvioNotificaciones(obtenerFechasClavesPorNegocio(fechasClavesProximasACumplirse));
	}

	private Map<Negocio, List<FechaClave>> obtenerFechasClavesPorNegocio(
			List<FechaClave> fechasClavesProximasACumplirse) {
	
		Map<Negocio, List<FechaClave>> fechasClavesPorNegocio = new HashMap<>();
		
		fechasClavesProximasACumplirse.forEach(fechaClave -> {
			if (!fechasClavesPorNegocio.containsKey(fechaClave.getNegocio())) {
				fechasClavesPorNegocio.put(fechaClave.getNegocio(), new ArrayList<>());
			}
			fechasClavesPorNegocio.get(fechaClave.getNegocio()).add(fechaClave);
		});
		
		return fechasClavesPorNegocio;
	}

	/**
	 * 
	 * @param fechasClavesPorNegocio
	 * @return
	 */
	private void procesarEnvioNotificaciones(Map<Negocio, List<FechaClave>> fechasClavesPorNegocio) {
		
		Map<ResponsableNotificacionDTO, Map<Negocio, List<FechaClave>>> equipoResponsable = obtenerEquipoResponsablePorNegocio(
				fechasClavesPorNegocio);
		
		DatosCorreoNotificacionDTO datosCorreoNotificacionDTO = inicializarDatosCorreoNotificacion();
		
		Map<ResponsableNotificacionDTO, String> cuerpoCorreoPorResponsable = new HashMap<>();

		equipoResponsable.forEach((responsable, fechasClavesNegocio) -> 
			cuerpoCorreoPorResponsable.put(responsable,
					construirCuerpoCorreo(responsable, fechasClavesNegocio, datosCorreoNotificacionDTO.getCuerpoCorreoNotificacion()))
		);

		List<ResponsableNotificacionDTO> responsablesNoNotificados = new ArrayList<>();
		equipoResponsable.forEach((responsable, fechasClavesNegocio) -> {

			// se obtiene el correo del responsable asociado al negocio
			String correo = responsable.getCorreo() == null ? EMPTY : responsable.getCorreo().trim();
			boolean correoValido = utilidades.validarCorreo(correo); 
			
			// Se valida si el correo tiene una estructura valida
			if (correoValido) {
				enviarCorreoElectronico(correo, datosCorreoNotificacionDTO.getAsuntoCorreoNotificacion(), cuerpoCorreoPorResponsable.get(responsable));
			} else {
				responsablesNoNotificados.add(responsable);
			}
			equipoResponsable.get(responsable).forEach((negocio,fechasClaves) ->
				fechasClaves.forEach(fechaClave->guardarAuditoriaNotificacion(responsable, fechaClave, cuerpoCorreoPorResponsable.get(responsable), correoValido))
			);
		});
		// Se envía un correo indicando que los usuarios no se pudieron contactar
		if (!responsablesNoNotificados.isEmpty()) {
			enviarNotificacionError(datosCorreoNotificacionDTO.getDireccionCorreoNotificacionFallida(), cuerpoCorreoPorResponsable, responsablesNoNotificados);
		}
		
	}

	private Map<ResponsableNotificacionDTO, Map<Negocio, List<FechaClave>>> obtenerEquipoResponsablePorNegocio(
			Map<Negocio, List<FechaClave>> fechasClavesPorNegocio) {
		
		Map<ResponsableNotificacionDTO, Map<Negocio, List<FechaClave>>> equipoResponsable = new HashMap<>();

		fechasClavesPorNegocio.keySet().forEach(negocio -> 
			negocio.getResponsablesNegocio().forEach(responsable -> {
				if (responsable.isActivo()) {
					ResponsableNotificacionDTO responsableNotificacionDTO = new ResponsableNotificacionDTO(responsable);
					if (!equipoResponsable.containsKey(responsableNotificacionDTO)) {
						equipoResponsable.put(responsableNotificacionDTO, new HashMap<>());
					}
					equipoResponsable.get(responsableNotificacionDTO).put(negocio, fechasClavesPorNegocio.get(negocio));
				}
			})
		);
		return equipoResponsable;
	}

	/**
	 * Método que guarda la auditoria de envio de notificación
	 * 
	 * @author efarias
	 * @param responsable
	 * @param fechaClave
	 * @param cuerpoMensaje
	 * @param envioNotificacion
	 */
	public void guardarAuditoriaNotificacion(ResponsableNotificacionDTO responsable, FechaClave fechaClave,
			String cuerpoMensaje, boolean envioNotificacion) {
		HistorialNotificaciones auditoria = new HistorialNotificaciones(fechaClave.getFechaClavePK(), new Date(),
				responsable.getNumeroDocumento(), responsable.getNombre(),
				responsable.getTipoResponsable(), responsable.getCorreo(), cuerpoMensaje, envioNotificacion);
		servicioHistorialNotificaciones.guardarAuditoriaNotificaciones(auditoria);

	}

	/**
	 * Se arma el mensaje con la notificación de error y se envía el correo
	 * electrónico.
	 * 
	 * @author efarias
	 * @param datosCorreo
	 * @param cuerpoMensaje
	 * @param usuariosNoNotificados
	 */
	private void enviarNotificacionError(String direccionCorreoNotificacionEntregada, Map<ResponsableNotificacionDTO, String> cuerpoCorreoPorResponsable,
			List<ResponsableNotificacionDTO> responsablesNoNotificados) {

		String asuntoCorreoErrorNotificacion = servicioParametroSistema.obtenerValorParametro(paramIdAsuntoCorreoErrorNotificacion);
		String cuerpoCorreoErrorNotificacion = servicioParametroSistema.obtenerValorParametro(paramIdCuerpoCorreoErrorNotificacion);
		StringBuilder listaNotificacionesNoEntregadas = new StringBuilder();
		
		responsablesNoNotificados.forEach(responsable -> 
			listaNotificacionesNoEntregadas.append(SALTO_LINEA_HTML).append(cuerpoCorreoPorResponsable.get(responsable)).append(SALTO_LINEA_HTML).append(GUIONES).append(SALTO_LINEA_HTML)
		);
		cuerpoCorreoErrorNotificacion = cuerpoCorreoErrorNotificacion.replace(KEY_LISTA_NOTIFICACIONES_FALLIDAS, listaNotificacionesNoEntregadas.toString());

		// Se envia correo electrónico con el mensaje de error
		enviarCorreoElectronico(direccionCorreoNotificacionEntregada,
				asuntoCorreoErrorNotificacion, cuerpoCorreoErrorNotificacion);
	}

	/**
	 * método que envia el correo electrónico.
	 * 
	 * @author efarias
	 * @param destinatario
	 * @param asunto
	 * @param cuerpoCorreo
	 */
	private void enviarCorreoElectronico(String destinatario, String asunto, String cuerpoCorreo) {
		try {
			servicioEnvioCorreo.enviarCorreo(destinatario, asunto, cuerpoCorreo);
		} catch (MessagingException e) {
			log.log(Level.SEVERE, ERROR_CORREO_NOTIFICACION, e);
		}
	}

	/**
	 * Valida si una fecha clave es apta para realizar el envío de notificaciones en
	 * base a las siguientes criterios: 1 - La fecha clave es mayor a la fecha
	 * actual. 2 - La resta de los días parametrizados a la fecha clave debe ser
	 * menor o igual a la fecha actual.
	 * 
	 * @author efarias
	 * @param fecha
	 * @return verdadero si la fecha cumple con los criterios dados, de lo contrario
	 *         falso.
	 */
	private boolean validarFechaClave(FechaClave fecha) {
		long diasDefault = 3;
		LocalDate fechaActual = LocalDate.now();
		LocalDate fechaClave = utilidades.transformarDateALocalDate(fecha.getFechaClavePK().getFecha());
		
		// Si la fecha es mayor o igual a la fecha actual.
		if (fechaClave.isEqual(fechaActual) || fechaClave.isAfter(fechaActual)) {
			Long diasARestar = fecha.getDominioFechaClave() == null ? diasDefault
					: Long.parseLong(fecha.getDominioFechaClave().getValor());
			fechaClave = fechaClave.minusDays(diasARestar);
			
			// Si la nueva fecha es menor o igual a la fecha actual.
			if (fechaClave.isEqual(fechaActual) || fechaClave.isBefore(fechaActual)) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
		} else {
			return Boolean.FALSE;
		}
	}

	/**
	 * Este método realiza el reemplazo de variables en el cuerpo del mensaje del
	 * correo de notificaciones de pago.
	 * 
	 * @author efarias
	 * @param cuerpoCorreoNotificacion
	 * @param fechaClave
	 * @param negocio
	 * @return cuerpo de correo.
	 */
	private String construirCuerpoCorreo(ResponsableNotificacionDTO responsableNotificacionDTO,
			Map<Negocio, List<FechaClave>> fechasClavesPorNegocio, String cuerpoCorreo) {
		cuerpoCorreo = cuerpoCorreo.replace(KEY_RESPONSABLE, responsableNotificacionDTO.getNombre());

		StringBuilder listaFechasANotificar = new StringBuilder();

		fechasClavesPorNegocio.forEach((negocio, fechasClaves) -> {
			listaFechasANotificar.append(negocio.getCodSfc()).append(GUION).append(negocio.getNombre()).append(SALTO_LINEA_HTML);
			fechasClaves.forEach(fechaClave -> 
				listaFechasANotificar.append(fechaClave.getNombre()).append(GUION)
						.append(Utilidades.formatearFecha(fechaClave.getFechaClavePK().getFecha(), formatoddmmyyyy))
						.append(GUION).append(fechaClave.getDominioFechaClave().getDescripcion()).append(SALTO_LINEA_HTML)
			);
			listaFechasANotificar.append(SALTO_LINEA_HTML);
		});

		cuerpoCorreo = cuerpoCorreo.replace(KEY_LISTA_FECHAS_CLAVES, listaFechasANotificar.toString());
		
		return cuerpoCorreo;
	}

	/**
	 * Método de inicializa los datos necesarios para el envio de notificación de
	 * pagos.
	 * 
	 * @author efarias
	 * @return datos correo notificación
	 */
	private DatosCorreoNotificacionDTO inicializarDatosCorreoNotificacion() {
		String asuntoCorreoNotificacion = servicioParametroSistema.obtenerValorParametro(paramAsuntoCorreoNotificacion);
		String cuerpoCorreoNotificacion = servicioParametroSistema.obtenerValorParametro(paramCuerpoCorreoNotificacion);
		String direccionCorreoNotificacionFallida = servicioParametroSistema
				.obtenerValorParametro(paramDireccionCorreoNotificacionFallida);
		return new DatosCorreoNotificacionDTO(asuntoCorreoNotificacion, cuerpoCorreoNotificacion,
				direccionCorreoNotificacionFallida);
	}

}