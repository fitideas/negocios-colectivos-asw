/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.excepciones.ParametroNoEncontradoException;

/**
 * Servicio utilizado para el manejo de los parámetros de la aplicación.
 * 
 * @author José Santiago Polo Acosta - jpolo@asesoftware.com
 *
 */

@Service
public interface IServicioParametroSistema {
	
	/**
	 * Busca un registro en la tabla CONF_PARAMETRO_SISTEMA usando el id.
	 * 
	 * @param idParametro el id del registro a consultar.
	 * @return los datos del registro encontrado. null en caso que la busquedad no obtenga un resultado.
	 */
	@Autowired
	public String obtenerValorParametro(Integer idParametro) throws ParametroNoEncontradoException;
	
	@Autowired
	public String obtenerValorCron() throws ParametroNoEncontradoException;
}
