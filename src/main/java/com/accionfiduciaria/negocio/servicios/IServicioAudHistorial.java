package com.accionfiduciaria.negocio.servicios;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.accionfiduciaria.excepciones.ErrorAlConectarAccionException;
import com.accionfiduciaria.modelo.dtos.ConteoTipoNegocio;
import com.accionfiduciaria.modelo.dtos.DatosPersonaUsuarioDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoCambioDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoCesionesCambioDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaConteoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaModificacionesValores;
import com.accionfiduciaria.modelo.entidad.AudHistorial;
import com.accionfiduciaria.modelo.entidad.AudHistorialDetalle;
import com.accionfiduciaria.modelo.entidad.SolicitudCambiosDatos;

public interface IServicioAudHistorial {

	/**
	 * @param dto
	 * @param tabla
	 * @param columnas
	 * @param operaciones
	 * @return
	 */
	public List<AudHistorial> consultarConFiltros(ParametrosFiltroDTO dto,  List<String> tabla, List<String> columnas, List<String> operaciones, String columnaOrdenar) ;

	public RespuestaConteoDTO obtenerConteoSolicitiduesCambio(ParametrosFiltroDTO dto);

	public RespuestaConteoDTO obtenerConteoCambiosDistribucion(ParametrosFiltroDTO dto);

	public RespuestaConteoDTO obtenerConteoCambiosBeneficiarios(ParametrosFiltroDTO dto);

	public RespuestaConteoDTO obtenerConteoCambiosCesiones(ParametrosFiltroDTO dto, String tokenAutorizacion) throws Exception;

	public List<DatosPersonaUsuarioDTO> obtenerUsuarios();

	List<HistoricoCambioDTO> consultarHistoricoAuditoria(ParametrosFiltroDTO dto, String columna, String tipoHistorico, String tokenAutorizacion);
	
	public List<SolicitudCambiosDatos> consultarConFiltrosBenSolicitudCambios(ParametrosFiltroDTO dto);

	public RespuestaModificacionesValores obtenerDatosInformacionNegocio(ParametrosFiltroDTO dto);
	
	List<HistoricoCesionesCambioDTO> obtenerHistoricoCambiosCesiones(ParametrosFiltroDTO dto, String tokenAutorizacion) throws ErrorAlConectarAccionException;
	
	/**
	 *Para este metodo solo aplica el fltro de fecha final por que si no dejaria por fuera a losnegocios que han sido c
	 *creados con anterioridad a la fecha de inicio
	 */
	public List<ConteoTipoNegocio> obtenerConteoDeTiposDeNegocio(ParametrosFiltroDTO dto, String token);

	
	public Map<String, List<String>> obtenerTiposDelNegocio(List<String> tipos, String negocioFiltrado,
			Date fechaMaxima);
	
	public List<AudHistorialDetalle> obtenerUltimoPagoSegunFechaCobro(Date s1, String s2, String s3, String s4);

}
