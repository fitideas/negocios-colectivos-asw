package com.accionfiduciaria.negocio.servicios;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.enumeradores.TipoExtensionArchivo;
import com.accionfiduciaria.enumeradores.TiposArchivo;
import com.accionfiduciaria.modelo.dtos.BeneficiarioPagosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;
import com.accionfiduciaria.modelo.dtos.TitularPagoDto;
import com.accionfiduciaria.negocio.componentes.Utilidades;


@Service
public class ServicioArchivo implements IServicioArchivo {

	private Logger log = LogManager.getLogger(ServicioArchivo.class);
	
	@Autowired
	private IServicioParametroSistema servicioParametros;
	
	@Autowired
	private Utilidades utilidades;
	
	@Value("${param.id.ruta.almacenamiento.archivos}")
	private Integer paramIdRutaAlmacenamientoArchivos;
	
	@Override
	public void guardarArchivo(byte[] archivo, String nombreArchivo) throws IOException {
		if(archivo.length > 0) {
			String ruta = servicioParametros.obtenerValorParametro(paramIdRutaAlmacenamientoArchivos);
			Path path = Paths.get(ruta + File.separator + nombreArchivo);
			Files.write(path, archivo);
		}
	}

	/**
	 * Metodo que permite construir un archivo a partir de una lista de datos y etiquetas
	 * @param registros Datos para diligenciar el archivo
	 * @param etiquetas Cabecera del archivo
	 * @param extension Tipo del archivo
	 * @return RespuestaArchivoDTO DTO con el archivo procesado en bytes.
	 * @throws IllegalAccessException 
	 * @throws InvalidFormatException 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public RespuestaArchivoDTO construirArchivoExcel(List<?> registros, List<String> etiquetas, TipoExtensionArchivo extension)
			throws IOException, IllegalAccessException{
		if(extension.equals(TipoExtensionArchivo.EXCEL) && !registros.isEmpty() && registros.get(0) instanceof BeneficiarioPagosDTO)
			return generaArchivoExcel((List<BeneficiarioPagosDTO>)registros, etiquetas);
		else if(extension.equals(TipoExtensionArchivo.EXCEL) && !registros.isEmpty() && registros.get(0) instanceof TitularPagoDto)
			return generaArchivoBeneficiariosExcel((List<TitularPagoDto>)registros, etiquetas);
		return null;
	}

	private RespuestaArchivoDTO generaArchivoBeneficiariosExcel(List<TitularPagoDto> registros, List<String> etiquetas)throws IOException, IllegalAccessException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet pagina = workbook.createSheet("Reporte detallado de Beneficiarios");
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.AQUA.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFRow fila = pagina.createRow(0);
		// Creamos la cabecera del archivo de excel y asignamos estilos
		for (int i = 0; i < etiquetas.size(); i++) {
			Cell celda = fila.createCell(i);
			celda.setCellStyle(style);
			celda.setCellValue(etiquetas.get(i));
		}
		
		for (Iterator<TitularPagoDto> iterator = registros.iterator(); iterator.hasNext();) {
			TitularPagoDto titularDto = iterator.next();
			fila = pagina.createRow(pagina.getLastRowNum()+1);
			Field[] campos = titularDto.getClass().getDeclaredFields();
			int posicion = 0;
			for (int i = 0; i < campos.length; i++) {
				if(ResolvableType.forField(campos[i]).asCollection() != ResolvableType.NONE) {
					List<BeneficiarioPagosDTO> beneficiarios = titularDto.getBeneficiarios();
					for (Iterator<BeneficiarioPagosDTO> iterator2 = beneficiarios.iterator(); iterator2.hasNext();) {
						posicion = 5;
						BeneficiarioPagosDTO beneficiarioPagosDTO = iterator2.next();
						Field[] camposPago = beneficiarioPagosDTO.getClass().getDeclaredFields();
						for(int j = 0; j < camposPago.length; j++) {
							escribirCelda(fila, camposPago[j], beneficiarioPagosDTO, posicion);
							posicion++;
						}
						if(iterator2.hasNext()) {
							fila = pagina.createRow(pagina.getLastRowNum()+1);	
						}
					}
				}else {
					escribirCelda(fila, campos[i], titularDto, posicion);
					posicion++;
				}
					
			}
		}
		// Exportamos el archivo de Excel
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		workbook.write(stream);
		workbook.close();
		RespuestaArchivoDTO archivoDTO = new RespuestaArchivoDTO();
		archivoDTO.setTipoArchivo(TiposArchivo.VALIDO.toString());
		archivoDTO.setRecurso(new ByteArrayResource(stream.toByteArray()));
		return  archivoDTO;
	}
	
	private void escribirCelda(XSSFRow fila, Field campo, Object objeto, int posicion) throws IllegalAccessException {
			Cell celda = fila.createCell(posicion);
			Object valorCampo = FieldUtils.readField(campo, objeto, true);
			celda.setCellValue(valorCampo == null ? "":valorCampo.toString());
	}

	/**
	 * Metodo que permite construir un archivo excel para historico de pagos a partir de una lista de datos y etiquetas
	 * @param registros Datos para diligenciar el archivo
	 * @param etiquetas Cabecera del archivo
	 * @param extension Extension de tipo XLSX
	 * @return RespuestaArchivoDTO DTO con el archivo procesado en bytes.
	 * @throws InvalidFormatException 
	 */
	private RespuestaArchivoDTO generaArchivoExcel(List<BeneficiarioPagosDTO> registros, List<String> etiquetas)throws IOException{
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet pagina = workbook.createSheet("Reporte de Historial de Pagos");
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.AQUA.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFRow fila = pagina.createRow(0);
		// Creamos la cabecera del archivo de excel y asignamos estilos
		for (int i = 0; i < etiquetas.size(); i++) {
			Cell celda = fila.createCell(i);
			celda.setCellStyle(style);
			celda.setCellValue(etiquetas.get(i));
		}
		// Diligenciamos el archivo mediante el Api Reflection de Java  
		for (Iterator<BeneficiarioPagosDTO> iterator = registros.iterator(); iterator.hasNext();) {
			BeneficiarioPagosDTO pago = iterator.next();
			fila = pagina.createRow(pagina.getLastRowNum()+1);
			Field[] campos = pago.getClass().getDeclaredFields();
			for (int i = 0; i < campos.length; i++) {
				Cell celda = fila.createCell(i);
				try {
					Object valorCampo = FieldUtils.readField(campos[i], pago, true);
					celda.setCellValue(valorCampo == null ? "":valorCampo.toString());
				}  catch (IllegalAccessException e) {
					log.error(e.getStackTrace());
				}
			}
		}
		// Exportamos el archivo de Excel
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		workbook.write(stream);
		workbook.close();
		RespuestaArchivoDTO archivoDTO = new RespuestaArchivoDTO();
		archivoDTO.setTipoArchivo(TiposArchivo.VALIDO.toString());
		archivoDTO.setRecurso(new ByteArrayResource(stream.toByteArray()));
		return  archivoDTO;
	}

	@Override
	public String obtenerArchivoBase64(String nombreAchivo){
		String ruta = servicioParametros.obtenerValorParametro(paramIdRutaAlmacenamientoArchivos);
		try(
		FileInputStream fileInputStream = new FileInputStream(ruta + nombreAchivo)) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			int numRead;
			byte[] buffer = new byte[16384];
			while ((numRead = fileInputStream.read(buffer, 0, buffer.length)) != -1) {
				  stream.write(buffer, 0, numRead);
			}
			return utilidades.convierteBytesToBase64(stream.toByteArray());
		} catch (IOException e) {
			return "";
		} 
	}
	
	

}
