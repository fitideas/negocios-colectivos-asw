/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.enumeradores.EstadosDistribucionBeneficiario;
import com.accionfiduciaria.modelo.dtos.DetalleDistribucionBeneficiarioDTO;
import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.BeneficiarioPK;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.DistribucionPK;
import com.accionfiduciaria.modelo.entidad.RelacionEntreDominios;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucionBeneficiario;

/**
 * @author efarias
 *
 */
@Service
@Transactional
@PropertySource("classpath:propiedades.properties")
public class ServicioDistribucionBeneficiario implements IServicioDistribucionBeneficiario {
	
	@Autowired
	private RepositorioDistribucionBeneficiario repositorioDistribucionBeneficiario;
	
	@Autowired
	private IServicioRelacionEntreDominios servicioRelacionEntreDominios;
	
	@Autowired
	private IServicioDominio servicioDominio; 
	
	@Autowired
	private IServicioRetencionImpuestoBeneficiario servicioRetencionImpuestoBeneficiario;
	
	@Value("${cod.dominio.tipos.cuenta}")
	private String codigoDominioTipoCuenta;
	
	@Value("${cod.dominio.banco}")
	private String codigoDominioBanco;
	
	@Value("${cod.dominio.tipos.cuenta}")
	private String codigoDominioCostoMedioPago;
	
	// Constantes
	public static final BigDecimal PORCENTAJE = new BigDecimal(100);
	public static final BigDecimal CUATRO_X_MIL = BigDecimal.valueOf(0.004);

	/**
	 * Este método realiza los cálculos necesarios para beneficiario asociado a un
	 * negocio, guardar y devuelve la lista con el detalle de distribucion de cada
	 * beneficiario.
	 * 
	 * @author efarias
	 * @param listaBeneficiarios
	 * @param distribucion       (DISTRIBUCION DISPONIBLE)
	 * @return lista de distribucion de todos los beneficiarios
	 */
	@Override
	public List<DistribucionBeneficiario> guardarDistribucionBeneficiarios(List<Beneficiario> listaBeneficiarios,
			Distribucion distribucion) {

		List<DistribucionBeneficiario> listaDistribucion = new ArrayList<>();
		if (!listaBeneficiarios.isEmpty()) {
			DistribucionPK distribucionPK = distribucion.getDistribucionPK();
			listaBeneficiarios.forEach(beneficiario -> listaDistribucion
					.add(calcularDistribucionBeneficiario(beneficiario, distribucionPK, distribucion)));
			// Guarda el detalle de todos los beneficiarios.
			return repositorioDistribucionBeneficiario.saveAll(listaDistribucion);

		} else {
			return listaDistribucion;
		}
	}
	
	/**
	 * Este método crea el objeto con el detalle de distribución para cada
	 * beneficiario.
	 * 
	 * @author efarias
	 * @param beneficiario
	 * @param distribucionPK
	 * @param distribucion
	 * @return DistribucionBeneficiario ->
	 */
	public DistribucionBeneficiario calcularDistribucionBeneficiario(Beneficiario beneficiario,
			DistribucionPK distribucionPK, Distribucion distribucion) {

		// Cálculo valores e inicialización variables
		BeneficiarioPK beneficiarioPK = beneficiario.getBeneficiarioPK();
		BigDecimal valorPago = distribucion.getTotalDistribucion()
				.multiply(beneficiario.getPorcentaje().divide(PORCENTAJE));
		BigDecimal valorMedioPago = consultarValorMedioPago(beneficiario.getBeneficiarioPK().getTipoCuenta());
		BigDecimal subTotal =	valorPago.subtract(valorMedioPago);
		BigDecimal valorGMF = subTotal.divide(BigDecimal.valueOf(1.004),RoundingMode.HALF_DOWN).multiply(CUATRO_X_MIL);
		BigDecimal valorMenosGMF = subTotal.subtract(valorGMF);

		// Creación detalle distribución
		DistribucionBeneficiario distribucionBeneficiario = new DistribucionBeneficiario(beneficiarioPK,
				distribucionPK);
		distribucionBeneficiario.setEstado(EstadosDistribucionBeneficiario.ACEPTADO.getValor());
		distribucionBeneficiario.setPorcentajeParticipacion(beneficiario.getPorcentaje());
		distribucionBeneficiario.setPorcentajeGiro(beneficiario.getPorcentajeGiro());
		distribucionBeneficiario.setValorPago(valorPago);
		distribucionBeneficiario.setValorGMF(valorGMF);
		distribucionBeneficiario.setValorTraslado(valorMedioPago);
		distribucionBeneficiario.setValorMenosDescuento(valorMenosGMF);
		distribucionBeneficiario.setBeneficiario(beneficiario);
		return distribucionBeneficiario;
	}

	/**
	 * Este método busca el costo por tipo de cuenta.
	 * 
	 * @author efarias
	 * @param tipoCuenta
	 * @return valor de la transacción por tipo de cuenta
	 */
	public BigDecimal consultarValorMedioPago(Integer tipoCuenta) {
		List<RelacionEntreDominios> relacionDominio = servicioRelacionEntreDominios
				.buscarRelacionDominioPorIdValorDominioYCodigoDominio(tipoCuenta, codigoDominioCostoMedioPago);
		if (tipoCuenta == null || relacionDominio.isEmpty()) {
			return BigDecimal.ZERO;
		} else {
			// La relación es puntual, solo debe existir un registro.
			RelacionEntreDominios relacion = relacionDominio.get(0);
			return new BigDecimal(relacion.getValorDominioNegocio().getValor());
		}
	}
	
	/**
	 * Este metodo busca el tipo de pago.
	 * 
	 * @author efarias
	 * @param tipoCuenta
	 * @return valor de la transacción por tipo de cuenta
	 */
	public String consultarTipoMedioPago(Integer tipoCuenta) {
		List<RelacionEntreDominios> relacionDominio = servicioRelacionEntreDominios
				.buscarRelacionDominioPorIdValorDominioYCodigoDominio(tipoCuenta, codigoDominioCostoMedioPago);
		if (tipoCuenta == null || relacionDominio.isEmpty()) {
			return "";
		} else {
			// La relación es puntual, solo debe existir un registro.
			RelacionEntreDominios relacion = relacionDominio.get(0);
			return relacion.getValorDominioNegocio().getDescripcion();
		}
	}

	@Override
	public Map<String, Object> listaDistribucionADTO(List<DistribucionBeneficiario> listaDistribucion) {

		List<DetalleDistribucionBeneficiarioDTO> listaDetalleDTO = new ArrayList<>();
		Map<String, Object> respuesta = new HashMap<>();

		listaDistribucion.forEach(detalle -> {

			ValorDominioNegocio tipoCuenta = servicioDominio.buscarValorDominioPorCodDominioYValorDominio(
					codigoDominioTipoCuenta, detalle.getDistribucionBeneficiarioPK().getTipoCuenta());

			ValorDominioNegocio banco = servicioDominio.buscarValorDominioPorCodDominioYValorDominio(codigoDominioBanco,
					detalle.getDistribucionBeneficiarioPK().getBanco());

			DetalleDistribucionBeneficiarioDTO dto = new DetalleDistribucionBeneficiarioDTO();
			dto.setNombreTitular(detalle.getBeneficiario().getTitular().getPersona().getNombreCompleto());
			dto.setTipoDocumentoTitular(detalle.getDistribucionBeneficiarioPK().getTipoDocumentoTitular());
			dto.setDocumentoTitular(detalle.getDistribucionBeneficiarioPK().getNumeroDocumentoTitular());
			dto.setNumeroDerechos(new BigDecimal(detalle.getBeneficiario().getTitular().getNumeroDerechos()));
			dto.setPorcentajeParticipacion(detalle.getBeneficiario().getTitular().getPorcentaje());
			dto.setNumeroDerechos(new BigDecimal(detalle.getBeneficiario().getTitular().getNumeroDerechos()));
			dto.setNombreBeneficiario(detalle.getBeneficiario().getMedioPagoPersona().getPersona().getNombreCompleto());
			dto.setTipoDocumento(detalle.getDistribucionBeneficiarioPK().getTipoDocumentoBeneficiario());
			dto.setDocumento(detalle.getDistribucionBeneficiarioPK().getNumeroDocumentoBeneficiario());
			dto.setPorcentajeGiro(detalle.getPorcentajeGiro());
			dto.setPorcentajedistribuido(detalle.getPorcentajeParticipacion());
			dto.setValorGiro(detalle.getValorPago());
			dto.setPorcentajeParticipacionDistribuido(detalle.getPorcentajeParticipacion());
			dto.setSubtotalPago(detalle.getValorMenosDescuento());
			dto.setGravamenMovimientoFinanciero(detalle.getValorGMF());
			dto.setTipoPago(consultarTipoMedioPago(detalle.getDistribucionBeneficiarioPK().getTipoCuenta()));
			dto.setValorTipoPago(consultarValorMedioPago(detalle.getDistribucionBeneficiarioPK().getTipoCuenta()));
			dto.setSubtotalGirado(detalle.getValorMenosDescuento());
			dto.setTipoCuenta(tipoCuenta != null ? tipoCuenta.getDescripcion() : "");
			dto.setCuentaEncargo(detalle.getDistribucionBeneficiarioPK().getCuenta());
			dto.setEntidadBancariaFideicomiso(banco != null ? banco.getDescripcion() : "");
			dto.setCodigoBanco(banco != null ? banco.getValor() : "");
			listaDetalleDTO.add(dto);
		});
		respuesta.put("listaBeneficiarios", listaDetalleDTO);
		return respuesta;
	}
	
	/**
	 * Este método crea un nuevo registro de DistribucionBeneficiario con los nuevos
	 * datos del medio de pago de beneficiario y el estado reprocesado, actualiza
	 * los registros de retencion de beneficiario y elimina los registros de
	 * DistribucionBeneficiario antiguo.
	 * 
	 * @author efarias
	 * @param distribucionBeneficiario
	 * @param beneficiario
	 * @return DistribucionBeneficiario -> con un nuevo MedioPagoPersona y estado
	 */
	@Override
	public DistribucionBeneficiario verificarModificarDistribucionBeneficiario(
			DistribucionBeneficiario distribucionBeneficiario, Beneficiario beneficiario) {

		// Agrega un nuevo registro de distribución.
		DistribucionBeneficiario distribucionBeneficiarioModificado = reprocesarDistribucionBeneficiario(
				distribucionBeneficiario, beneficiario);

		// Se agrega las nuevas retenciones de la nueva distribución.
		servicioRetencionImpuestoBeneficiario.agregarRetencionImpuestos(distribucionBeneficiarioModificado,
				distribucionBeneficiario);

		// Se elimina registros de retencion de beneficiario antiguo:
		servicioRetencionImpuestoBeneficiario.eliminarRetencionesPorDistribucionBeneficiario(distribucionBeneficiario);
		repositorioDistribucionBeneficiario
				.eliminarDistribucionBeneficiario(distribucionBeneficiario.getDistribucionBeneficiarioPK());
		return distribucionBeneficiarioModificado;
	}
	
	/**
	 * Este método crea el nuevo registro de distribución de beneficiario a partir
	 * de los nuevos datos del medio de pago del beneficiario.
	 * 
	 * @author efarias
	 * @param distribucionBeneficiario
	 * @param beneficiario
	 * @return DistribucionBeneficiario -> distribución actualizada
	 */
	private DistribucionBeneficiario reprocesarDistribucionBeneficiario(
			DistribucionBeneficiario distribucionBeneficiario, Beneficiario beneficiario) {

		DistribucionPK distribucionPK = distribucionBeneficiario.getDistribucion().getDistribucionPK();
		DistribucionBeneficiario distribucion = new DistribucionBeneficiario(beneficiario.getBeneficiarioPK(),
				distribucionBeneficiario.getDistribucion().getDistribucionPK());

		distribucion.setEstado(EstadosDistribucionBeneficiario.REPROCESADO.getValor());
		distribucion.setPorcentajeParticipacion(distribucionBeneficiario.getPorcentajeParticipacion());
		distribucion.setPorcentajeGiro(distribucionBeneficiario.getPorcentajeGiro());
		distribucion.setValorPago(distribucionBeneficiario.getValorPago());
		distribucion.setValorGMF(distribucionBeneficiario.getValorGMF());
		distribucion.setValorTraslado(distribucionBeneficiario.getValorTraslado());
		distribucion.setValorMenosDescuento(distribucionBeneficiario.getValorMenosDescuento());
		distribucion.setDistribucionCodigoSFC(distribucionPK.getCodSfc());
		distribucion.setDistribucionTipoNegocio(distribucionPK.getTipoNegocio());
		distribucion.setFechaHora(distribucionPK.getFechaHora());
		distribucion.setPeriodo(distribucionPK.getPeriodo());

		return repositorioDistribucionBeneficiario.saveAndFlush(distribucion);
	}

	@Override
	public DistribucionBeneficiario agregarInformacionDistribucionBeneficiario(DistribucionBeneficiario distribucion,
			DistribucionBeneficiario distribucionBeneficiario, Beneficiario beneficiario) {
		ValorDominioNegocio tipoCuenta = servicioDominio.buscarValorDominioPorCodDominioYValorDominio(
				codigoDominioTipoCuenta, beneficiario.getBeneficiarioPK().getTipoCuenta());
		distribucion.setDominioCuenta(tipoCuenta);
		// Se agrega dominio cuenta.
		ValorDominioNegocio banco = servicioDominio.buscarValorDominioPorCodDominioYValorDominio(codigoDominioBanco,
				beneficiario.getBeneficiarioPK().getBanco());
		distribucion.setDominioBanco(banco);
		// Se agrega beneficiario
		distribucion.setBeneficiario(beneficiario);
		// Se agrega distribución
		distribucion.setDistribucion(distribucionBeneficiario.getDistribucion());
		// Se agrega negocio
		distribucion.setNegocio(distribucionBeneficiario.getDistribucion().getNegocio());
		return distribucion;
	}
	


}
