package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.enumeradores.Estados;
import com.accionfiduciaria.excepciones.ErrorAlConectarAccionException;
import com.accionfiduciaria.modelo.dtos.ConteoTipoNegocio;
import com.accionfiduciaria.modelo.dtos.DatosPersonaUsuarioDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoCambioDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoCesionesCambioDTO;
import com.accionfiduciaria.modelo.dtos.NegocioFiltroDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionConsultaCesionesSucesionesNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaConteoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaModificacionesValores;
import com.accionfiduciaria.modelo.entidad.AudHistorial;
import com.accionfiduciaria.modelo.entidad.AudHistorialDetalle;
import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.ResponsableNotificacion;
import com.accionfiduciaria.modelo.entidad.SolicitudCambiosDatos;
import com.accionfiduciaria.modelo.entidad.Usuario;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.especificaciones.AudHistorialDetalleEspecificacion;
import com.accionfiduciaria.negocio.especificaciones.AudHistorialEspecificacion;
import com.accionfiduciaria.negocio.especificaciones.SolicitudCambioEspecificacion;
import com.accionfiduciaria.negocio.repositorios.RepositorioAudHistorial;
import com.accionfiduciaria.negocio.repositorios.RepositorioAudHistorialDetalle;
import com.accionfiduciaria.negocio.repositorios.RepositorioSolicitudCambioDatos;
import com.accionfiduciaria.negocio.repositorios.RepositorioValorDominioNegocio;
import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioUsuario;

@Service
@PropertySource("classpath:propiedades.properties")
public class ServicioAudHistorial implements IServicioAudHistorial {

	// constantes
	private static final String DIFERENCIA_ANO = "difAno";
	private static final String DIFERENCIA_MES = "difMes";
	private static final String INSERT = "INSERT";
	private static final String UPDATE = "UPDATE";
	private static final String DELETE = "DELETE";
	private static final String mediaNoche = " 23:59:59";

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	@Value("${formato.ddmmyyyy.guion")
	private String formatoddmmyyguion;

	@Value("${cod.dominio.tipo.negocio}")
	private String dominioTipoNegocio;

	@Value("${tabla.neg_titular}")
	private String tablaNegTitular;
	@Value("${columna.neg_titular.cotitular}")
	private String columnaCotitular;
	@Value("${tabla.neg_retencion_titular}")
	private String tablaNegRetTitular;

	@Value("${columna.neg_retencion_titular.id_retencion}")
	private String idRetencion;
	@Value("${columna.neg_titular.id_tip_nat_jur}")
	private String idtipNatJur;
	@Value("${tabla.neg_beneficiario}")
	private String tablaNegBen;
	@Value("${tabla.neg_medio_pago_persona}")
	private String tablaNegMedPago;
	@Value("${columna.neg_beneficiario.estado}")
	private String columnaEstadoBen;
	@Value("${columna.neg_negocio.estado}")
	private String columnaEstadoNeg;
	@Value("${columna.neg_beneficiario.num_doc_beneficiario}")
	private String columnaNumDocBen;
	@Value("${columna.neg_beneficiario.porcentaje_giro}")
	private String columnaPorGiro;
	@Value("${columna.neg_beneficiario.porcentaje}")
	private String columnaPor;
	@Value("${tabla.neg_negocio}")
	private String tablaNegNegocio;
	@Value("${columna.neg_negocio.avaluo_inmueble}")
	private String columnaAvaInmueble;
	@Value("${columna.neg_negocio.valor_valorizacion}")
	private String valorValorizacion;
	@Value("${columna.neg_negocio.fec_cobro_valorizacion}")
	private String fecCobroValorizacion;
	@Value("${columna.neg_negocio.valor_predial}")
	private String valorPredial;
	@Value("${columna.neg_negocio.fec_cobro_predial}")
	private String fecCobroPredial;
	@Value("${tabla.neg_tipo_negocio}")
	private String tablaNegTipoNegocio;
	@Value("${columna.neg_tipo_negocio.tipo_negocio}")
	private String columnaTipoNegocio;
	@Value("${neg_negocio.estado.activo}")
	private String negocioActivo;
	@Value("${neg_negocio.estado.liquidado}")
	private String negocioLiquidado;
	@Value("${neg_negocio.estado.en_liquidacion}")
	private String negocioEnLiquidacion;
	
	@Value("${formato.ddMMyyyyhhmmss}")
	private String formatoddMMyyyyhhmmss;
	
	@Value("${columna.neg_negocio.valor.ultimo.predial}")
	private String valorUltimoPredial;
	
	
	
	
	@Autowired
	private Utilidades utilidades;

	@Autowired
	private RepositorioAudHistorial repositorioAudHistorial;

	@Autowired
	private RepositorioAudHistorialDetalle repositorioAudHistorialDetalle;

	@Autowired
	private IServicioNegocio servicioNegocio;

	@Autowired
	private RepositorioSolicitudCambioDatos repositorioSolicitudCambioDatos;

	@Autowired
	private IServicioUsuario servicioUsuario;
	
	@Autowired
	private RepositorioValorDominioNegocio repositorioValorDominioNegocio;
	
	@Autowired
	private IServicioDominio servicioDominio;

	/**
	 *los usuarios de la aplicacion
	 */
	@Override
	public List<DatosPersonaUsuarioDTO> obtenerUsuarios() {
		List<DatosPersonaUsuarioDTO> dto = new ArrayList<>();
		Iterator<Usuario> iterador = servicioUsuario.obtenerTodos().iterator();
		while (iterador.hasNext()) {
			Usuario usuario = iterador.next();
			dto.add(new DatosPersonaUsuarioDTO(usuario.getPersona().getNombreCompleto(),
					usuario.getPersona().getPersonaPK().getTipoDocumento(),
					usuario.getPersona().getPersonaPK().getNumeroDocumento(), usuario.getLogin()));
		}
		return dto;

	}

	// metodo que devuelve los negocios por maxima fecha
	/**
	 * @param historialCompleto
	 * @return el registro cuya fecha es mayor
	 */
	public List<AudHistorial> obtenerMaximasFechas(List<AudHistorial> historialCompleto) {
		HashMap<String, AudHistorial> maximasFechas = new HashMap<>();
		for (AudHistorial registro : historialCompleto) {
			String clave = registro.getAudHistorialPK().getCodSFC();
			if (!maximasFechas.containsKey(clave)) {
				maximasFechas.put(registro.getAudHistorialPK().getCodSFC(), registro);
			} else {
				// la fecha que esta almacenada es menor que la del array
				if (maximasFechas.get(clave).getAudHistorialPK().getFecha()
						.compareTo(registro.getAudHistorialPK().getFecha()) < 0) {
					maximasFechas.put(clave, registro);
				}
			}
		}
		return new ArrayList<>(maximasFechas.values());
	}

	/**
	 *Este metodo realiza consultas a la tabal audhistorial segun filtros
	 */
	@Override
	public List<AudHistorial> consultarConFiltros(ParametrosFiltroDTO dto, List<String> tabla, List<String> columnas,
			List<String> operaciones, String columnaOrdenar) {
		List<String> usuarios = null;
		if (!dto.getSolicitadoPor().isEmpty()) {
			usuarios = dto.getSolicitadoPor().stream()
					.map(persona -> persona.getTipoDocumento() + persona.getNumeroDocumento())
					.collect(Collectors.toList());
		}
		Specification<AudHistorial> especificacionCombinada = Specification
				.where(dto.getNegocios().isEmpty() ? null
						: AudHistorialEspecificacion.filtrarPorNegocios(dto.getNegocios()))
				.and((dto.getRangoFecha().isEmpty()) ? null
						: AudHistorialEspecificacion.filtrarPorFecha(
								utilidades.convertirFecha(dto.getRangoFecha().get(0), formatoddmmyyyy),
								utilidades.convertirFecha(dto.getRangoFecha().get(1) + mediaNoche, formatoddMMyyyyhhmmss)))
				.and(tabla.isEmpty() ? null : AudHistorialEspecificacion.filtrarPorTabla(tabla))
				.and(columnas == null || columnas.isEmpty() ? null
						: AudHistorialEspecificacion.filtrarPorColumna(columnas))
				.and(usuarios == null || usuarios.isEmpty() ? null
						: AudHistorialEspecificacion.filtrarPorTipoNumeroDocumento(usuarios))
				.and(operaciones == null || operaciones.isEmpty() ? null
						: AudHistorialEspecificacion.filtrarPorOperacion(operaciones));
		if(columnaOrdenar != null) {
			return repositorioAudHistorial.findAll(especificacionCombinada,Sort.by(Sort.Direction.DESC,columnaOrdenar));
		}
		return repositorioAudHistorial.findAll(especificacionCombinada);
	}

	/**
	 * realiza una consulta filtrada en la tabla de ben_solicitud de cambios
	 */
	@Override
	public List<SolicitudCambiosDatos> consultarConFiltrosBenSolicitudCambios(ParametrosFiltroDTO dto) {
		List<String> usuarios = dto.getSolicitadoPor().stream()
				.map(persona -> persona.getTipoDocumento() + persona.getNumeroDocumento()).collect(Collectors.toList());
		Specification<SolicitudCambiosDatos> especificacionCombinada = Specification
				.where(dto.getRangoFecha().isEmpty() ? null
						: SolicitudCambioEspecificacion.filtrarPorFechaInicial(
								utilidades.convertirFecha(dto.getRangoFecha().get(0), formatoddmmyyyy)))
				.and(dto.getRangoFecha().isEmpty() ? null
						: SolicitudCambioEspecificacion.filtrarPorFechaFinal(utilidades.convertirFecha(
								dto.getRangoFecha().get(dto.getRangoFecha().size() - 1) + mediaNoche, formatoddMMyyyyhhmmss)))
				.and(usuarios.isEmpty() ? null
						: SolicitudCambioEspecificacion.filtrarPorTipoNumeroDocumentoUsuario(usuarios));

		return repositorioSolicitudCambioDatos.findAll(especificacionCombinada);

	}

	/**
	 * este metodo consulta los beneficiarios activos segun la tabla de aud historial
	 * @param dto
	 * @param tabla
	 * @param columna
	 * @param valor
	 * @returna los beneficiarios activos de la aplicaicons
	 */
	private List<AudHistorial> consultarBeneficiariosActivos(ParametrosFiltroDTO dto, String tabla, String columna,
			String valor) {
		List<String> usuarios = dto.getSolicitadoPor().stream()
				.map(persona -> persona.getTipoDocumento() + persona.getNumeroDocumento()).collect(Collectors.toList());

		Specification<AudHistorial> especificacionCombinada = Specification
				.where(dto.getNegocios().isEmpty() ? null
						: AudHistorialEspecificacion.filtrarPorNegocios(dto.getNegocios()))
				.and((dto.getRangoFecha().isEmpty()) ? null
						: AudHistorialEspecificacion.filtrarPorFecha(
								utilidades.convertirFecha(dto.getRangoFecha().get(0), formatoddmmyyyy),
								utilidades.convertirFecha(dto.getRangoFecha().get(1) + mediaNoche, formatoddMMyyyyhhmmss)))
				.and(AudHistorialEspecificacion.filtrarPorCampoValor(columna, valor))
				.and(usuarios.isEmpty() ? null : AudHistorialEspecificacion.filtrarPorTipoNumeroDocumento(usuarios));
		return repositorioAudHistorial.findAll(especificacionCombinada);
	}

	/**
	 * @param total
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return retorna el compilado mensual y anual.
	 */
	public float[] calculoCompilados(float total, String fechaInicial, String fechaFinal) {

		float totalMes = total;
		float totalAno = total;

		Date inicio = utilidades.convertirFecha(fechaInicial, formatoddmmyyyy);
		Date fin = utilidades.convertirFecha(fechaFinal, formatoddmmyyyy);
		HashMap<String, Float> diferencias = utilidades.obtieneDiferenciaDeMesAno(inicio, fin);
		// si las diferencias son 0, se usa uno para evitar el error NAN
		if (diferencias.get(DIFERENCIA_MES) == 0) {
			diferencias.put(DIFERENCIA_MES, (float) 1);
			totalMes = 0;
		}
		if (diferencias.get(DIFERENCIA_ANO) == 0) {
			diferencias.put(DIFERENCIA_ANO, (float) 1);
			totalAno = 0;
		}
		float mensual = totalMes / diferencias.get(DIFERENCIA_MES);
		float anual = totalAno / diferencias.get(DIFERENCIA_ANO);
		mensual = (float) (Math.round(mensual * 100d) / 100d);
		anual = (float) (Math.round(anual * 100d) / 100d);
		return new float[] { mensual, anual };
	}

	/**
	 * Realiza el conteo de solicitud de cambios solo se puede filtrar por usuario
	 * que realiza el cambio y fechas de incio y fin
	 */
	@Override
	public RespuestaConteoDTO obtenerConteoSolicitiduesCambio(ParametrosFiltroDTO dto) {

		List<SolicitudCambiosDatos> historial = consultarConFiltrosBenSolicitudCambios(dto);
		int total = historial.size();
		RespuestaConteoDTO respuesta = new RespuestaConteoDTO();

		float[] compilados;
		String fechaMinima = obtenerFechaMinima(dto.getRangoFecha());
		String fechaMaxima = obtenerFechaMaxima(dto.getRangoFecha());

		compilados = calculoCompilados(total, fechaMinima, fechaMaxima);
		respuesta.setCompiladoTotal(total);
		respuesta.setCompiladoAnual(compilados[1]);
		respuesta.setCompiladoMensual(compilados[0]);
		return respuesta;

	}

	/**
	 * obtiene el conteo de cambios de distribucion, puede ser filtrado por fecha,
	 * negocuo, susuario y tipo
	 */
	@Override
	public RespuestaConteoDTO obtenerConteoCambiosDistribucion(ParametrosFiltroDTO dto) {

		List<AudHistorial> historialTitular = consultarConFiltros(dto, Arrays.asList(tablaNegTitular),
				Arrays.asList(columnaCotitular), null, null);
		historialTitular = verificarNegocioContieneTipo(historialTitular, dto.getTipoNegocio());
		int total = historialTitular.size();

		List<AudHistorial> historialRetencion = consultarConFiltros(dto, Arrays.asList(tablaNegRetTitular),
				Arrays.asList(idRetencion), null, null);
		historialRetencion = verificarNegocioContieneTipo(historialRetencion, dto.getTipoNegocio());
		int totalRetencion = historialRetencion.size();

		List<AudHistorial> historialNatJ = consultarConFiltros(dto, Arrays.asList(tablaNegTitular),
				Arrays.asList(idtipNatJur), null, null);
		historialNatJ = verificarNegocioContieneTipo(historialNatJ, dto.getTipoNegocio());
		int totalNatJ = historialNatJ.size();
		RespuestaConteoDTO respuesta = new RespuestaConteoDTO();

		String fechaMinima = obtenerFechaMinima(dto.getRangoFecha());
		String fechaMaxima = obtenerFechaMaxima(dto.getRangoFecha());

		float[] compiladosTitularidad = calculoCompilados(total, fechaMinima, fechaMaxima);
		respuesta.setTitularidadTotal(total);
		respuesta.setTitularidadAnual(compiladosTitularidad[1]);
		respuesta.setTitularidadMes(compiladosTitularidad[0]);

		float[] compiladosNaturaleza = calculoCompilados(totalNatJ, fechaMinima, fechaMaxima);

		respuesta.setTipoNaturalezaTotal(totalNatJ);
		respuesta.setTipoNaturalezAnual(compiladosNaturaleza[1]);
		respuesta.setTipoNaturalezaMes(compiladosNaturaleza[0]);

		float[] compiladosRetenciones = calculoCompilados(totalRetencion, fechaMinima, fechaMaxima);
		respuesta.setRetencionesTotal(totalRetencion);
		respuesta.setRetencionesAnual(compiladosRetenciones[1]);
		respuesta.setRetencionesMes(compiladosRetenciones[0]);

		return respuesta;

	}

	@Override
	public RespuestaConteoDTO obtenerConteoCambiosBeneficiarios(ParametrosFiltroDTO dto) {

		List<AudHistorial> numeroBeneficiarios = consultarBeneficiariosActivos(dto, tablaNegBen, columnaEstadoBen, "1");
		numeroBeneficiarios = verificarNegocioContieneTipo(numeroBeneficiarios, dto.getTipoNegocio());

		List<AudHistorial> cambiosBeneficiarios = consultarConFiltros(dto, Arrays.asList(tablaNegBen),
				Arrays.asList(columnaNumDocBen), null, null);
		cambiosBeneficiarios = verificarNegocioContieneTipo(cambiosBeneficiarios, dto.getTipoNegocio());

		List<AudHistorial> cambiosPorcentajeGiro = consultarConFiltros(dto, Arrays.asList(tablaNegBen),
				Arrays.asList(columnaPorGiro), null, null);
		cambiosPorcentajeGiro = verificarNegocioContieneTipo(cambiosPorcentajeGiro, dto.getTipoNegocio());

		List<AudHistorial> cambiosPorcentaje = consultarConFiltros(dto, Arrays.asList(tablaNegBen),
				Arrays.asList(columnaPor), null, null);
		cambiosPorcentaje = verificarNegocioContieneTipo(cambiosPorcentaje, dto.getTipoNegocio());

		List<AudHistorial> cambiosMedioPago = consultarConFiltros(dto, Arrays.asList(tablaNegMedPago), null, null, null);
		cambiosMedioPago = verificarNegocioContieneTipo(cambiosMedioPago, dto.getTipoNegocio());

		int totalBeneficiarios = numeroBeneficiarios.size();
		int totalCambioBeneficiarios = cambiosBeneficiarios.size();
		int totalCambioPorcentajeGiro = cambiosPorcentajeGiro.size();
		int totalCambioPorcentaje = cambiosPorcentaje.size();
		int totalCambioMediosPago = cambiosMedioPago.size();

		RespuestaConteoDTO respuesta = new RespuestaConteoDTO();

		String fechaMinima = obtenerFechaMinima(dto.getRangoFecha());
		String fechaMaxima = obtenerFechaMaxima(dto.getRangoFecha());

		respuesta.setNumeroBeneficiarios(totalBeneficiarios);

		float[] compilados = calculoCompilados(totalCambioBeneficiarios, fechaMinima, fechaMaxima);
		respuesta.setCambioBeneficiarios(totalCambioBeneficiarios);
		respuesta.setCambioBeneficiariosAnual(compilados[1]);
		respuesta.setCambioBeneficiariosMes(compilados[0]);

		compilados = calculoCompilados(totalCambioPorcentajeGiro, fechaMinima, fechaMaxima);
		respuesta.setPorcentajeGiro(totalCambioPorcentajeGiro);
		respuesta.setPorcentajeGiroAnual(compilados[1]);
		respuesta.setPorcentajeGiroMes(compilados[0]);

		compilados = calculoCompilados(totalCambioPorcentaje, fechaMinima, fechaMaxima);
		respuesta.setPorcentajeDist(totalCambioPorcentaje);
		respuesta.setPorcentajeDistAnual(compilados[1]);
		respuesta.setPorcentajeDistMes(compilados[0]);

		compilados = calculoCompilados(totalCambioMediosPago, fechaMinima, fechaMaxima);
		respuesta.setTipoPago(totalCambioMediosPago);
		respuesta.setTipoPagoAnual(compilados[1]);
		respuesta.setTipoPagoMes(compilados[0]);

		return respuesta;

	}

	@Override
	public RespuestaConteoDTO obtenerConteoCambiosCesiones(ParametrosFiltroDTO dto, String tokenAutorizacion)
			throws Exception {
		int totalPorcentaje = 0;
		int totalTitulares = 0;
		BigDecimal ceroBigDecimal = new BigDecimal("0.0");

		String fechaMinima = obtenerFechaMinima(dto.getRangoFecha());
		String fechaMaxima = obtenerFechaMaxima(dto.getRangoFecha());

		if (dto.getNegocios().isEmpty()) {
			dto.setNegocios(servicioNegocio.obtenerNegocios().stream().map(NegocioFiltroDTO::getCodigoSFC)
					.collect(Collectors.toList()));
		}

		for (String negocio : dto.getNegocios()) {

			RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] temp = servicioNegocio
					.buscarHistoricoCesionesSucesiones(negocio, fechaMinima.replace("/", "-"),
							fechaMaxima.replace("/", "-"), tokenAutorizacion);

			if (temp != null && verificarNegocioContieneTipo(negocio, fechaMaxima, dto.getTipoNegocio())) {

				for (RespuestaAccionConsultaCesionesSucesionesNegocioDTO registro : temp) {
					if (registro.getDiferenciaPorcentaje().compareTo(ceroBigDecimal) != 0) {
						totalPorcentaje++;
					}
					if (registro.getPropietarioNuevo() != null) {
						totalTitulares++;
					}

				}
			}
		}

		return obtenerRespuestaDeCambiosDeCesiones(totalPorcentaje, totalTitulares, fechaMinima, fechaMaxima);

	}

	/**
	 * @param historialCompleto
	 * @return dado un conjunto de registro de historiales devielve el mas reciente
	 *         por negocio
	 */
	public List<AudHistorial> obtenerMaximasFechasAudHistorial(List<AudHistorial> historialCompleto) {
		HashMap<String, AudHistorial> maximasFechas = new HashMap<>();
		for (AudHistorial registro : historialCompleto) {
			String clave = registro.getAudHistorialPK().getCodSFC();
			if (!maximasFechas.containsKey(clave)) {
				maximasFechas.put(registro.getAudHistorialPK().getCodSFC(), registro);

			} else {
				// la fecha que esta almacenada es menor que la del array
				if (maximasFechas.get(clave).getAudHistorialPK().getFecha()
						.compareTo(registro.getAudHistorialPK().getFecha()) < 0) {
					maximasFechas.put(clave, registro);
				}
			}
		}
		return new ArrayList<>(maximasFechas.values());
	}

	/**
	 * dado un conjunto de registro de historiales detalle devuelve el mas
	 *         reciente por negocio
	 * @param historialCompleto
	 * @return el registro de la historia mas reciente
	 */

	public List<AudHistorialDetalle> obtenerMaximasFechasAudHistorialDetalle(
			List<AudHistorialDetalle> historialCompleto) {
		HashMap<String, AudHistorialDetalle> maximasFechas = new HashMap<>();
		for (AudHistorialDetalle registro : historialCompleto) {
			String clave = registro.getAudHistoriaDetallePK().getCodSFC();
			if (!maximasFechas.containsKey(clave)) {
				maximasFechas.put(registro.getAudHistoriaDetallePK().getCodSFC(), registro);
			} else {
				// la fecha que esta almacenada es menor que la del array
				if (maximasFechas.get(clave).getAudHistoriaDetallePK().getFecha()
						.compareTo(registro.getAudHistoriaDetallePK().getFecha()) < 0) {
					maximasFechas.put(clave, registro);
				}
			}
		}
		return new ArrayList<>(maximasFechas.values());
	}

	/**
	 * @param dto, encyuentra los valore de avaluo y los suma puede ser filtrado por
	 *             fecha, negocios y condigo tipos de negocio
	 * @return
	 */
	public float calcularSumaDeValorAvaluo(ParametrosFiltroDTO dto) {
		List<AudHistorial> valoresAvaluo = consultarConFiltros(dto, Arrays.asList(tablaNegNegocio),
				Arrays.asList(columnaAvaInmueble), null, null);
		List<AudHistorial> filtroFechaMaxima = obtenerMaximasFechasAudHistorial(valoresAvaluo);
		float valorSumaAvaluo = 0;

		// Se valida si el filtro responsables negocio es diferente de nulo o vacio
		if (dto.getResponsablesNegocio() == null || dto.getResponsablesNegocio().isEmpty()) {
			valorSumaAvaluo = calcularValorSumaAvaluo(filtroFechaMaxima);
		} else {
			valorSumaAvaluo = calcularValorSumaAvaluo(
					filtrarListaHistorialAuditoria(filtroFechaMaxima, dto.getResponsablesNegocio()));
		}

		return valorSumaAvaluo;
	}
	
	/**
	 * Metodo que retorna la suma de avaluo sin filtro de Responsable Negocio
	 * 
	 * @param filtroFechaMaxima
	 * @return
	 */
	public float calcularValorSumaAvaluo(List<AudHistorial> filtroFechaMaxima) {
		float valorSumaAvaluo = 0;
		for (AudHistorial historial : filtroFechaMaxima) {
			// para cada negocio se buisca el detalle del valor del avaluo y se suma
			for (AudHistorialDetalle d : historial.getHistorialDetalles()) {
				if (d.getAudHistoriaDetallePK().getCampo().equals(columnaAvaInmueble)) {
					valorSumaAvaluo = valorSumaAvaluo + Float.parseFloat(d.getDatoNuevo());
				}
			}
		}
		return valorSumaAvaluo;
	}
	
	/**
	 * Método que filtra una lista de AudHistorial con la lista de responsables
	 * asociados a un negocio enviados en el filtro.
	 * 
	 * @author efarias
	 * @param listaHistorialAuditoria
	 * @param listaResponsables
	 * @return List<AudHistorial> -> lista filtrada.
	 */
	public List<AudHistorial> filtrarListaHistorialAuditoria(List<AudHistorial> listaHistorialAuditoria,
			List<String> listaResponsables) {
		List<AudHistorial> listaAudHistorial = new ArrayList<>();
		for (AudHistorial historial : listaHistorialAuditoria) {
			Negocio negocio = servicioNegocio.obtenerNegocio(historial.getAudHistorialPK().getCodSFC());

			/*
			 * Se realiza la búsqueda del negocio de acuerdo al código de negocio y se toman
			 * aquellos que son diferentes a nulo.
			 */
			if (negocio != null) {

				// Se filtra los responsables de negocio con los valores a filtrar.
				List<ResponsableNotificacion> listaFiltrada = negocio.getResponsablesNegocio().stream()
						.filter(responsable -> listaResponsables.stream().anyMatch(
								filtro -> responsable.getResponsablesNotificacionPK().getDocumento().equals(filtro)))
						.collect(Collectors.toList());

				// Si coinciden responsables de negocio, se agrega a la lista.
				if (!listaFiltrada.isEmpty()) {
					listaAudHistorial.add(historial);
				}
			}
		}
		return listaAudHistorial;
	}
	
	/**
	 * Método que filtra una lista de AudHistorialDetalle con la lista de
	 * responsables asociados a un negocio enviados en el filtro.
	 * 
	 * @author efarias
	 * @param listaHistorialDetalle
	 * @param listaResponsables
	 * @return
	 */
	public List<AudHistorialDetalle> filtrarListaHistorialAuditoriaDetalle(
			List<AudHistorialDetalle> listaHistorialDetalle, List<String> listaResponsables) {
		List<AudHistorialDetalle> listaAudHistorial = new ArrayList<>();
		for (AudHistorialDetalle detalle : listaHistorialDetalle) {
			AudHistorial historial = detalle.getAudHistorial();
			Negocio negocio = servicioNegocio.obtenerNegocio(historial.getAudHistorialPK().getCodSFC());
			
			/*
			 * Se realiza la búsqueda del negocio de acuerdo al código de negocio y se toman
			 * aquellos que son diferentes a nulo.
			 */
			if (negocio != null) {

				// Se filtra los responsables de negocio con los valores a filtrar.
				List<ResponsableNotificacion> listaFiltrada = negocio.getResponsablesNegocio().stream()
						.filter(responsable -> listaResponsables.stream().anyMatch(
								filtro -> responsable.getResponsablesNotificacionPK().getDocumento().equals(filtro)))
						.collect(Collectors.toList());

				// Si coinciden responsables de negocio, se agrega a la lista.
				if (!listaFiltrada.isEmpty()) {
					listaAudHistorial.add(detalle);
				}
			}
		}
		return listaAudHistorial;
	}

	/**
	 * @param dto
	 * @return retorna el numero de cambios en el campo Estado de la tabla de
	 *         mnegocio en un rango de fechas
	 */
	private int contarCambiosEstadoProyecto(ParametrosFiltroDTO dto) {

		List<AudHistorial> cambiosEstado = consultarConFiltros(dto, Arrays.asList(tablaNegNegocio),
				Arrays.asList(columnaEstadoNeg), Arrays.asList(UPDATE), null);
		
		// Se valida si el filtro responsables negocio es diferente de nulo o vacio
		if (dto.getResponsablesNegocio() == null || dto.getResponsablesNegocio().isEmpty()) {
			cambiosEstado = verificarNegocioContieneTipo(cambiosEstado, dto.getTipoNegocio());
		} else {
			cambiosEstado = verificarNegocioContieneTipo(
					filtrarListaHistorialAuditoria(cambiosEstado, dto.getResponsablesNegocio()), dto.getTipoNegocio());
		}

		return cambiosEstado.size();
	}

	@Override
	public RespuestaModificacionesValores obtenerDatosInformacionNegocio(ParametrosFiltroDTO dto) {

		RespuestaModificacionesValores valores = new RespuestaModificacionesValores();

		valores.setAvaluoInmuebles(calcularSumaDeValorAvaluo(dto));

		int cambiosEstado = contarCambiosEstadoProyecto(dto);
		valores.setCambiosProyecto(cambiosEstado);

		String fechaMinima = obtenerFechaMinima(dto.getRangoFecha());
		String fechaMaxima = obtenerFechaMaxima(dto.getRangoFecha());

		
		float[] compilados = calculoCompilados(cambiosEstado, fechaMinima, fechaMaxima);
		valores.setCambiosMensual(compilados[0]);
		valores.setCambiosAnual(compilados[1]);

		int activo = obtenerConteoDeEstados(dto, negocioActivo);
		valores.setProyectosActivos(activo);

		int enLiquidacion = obtenerConteoDeEstados(dto, negocioLiquidado);
		valores.setProyectosEnLiquidacion(enLiquidacion);

		valores.setProyectosLiquidados(obtenerConteoDeEstados(dto, negocioEnLiquidacion));

		float valorPredial = obtenerValorPredial(dto);

		compilados = calculoCompilados(valorPredial, fechaMinima, fechaMaxima);
		valores.setValorPrediales(valorPredial);
		valores.setValorPredialesMensual(compilados[0]);
		valores.setValorPredialesAnual(compilados[1]);

		float sumaValorizacion = obtenerValorValorizacion(dto);
		compilados = calculoCompilados(sumaValorizacion, fechaMinima, fechaMaxima);
		valores.setValorValorizacion(sumaValorizacion);
		valores.setValorValorizacionMensual(compilados[0]);
		valores.setValorValorizacionAnual(compilados[1]);

		return valores;
	}

	/**
	 * Para este metodo solo aplica el fltro de fecha final por que si no dejaria
	 * por fuera a losnegocios que han sido creados con anterioridad a la fecha de
	 * inicio, el metodo cuenta los tipos de negocio a la fecha.
	 */
	@Override
	public List<ConteoTipoNegocio> obtenerConteoDeTiposDeNegocio(ParametrosFiltroDTO dto, String token) {
		Specification<AudHistorialDetalle> especificacionCombinada = Specification
				.where(dto.getRangoFecha().isEmpty() ? null
						: AudHistorialDetalleEspecificacion.filtrarPorFechaFinal(
								utilidades.convertirFecha(dto.getRangoFecha().get(1), formatoddmmyyyy)))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreTabla(tablaNegTipoNegocio))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreCampo(columnaTipoNegocio))
				.and(AudHistorialDetalleEspecificacion.filtrarPorOperaciones(Arrays.asList(INSERT, UPDATE, DELETE)));

		List<AudHistorialDetalle> listaTipoNegocio = repositorioAudHistorialDetalle.findAll(especificacionCombinada);
		
		/*
		 * Si el filtro de responsables de negocio es diferente a nulo o vacio se
		 * realiza validación y filtrado de la información
		 */
		if (dto.getResponsablesNegocio() != null && !dto.getResponsablesNegocio().isEmpty()) {
			listaTipoNegocio = filtrarListaHistorialAuditoriaDetalle(listaTipoNegocio, dto.getResponsablesNegocio());
		}

		// se ordena la lista por fecha de menor a mayor para reconstruir el historico
		// de tipos de negocio

		listaTipoNegocio = listaTipoNegocio.stream().sorted(
				(a1, a2) -> a1.getAudHistoriaDetallePK().getFecha().compareTo(a2.getAudHistoriaDetallePK().getFecha()))
				.collect(Collectors.toList());
		// por cada uno de los negocios en la lista se obtiene sus tipos de negocio a
		HashMap<String, Integer> conteoAgregados = sumarTiposDelNegocio(listaTipoNegocio);

		List<ConteoTipoNegocio> tiposNegocio = new ArrayList<>();
		// se recorre la lista y se encuyentra la descripcion del tpo de negocio
		for (Map.Entry<String, Integer> clave : conteoAgregados.entrySet()) {
			String descripcion = repositorioValorDominioNegocio
					.findByDominioCodDominioAndIdValorDominio(dominioTipoNegocio, Integer.parseInt(clave.getKey()))
					.getDescripcion();
			tiposNegocio.add(new ConteoTipoNegocio(descripcion, clave.getValue()));
		}

		return tiposNegocio;

	}

	/**
	 * @param dto devuelve el conteo de estado en este se debe tener en cuenta fecha
	 *            de inicio y fin.
	 * @return
	 */
	public int obtenerConteoDeEstados(ParametrosFiltroDTO dto, String estado) {

		Specification<AudHistorialDetalle> especificacionCombinada = Specification
				.where(dto.getNegocios().isEmpty() ? null
						: AudHistorialDetalleEspecificacion.filtrarPorNegocios(dto.getNegocios()))
				.and(dto.getRangoFecha().isEmpty() ? null
						: AudHistorialDetalleEspecificacion.filtrarPorFechaInicial(
								utilidades.convertirFecha(dto.getRangoFecha().get(0), formatoddmmyyyy)))
				.and(dto.getRangoFecha().isEmpty() ? null
						: AudHistorialDetalleEspecificacion.filtrarPorFechaFinal(utilidades.convertirFecha(
								dto.getRangoFecha().get(dto.getRangoFecha().size() - 1), formatoddmmyyyy)))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreTabla(tablaNegNegocio))
				.and(AudHistorialDetalleEspecificacion.filtrarPorCampoValor(columnaEstadoNeg, estado));
		
		// Se obtiene el valor mas reciente del negocio
		List<AudHistorialDetalle> negocios = obtenerMaximasFechasAudHistorialDetalle(
				repositorioAudHistorialDetalle.findAll(especificacionCombinada));
		
		/*
		 * Si el filtro de responsables de negocio es diferente a nulo o vacio se
		 * realiza validación y filtrado de la información
		 */
		if (dto.getResponsablesNegocio() != null && !dto.getResponsablesNegocio().isEmpty()) {
			negocios = filtrarListaHistorialAuditoriaDetalle(negocios, dto.getResponsablesNegocio());
		}
		
		HashMap<String, List<String>> negociosConTipos = new HashMap<>();
		// se buscan los tipos de los negocios
		for (AudHistorialDetalle negocio : negocios) {

			HashMap<String, List<String>> temporal = obtenerTiposDelNegocio(null,
					negocio.getAudHistoriaDetallePK().getCodSFC(), negocio.getAudHistoriaDetallePK().getFecha());
			if (temporal != null && !temporal.isEmpty()) {

				negociosConTipos.put(negocio.getAudHistoriaDetallePK().getCodSFC(),
						temporal.get(negocio.getAudHistoriaDetallePK().getCodSFC()));
			}
		}
		// se aplica el filtro de tipo de negocio si existe, si un negocio tiene mas de
		// cuenta por 2 o mas dependidendo de los tipos, en esta seccion se hace el
		// filtro
		// de tipos si es que lo tiene

		return calcularSumaEstado(dto, negociosConTipos);
	}

	@Override
	public List<HistoricoCambioDTO> consultarHistoricoAuditoria(ParametrosFiltroDTO dto, String columna,
			String tipoHistorico, String tokenAutorizacion) {
		List<AudHistorial> historial = new ArrayList<>();
		if (tipoHistorico.equalsIgnoreCase("BENEFICIARIO")) {
			List<String> tablas = new ArrayList<>();
			tablas.add("NEG_BENEFICIARIO");
			tablas.add("NEG_TITULAR");
			historial.addAll(consultarConFiltros(dto, tablas, null, null, "audHistorialPK.fecha"));
		} else {
			List<String> campos = new ArrayList<>();
			campos.add("ESTADO");
			campos.add("AVALUO_INMUEBLE");
			campos.add("VALOR_ULTIMO_PREDIAL");
			campos.add("FECHA_COBRO_PREDIAL");
			campos.add("VALOR_VALORIZACION");
			campos.add("FECHA_COBRO_VALORIZACION");
			campos.add("TIPO_NEGOCIO");
			campos.add("NOMBRE_GASTO");
			campos.add("MOVIMIENTO_FONDO");
			campos.add("NUMERO_PAGO_ORDINARIO");
			campos.add("TIPO_FIDECOMISO");
			campos.add("TIPO_SUBTIPO_FIDECOMISO");
			campos.add("MOVIMIENTO");
			campos.add("TIPO_PROCESO");
			campos.add("TIP_MOV_NEGOCIO");
			campos.add("CODIGO_PAIS");
			campos.add("CODIGO_DEPARTAMENTO");
			campos.add("CODIGO_CIUDAD");
			List<String> tablas = new ArrayList<>();
			tablas.add("NEG_NEGOCIO");
			tablas.add("NEG_TIPO_NEGOCIO");
			tablas.add("NEG_OBLIGACION_NEGOCIO");
			tablas.add("NEG_ENCARGO");
			tablas.add("NEG_FECHA CLAVE");
			tablas.add("NEG_CONSTANTES_PAGO");
			tablas.add("NEG_RETENCION_TIPO_NEGOCIO");
			historial.addAll(consultarConFiltros(dto, tablas, campos, null,"audHistorialPK.fecha"));
		}

		List<HistoricoCambioDTO> listaHistoricos = new ArrayList<>();
		for (Iterator<AudHistorial> iterator = historial.iterator(); iterator.hasNext();) {
			AudHistorial auHistorial = iterator.next();
			auHistorial.getHistorialDetalles().forEach(detalle ->{
				HistoricoCambioDTO historico = auditoriaAHistoricoCambio(detalle, tokenAutorizacion);
				if(historico != null && !listaHistoricos.contains(historico)) {
					listaHistoricos.add(historico);
				}
			});
		}
		return listaHistoricos;
	}

	@Override
	public List<HistoricoCesionesCambioDTO> obtenerHistoricoCambiosCesiones(ParametrosFiltroDTO dto,
			String tokenAutorizacion) throws ErrorAlConectarAccionException {
		String fechaInicial = null;
		String fechaFinal = null;
		if (!dto.getRangoFecha().isEmpty()) {
			Date dateInicial = utilidades.convertirFecha(dto.getRangoFecha().get(0), formatoddmmyyyy);
			Date dateFinal = utilidades.convertirFecha(dto.getRangoFecha().get(1), formatoddmmyyyy);
			fechaInicial = dto.getRangoFecha().get(0) == null || dto.getRangoFecha().get(0).isEmpty() ? null
					: Utilidades.formatearFecha(dateInicial, "yyyy-mm-dd");
			fechaFinal = dto.getRangoFecha().get(1) == null || dto.getRangoFecha().get(1).isEmpty() ? null
					: Utilidades.formatearFecha(dateFinal, "yyyy-mm-dd");
		}
		List<HistoricoCesionesCambioDTO> listaHistoricos = new ArrayList<>();
		for (Iterator<String> iterator = dto.getNegocios().iterator(); iterator.hasNext();) {
			String negocio = iterator.next();
			RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] listaCambios = servicioNegocio
					.buscarHistoricoCesionesSucesiones(negocio, fechaInicial, fechaFinal, tokenAutorizacion);
			if (listaCambios != null)
				listaHistoricos.addAll(Arrays.asList(listaCambios).stream()
						.map(cambio -> historicoCesionesAHistoricoCambio(cambio, tokenAutorizacion))
						.collect(Collectors.toList()));
		}
		return listaHistoricos;
	}

	public HistoricoCambioDTO auditoriaAHistoricoCambio(AudHistorialDetalle auditoria, String tokenAutorizacion) {
		HistoricoCambioDTO historico = null;
		if(!Objects.equals(auditoria.getDatoAnterior(),auditoria.getDatoNuevo())) {
			historico = new HistoricoCambioDTO();
			String codigoSFC = auditoria.getAudHistorial().getAudHistorialPK().getCodSFC();
			historico.setCampoCambio(auditoria.getAudHistoriaDetallePK().getCampo().replace("_", " "));
			historico.setFechaCambio(
					Utilidades.formatearFecha(auditoria.getAudHistoriaDetallePK().getFecha(), "dd/MM/yyyy HH:mm:ss"));
			historico.setDireccionIp(auditoria.getAudHistorial().getIp());
			historico.setRegistradoPor(auditoria.getAudHistorial().getUsuario());
			historico.setDatoAnterior(homologarDato(auditoria.getDatoAnterior(), auditoria.getAudHistoriaDetallePK().getCampo()));
			historico.setDatoNuevo(homologarDato(auditoria.getDatoNuevo(), auditoria.getAudHistoriaDetallePK().getCampo()));
			historico.setCodigoNegocio(auditoria.getAudHistorial().getAudHistorialPK().getCodSFC());
			historico.setOperacion(auditoria.getAudHistoriaDetallePK().getOperacion());
			historico.setNombreNegocio(servicioNegocio.buscarNegocioPorCodigo(codigoSFC, tokenAutorizacion).getNombre());
		}
		return historico;
	}

	private String homologarDato(String dato, String campo) {
		if(dato != null) {
			if(campo.equalsIgnoreCase("ESTADO")) {
				dato = Estados.buscarEstado(dato) != null ? Estados.buscarEstado(dato).getHomologacion() : dato;
			} else if(campo.equalsIgnoreCase("BANCO") || campo.equalsIgnoreCase("TIPO_CUENTA")) {
				dato = servicioDominio.buscarValorDominio(Integer.valueOf(dato)).getDescripcion();
			}
		}
		return dato;
	}
	
	private HistoricoCesionesCambioDTO historicoCesionesAHistoricoCambio(
			RespuestaAccionConsultaCesionesSucesionesNegocioDTO cesion, String tokenAuthorization) {
		HistoricoCesionesCambioDTO historicoCesion = new HistoricoCesionesCambioDTO();
		historicoCesion.setCodigoNegocio(cesion.getIdNegocio());
		historicoCesion.setDiferenciaPorcentaje(cesion.getDiferenciaPorcentaje().toString());
		historicoCesion.setFechaCambio(cesion.getFechaRegistro());
		historicoCesion.setNombreNegocio(
				servicioNegocio.buscarNegocioPorCodigo(cesion.getIdNegocio(), tokenAuthorization).getNombre());
		historicoCesion.setNombrePropietarioAnterior(cesion.getPropietarioPrevio().getNombreCompleto());
		historicoCesion.setNombrePropietarioNuevo(cesion.getPropietarioNuevo().getNombreCompleto());
		historicoCesion.setPorcentajeActualCedente(cesion.getPorceActualCedente().toString());
		historicoCesion.setPorcentajeNuevoCedente(cesion.getPorceNuevoCedente().toString());
		return historicoCesion;
	}

	/**
	 * si no se pasa los tipos devuelve todos tipos del negocio, activos a la fecha
	 * si no se pasa la fecha devuelve todos los tipos del negocio activos hasta el
	 * ultimo que exita si no se pasa el negocio devuelve todos los tipos para todos
	 * 
	 **/
	public HashMap<String, List<String>> obtenerTiposDelNegocio(List<String> tipos, String negocioFiltrado,
			Date fechaMaxima) {
		Specification<AudHistorialDetalle> especificacionCombinada = Specification
				.where(fechaMaxima == null ? null : AudHistorialDetalleEspecificacion.filtrarPorFechaFinal(fechaMaxima))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreTabla("NEG_TIPO_NEGOCIO"))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreCampo("TIPO_NEGOCIO"))
				.and(AudHistorialDetalleEspecificacion.filtrarPorOperaciones(Arrays.asList(INSERT, UPDATE, DELETE)))
				.and(negocioFiltrado == null ? null
						: AudHistorialDetalleEspecificacion.filtrarPorCodSFC(negocioFiltrado))
				.and(tipos == null ? null : AudHistorialDetalleEspecificacion.filtrarPorTipoNegocio(tipos));

		List<AudHistorialDetalle> listaTipoNegocio = repositorioAudHistorialDetalle.findAll(especificacionCombinada);

		listaTipoNegocio = listaTipoNegocio.stream().sorted(
				(a1, a2) -> a1.getAudHistoriaDetallePK().getFecha().compareTo(a2.getAudHistoriaDetallePK().getFecha()))
				.collect(Collectors.toList());
		// por cada uno de los negocios en la lista se obtiene sus tipos de negocio a
		return reconstruirHistorialDeTiposDeNegocio(listaTipoNegocio);

	}

	// Devuelve los negocios que cumplen con el critero de topo de negocio,
	// consultando en la tabla
	// audHistorial
	public List<AudHistorial> verificarNegocioContieneTipo(List<AudHistorial> negocios, List<Integer> tipos) {

		List<AudHistorial> contieneElFiltro = new ArrayList<>();
		if (!tipos.isEmpty()) {
			HashMap<String, List<String>> negociosConTipos = new HashMap<>();
			// se buscan los tipos de los negocios activos
			for (AudHistorial negocio : negocios) {

				HashMap<String, List<String>> temporal = obtenerTiposDelNegocio(null,
						negocio.getAudHistorialPK().getCodSFC(), negocio.getAudHistorialPK().getFecha());

				negociosConTipos.put(negocio.getAudHistorialPK().getCodSFC(),
						temporal.get(negocio.getAudHistorialPK().getCodSFC()));

				boolean contieneElTipo = false;
				for (int tipoFiltro : tipos) {
					if (negociosConTipos.get(negocio.getAudHistorialPK().getCodSFC()) != null && negociosConTipos
							.get(negocio.getAudHistorialPK().getCodSFC()).contains(String.valueOf(tipoFiltro))) {
						contieneElTipo = true;
						break;

					}
				}
				if (contieneElTipo) {
					contieneElFiltro.add(negocio);
				}
			}

		} else {
			contieneElFiltro = negocios;
		}
		return contieneElFiltro;
	}

	/**
	 * En caso de que no se ingrese un rango de fecha, el metodo devuelve la fecha
	 * minima en el historico de auditoria
	 * 
	 * @param rangoFecha
	 * @return
	 */
	public String obtenerFechaMinima(List<String> rangoFecha) {
		String fechaMinima = "";
		if (!rangoFecha.isEmpty()) {
			fechaMinima = rangoFecha.get(0);

		} else {
			fechaMinima = Utilidades.formatearFecha(
					repositorioAudHistorial.findFirstByOrderByAudHistorialPKFechaAsc().getAudHistorialPK().getFecha(),
					formatoddmmyyyy);

		}
		return fechaMinima;
	}

	/**
	 * En caso de que no se ingrese un rango de fecha, el metodo devuelve la fecha
	 * maxima en el historico de auditoria
	 * 
	 * @param rangoFecha
	 * @return
	 */
	public String obtenerFechaMaxima(List<String> rangoFecha) {
		String fechaMaxima = "";
		if (!rangoFecha.isEmpty()) {
			fechaMaxima = rangoFecha.get(1);

		} else {

			fechaMaxima = Utilidades.formatearFecha(
					repositorioAudHistorial.findFirstByOrderByAudHistorialPKFechaDesc().getAudHistorialPK().getFecha(),
					formatoddmmyyyy);

		}
		return fechaMaxima;
	}

	public float obtenerValorPredial(ParametrosFiltroDTO dto) {
		// Se consultan las fechas de cobro del predial

		List<AudHistorialDetalle> fechasDePago = consultarAudHistorialDetalle(dto, Arrays.asList(INSERT, UPDATE),
				tablaNegNegocio, fecCobroPredial);
		/*
		 * Si el filtro de responsables de negocio es diferente a nulo o vacio se
		 * realiza validación y filtrado de la información
		 */
		if (dto.getResponsablesNegocio() != null && !dto.getResponsablesNegocio().isEmpty()) {
			fechasDePago = filtrarListaHistorialAuditoriaDetalle(fechasDePago, dto.getResponsablesNegocio());
		}
		
		// aqui se filtro por tipo
		fechasDePago = verificarNegocioContieneTipoAudHistorialDetalle(fechasDePago, dto.getTipoNegocio());
		float valorPagado = 0;

		// por cada fecha de cobro se haya el valor del predual en ese momento
		for (AudHistorialDetalle fechaDePago : fechasDePago) {

			List<AudHistorialDetalle> pago = obtenerUltimoPagoSegunFechaCobro(
					utilidades.convertirFecha(
							fechaDePago.getDatoNuevo(),formatoddMMyyyyhhmmss), fechaDePago.getAudHistoriaDetallePK().getCodSFC(),
					tablaNegNegocio, valorUltimoPredial);
			if (!pago.isEmpty() ) {
				valorPagado += Float.parseFloat(pago.get(0).getDatoNuevo() == null ?  "0" :pago.get(0).getDatoNuevo() );
			}
		}

		return valorPagado;

	}

	/**
	 * obtiene el valor de la valorizacion
	 * @param dto
	 * @return float con el valor de la valorizacion
	 */
	private float obtenerValorValorizacion(ParametrosFiltroDTO dto) {
		// Se consultan las fechas de cobro del predial

		List<AudHistorialDetalle> fechasDePago = consultarAudHistorialDetalle(dto, Arrays.asList(INSERT, UPDATE),
				tablaNegNegocio, fecCobroValorizacion);
		
		/*
		 * Si el filtro de responsables de negocio es diferente a nulo o vacio se
		 * realiza validación y filtrado de la información
		 */
		if (dto.getResponsablesNegocio() != null && !dto.getResponsablesNegocio().isEmpty()) {
			fechasDePago = filtrarListaHistorialAuditoriaDetalle(fechasDePago, dto.getResponsablesNegocio());
		}
		
		// aqui se filtro por tipo si es lo que tiene si no devuelve la misma lista
		fechasDePago = verificarNegocioContieneTipoAudHistorialDetalle(fechasDePago, dto.getTipoNegocio());

		float valorPagado = 0;

		// por cada fecha de cobro se haya el valor del la valorizacion en ese momento
		for (AudHistorialDetalle fechaDePago : fechasDePago) {

			List<AudHistorialDetalle> pago = obtenerUltimoPagoSegunFechaCobro(
					utilidades.convertirFecha(
							fechaDePago.getDatoNuevo(),formatoddMMyyyyhhmmss), fechaDePago.getAudHistoriaDetallePK().getCodSFC(),
					tablaNegNegocio, valorValorizacion);

			if (!pago.isEmpty()) {
				valorPagado += Float.parseFloat(pago.get(0).getDatoNuevo());
			}
		}

		return valorPagado;
	}


	/**
	 *obtiene el ultimo pago realizado segun una fecha de cobro en especifico
	 */
	@Override
	public List<AudHistorialDetalle> obtenerUltimoPagoSegunFechaCobro(Date fechaMaxima, String negocioFiltrado,
			String tabla, String columna) {

		Specification<AudHistorialDetalle> especificacionCombinada = Specification
				.where(fechaMaxima == null ? null : AudHistorialDetalleEspecificacion.filtrarPorFechaFinal(fechaMaxima))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreTabla(tabla))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreCampo(columna))
				.and(AudHistorialDetalleEspecificacion.filtrarPorOperaciones(Arrays.asList(INSERT, UPDATE)))
				.and(negocioFiltrado == null ? null
						: AudHistorialDetalleEspecificacion.filtrarPorCodSFC(negocioFiltrado));

		List<AudHistorialDetalle> valores = repositorioAudHistorialDetalle.findAll(especificacionCombinada);
		// se obtiene el ultimo valor cobrado
		valores = obtenerMaximasFechasAudHistorialDetalle(valores);
		return valores;

	}

	/**
	 * hace operaciones en la tabla AudHistorialDetalle
	 * @param dto
	 * @param operaciones
	 * @param tabla
	 * @param campo
	 * @return List<AudHistorialDetalle>
	 */
	public List<AudHistorialDetalle> consultarAudHistorialDetalle(ParametrosFiltroDTO dto, List<String> operaciones,
			String tabla, String campo) {
		Specification<AudHistorialDetalle> especificacionCombinada = Specification
				.where(dto.getNegocios()
						.isEmpty() ? null
								: AudHistorialDetalleEspecificacion.filtrarPorNegocios(dto.getNegocios()))
				.and(dto.getRangoFecha().isEmpty() ? null
						: AudHistorialDetalleEspecificacion.filtrarPorFechaFinal(utilidades.convertirFecha(
								dto.getRangoFecha().get(1) + mediaNoche,formatoddMMyyyyhhmmss)))
				.and(AudHistorialDetalleEspecificacion.filtrarPorOperaciones(operaciones))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreTabla(tabla))
				.and(AudHistorialDetalleEspecificacion.filtrarPorNombreCampo(campo));

		return repositorioAudHistorialDetalle.findAll(especificacionCombinada);

	}

	/**
	 * Dada una lista de negocios que viene de la entidad AudHitorialDetalle, devuelve aquellos que contienen
	 * alguno de los tipos de negocio que se pasan como parametro
	 * @param negocios
	 * @param tipos
	 * @return
	 */
	public List<AudHistorialDetalle> verificarNegocioContieneTipoAudHistorialDetalle(List<AudHistorialDetalle> negocios,
			List<Integer> tipos) {

		List<AudHistorialDetalle> contieneElFiltro = new ArrayList<>();

		if (!tipos.isEmpty()) {
			HashMap<String, List<String>> negociosConTipos = new HashMap<>();
			// se buscan los tipos de los negocios activos
			for (AudHistorialDetalle negocio : negocios) {

				HashMap<String, List<String>> temporal = obtenerTiposDelNegocio(null,
						negocio.getAudHistoriaDetallePK().getCodSFC(), negocio.getAudHistoriaDetallePK().getFecha());

				negociosConTipos.put(negocio.getAudHistoriaDetallePK().getCodSFC(),
						temporal.get(negocio.getAudHistoriaDetallePK().getCodSFC()));

				boolean contieneElTipo = false;
				for (int tipoFiltro : tipos) {
					if (negociosConTipos.get(negocio.getAudHistoriaDetallePK().getCodSFC()) != null && negociosConTipos
							.get(negocio.getAudHistoriaDetallePK().getCodSFC()).contains(String.valueOf(tipoFiltro))) {
						contieneElTipo = true;
						break;

					}
				}
				if (contieneElTipo) {
					contieneElFiltro.add(negocio);
				}
			}

		} else {
			contieneElFiltro = negocios;
		}
		return contieneElFiltro;
	}

	/**
	 * devuelve un booleano para indicar si un negocio contiene un tipo en una fecha dada 
	 * @param negocio
	 * @param fecha
	 * @param tipos
	 * @return booleano
	 */
	public boolean verificarNegocioContieneTipo(String negocio, String fecha, List<Integer> tipos) {

		boolean contieneElFiltro = false;

		if (!tipos.isEmpty()) {

			HashMap<String, List<String>> temporal = obtenerTiposDelNegocio(null, negocio,
					utilidades.convertirFecha(fecha, formatoddmmyyguion));

			boolean contieneElTipo = false;
			for (int tipoFiltro : tipos) {
				if (temporal.get(negocio) != null && temporal.get(negocio).contains(String.valueOf(tipoFiltro))) {

					contieneElTipo = true;
					break;

				}
			}
			if (contieneElTipo) {
				contieneElFiltro = true;
			}
		} else {
			contieneElFiltro = true;
		}
		return contieneElFiltro;
	}

	/**
	 * Devuelve el dto de respuesta para cambios de cesiones
	 * @param totalPorcentaje
	 * @param totalTitulares
	 * @param fechaMinima
	 * @param fechaMaxima
	 * @return respuestaconteoDTO
	 */
	public RespuestaConteoDTO obtenerRespuestaDeCambiosDeCesiones(int totalPorcentaje, int totalTitulares,
			String fechaMinima, String fechaMaxima) {
		RespuestaConteoDTO respuesta = new RespuestaConteoDTO();

		float[] compilados = calculoCompilados(totalPorcentaje, fechaMinima, fechaMaxima);
		respuesta.setCambioTotalPorcentajes(totalPorcentaje);
		respuesta.setCambioAnualPorcentajes(compilados[1]);
		respuesta.setCambioMensualPorcentajes(compilados[0]);
		compilados = calculoCompilados(totalTitulares, fechaMinima, fechaMaxima);
		respuesta.setCambioTotalTitulares(totalTitulares);
		respuesta.setCambioAnualTitulares(compilados[1]);
		respuesta.setCambioMensualTitulares(compilados[0]);
		return respuesta;

	}

	private int calcularSumaEstado(ParametrosFiltroDTO dto, HashMap<String, List<String>> negociosConTipos) {
		int sumaEstado = 0;
		if (!dto.getTipoNegocio().isEmpty()) {
			for (int tipoFiltro : dto.getTipoNegocio()) {
				for (Map.Entry<String, List<String>> negocio : negociosConTipos.entrySet()) {
					if (negocio.getValue().contains(String.valueOf(tipoFiltro))) {
						sumaEstado += 1;
					}
				}
			}
		} else {
			// si no tiene filtro de tipo devuelve todos los
			// negocios por la cantidad de tipos que se encontro
			for (Map.Entry<String, List<String>> negocio : negociosConTipos.entrySet()) {
				sumaEstado = sumaEstado + negocio.getValue().size();
			}
		}
		return sumaEstado;
	}

	/**
	 * Devuelve un hashmap con clave nombre de negocio y valor una lista de sus tipos
	 * @param listaTipoNegocio de la entidad AudHistorialDetalle
	 * @return controlTiposNegocio 
	 */
	private HashMap<String, List<String>> reconstruirHistorialDeTiposDeNegocio(
			List<AudHistorialDetalle> listaTipoNegocio) {
		HashMap<String, List<String>> controlTiposNegocio = new HashMap<>();
		for (AudHistorialDetalle item : listaTipoNegocio) {
			//
			String negocio = item.getAudHistoriaDetallePK().getCodSFC();
			String datoAnterior = item.getDatoAnterior();

			String datoNuevo = item.getDatoNuevo();// el tipo de proyecto

			if (!controlTiposNegocio.containsKey(negocio)) {
				controlTiposNegocio.put(negocio, new ArrayList<String>());
			}

			if (item.getAudHistoriaDetallePK().getOperacion().equals(INSERT)) {
				if (controlTiposNegocio.containsKey(negocio)) {
					controlTiposNegocio.get(negocio).add(datoNuevo);
				}

			} else if (item.getAudHistoriaDetallePK().getOperacion().equals(UPDATE)) {
				if (controlTiposNegocio.containsKey(negocio)
						&& controlTiposNegocio.get(negocio).contains(datoAnterior)) {
					controlTiposNegocio.get(negocio).remove(datoAnterior);
					controlTiposNegocio.get(negocio).add(datoNuevo);
				} else {
					controlTiposNegocio.get(negocio).add(datoNuevo);
				}

			} else if (item.getAudHistoriaDetallePK().getOperacion().equals(DELETE)
					&& controlTiposNegocio.containsKey(negocio)) {
				controlTiposNegocio.get(negocio).remove(datoAnterior);

			}

		}
		return controlTiposNegocio;
	}

	/**
	 * este metodo devuelve un hashmap de tipo de negocio y el numero de veces qie aparecio
	 * @param listaTipoNegocio de la clase AuditoriaHistorioalDetalle
	 * @return conteoAgregados
	 */
	private HashMap<String, Integer> sumarTiposDelNegocio(List<AudHistorialDetalle> listaTipoNegocio) {
		HashMap<String, Integer> conteoAgregados = new HashMap<>();
		for (AudHistorialDetalle item : listaTipoNegocio) {
			// dato nuevo corresponde al codigo de negocio
			String datoAnterior = item.getDatoAnterior();// el tipo de proyecto

			String datoNuevo = item.getDatoNuevo();// el tipo de proyecto

			if (item.getAudHistoriaDetallePK().getOperacion().equals(INSERT)) {
				if (conteoAgregados.containsKey(datoNuevo) && datoNuevo != null) {
					conteoAgregados.put(datoNuevo, conteoAgregados.get(datoNuevo) + 1);
				} else {
					conteoAgregados.put(datoNuevo, 1);

				}

			} else if (item.getAudHistoriaDetallePK().getOperacion().equals(UPDATE)) {
				if (conteoAgregados.containsKey(datoAnterior)) {
					conteoAgregados.put(datoAnterior, conteoAgregados.get(datoAnterior) - 1);
				} else {
					if (conteoAgregados.containsKey(datoNuevo)) {
						conteoAgregados.put(datoNuevo, conteoAgregados.get(datoNuevo) + 1);
					} else {
						conteoAgregados.put(datoNuevo, 1);

					}

				}

			} else if (item.getAudHistoriaDetallePK().getOperacion().equals(DELETE)
					&& conteoAgregados.containsKey(datoAnterior)) {
				conteoAgregados.put(datoAnterior, conteoAgregados.get(datoAnterior) - 1);

			}

		}
		return conteoAgregados;
	}
}
