/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.ResponsablesNegocioDTO;
import com.accionfiduciaria.modelo.entidad.ResponsableNotificacion;
import com.accionfiduciaria.negocio.repositorios.RepositorioResponsablesNotificacion;

/**
 * @author efarias
 *
 */
@Service
@Transactional
public class ServicioResponsablesNotificacion implements IServicioResponsablesNotificacion {

	@Autowired
	private RepositorioResponsablesNotificacion repositorioResponsablesNotificacion;

	/**
	 * Método que guarda el responsable asociado a un negocio;
	 * 
	 * @author efarias
	 * @param responsablesNotificacion
	 */
	@Override
	public void guardarResponsable(ResponsableNotificacion responsablesNotificacion) {
		repositorioResponsablesNotificacion.save(responsablesNotificacion);
	}

	/**
	 * Método que obtiene todos los responsables de todos los negocios en estado
	 * activo.
	 * 
	 * @author efarias
	 * @return Responsables asociados a un negocio en estado activo.
	 */
	@Override
	public List<ResponsablesNegocioDTO> obtenerResponsablesNegocioActivos() {
		List<ResponsablesNegocioDTO> listaResponsablesDTO = new ArrayList<>();
		List<ResponsableNotificacion> listaResponsables = repositorioResponsablesNotificacion
				.obtenerResponsablesPorEstado(Boolean.TRUE);

		if (!listaResponsables.isEmpty()) {
			listaResponsables.forEach(responsable -> listaResponsablesDTO.add(entidadADTO(responsable)));
		}
		return listaResponsablesDTO;
	}

	/**
	 * Método que transforma un objeto ResponsableNotificacion a un objeto
	 * ResponsablesNegocioDTO.
	 * 
	 * @author efarias
	 * @param responsable
	 * @return ResponsablesNegocioDTO
	 */
	public ResponsablesNegocioDTO entidadADTO(ResponsableNotificacion responsable) {
		return new ResponsablesNegocioDTO(responsable.getNombre(),
				responsable.getResponsablesNotificacionPK().getDocumento(),
				responsable.getResponsablesNotificacionPK().getTipoDocumento());
	}

}
