package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioObligacionNegocio;

@Service
public class ServicioObligacionNegocio implements IServicioObligacionNegocio {

	@Autowired
	RepositorioObligacionNegocio repositorioObligacionNegocio;
	
	@Override
	public List<ObligacionNegocio> buscarObligacionesNegocio(Negocio negocio) {
		return repositorioObligacionNegocio.findByTipoNegocioNegocioAndActivo(negocio, Boolean.TRUE);
	}

	@Override
	public List<ObligacionNegocio> buscarObligacionesTipoNegocio(TipoNegocio tipoNegocio) {
		return repositorioObligacionNegocio.findByTipoNegocioAndActivo(tipoNegocio, Boolean.TRUE);
	}

	@Override
	public ObligacionNegocio guardarObligacionNegocio(ObligacionNegocio obligacion) {
		return repositorioObligacionNegocio.save(obligacion);
	}

	@Override
	public List<ObligacionNegocio> buscarObligacionesNegocioPorIdTipoNegocio(Integer idTipoNegocio) {
		return repositorioObligacionNegocio.findByObligacionNegocioPK_tipoNegocioAndActivo(idTipoNegocio, Boolean.TRUE);
	}

	@Override
	public List<ObligacionNegocio> buscarObligacionesNegocioPorIdTipoGasto(Integer idTipoGasto) {
		return repositorioObligacionNegocio.findByObligacionNegocioPK_tipoPagoAndActivo(idTipoGasto, Boolean.TRUE);
	}

}
