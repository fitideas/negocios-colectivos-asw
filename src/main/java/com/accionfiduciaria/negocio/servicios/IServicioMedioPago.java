package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.datosFinancierosBeneficiarioDTO;
import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MedioPagoPK;

public interface IServicioMedioPago {

	@Autowired
	public MedioPago guardarMedioPago(MedioPago medioPago);

	@Autowired
	public boolean existeMedioPago(MedioPagoPK medioPagoPK);

	@Autowired
	public MedioPago buscarMedioPagoPorID(MedioPagoPK medioPagoPK);

	@Autowired
	public MedioPago verificarCrearMedioPago(datosFinancierosBeneficiarioDTO datoNuevo);
}
