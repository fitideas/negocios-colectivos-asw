package com.accionfiduciaria.negocio.servicios;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.accionfiduciaria.modelo.dtos.TipoFideicomisoDTO;

public interface IServicioTipoFideicomiso {

	@Autowired
	public List<TipoFideicomisoDTO> obtenerTipoFideicomiso();

	@Autowired
	public List<TipoFideicomisoDTO> obtenerSubTipoFideicomiso(String tipoFideicomiso);
	
}
