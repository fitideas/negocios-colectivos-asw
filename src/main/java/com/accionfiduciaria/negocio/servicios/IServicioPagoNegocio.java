/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.IngresosTipoOperacionDTO;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;
import com.accionfiduciaria.modelo.entidad.PagoNegocioPK;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

/**
 * @author efarias
 *
 */
public interface IServicioPagoNegocio {
	
	@Autowired
	public List<PagoNegocio> obtenerListaIdsPagoNegocio(List<TipoNegocio> listaTiposNegocio, String periodo);
	
	@Autowired
	public boolean existePagoNegocio(PagoNegocioPK pagoNegocioPK);
	
	@Autowired
	public PagoNegocio buscarPagoNegocio(PagoNegocioPK pagoNegocioPK);
	
	@Autowired
	public PagoNegocio guardarPagoNegocio(PagoNegocio pagoNegocio);
	
	@Autowired
	public PagoNegocio obtenerPagoNegocio(IngresosTipoOperacionDTO ingresosTipoOperacionDTO, String codigoNegocio, String periodo);

}
