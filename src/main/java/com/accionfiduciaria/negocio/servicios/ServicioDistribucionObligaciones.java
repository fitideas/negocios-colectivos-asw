/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.ConceptoDTO;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionObligacion;
import com.accionfiduciaria.modelo.entidad.DistribucionObligacionPK;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucionObligaciones;

/**
 * @author efarias
 *
 */
@Service
@Transactional
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:propiedades.properties")
public class ServicioDistribucionObligaciones implements IServicioDistribucionObligaciones {

	@Autowired
	private RepositorioDistribucionObligaciones repositorioDistribucionObligaciones;

	@Autowired
	private Utilidades utilidades;

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	// Constantes
	private static final String UNIDAD_PORCENTAJE = "%";
	private static final String UNIDAD_COP = "COP";
	private static final Double CIEN = (double) 100;

	@Override
	public void guardarDistribucionObligaciones(Distribucion distribucion, List<ObligacionNegocio> listaObligaciones,
			BigDecimal totalIngresos, String periodo) {
		List<DistribucionObligacion> listaDistribucionObligaciones = new ArrayList<>();

		if (!listaObligaciones.isEmpty()) {
			listaObligaciones.forEach(obligacion -> {
				
				// Construir PK
				DistribucionObligacionPK distribucionObligacionPK = new DistribucionObligacionPK(
						distribucion.getDistribucionPK(), obligacion.getObligacionNegocioPK());
				
				// Construir Objeto
				DistribucionObligacion distribucionObligacion = new DistribucionObligacion(distribucionObligacionPK,
						obligacion.getUnidad(), obligacion.getValor(),
						calcularValorPagoObligacion(obligacion, totalIngresos));
				
				// Se agrega el objeto a la lista
				listaDistribucionObligaciones.add(distribucionObligacion);
			});
			repositorioDistribucionObligaciones.saveAll(listaDistribucionObligaciones);
		}
	}

	@Override
	public BigDecimal calcularValorPagoObligacion(ObligacionNegocio obligacion, BigDecimal totalIngresos) {

		BigDecimal valor = BigDecimal.ZERO;

		if (obligacion.getUnidad().equals(UNIDAD_COP)) {
			valor = obligacion.getValor();
		} else if (obligacion.getUnidad().equals(UNIDAD_PORCENTAJE)) {
			valor = totalIngresos.multiply(obligacion.getValor().divide(BigDecimal.valueOf(CIEN)));
		}
		return valor;
	}

	/**
	 * Este método convierte una lista listaObligaciones a una lista de ConceptoDTO.
	 * Adicional calcula el valor cuando la unidad es "Porcentaje" sobre el valor
	 * total de ingresos, en caso contrario, si el unidad es "COP" asigna el valor
	 * ingresado
	 * 
	 * @author efarias
	 * 
	 * @param periodo
	 * @param totalIngresos
	 * @param listaObligaciones
	 * @return List<ConceptoDTO>
	 */
	@Override
	public List<ConceptoDTO> obtenerListaConceptosObligacionesDTO(List<ObligacionNegocio> listaObligaciones,
			BigDecimal totalIngresos, String periodo) {

		List<ConceptoDTO> obligacionesDTO = new ArrayList<>();

		if (!listaObligaciones.isEmpty()) {
			listaObligaciones.forEach(obligacion -> {

				// Creación de la fecha con datos del dia de pago y periodo.
				int dia = obligacion.getDiaPago() == null ? 15 : obligacion.getDiaPago();
				int mes = utilidades.obtenerMesPeriodoInteger(periodo);
				int year = utilidades.obtenerAnioPeriodoInteger(periodo);
				Date fecha = utilidades.construirFecha(year, mes, dia);

				ConceptoDTO conceptoDTO = new ConceptoDTO();
				conceptoDTO.setConcepto(obligacion.getObligacionNegocioPK().getNombreGasto());
				conceptoDTO.setFecha(Utilidades.formatearFecha(fecha, formatoddmmyyyy));

				if (obligacion.getUnidad().equals(UNIDAD_COP)) {
					conceptoDTO.setValor(obligacion.getValor());
				} else if (obligacion.getUnidad().equals(UNIDAD_PORCENTAJE)) {
					BigDecimal valor = totalIngresos
							.multiply(obligacion.getValor().divide(BigDecimal.valueOf(CIEN)));
					conceptoDTO.setValor(valor);
				}
				obligacionesDTO.add(conceptoDTO);
			});
			return obligacionesDTO;
		} else {
			return obligacionesDTO;
		}
	}
}
