/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.ConceptoDTO;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;

/**
 * @author efarias
 *
 */
public interface IServicioDistribucionObligaciones {

	@Autowired
	public void guardarDistribucionObligaciones(Distribucion distribucion, List<ObligacionNegocio> listaObligaciones,
			BigDecimal totalIngresos, String periodo);
	
	@Autowired
	public List<ConceptoDTO> obtenerListaConceptosObligacionesDTO(List<ObligacionNegocio> listaObligaciones,
			BigDecimal totalIngresos, String periodo);
	
	@Autowired
	public BigDecimal calcularValorPagoObligacion(ObligacionNegocio obligacion, BigDecimal totalIngresos);

}
