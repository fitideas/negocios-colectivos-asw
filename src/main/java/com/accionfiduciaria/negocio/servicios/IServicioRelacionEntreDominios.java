package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.accionfiduciaria.modelo.entidad.RelacionEntreDominios;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;

/**
 * Servicio que maneja la entidad {@link RelacionEntreDominios}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioRelacionEntreDominios {
	
	/**
	 * Verifica si las retenciones enviadas ya estan asignadas a un valor. 
	 * En caso contrario las agrega a los valores que usan retención.
	 * 
	 * @param retenciones Las retenciones a agregar.
	 */
	@Autowired
	public void agregarRelacionRetencion(List<ValorDominioNegocio> retenciones);
	
	/**
	 * Elimina las asociacios de una valor de dominio.
	 * 
	 * @param idValorDominio El id del valor de negocio al que se le eliminaran las relaciones.
	 */
	@Autowired
	public void eliminarRelacionDominio(Integer idValorDominio);

	/**
	 * Busca todos los registros asociados a un valor de dominio.
	 * 
	 * @param idValorDominio El id del valor de dominio a consultar.
	 * @return Una lista con los registros relacionados al valor de dominio enviado.
	 */
	@Autowired
	public List<RelacionEntreDominios> buscarRelacionesValorDominio(Integer idValorDominio);

	/**
	 * Guarda un registro en la tabla.
	 * 
	 * @param relacion Entidad con los datos del regsitro a guardar.
	 */
	@Autowired
	public void guardarRelacionDominio(RelacionEntreDominios relacion);
	
	/**
	 * Busca todos los registros que le apliquen a un valor de dominio (valor=true) 
	 * 
	 * @param idValorDominio El id del valor de dominio a consultar.
	 * @return Una lista con los registros que le aplican al valor de dominio enviado.
	 */
	@Autowired
	public List<RelacionEntreDominios> buscarValoresAplicanRelacion(Integer idValorDominio);
	
	/**
	 * Busca todos los registros que le apliquen con los criterios de búsqueda dados
	 * 
	 * @param idValorDominio
	 * @param codDominio
	 * @return Lista relacionEntreDominios con los criterios de búsqueda dados.
	 */
	@Autowired
	public List<RelacionEntreDominios> buscarRelacionDominioPorIdValorDominioYCodigoDominio(Integer idValorDominio,
			String codDominio);
}
