package com.accionfiduciaria.negocio.servicios;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.FechaClavePK;
import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioFechaClave;

@Service
public class ServicioFechaClave implements IServicioFechaClave {

	@Autowired
	private RepositorioFechaClave repositorioFechaClave;
	
	@Autowired
	private Utilidades utilidades;
	
	@Override
	public List<FechaClave> buscarFechasClaveNegocio(Negocio negocio) {
		return repositorioFechaClave.findByNegocioAndActivo(negocio, Boolean.TRUE);
	}

	@Override
	public FechaClave guardarFechaClave(FechaClave fechaActual) {
		return repositorioFechaClave.save(fechaActual);
	}

	@Override
	public FechaClave guardarFechaClave(Date fecha, String codigoNegocio, boolean activo) {
		FechaClave fechaClave = new FechaClave();
		FechaClavePK fechaClavePK = new FechaClavePK();
		fechaClavePK.setCodSfc(codigoNegocio);
		fechaClavePK.setFecha(fecha);
		fechaClave.setFechaClavePK(fechaClavePK);
		fechaClave.setFechaDia(utilidades.obtieneDiaDeLaSemana(fecha));
		fechaClave.setFechaMes(utilidades.obtieneDiaDelMes(fecha));
		fechaClave.setActivo(activo);
		return guardarFechaClave(fechaClave);
	}

	@Override
	public List<FechaClave> buscarPorIdFechaClave(Integer idEliminar) {
		return repositorioFechaClave.findByTipoAndActivo(idEliminar, Boolean.TRUE);
	}

	@Override
	public List<FechaClave> buscarProximasFechasClaves(Date fechaActual) {
		return repositorioFechaClave.findByFechaClavePK_fechaGreaterThanEqualAndActivo(fechaActual, Boolean.TRUE);
	}

}
