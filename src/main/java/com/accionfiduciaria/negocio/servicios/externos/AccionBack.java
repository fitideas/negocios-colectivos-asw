/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.externos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.accionfiduciaria.modelo.dtos.NovedadesJuridicasAccionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionConsultaExactaDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionPaginada;
import com.accionfiduciaria.modelo.dtos.RepuestaConsultaNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionBuscarEncargoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionConsultaCesionesSucesionesNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionEquipoResponsableDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionObtenerNovedadesJuridicasDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaBusquedadVinculadoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaNegociosVinculadoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaVinculadosDTO;

/**
 * Servicio que implementa la interfaz {@link IAccionBack}.
 * 
 * @author José Santiago Polo Acosta - 05/12/2019 - jpolo@asesoftware.com
 *
 */
@Service
@PropertySource("classpath:propiedades.properties")
public class AccionBack implements IAccionBack {

	@Autowired
	ResponseErrorHandler responseErrorHandler;

	@Autowired
	RestTemplate restTemplate;

	@Value("${param.name.limit}")
	private String paramLimit;
	@Value("${param.name.start}")
	private String paramStart;
	@Value("${param.name.exact.match}")
	private String paramExactMatch;
	@Value("${param.name.nombre.cliente}")
	private String paramNombreCliente;
	@Value("${param.name.identificacion}")
	private String paramIdentificacion;
	@Value("${header.title.authorization}")
	private String headerTitleAuthorization;
	@Value("${header.value.bearer}")
	private String headerValueBearer;
	@Value("${param.name.sfc}")
	private String paramSFC;
	@Value("${param.name.nombre.negocio}")
	private String paramNombreNegocio;
	@Value("${param.name.codigo.negocio}")
	private String paramCodigoNegocio;
	@Value("${param.name.documento}")
	private String paramDocumento;
	@Value("${param.name.codigo.novedad}")
	private String paramCodigoNovedad;
	@Value("${param.name.nrodocumento}")
	private String paramNroDocumento;
	@Value("${param.name.startdate}")
	private String paramStartDate;
	@Value("${param.name.endate}")
	private String paramEndDate;

	@Override
	public RespuestaPaginadaConsultaVinculadosDTO buscarBeneficiario(
			PeticionAccionPaginada peticionAccionBusquedadBeneficiarioDTO) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization,
				headerValueBearer + " " + peticionAccionBusquedadBeneficiarioDTO.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(peticionAccionBusquedadBeneficiarioDTO.getUrlServicioAccion())
				.queryParam(paramStart, peticionAccionBusquedadBeneficiarioDTO.getStart())
				.queryParam(paramExactMatch, peticionAccionBusquedadBeneficiarioDTO.isExactMatch())
				.queryParam(
						peticionAccionBusquedadBeneficiarioDTO.getNombre().isEmpty() ? paramIdentificacion
								: paramNombreCliente,
						peticionAccionBusquedadBeneficiarioDTO.getNombre().isEmpty()
								? peticionAccionBusquedadBeneficiarioDTO.getIdentificacion()
								: peticionAccionBusquedadBeneficiarioDTO.getNombre());
		if (peticionAccionBusquedadBeneficiarioDTO.getLimit() != null) {
			uriBuilder.queryParam(paramLimit, peticionAccionBusquedadBeneficiarioDTO.getLimit());
		}

		ResponseEntity<RespuestaPaginadaConsultaVinculadosDTO> response = restTemplate.exchange(
				uriBuilder.toUriString(), HttpMethod.GET, request, RespuestaPaginadaConsultaVinculadosDTO.class);
		return response.getBody();
	}

	@Override
	public RespuestaBusquedadVinculadoDTO[] buscarBeneficiarioPorDocumento(
			PeticionAccionConsultaExactaDTO peticionAccionConsultaExactaDTO) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + peticionAccionConsultaExactaDTO.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(peticionAccionConsultaExactaDTO.getUrl())
				.queryParam(paramExactMatch, Boolean.TRUE)
				.queryParam(paramIdentificacion, peticionAccionConsultaExactaDTO.getDocumento());

		ResponseEntity<RespuestaBusquedadVinculadoDTO[]> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaBusquedadVinculadoDTO[].class);
		return response.getBody();
	}

	@Override
	public RespuestaPaginadaConsultaNegociosVinculadoDTO buscarNegociosBeneficiario(
			PeticionAccionPaginada peticionBusquedaNegociosBeneficiario) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization,
				headerValueBearer + " " + peticionBusquedaNegociosBeneficiario.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(peticionBusquedaNegociosBeneficiario.getUrlServicioAccion())
				.queryParam(paramLimit, peticionBusquedaNegociosBeneficiario.getLimit())
				.queryParam(paramStart, peticionBusquedaNegociosBeneficiario.getStart());

		ResponseEntity<RespuestaPaginadaConsultaNegociosVinculadoDTO> response = restTemplate.exchange(
				uriBuilder.toUriString(), HttpMethod.GET, request, RespuestaPaginadaConsultaNegociosVinculadoDTO.class);
		return response.getBody();
	}

	@Override
	public RespuestaAccionBuscarEncargoDTO[] validarEncargoCliente(String urlValidarEncargo, String documento,String tokenAutorizacion) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + tokenAutorizacion);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(urlValidarEncargo)
				.queryParam(paramNroDocumento, documento);

		ResponseEntity<RespuestaAccionBuscarEncargoDTO[]> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaAccionBuscarEncargoDTO[].class);
		return response.getBody();
	}

	@Override
	public RespuestaPaginadaConsultaNegocioDTO buscarNegocio(PeticionAccionPaginada peticionBusquedaNegocio) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + peticionBusquedaNegocio.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(peticionBusquedaNegocio.getUrlServicioAccion())
				.queryParam(paramStart, peticionBusquedaNegocio.getStart())
				.queryParam(paramExactMatch, peticionBusquedaNegocio.isExactMatch())
				.queryParam(
						peticionBusquedaNegocio.getNombre().isEmpty() ? paramCodigoNegocio
								: paramNombreNegocio,
								peticionBusquedaNegocio.getNombre().isEmpty()
								? peticionBusquedaNegocio.getIdentificacion()
								: peticionBusquedaNegocio.getNombre());
		if (peticionBusquedaNegocio.getLimit() != null) {
			uriBuilder.queryParam(paramLimit, peticionBusquedaNegocio.getLimit());
		}
		
		ResponseEntity<RespuestaPaginadaConsultaNegocioDTO> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaPaginadaConsultaNegocioDTO.class);
		return response.getBody();
	}
	
	@Override
	public RepuestaConsultaNegocioDTO[] buscarNegocioPorCodigo(String codigoNegocio, String tokenAutorizacion, String urlServicio) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + tokenAutorizacion);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(urlServicio)
				.queryParam(paramCodigoNegocio, codigoNegocio)
				.queryParam(paramExactMatch, Boolean.TRUE);

		ResponseEntity<RepuestaConsultaNegocioDTO[]> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RepuestaConsultaNegocioDTO[].class);
		return response.getBody();
	}

	@Override
	public RespuestaAccionBuscarEncargoDTO[] buscarEncargosNegocio(String codigoNegocio, String tokenAutorizacion, String urlServicio) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + tokenAutorizacion);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(urlServicio)
				.queryParam(paramCodigoNegocio, codigoNegocio);

		ResponseEntity<RespuestaAccionBuscarEncargoDTO[]> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaAccionBuscarEncargoDTO[].class);
		return response.getBody();
	}
	
	@Override
	public RespuestaAccionObtenerNovedadesJuridicasDTO obtenerNovedadesJuridicas(PeticionAccionPaginada peticionPaginadaNovedadesJuridicas) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + peticionPaginadaNovedadesJuridicas.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(peticionPaginadaNovedadesJuridicas.getUrlServicioAccion())
				.queryParam(paramStart, peticionPaginadaNovedadesJuridicas.getStart());
		
		if (peticionPaginadaNovedadesJuridicas.getLimit() != null) {
			uriBuilder.queryParam(paramLimit, peticionPaginadaNovedadesJuridicas.getLimit());
		}

		ResponseEntity<RespuestaAccionObtenerNovedadesJuridicasDTO> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaAccionObtenerNovedadesJuridicasDTO.class);
		return response.getBody();
	}

	@Override
	public NovedadesJuridicasAccionDTO obtenerDetalleNovedadesJuridicas(
			PeticionAccionPaginada peticionBusquedaNovedadesJuridicas) {
		HttpHeaders headers = new HttpHeaders();
		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + peticionBusquedaNovedadesJuridicas.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(peticionBusquedaNovedadesJuridicas.getUrlServicioAccion());

		ResponseEntity<NovedadesJuridicasAccionDTO> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, NovedadesJuridicasAccionDTO.class);
		return response.getBody();
	}

	@Override
	public RespuestaBusquedadVinculadoDTO[] buscarBeneficiariosNegocio(String urlServicio, String tokenAutorizacion) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + tokenAutorizacion);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(urlServicio);
		
		ResponseEntity<RespuestaBusquedadVinculadoDTO[]> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaBusquedadVinculadoDTO[].class);
		return response.getBody();
	}

	@Override
	public RespuestaAccionEquipoResponsableDTO[] buscarEquipoResponsable(String urlServicio, String tokenAutorizacion) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + tokenAutorizacion);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(urlServicio);

		ResponseEntity<RespuestaAccionEquipoResponsableDTO[]> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaAccionEquipoResponsableDTO[].class);
		return response.getBody();
	}

	@Override
	public RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] buscarHistoricoCesionesSucesiones(String urlServicio,
			String codigoNegocio, String fechaInicio, String fechaFin, String tokenAutorizacion) {
		HttpHeaders headers = new HttpHeaders();

		restTemplate.setErrorHandler(responseErrorHandler);
		headers.set(headerTitleAuthorization, headerValueBearer + " " + tokenAutorizacion);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(urlServicio)
				.queryParam(paramCodigoNegocio, codigoNegocio);
		if(fechaInicio != null && !fechaInicio.isEmpty() && fechaFin != null && !fechaFin.isEmpty()) {
			uriBuilder.queryParam(paramStartDate, fechaInicio);
			uriBuilder.queryParam(paramEndDate, fechaFin);
		}

		ResponseEntity<RespuestaAccionConsultaCesionesSucesionesNegocioDTO[]> response = restTemplate.exchange(uriBuilder.toUriString(),
				HttpMethod.GET, request, RespuestaAccionConsultaCesionesSucesionesNegocioDTO[].class);
		return response.getBody();
	}

}
