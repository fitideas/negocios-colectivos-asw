/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.externos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.dtos.PeticionAccionAutenticacionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionIntrospeccionToken;
import com.accionfiduciaria.modelo.dtos.RespuestaAcccionValidarToken;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionAutenticacionDTO;

/**
 * @author José Santiago Polo Acosta - 13/12/2019 - jpolo@asesoftware.com
 *
 */

@Service
public interface IAutenticacion {
		
	/**
	 * 
	 * @param peticionAutenticacionDTO
	 * @return
	 */
	@Autowired
	public RespuestaAccionAutenticacionDTO autenticarUsuario(PeticionAccionAutenticacionDTO peticionAutenticacionDTO);

	/**
	 * @param peticionAccionInvalidarToken
	 * @return
	 */
	@Autowired
	public String invalidarToken(PeticionAccionIntrospeccionToken peticionAccionInvalidarToken);
	
	@Autowired
	public RespuestaAcccionValidarToken validarToken(PeticionAccionIntrospeccionToken peticionAccionValidarToken);
}
