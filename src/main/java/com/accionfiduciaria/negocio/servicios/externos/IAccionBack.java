/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.externos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.dtos.NovedadesJuridicasAccionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionConsultaExactaDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionPaginada;
import com.accionfiduciaria.modelo.dtos.RepuestaConsultaNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionBuscarEncargoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionConsultaCesionesSucesionesNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionEquipoResponsableDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionObtenerNovedadesJuridicasDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaBusquedadVinculadoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaNegociosVinculadoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaVinculadosDTO;

/**
 * Clase encargada de realizar el consumo de los servicios que ofrece el sistema AccionBack de Acción Fiduciaria.
 * 
 * @author José Santiago Polo Acosta - jpolo@asesoftware.com
 *
 */
@Service
public interface IAccionBack {

	/**
	 * Consulta los datos de los cientes.
	 * 
	 * @param peticionBusquedadBeneficiarioDTO DTO con los parámetros de la consulta.
	 * @return Un DTO con la respuesta de la consulta.
	 */
	@Autowired
	public RespuestaPaginadaConsultaVinculadosDTO buscarBeneficiario(
			PeticionAccionPaginada peticionBusquedadBeneficiarioDTO);

	/**
	 * Consulta el listado de negocios de un beneficiario.
	 * 
	 * @param peticionBusquedaNegociosBeneficiario DTO con los parámetros de la consulta.
	 * @return Un DTO con la respuesta de la consulta.
	 */
	@Autowired
	public RespuestaPaginadaConsultaNegociosVinculadoDTO buscarNegociosBeneficiario(
			PeticionAccionPaginada peticionBusquedaNegociosBeneficiario);

	@Autowired
	public RespuestaAccionBuscarEncargoDTO[] validarEncargoCliente(String obtenerUrlValidarEncargo, String documento, String tokenAutorizacion);

	@Autowired
	public RespuestaPaginadaConsultaNegocioDTO buscarNegocio(PeticionAccionPaginada generarPeticionBusquedaNegocio);

	@Autowired
	public RepuestaConsultaNegocioDTO[] buscarNegocioPorCodigo(String codigoNegocio, String tokenAutorizacion, String urlServicio);

	@Autowired
	public RespuestaAccionBuscarEncargoDTO[] buscarEncargosNegocio(String codigoNegocio, String tokenAutorizacion, String urlServicio);

	@Autowired
	public RespuestaBusquedadVinculadoDTO[] buscarBeneficiarioPorDocumento(PeticionAccionConsultaExactaDTO peticionAccionConsultaExactaDTO);
	
	@Autowired
	public RespuestaAccionObtenerNovedadesJuridicasDTO obtenerNovedadesJuridicas(PeticionAccionPaginada peticionPaginadaNovedadesJuridicas);

    @Autowired
	public NovedadesJuridicasAccionDTO obtenerDetalleNovedadesJuridicas(
			PeticionAccionPaginada peticionBusquedaNovedadesJuridicas);
    
    @Autowired
	public RespuestaBusquedadVinculadoDTO[] buscarBeneficiariosNegocio(String urlServicio, String tokenAutorizacion);

    @Autowired
	public RespuestaAccionEquipoResponsableDTO[] buscarEquipoResponsable(String urlServicio, String tokenAutorizacion);
    
    @Autowired
	public RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] buscarHistoricoCesionesSucesiones(String urlServicio, String codigoNegocio, String fechaInicio, String fechaFin, String tokenAutorizacion);
}
