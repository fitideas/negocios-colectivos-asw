/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.externos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import com.accionfiduciaria.modelo.dtos.PeticionAccionAutenticacionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionIntrospeccionToken;
import com.accionfiduciaria.modelo.dtos.RespuestaAcccionValidarToken;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionAutenticacionDTO;

/**
 * @author José Santiago Polo Acosta - 10/12/2019 - jpolo@asesoftware.com
 *
 */
@Service
@PropertySource("classpath:propiedades.properties")
public class Autenticacion implements IAutenticacion {

	@Autowired
	private ResponseErrorHandler responseErrorHandler;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${param.name.grant.type}")
	private String paramNameGrantType;
	@Value("${param.value.grant.type}")
	private String paramValueGrantType;
	@Value("${param.name.scope}")
	private String paramNameScope;
	@Value("${param.value.scope}")
	private String paramValueScope;
	@Value("${param.name.username}")
	private String paramNameUserName;
	@Value("${param.name.password}")
	private String paramNamePassword;
	@Value("${param.name.appkey}")
	private String paramNameAppKey;
	@Value("${header.title.authorization}")
	private String headerTitleAuthorization;
	@Value("${param.name.token}")
	private String paramNameToken;
	
	
	@Override
	public RespuestaAccionAutenticacionDTO autenticarUsuario(PeticionAccionAutenticacionDTO peticionAutenticacionDTO) {
		HttpHeaders headers = new HttpHeaders();
		
		restTemplate.setErrorHandler(responseErrorHandler);
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);		
		headers.set(headerTitleAuthorization, peticionAutenticacionDTO.getAuthorization());		
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add(paramNameGrantType, paramValueGrantType);
		map.add(paramNameScope, paramValueScope);
		map.add(paramNameUserName, peticionAutenticacionDTO.getUserName());
		map.add(paramNamePassword, peticionAutenticacionDTO.getPassword());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
		
		return restTemplate.exchange(peticionAutenticacionDTO.getUrlServidorAutenticacion(), HttpMethod.POST,request , RespuestaAccionAutenticacionDTO.class ).getBody();
	}


	/**
	 * 
	 * @param peticionAccionInvalidarToken
	 * @return
	 */
	@Override
	public String invalidarToken(PeticionAccionIntrospeccionToken peticionAccionInvalidarToken) {
		HttpHeaders headers = new HttpHeaders();
		
		restTemplate.setErrorHandler(responseErrorHandler);
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);		
		headers.set(headerTitleAuthorization, peticionAccionInvalidarToken.getAuthorization());		
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add(paramNameToken, peticionAccionInvalidarToken.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
		
		return restTemplate.exchange(peticionAccionInvalidarToken.getUrlServicio(), HttpMethod.POST,request , String.class ).getBody();
	}
	
	@Override
	public RespuestaAcccionValidarToken validarToken(PeticionAccionIntrospeccionToken peticionAccionValidarToken) {
		HttpHeaders headers = new HttpHeaders();
		
		restTemplate.setErrorHandler(responseErrorHandler);
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);		
		headers.set(headerTitleAuthorization, peticionAccionValidarToken.getAuthorization());		
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add(paramNameToken, peticionAccionValidarToken.getToken());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
		
		return restTemplate.exchange(peticionAccionValidarToken.getUrlServicio(), HttpMethod.POST,request , RespuestaAcccionValidarToken.class ).getBody();
	}
}
