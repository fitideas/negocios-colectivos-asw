package com.accionfiduciaria.negocio.servicios;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.accionfiduciaria.enumeradores.EstadosDistribucion;
import com.accionfiduciaria.enumeradores.TipoAsistente;
import com.accionfiduciaria.enumeradores.TipoDesicion;
import com.accionfiduciaria.enumeradores.TipoExtensionArchivo;
import com.accionfiduciaria.enumeradores.TipoReunion;
import com.accionfiduciaria.enumeradores.TiposArchivo;
import com.accionfiduciaria.excepciones.ArchivoNoValidoException;
import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.ErrorAlConectarAccionException;
import com.accionfiduciaria.excepciones.ErrorAlmacenandoAsambleas;
import com.accionfiduciaria.excepciones.ErrorProcesandoArchivo;
import com.accionfiduciaria.excepciones.NoContentException;
import com.accionfiduciaria.excepciones.ServiceNotFoundException;
import com.accionfiduciaria.modelo.dtos.AsambleasDTO;
import com.accionfiduciaria.modelo.dtos.AsistenteDTO;
import com.accionfiduciaria.modelo.dtos.BeneficiarioTipoPagoDTO;
import com.accionfiduciaria.modelo.dtos.CambioDatosTitularesNegocioDTO;
import com.accionfiduciaria.modelo.dtos.ComiteDTO;
import com.accionfiduciaria.modelo.dtos.ConstantesPagoDTO;
import com.accionfiduciaria.modelo.dtos.DatosBasicosPersonaDTO;
import com.accionfiduciaria.modelo.dtos.DatosBasicosValorDominioDTO;
import com.accionfiduciaria.modelo.dtos.DatosEquipoResponsableDTO;
import com.accionfiduciaria.modelo.dtos.DecisionComiteDTO;
import com.accionfiduciaria.modelo.dtos.DecisionDTO;
import com.accionfiduciaria.modelo.dtos.DetalleDatosCambiadosDTO;
import com.accionfiduciaria.modelo.dtos.EncargoDTO;
import com.accionfiduciaria.modelo.dtos.EncargoNegocioDTO;
import com.accionfiduciaria.modelo.dtos.FechaClaveDTO;
import com.accionfiduciaria.modelo.dtos.GastoDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoCambioDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoReunionDTO;
import com.accionfiduciaria.modelo.dtos.InformacionFinancieraRecepcionDTO;
import com.accionfiduciaria.modelo.dtos.InformacionTitularesIncompletaDTO;
import com.accionfiduciaria.modelo.dtos.ListaNegociosDTO;
import com.accionfiduciaria.modelo.dtos.NegocioCompletoDTO;
import com.accionfiduciaria.modelo.dtos.NegocioDTO;
import com.accionfiduciaria.modelo.dtos.NegocioFiltroDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionPaginada;
import com.accionfiduciaria.modelo.dtos.PeticionAsambleasDTO;
import com.accionfiduciaria.modelo.dtos.PeticionHistorialReunionesDTO;
import com.accionfiduciaria.modelo.dtos.PeticionInformacionGeneralPagosDTO;
import com.accionfiduciaria.modelo.dtos.RepuestaConsultaNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionBuscarEncargoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionConsultaCesionesSucesionesNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionEquipoResponsableDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaBusquedadVinculadoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaDetalleBeneficiarioPagoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoCambiosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoReunionesDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaSincronizacionDatosNegocioDTO;
import com.accionfiduciaria.modelo.dtos.ResultadoSincronizacionTitularesYEncargosDTO;
import com.accionfiduciaria.modelo.dtos.TerceroDTO;
import com.accionfiduciaria.modelo.dtos.TipoDocumentoDTO;
import com.accionfiduciaria.modelo.dtos.TipoNegocioCompletoDTO;
import com.accionfiduciaria.modelo.dtos.TitularPagoDto;
import com.accionfiduciaria.modelo.dtos.ValorDominioSimpleDTO;
import com.accionfiduciaria.modelo.entidad.Asistente;
import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.ConstantesPago;
import com.accionfiduciaria.modelo.entidad.Decision;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.Encargo;
import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.InformacionGeneralPagosDTO;
import com.accionfiduciaria.modelo.entidad.IngresoOperacion;
import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MiembroMesaDirectiva;
import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.modelo.entidad.PagoDistribuidoDTO;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;
import com.accionfiduciaria.modelo.entidad.ParticipacionesNegocio;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.PersonaPK;
import com.accionfiduciaria.modelo.entidad.RelacionEntreDominios;
import com.accionfiduciaria.modelo.entidad.ResponsableNotificacion;
import com.accionfiduciaria.modelo.entidad.ResponsableNotificacionPK;
import com.accionfiduciaria.modelo.entidad.RetencionTipoNegocio;
import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.Reunion;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.TitularPK;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.especificaciones.PagoNegocioEspecificaciones;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucion;
import com.accionfiduciaria.negocio.repositorios.RepositorioNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioPagoNegocio;
import com.accionfiduciaria.negocio.servicios.externos.IAccionBack;

/**
 * Case que implementa la interfaz {@link IServicioNegocio}
 * 
 * @author JosÃ© Polo - jpolo@asesoftware.com
 *
 */
@Service
@Transactional
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:propiedades.properties")
public class ServicioNegocio implements IServicioNegocio {

	private Logger logger = LogManager.getLogger(ServicioNegocio.class);
	
	@Autowired
	RepositorioNegocio repositorioNegocio;

	@Autowired
	private IServicioParametroSistema servicioParametroSistema;

	@Autowired
	private IAccionBack accionBack;

	@Autowired
	private IServicioReunion servicioReunion;

	@Autowired
	private IServicioArchivo servicioArchivo;

	@Autowired
	private IServicioVinculados servicioVinculados;

	@Autowired
	private IServicioPersona servicioPersona;

	@Autowired
	private IServicioTitular servicioTitular;

	@Autowired
	private IServicioParticipacionNegocio servicioParticipacion;

	@Autowired
	private IServicioBeneficiario servicioBeneficiario;

	@Autowired
	private IServicioMedioPago servicioMedioPago;

	@Autowired
	private IServicioConstantesPago servicioConstantesPago;

	@Autowired
	private Utilidades utilidades;

	@Autowired
	private IServicioDominio servicioDominio;

	@Autowired
	private IServicioObligacionNegocio servicioObligacionNegocio;

	@Autowired
	private IServicioTipoNegocio servicioTipoNegocio;

	@Autowired
	private IServicioFechaClave servicioFechaClave;

	@Autowired
	private IServicioEncargo servicioEncargo;

	@Autowired
	private IServicioRetencionTipoNegocio servicioRetencionTipoNegocio;

	@Autowired
	private RepositorioPagoNegocio repositorioPagoNegocio;

	@Autowired
	private RepositorioDistribucion repositorioDistribucon;
	
	@Autowired
	private IServicioAudHistorial servicioAuditoria;
	
	@Autowired
	private IServicioRelacionEntreDominios servicioRelacionEntreDominios;
	
	@Autowired
	private IServicioRetencionTitular servicioRetencionTitular;
	
	@Autowired
	private IServicioResponsablesNotificacion servicioResponsablesNotificacion;

	@Value("${prop.nombre}")
	private String propNombre;

	@Value("${prop.codigo}")
	private String propCodigo;
	
	@Value("${cod.dominio.rol.tercero}")
	private String codDominioRolTercero;

	@Value("${param.id.url.servicio.accion.buscar.negocio}")
	private Integer paramIdUrlServicioAccionBusquedadNegocio;

	@Value("${param.id.url.servicio.accion.buscar.encargos.negocio}")
	private Integer paramIdUrlServicioAccionBusquedadEncargosNegocio;

	@Value("${error.negocio.codigononumerico}")
	private String codigoNegocoNoValido;

	@Value("${error.participacion.no.valido}")
	private String porcetanjeParticipacionNoValido;
	
	@Value("${error.participacion.giro.no.valido}")
	private String porcetanjeParticipacionGiroNoValido;

	@Value("${error.titular.tipo.documento.novalido}")
	private String titularTipoDocNoValido;

	@Value("${error.titular.numero.doocumento.novalido}")
	private String titularNumeroDocNoValido;

	@Value("${error.beneficiario.tipo.documento.novalido}")
	private String beneficiarioTipoDocNoValido;

	@Value("${error.beneficiario.numero.documento.novalido}")
	private String beneficiarioNumeroDocNoValido;

	@Value("${error.tipo.banco.nopermitido}")
	private String bancoTipoNoValido;

	@Value("${error.tipo.cuenta.novalido}")
	private String tipoCuentaNoValida;

	@Value("${error.tipo.pago.novalido}")
	private String tipoPagoNoValido;

	@Value("${error.numero.cuenta.novalido}")
	private String numeroCuentaNoValida;

	@Value("${error.titular.no.encontrado}")
	private String titularNoEncontrado;

	@Value("${error.numero.columnas.novalido}")
	private String numeroColumnasNoValido;

	@Value("${error.porcentaje.giro.noesvalido}")
	private String porcentajeGiroNoValido;

	@Value("${error.conexion.fiduciaria}")
	private String errorConexionFiduciaaria;

	@Value("${param.id.minutos.sincronizacion.bus}")
	private Integer paramIdMinutosSincronizacionBus;

	@Value("${param.id.url.servicio.accion.buscar.beneficiarios.negocio}")
	private Integer paramIdUrlServicioAccionBuscarBeneficiariosNegocio;

	@Value("${param.id.activar.sincronizacion.bus}")
	private Integer paramIdActivarSincronizacion;

	@Value("${param.id.url.servicio.accion.buscar.equipo.negocio}")
	private Integer paramIdUrlServicioAccionBusquedadEquipoNegocioNegocio;
	
	@Value("${param.id.url.servicio.accion.buscar.cesiones.negocio}")
	private Integer paramIdUrlServicioAccionCesiones;

	@Value("${msg.error.suma.porcentajes.titulares}")
	private String msgErrorSumaPorcentajesTitulares;

	@Value("${msg.error.informacion.financiera.incompleta}")
	private String msgErrorInformacionFinancieraIncompleta;

	@Value("${msg.error.constantes.sin.valor}")
	private String msgErrorConstantesSinValor;

	@Value("${msg.error.titulares.nuevos}")
	private String msgErrorTitularesNuevos;

	@Value("${titulo.informacion.financiera.incompleta}")
	private String tituloInformacionFinancieraIncompleta;

	@Value("${titulo.constantes.incompletas}")
	private String tituloConstantesIncompletas;

	@Value("${titulo.sincronizacion.exitosa}")
	private String tituloSincronizacionExitosa;

	@Value("${titulo.cambios.datos.titulares}")
	private String tituloCambiosDatosTitulares;

	@Value("${titulo.cambio.titulares.negocio}")
	private String tituloTitularesNuevos;

	@Value("${titulo.error.porcentajes.titulares}")
	private String tituloErrorPorcentajesTitulares;
	
	@Value("${error.participacion.titulares.no.valido}")
	private String errorValidacionPorcentajesTitulares;
	
	@Value("${error.archivo.sin.contenido}")
	private String archivoVacio;
	
	@Value("${error.archivo.sin.datos}")
	private String archivoSinDatos;
	
	@Value("${error.archivo.sin.encabezado}")
	private String archivoSinEncabezado;

	@Value ("${error.conexion.servicio.cesiones}")
	private String errorConectandoAlServicioCesiones;
	
	private static final int MAXIMO_NUMERO_COLUMNAS = 13;
	private static final String MENSAJE_OK = "OK";
	private static final double MAXIMO_PORCENTAJE = 100;
	public static final String UNDEFINED = "undefined";
	public static final String PARAMETROS_VACIOS = "Los parámetros de búsqueda no pueden ser nulos o vacios";

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	@Value("${codigo.tipo.banco.traslado}")
	private String codigoTipoBancoTraslado;

	@Value("${codigo.tipo.cuenta.traslado}")
	private String codigoTipoCuentaTraslado;

	@Value("${cod.dominio.banco}")
	private String codigoDominioBanco;

	@Value("${cod.dominio.tipos.cuenta}")
	private String codigoDominioTipoCuenta;
	
	@Value("${cod.dominio.costo.tipo.cuenta}")
	private String codigoDominioCostoTipoCuenta;

	@Value("${cod.encargo.inactivo}")
	private String codigoEncargoInactivo;

	@Value("${cod.encargo.activo}")
	private String codigoEncargoActivo;

	@Value("${constante.negocio.codigo.ciudad}")
	private String constanteNegocioCodigoCiudad;

	@Value("${constante.negocio.codigo.departamento}")
	private String constanteNegocioCodigoDepartamento;

	@Value("${constante.negocio.codigo.pais}")
	private String constanteNegocioCodigoPais;

	@Value("${constante.negocio.movimiento}")
	private String constanteNegocioMovimiento;

	@Value("${constante.negocio.numero.pago.ordinario}")
	private String constanteNegocioNumeroPagoOrdinario;

	@Value("${constante.negocio.tipo.fideicomiso}")
	private String constanteNegocioTipoFideicomiso;

	@Value("${constante.negocio.tipo.movimiento.negocio}")
	private String constanteNegocioTipoMovimientoNegocio;

	@Value("${constante.negocio.tipo.proceso}")
	private String constanteNegocioTipoProceso;

	@Value("${constante.negocio.subtipo.fideicomiso}")
	private String constanteNegocioSubtipoFideicomiso;

	@Value("${titular.sin.beneficiarios}")
	private String titularSinBeneficiarios;

	@Value("${titular.beneficiarios.sin.medios.pago}")
	private String titularBeneficiariosSinMediosPago;

	@Value("${titular.suma.porcentajes.giro.diferente.cien}")
	private String titularSumaPorcentajesGiroDiferenteCien;

	@Value("${titular.cambio.nombre.completo}")
	private String titularcambioNombreCompleto;

	@Value("${titular.cambio.porcentaje.participacion}")
	private String titularCambioPorcentajeParticipacion;

	@Value("${titular.sin.naturaleza.juridica}")
	private String titularSinNaturalezaJuridica;

	@Value("${msg.error.encargos.nuevos}")
	private String msgErrorEncargosNuevos;

	@Value("${titulo.error.encargos.nuevos}")
	private String tituloEncargosNuevos;

	@Value("${formato.ddmmyyyy.guion}")
	private String formatoddMMyyyyGuion;

	@Value("${param.id.url.servicio.accion.buscar.cesiones.negocio}")
	private Integer idParamUrlServicioAccionBuscarCesionesNegocio;
	
	@Value("${clase.cesion.usufructo}")
	private Integer claseCesionUsufructo;

	@Value("${msg.error.tipo.documento.no.corresponde}")
	private String tipoDocumentoNoCorresponde;
	
	@Value("${error.encargo.no.valido}")
	private String encargoNoValido;

	@Value("${error.cliente.no.pertenece.negocio}")
	private String clienteNoPerteneceANegocio;

	@Value("${error.participacion.no.corresponde}")
	private String participacionNoCorresponde;
	
	@Value("${cod.dominio.tipo.naturaleza}")
	private String codigoDominioTipoNaturaleza;
	
	@Value("${cod.tipo.naturaleza.natural}")
	private String codigoTipoNaturalezaNatural;
	
	@Value("${cod.tipo.naturaleza.juridica}")
	private String codigoTipoNaturalezaJuridica;

	@Value("${negocio.estado.activo}")
	private String negocioActivo;
	
	@Value("${neg_negocio.estado.activo}")
	private String negocioVigente;

	@Value("${msg.error.negocio.inactivo}")
	private String msgErrorNegocioInactivo;

	@Value("${titulo.error.negocio.inactivo}")
	private String tituloNegocioInactivo;

	@Value("${pago.distribuido}")
	private String distribuido;
	
	@Override
	public boolean existeNegocioColectivo(String codigoSFC) {
		return repositorioNegocio.findByCodSfc(codigoSFC) != null;
	}

	@Override
	public Negocio obtenerNegocio(String codigoSFC) {
		return repositorioNegocio.findById(codigoSFC).orElse(null);
	}

	@Override
	public ListaNegociosDTO buscarNegocios(String argumento, String criterioBuscar, int pagina, int elementos,
			String tokenAutorizacion) {
		return procesarRespuestaBusquedaNegocios(accionBack.buscarNegocio(generarPeticionBusquedaNegocio(argumento,
				criterioBuscar, false, pagina, elementos, tokenAutorizacion)));
	}

	@Override
	public RepuestaConsultaNegocioDTO buscarNegocioPorCodigo(String codigoSFC, String tokenAutorizacion) {
		RepuestaConsultaNegocioDTO[] respuesta = accionBack.buscarNegocioPorCodigo(codigoSFC, tokenAutorizacion,
				servicioParametroSistema.obtenerValorParametro(paramIdUrlServicioAccionBusquedadNegocio));
		return respuesta[0];
	}

	@Override
	public NegocioCompletoDTO obtenerDatosNegocio(String codigoSFC, String tokenAutorizacion) {

		RepuestaConsultaNegocioDTO negocioAccion = buscarNegocioPorCodigo(codigoSFC, tokenAutorizacion);

		NegocioCompletoDTO negocioCompletoDTO = new NegocioCompletoDTO();

		if (negocioAccion == null) {
			return negocioCompletoDTO;
		}
		negocioCompletoDTO.setCodigoSFC(codigoSFC);
		negocioCompletoDTO.setCodigoInterno(negocioAccion.getCodigo());
		negocioCompletoDTO.setEstadoProyecto(negocioAccion.getEstadoNegocio().getDescripcion());
		negocioCompletoDTO.setNombre(negocioAccion.getNombre());

		negocioCompletoDTO.setEncargos(obtenerEncargosNegocio(codigoSFC, tokenAutorizacion));
		negocioCompletoDTO.setEquipoResponsable(cargarEquipoResponsable(codigoSFC, tokenAutorizacion));

		Negocio negocio = obtenerNegocio(codigoSFC);
		if (negocio != null) {
			negocioCompletoDTO.setAvaluoInmueble(negocio.getAvaluoInmueble());
			negocioCompletoDTO.setCompleto(negocio.getArchivoCargado());
			negocioCompletoDTO.setEstado(negocio.getEstado());
			negocioCompletoDTO.setFechaConstitucionNegocio(
					Utilidades.formatearFecha(negocio.getFechaConstitucion(), formatoddmmyyyy));
			negocioCompletoDTO.setFechaLiquidacionNegocioPromotor(
					Utilidades.formatearFecha(negocio.getFechaLiqNegProm(), formatoddmmyyyy));
			negocioCompletoDTO.setValorUltimoPredial(negocio.getValorUltimoPredial());
			negocioCompletoDTO.setValorUltimaValorizacion(negocio.getValorValorizacion());
			negocioCompletoDTO.setFechaCobroValorizacion(
					Utilidades.formatearFecha(negocio.getFechaCobroValorizacion(), formatoddmmyyyy));
			negocioCompletoDTO.setFechaApertura(Utilidades.formatearFecha(negocio.getFechaApertura(), formatoddmmyyyy));
			negocioCompletoDTO
					.setFechaCobroPredial(Utilidades.formatearFecha(negocio.getFechaCobroPredial(), formatoddmmyyyy));
			negocioCompletoDTO.setNumeroDerechosTotales(negocio.getTotalParticipaciones());
			negocioCompletoDTO.setConstanteNegocio(cargarConstantesNegocio(codigoSFC));
			negocioCompletoDTO.setFechasClaves(obtenerFechasClaves(negocio.getFechasClave()));
			negocioCompletoDTO.setTiposNegocio(cargarTiposNegocio(negocio));
			negocioCompletoDTO.setArchivoCargado(negocio.getArchivoCargado());
			negocioCompletoDTO.setSeccionalAduanaDepartamento(negocio.getSeccionalAduanaDepartamento());
			negocioCompletoDTO.setSeccionalAduanaCiudad(negocio.getSeccionalAduanaCiudad());
			negocioCompletoDTO.setMatriculaInmobiliaria(negocio.getMatriculaInmobiliaria());
			negocioCompletoDTO.setLicenciaConstruccion(negocio.getLicenciaConstruccion());
			negocioCompletoDTO.setCiudadUbicacionPais(negocio.getCiudadUbicacionPais());
			negocioCompletoDTO.setCiudadUbicacionDepartamento(negocio.getCiudadUbicacionDepartamento());
			negocioCompletoDTO.setCiudadUbicacion(negocio.getCiudadUbicacion());
			negocioCompletoDTO.setFideicomitenteNombreCompleto(negocio.getFideicomitente() != null ? negocio.getFideicomitente().getNombreCompleto() : null);
			negocioCompletoDTO.setFideicomitenteTipoDocumento(negocio.getFideicomitente() != null ? negocio.getFideicomitente().getPersonaPK().getTipoDocumento() : null);
			negocioCompletoDTO.setFideicomitenteNumeroDocumento(negocio.getFideicomitente() != null ? negocio.getFideicomitente().getPersonaPK().getNumeroDocumento() : null);
			negocioCompletoDTO.setCalidadFidecomitente(negocio.getCalidadFideicomitente());
			negocioCompletoDTO.setCalidadFiduciariaFideicomiso(negocio.getCalidadFiduciariaFideicomiso());
			
		}
		return negocioCompletoDTO;
	}

	@Override
	public void guardarInformacionNegocio(NegocioCompletoDTO negocioCompletoDTO, String tokenAutorizacion) {
		Negocio negocio = new Negocio();
		negocio.setCodSfc(negocioCompletoDTO.getCodigoSFC());
		negocio.setNombre(negocioCompletoDTO.getNombre());
		negocio.setEstado(negocioCompletoDTO.getEstadoProyecto());
		negocio.setFechaApertura(utilidades.convertirFecha(negocioCompletoDTO.getFechaApertura(), formatoddmmyyyy));
		negocio.setTotalParticipaciones(negocioCompletoDTO.getNumeroDerechosTotales() != null ? negocioCompletoDTO.getNumeroDerechosTotales() : 0);
		negocio.setFechaLiqNegProm(
				utilidades.convertirFecha(negocioCompletoDTO.getFechaLiquidacionNegocioPromotor(), formatoddmmyyyy));
		negocio.setFechaConstitucion(
				utilidades.convertirFecha(negocioCompletoDTO.getFechaConstitucionNegocio(), formatoddmmyyyy));
		negocio.setAvaluoInmueble(negocioCompletoDTO.getAvaluoInmueble());
		negocio.setValorUltimoPredial(negocioCompletoDTO.getValorUltimoPredial());
		negocio.setValorValorizacion(negocioCompletoDTO.getValorUltimaValorizacion());
		negocio.setFechaCobroPredial(
				utilidades.convertirFecha(negocioCompletoDTO.getFechaCobroPredial(), formatoddmmyyyy));
		negocio.setFechaCobroValorizacion(
				utilidades.convertirFecha(negocioCompletoDTO.getFechaCobroValorizacion(), formatoddmmyyyy));
		negocio.setSeccionalAduanaDepartamento(negocioCompletoDTO.getSeccionalAduanaDepartamento());
		negocio.setSeccionalAduanaCiudad(negocioCompletoDTO.getSeccionalAduanaCiudad());
		negocio.setMatriculaInmobiliaria(negocioCompletoDTO.getMatriculaInmobiliaria());
		negocio.setLicenciaConstruccion(negocioCompletoDTO.getLicenciaConstruccion());
		negocio.setCiudadUbicacionPais(negocioCompletoDTO.getCiudadUbicacionPais());
		negocio.setCiudadUbicacionDepartamento(negocioCompletoDTO.getCiudadUbicacionDepartamento());
		negocio.setCiudadUbicacion(negocioCompletoDTO.getCiudadUbicacion());
		if (negocioCompletoDTO.getFideicomitenteNombreCompleto() != null
				&& !negocioCompletoDTO.getFideicomitenteNombreCompleto().isEmpty()
				&& negocioCompletoDTO.getFideicomitenteTipoDocumento() != null
				&& !negocioCompletoDTO.getFideicomitenteTipoDocumento().isEmpty()
				&& negocioCompletoDTO.getFideicomitenteNumeroDocumento() != null
				&& !negocioCompletoDTO.getFideicomitenteNumeroDocumento().isEmpty()) {
			
			negocio.setFideicomitente(obtenerPersona(negocioCompletoDTO.getFideicomitenteNombreCompleto(),
					negocioCompletoDTO.getFideicomitenteNumeroDocumento(),
					negocioCompletoDTO.getFideicomitenteTipoDocumento()));
		}
		
		negocio.setCalidadFideicomitente(negocioCompletoDTO.getCalidadFidecomitente());
		negocio.setCalidadFiduciariaFideicomiso(negocioCompletoDTO.getCalidadFiduciariaFideicomiso());
		
		Negocio negocioActual = obtenerNegocio(negocioCompletoDTO.getCodigoSFC());
		
		if (negocioActual != null) {
			negocio.setArchivoCargado(negocioActual.getArchivoCargado());
		} else {
			negocio.setArchivoCargado(Boolean.FALSE);
		}

		negocio = repositorioNegocio.save(negocio);

		guardarTiposNegocio(negocioCompletoDTO, negocio);
		guardarFechasClave(negocioCompletoDTO, negocio);
		guardarEncargos(negocioCompletoDTO.getEncargos(), negocio);
		guardarConstantesPago(negocioCompletoDTO.getConstanteNegocio(), negocioCompletoDTO.getCodigoSFC());
		guardarEquipoResponsable(negocioCompletoDTO.getCodigoSFC(), tokenAutorizacion);
	}

	@Override
	public List<EncargoNegocioDTO> obtenerEncargosNegocio(String codigoNegocio, String tokenAutorizacion) {
		List<Encargo> encargos = servicioEncargo.buscarEncargoNegocio(codigoNegocio);
		List<EncargoNegocioDTO> encargosDTO = new ArrayList<>();
		if (encargos != null) {
			encargosDTO = encargos.stream().filter(encargo -> encargo.getEstado().equals(negocioActivo))
					.map(encargo -> new EncargoNegocioDTO(encargo.getEstado(), encargo.getEncargoPK().getCuenta(),
							encargo.getMovimientoFondo(), encargo.getNumeroPagoOrdinario()))
					.collect(Collectors.toList());
		}

		Set<EncargoNegocioDTO> encargosCombinados = new LinkedHashSet<>(encargosDTO);

		RespuestaAccionBuscarEncargoDTO[] encargosAccion = accionBack.buscarEncargosNegocio(codigoNegocio,
				tokenAutorizacion,
				servicioParametroSistema.obtenerValorParametro(paramIdUrlServicioAccionBusquedadEncargosNegocio));

		encargosCombinados
				.addAll(Arrays.stream(encargosAccion).filter(encargo -> encargo.getEstado().equals(negocioActivo))
						.map(encargo -> new EncargoNegocioDTO(encargo.getEstado(), encargo.getNumeroEncargo()))
						.collect(Collectors.toList()));

		return new ArrayList<>(encargosCombinados);
	}

	@Override
	public RespuestaSincronizacionDatosNegocioDTO sincronizarYValidarDatos(String codigoNegocio,
			String tokenAutorizacion) {
		Negocio negocio = obtenerNegocio(codigoNegocio);

		ResultadoSincronizacionTitularesYEncargosDTO resultadoSincronizacionTitularesYEncargosDTO = sincronizarDatosNegocio(
				negocio, tokenAutorizacion);

		List<Titular> titulares = servicioTitular.buscarTitularesPorCodigoNegocio(negocio.getCodSfc());

		boolean error = false;
		String mensaje = "";
		String titulo = "";

		RespuestaSincronizacionDatosNegocioDTO respuestaSincronizacionDatosNegocioDTO = new RespuestaSincronizacionDatosNegocioDTO();

		if(!resultadoSincronizacionTitularesYEncargosDTO.isNegocioActivo()) {
			error = true;
			mensaje = msgErrorNegocioInactivo;
			titulo = tituloNegocioInactivo;
		} else if (resultadoSincronizacionTitularesYEncargosDTO.isTieneEncargosNuevos()) {
			error = true;
			mensaje = msgErrorEncargosNuevos;
			titulo = tituloEncargosNuevos;
		} else if (!resultadoSincronizacionTitularesYEncargosDTO.getTitularesNuevos().isEmpty()) {
			error = true;
			mensaje = msgErrorTitularesNuevos;
			titulo = tituloTitularesNuevos;
			respuestaSincronizacionDatosNegocioDTO
					.setTitularesNuevos(resultadoSincronizacionTitularesYEncargosDTO.getTitularesNuevos());

		} else if (titulares.stream().map(titular -> titular.getPorcentaje()).reduce(BigDecimal.ZERO, BigDecimal::add)
				.compareTo(BigDecimal.valueOf(MAXIMO_PORCENTAJE)) != 0) {
			error = true;
			mensaje = msgErrorSumaPorcentajesTitulares;
			titulo = tituloErrorPorcentajesTitulares;
		} else {
			List<InformacionTitularesIncompletaDTO> informacionTitularesIncompleta = titulares.stream()
					.map(titular -> verificarInformacionFinancieraTitular(titular)).filter(Objects::nonNull)
					.collect(Collectors.toList());
			if (!informacionTitularesIncompleta.isEmpty()) {
				error = true;
				mensaje = msgErrorInformacionFinancieraIncompleta;
				titulo = tituloInformacionFinancieraIncompleta;
				respuestaSincronizacionDatosNegocioDTO
						.setInformacionTitularesIncompleta(informacionTitularesIncompleta);
			} else {
				List<String> constantesIncompletas = validarConstatesNegocio(negocio.getCodSfc());
				if (!constantesIncompletas.isEmpty()) {
					error = true;
					mensaje = msgErrorConstantesSinValor;
					titulo = tituloConstantesIncompletas;
					respuestaSincronizacionDatosNegocioDTO.setConstantesIncompletas(constantesIncompletas);
				}
			}
		}

		if (!error) {
			if (!resultadoSincronizacionTitularesYEncargosDTO.getCambioDatosTitularesNegocio().isEmpty()) {
				titulo = tituloCambiosDatosTitulares;
				respuestaSincronizacionDatosNegocioDTO.setCambioDatosTitularesNegocio(
						resultadoSincronizacionTitularesYEncargosDTO.getCambioDatosTitularesNegocio());
			} else {
				titulo = tituloSincronizacionExitosa;
			}
		}

		respuestaSincronizacionDatosNegocioDTO.setError(error);
		respuestaSincronizacionDatosNegocioDTO.setMensaje(mensaje);
		respuestaSincronizacionDatosNegocioDTO.setTitulo(titulo);

		return respuestaSincronizacionDatosNegocioDTO;
	}

	/**
	 * private String nombreGasto; private Integer tipoGasto; private String
	 * unidadGasto; private String valorGasto; private List<String> periodicidad;
	 * private String fechaPagoGasto; private String ficOrigen;
	 */

	/**
	 * Carga los parÃ¡metros necesarios para realizar la peticiÃ³n al servicio de
	 * AccionBack de busquedad de beneficiarios.
	 * 
	 * @param argumento         El valor del criterio por el cual se realizara la
	 *                          bÃºsqueda.
	 * @param criterioBuscar    El criterio por el cual se realizara la busquedad:
	 *                          nombre o documento
	 * @param pagina            La pagina en la que se ubicara el listado de
	 *                          resultados.
	 * @param elementos         El nÃºmero de elementos por pagina que se mostraran.
	 * @param tokenAutorizacion El token de autorizaciÃ³n.
	 * @return Un DTOP con los datos de la consulta.
	 */
	private PeticionAccionPaginada generarPeticionBusquedaNegocio(String argumento, String criterioBuscar,
			boolean busquedaExacta, int pagina, int elementos, String tokenAutorizacion) {
		
		String nombre = criterioBuscar.equalsIgnoreCase(propNombre.trim()) ? argumento.trim() : "";
		String codigo = criterioBuscar.equalsIgnoreCase(propCodigo.trim()) ? argumento.trim() : "";

		String urlServicioAccionBusquedadBeneficiario = servicioParametroSistema
				.obtenerValorParametro(paramIdUrlServicioAccionBusquedadNegocio);

		return new PeticionAccionPaginada(elementos, pagina - 1, tokenAutorizacion, busquedaExacta, nombre, codigo,
				urlServicioAccionBusquedadBeneficiario);
	}

	/**
	 * Metodo que permite crear titulares y beneficiarios masivamente a un negocio
	 * que previamente se encuenttre generado
	 * 
	 * @param archivo       Objeto que contiene el archivo excel que sera procesado
	 * @param token         Token de seguridad de acceso a la aplicacion
	 * @param codigoNegocio Codigo del negocio que ha sido generado previaamente
	 * @return RespuestaArchivoDTO Objeto que envia al fronted de la aplicacion los
	 *         resultados del proceso de cargue.
	 * @throws Exception
	 */
	@Override
	public RespuestaArchivoDTO asociarBeneficiarios(MultipartFile archivo, String token, String  codigoNegocio)
			throws Exception {
		try {
			RespuestaArchivoDTO respuesta = new RespuestaArchivoDTO();
			if (archivo == null || !archivo.getOriginalFilename().endsWith(".xlsx"))
				throw new ArchivoNoValidoException();
			// Realizamos la lectura del archivo a ser procesado
			XSSFWorkbook libro = new XSSFWorkbook(archivo.getInputStream());
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			XSSFSheet hoja = libro.getSheetAt(0);
			XSSFRow filaCabecera = hoja.getRow(0);
			List<String> cabecera = new ArrayList<>();
			// Almacenamos la informacion de la cabecera y luego la eliminamos para que
			// genere conflicto durante el procesamiento
			if(filaCabecera != null) {
				for (Iterator<Cell> iterator = filaCabecera.cellIterator(); iterator.hasNext();) {
					Cell celda = iterator.next();
					cabecera.add(celda.toString());
				}
			}
			// Valida la estructura general del archivo
			boolean estructuraValida = validaEstructuraArchivo(libro);
			if (estructuraValida) {
				hoja.removeRow(filaCabecera);
			}
			// Realizamos las validaciones sobre el archivo antes de ser procesado.
			if (estructuraValida && esValido(libro, codigoNegocio, token)) {
				// Procesamiento del cargue del archivo.
				procesaArchivoVinculados(libro, codigoNegocio, token);
				respuesta.setTipoArchivo(TiposArchivo.VALIDO.toString());
			} else {
				respuesta.setTipoArchivo(TiposArchivo.INVALIDO.toString());
			}
			// Generamos nnuevamente la cabecera del archivo resultante
			if(!cabecera.isEmpty()) {
				filaCabecera = hoja.createRow(0);
				cabecera.add("RESULTADOS");
				for (int i = 0; i < cabecera.size(); i++) {
					Cell celda = filaCabecera.createCell(i);
					celda.setCellValue(cabecera.get(i));
				}
			}
			// Se construye y envia el archivo al servicio Rest.
			libro.write(stream);
			libro.close();
			respuesta.setRecurso(new ByteArrayResource(stream.toByteArray()));
			return respuesta;
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}

	/**
	 * Agrupa y procesa un archivo de cargue de beneficiarios
	 * 
	 * @param libro
	 * @param codigoNegocio
	 * @throws Exception 
	 */
	private void procesaArchivoVinculados(XSSFWorkbook libro, String codigoNegocio, String token) throws Exception {
		XSSFSheet hoja = libro.getSheetAt(0);

		// Busca el negocio asociado
		Negocio negocio = obtenerNegocio(codigoNegocio);

		if (negocio == null)
			throw new BusquedadSinResultadosException("No se encontro algun negocio asociado");

		Integer idTipoNaturalezaNatural = servicioDominio
				.buscarPorCodigoDominioYValor(codigoDominioTipoNaturaleza, codigoTipoNaturalezaNatural)
				.getIdValorDominio();
		Integer idTipoNaturalezaJuridica = servicioDominio
				.buscarPorCodigoDominioYValor(codigoDominioTipoNaturaleza, codigoTipoNaturalezaJuridica)
				.getIdValorDominio();
		List<RetencionTipoNegocio> retencionesNegocio = servicioRetencionTipoNegocio
				.buscarRetencionesTipoNegocioPorNegocio(negocio);
		List<RelacionEntreDominios> retencionesTipoNatural = servicioRelacionEntreDominios
				.buscarValoresAplicanRelacion(idTipoNaturalezaNatural);
		List<RelacionEntreDominios> retencionesTipoJuridica = servicioRelacionEntreDominios
				.buscarValoresAplicanRelacion(idTipoNaturalezaJuridica);

		// Agrupa por titulares.
		for (Map.Entry<String, List<Row>> entry : agrupaParticipantes(hoja).entrySet()) {
			procesarParticipante(entry.getValue(), negocio, idTipoNaturalezaNatural, idTipoNaturalezaJuridica,
					retencionesNegocio, retencionesTipoNatural, retencionesTipoJuridica, token);
		}

		// Cambia el estado del cargue de un archivo para el negocio asociaado.
		negocio.setArchivoCargado(true);
		negocio.setFechaSincronizacionBus(new Date());
		repositorioNegocio.save(negocio);
	}

	/**
	 * Procesa una agrupacion de titulares
	 * 
	 * @param filas   Conjunto de titulares que pertenecen a un determinado grupo
	 * @param negocio Negocio de asociacion.
	 * @throws Exception 
	 */
	private void procesarParticipante(List<Row> filas, Negocio negocio, Integer idTipoNaturalezaNatural,
			Integer idTipoNaturalezaJuridica, List<RetencionTipoNegocio> retencionesNegocio,
			List<RelacionEntreDominios> retencionesTipoNatural, List<RelacionEntreDominios> retencionesTipoJuridica,
			String token) throws Exception {
		List<Row> titulares = filas.stream().filter(distinctByKey(fila -> fila.getCell(4).toString()))
				.collect(Collectors.toList());
		ParticipacionesNegocio participaciones = new ParticipacionesNegocio();
		for (int indice = 0; indice < titulares.size(); indice++) {
			Row titular = titulares.get(indice);
			String numeroDocumentoOriginal = titular.getCell(4).toString();
			String numeroDocumento = transformarDocumentoAString(numeroDocumentoOriginal);
			String tipoDocumento = titular.getCell(3).toString();
			float porcentajeParticipacion = Float.parseFloat(titular.getCell(1).toString());
			Integer totalDerechos = negocio.getTotalParticipaciones();

			registrarTitular(titular, negocio, indice, titulares.size(), participaciones, idTipoNaturalezaNatural,
					idTipoNaturalezaJuridica, retencionesNegocio, retencionesTipoNatural, retencionesTipoJuridica,
					token);
			// Obtiene la lista de beneficiarios asociados a un determminados titular
			List<BeneficiarioTipoPagoDTO> beneficiarios = filtrarRegistros(filas, 4, numeroDocumentoOriginal).stream()
					.map(fila -> transformarBeneficiario(fila, totalDerechos)).collect(Collectors.toList());
			// Calcula la proporcion del numero de derechos que tiene de participacion.
			float numeroDerechos = utilidades.calcularProporcionPorcentaje(porcentajeParticipacion, totalDerechos);
			// Transforma los datos de los beneficiarios para que puedaan ser registrados.
			InformacionFinancieraRecepcionDTO informacion = new InformacionFinancieraRecepcionDTO();
			informacion.setBeneficiarios(beneficiarios);
			informacion.setCodigoNegocio(negocio.getCodSfc());
			informacion.setDocumento(numeroDocumento);
			informacion.setTotalDerechosNegocio(totalDerechos);
			informacion.setNumeroDerechosTitular((int) numeroDerechos);
			informacion.setTipoDocumentoTitular(tipoDocumento);
			// Registra la lista de beneficiarios cargados.
			servicioBeneficiario.guardarInformacionFinanciera(informacion);
		}
	}

	/**
	 * Metodo que realiza distinct sobre una colleccion.
	 * 
	 * @param <T>
	 * @param keyExtractor
	 * @return
	 */
	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

	/**
	 * Metodo que realiza el registro de titulares aagrupados y sus respectivas
	 * participaciones dentro de un negocio
	 * 
	 * @param fila
	 * @param negocio
	 * @param indiceFila
	 * @param cantidadParticipantes
	 * @throws Exception 
	 */
	private void registrarTitular(Row fila, Negocio negocio, int indiceFila, int cantidadParticipantes,
			ParticipacionesNegocio participaciones, Integer idTipoNaturalezaNatural, Integer idTipoNaturalezaJuridica,
			List<RetencionTipoNegocio> retencionesNegocio, List<RelacionEntreDominios> retencionesTipoNatural,
			List<RelacionEntreDominios> retencionesTipoJuridica, String token) throws Exception {

		String porcentajeParticipacion = fila.getCell(1).toString();
		String nombreTitular = fila.getCell(2).toString();
		String numeroDocumentoTitular = transformarDocumentoAString(fila.getCell(4).toString());
		String tipoDocumentoTitular = fila.getCell(3).toString();

		// Registro de los datos basicos del Titular.
		Persona persona = obtenerPersona(nombreTitular, numeroDocumentoTitular, tipoDocumentoTitular);

		BigDecimal numeroDerechos = new BigDecimal(porcentajeParticipacion)
				.multiply(BigDecimal.valueOf(negocio.getTotalParticipaciones().longValue()))
				.divide(BigDecimal.valueOf(100));

		// Registro de las participaciones de un titular.
		if (indiceFila == 0 && participaciones.getIdParticipacion() == null) {
			participaciones.setNegocio(negocio);
			participaciones.setNumeroDerechos(numeroDerechos.intValue());
			participaciones.setPorcentajeParticipacion(new BigDecimal(porcentajeParticipacion));
			servicioParticipacion.guardar(participaciones);
		}

		BigDecimal porcentaje = new BigDecimal(porcentajeParticipacion)
				.divide(BigDecimal.valueOf(cantidadParticipantes));

		// Registro de un titular asociado a un negocio.
		TitularPK llaveTitular = new TitularPK(negocio.getCodSfc(), tipoDocumentoTitular, numeroDocumentoTitular);

		RespuestaBusquedadVinculadoDTO vinculado = servicioVinculados
				.buscarBeneficiarioPorDocumento(numeroDocumentoTitular, token);

		Titular titular = new Titular();
		titular.setTitularPK(llaveTitular);
		titular.setPorcentaje(porcentaje);
		titular.setEsCotitular(indiceFila != 0);
		titular.setNumeroDerechos((int) (numeroDerechos.intValue() / cantidadParticipantes));
		titular.setParticipacion(participaciones);
		titular.setActivo(Boolean.TRUE);
		titular.setPersona(persona);
		if (vinculado.getNaturalezaJuridicaVinculado() != null) {
			titular.setIdTipoNaturalezaJuridica(
					vinculado.getNaturalezaJuridicaVinculado().equalsIgnoreCase(codigoTipoNaturalezaJuridica)
							? idTipoNaturalezaJuridica
							: idTipoNaturalezaNatural);

		}

		servicioTitular.guardarTitular(titular);
		
		List<RetencionTitular> retencionesTitular = retencionesNegocio.stream()
				.map(retencionNegocio -> new RetencionTitular(
						retencionNegocio.getRetencionTipoNegocioPK().getIdRetencion(), negocio.getCodSfc(),
						numeroDocumentoTitular, tipoDocumentoTitular, Boolean.TRUE,
						retencionNegocio.getTipoNegocio().getTipoNegocioPK().getTipoNegocio()))
				.collect(Collectors.toList());
		servicioRetencionTitular.guardarRetencionesTitular(retencionesTitular);

		if (vinculado.getNaturalezaJuridicaVinculado() != null) {
			if (vinculado.getNaturalezaJuridicaVinculado().equalsIgnoreCase(codigoTipoNaturalezaJuridica)) {
				retencionesTitular = retencionesTipoJuridica.stream()
						.map(relacionEntreDominios -> new RetencionTitular(
								relacionEntreDominios.getValorDominioNegocio().getIdValorDominio(),
								negocio.getCodSfc(), numeroDocumentoTitular, tipoDocumentoTitular, Boolean.TRUE,
								relacionEntreDominios.getValorDominioNegocio1().getIdValorDominio()))
						.collect(Collectors.toList());
			} else {
				retencionesTitular = retencionesTipoNatural.stream()
						.map(relacionEntreDominios -> new RetencionTitular(
								relacionEntreDominios.getValorDominioNegocio().getIdValorDominio(),
								negocio.getCodSfc(), numeroDocumentoTitular, tipoDocumentoTitular, Boolean.TRUE,
								relacionEntreDominios.getValorDominioNegocio1().getIdValorDominio()))
						.collect(Collectors.toList());
			}
			servicioRetencionTitular.guardarRetencionesTitular(retencionesTitular);
		}
	}

	private Persona obtenerPersona(String nombre, String numeroDocumento, String tipoDocumento) {
		Persona persona = servicioPersona.buscarPersona(tipoDocumento, numeroDocumento);
		if (persona == null) {
			PersonaPK llavePersona = new PersonaPK();
			llavePersona.setNumeroDocumento(numeroDocumento);
			llavePersona.setTipoDocumento(tipoDocumento);
			persona = new Persona();
			persona.setPersonaPK(llavePersona);
			persona.setNombreCompleto(nombre);
			servicioPersona.guardarPersona(persona);
		}
		return persona;
	}

	/**
	 * Metodo que transforma la informacion de un beneficiario hacia el DTO que se
	 * enviara para ser registrado.
	 * 
	 * @param fila
	 * @param derechosTotalesNegocio
	 * @return
	 */
	private static BeneficiarioTipoPagoDTO transformarBeneficiario(Row fila, Integer derechosTotalesNegocio) {
		BigDecimal porcentajeGiro = new BigDecimal(fila.getCell(5).toString());
		String nombreBeneficiario = fila.getCell(6).toString();
		String tipoDocumentoBeneficiario = fila.getCell(7).toString();
		String numeroDocumentoBeneficiario = transformarDocumentoAString(fila.getCell(8).toString());
		String codigoBanco = transformarDocumentoAString(fila.getCell(10).toString());
		String tipoCuenta = transformarDocumentoAString(fila.getCell(11).toString());
		String numeroCuenta = transformarDocumentoAString(fila.getCell(12).toString());
		String tipoPago = fila.getCell(9).toString();

		String porcentajeTitular = fila.getCell(1).toString();
		BigDecimal porcentajeDistrucion = porcentajeGiro.multiply(new BigDecimal(porcentajeTitular)).divide(BigDecimal.valueOf(100));
		BigDecimal derechosBeneficiario =  BigDecimal.valueOf(derechosTotalesNegocio.longValue()).multiply(porcentajeDistrucion).divide(BigDecimal.valueOf(100));

		BeneficiarioTipoPagoDTO beneficiarioPago = new BeneficiarioTipoPagoDTO();
		beneficiarioPago.setCodigoBanco(codigoBanco);
		beneficiarioPago.setTipoCuenta(tipoCuenta);
		beneficiarioPago.setNumeroCuentaBancaria(numeroCuenta);
		beneficiarioPago.setPorcentaje(porcentajeGiro);
		beneficiarioPago.setPorcentajeGiro(porcentajeGiro);
		beneficiarioPago.setPorcentajeDistribucion(porcentajeDistrucion);
		beneficiarioPago.setBeneficiario(nombreBeneficiario);
		beneficiarioPago.setTipoDocumento(tipoDocumentoBeneficiario);
		beneficiarioPago.setNumeroDocumento(numeroDocumentoBeneficiario);
		beneficiarioPago.setNumeroReferencia(numeroDocumentoBeneficiario);
		beneficiarioPago.setNumeroEncargo(numeroCuenta);
		beneficiarioPago.setNumeroDerechos(derechosBeneficiario.floatValue());
		beneficiarioPago.setTipoPago(tipoPago.toUpperCase());
		return beneficiarioPago;
	}

	/**
	 * Metodo de validacion general del cargue de beneficiarios.
	 * 
	 * @param libro Archivo excel a procesarse.
	 * @param token LLave de autenticacion de la aplicacion.
	 * @return boolean Si el archivo es valido o si no lo es.
	 * @throws Exception
	 */
	private boolean esValido(XSSFWorkbook libro, String codigoNegocio, String token) throws Exception {
		boolean valido = true;
		XSSFSheet hoja = libro.getSheetAt(0);
		
		// Valida cada unos de los campos de acuerdo a la logica de negocio.
		if (!validaCampos(libro, token))
			return false;
		// Valida que el procentaje de participacion de cada grupo sea igual a 100
		if (!validaProcentajeParticipacion(hoja)) {
			escribeLogExcel(hoja, hoja.getLastRowNum() + 1, 0, errorValidacionPorcentajesTitulares, utilidades.estiloDefecto(libro));
			valido = false;
		}
		// Valida que la suma de los porcentajes de los beneficiarioa para cada grupo
		// sea igual a 100.
		if (!validaPorcentajeGiro(hoja)) {
			escribeLogExcel(hoja, hoja.getLastRowNum() + 1, 0, porcentajeGiroNoValido, utilidades.estiloDefecto(libro));
			valido = false;
		}
		// Valida que se encuentren titulares registrados en AccionFiduciaria
		try {
			if (!validaExistenciaVinculados(hoja, codigoNegocio, token))
				valido = false;
		} catch (ErrorAlConectarAccionException e) {
			escribeLogExcel(hoja, hoja.getLastRowNum() + 1, 0, errorConexionFiduciaaria, utilidades.estiloDefecto(libro));
			return false;
		}

		return valido;
	}

	private boolean validaEstructuraArchivo(XSSFWorkbook libro) {
		XSSFSheet hoja = libro.getSheetAt(0);
		if(hoja.getLastRowNum() == -1) {
			escribeLogExcel(hoja, 0, 0, archivoVacio, utilidades.estiloDefecto(libro));
			return false;
		}
		
		if(hoja.getRow(0) == null || utilidades.isRowEmpty(hoja.getRow(0))) {
			escribeLogExcel(hoja, hoja.getLastRowNum() + 1, 0, archivoSinEncabezado, utilidades.estiloDefecto(libro));
			return false;
		}
		// Valida que el numero maximo de columnas permitidas e el archivo de excel sea
		// igual a 13
		if(obtenerCantidadColumnas(libro.getSheetAt(0)) != MAXIMO_NUMERO_COLUMNAS) {
			escribeLogExcel(hoja, hoja.getLastRowNum() + 1, 0, numeroColumnasNoValido, utilidades.estiloDefecto(libro));
			return false;
		}
		
		if(hoja.getLastRowNum() < 1) {
			escribeLogExcel(hoja, hoja.getLastRowNum() + 1, 0, archivoSinDatos, utilidades.estiloDefecto(libro));
			return false;
		}
		return true;	
	}

	/**
	 * 
	 * @param hoja
	 * @param indiceFila
	 * @param indiceCelda
	 * @param mensaje
	 */
	private void escribeLogExcel(XSSFSheet hoja, Integer indiceFila, Integer indiceCelda, String mensaje,
			CellStyle estiloCelda) {
		if (indiceFila == null || mensaje.length() == 0)
			return;

		XSSFRow fila = hoja.getRow(indiceFila);
		if (fila == null)
			fila = hoja.createRow(indiceFila);
		XSSFCell ultimaCelda = fila.getCell(indiceCelda);
		if (ultimaCelda == null)
			ultimaCelda = fila.createCell(indiceCelda);
		if (estiloCelda != null)
			ultimaCelda.setCellStyle(estiloCelda);
		ultimaCelda.setCellValue(ultimaCelda.getCellType() == CellType.BLANK ? mensaje : ultimaCelda.toString().trim().replace(MENSAJE_OK, "") + mensaje);
	}

	private int obtenerCantidadColumnas(XSSFSheet hoja) {
		return hoja.getRow(hoja.getFirstRowNum()).getLastCellNum();
	}

	private boolean validaCampos(XSSFWorkbook libro, String tokenAutorizacion) throws Exception {
		boolean valido = true;
		XSSFSheet hoja = libro.getSheetAt(0);

		CellStyle estiloCelda = libro.createCellStyle();
		Font fuente = libro.createFont();
		fuente.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
		estiloCelda.setFont(fuente);

		CellStyle estiloCeldaDos = libro.createCellStyle();
		fuente.setColor(HSSFColor.HSSFColorPredefined.GREEN.getIndex());
		estiloCeldaDos.setFont(fuente);

		Iterator<Row> rowtIterator = hoja.iterator();
		List<TipoDocumentoDTO> tiposoDocumentoPermitidos = servicioDominio.obtenerTiposDocumento();
		List<DatosBasicosValorDominioDTO> bancosPermitidos = servicioDominio.obtenerValoresDominio(codigoDominioBanco);
		List<DatosBasicosValorDominioDTO> tipoCuentasPermitidas = servicioDominio.obtenerValoresDominio(codigoDominioTipoCuenta);
		List<DatosBasicosValorDominioDTO> tiposPagoPermitidos = servicioDominio.obtenerValoresDominio(codigoDominioCostoTipoCuenta);
		while (rowtIterator.hasNext()) {
			Row fila = rowtIterator.next();
			if(utilidades.isRowEmpty(fila)) {
				escribeLogExcel(hoja, fila.getRowNum(), 0, archivoSinDatos, estiloCeldaDos);
				continue;
			}
			if (fila.getRowNum() == 0)
				continue;
			if (fila.getCell(0) == null) {
				break;
			}
			StringBuilder mensaje = new StringBuilder();
			if (!utilidades.esNumerico(fila.getCell(0).toString()))
				mensaje.append(codigoNegocoNoValido);

			if (!utilidades.esNumerico(fila.getCell(1).toString()))
				mensaje.append(porcetanjeParticipacionNoValido);

			if (tiposoDocumentoPermitidos.stream().filter(tipo -> fila.getCell(3).toString().equals(tipo.getId()))
					.count() <= 0)
				mensaje.append(titularTipoDocNoValido);

			if (!(transformarDocumentoAString(fila.getCell(4).toString()).length() < 11
					&& StringUtils.isNumeric(transformarDocumentoAString(fila.getCell(4).toString()))))
				mensaje.append(titularNumeroDocNoValido);
			
			if (!utilidades.esNumerico(fila.getCell(5).toString()))
				mensaje.append(porcetanjeParticipacionGiroNoValido);

			if (tiposoDocumentoPermitidos.stream().filter(tipo -> fila.getCell(7).toString().equals(tipo.getId()))
					.count() <= 0)
				mensaje.append(beneficiarioTipoDocNoValido);

			if (!(transformarDocumentoAString(fila.getCell(8).toString()).length() < 11
					&& StringUtils.isNumeric(transformarDocumentoAString(fila.getCell(8).toString()))))
				mensaje.append(beneficiarioNumeroDocNoValido);

			if (tiposPagoPermitidos.stream()
					.filter(tipoPago -> fila.getCell(9).toString().trim().equals(tipoPago.getDescripcion()))
					.count() <= 0)
				mensaje.append(tipoPagoNoValido);

			if (bancosPermitidos.stream().filter(tipoBanco -> transformarDocumentoAString(fila.getCell(10).toString())
					.trim().equals(tipoBanco.getValor())).count() <= 0)
				mensaje.append(bancoTipoNoValido);

			if (tipoCuentasPermitidas.stream()
					.filter(tipoCuenta -> transformarDocumentoAString(fila.getCell(11).toString())
							.equalsIgnoreCase(tipoCuenta.getValor()))
					.count() <= 0)
				mensaje.append(tipoCuentaNoValida);

			if(transformarDocumentoAString(fila.getCell(11).toString())
					.equals(codigoTipoCuentaTraslado)) {
				EncargoDTO encargo = servicioVinculados.validarEncargo(transformarDocumentoAString(fila.getCell(12).toString()), transformarDocumentoAString(fila.getCell(8).toString()), tokenAutorizacion);
				if(Boolean.FALSE.equals(encargo.getActivo())) {
					mensaje.append(encargoNoValido);
				}
			}
			
			if (!(StringUtils.isNumeric(transformarDocumentoAString(fila.getCell(12).toString()))))
				mensaje.append(numeroCuentaNoValida);
			int cantidadCaracteres = mensaje.length();
			escribeLogExcel(hoja, fila.getRowNum(), (int) fila.getLastCellNum(),
					cantidadCaracteres > 0 ? mensaje.toString() : MENSAJE_OK,
					cantidadCaracteres > 0 ? estiloCelda : estiloCeldaDos);
			valido &= cantidadCaracteres == 0;
		}
		return valido;
	}

	private boolean validaExistenciaVinculados(XSSFSheet hoja, String codigoNegocio, String token) throws ErrorAlConectarAccionException {
		boolean valido = true;
		Iterator<Row> rowtIterator = hoja.iterator();
		
		RespuestaBusquedadVinculadoDTO[] clientesNegocio = accionBack.buscarBeneficiariosNegocio(
				obtenerUrlServiciosNegocio(codigoNegocio, paramIdUrlServicioAccionBuscarBeneficiariosNegocio),
				token);
		
		while (rowtIterator.hasNext()) {
			Row fila = rowtIterator.next();
			if (fila.getRowNum() == 0 || fila.getCell(4) == null)
				continue;
			try {
				String documento = transformarDocumentoAString(fila.getCell(4).toString());
				String tipoDocumento = transformarDocumentoAString(fila.getCell(3).toString());
				BigDecimal participacion = BigDecimal.valueOf((fila.getCell(1).getNumericCellValue()));

				RespuestaBusquedadVinculadoDTO vinculado = servicioVinculados.buscarBeneficiarioPorDocumento(documento,
						token);
				if (!tipoDocumento.equalsIgnoreCase(vinculado.getTipoDocumento().getId())) {
					valido = false;
					escribeLogExcel(hoja, fila.getRowNum(), fila.getLastCellNum() - 1, tipoDocumentoNoCorresponde,
							null);
				}

				RespuestaBusquedadVinculadoDTO clienteActual = Arrays.stream(clientesNegocio)
						.filter(cliente -> cliente.getNumeroDocumento().equals(documento) && utilidades
								.covertirTipoDocumento(cliente.getTipoDocumento().getId()).equals(tipoDocumento))
						.findAny().orElse(null);

				if (clienteActual == null) {
					valido = false;
					escribeLogExcel(hoja, fila.getRowNum(), fila.getLastCellNum() - 1, clienteNoPerteneceANegocio,
							null);
				} else if (clienteActual.getParticipacion().compareTo(participacion) != 0) {
					valido = false;
					escribeLogExcel(hoja, fila.getRowNum(), fila.getLastCellNum() - 1, participacionNoCorresponde,
							null);
				}

			} catch (ServiceNotFoundException | NoContentException e) {
				escribeLogExcel(hoja, fila.getRowNum(), fila.getLastCellNum() - 1, titularNoEncontrado, null);
				valido = false;
			} catch (Exception e) {
				throw new ErrorAlConectarAccionException();
			}
		}
		return valido;
	}

	

	private boolean validaProcentajeParticipacion(XSSFSheet hoja) {
		BigDecimal sumatoria = BigDecimal.ZERO;
		Map<String, List<Row>> grupos = agrupaParticipantes(hoja);
		try {
			for (Map.Entry<String, List<Row>> entry : grupos.entrySet()) {
				sumatoria = sumatoria.add(BigDecimal.valueOf((entry.getValue().get(0).getCell(1).getNumericCellValue())));
			}
			return sumatoria.compareTo(BigDecimal.valueOf((MAXIMO_PORCENTAJE))) == 0;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean validaPorcentajeGiro(XSSFSheet hoja) {
		boolean esValido = true;
		Map<String, List<Row>> grupos = agrupaParticipantes(hoja);
		for (Map.Entry<String, List<Row>> entry : grupos.entrySet()) {
			List<Row> listaFilas = entry.getValue();
			List<String> procesados = new ArrayList<>();
			// Valida porcentage de giro de acuerdo al agrupamiento por numero de documento
			// del titular
			for (Row fila : listaFilas) {
				String numeroDocumentoOriginal = fila.getCell(4) == null ? null : fila.getCell(4).toString();
				if (numeroDocumentoOriginal == null) continue;
				String numeroDocumento = transformarDocumentoAString(numeroDocumentoOriginal);
				if (numeroDocumento == null || procesados.contains(numeroDocumento))
					continue;
				BigDecimal suma = filtrarRegistros(listaFilas, 4, numeroDocumentoOriginal).stream()
						.map(beneficiario -> BigDecimal.valueOf(beneficiario.getCell(5).getNumericCellValue()))
						.reduce(BigDecimal.ZERO, BigDecimal::add);
				procesados.add(numeroDocumento);
				if(listaFilas.lastIndexOf(fila) == (listaFilas.size()-1))
					esValido &= suma.compareTo(BigDecimal.valueOf((MAXIMO_PORCENTAJE))) == 0;
			}
		}
		return esValido;
	}

	/**
	 * Metodo que permite realizar la agrupalcion de titulres de acuerdo al codigo
	 * de agrupamiento del aarchivo de excel.
	 * 
	 * @param hoja
	 * @return
	 */
	private Map<String, List<Row>> agrupaParticipantes(XSSFSheet hoja) {
		Iterator<Row> rowtIterator = hoja.iterator();
		Map<String, List<Row>> mapaGrupos = new HashedMap<>();
		Set<String> etiquetas = new HashSet<>();
		while (rowtIterator.hasNext()) {
			Row fila = rowtIterator.next();
			if (fila.getCell(0) != null) {
				etiquetas.add(fila.getCell(0).toString());
			} else {
				break;
			}
		}
		Collection<Row> lista = new ArrayList<>();
		hoja.iterator().forEachRemaining(lista::add);
		etiquetas.forEach(etiqueta -> {
			List<Row> listaGrupo = lista.stream()
					.filter(fila -> fila.getCell(fila.getFirstCellNum()).toString().equals(etiqueta))
					.collect(Collectors.toList());
			mapaGrupos.put(etiqueta, listaGrupo);
		});
		return mapaGrupos;
	}

	private List<Row> filtrarRegistros(List<Row> registros, int indiceColumna, String valor) {
		return registros.stream()
				.filter(registro -> registro.getCell(indiceColumna) != null
						&& registro.getCell(indiceColumna).toString().equalsIgnoreCase(valor))
				.collect(Collectors.toList());
	}

	/**
	 * 
	 * @param respuesta
	 * @return
	 */
	private ListaNegociosDTO procesarRespuestaBusquedaNegocios(RespuestaPaginadaConsultaNegocioDTO respuesta) {
		if (respuesta.getResults() == null || respuesta.getResults().isEmpty()) {
			throw new NoContentException();
		}
		List<NegocioDTO> negociosDTO = respuesta.getResults().stream().map(negocio -> obtenerNegocioDTO(negocio))
				.collect(Collectors.toList());
		return new ListaNegociosDTO(respuesta.getLimit() * respuesta.getTotalPages(), negociosDTO);
	}

	/**
	 * 
	 * @param datosNegocio
	 * @return
	 */
	private NegocioDTO obtenerNegocioDTO(RepuestaConsultaNegocioDTO datosNegocio) {

		NegocioDTO negocioDTO = new NegocioDTO();
		negocioDTO.setCodigoInterno(datosNegocio.getCodigo());
		negocioDTO.setCodigoSFC(datosNegocio.getSfc());
		negocioDTO.setNombre(datosNegocio.getNombre());
		negocioDTO.setEstado(datosNegocio.getEstadoNegocio().getId());

		Negocio negocio = obtenerNegocio(datosNegocio.getSfc());

		if (negocio != null) {
			negocioDTO.setCompleto(negocio.getArchivoCargado());
		} else {
			negocioDTO.setCompleto(false);
		}
		return negocioDTO;
	}

	/**
	 * 
	 * @param fechasClave
	 * @return
	 */
	private List<FechaClaveDTO> obtenerFechasClaves(List<FechaClave> fechasClave) {
		if (fechasClave == null) {
			return new ArrayList<>();
		}
		return fechasClave.stream().filter(fecha -> fecha.getActivo()).map(fecha -> entidadAFechaClaveDTO(fecha))
				.collect(Collectors.toList());
	}

	/**
	 * 
	 * @param fecha
	 * @return
	 */
	private FechaClaveDTO entidadAFechaClaveDTO(FechaClave fecha) {
		FechaClaveDTO fechaClaveDTO = new FechaClaveDTO(fecha.getNombre(),
				Utilidades.formatearFecha(fecha.getFechaClavePK().getFecha(), formatoddmmyyyy));
		if (fecha.getTipo() != null) {
			fechaClaveDTO.setIdTipoFechaClave(servicioDominio.buscarValorDominio(fecha.getTipo()).getIdValorDominio());
		}
		return fechaClaveDTO;
	}

	/**
	 * 
	 * @param negocio
	 * @return
	 */
	private List<TipoNegocioCompletoDTO> cargarTiposNegocio(Negocio negocio) {
		return negocio.getTiposNegocio().stream().filter(tipoNegocio -> tipoNegocio.getActivo())
				.map(tipoNegocio -> entidadATipoNegocioDTO(tipoNegocio)).collect(Collectors.toList());
	}

	@Override
	public void guardaInformacionAsambleas(PeticionAsambleasDTO asambleasDTO, String token)
			throws ErrorProcesandoArchivo, ErrorAlmacenandoAsambleas {

		Date fechaCalendario = utilidades.convertirFecha(asambleasDTO.getFechaReunion(),
				"dd/MM/yyyy HH:mm:ss");
		LocalDateTime fechaTime = utilidades.convierteDateToLocalDateTime(fechaCalendario);
		List<AsistenteDTO> listaPersonas = new ArrayList<>();

		try {
			List<Decision> pendientes = asambleasDTO.getTareaPendiente().stream()
					.map(pendiente -> convierteDecisionesAEntidad(pendiente, fechaTime, asambleasDTO.getCodigoSFC(),
							TipoDesicion.PENDIENTE))
					.collect(Collectors.toList());
			List<Decision> decisionesTomadas = asambleasDTO
					.getDecisionTomada().stream().map(decisionTomada -> convierteDecisionesAEntidad(decisionTomada,
							fechaTime, asambleasDTO.getCodigoSFC(), TipoDesicion.DECISIONTOMADA))
					.collect(Collectors.toList());
			decisionesTomadas.addAll(pendientes);
			List<MiembroMesaDirectiva> directivos = asambleasDTO.getMesaDirectiva().stream()
					.map(directivo -> convierteMiembrosAEntidad(directivo, fechaTime, asambleasDTO.getCodigoSFC(),
							TipoAsistente.DIRECTIVO))
					.collect(Collectors.toList());
			List<MiembroMesaDirectiva> expositores = asambleasDTO.getExpositor().stream()
					.map(expositor -> convierteMiembrosAEntidad(expositor, fechaTime, asambleasDTO.getCodigoSFC(),
							TipoAsistente.EXPOSITOR))
					.collect(Collectors.toList());
			directivos.addAll(expositores);

			// Se agrupan las personas seleccionadas desde el front
			listaPersonas.addAll(asambleasDTO.getMesaDirectiva());
			listaPersonas.addAll(asambleasDTO.getExpositor());

			FechaClave fechaClave = servicioFechaClave.guardarFechaClave(fechaCalendario, asambleasDTO.getCodigoSFC(),
					true);
			servicioReunion.guardarReunion(asambleasDTO, fechaClave, directivos, decisionesTomadas, listaPersonas,
					token);
			servicioArchivo.guardarArchivo(utilidades.convierteBase64ToBytes(asambleasDTO.getArchivo()),
					asambleasDTO.getNombreArchivo());
		} catch (IOException e) {
			logger.error("", e);
			throw new ErrorProcesandoArchivo("El archivo no se pudo cargar por " + e.getMessage());
		} catch (Exception e) {
			logger.error("", e);
			throw new ErrorAlmacenandoAsambleas(
					"Fallo el almacenamiento de la informacion de asambleas por " + e.getMessage());
		}

	}

	private Decision convierteDecisionesAEntidad(DecisionDTO decisionDTO, LocalDateTime fecha, String codigoSfc,
			TipoDesicion tipo) {
		Decision decision = new Decision();
		decision.setCodSfc(codigoSfc);
		decision.setFecha(fecha);
		decision.setDescripcion(decisionDTO.getDescripcion());
		decision.setEstado(TipoDesicion.DECISIONTOMADA.equals(tipo));
		decision.setResponsable(decisionDTO.getResponsable());
		decision.setQuorumAprobatorio(decisionDTO.getQuorumAprobatorio());
		return decision;
	}

	private MiembroMesaDirectiva convierteMiembrosAEntidad(AsistenteDTO asistente, LocalDateTime fecha,
			String codigoSfc, TipoAsistente tipo) {
		MiembroMesaDirectiva miembroMesaDirectiva = new MiembroMesaDirectiva();
		miembroMesaDirectiva.setCodigoSfc(codigoSfc);
		if (TipoAsistente.EXPOSITOR.equals(tipo)) {
			miembroMesaDirectiva.setDirectivo(false);
			miembroMesaDirectiva.setExpositor(true);
		} else {
			miembroMesaDirectiva.setDirectivo(true);
			miembroMesaDirectiva.setExpositor(false);
		}
		miembroMesaDirectiva.setNumeroDocumento(asistente.getNumeroDocumento());
		miembroMesaDirectiva.setTipoDocumento(asistente.getTipoDocumento());
		miembroMesaDirectiva.setFecha(fecha);
		miembroMesaDirectiva.setIdRol(servicioDominio.buscarPorCodigoDominioYValor
				(codDominioRolTercero, asistente.getRol()).getIdValorDominio().toString());
		return miembroMesaDirectiva;
	}

	/**
	 * 
	 * @param tipoNegocio
	 * @return
	 */
	private TipoNegocioCompletoDTO entidadATipoNegocioDTO(TipoNegocio tipoNegocio) {
		TipoNegocioCompletoDTO tipoNegocioCompletoDTO = new TipoNegocioCompletoDTO();
		tipoNegocioCompletoDTO.setId(tipoNegocio.getTipoNegocioPK().getTipoNegocio());
		tipoNegocioCompletoDTO.setDescripcion(
				servicioDominio.buscarValorDominio(tipoNegocio.getTipoNegocioPK().getTipoNegocio()).getDescripcion());
		tipoNegocioCompletoDTO.setRetenciones(obtenerRetencionesTipoNegocio(tipoNegocio.getRetencionesTipoNegocio()));
		obtenerGastosTiposNegocio(tipoNegocio, tipoNegocioCompletoDTO);
		return tipoNegocioCompletoDTO;
	}

	/**
	 * 
	 * @param retenciones
	 * @return
	 */
	private List<DatosBasicosValorDominioDTO> obtenerRetencionesTipoNegocio(List<RetencionTipoNegocio> retenciones) {
		return retenciones.stream().filter(retencion -> retencion.getActivo())
				.map(retencion -> obtenerRetencion(retencion.getRetencionTipoNegocioPK().getIdRetencion()))
				.collect(Collectors.toList());
	}

	/**
	 * 
	 * @param idRetencion
	 * @return
	 */
	private DatosBasicosValorDominioDTO obtenerRetencion(Integer idRetencion) {
		ValorDominioNegocio retencion = servicioDominio.buscarValorDominio(idRetencion);
		DatosBasicosValorDominioDTO datosBasicosValorDominioDTO = new DatosBasicosValorDominioDTO();
		if (retencion != null) {
			datosBasicosValorDominioDTO.setId(retencion.getIdValorDominio());
			datosBasicosValorDominioDTO.setDescripcion(retencion.getDescripcion());
			datosBasicosValorDominioDTO.setUnidad(retencion.getUnidad());
			datosBasicosValorDominioDTO.setValor(retencion.getValor());
		}
		return datosBasicosValorDominioDTO;
	}

	/**
	 * 
	 * @param tipoNegocio
	 * @return
	 */
	private void obtenerGastosTiposNegocio(TipoNegocio tipoNegocio, TipoNegocioCompletoDTO tipoNegocioDTO) {
		tipoNegocioDTO.setGastos(new ArrayList<>());
		tipoNegocioDTO.setTerceros(new ArrayList<>());
		servicioObligacionNegocio.buscarObligacionesTipoNegocio(tipoNegocio).forEach(obligacion -> {
			if(obligacion.getTerceroNumeroDocumento() == null) {
				tipoNegocioDTO.getGastos().add(entidadAGastosDTO(obligacion));
			} else {
				tipoNegocioDTO.getTerceros().add(entidadAterceroDTO(obligacion));
			}
		});
	}

	/**
	 * 
	 * @param obligacion
	 * @return
	 */
	private GastoDTO entidadAGastosDTO(ObligacionNegocio obligacion) {
		GastoDTO gastoDTO = new GastoDTO();
		gastoDTO.setDiaPago(obligacion.getDiaPago());
		gastoDTO.setNumeroRadicado(obligacion.getNumeroRadicado());
		gastoDTO.setNombre(obligacion.getObligacionNegocioPK().getNombreGasto());
		gastoDTO.setPeriodicidad(
				obligacion.getPeriodicidad() != null ? Arrays.stream(obligacion.getPeriodicidad().split(","))
						.map(Integer::parseInt).collect(Collectors.toList()) : null);
		gastoDTO.setIdTipo(servicioDominio.buscarValorDominio(obligacion.getObligacionNegocioPK().getTipoPago())
				.getIdValorDominio());
		gastoDTO.setUnidad(obligacion.getUnidad());
		gastoDTO.setValor(obligacion.getValor().floatValue());
		gastoDTO.setFechaVencimiento(Utilidades.formatearFecha(obligacion.getFechaVencimiento(), formatoddmmyyyy));
		return gastoDTO;
	}

	/**
	 * 
	 * @param codigoSFC
	 * @return
	 */
	private ConstantesPagoDTO cargarConstantesNegocio(String codigoSFC) {
		ConstantesPago constantesPago = servicioConstantesPago.buscarConstantesPagoNegocio(codigoSFC);
		ConstantesPagoDTO constantesPagoDTO = new ConstantesPagoDTO();
		if (constantesPago != null) {
			constantesPagoDTO.setCodigoCiudad(constantesPago.getCodigoCiudad());
			constantesPagoDTO.setCodigoDepartamento(constantesPago.getCodigoDepartamento());
			constantesPagoDTO.setCodigoPais(constantesPago.getCodigoPais());
			constantesPagoDTO.setMovimientoPago(constantesPago.getMovimiento());
			constantesPagoDTO.setSubtipoFideicomiso(constantesPago.getTipoSubtipoFideicomiso());
			constantesPagoDTO.setTipoFideicomiso(constantesPago.getTipoFideicomiso());
			constantesPagoDTO.setTipoMovimiento(constantesPago.getTipoMovNegocio());
			constantesPagoDTO.setTipoProceso(constantesPago.getTipoProceso());
		}
		return constantesPagoDTO;
	}

	/**
	 * 
	 * @param obligacion
	 * @return
	 */
	private TerceroDTO entidadAterceroDTO(ObligacionNegocio obligacion) {
		TerceroDTO tercero = new TerceroDTO();
		tercero.setNombre(obligacion.getObligacionNegocioPK().getNombreGasto());
		tercero.setTipoDocumento(obligacion.getTerceroTipoDocumento());
		tercero.setNumeroDocumento(obligacion.getTerceroNumeroDocumento());
		tercero.setIdTipo(obligacion.getObligacionNegocioPK().getTipoPago());
		tercero.setUnidad(obligacion.getUnidad());
		tercero.setValor(obligacion.getValor().floatValue());
		tercero.setDiaPago(obligacion.getDiaPago());
		tercero.setFechaVencimiento(Utilidades.formatearFecha(obligacion.getFechaVencimiento(), formatoddmmyyyy));
		tercero.setNumeroRadicado(obligacion.getNumeroRadicado());
		tercero.setPeriodicidad(
				obligacion.getPeriodicidad() != null ? Arrays.stream(obligacion.getPeriodicidad().split(","))
						.map(Integer::parseInt).collect(Collectors.toList()) : null);
		return tercero;
	}

	/**
	 * 
	 * @param negocioCompletoDTO
	 * @param negocio
	 */
	private void guardarFechasClave(NegocioCompletoDTO negocioCompletoDTO, Negocio negocio) {
		List<FechaClave> fechasClave = negocioCompletoDTO.getFechasClaves().stream()
				.map(fechaClave -> dtoAFechaClave(fechaClave, negocio)).collect(Collectors.toList());

		List<FechaClave> fechasClaveActuales = servicioFechaClave.buscarFechasClaveNegocio(negocio);

		fechasClaveActuales.forEach(fechaActual -> {
			if (!fechasClave.contains(fechaActual)) {
				fechaActual.setActivo(Boolean.FALSE);
				servicioFechaClave.guardarFechaClave(fechaActual);
			}
		});
		fechasClave.forEach(fecha -> servicioFechaClave.guardarFechaClave(fecha));
	}

	/**
	 * 
	 * @param fechaClaveDTO
	 * @param negocio
	 * @return
	 */
	private FechaClave dtoAFechaClave(FechaClaveDTO fechaClaveDTO, Negocio negocio) {
		FechaClave fechaClave = new FechaClave(negocio.getCodSfc(),
				utilidades.convertirFecha(fechaClaveDTO.getFechaCalendario(), formatoddmmyyyy));
		fechaClave.setTipo(fechaClaveDTO.getIdTipoFechaClave());
		fechaClave.setNombre(fechaClaveDTO.getNombreFechaClave());
		LocalDate localDate = fechaClave.getFechaClavePK().getFecha().toInstant().atZone(ZoneId.systemDefault())
				.toLocalDate();
		fechaClave.setFechaDia(localDate.getDayOfMonth());
		fechaClave.setFechaMes(localDate.getMonthValue());
		fechaClave.setActivo(Boolean.TRUE);
		return fechaClave;
	}

	/**
	 * 
	 * @param encargosDTO
	 * @param negocio
	 */
	private void guardarEncargos(List<EncargoNegocioDTO> encargosDTO, Negocio negocio) {
		encargosDTO.stream().map(encargoDTO -> servicioEncargo.guardarEncargo(encargoDTOAEntidad(encargoDTO, negocio)))
				.collect(Collectors.toList());
	}

	/**
	 * 
	 * @param constanteNegocioDTO
	 * @param codigoSFC
	 * @return
	 */
	private ConstantesPago guardarConstantesPago(ConstantesPagoDTO constanteNegocioDTO, String codigoSFC) {
		ConstantesPago constantesPago = new ConstantesPago();
		constantesPago.setCodigoCiudad(constanteNegocioDTO.getCodigoCiudad());
		constantesPago.setCodigoDepartamento(constanteNegocioDTO.getCodigoDepartamento());
		constantesPago.setCodigoPais(constanteNegocioDTO.getCodigoPais());
		constantesPago.setCodSfc(codigoSFC);
		constantesPago.setMovimiento(constanteNegocioDTO.getMovimientoPago());
		constantesPago.setTipoFideicomiso(constanteNegocioDTO.getTipoFideicomiso());
		constantesPago.setTipoMovNegocio(constanteNegocioDTO.getTipoMovimiento());
		constantesPago.setTipoProceso(constanteNegocioDTO.getTipoProceso());
		constantesPago.setTipoSubtipoFideicomiso(constanteNegocioDTO.getSubtipoFideicomiso());
		return servicioConstantesPago.guardarConstantesPago(constantesPago);
	}
	
	/**
	 * Método que busca los responsables de un negocio y los guarda en la entidad
	 * Responsables Notificación.
	 * 
	 * @author efarias
	 * @param codigoSFC
	 * @param tokenAutorizacion
	 */
	private void guardarEquipoResponsable(String codigoSFC, String tokenAutorizacion) {
		List<RespuestaAccionEquipoResponsableDTO> listaResponsablesNegocio = obtenerEquipoResponsable(codigoSFC,
				tokenAutorizacion);
		if (!listaResponsablesNegocio.isEmpty()) {
			listaResponsablesNegocio.forEach(responsable -> {
				guardarResponsableNotificacion(responsable, codigoSFC);
			});
		}
	}

	
	/**
	 * Método que guarda acda uno de los responsables asociados a un negocio.
	 * 
	 * @author efarias
	 * @param responsable
	 * @param codigoSFC
	 */
	private void guardarResponsableNotificacion(RespuestaAccionEquipoResponsableDTO responsable, String codigoSFC) {
		ResponsableNotificacionPK responsablePK = new ResponsableNotificacionPK(codigoSFC,
				responsable.getInformacion().getTipoDocumento().getDescripcion(),
				responsable.getInformacion().getNumeroDocumento());
		ResponsableNotificacion responsableNeg = new ResponsableNotificacion(responsablePK,
				responsable.getInformacion().getNombreCompleto(), responsable.getTipoResponsable(),
				responsable.getInformacion().getTelefono(), responsable.getInformacion().getEmail(), Boolean.TRUE);
		servicioResponsablesNotificacion.guardarResponsable(responsableNeg);
	}

	/**
	 * 
	 * @param encargoDTO
	 * @param negocio
	 * @return
	 */
	private Encargo encargoDTOAEntidad(EncargoNegocioDTO encargoDTO, Negocio negocio) {
		Encargo encargo = new Encargo(negocio.getCodSfc(), encargoDTO.getNumeroEncargo());
		encargo.setEstado(encargoDTO.getEstado());
		MedioPago medioPago = guardarMedioPago(encargoDTO);
		encargo.setMedioPago(medioPago);
		encargo.setBanco(medioPago.getMedioPagoPK().getBanco());
		encargo.setTipoCuenta(medioPago.getMedioPagoPK().getTipoCuenta());
		encargo.setMovimientoFondo(encargoDTO.getMovimientoFondo());
		encargo.setNegocio(negocio);
		encargo.setNumeroPagoOrdinario(encargoDTO.getNumeroPagoOrdinario());
		return encargo;
	}

	/**
	 * 
	 * @param encargoDTO
	 * @return
	 */
	private MedioPago guardarMedioPago(EncargoNegocioDTO encargoDTO) {
		Integer idBancoTraslado = servicioDominio
				.buscarPorCodigoDominioYValor(codigoDominioBanco, codigoTipoBancoTraslado).getIdValorDominio();
		Integer idTipoCuentaTraslado = servicioDominio
				.buscarPorCodigoDominioYValor(codigoDominioTipoCuenta, codigoTipoCuentaTraslado).getIdValorDominio();
		return servicioMedioPago
				.guardarMedioPago(new MedioPago(idBancoTraslado, encargoDTO.getNumeroEncargo(), idTipoCuentaTraslado));
	}

	/**
	 * 
	 * @param negocioCompletoDTO
	 * @param negocio
	 */
	private void guardarTiposNegocio(NegocioCompletoDTO negocioCompletoDTO, Negocio negocio) {

		List<Titular> titulares = servicioTitular.buscarTitularesPorCodigoNegocio(negocio.getCodSfc());
		
		List<TipoNegocio> tiposNegocios = negocioCompletoDTO.getTiposNegocio().stream().map(
				tipoNegocio -> new TipoNegocio(tipoNegocio.getId(), negocioCompletoDTO.getCodigoSFC(), Boolean.TRUE))
				.collect(Collectors.toList());

		List<TipoNegocio> tiposNegociosActuales = servicioTipoNegocio.buscarTiposNegocio(negocio);

		tiposNegociosActuales.forEach(tipoActual -> {
			if (!tiposNegocios.contains(tipoActual)) {
				tipoActual.setActivo(Boolean.FALSE);
				titulares.forEach(titular -> titular.getRetencionTitularList().stream()
						.filter(retencionTitular -> retencionTitular.getRetencionTitularPK()
								.getTipoNaturalezaNegocio().equals(tipoActual.getTipoNegocioPK().getTipoNegocio()))
						.forEach(retencionTitular -> {
							retencionTitular.setActivo(Boolean.FALSE);
							servicioRetencionTitular.guardarRetencionTitular(retencionTitular);
						}));
				servicioTipoNegocio.guardarTipoNegocio(tipoActual);
			}
		});

		tiposNegocios.forEach(tipoNegocio -> servicioTipoNegocio.guardarTipoNegocio(tipoNegocio));

		negocioCompletoDTO.getTiposNegocio().forEach(tipoNegocio -> {
			guardarObligacionesNegocio(tipoNegocio, negocioCompletoDTO.getCodigoSFC());
			guardarRetencionesTipoNegocio(tipoNegocio.getId(), negocioCompletoDTO.getCodigoSFC(),
					tipoNegocio.getRetenciones());
		});
	}

	/**
	 * 
	 * @param idTipoNegocio
	 * @param codigoSfc
	 * @param retencionesDTO
	 */
	private void guardarRetencionesTipoNegocio(Integer idTipoNegocio, String codigoSfc,
			List<DatosBasicosValorDominioDTO> retencionesDTO) {

		List<RetencionTipoNegocio> retencionesNegocio = retencionesDTO.stream().map(
				retencioDTO -> new RetencionTipoNegocio(retencioDTO.getId(), idTipoNegocio, codigoSfc, Boolean.TRUE))
				.collect(Collectors.toList());

		List<RetencionTipoNegocio> retencionesNegocioActuales = servicioRetencionTipoNegocio
				.buscarRetencionesTipoNegocio(new TipoNegocio(idTipoNegocio, codigoSfc));

		retencionesNegocioActuales.forEach(retencionActual -> {
			if (!retencionesNegocio.contains(retencionActual)) {
				retencionActual.setActivo(Boolean.FALSE);
				servicioRetencionTipoNegocio.guardarRetencionTipoNegocio(retencionActual);
			}
		});

		List<RetencionTitular> retencionTitulares = servicioRetencionTitular
				.buscarRetencionesTitularPorCodigoNegocioYTipoNegocio(codigoSfc, idTipoNegocio);

		List<RetencionTipoNegocio> retencionesActualesTitulares = new ArrayList<>();
		List<Titular> titulares = new ArrayList<>();

		retencionTitulares.forEach(retencionTitular -> {
			retencionesActualesTitulares.add(new RetencionTipoNegocio(
					retencionTitular.getRetencionTitularPK().getIdRetencion(), idTipoNegocio, codigoSfc, Boolean.TRUE));
			if (retencionTitular.getTitular()!= null && !titulares.contains(retencionTitular.getTitular())) {
				titulares.add(retencionTitular.getTitular());
			}
			
		});

		retencionesActualesTitulares.forEach(retencionActualTitular -> {
			if (!retencionesNegocio.contains(retencionActualTitular)) {
				retencionTitulares.forEach(retencionTitular -> {
					if (retencionTitular.getRetencionTitularPK().getIdRetencion()
							.equals(retencionActualTitular.getRetencionTipoNegocioPK().getIdRetencion())) {
						retencionTitular.setActivo(Boolean.FALSE);
						servicioRetencionTitular.guardarRetencionTitular(retencionTitular);
					}
				});

			}
		});

		retencionesNegocio.forEach(retencion -> {
			servicioRetencionTipoNegocio.guardarRetencionTipoNegocio(retencion);
			titulares.forEach(titular -> servicioRetencionTitular.guardarRetencionTitular(
					new RetencionTitular(retencion.getRetencionTipoNegocioPK().getIdRetencion(), codigoSfc,
							titular.getTitularPK().getNumeroDocumento(), titular.getTitularPK().getTipoDocumento(),
							Boolean.TRUE, idTipoNegocio)));
		});

	}

	/**
	 * 
	 * @param tipoNegocio
	 * @param codigoSfc
	 * @param idBancoTraslado
	 * @param idTipoCuentaTraslado
	 */
	private void guardarObligacionesNegocio(TipoNegocioCompletoDTO tipoNegocio, String codigoSfc) {

		List<ObligacionNegocio> obligaciones = tipoNegocio.getGastos().stream()
				.map(gasto -> gastoDtoAObligacionNegocio(gasto, tipoNegocio.getId(), codigoSfc))
				.collect(Collectors.toList());

		obligaciones.addAll(tipoNegocio.getTerceros().stream()
				.map(tercero -> terceroDtoAObligacionNegocio(tercero, tipoNegocio.getId(), codigoSfc))
				.collect(Collectors.toList()));

		List<ObligacionNegocio> obligacionesActuales = servicioObligacionNegocio
				.buscarObligacionesTipoNegocio(new TipoNegocio(tipoNegocio.getId(), codigoSfc));

		obligacionesActuales.forEach(obligacionActual -> {
			if (!obligaciones.contains(obligacionActual)) {
				obligacionActual.setActivo(Boolean.FALSE);
			}
		});

		obligaciones.forEach(obligacion -> servicioObligacionNegocio.guardarObligacionNegocio(obligacion));
	}

	/**
	 * 
	 * @param tercero
	 * @param idTipoNegocio
	 * @param codigoSfc
	 * @return
	 */
	private ObligacionNegocio terceroDtoAObligacionNegocio(TerceroDTO tercero, Integer idTipoNegocio,
			String codigoSfc) {
		ObligacionNegocio obligacionNegocio = new ObligacionNegocio(tercero.getIdTipo(), idTipoNegocio, codigoSfc,
				tercero.getNombre());
		obligacionNegocio.setTerceroTipoDocumento(tercero.getTipoDocumento());
		obligacionNegocio.setTerceroNumeroDocumento(tercero.getNumeroDocumento());
		obligacionNegocio.setUnidad(tercero.getUnidad());
		obligacionNegocio.setValor(BigDecimal.valueOf(tercero.getValor()));
		obligacionNegocio.setDiaPago(tercero.getDiaPago());
		obligacionNegocio.setNumeroRadicado(tercero.getNumeroRadicado());
		obligacionNegocio.setPeriodicidad(
				tercero.getPeriodicidad().stream().map(String::valueOf).collect(Collectors.joining(",")));
		obligacionNegocio.setActivo(Boolean.TRUE);
		obligacionNegocio
				.setFechaVencimiento(utilidades.convertirFecha(tercero.getFechaVencimiento(), formatoddmmyyyy));
		return obligacionNegocio;
	}

	/**
	 * 
	 * @param gasto
	 * @param idTipoNegocio
	 * @param codigoSfc
	 * @param idBancoTraslado
	 * @param idTipoCuentaTraslado
	 * @return
	 */
	private ObligacionNegocio gastoDtoAObligacionNegocio(GastoDTO gasto, Integer idTipoNegocio, String codigoSfc) {
		ObligacionNegocio obligacionNegocio = new ObligacionNegocio(gasto.getIdTipo(), idTipoNegocio, codigoSfc,
				gasto.getNombre());
		obligacionNegocio.setUnidad(gasto.getUnidad());
		obligacionNegocio.setValor(BigDecimal.valueOf(gasto.getValor()));
		obligacionNegocio.setDiaPago(gasto.getDiaPago());
		obligacionNegocio.setNumeroRadicado(gasto.getNumeroRadicado());
		obligacionNegocio.setPeriodicidad(
				gasto.getPeriodicidad().stream().map(String::valueOf).collect(Collectors.joining(",")));
		obligacionNegocio.setFechaVencimiento(utilidades.convertirFecha(gasto.getFechaVencimiento(),formatoddmmyyyy));
		obligacionNegocio.setActivo(Boolean.TRUE);
		return obligacionNegocio;
	}

	public static String transformarDocumentoAString(String celdaDocumento) {

		String documento;
		if (celdaDocumento.contains(".")) {
			documento = String.format("%.0f", Double.parseDouble(celdaDocumento));
		} else if (celdaDocumento.contains(",")) {
			documento = String.format("%,0f", Double.parseDouble(celdaDocumento));
		} else {
			documento = celdaDocumento;
		}
		return documento;
	}

	@Override
	public InformacionGeneralPagosDTO obtenerPagosNegocio(PeticionInformacionGeneralPagosDTO peticion,
			String tokenAutorizacion) throws Exception {
		InformacionGeneralPagosDTO pagosNegocio = new InformacionGeneralPagosDTO();
		Specification<PagoNegocio> especificacionCombinada = Specification
				.where(PagoNegocioEspecificaciones.filtrarPorNegocio(peticion.getCodigoNegocio()))
				.and(PagoNegocioEspecificaciones
						.filtrarPorEstadoDistribucion(EstadosDistribucion.DISTRIBUIDO.getValor()))
				.and(peticion.getRangoFecha().isEmpty() ? null
						: PagoNegocioEspecificaciones.filtrarPorPeriodo(utilidades.obtenerFechasEntreRango(
								peticion.getRangoFecha().get(0), peticion.getRangoFecha().get(1))))
				.and(peticion.getCodigoTipoNegocio().isEmpty() ? null
						: PagoNegocioEspecificaciones.filtrarPorTipoNegocio(peticion.getCodigoTipoNegocio()))
				.and(peticion.getNumeroEncargo().isEmpty() ? null
						: PagoNegocioEspecificaciones.filtrarPorEncargo(peticion.getNumeroEncargo()));

		List<PagoDistribuidoDTO> pagos;
		int cantidadRegistros = (int) repositorioPagoNegocio.count(especificacionCombinada);
		if (peticion.getElementos() != 0 && peticion.getPagina() != 0) {
			// Asignamos la paginacion de la consulta
			Pageable rango = PageRequest.of(peticion.getPagina() - 1, peticion.getElementos());
			Page<PagoNegocio> resultados = repositorioPagoNegocio.findAll(especificacionCombinada, rango);
			// Transformamos la informacion obtenida en el dto de respuesta.
			pagos = resultados.stream().map(pago -> transformarHistorialPagosNegocio(pago))
					.collect(Collectors.toList());
		} else {
			List<PagoNegocio> resultados = repositorioPagoNegocio.findAll(especificacionCombinada);
			pagos = resultados.stream().map(pago -> transformarHistorialPagosNegocio(pago))
					.collect(Collectors.toList());
		}
		pagosNegocio.setHistorialPagos(pagos);
		pagosNegocio.setTotalRegistros(cantidadRegistros);
		return pagosNegocio;
	}

	private PagoDistribuidoDTO transformarHistorialPagosNegocio(PagoNegocio pago) {
		PagoDistribuidoDTO pagoDistribuido = new PagoDistribuidoDTO();
		int tipoNegocio = pago.getTipoNegocio().getTipoNegocioPK().getTipoNegocio();
		pagoDistribuido.setTipoNegocio(servicioDominio.buscarValorDominio(tipoNegocio).getValor());
		BigDecimal totalDistribuido = pago.getDistribuciones().stream().filter(distribucion->distribucion.getEstado().equals(distribuido)).map(Distribucion::getTotalDistribucion).reduce(BigDecimal.ZERO, BigDecimal::add);
		pagoDistribuido.setValorDistribuido(totalDistribuido);
		pagoDistribuido.setEncargoDebitado(pago.getEncargo().getEncargoPK().getCuenta());
		BigDecimal totalIngresos = pago.getIngresosOperacion().stream().map(IngresoOperacion::getValor).reduce(BigDecimal.ZERO, BigDecimal::add);
		pagoDistribuido.setValorIngreso(totalIngresos);
		pagoDistribuido.setPeriodo(pago.getPagoNegocioPK().getPeriodo());
		return pagoDistribuido;
	}

	@Override
	public RespuestaDetalleBeneficiarioPagoDTO obtenerBeneficiariosPorPago(String codigoNegocio, String tipoNegocio,
			String periodo) {
		Integer idTipoNegocio = servicioDominio.buscarPorCodigoDominioYValor("TIPNEG", tipoNegocio).getIdValorDominio();
		List<Distribucion> distribuciones = repositorioDistribucon.buscarDistribucionPorEstado(codigoNegocio,
				idTipoNegocio, periodo, EstadosDistribucion.DISTRIBUIDO.getValor());
		RespuestaDetalleBeneficiarioPagoDTO detalleBeneficiarios = new RespuestaDetalleBeneficiarioPagoDTO();
		int cantidadRegistros = 0;
		List<TitularPagoDto> titulares = new ArrayList<>();
		distribuciones.forEach(distribucion -> distribucion.getDistribucionBeneficiarios()
				.forEach(distribucionBeneficiario -> transformarDetalleDistribucionesADTO(distribucionBeneficiario,
						titulares, cantidadRegistros)));
		detalleBeneficiarios.setListaDeBeneficiarios(titulares);
		detalleBeneficiarios.setTotalRegistros(cantidadRegistros);
		return detalleBeneficiarios;
	}

	/**
	 * Metodo que permite obtener un archivo de excel con el historial de pagos
	 * 
	 * @param peticion Datos de diligenciamiento del archivo.
	 * @return RespuestaArchivoDTO Dto que contiene el archivo en bytes generado.
	 */
	@Override
	public RespuestaArchivoDTO obtenerArchivoDetallePagos(String codigoNegocio, String tipoNegocio, String periodo)
			throws Exception {
		RespuestaDetalleBeneficiarioPagoDTO respuesta = obtenerBeneficiariosPorPago(codigoNegocio, tipoNegocio,
				periodo);
		List<String> etiquetas = new ArrayList<>();
		Field[] camposTitular = respuesta.getListaDeBeneficiarios().get(0).getClass().getDeclaredFields();
		for (int i = 0; i < camposTitular.length; i++) {
			if (ResolvableType.forField(camposTitular[i]).asCollection() == ResolvableType.NONE)
				etiquetas.add(camposTitular[i].getName().toUpperCase());
		}
		Field[] camposBeneficiario = respuesta.getListaDeBeneficiarios().get(0).getBeneficiarios().get(0).getClass()
				.getDeclaredFields();
		for (int i = 0; i < camposBeneficiario.length; i++) {
			etiquetas.add(camposBeneficiario[i].getName().toUpperCase());
		}
		return servicioArchivo.construirArchivoExcel(respuesta.getListaDeBeneficiarios(), etiquetas,
				TipoExtensionArchivo.EXCEL);
	}

	private void transformarDetalleDistribucionesADTO(DistribucionBeneficiario distribucionBeneficiario,
			List<TitularPagoDto> titulares, int cantidadRegistros) {
		Titular titular = distribucionBeneficiario.getBeneficiario().getTitular();
		Optional<TitularPagoDto> titularRegistrado = buscaTitularRegistrado(titulares,
				titular.getTitularPK().getNumeroDocumento());
		if (titularRegistrado.isPresent()) {
			titularRegistrado.get().getBeneficiarios()
					.add(servicioBeneficiario.procesarHistorialPagos(distribucionBeneficiario));
		} else {
			TitularPagoDto titularPagoDto = new TitularPagoDto();
			titularPagoDto.setNombreTitular(titular.getPersona().getNombreCompleto());
			titularPagoDto.setNumeroDerechoFiduciarios(titular.getNumeroDerechos());
			titularPagoDto.setNumeroDocumentoTitular(titular.getTitularPK().getNumeroDocumento());
			titularPagoDto.setTipoDocumentoTitular(titular.getTitularPK().getTipoDocumento());
			titularPagoDto.setPorcentajeParticipacionTitular(
					titular.getPorcentaje() != null ? titular.getPorcentaje().floatValue() : 0);
			titularPagoDto.setBeneficiarios(new ArrayList<>());
			titularPagoDto.getBeneficiarios()
					.add(servicioBeneficiario.procesarHistorialPagos(distribucionBeneficiario));
			titulares.add(titularPagoDto);
		}
		cantidadRegistros += 1;
	}

	private Optional<TitularPagoDto> buscaTitularRegistrado(List<TitularPagoDto> titulares,
			String numeroDocumentoTitular) {
		return titulares.stream()
				.filter(titularDto -> numeroDocumentoTitular.equals(titularDto.getNumeroDocumentoTitular()))
				.findFirst();
	}

	private List<String> validarConstatesNegocio(String codigoNegocio) {
		ConstantesPago constantes = servicioConstantesPago.buscarConstantesPagoNegocio(codigoNegocio);

		List<String> constantesIncompletas = new ArrayList<>();

		if (constantes.getCodigoCiudad() == null || constantes.getCodigoCiudad().isEmpty()) {
			constantesIncompletas.add(constanteNegocioCodigoCiudad);
		}
		if (constantes.getCodigoDepartamento() == null || constantes.getCodigoDepartamento().isEmpty()) {
			constantesIncompletas.add(constanteNegocioCodigoDepartamento);
		}
		if (constantes.getCodigoPais() == null || constantes.getCodigoPais().isEmpty()) {
			constantesIncompletas.add(constanteNegocioCodigoPais);
		}
		if (constantes.getMovimiento() == null || constantes.getMovimiento().isEmpty()) {
			constantesIncompletas.add(constanteNegocioMovimiento);
		}
		if (constantes.getTipoFideicomiso() == null || constantes.getTipoFideicomiso().isEmpty()) {
			constantesIncompletas.add(constanteNegocioTipoFideicomiso);
		}
		if (constantes.getTipoMovNegocio() == null || constantes.getTipoMovNegocio().isEmpty()) {
			constantesIncompletas.add(constanteNegocioTipoMovimientoNegocio);
		}
		if (constantes.getTipoProceso() == null || constantes.getTipoProceso().isEmpty()) {
			constantesIncompletas.add(constanteNegocioTipoProceso);
		}
		if (constantes.getTipoSubtipoFideicomiso() == null || constantes.getTipoSubtipoFideicomiso().isEmpty()) {
			constantesIncompletas.add(constanteNegocioSubtipoFideicomiso);
		}
		return constantesIncompletas;
	}

	/**
	 * 
	 * @param titular
	 * @return
	 */
	private InformacionTitularesIncompletaDTO verificarInformacionFinancieraTitular(Titular titular) {
		InformacionTitularesIncompletaDTO informacionTitularesIncompletaDTO = null;
		List<String> datosFaltantes = new ArrayList<>();

		if (titular.getIdTipoNaturalezaJuridica() == null) {
			datosFaltantes.add(titularSinNaturalezaJuridica);
		}

		if (titular.getBeneficiarios() == null || titular.getBeneficiarios().isEmpty()
				|| (titular.getBeneficiarios() != null && titular.getBeneficiarios().stream()
						.allMatch(beneficiario -> beneficiario.getEstado().equals(Boolean.FALSE)))) {
			datosFaltantes.add(titularSinBeneficiarios);

		} else {
			if (titular.getBeneficiarios().stream().anyMatch(beneficiario -> beneficiario.getMedioPagoPersona() == null
					|| beneficiario.getMedioPagoPersona().getEstado().equals(Boolean.FALSE))) {
				datosFaltantes.add(titularBeneficiariosSinMediosPago);
			}

			if (titular.getBeneficiarios().stream().filter(beneficiario -> beneficiario.getEstado())
					.map(Beneficiario::getPorcentajeGiro)
					.reduce(BigDecimal.ZERO, BigDecimal::add).compareTo(BigDecimal.valueOf(100)) != 0) {
				datosFaltantes.add(titularSumaPorcentajesGiroDiferenteCien);
			}

		}

		if (!datosFaltantes.isEmpty()) {
			informacionTitularesIncompletaDTO = new InformacionTitularesIncompletaDTO();
			informacionTitularesIncompletaDTO.setNombreCompleto(titular.getPersona().getNombreCompleto());
			informacionTitularesIncompletaDTO
					.setNumeroDocumento(titular.getPersona().getPersonaPK().getNumeroDocumento());
			informacionTitularesIncompletaDTO.setTipoDocumento(titular.getPersona().getPersonaPK().getTipoDocumento());
			informacionTitularesIncompletaDTO.setDatosFaltantes(datosFaltantes);

		}
		return informacionTitularesIncompletaDTO;
	}

	/**
	 * 
	 * @param negocio
	 * @param tokenAutorizacion
	 * @return
	 */
	private ResultadoSincronizacionTitularesYEncargosDTO sincronizarDatosNegocio(Negocio negocio,
			String tokenAutorizacion) {

		boolean sincronizarBus = Boolean
				.parseBoolean(servicioParametroSistema.obtenerValorParametro(paramIdActivarSincronizacion));
		ResultadoSincronizacionTitularesYEncargosDTO resultadoSincronizacionTitularesYEncargosDTO = new ResultadoSincronizacionTitularesYEncargosDTO();

		boolean negocioAct = negocio.getEstado().equalsIgnoreCase(negocioVigente);
		
		if (sincronizarBus) {

			int minutosSincronizacionBus = Integer
					.parseInt(servicioParametroSistema.obtenerValorParametro(paramIdMinutosSincronizacionBus));

			long minutosDesdeUltimaSincronizacion = 0;

			if (negocio.getFechaSincronizacionBus() != null) {
				long difMilisegundos = Math.abs(new Date().getTime() - negocio.getFechaSincronizacionBus().getTime());
				minutosDesdeUltimaSincronizacion = TimeUnit.MINUTES.convert(difMilisegundos, TimeUnit.MILLISECONDS);
			}

			if (negocio.getFechaSincronizacionBus() == null
					|| (minutosDesdeUltimaSincronizacion > minutosSincronizacionBus)) {
				negocioAct = sincronizarEstadoNegocio(negocio, tokenAutorizacion);
				if(negocioAct) {
					resultadoSincronizacionTitularesYEncargosDTO = sincronizarTitulares(negocio, negocio.getFechaSincronizacionBus(), tokenAutorizacion);
					resultadoSincronizacionTitularesYEncargosDTO
							.setTieneEncargosNuevos(sincronizarEncargos(negocio, tokenAutorizacion));
					// Sincronización Equipo Responsable
					sincronizarEquipoResponsable(negocio, tokenAutorizacion);
					negocio.setFechaSincronizacionBus(new Date());
					repositorioNegocio.save(negocio);
				}
				
			}
		}
		resultadoSincronizacionTitularesYEncargosDTO.setNegocioActivo(negocioAct);
		return resultadoSincronizacionTitularesYEncargosDTO;
	}

	/**
	 * Método que sincroniza los datos del equipo responsable asociado a un negocio.
	 * 
	 * @author efarias
	 * @param negocio
	 * @param tokenAutorizacion
	 */
	private void sincronizarEquipoResponsable(Negocio negocio, String tokenAutorizacion) {
		List<RespuestaAccionEquipoResponsableDTO> listaResponsablesNegocio = obtenerEquipoResponsable(
				negocio.getCodSfc(), tokenAutorizacion);

		if (!listaResponsablesNegocio.isEmpty()) {
			// Crear Actualizar Equipo Responsable
			crearActualizarEquipoResponsable(negocio, listaResponsablesNegocio);
			// Inactivar / Actualizar responsables no existentes.
			inactivarActualizarEquipoResponsable(negocio, listaResponsablesNegocio);
		} else {
			/*
			 * Si no retorna ningún resultado, se inactivan todos los responsables del
			 * negocio dado que oficialmente no existe ninguno.
			 */
			if (!negocio.getResponsablesNegocio().isEmpty()) {
				negocio.getResponsablesNegocio().stream()
						.filter(responsable -> Boolean.TRUE.equals(responsable.isActivo())).forEach(responsable -> {
							responsable.setActivo(Boolean.FALSE);
							servicioResponsablesNotificacion.guardarResponsable(responsable);
						});
			}
		}
	}

	/**
	 * Método que crea los responsables del negocio provenientes del servicio de
	 * búsqueda del equipo responsable.
	 * 
	 * @author efarias
	 * @param negocio
	 * @param listaResponsablesNegocio
	 */
	private void crearActualizarEquipoResponsable(Negocio negocio,
			List<RespuestaAccionEquipoResponsableDTO> listaResponsablesNegocio) {

		if (!negocio.getResponsablesNegocio().isEmpty()) {

			listaResponsablesNegocio.forEach(responsable -> {
				/*
				 * Por cada responsable que trae el servicio verifica si existe, en caso de no
				 * existir lo crea.
				 */
				if (noExisteResponsableServicio(negocio.getResponsablesNegocio(), responsable)) {
					guardarResponsableNotificacion(responsable, negocio.getCodSfc());
				}
			});

		} else {
			/*
			 * si no existen responsables en base de datos pero si en la búsqueda del
			 * servicio de equipo responsable, guarda estos responsables en BD.
			 */
			listaResponsablesNegocio
					.forEach(responsable -> guardarResponsableNotificacion(responsable, negocio.getCodSfc()));
		}
	}
	
	/**
	 * Método que valida si existe o no un responsable del equipo dentro de los
	 * responsables que ya existen en base de datos
	 * 
	 * @author efarias
	 * @param responsablesNegocio
	 * @param responsable
	 * @return boolean -> verdadero si no existe, falso si existe.
	 */
	private boolean noExisteResponsableServicio(List<ResponsableNotificacion> responsablesNegocio,
			RespuestaAccionEquipoResponsableDTO responsable) {
		boolean noExiste = Boolean.TRUE;
		for (ResponsableNotificacion responsableServicio : responsablesNegocio) {
			if (responsable.getInformacion().getTipoDocumento().getDescripcion().trim()
					.equals(responsableServicio.getResponsablesNotificacionPK().getTipoDocumento().trim())
					&& responsable.getInformacion().getNumeroDocumento().trim()
							.equals(responsableServicio.getResponsablesNotificacionPK().getDocumento().trim())
					&& responsable.getCodigoFideicomiso().trim()
							.equals(responsableServicio.getResponsablesNotificacionPK().getCodSfc())) {
				noExiste = Boolean.FALSE;
				break;
			}
		}
		return noExiste;
	}
	
	/**
	 * Método que inactiva un resposable de la BD que no exista dentro de la
	 * respuesta del servicio de obtener equipo responsable.
	 * 
	 * @author efarias
	 * @param negocio
	 * @param listaResponsablesNegocio
	 */
	private void inactivarActualizarEquipoResponsable(Negocio negocio,
			List<RespuestaAccionEquipoResponsableDTO> listaResponsablesNegocio) {
		if (!negocio.getResponsablesNegocio().isEmpty()) {
			negocio.getResponsablesNegocio().stream().forEach(responsable -> {
				// Si no existe lo inactiva
				if (noExisteResponsable(responsable, listaResponsablesNegocio, negocio.getCodSfc())) {
					responsable.setActivo(Boolean.FALSE);
					servicioResponsablesNotificacion.guardarResponsable(responsable);
				}
			});
		}
	}
	
	/**
	 * Método que actualiza los datos del responsables con los nuevos datos
	 * provenientes del servicio de búsqueda de equipo responsable.
	 * 
	 * @author efarias
	 * @param responsable
	 * @param responsableServicio
	 */
	private void actualizarResponsableNegocio(ResponsableNotificacion responsable,
			RespuestaAccionEquipoResponsableDTO responsableServicio) {
		boolean tieneModificaciones = Boolean.FALSE;

		// Valida cada uno de los campos verificando si son diferentes.
		if (campoModificado(responsable.getNombre(), responsableServicio.getInformacion().getNombreCompleto())) {
			tieneModificaciones = Boolean.TRUE;
			responsable.setNombre(responsableServicio.getInformacion().getNombreCompleto());
		}
		if (campoModificado(responsable.getTipoResponsable(), responsableServicio.getTipoResponsable())) {
			tieneModificaciones = Boolean.TRUE;
			responsable.setTipoResponsable(responsableServicio.getTipoResponsable());
		}
		if (campoModificado(responsable.getTelefono(), responsableServicio.getInformacion().getTelefono())) {
			tieneModificaciones = Boolean.TRUE;
			responsable.setTelefono(responsableServicio.getInformacion().getTelefono());
		}
		if (campoModificado(responsable.getCorreo(), responsableServicio.getInformacion().getEmail())) {
			tieneModificaciones = Boolean.TRUE;
			responsable.setCorreo(responsableServicio.getInformacion().getEmail());
		}

		// Si existen modificaciones, actualiza el registro en BD
		if (tieneModificaciones) {
			responsable.setActivo(Boolean.TRUE);
			servicioResponsablesNotificacion.guardarResponsable(responsable);
		}
	}
	
	/**
	 * Método que valida si el valor obtenido por el servicio de consulta de equipo
	 * de negocio es diferente al valor almacenado en BD.
	 * 
	 * @author efarias
	 * @param campoBD
	 * @param campoServicio
	 * @return verdadero si son difeentes, falso si son iguales
	 */
	private boolean campoModificado(String campoBD, String campoServicio) {
		if(campoBD != null && campoServicio != null) {
			return campoBD.trim().equals(campoServicio.trim());
		} else {
			return Objects.equals(campoBD, campoServicio);
		}
	}
	
	/**
	 * Método que identifica si no existe un responsable dentro del servicio de
	 * búsqueda del equipo responsable, si existe realiza la actualización de datos.
	 * 
	 * @param responsable
	 * @param listaResponsablesNegocio
	 * @param codSfc
	 * @return boolean -> si existe el responsable que se encuentra en BD dentro del
	 *         servicio de búsqueda del equipo responsable
	 */
	private boolean noExisteResponsable(ResponsableNotificacion responsable,
			List<RespuestaAccionEquipoResponsableDTO> listaResponsablesNegocio, String codSfc) {
		boolean noExiste = Boolean.TRUE;

		for (RespuestaAccionEquipoResponsableDTO responsableServicio : listaResponsablesNegocio) {
			if (responsableServicio.getInformacion().getTipoDocumento().getDescripcion().trim()
					.equals(responsable.getResponsablesNotificacionPK().getTipoDocumento().trim())
					&& responsableServicio.getInformacion().getNumeroDocumento().trim()
							.equals(responsable.getResponsablesNotificacionPK().getDocumento().trim())
					&& responsableServicio.getCodigoFideicomiso().trim().equals(codSfc.trim())) {

				// Actualiza responsable de negocio con los nuevos datos, sui existe al menos un
				// campo a modificar.
				actualizarResponsableNegocio(responsable, responsableServicio);
				noExiste = Boolean.FALSE;
				break;
			}
		}
		return noExiste;
	}

	/**
	 * 
	 * @param negocio
	 * @param tokenAutorizacion
	 * @return
	 */
	private ResultadoSincronizacionTitularesYEncargosDTO sincronizarTitulares(Negocio negocio, Date fechaInicial,
			String tokenAutorizacion) {
		RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] cesionesNegocio;
		try {
			cesionesNegocio = accionBack
				.buscarHistoricoCesionesSucesiones(
						servicioParametroSistema.obtenerValorParametro(idParamUrlServicioAccionBuscarCesionesNegocio),
						negocio.getCodSfc(), Utilidades.formatearFecha(fechaInicial, formatoddMMyyyyGuion),
						Utilidades.formatearFecha(new Date(), formatoddMMyyyyGuion), tokenAutorizacion);
		} catch (NoContentException e) {
			cesionesNegocio = new RespuestaAccionConsultaCesionesSucesionesNegocioDTO[0];
		}
		
		List<DatosBasicosPersonaDTO> titularesNuevos = Arrays.stream(cesionesNegocio)
				.map(cesion -> realizarCesion(negocio, cesion)).filter(Objects::nonNull).collect(Collectors.toList());
		
		List<Titular> titularesActuales = servicioTitular.buscarTitularesPorCodigoNegocio(negocio.getCodSfc());

		RespuestaBusquedadVinculadoDTO[] titularesAccionBack = accionBack.buscarBeneficiariosNegocio(
				obtenerUrlServiciosNegocio(negocio.getCodSfc(), paramIdUrlServicioAccionBuscarBeneficiariosNegocio),
				tokenAutorizacion);

		List<Titular> titularesAccion = Arrays.stream(titularesAccionBack)
				.map(titularDTO -> dtoAccionAEntidadTitular(titularDTO, negocio)).collect(Collectors.toList());

		List<CambioDatosTitularesNegocioDTO> cambioDatosTitularesNegocio = new ArrayList<>();

		titularesActuales.forEach(titularActual -> {
			if (titularesAccion.contains(titularActual)) {
				CambioDatosTitularesNegocioDTO cambioDatosTitularesNegocioDTO = compararDatosTitular(titularActual,
						titularesAccion.get(titularesAccion.indexOf(titularActual)));
				if (cambioDatosTitularesNegocioDTO != null) {
					cambioDatosTitularesNegocio.add(cambioDatosTitularesNegocioDTO);
				}
			}
		});

		return new ResultadoSincronizacionTitularesYEncargosDTO(cambioDatosTitularesNegocio, titularesNuevos);
	}

	
	private DatosBasicosPersonaDTO realizarCesion(Negocio negocio, RespuestaAccionConsultaCesionesSucesionesNegocioDTO cesion) {
		
		if (cesion.getClaseBeneficio() != null
				&& Objects.equals(cesion.getClaseBeneficio().getIdClaseBeneficio(), claseCesionUsufructo)) {

			Titular titularCedente = servicioTitular.buscarTitular(
					utilidades.covertirTipoDocumento(cesion.getPropietarioPrevio().getTipoDocumento().getId()),
					cesion.getPropietarioPrevio().getNumeroDocumento(), negocio.getCodSfc());
			Titular titularNuevo = servicioTitular.buscarTitular(
					utilidades.covertirTipoDocumento(cesion.getPropietarioNuevo().getTipoDocumento().getId()),
					cesion.getPropietarioNuevo().getNumeroDocumento(), negocio.getCodSfc());

			if (titularNuevo == null) {
				titularNuevo = dtoAccionAEntidadTitular(cesion.getPropietarioNuevo(), negocio);
				titularNuevo.setPersona(servicioPersona.guardarPersona(titularNuevo.getPersona()));
			}

			ParticipacionesNegocio participacionTitularNuevo;

			if (cesion.getPorceNuevoCedente().compareTo(BigDecimal.ZERO) != 0) {
				titularCedente.setPorcentaje(cesion.getPorceNuevoCedente());
				participacionTitularNuevo = new ParticipacionesNegocio();

			} else {
				titularCedente.setActivo(false);
				participacionTitularNuevo = titularCedente.getParticipacion();
			}

			if (titularNuevo.getParticipacion() == null) {
				participacionTitularNuevo.setNegocio(negocio);
				titularNuevo.setParticipacion(participacionTitularNuevo);
			}

			titularCedente.setPorcentaje(cesion.getPropietarioPrevio().getParticipacion());
			titularNuevo.getParticipacion().setPorcentajeParticipacion(cesion.getPropietarioNuevo().getParticipacion());
			titularNuevo.setPorcentaje(cesion.getPropietarioNuevo().getParticipacion());

			servicioParticipacion.guardar(titularCedente.getParticipacion());
			titularNuevo.setParticipacion(servicioParticipacion.guardar(participacionTitularNuevo));

			servicioTitular.guardarTitular(titularCedente);
			servicioTitular.guardarTitular(titularNuevo);
			return new DatosBasicosPersonaDTO(titularNuevo.getPersona().getNombreCompleto(),
					titularNuevo.getPersona().getPersonaPK().getTipoDocumento(),
					titularNuevo.getPersona().getPersonaPK().getNumeroDocumento());
		}
				
		return null;
	}

	/**
	 * 
	 * @param titularActual
	 * @param titularAccion
	 * @return
	 */
	private CambioDatosTitularesNegocioDTO compararDatosTitular(Titular titularActual, Titular titularAccion) {
		StringBuilder resumenDatosCambiados = new StringBuilder();
		List<DetalleDatosCambiadosDTO> detalleDatosCambiados = new ArrayList<>();

		if (!titularActual.getPersona().getNombreCompleto()
				.equalsIgnoreCase(titularAccion.getPersona().getNombreCompleto())) {
			resumenDatosCambiados.append(titularcambioNombreCompleto);

			detalleDatosCambiados.add(new DetalleDatosCambiadosDTO(titularcambioNombreCompleto,
					titularAccion.getPersona().getNombreCompleto(), titularActual.getPersona().getNombreCompleto()));
			servicioPersona.guardarPersona(titularAccion.getPersona());
		}

		if (titularActual.getPorcentaje().compareTo(titularAccion.getPorcentaje()) !=0 ) {
			resumenDatosCambiados.append(resumenDatosCambiados.length() > 0 ? " ," : "");
			resumenDatosCambiados.append(titularCambioPorcentajeParticipacion);

			detalleDatosCambiados.add(new DetalleDatosCambiadosDTO(titularCambioPorcentajeParticipacion,
					String.valueOf(titularActual.getPorcentaje()), String.valueOf(titularAccion.getPorcentaje())));
			titularActual.setPorcentaje(titularAccion.getPorcentaje());
			servicioTitular.guardarTitular(titularActual);
		}

		CambioDatosTitularesNegocioDTO cambioDatosTitularesNegocioDTO = null;
		if (!detalleDatosCambiados.isEmpty()) {
			cambioDatosTitularesNegocioDTO = new CambioDatosTitularesNegocioDTO();
			cambioDatosTitularesNegocioDTO.setNombreCompleto(titularAccion.getPersona().getNombreCompleto());
			cambioDatosTitularesNegocioDTO
					.setNumeroDocumento(titularAccion.getPersona().getPersonaPK().getNumeroDocumento());
			cambioDatosTitularesNegocioDTO
				.setTipoDocumento(titularAccion.getPersona().getPersonaPK().getTipoDocumento());
			cambioDatosTitularesNegocioDTO.setResumenDatosCambiados(resumenDatosCambiados.toString());
			cambioDatosTitularesNegocioDTO.setDetalleDatosCambiados(detalleDatosCambiados);
		}

		return cambioDatosTitularesNegocioDTO;
	}

	/**
	 * 
	 * @param titularDTO
	 * @param negocio
	 * @return
	 */
	private Titular dtoAccionAEntidadTitular(RespuestaBusquedadVinculadoDTO titularDTO, Negocio negocio) {
		Titular titular = new Titular();
		String tipoDocumento = utilidades.covertirTipoDocumento(titularDTO.getTipoDocumento().getId());
		titular.setTitularPK(new TitularPK(negocio.getCodSfc(), tipoDocumento, titularDTO.getNumeroDocumento()));
		titular.setNegocio(negocio);
		titular.setPorcentaje(titularDTO.getParticipacion());
		titular.setPersona(new Persona(tipoDocumento, titularDTO.getNumeroDocumento(), titularDTO.getNombreCompleto()));
		titular.setEsCotitular(Boolean.FALSE);
		titular.setActivo(Boolean.TRUE);
		return titular;
	}

	/**
	 * 
	 * @param codigoNegocio
	 * @return
	 */
	private String obtenerUrlServiciosNegocio(String codigoNegocio, Integer idParametroUrl) {
		String urlServicioNegocio = servicioParametroSistema.obtenerValorParametro(idParametroUrl);
		urlServicioNegocio = urlServicioNegocio.replace("{idnegocio}", codigoNegocio);
		return urlServicioNegocio;
	}

	/**
	 * 
	 * @param negocio
	 * @param tokenAutorizacion
	 */
	private boolean sincronizarEncargos(Negocio negocio, String tokenAutorizacion) {

		List<Encargo> encargosActuales = servicioEncargo.buscarEncargoNegocio(negocio.getCodSfc());
		RespuestaAccionBuscarEncargoDTO[] encargos = new RespuestaAccionBuscarEncargoDTO[0]; 
		try {
			encargos = accionBack.buscarEncargosNegocio(negocio.getCodSfc(),
				tokenAutorizacion,
				servicioParametroSistema.obtenerValorParametro(paramIdUrlServicioAccionBusquedadEncargosNegocio));
		} catch (NoContentException e) {}
		
		List<Encargo> encargosAccion = Arrays.stream(encargos)
				.map(encargo -> encargoDTOAEntidad(
						new EncargoNegocioDTO(codigoEncargoActivo, encargo.getNumeroEncargo()), negocio))
				.collect(Collectors.toList());

		encargosActuales.forEach(encargo -> {
			if (!encargosAccion.contains(encargo)) {
				encargo.setEstado(codigoEncargoInactivo);
				servicioEncargo.guardarEncargo(encargo);
			}
		});
		return encargosAccion.stream().anyMatch(encargo -> !encargosActuales.contains(encargo));
	}

	/**
	 * 
	 * @param codigoNegocio
	 * @param tokenAutorizacion
	 * @return
	 */
	@Override
	public List<DatosEquipoResponsableDTO> cargarEquipoResponsable(String codigoNegocio, String tokenAutorizacion) {
		try {
			RespuestaAccionEquipoResponsableDTO[] respuestaAccionEquipoResponsable = accionBack.buscarEquipoResponsable(
					obtenerUrlServiciosNegocio(codigoNegocio, paramIdUrlServicioAccionBusquedadEquipoNegocioNegocio),
					tokenAutorizacion);
			return Arrays.stream(respuestaAccionEquipoResponsable)
					.map(equipo -> new DatosEquipoResponsableDTO(equipo.getInformacion().getNombreCompleto(),
							equipo.getTipoResponsable()))
					.collect(Collectors.toList());
		} catch (NoContentException e) {
			return new ArrayList<>(); 
		}
		
	}
	
	/**
	 * Método que consulta el equipo resposable asociado a un equipo.
	 * 
	 * @author efarias
	 * @param codigoNegocio
	 * @param tokenAutorizacion
	 * @return
	 */
	private List<RespuestaAccionEquipoResponsableDTO> obtenerEquipoResponsable(String codigoNegocio,
			String tokenAutorizacion) {
		try {
			RespuestaAccionEquipoResponsableDTO[] respuestaAccionEquipoResponsable = accionBack.buscarEquipoResponsable(
					obtenerUrlServiciosNegocio(codigoNegocio, paramIdUrlServicioAccionBusquedadEquipoNegocioNegocio),
					tokenAutorizacion);
			return Arrays.asList(respuestaAccionEquipoResponsable);
		} catch (NoContentException e) {
			return new ArrayList<>();
		}
	}
	
	@Override
	public RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] buscarHistoricoCesionesSucesiones(
			String codigoNegocio, String fechaInicio, String fechaFin, String tokenAutorizacion) {
		RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] respuestaAccionSucesiones = null;
		try {
			respuestaAccionSucesiones = accionBack.buscarHistoricoCesionesSucesiones(
					obtenerUrlServiciosNegocio(codigoNegocio, paramIdUrlServicioAccionCesiones),
					codigoNegocio,
					fechaInicio,
					fechaFin,
					tokenAutorizacion);
		} catch (NoContentException e) {
			return new RespuestaAccionConsultaCesionesSucesionesNegocioDTO[0];
		}
		return respuestaAccionSucesiones;
	}
	
	@Override
	public RespuestaHistoricoReunionesDTO consultaHistoricoReuniones(PeticionHistorialReunionesDTO peticion,
			String tokenAuthorization) throws Exception {
		RespuestaHistoricoReunionesDTO historicoDTO = new RespuestaHistoricoReunionesDTO();
		List<HistoricoReunionDTO> historico = servicioReunion.consultaHistoricoReuniones(peticion);
		historicoDTO.setHistoricoReuniones(historico);
		historicoDTO.setTotalRegistros(historico.size());
		return historicoDTO;
	}
	
	/**
	 * Metodo que permite consultar los historicos de asamblea o comite a un negocio
	 * que previamente se encuentre generado
	 * 
	 * @param archivo Objeto que contiene el archivo excel que sera procesado
	 * @param token   Token de seguridad de acceso a la aplicacion
	 * @return RespuestaArchivoDTO Objeto que envia al fronted de la aplicacion los
	 *         resultados del proceso de cargue.
	 * @throws Exception
	 */
	@Override
	@Transactional
	public RespuestaHistoricoDTO consultaHistoricoReunion(String codigoNegocio, String tipoHistorico) throws Exception {
		RespuestaHistoricoDTO respuesta = new RespuestaHistoricoDTO();
		List<AsambleasDTO> historicoAsambleas = new ArrayList<>();
		List<ComiteDTO> historicoComite = new ArrayList<>();
		List<DecisionDTO> listaDecisiones = new ArrayList<>();
		List<AsistenteDTO> listaAsistentes = new ArrayList<>();
		List<DecisionComiteDTO> listaDecisionesComite = new ArrayList<>();
		int numeroAsistentes;
		if (tipoHistorico.equalsIgnoreCase(TipoReunion.ASAMBLEA.toString())) {
			List<Reunion> ListaAsambleas = servicioReunion.consultaHistoricoReunion(codigoNegocio, tipoHistorico);
			for(Reunion asamblea: ListaAsambleas) {
				AsambleasDTO resAsamblea = new AsambleasDTO();
				resAsamblea.setCodigoSFC(asamblea.getReunionPk().getCodigoSfc());
				for(Decision des: asamblea.getDecisiones()) {
					DecisionDTO decision = new DecisionDTO();
					decision.setDescripcion(des.getDescripcion());
					decision.setQuorumAprobatorio(des.getQuorumAprobatorio()==null?0:des.getQuorumAprobatorio());
					decision.setResponsable(des.getResponsable());
					listaDecisiones.add(decision);
				}
				resAsamblea.setDecision(listaDecisiones);
				resAsamblea.setDireccionReunion(asamblea.getDireccion());
				resAsamblea.setFechaReunion(utilidades.convierteLocalDateTimeToString(asamblea.getFechaReunion()));
				numeroAsistentes=0;
				for(Asistente asistente: asamblea.getAsistentesReunion()) {
					AsistenteDTO asist = new AsistenteDTO();
					asist.setNumeroDocumento(asistente.getAsistentePK().getNumeroDocumento());
					asist.setTipoDocumento(asistente.getAsistentePK().getTipoDocumento());
					listaAsistentes.add(asist);
					numeroAsistentes++;
				}
				resAsamblea.setMesaDirectiva(listaAsistentes);
				resAsamblea.setNumeroAsistentes(numeroAsistentes);
				resAsamblea.setOrdenDia(asamblea.getOrdenDia());
				resAsamblea.setRadicado(asamblea.getIdActa());
				historicoAsambleas.add(resAsamblea);
			}
			respuesta.setHistoricoAsambleas(historicoAsambleas);
		} else if(tipoHistorico.equalsIgnoreCase(TipoReunion.COMITE.toString())){
			List<Reunion> Listacomites = servicioReunion.consultaHistoricoReunion(codigoNegocio, tipoHistorico);
			for (Reunion comite: Listacomites) {
				ComiteDTO resComite = new ComiteDTO();
				for(Asistente asistente : comite.getAsistentesReunion()) {
					AsistenteDTO asist = new AsistenteDTO();
					asist.setNumeroDocumento(asistente.getAsistentePK().getNumeroDocumento());
					asist.setTipoDocumento(asistente.getAsistentePK().getTipoDocumento());
					listaAsistentes.add(asist);
				}
				resComite.setAsistentes(listaAsistentes);
				resComite.setCodigoSFC(comite.getReunionPk().getCodigoSfc());
				for (Decision decisionesComite: comite.getDecisiones()) {
					DecisionComiteDTO resDecisionComite = new DecisionComiteDTO();
					resDecisionComite.setDescripcion(decisionesComite.getDescripcion());
					listaDecisionesComite.add(resDecisionComite);
				}
				resComite.setDecisiones(listaDecisionesComite);
				resComite.setDireccionReunion(comite.getDireccion());
				resComite.setFechaHoraReunion(utilidades.convierteLocalDateTimeToString(comite.getFechaReunion()));
				resComite.setNombreArchivo(comite.getPresentacion());
				resComite.setNumeroRadicado(comite.getIdActa());
				resComite.setOrdenDia(comite.getOrdenDia());
				resComite.setTemasTratados(comite.getResumenReunion());
				historicoComite.add(resComite);
			}
			respuesta.setHistoricoComites(historicoComite);
		}
		else {
			throw new Exception("El tipo de reunion es invalido ");
		}

		return respuesta;
	}
	
	// Método que permite transformar la información del negocio en un dto de negocio
	private NegocioFiltroDTO transformarNegocios(Negocio negocio) {
		NegocioFiltroDTO negocioFiltro = new NegocioFiltroDTO();
		String nombreNegocio = negocio.getCodSfc().concat(" "+negocio.getNombre());
		negocioFiltro.setNombre(nombreNegocio);
		String codigoSfc = negocio.getCodSfc();
		negocioFiltro.setCodigoSFC(codigoSfc);
		return negocioFiltro;
	}
		
	// Método que obtiene el listado de negocios
	@Override
	public  List<NegocioFiltroDTO> obtenerNegocios() throws Exception {
		List <Negocio> resultados = repositorioNegocio.findAll();
		return resultados.stream()
		.map(negociosTransfor -> transformarNegocios(negociosTransfor))
		.collect(Collectors.toList());	
	}

	@Override
	public RespuestaHistoricoCambiosDTO listaNegocioHistorialCambios(ParametrosFiltroDTO filtros,String tokenAutorizacion) {
		List<HistoricoCambioDTO> historialAuditoria = servicioAuditoria.consultarHistoricoAuditoria(filtros, null, "NEGOCIO", tokenAutorizacion);
		int cantidadRegistros =  historialAuditoria.isEmpty() ? 0: historialAuditoria.size();
		RespuestaHistoricoCambiosDTO respuesta = new RespuestaHistoricoCambiosDTO();
		respuesta.setTotalRegistro(cantidadRegistros);
		respuesta.setListaCambios(historialAuditoria);
		return respuesta;
	}	

	@Override
	public List<Negocio> obtenerTodosNegocios() {
		return repositorioNegocio.findAll();
	}

	@Override
	public List<ValorDominioSimpleDTO> obtenerTiposNegocio(String codigoNegocio) {
		List<TipoNegocio> tiposNegocio = servicioTipoNegocio.buscarTiposNegocioPorCodigoNegocio(codigoNegocio);
		return tiposNegocio.stream()
				.map(tipoNegocio -> ServicioDominio.entidadAValorDominioSimpleDTO(
						servicioDominio.buscarValorDominio(tipoNegocio.getTipoNegocioPK().getTipoNegocio())))
				.collect(Collectors.toList());
	}

	@Override
	public List<Negocio> buscarNegocioPorNombrePaginado(String nombreNegocio, int pagina, int elementos) {
		return repositorioNegocio.findByNombreContainingIgnoreCase(nombreNegocio,PageRequest.of(pagina - 1, elementos)).toList();
	}

	@Override
	public List<Negocio> buscarNegociosPorDocumentoTitular(String tipoDocumento, String numeroDocumento, int pagina,
			int elementos) {
		return repositorioNegocio.findByTitulares_TitularPK_TipoDocumentoAndTitulares_TitularPK_NumeroDocumento(tipoDocumento, numeroDocumento, PageRequest.of(pagina - 1, elementos)).toList();
	}
		
	private boolean sincronizarEstadoNegocio(Negocio negocio, String tokenAutorizacion) {
		RepuestaConsultaNegocioDTO negocioAccion = buscarNegocioPorCodigo(negocio.getCodSfc(), tokenAutorizacion);
		if(negocioAccion.getEstadoNegocio() == null || !negocioAccion.getEstadoNegocio().getDescripcion().equalsIgnoreCase(negocio.getEstado())) {
			negocio.setEstado(negocioAccion.getEstadoNegocio().getDescripcion());
			repositorioNegocio.save(negocio);
		}
		return negocioAccion.getEstadoNegocio() != null && negocioAccion.getEstadoNegocio().getDescripcion().equalsIgnoreCase(negocioVigente);
	}
}
