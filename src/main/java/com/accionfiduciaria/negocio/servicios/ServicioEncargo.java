package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Encargo;
import com.accionfiduciaria.negocio.repositorios.RepositorioEncargo;

@Service
public class ServicioEncargo implements IServicioEncargo {
	
	@Autowired
	RepositorioEncargo repositorioEncargo;
	
	@Override
	public List<Encargo> buscarEncargoNegocio(String codigoNegocio) {
		return repositorioEncargo.findByNegocio_codSfc(codigoNegocio);
	}

	@Override
	public Encargo guardarEncargo(Encargo encargo) {
		return repositorioEncargo.save(encargo);
	}

}
