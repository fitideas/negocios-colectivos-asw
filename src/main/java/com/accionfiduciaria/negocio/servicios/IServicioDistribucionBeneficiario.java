/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;

/**
 * @author efarias
 *
 */
public interface IServicioDistribucionBeneficiario {

	@Autowired
	public List<DistribucionBeneficiario> guardarDistribucionBeneficiarios(List<Beneficiario> listaBeneficiarios,
			Distribucion distribucion);

	@Autowired
	public Map<String, Object> listaDistribucionADTO(List<DistribucionBeneficiario> listaDistribucion);

	@Autowired
	public DistribucionBeneficiario verificarModificarDistribucionBeneficiario(
			DistribucionBeneficiario distribucionBeneficiario, Beneficiario beneficiario);

	@Autowired
	public DistribucionBeneficiario agregarInformacionDistribucionBeneficiario(DistribucionBeneficiario distribucion,
			DistribucionBeneficiario distribucionBeneficiario, Beneficiario beneficiario);

}
