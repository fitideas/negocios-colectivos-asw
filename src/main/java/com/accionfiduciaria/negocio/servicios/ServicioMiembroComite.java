package com.accionfiduciaria.negocio.servicios;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.enumeradores.TipoDesicion;
import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.ErrorProcesandoArchivo;
import com.accionfiduciaria.modelo.dtos.AsistenteDTO;
import com.accionfiduciaria.modelo.dtos.ComiteRegistroDTO;
import com.accionfiduciaria.modelo.dtos.DecisionComiteDTO;
import com.accionfiduciaria.modelo.dtos.MiembroComiteDTO;
import com.accionfiduciaria.modelo.entidad.Asistente;
import com.accionfiduciaria.modelo.entidad.AsistentePK;
import com.accionfiduciaria.modelo.entidad.ComisionAprobadoraComite;
import com.accionfiduciaria.modelo.entidad.Decision;
import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.MiembroComite;
import com.accionfiduciaria.modelo.entidad.MiembroComitePK;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioAsistente;
import com.accionfiduciaria.negocio.repositorios.RepositorioComisionAprobadoraComite;
import com.accionfiduciaria.negocio.repositorios.RepositorioDecision;
import com.accionfiduciaria.negocio.repositorios.RepositorioMiembroComite;

@Service
@PropertySource("classpath:propiedades.properties")
public class ServicioMiembroComite implements IServicioMiembrosComite {

	public static final String NOT_FOUND_EXCEPCION_TIPO_NEGOCIO = "No se encontró ningún tipo de negocio asociado";

	@Autowired
	private Utilidades utilidades;

	@Autowired
	private RepositorioMiembroComite repositorioMC;

	@Autowired
	private ServicioTitular st;
	
	@Autowired
	private ServicioPersona sp;
	
	@Autowired
	private ServicioDominio servicioDominio;
	
	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;
	
	@Value("${cod.dominio.rol.tercero}")
	private String codDominioRolTercero;

	@Autowired
	private IServicioFechaClave servicioFechaClave;

	@Autowired
	private IServicioReunion servicioReunion;
	
	@Autowired
	private RepositorioDecision repositorioDecision;
	
	@Autowired
	private RepositorioComisionAprobadoraComite repositorioComisionAprobatoria;
	
	
	@Autowired
	private RepositorioAsistente repositorioAsistente;

	
	@Autowired
	private IServicioArchivo servicioArchivo;

	@Override
	public List<MiembroComiteDTO> obtenerMiembrosComiteNegocio(String codSFC) throws BusquedadSinResultadosException {
		List<MiembroComite> listaAnadidos = repositorioMC.buscarMiembroPorNegocio(codSFC);
		List<Titular> listaTitulares = st.buscarTitularesPorCodigoNegocio(codSFC);
		for (Titular temp : listaTitulares) {

			MiembroComite mc = new MiembroComite(temp.getTitularPK().getCodSfc(),
					temp.getTitularPK().getTipoDocumento(), temp.getTitularPK().getNumeroDocumento());
			if (!listaAnadidos.contains(mc)) {
				listaAnadidos.add(mc);
			}
		}
		if (listaAnadidos.isEmpty())
			throw new BusquedadSinResultadosException(NOT_FOUND_EXCEPCION_TIPO_NEGOCIO);
		return listaAnadidos.stream().map(miembroComite -> aMiembroComiteDTO(miembroComite))
				.collect(Collectors.toList());

	}

	public void guardarMiembroComite(MiembroComiteDTO dto) {
		MiembroComitePK pk = new MiembroComitePK(dto.getCodigoSFC(), dto.getTipoDocumento(), dto.getNumDocumento());
		MiembroComite miembro = new MiembroComite(pk);
		miembro.setEstado(dto.getEstado());
		miembro.setRadicado(dto.getRadicado());
		miembro.setRol(dto.getRol());
		miembro.setFechaVigenciaRol(utilidades.convertirFecha(dto.getFecVigenciaRol(), formatoddmmyyyy));
		repositorioMC.save(miembro);

	}

	public MiembroComiteDTO aMiembroComiteDTO(MiembroComite mc) {
		MiembroComiteDTO mcDTO = new MiembroComiteDTO();
		mcDTO.setCodigoSFC(mc.getMiembroComitePK().getCodSfc());
		mcDTO.setTipoDocumento(mc.getMiembroComitePK().getTipoDocumento());
		mcDTO.setNumDocumento(mc.getMiembroComitePK().getNumeroDocumento());	
		if (mc.getEstado() != null) {
			mcDTO.setNombre(mc.getPersona().getNombreCompleto());
			mcDTO.setAgregado(true);
			mcDTO.setEstado(mc.getEstado());
			mcDTO.setRol(mc.getRol());
			mcDTO.setNombreRol(
					servicioDominio.buscarValorDominioPorCodDominioYValorDominio(codDominioRolTercero, mc.getRol())
					.getDescripcion());
			mcDTO.setFecVigenciaRol(Utilidades.formatearFecha(mc.getFechaVigenciaRol(), formatoddmmyyyy));
			mcDTO.setRadicado(mc.getRadicado());
		} else {
			Persona p =sp.buscarPersona(mc.getMiembroComitePK().getTipoDocumento(), mc.getMiembroComitePK().getNumeroDocumento());
			mcDTO.setNombre(p.getNombreCompleto());
			mcDTO.setAgregado(false);
			mcDTO.setEstado(null);
			mcDTO.setRol(0);
			mcDTO.setFecVigenciaRol("");
			mcDTO.setRadicado("");
		}

		return mcDTO;
	}

	@Override
	public void guardaInformacionComite(ComiteRegistroDTO comiteDTO, String token) throws Exception {
		Date fechaCalendario = utilidades.convertirFecha(comiteDTO.getFechaHoraReunion(),
				"dd/MM/yyyy HH:mm:ss");

		LocalDateTime fechaTime = utilidades.convierteDateToLocalDateTime(fechaCalendario);	
		
		try {
		
			FechaClave fechaClave = servicioFechaClave.guardarFechaClave(fechaCalendario, comiteDTO.getCodigoSFC(),
					true);
			servicioReunion.guardarReunionComite(comiteDTO, fechaClave, token);
			List<Decision> pendientes = comiteDTO.getTareaPendiente().stream()
					.map(pendiente -> convierteDecisionesAEntidad(pendiente, fechaTime, comiteDTO.getCodigoSFC(),
							TipoDesicion.PENDIENTE))
					.collect(Collectors.toList());
			List<Decision> decisionesTomadas = comiteDTO
					.getDecisionTomada().stream().map(decisionTomada -> convierteDecisionesAEntidad(decisionTomada,
							fechaTime, comiteDTO.getCodigoSFC(), TipoDesicion.DECISIONTOMADA))
					.collect(Collectors.toList());
			decisionesTomadas.addAll(pendientes);
			comiteDTO.getAsistentes().stream()
					.map(asistente -> guardarAsistentesAEntidad(asistente, fechaTime, comiteDTO.getCodigoSFC()))
					.collect(Collectors.toList());
			servicioArchivo.guardarArchivo(utilidades.convierteBase64ToBytes(comiteDTO.getArchivo()),
					comiteDTO.getNombreArchivo());
		} catch (IOException e) {
			throw new ErrorProcesandoArchivo("El archivo no se pudo cargar por " + e.getMessage());
		} catch (Exception e) {
			throw new Exception(
					"Fallo el almacenamiento de la informacion de comites por " + e.getMessage());
		}
		
		
		
	}

	private Decision convierteDecisionesAEntidad(DecisionComiteDTO d, LocalDateTime fec, String codigoSFC,
			TipoDesicion tipo) {
		 Decision decision = new Decision();
		decision.setCodSfc(codigoSFC);
		decision.setFecha(fec);
		decision.setDescripcion(d.getDescripcion());
		decision.setEstado(TipoDesicion.DECISIONTOMADA.equals(tipo));
		Decision dg= repositorioDecision.save(decision);
		
		List<ComisionAprobadoraComite > comision =
				d.getResponsables().stream()
				.map(r ->  
				new ComisionAprobadoraComite(dg.getDecisionesId(), r.getTipoDocumento(),r.getNumeroDocumento(), r.getNomResponsable()))
				.collect(Collectors.toList());
		repositorioComisionAprobatoria.saveAll(comision);
		return decision;
	}
	
	private Asistente guardarAsistentesAEntidad(AsistenteDTO asis, LocalDateTime fec, String codigoSFC) {

		Asistente asistente = new Asistente();
		asistente.setAsistentePK(
				new AsistentePK(codigoSFC, asis.getTipoDocumento(), asis.getNumeroDocumento(), codigoSFC, fec));
		asistente.setAsistio(1);
		repositorioAsistente.save(asistente);
		return asistente;
	}

	@Override
	public List<MiembroComite> buscarMiembrosComitePorRol(Integer idRol) {
		return repositorioMC.findByRolAndEstadoIgnoreCase(idRol, "ACTIVO");
	}

	@Override
	public MiembroComite guardarMiembroComite(MiembroComite miembroComite) {
		return repositorioMC.save(miembroComite);
	}
}
