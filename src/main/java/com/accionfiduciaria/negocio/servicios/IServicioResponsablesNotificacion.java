/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.ResponsablesNegocioDTO;
import com.accionfiduciaria.modelo.entidad.ResponsableNotificacion;

/**
 * @author efarias
 *
 */
public interface IServicioResponsablesNotificacion {

	/**
	 * Método que guarda el responsable asociado a un negocio;
	 * 
	 * @author efarias
	 * @param responsablesNotificacion
	 */
	@Autowired
	public void guardarResponsable(ResponsableNotificacion responsablesNotificacion);

	/**
	 * Método que obtiene todos los responsables de todos los negocios en estado
	 * activo.
	 * 
	 * @author efarias
	 * @return Responsables asociados a un negocio en estado Activo.
	 */
	@Autowired
	public List<ResponsablesNegocioDTO> obtenerResponsablesNegocioActivos();

}
