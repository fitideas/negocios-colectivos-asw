/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionRetenciones;
import com.accionfiduciaria.modelo.entidad.RetencionTipoNegocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucionRetenciones;

/**
 * @author efarias
 *
 */
@Service
public class ServicioDistribucionRetenciones implements IServicioDistribucionRetenciones {

	@Autowired
	private RepositorioDistribucionRetenciones repositorioDistribucionRetenciones;

	@Autowired
	private Utilidades utilidades;

	/**
	 * Cálcula las retenciones en estado activo para un tipo de negocio.
	 * 
	 * @author efarias
	 * @param tipoNegocio
	 * @param distribucion
	 * @return List<DistribucionRetenciones> -> lista distribución de retenciones
	 *         por tipo de negocio
	 */
	@Override
	public List<DistribucionRetenciones> calcularDistribucionRetenciones(TipoNegocio tipoNegocio,
			Distribucion distribucion) {
		List<RetencionTipoNegocio> listaRetencionesNegocio = tipoNegocio.getRetencionesTipoNegocio().stream()
				.filter(retencion -> retencion.getActivo().equals(Boolean.TRUE)).collect(Collectors.toList());
		List<DistribucionRetenciones> listaDistribucionRetenciones = new ArrayList<>();

		// Valor base de cálculo.
		BigDecimal valorBase = distribucion.getTotalDistribucion();

		if (!listaRetencionesNegocio.isEmpty()) {
			for (RetencionTipoNegocio retencion : listaRetencionesNegocio) {
				DistribucionRetenciones detalle = new DistribucionRetenciones(distribucion.getDistribucionPK(),
						retencion.getRetencionTipoNegocioPK());
				detalle.setUnidad(retencion.getDominioRetencion().getUnidad());
				detalle.setValorConfiguradoImpuesto(new BigDecimal(retencion.getDominioRetencion().getValor()));
				detalle.setValorBase(valorBase);
				detalle.setValorImpuesto(utilidades.calcularValorImpuesto(retencion.getDominioRetencion(), valorBase));
				listaDistribucionRetenciones.add(detalle);
			}
		}
		return listaDistribucionRetenciones;
	}

	/**
	 * Guadar una lista de distribución retenciones
	 * 
	 * @author efarias
	 */
	@Override
	public void guardarListaDistribucionretenciones(List<DistribucionRetenciones> listaDistribucionRetenciones) {
		repositorioDistribucionRetenciones.saveAll(listaDistribucionRetenciones);
	}

}
