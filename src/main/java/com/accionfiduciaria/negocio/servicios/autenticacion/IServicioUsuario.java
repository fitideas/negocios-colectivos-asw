/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.autenticacion;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Usuario;


/**
 * @author José Santiago Polo Acosta - 13/12/2019 - jpolo@asesoftware.com
 *
 */
@Service
public interface IServicioUsuario {
	
	/**
	 * 
	 * @param login
	 * @return
	 */
	@Autowired
	public Usuario buscarUsuario(String login);
	
	/**
	 * 
	 * @param usuario
	 * @return
	 */
	@Autowired
	public Usuario guardarUsuario(Usuario usuario);

	@Autowired
	public Iterable<Usuario> obtenerTodos();
	@Autowired
	public Usuario buscarUsuarioPorToken(String token);

}
