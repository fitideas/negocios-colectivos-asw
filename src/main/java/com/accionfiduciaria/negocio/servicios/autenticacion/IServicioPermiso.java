/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.autenticacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.OpcionesMenuDTO;

/**
 * Servicio encargado de manejar la logica de los permisos de los roles.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */

public interface IServicioPermiso {
	
	/**
	 * Obtiene el listado de permisos activos del sistema.
	 * 
	 * @return la lista de todos los permisos activos del sistema.
	 * @throws Exception
	 */
	@Autowired
	public List<OpcionesMenuDTO> obtenerPermisosActivos() throws Exception;
}
