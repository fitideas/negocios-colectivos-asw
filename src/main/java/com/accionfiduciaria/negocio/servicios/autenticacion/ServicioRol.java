/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.autenticacion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.dtos.ListaFuncionalidadesDTO;
import com.accionfiduciaria.modelo.dtos.PermisoDTO;
import com.accionfiduciaria.modelo.dtos.RolDTO;
import com.accionfiduciaria.modelo.entidad.Rol;
import com.accionfiduciaria.negocio.repositorios.RepositorioRol;

/**
 * Clase que implementa la interfaz {@link IServicioRol}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
public class ServicioRol implements IServicioRol{
	
	@Autowired
	RepositorioRol repositorioRol;
	
	@Autowired
	ServicioPermiso servicioPermiso;

	@Override
	public List<RolDTO> obtenerRoles() throws Exception{
		return procesarRoles(repositorioRol.findAll());
	}

	/**
	 * Procesa la respuesta de la consulta y la convierte a un listado de
	 * dtos para su envío al controlador.
	 * 
	 * @param roles El listado de roles a procesar.
	 * @return Un listado de dto de tipo {@link RolDTO} con los datos de los roles enviados junto con sus permisos.
	 */
	private static List<RolDTO> procesarRoles(List<Rol> roles){
		return roles.stream().map(ServicioRol::rolADTO).collect(Collectors.toList());
	}
	
	/**
	 * Convierte una entidad de tipo {@link Rol} a un dto de tipo {@link RolDTO}.
	 * Verifica el estado de los permisos y solo añade aquellos que esten activos.
	 * 
	 * @param rol EL rol a convertir.
	 * @return El dto correspondiente al rol enviado.
	 */
	private static RolDTO rolADTO(Rol rol) {
		RolDTO rolDTO = new RolDTO();
		rolDTO.setIdRol(rol.getIdRol());
		rolDTO.setRol(rol.getNombreRol());
		
		List<PermisoDTO> funcionalidades = new ArrayList<>();
		
		rol.getRolPermisos().forEach(rp -> {
			if (Boolean.TRUE.equals(rp.getPermiso().getEstado())) {
				funcionalidades.add(new PermisoDTO(rp.getPermiso().getIdPermiso(), rp.getPermiso().getNombre(),
						rp.getPuedeEditar()));
			}
		});
		
		rolDTO.setListaFuncionalidades(new ListaFuncionalidadesDTO(funcionalidades));
		return rolDTO;
	}

	@Override
	public boolean existeRol(String rol) {
		return repositorioRol.existsByNombreRol(rol);
	}
}
