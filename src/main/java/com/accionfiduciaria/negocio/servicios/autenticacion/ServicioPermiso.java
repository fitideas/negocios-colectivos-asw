package com.accionfiduciaria.negocio.servicios.autenticacion;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.dtos.OpcionesMenuDTO;
import com.accionfiduciaria.modelo.dtos.PermisoDTO;
import com.accionfiduciaria.modelo.entidad.Permiso;
import com.accionfiduciaria.negocio.repositorios.RepositorioPermiso;

/**
 * Clase que implementa la interfaz {@link IServicioPermiso}.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
public class ServicioPermiso implements IServicioPermiso {

	@Autowired
	RepositorioPermiso repositorioPermiso;

	@Override
	public List<OpcionesMenuDTO> obtenerPermisosActivos() throws Exception {
		return procesarPermisos(repositorioPermiso.findAllByPermisoPadreIsNull());
	}

	/**
	 * Retorna las opciones del menu de la aplicación junto con sus funcionalidades.
	 * 
	 * @param permisos El listado de permisos a procesar.
	 * @return El listado de opciones de menú de la aplicación junto con sus
	 *         funcionalidades.
	 */
	private static List<OpcionesMenuDTO> procesarPermisos(List<Permiso> permisos) {
		return permisos.stream().map(ServicioPermiso::permisoAOpcionMenuDTO).collect(Collectors.toList());
	}

	/**
	 * Convierte una entidad de tipo {@link Permiso} a un dto de tipo
	 * {@link OpcionesMenuDTO}
	 * 
	 * @param permiso La entidad a convertir.
	 * @return El dto correpondiente a la entidad enviada.
	 */
	private static OpcionesMenuDTO permisoAOpcionMenuDTO(Permiso permiso) {
		OpcionesMenuDTO opcionDto = new OpcionesMenuDTO(permiso.getIdPermiso(), permiso.getNombre());
		opcionDto.setFuncionalidades(permiso.getPermisosHijos().stream().map(ServicioPermiso::permisoApermisoDTO)
				.collect(Collectors.toList()));
		return opcionDto;
	}
	
	/**
	 * Convierte una entidad de tipo {@link Permiso} a un dto de tipo
	 * {@link FuncionalidadDTO}
	 * 
	 * @param permiso La entidad a convertir.
	 * @return El dto correpondiente a la entidad enviada.
	 */
	private static PermisoDTO permisoApermisoDTO(Permiso permiso) {
		return new PermisoDTO(permiso.getIdPermiso(), permiso.getNombre(), permiso.getPuedeEditar());
	}
}
