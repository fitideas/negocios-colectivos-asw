/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.autenticacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.RolDTO;

/**
 * Servicio encargado de manejar la logica de los roles.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */

public interface IServicioRol {
	
	/**
	 * Obtiene el listado completo de roles que existen en la aplicación.
	 * 
	 * @return Los datos de los roles existentes.
	 * @throws Exception
	 */
	@Autowired
	public List<RolDTO> obtenerRoles() throws Exception;

	@Autowired
	public boolean existeRol(String rol);
}
