package com.accionfiduciaria.negocio.servicios.autenticacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.FaltanDatosObligatoriosException;
import com.accionfiduciaria.excepciones.UsuarioSinPermisosException;
import com.accionfiduciaria.modelo.dtos.RolDTO;
import com.accionfiduciaria.modelo.dtos.RolMenuDTO;

/**
 * Servicio encargado de manejar la logica de los roles.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioRolPermiso {
	
	/**
	 * Actualiza los permisos de los roles enviados.
	 * 
	 * @param roles El listado de roles con los permisos a actualizar.
	 */
	@Autowired
	public void actualizarPermisosRoles(List<RolDTO> roles) throws FaltanDatosObligatoriosException;
	
	/**
	 * Obtiene las opciones de menu junto con las funcionalidades a las que tiene accesso un rol.
	 * 
	 * @param nombreRol El nombre del rol.
	 * @return Un DTO con las opciones a las que tiene acceso el rol enviado.
	 */
	@Autowired
	public RolMenuDTO obtenerOpcionesRol(String nombreRol) throws UsuarioSinPermisosException;
	
}
