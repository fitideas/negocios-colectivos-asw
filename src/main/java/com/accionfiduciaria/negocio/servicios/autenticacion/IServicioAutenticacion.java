/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.autenticacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.excepciones.ParametroNoEncontradoException;
import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.excepciones.UsuarioSinPermisosException;
import com.accionfiduciaria.excepciones.UsuarioSinRolesException;
import com.accionfiduciaria.modelo.dtos.UsuarioDTO;

/**
 * Servicio encargado de manejar el proceso de autenticación de un usuario.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
public interface IServicioAutenticacion {

	/**
	 * 
	 * @param usuarioDTO
	 * @return
	 * @throws UnauthorizedException
	 * @throws UsuarioSinRolesException
	 * @throws ParametroNoEncontradoException
	 * @throws Exception
	 */
	@Autowired
	public UsuarioDTO autenticarUsuario(UsuarioDTO usuarioDTO)
			throws UnauthorizedException, UsuarioSinRolesException, UsuarioSinPermisosException,
			ParametroNoEncontradoException, Exception;

	/**
	 * @param tokenAutorizacion
	 * @return
	 */
	@Autowired
	public void cerrarSesion(String tokenAutorizacion);

	@Autowired
	public boolean validarToken(String tokenAutorizacion);

}
