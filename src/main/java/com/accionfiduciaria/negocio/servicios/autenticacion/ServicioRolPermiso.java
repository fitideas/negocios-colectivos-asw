package com.accionfiduciaria.negocio.servicios.autenticacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.excepciones.FaltanDatosObligatoriosException;
import com.accionfiduciaria.excepciones.UsuarioSinPermisosException;
import com.accionfiduciaria.modelo.dtos.OpcionesMenuDTO;
import com.accionfiduciaria.modelo.dtos.PermisoDTO;
import com.accionfiduciaria.modelo.dtos.RolDTO;
import com.accionfiduciaria.modelo.dtos.RolMenuDTO;
import com.accionfiduciaria.modelo.entidad.Permiso;
import com.accionfiduciaria.modelo.entidad.RolPermiso;
import com.accionfiduciaria.modelo.entidad.RolPermisoPK;
import com.accionfiduciaria.negocio.repositorios.RepositorioRolPermiso;

/**
 * Clase que implementa la interfaz {@link IServicioRolPermiso}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
@Transactional
@PropertySource("classpath:mensajes.properties")
public class ServicioRolPermiso implements IServicioRolPermiso {

	@Value("${usuario.sin.permisos}")
	private String mensajeUsuarioSinPermisos;
	
	@Autowired
	private RepositorioRolPermiso repositorioRolPermiso;

	@Override
	public void actualizarPermisosRoles(List<RolDTO> roles) throws FaltanDatosObligatoriosException {
		procesarActualizacion(roles);
	}

	@Override
	public RolMenuDTO obtenerOpcionesRol(String nombreRol) throws UsuarioSinPermisosException {
		return generarMenuRol(
				repositorioRolPermiso.findByRolNombreRolIgnoreCaseAndPermisoEstadoAndPermisoPermisoPadreNotNull(nombreRol, true));
	}

	/**
	 * Método encargado de procesar la actualización de los roles.
	 * 
	 * @param roles El listado de roles a procesar junto con sus permisos.
	 */
	private void procesarActualizacion(List<RolDTO> roles) throws FaltanDatosObligatoriosException {
		for (RolDTO rolDTO : roles) {
			if (rolDTO.getIdRol() == null || rolDTO.getListaFuncionalidades().getFuncionalidades() == null
					|| rolDTO.getListaFuncionalidades().getFuncionalidades().stream()
							.anyMatch(f -> (f.getIdFuncionalidad() == null || f.isPermiteEscribir() == null))) {
				throw new FaltanDatosObligatoriosException();
			}
			eliminarPermisosRol(rolDTO);
			guardarPermisosRol(rolDTO);
		}
	}

	/**
	 * Consulta que permisos posee un rol y los compara con los permisos enviados.
	 * 
	 * Si permiso no se encuentra dentro del listado de permisos enviados lo
	 * elimina.
	 * 
	 * @param rolDTO El rol con el listado de permisos.
	 */
	private void eliminarPermisosRol(RolDTO rolDTO) {
		List<RolPermiso> permisosRolOriginales = repositorioRolPermiso.findByPkIdRolAndPermisoEstado(rolDTO.getIdRol(),
				true);
		permisosRolOriginales.forEach(rolPermiso -> {
			PermisoDTO permisoDTO = new PermisoDTO(rolPermiso.getPk().getIdPermiso());
			if (!rolDTO.getListaFuncionalidades().getFuncionalidades().contains(permisoDTO)) {
				repositorioRolPermiso.delete(rolPermiso);
			}
		});
	}

	/**
	 * Guarda los cambios hechos en los permisos del rol enviado.
	 * 
	 * @param rolDTO El rol junto con los permisos a modificar.
	 */
	private void guardarPermisosRol(RolDTO rolDTO) {
		rolDTO.getListaFuncionalidades().getFuncionalidades()
				.forEach(permisoDTO -> repositorioRolPermiso.save(dtoAEntidad(rolDTO.getIdRol(), permisoDTO)));
	}

	/**
	 * Obtiene la entidad de tipo {@link RolPermiso} a partir del id del rol y de un
	 * dto de tipo {@link PermisoDTO}
	 * 
	 * @param idRol      El id del rol.
	 * @param permisoDTO El dto con los datos del permiso del rol.
	 * @return La entidad correspondiente.
	 */
	private RolPermiso dtoAEntidad(Integer idRol, PermisoDTO permisoDTO) {
		RolPermiso rolPermiso = new RolPermiso();
		rolPermiso.setPk(new RolPermisoPK(idRol, permisoDTO.getIdFuncionalidad()));
		rolPermiso.setPuedeEditar(permisoDTO.isPermiteEscribir());
		return rolPermiso;
	}

	/**
	 * Genera un DTO con la información del rol y las opciones a las que tiene
	 * acceso.
	 * 
	 * @param rolPermisos El lista de rolPermisos asociados al rol.
	 * @return un DTO con la información del rol y las opciones a las que tiene
	 *         acceso.
	 * @throws UsuarioSinPermisosException 
	 */
	private RolMenuDTO generarMenuRol(List<RolPermiso> rolPermisos) throws UsuarioSinPermisosException {
		if (rolPermisos.isEmpty()) {
			throw new UsuarioSinPermisosException(mensajeUsuarioSinPermisos);
		}
		
		HashMap<Permiso, List<RolPermiso>> menu = new HashMap<>();

		rolPermisos.forEach(rp -> {
			if (!menu.containsKey(rp.getPermiso().getPermisoPadre())) {
				menu.put(rp.getPermiso().getPermisoPadre(), new ArrayList<RolPermiso>());
			}
			menu.get(rp.getPermiso().getPermisoPadre()).add(rp);
		});

		List<OpcionesMenuDTO> opcionesMenuDTO = new ArrayList<>();
		menu.forEach(
				(opcion, funcionalidades) -> opcionesMenuDTO.add(rolPermisoAOpcionMenudDTO(opcion, funcionalidades)));

		RolMenuDTO rolMenuDTo = new RolMenuDTO();
		rolMenuDTo.setIdRol(rolPermisos.get(0).getRol().getIdRol());
		rolMenuDTo.setRol(rolPermisos.get(0).getRol().getNombreRol());
		rolMenuDTo.setMenu(opcionesMenuDTO);
		return rolMenuDTo;
	}

	/**
	 * Genera un DTO con la información de una opción de menú junto con sus funcionalidades.
	 * 
	 * @param opcionMenu La opción de menu.
	 * @param funcionalidades Las funcionalidades de la opción.
	 * @return Un DTO con la información de una opción de menú junto con sus funcionalidades.
	 */
	private OpcionesMenuDTO rolPermisoAOpcionMenudDTO(Permiso opcionMenu, List<RolPermiso> funcionalidades) {
		OpcionesMenuDTO opcionMenuDTO = new OpcionesMenuDTO(opcionMenu.getIdPermiso(), opcionMenu.getNombre());
		opcionMenuDTO.setFuncionalidades(
				funcionalidades.stream().map(rp -> rolPermisoAFuncionalidadDTO(rp)).collect(Collectors.toList()));
		return opcionMenuDTO;
	}

	/**
	 * Genera un DTO con la información de una funcionalidad de una opción de menu a la que tiene acceso un rol.
	 * 
	 * @param rolPermiso Un objeto {@link RolPermiso} con los datos de la funcionalidad.
	 * @return Un DTO con la información de una funcionalidad de una opción de menu a la que tiene acceso un rol.
	 */
	private PermisoDTO rolPermisoAFuncionalidadDTO(RolPermiso rolPermiso) {
		return new PermisoDTO(rolPermiso.getPermiso().getIdPermiso(), rolPermiso.getPermiso().getNombre(), rolPermiso.getPuedeEditar());
	}
}
