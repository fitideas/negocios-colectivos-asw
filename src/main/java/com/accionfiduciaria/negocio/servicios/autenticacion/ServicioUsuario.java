/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.autenticacion;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Usuario;
import com.accionfiduciaria.negocio.repositorios.RepositorioUsuario;

/**
 * @author José Santiago Polo Acosta - 13/12/2019 - jpolo@asesoftware.com
 *
 */
@Service
public class ServicioUsuario implements IServicioUsuario{
	
	@Autowired
	RepositorioUsuario repositorioUsuario;
	
	/**
	 * 
	 * @param login
	 * @return
	 */
	@Override
	public Usuario buscarUsuario(String login) {
		return repositorioUsuario.findById(login).orElse(null);
	}
	
	/**
	 * 
	 * @param usuario
	 * @return
	 */
	@Override
	public Usuario guardarUsuario(Usuario usuario) {
		return repositorioUsuario.save(usuario);
	}
	
	@Override
	public Iterable<Usuario> obtenerTodos(){
		return repositorioUsuario.findAll();
		
	}

	@Override
	public Usuario buscarUsuarioPorToken(String token){
		return repositorioUsuario.findFirstByToken(token);
	}
	
}

