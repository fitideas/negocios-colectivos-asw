/**
 * 
 */
package com.accionfiduciaria.negocio.servicios.autenticacion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.excepciones.ParametroNoEncontradoException;
import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.excepciones.UsuarioSinPermisosException;
import com.accionfiduciaria.excepciones.UsuarioSinRolesException;
import com.accionfiduciaria.modelo.dtos.PeticionAccionAutenticacionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionIntrospeccionToken;
import com.accionfiduciaria.modelo.dtos.RespuestaAcccionValidarToken;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionAutenticacionDTO;
import com.accionfiduciaria.modelo.dtos.RolMenuDTO;
import com.accionfiduciaria.modelo.dtos.UsuarioDTO;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.PersonaPK;
import com.accionfiduciaria.modelo.entidad.Usuario;
import com.accionfiduciaria.negocio.componentes.WebUtils;
import com.accionfiduciaria.negocio.servicios.IServicioParametroSistema;
import com.accionfiduciaria.negocio.servicios.IServicioPersona;
import com.accionfiduciaria.negocio.servicios.externos.Autenticacion;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * Clase que implementa la interfaz {@link IServicioAutenticacion}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:mensajes.properties")
@PropertySource("classpath:propiedades.properties")
@Transactional
public class ServicioAutenticacion implements IServicioAutenticacion {

	@Autowired
	private Autenticacion autenticacion;

	@Autowired
	private IServicioUsuario servicioUsuario;

	@Autowired
	private IServicioPersona servicioPersona;

	@Autowired
	private IServicioParametroSistema servicioParametroSistema;

	@Autowired
	private IServicioRolPermiso servicioRolPermiso;
	
	@Autowired
	private IServicioRol servicioRol;
	
	@Autowired
	private WebUtils webUtils;

	@Value("${param.id.url.servicio.autenticacion}")
	private Integer paramIdUrlServicioAutenticacion;
	@Value("${param.id.url.servicio.accion.validar.token}")
	private Integer paramIdUrlServicioValidarToken;
	@Value("${param.id.url.servicio.accion.invalidar.token}")
	private Integer paramIdUrlServicioInvalidarToken;
	@Value("${param.id.app.key}")
	private Integer paramIdAppKey;
	@Value("${param.id.authorization}")
	private Integer paramIdAuthorizationValue;

	@Value("${prop.jwt.member.of}")
	private String paramJwtMemberOf;
	@Value("${prop.jwt.display.name}")
	private String paramJwtDisplayName;
	@Value("${prop.jwt.employee.id}")
	private String paramJwtEmployeeID;
	@Value("${usuario.sin.roles}")
	private String mensajeUsuarioSinRoles;

	@Override
	public UsuarioDTO autenticarUsuario(UsuarioDTO usuarioDTO) throws UnauthorizedException, UsuarioSinRolesException,
			UsuarioSinPermisosException, ParametroNoEncontradoException, Exception {
		RespuestaAccionAutenticacionDTO respuesta = autenticacion.autenticarUsuario(
				this.generarPeticionAutenticacion(usuarioDTO.getNombreUsuario(), usuarioDTO.getClave()));
		return this.procesarRepuestaAutenticacion(respuesta, usuarioDTO.getNombreUsuario());

	}

	@Override
	public void cerrarSesion(String tokenAutorizacion) {
		autenticacion.invalidarToken(
				this.generarPeticionIntrospeccionToken(tokenAutorizacion, this.paramIdUrlServicioInvalidarToken));
	}

	@Override
	public boolean validarToken(String tokenAutorizacion) {
		RespuestaAcccionValidarToken respuestaAcccionValidarToken = autenticacion.validarToken(
				this.generarPeticionIntrospeccionToken(tokenAutorizacion, this.paramIdUrlServicioValidarToken));
		return respuestaAcccionValidarToken.getActive();
	}

	/**
	 * Genera un DTO con los datos de la petición a enviar al srevicio de
	 * autenticación de Acción Fiduciraria.
	 * 
	 * @param login El login del usuario
	 * @param clave La contraseña del usuario.
	 * @return Un DTO con los datos de la petición a enviar al srevicio de
	 *         autenticación de Acción Fiduciraria.
	 * @throws ParametroNoEncontradoException
	 */
	private PeticionAccionAutenticacionDTO generarPeticionAutenticacion(String login, String clave)
			throws ParametroNoEncontradoException {

		String appKey = servicioParametroSistema.obtenerValorParametro(paramIdAppKey);
		String authorization = servicioParametroSistema.obtenerValorParametro(paramIdAuthorizationValue);
		String urlServicioAutenticacion = servicioParametroSistema
				.obtenerValorParametro(paramIdUrlServicioAutenticacion);

		return new PeticionAccionAutenticacionDTO(authorization, login, clave, appKey, urlServicioAutenticacion);
	}

	/**
	 * Genera un DTO con los datos de la petición a enviar al servicio de
	 * introspección de token de Acción Fiduciraria. Se utiliza para generar la
	 * petición de los servicios de validación e inactivación del token.
	 * 
	 * @param tokenAutorizacion El token de autorización del usuario.
	 * @param paramIdUrl        La url del servicio a usar.
	 * @return Un DTO con los datos de la petición a enviar al servicio de
	 *         introspección de token de Acción Fiduciraria.
	 */
	private PeticionAccionIntrospeccionToken generarPeticionIntrospeccionToken(String tokenAutorizacion,
			Integer paramIdUrl) {

		String authorization = servicioParametroSistema.obtenerValorParametro(paramIdAuthorizationValue);
		String urlServicio = servicioParametroSistema.obtenerValorParametro(paramIdUrl);

		return new PeticionAccionIntrospeccionToken(urlServicio, tokenAutorizacion, authorization);
	}

	/**
	 * Decodifica el token JWT obtenido en la respuesta al servicio de autenticación
	 * de Acción Fiduciaria.
	 * 
	 * @param El token JWT a decodificar.
	 * @return jwtToken Los datos del token decodificado.
	 */
	private DatoIdToken decodeJWTToken(String jwtToken) {

		DecodedJWT jwt = JWT.decode(jwtToken);
		Map<String, Claim> claims = jwt.getClaims();

		DatoIdToken datoIdToken = new DatoIdToken();
		List<String> roles = claims.get(paramJwtMemberOf).asList(String.class);
		if(roles != null) {
			datoIdToken.rol = obtenerRol(roles);
		} else {
			datoIdToken.rol = obtenerRol(claims.get(paramJwtMemberOf).asString());
		}
		datoIdToken.nombres = claims.get(paramJwtDisplayName).asString();
		datoIdToken.numeroDocumento = claims.get(paramJwtEmployeeID).asString();

		return datoIdToken;
	}

	private String obtenerRol(String cadenaRol) {
		String rol = "";
		try {
			if( cadenaRol != null) {
				rol = cadenaRol.split(",")[0];
				rol = rol.split("=")[1];
			}
		} catch (Exception e) {
			rol = "";
		}
		return rol;
	}
	
	private String obtenerRol(List<String> gruposLdap) {
		List<String> roles = gruposLdap.stream().map(grupo -> obtenerRol(grupo)).collect(Collectors.toList());
		return roles.stream().filter(rol -> servicioRol.existeRol(rol)).findAny().orElse(null);
	}
	
	/**
	 * Procesa la respuesta de autetnicación: genera el DTO con los datos del
	 * usuario y las opciones de la aplicación a las que tiene acceso.
	 * 
	 * Almacena la información del usuario en la base de datos.
	 * 
	 * @param respuesta Un DTO con los datos de la respuesta de la consulta al
	 *                  servicio de autentiocación de acción.
	 * @return Un DTo con los datos del usuario y las opciones de la aplicación a
	 *         las que tiene acceso.
	 * @throws UsuarioSinRolesException
	 * @throws UsuarioSinPermisosException
	 */
	private UsuarioDTO procesarRepuestaAutenticacion(RespuestaAccionAutenticacionDTO respuesta, String login)
			throws UsuarioSinRolesException, UsuarioSinPermisosException {
		
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		
		DatoIdToken datosIdToken = this.decodeJWTToken(respuesta.getIdToken());
				
		List<RolMenuDTO> roles = new ArrayList<>();
		
		if(datosIdToken.rol == null || datosIdToken.rol.isEmpty()) {
			throw new UsuarioSinRolesException();
		}
		
		roles.add(servicioRolPermiso.obtenerOpcionesRol(datosIdToken.rol));

		usuarioDTO.setNombreUsuario(login);
		usuarioDTO.setNombreCompleto(datosIdToken.nombres);
		usuarioDTO.setToken(respuesta.getAccessToken());
		usuarioDTO.setTiempoVidaToken(respuesta.getExpiresIn());
		usuarioDTO.setRoles(roles);

		Usuario usuario = servicioUsuario.buscarUsuario(login);

		if (usuario == null) {
			usuario = new Usuario();
		}

		if (usuario.getFechaUltimoAcceso() != null) {
			SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
			usuarioDTO.setFechaUltimoAcceso(sf.format(usuario.getFechaUltimoAcceso()));
		}

		if (usuario.getPersona() == null) {
			Persona persona = new Persona();
			persona.setNombreCompleto(datosIdToken.nombres);
			persona.setPersonaPK(new PersonaPK("USR", datosIdToken.numeroDocumento));
			servicioPersona.guardarPersona(persona);
			usuario.setPersona(persona);
		}
		usuario.setLogin(login);
		usuario.setFechaUltimoAcceso(new Date());
		usuario.setToken(respuesta.getAccessToken());
		usuario.setRefreshToken(respuesta.getRefreshToken());
		usuario.setUltimaIP(webUtils.getClientIp());
		usuario.setUltimoRol(datosIdToken.rol);
		servicioUsuario.guardarUsuario(usuario);

		return usuarioDTO;
	}

	/**
	 * Clase interna usada para almacenar los datos del token jwt devuelto por el
	 * servicio de autenticación de Acción Fiduciaria.
	 * 
	 * @author José Polo - jpolo@asesoftware.com
	 *
	 */
	private class DatoIdToken {
		/**
		 * El listado de roles del usuario.
		 */
		protected String rol;
		/**
		 * Los nombres del usuario.
		 */
		protected String nombres;
		/**
		 * El número de documento del usuario.
		 */
		protected String numeroDocumento;
	}
}
