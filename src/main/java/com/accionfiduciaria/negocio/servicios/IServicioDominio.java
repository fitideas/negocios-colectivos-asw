package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.BancoDTO;
import com.accionfiduciaria.modelo.dtos.DatosBasicosValorDominioDTO;
import com.accionfiduciaria.modelo.dtos.DominioCompuestoDTO;
import com.accionfiduciaria.modelo.dtos.DominioSimpleDTO;
import com.accionfiduciaria.modelo.dtos.EliminacionDominioDTO;
import com.accionfiduciaria.modelo.dtos.TipoDocumentoDTO;
import com.accionfiduciaria.modelo.dtos.ValorDominioCompuestoDTO;
import com.accionfiduciaria.modelo.dtos.ValorDominioDTO;
import com.accionfiduciaria.modelo.dtos.ValorDominioSimpleDTO;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;

/**
 * Servicio encargado de manejar la logica de los dominios de la aplicación.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioDominio {

	/**
	 * Busca todos los dominios simples junto con sus valores activos.
	 * 
	 * @return Un listado de dtos con los dominios.
	 * @throws Exception
	 */
	@Autowired
	public List<DominioSimpleDTO> obtenerDominiosSimples() throws Exception;

	/**
	 * Busca todos los dominios compuestos junto con sus valores activos y las
	 * relaciones entre los valores.
	 * 
	 * @return Un listado de dtos con los dominios.
	 * @throws Exception
	 */
	@Autowired
	public List<DominioCompuestoDTO> obtenerDominiosCompuestos() throws Exception;

	/**
	 * Actualiza los valores de los dominios simples enviados.
	 * 
	 * @param dominios Un listado de dtos con los datos a actualizar.
	 * @throws Exception
	 */
	@Autowired
	public void actualizarDominiosSimples(List<DominioSimpleDTO> dominios) throws Exception;

	/**
	 * Actualiza los valores de los dominios compuestos enviados junto con las
	 * relaciones entre valores.
	 * 
	 * @param dominios Un listado de dtos con los datos a actualizar.
	 * @throws Exception
	 */
	@Autowired
	public void actualizarDominiosCompuestos(List<DominioCompuestoDTO> dominios) throws Exception;

	/**
	 * Inactiva un valor de dominio y elimina los registros relacionados.
	 * 
	 * @param eliminacionDominioDTO Contiene el id del valor a eliminar y el id del
	 *                              valor por el que se reemplazara el valor
	 *                              eliminado. Si este valor es nulo solo se
	 *                              realizará el proceso de eliminación.
	 */
	@Autowired
	public void eliminarValorDominio(EliminacionDominioDTO eliminacionDominioDTO)  throws Exception;

	/**
	 * Consulta los valores activos de un determinado dominio.
	 * 
	 * @param codigoDominio El código del dominio.
	 * @return El listado de valores del dominio enviado.
	 */
	@Autowired
	public List<DatosBasicosValorDominioDTO> obtenerValoresDominio(String codigoDominio)  throws Exception;

	@Autowired
	public ValorDominioNegocio buscarValorDominio(Integer idValorDominio);

	@Autowired
	public List<TipoDocumentoDTO> obtenerTiposDocumento() throws Exception;

	@Autowired
	public List<BancoDTO> obtenerbancos();

	@Autowired
	public ValorDominioNegocio buscarPorCodigoDominioYValor(String codigoDominio, String valorDominio);
	
	/**
	 * Busca un registro con el idValorDominio y el codigo de dominio especificado..
	 * 
	 * @param codigoDominio
	 * @param codigoDominio
	 * @return ValorDominioNegocio.
	 */
	@Autowired
	public ValorDominioNegocio buscarValorDominioPorCodDominioYValorDominio(String codigoDominio, Integer idValorDominio);

	
	/**
	 * Busca los tipos de terceros.
	 * 
	 * @param codigo El tipo de tercero: tercero o comite
	 * @return El listado de terceros de acuerdo al tipo enviado.
	 */
	@Autowired
	public List<ValorDominioDTO> obtenerTipoTercero(String codigo);

	/**
	 * Busca la lista de valores de un dominio por el código del mismo.
	 * 
	 * @param codigo El código del dominio a consultar.
	 * @return La lista de valores del dominio consultado.
	 */
	@Autowired
	List<ValorDominioCompuestoDTO> obtenerValoresDominioCompuesto(String codigo);

	@Autowired
	public List<ValorDominioSimpleDTO> obtenerRetencionesAsociadas(Integer idValorDominio);

	@Autowired
	public ValorDominioNegocio buscarPorCodigoDominioYDescripcion(String codigo, String descripcion, Boolean activo);
}
