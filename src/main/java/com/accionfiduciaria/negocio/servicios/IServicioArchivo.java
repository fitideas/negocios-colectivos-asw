package com.accionfiduciaria.negocio.servicios;

import java.io.IOException;
import java.util.List;


import com.accionfiduciaria.enumeradores.TipoExtensionArchivo;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;


public interface IServicioArchivo {

	void guardarArchivo(byte[] archivo, String nombreArchivo)throws IOException;
	
	RespuestaArchivoDTO construirArchivoExcel(List<?> registros, List<String> etiquetas, TipoExtensionArchivo extension)throws IOException, IllegalAccessException;

	String obtenerArchivoBase64(String nombreAchivo);
}
