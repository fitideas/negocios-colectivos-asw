package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.negocio.repositorios.RepositorioRetencionTitular;

/**
 * Clase que implementa la interfaz {@link IServicioRetencionTitular}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
public class ServicioRetencionTitular implements IServicioRetencionTitular {

	@Autowired
	RepositorioRetencionTitular repositorioRetencionTitular;
	
	@Override
	public List<RetencionTitular> buscarRetencionesTitular(Titular titular) {
		List<RetencionTitular> retencionesTitular = repositorioRetencionTitular.findByTitularAndActivo(titular, Boolean.TRUE); 
		return retencionesTitular != null ? retencionesTitular : new ArrayList<>();
	}

	@Override
	public void guardarRetencionesTitular(List<RetencionTitular> retencionesTitular) {
		repositorioRetencionTitular.saveAll(retencionesTitular);
	}

	@Override
	public RetencionTitular guardarRetencionTitular(RetencionTitular retencionTitular) {
		return repositorioRetencionTitular.save(retencionTitular);
	}

	@Override
	public List<RetencionTitular> buscarRetencionesTitularPorTipoNaturalezaNegocio(Integer idTipoNaturalezaNegocio) {
		return repositorioRetencionTitular.findByRetencionTitularPK_tipoNaturalezaNegocioAndActivo(idTipoNaturalezaNegocio, Boolean.TRUE);
	}

	@Override
	public List<RetencionTitular> buscarRetencionesTitularPorIdRetencion(Integer idRetencion) {
		return repositorioRetencionTitular.findByRetencionTitularPK_idRetencionAndActivo(idRetencion, Boolean.TRUE);
	}

	@Override
	public List<RetencionTitular> buscarRetencionesTitularPorCodigoNegocio(String codigoSfc) {
		return repositorioRetencionTitular.findByRetencionTitularPK_codSfcAndActivo(codigoSfc, Boolean.TRUE);
	}

	@Override
	public List<RetencionTitular> buscarRetencionesTitularPorTipoNaturalezaNegocioYTitular(int idTipoNaturalezaNegocio,
			Titular titular) {
		return repositorioRetencionTitular.findByRetencionTitularPK_tipoNaturalezaNegocioAndTitularAndActivo(idTipoNaturalezaNegocio, titular ,Boolean.TRUE);
	}

	@Override
	public List<RetencionTitular> buscarRetencionesTitularPorCodigoNegocioYTipoNegocio(String codigoSfc,
			Integer idTipoNegocio) {
		return repositorioRetencionTitular.findByRetencionTitularPK_codSfcAndRetencionTitularPK_tipoNaturalezaNegocioAndActivo(codigoSfc, idTipoNegocio, Boolean.TRUE);
	}
}
