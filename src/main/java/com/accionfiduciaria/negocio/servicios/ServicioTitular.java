package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.excepciones.TitularNoEncontradoException;
import com.accionfiduciaria.modelo.dtos.BeneficiarioDTO;
import com.accionfiduciaria.modelo.dtos.CotitularDTO;
import com.accionfiduciaria.modelo.dtos.DatosAGuardarCotitularDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaTitularesDTO;
import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.TitularPK;
import com.accionfiduciaria.negocio.repositorios.RepositorioTitular;

/**
 * Clase que implementa a interfaz {@link IServicioTitular}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
@Transactional
@PropertySource("classpath:propiedades.properties")
public class ServicioTitular implements IServicioTitular {

	@Value("${prop.nombre}")
	private String nombre;

	@Value("${prop.documento}")
	private String documento;

	@Autowired
	private RepositorioTitular repositorioTitular;
	
	@Autowired
	private IServicioDominio servicioDominio;

	@Override
	public Titular buscarTitular(String tipoDocumento, String documento, String codigoSFC) {
		return repositorioTitular.findById(new TitularPK(codigoSFC, tipoDocumento, documento)).orElse(null);
	}
	

	@Override
	public Titular guardarTitular(Titular titular) {
		return repositorioTitular.save(titular);
	}

	@Override
	public List<CotitularDTO> obtenerCotitular(String tipoDocumentoTitular, String documentoTitular,
			String codigoNegocio, String argumento, String criterioBuscar) throws TitularNoEncontradoException {

		Titular titular = buscarTitular(tipoDocumentoTitular, documentoTitular, codigoNegocio);

		if (titular == null) {
			throw new TitularNoEncontradoException();
		}

		List<Titular> cotitulares;
		if (Objects.equals(criterioBuscar, nombre)) {
			cotitulares = repositorioTitular.findByParticipacionAndPersonaNombreCompletoContainingIgnoreCase(
					titular.getParticipacion(), argumento);
		} else if (Objects.equals(criterioBuscar, documento)) {
			cotitulares = repositorioTitular.findByParticipacionAndPersona_PersonaPK_NumeroDocumentoContainingIgnoreCase(
					titular.getParticipacion(), argumento);
		} else {
			cotitulares = repositorioTitular.findByParticipacionAndEsCotitular(titular.getParticipacion(), true);
		}

		return cotitulares.parallelStream().map(cotitular -> entidadACotitularDTO(cotitular, titular))
				.filter(Objects::nonNull).collect(Collectors.toList());
	}

	@Override
	public void administrarCotitular(DatosAGuardarCotitularDTO datosAGuardarCotitularDTO)
			throws TitularNoEncontradoException {

		Titular titular = buscarTitular(datosAGuardarCotitularDTO.getTipoDocumentoTitular(),
				datosAGuardarCotitularDTO.getDocumentoTitular(), datosAGuardarCotitularDTO.getCodigoNegocio());

		if (titular == null) {
			throw new TitularNoEncontradoException();
		}
		datosAGuardarCotitularDTO.getCotitulares().forEach(cotitularDTO -> {

			// Búsqueda de titular por Persona y Participacion Negocio
			List<Titular> titularesActualizar = repositorioTitular.buscarPorPersonaParticipacionNegocio(
					new Persona(cotitularDTO.getTipoDocumentoCotitular(), cotitularDTO.getDocumentoCotitular()),
					titular.getParticipacion());

			// Actualiza esCotitular y actualiza el titular.
			if (!titularesActualizar.isEmpty())
				titularesActualizar.forEach(actualizarTitular -> {
					actualizarTitular.setEsCotitular(cotitularDTO.isAsociado());
					repositorioTitular.save(actualizarTitular);
				});

			/*
			 * TODO: Verificar esta actualización si funciona eliminar el mtodo del
			 * repositorio.
			 * repositorioTitular.actualizarCotitularidad(cotitularDTO.isAsociado(), new
			 * Persona(cotitularDTO.getTipoDocumentoCotitular(),
			 * cotitularDTO.getDocumentoCotitular()), titular.getParticipacion());
			 */
		});
	}

	private CotitularDTO entidadACotitularDTO(Titular cotitular, Titular titular) {
		if (Objects.deepEquals(cotitular, titular)) {
			return null;
		}
		CotitularDTO cotitularDTO = new CotitularDTO();
		cotitularDTO.setAsociado(cotitular.getEsCotitular());
		cotitularDTO.setCodigoNegocio(cotitular.getNegocio().getCodSfc());
		cotitularDTO.setDocumentoCotitular(cotitular.getTitularPK().getNumeroDocumento());
		cotitularDTO.setDocumentoTitular(titular.getTitularPK().getNumeroDocumento());
		cotitularDTO.setNombreApellidoCotitular(cotitular.getPersona().getNombreCompleto());
		cotitularDTO.setTipoDocumentoCotitular(cotitular.getTitularPK().getTipoDocumento());
		cotitularDTO.setTipoDocumentoTitular(titular.getTitularPK().getTipoDocumento());
		return cotitularDTO;
	}

	@Override
	public List<RespuestaTitularesDTO> obtenerTitulares(String codigoNegocio) {
		List<Titular> titularesDto = new ArrayList<>();
		repositorioTitular.findAll().forEach(titularesDto::add);
		List<Titular> titulares= repositorioTitular.findByTitularPK_CodSfcContainingIgnoreCase(codigoNegocio);
		return  procesarRespuestaTitulares(titulares);
	}
	
	private List<RespuestaTitularesDTO> procesarRespuestaTitulares(List<Titular> titulares) {
		return titulares.stream().map(titular -> entidadTitularDTO(titular)).collect(Collectors.toList());
	}
	
	private RespuestaTitularesDTO entidadTitularDTO(Titular titular) {
		
		RespuestaTitularesDTO titularDto = new RespuestaTitularesDTO();
		titularDto.setNombreTitular(titular.getPersona().getNombreCompleto());
		if(Boolean.TRUE.equals(titular.getEsCotitular())) {
			titularDto.setTipoTitularidad("COTITULAR");
			Titular titularDelGrupo = titular.getParticipacion().getTitulares().stream().filter(tit->!tit.getEsCotitular()).findAny().orElse(null);
			titularDto.setNumeroEncargoInd(titularDelGrupo != null ? titularDelGrupo.getPersona().getNombreCompleto() : "");
		} else {
			titularDto.setTipoTitularidad("TITULAR");
		}
		titularDto.setTipoDocumentoTitular(titular.getTitularPK().getTipoDocumento());
		titularDto.setDocumentoTitular(titular.getTitularPK().getNumeroDocumento());
		titularDto.setPorcentajeParticipacion(titular.getPorcentaje());
		titularDto.setNumeroDerechosFiduciarios(titular.getNumeroDerechos());
		List<BeneficiarioDTO> beneficiarios = titular.getBeneficiarios()
				.stream()
				.map(beneficiario -> entidadBeneficiarioDTO(beneficiario)).filter(Objects::nonNull)
				.collect(Collectors.toList());
		titularDto.setBeneficiarios(beneficiarios);
		return titularDto;
	}
	
	private BeneficiarioDTO entidadBeneficiarioDTO(Beneficiario beneficiario) {
		BeneficiarioDTO beneficiarioDto = null;
		if(Boolean.TRUE.equals(beneficiario.getEstado())) {
			beneficiarioDto = new BeneficiarioDTO();
			beneficiarioDto.setNumeroCuenta(beneficiario.getMedioPagoPersona().getMedioPagoPersonaPK().getCuenta());
			beneficiarioDto.setNumeroDocumentoBeneficiario(beneficiario.getBeneficiarioPK().getNumeroDocumentoBeneficiario());
			beneficiarioDto.setBeneficiario(beneficiario.getMedioPagoPersona().getPersona().getNombreCompleto());
			beneficiarioDto.setTipoCuenta(servicioDominio.buscarValorDominio(beneficiario.getBeneficiarioPK().getTipoCuenta()).getDescripcion());
			beneficiarioDto.setTipoDocumentoBeneficiario(beneficiario.getBeneficiarioPK().getTipoDocumentoBeneficiario());
		}
		return beneficiarioDto;
	}

	@Override
	public List<Titular> obtenerTitularesPersona(String tipoDocumento, String numeroDocumento) {
		return repositorioTitular.findByPersona(new Persona(tipoDocumento, numeroDocumento));
	}

	@Override
	public List<Titular> buscarTitularesPorDocumento(String numeroDocumento) {
		return repositorioTitular.findByTitularPK_NumeroDocumento(numeroDocumento);
	}


	/**
	 * Este método busca todos los titulares asociados a un codigo de negocio.
	 * 
	 * @author efarias
	 * @param codigoNegocio
	 * @return List<Titular>
	 */
	@Override
	public List<Titular> buscarTitularesPorCodigoNegocio(String codigoNegocio) {
		return repositorioTitular.findByTitularPK_CodSfcAndActivo(codigoNegocio, Boolean.TRUE);
	}


	@Override
	public List<Titular> buscarTitularesPorTipoNaturalezaNegocio(Integer idTipoNaturalezaNegocio) {
		return repositorioTitular.findByIdTipoNaturalezaJuridicaAndActivo(idTipoNaturalezaNegocio, Boolean.TRUE);
	}

	@Override
	public List<Titular> buscarTitularNombrePaginado(String nombreTitular, int pagina, int elementos) {
		return repositorioTitular.findByPersona_nombreCompletoContainingIgnoreCase(nombreTitular, PageRequest.of(pagina - 1, elementos)).toList();
	}

	@Override
	public List<Titular> buscarTitularesPersonaPaginado(String tipoDocumento, String numeroDocumento, int pagina, int elementos) {
		return repositorioTitular.findByPersona(new Persona(tipoDocumento, numeroDocumento), PageRequest.of(pagina - 1, elementos)).toList();
	}


	@Override
	public List<Titular> buscarTitularesPorCodigoNegocioPaginado(String codigoNegocio, int pagina, int elementos) {
		return repositorioTitular.findByTitularPK_CodSfc(codigoNegocio, PageRequest.of(pagina - 1, elementos)).toList();
	}


	@Override
	public List<Titular> buscarTitularesPorDocumentoPaginado(String numeroDocumento, int pagina, int elementos) {
		return repositorioTitular.findByTitularPK_numeroDocumento(numeroDocumento, PageRequest.of(pagina - 1, elementos)).toList();
	}

}
