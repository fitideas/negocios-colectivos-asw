package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.RetencionTipoNegocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioRetencionTipoNegocio;

@Service
public class ServicioRetencionTipoNegocio implements IServicioRetencionTipoNegocio {

	@Autowired
	RepositorioRetencionTipoNegocio repositorioRetencionTipoNegocio;
	
	@Override
	public List<RetencionTipoNegocio> buscarRetencionesTipoNegocio(TipoNegocio tipoNegocio) {
		return repositorioRetencionTipoNegocio.findByTipoNegocioAndActivo(tipoNegocio, Boolean.TRUE);
	}

	@Override
	public RetencionTipoNegocio guardarRetencionTipoNegocio(RetencionTipoNegocio retencionTipoNegocio) {
		return repositorioRetencionTipoNegocio.save(retencionTipoNegocio);
	}

	@Override
	public List<RetencionTipoNegocio> buscarRetencionesTipoNegocioPorNegocio(Negocio negocio) {
		return repositorioRetencionTipoNegocio.findByTipoNegocio_NegocioAndActivo(negocio, Boolean.TRUE);
	}

	@Override
	public List<RetencionTipoNegocio> buscarPorIdTipoNegocio(Integer idTipoNegocio) {
		return repositorioRetencionTipoNegocio.findByRetencionTipoNegocioPK_TipoNegocioAndActivo(idTipoNegocio, Boolean.TRUE);
	}

	@Override
	public List<RetencionTipoNegocio> buscarPorIdRetencion(Integer idRetencion) {
		return repositorioRetencionTipoNegocio.findByRetencionTipoNegocioPK_idRetencionAndActivo(idRetencion, Boolean.TRUE);
	}

}
