package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.ConstantesPago;
import com.accionfiduciaria.negocio.repositorios.RepositorioConstantesPago;

@Service
public class ServicioConstantesPago implements IServicioConstantesPago {

	@Autowired
	RepositorioConstantesPago repositorioConstantesPago;
	
	@Override
	public ConstantesPago buscarConstantesPagoNegocio(String codigoNegocio) {
		return repositorioConstantesPago.findById(codigoNegocio).orElse(null);
	}

	@Override
	public ConstantesPago guardarConstantesPago(ConstantesPago constantesPago) {
		return repositorioConstantesPago.save(constantesPago);
	}

}
