/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.ErrorProcesandoArchivo;
import com.accionfiduciaria.modelo.dtos.ComiteRegistroDTO;
import com.accionfiduciaria.modelo.dtos.MiembroComiteDTO;
import com.accionfiduciaria.modelo.entidad.MiembroComite;

/**
 * @author fcher
 * 
 * Servicio encargado de las operaciones crud de un miembro de comite
 *
 */
public interface IServicioMiembrosComite {


	/**
	 * @param codSFC
	 * @return lista de miembros de un comite, incluyendo los que estan activos e inactivos asi 
	 * como aquellos titulares que aun no se han añadido a la tabla de miembros comite 
	 * @throws BusquedadSinResultadosException 
	 */
	@Autowired
	public List<MiembroComiteDTO> obtenerMiembrosComiteNegocio(String codSFC  ) throws BusquedadSinResultadosException;
	
	

	/**
	 * @param dto
	 * permite guardar un miembro nuevo, cambiar su estado a inactivo
	 *  y/o editar algun otro parametro
	 */
	@Autowired
	public void  guardarMiembroComite(MiembroComiteDTO dto);
	
	/**
	 * @param miembrocomite
	 * transforma una entidad miembro comite
	 *  a una entidad miembrocomitedto
	 */
	@Autowired
	public MiembroComiteDTO aMiembroComiteDTO(MiembroComite mc);


	/**
	 * @param comitedto
	 * recibe un miembrocomitedto
	 *  lo procesa y almacena la informacion
	 */
	@Autowired
	public void guardaInformacionComite(ComiteRegistroDTO comitedto, String token) throws ErrorProcesandoArchivo, Exception;

	@Autowired
	public List<MiembroComite> buscarMiembrosComitePorRol(Integer idEliminar);

	@Autowired
	public MiembroComite guardarMiembroComite(MiembroComite miembroComite);
}
