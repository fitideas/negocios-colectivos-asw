/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitular;
import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucionRetencionesTitular;

/**
 * @author efarias
 *
 */
@Service
public class ServicioDistribucionRetencionesTitular implements IServicioDistribucionRetencionesTitular {

	@Autowired
	private RepositorioDistribucionRetencionesTitular repositorioDistribucionRetencionesTitular;

	@Autowired
	private Utilidades utilidades;

	/**
	 * Cálcula las retenciones en estado activo de los titulares asociados a un
	 * negocio.
	 * 
	 * @author efarias
	 * @param negocio
	 * @param distribucion
	 * @return List<DistribucionRetencionesTitular> -> lista distribución de
	 *         retenciones por titular
	 */
	@Override
	public List<DistribucionRetencionesTitular> calcularRetencionTitularesPorTipoNegocio(TipoNegocio tipoNegocio,
			Distribucion distribucion) {

		List<DistribucionRetencionesTitular> listaDistribucionRetencionTitulares = new ArrayList<>();
		List<Titular> listaTitulares = tipoNegocio.getNegocio().getTitulares();

		if (!listaTitulares.isEmpty()) {
			
			for (Titular titular : listaTitulares) {
				List<RetencionTitular> listaRetencionesTitularTipoNaturaleza = new ArrayList<>();
				List<RetencionTitular> listaRetencionesTitular = new ArrayList<>();
				List<Integer> idRetenciones = new ArrayList<>();
				titular.getRetencionTitularList().forEach(retencionTitular -> {
					if(Boolean.TRUE.equals(retencionTitular.getActivo())) {
						if(retencionTitular.getRetencionTitularPK().getTipoNaturalezaNegocio().equals(tipoNegocio.getTipoNegocioPK().getTipoNegocio())){
							listaRetencionesTitular.add(retencionTitular);
							idRetenciones.add(retencionTitular.getRetencionTitularPK().getIdRetencion());
							
						} else if(retencionTitular.getRetencionTitularPK().getTipoNaturalezaNegocio().equals(titular.getIdTipoNaturalezaJuridica())){
							listaRetencionesTitularTipoNaturaleza.add(retencionTitular);
						}
					}
				});
				listaRetencionesTitularTipoNaturaleza.forEach(retencionTitular -> {					
					if(Boolean.TRUE.equals(retencionTitular.getActivo() 
							&& !idRetenciones.contains(retencionTitular.getRetencionTitularPK().getIdRetencion()))) {
						listaRetencionesTitular.add(retencionTitular);
					}
				});
				if (!listaRetencionesTitular.isEmpty())
					listaDistribucionRetencionTitulares
							.addAll(calcularRetencionesTitular(listaRetencionesTitular, distribucion));
			}
		}
		return listaDistribucionRetencionTitulares;
	}

	/**
	 * Cálcula las retenciones en estado activo para un titular.
	 * 
	 * @author efarias
	 * @param listaRetencionesTitular
	 * @param distribucion
	 * @return List<DistribucionRetencionesTitular> -> lista distribución de
	 *         retenciones por titular
	 */
	public List<DistribucionRetencionesTitular> calcularRetencionesTitular(
			List<RetencionTitular> listaRetencionesTitular, Distribucion distribucion) {

		List<DistribucionRetencionesTitular> listaDistribucionRetencionTitulares = new ArrayList<>();

		// Valor base de cálculo.

		for (RetencionTitular retencionTitular : listaRetencionesTitular) {
			DistribucionRetencionesTitular detalle = new DistribucionRetencionesTitular(
					retencionTitular.getRetencionTitularPK(), distribucion.getDistribucionPK());
			detalle.setUnidad(retencionTitular.getDominioRetencion().getUnidad());
			detalle.setValorConfiguradoImpuesto(new BigDecimal(retencionTitular.getDominioRetencion().getValor()));
			BigDecimal valorBase = distribucion.getTotalDistribucion().multiply(retencionTitular.getTitular().getPorcentaje()).divide(new BigDecimal(100));
			detalle.setValorBase(valorBase);
			detalle.setValorImpuesto(
					utilidades.calcularValorImpuesto(retencionTitular.getDominioRetencion(), valorBase));
			listaDistribucionRetencionTitulares.add(detalle);
		}
		return listaDistribucionRetencionTitulares;
	}

	/**
	 * Guadar una lista de distribución retenciones titular
	 * 
	 * @author efarias
	 */
	@Override
	public void guardarListaDistribucionRetencionesTitular(
			List<DistribucionRetencionesTitular> listaRetencionTitulares) {
		repositorioDistribucionRetencionesTitular.saveAll(listaRetencionTitulares);

	}
}
