/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;

/**
 * @author efarias
 *
 */
public interface IServicioRetencionImpuestoBeneficiario {

	@Autowired
	public void calcularRetencionImpuestoBeneficiarios(List<DistribucionBeneficiario> listaDistribucion,
			Distribucion distribucion);

	@Autowired
	public void agregarRetencionImpuestos(DistribucionBeneficiario distribucionBeneficiarioModificado,
			DistribucionBeneficiario distribucionBeneficiario);

	@Autowired
	public void eliminarRetencionesPorDistribucionBeneficiario(DistribucionBeneficiario distribucionBeneficiario);

}
