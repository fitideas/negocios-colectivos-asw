/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;

/**
 * Servicio que encapsula lógica de negocio de envio de notificaciones a los
 * responsables de un negocio
 * 
 * @author efarias
 *
 */
public interface IServicioNotificacion {

	/**
	 * Este método permite enviar notificaciones vía email a todos los interesados
	 * del negocio que se encuentran parametrizados en el servicio de acción back
	 * 'Buscar Equipo Responsable' por medio de las distintas fechas clave
	 * parametrizadas para cada negocio.
	 * 
	 * @author efarias
	 * @throws BusquedadSinResultadosException 
	 */
	@Autowired
	public void notificarInteresadosNegocio() throws BusquedadSinResultadosException;

}
