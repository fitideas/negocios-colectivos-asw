package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import com.accionfiduciaria.modelo.dtos.AsambleasDTO;
import com.accionfiduciaria.modelo.dtos.AsistenteDTO;
import com.accionfiduciaria.modelo.dtos.ComiteDTO;
import com.accionfiduciaria.modelo.dtos.ComiteRegistroDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoReunionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAsambleasDTO;
import com.accionfiduciaria.modelo.dtos.PeticionHistorialReunionesDTO;
import com.accionfiduciaria.modelo.entidad.Decision;
import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.MiembroMesaDirectiva;
import com.accionfiduciaria.modelo.entidad.Reunion;

public interface IServicioReunion {

	public Reunion guardarReunion(Reunion reunion);

	Reunion guardarReunion(PeticionAsambleasDTO asambleasDTO, FechaClave fechaClave,
			List<MiembroMesaDirectiva> directivos, List<Decision> decisiones, List<AsistenteDTO> listaPersonas, String token);

	public void guardarMiembroDirectivo(MiembroMesaDirectiva miembro);

	public void guardarDecision(Decision decision);

	public void guardarMiembrosMesaDirectiva(List<MiembroMesaDirectiva> miembros);

	void guardarDecisiones(List<Decision> decisiones);

	public Reunion guardarReunionComite(ComiteRegistroDTO comiteDTO, FechaClave fechaClave, String token);

	public List<Reunion> consultaHistoricoReunion(String codigoSFC, String tipoReunion);
	
	public List<HistoricoReunionDTO> consultaHistoricoReuniones(PeticionHistorialReunionesDTO peticion);
	
	AsambleasDTO obtenerReunionAsamblea(String codigoSFC, String fechaHora, String numeroRadicado);
	
	ComiteDTO obtenerReunionComite(String codigoSFC, String fechaHora, String numeroRadicado);
}
