package com.accionfiduciaria.negocio.servicios;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Servicio encargado de enviar los correos electrónicos.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioEnvioCorreo {
	
	/**
	 * Envía un correo electronico.
	 * La configuración de del servidor de correo se encuentra en el archivo application.properties.
	 * 
	 * @param destinatario El correo de destino.
	 * @param asunto El asunto del correo.
	 * @param cuerpoCorreo El cuerpo del correo.
	 * @throws MessagingException
	 */
	@Autowired
	public void enviarCorreo(String destinatario, String asunto, String cuerpoCorreo) throws MessagingException;
}
