/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.FaltanDatosObligatoriosException;
import com.accionfiduciaria.modelo.dtos.ConceptoDTO;
import com.accionfiduciaria.modelo.dtos.ListaIngresosOperacionDTO;
import com.accionfiduciaria.modelo.entidad.IngresoOperacion;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;

/**
 * @author efarias
 *
 */
public interface IServicioIngresoOperacion {
	
	@Autowired
	public ListaIngresosOperacionDTO obtenerListaIngresosOperacion(String tokenAutorizacion, String periodo,
			String codigoNegocio) throws BusquedadSinResultadosException, FaltanDatosObligatoriosException;
	
	@Autowired
	public void guardarListaIngresosOperacion(ListaIngresosOperacionDTO listaIngresosOperacion);
	
	@Autowired
	public List<IngresoOperacion> buscarIngresosOperacion(PagoNegocio pagoNegocio);
	
	@Autowired
	public List<ConceptoDTO> obtenerListaConceptosIngresosDTO(List<IngresoOperacion> listaIngresos);

}
