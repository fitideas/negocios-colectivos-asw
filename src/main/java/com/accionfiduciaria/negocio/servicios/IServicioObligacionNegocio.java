package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

public interface IServicioObligacionNegocio {

	@Autowired
	public List<ObligacionNegocio> buscarObligacionesNegocio(Negocio negocio);

	@Autowired
	public List<ObligacionNegocio> buscarObligacionesTipoNegocio(TipoNegocio tipoNegocio);

	@Autowired
	public ObligacionNegocio guardarObligacionNegocio(ObligacionNegocio obligacion);

	@Autowired
	public List<ObligacionNegocio> buscarObligacionesNegocioPorIdTipoNegocio(Integer idTipoNegocio);

	@Autowired
	public List<ObligacionNegocio> buscarObligacionesNegocioPorIdTipoGasto(Integer idTipoGasto);

}
