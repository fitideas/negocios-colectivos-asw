package com.accionfiduciaria.negocio.servicios;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.accionfiduciaria.excepciones.ArchivoNoValidoException;
import com.accionfiduciaria.excepciones.ErrorAlmacenandoAsambleas;
import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.ErrorProcesandoArchivo;
import com.accionfiduciaria.excepciones.PorcentajeGiroBeneficiariosNoValidoException;
import com.accionfiduciaria.excepciones.TitularSinBeneficiariosException;
import com.accionfiduciaria.excepciones.ErrorAlConectarAccionException;
import com.accionfiduciaria.modelo.dtos.DatosEquipoResponsableDTO;
import com.accionfiduciaria.modelo.dtos.EncargoNegocioDTO;
import com.accionfiduciaria.modelo.dtos.ListaNegociosDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAsambleasDTO;
import com.accionfiduciaria.modelo.dtos.PeticionHistorialReunionesDTO;
import com.accionfiduciaria.modelo.dtos.PeticionInformacionGeneralPagosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaDetalleBeneficiarioPagoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoCambiosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoReunionesDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaSincronizacionDatosNegocioDTO;
import com.accionfiduciaria.modelo.dtos.ValorDominioSimpleDTO;
import com.accionfiduciaria.modelo.dtos.NegocioCompletoDTO;
import com.accionfiduciaria.modelo.dtos.NegocioFiltroDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.RepuestaConsultaNegocioDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionConsultaCesionesSucesionesNegocioDTO;
import com.accionfiduciaria.modelo.entidad.InformacionGeneralPagosDTO;
import com.accionfiduciaria.modelo.entidad.Negocio;

/**
 * Servicio para el manejo de la logica de negocio de los Negocios.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioNegocio {

	/**
	 * Compruba si existe el negocio enviado y que no sea de tipo promotor.
	 * 
	 * @param codigoSFC El codigo de negocio a comprobar.
	 * @return true si el negocio existe, false en caso contrario.
	 */
	@Autowired
	public boolean existeNegocioColectivo(String codigoSFC);

	/**
	 * Busca los datos de un negocio.
	 * 
	 * @param codigoSFC el código del negocio a buscar.
	 * @return Una entidad con los datos del negocio encontrado.
	 */
	@Autowired
	public Negocio obtenerNegocio(String codigoSFC);

	@Autowired
	public ListaNegociosDTO buscarNegocios(String argumento, String criterioBuscar, int pagina, int elementos,
			String tokenAutorizacion);

	@Autowired
	RespuestaArchivoDTO asociarBeneficiarios(MultipartFile archivo, String token, String codigoNegocio)
			throws IOException, ArchivoNoValidoException, ErrorAlConectarAccionException,
			BusquedadSinResultadosException, ErrorProcesandoArchivo, TitularSinBeneficiariosException,
			PorcentajeGiroBeneficiariosNoValidoException, Exception;

	@Autowired
	void guardaInformacionAsambleas(PeticionAsambleasDTO asambleasDTO, String token)
			throws ErrorProcesandoArchivo, ErrorAlmacenandoAsambleas;

	@Autowired
	public RepuestaConsultaNegocioDTO buscarNegocioPorCodigo(String codigoSFC, String tokenAutorizacion);

	@Autowired
	public NegocioCompletoDTO obtenerDatosNegocio(String codigoSFC, String tokenAutorizacion);

	@Autowired
	public void guardarInformacionNegocio(NegocioCompletoDTO negocioCompletoDTO, String tokenAutorizacion);

	@Autowired
	public List<EncargoNegocioDTO> obtenerEncargosNegocio(String codigoNegocio, String tokenAutorizacion);

	@Autowired
	InformacionGeneralPagosDTO obtenerPagosNegocio(PeticionInformacionGeneralPagosDTO peticion,
			String tokenAutorizacion) throws Exception;

	@Autowired
	public RespuestaDetalleBeneficiarioPagoDTO obtenerBeneficiariosPorPago(String codigoNegocio, String tipoNegocio,
			String periodo);

	RespuestaArchivoDTO obtenerArchivoDetallePagos(String codigoNegocio, String tipoNegocio, String periodo)
			throws Exception;

	@Autowired
	public RespuestaSincronizacionDatosNegocioDTO sincronizarYValidarDatos(String codigoNegocio,
			String tokenAutorizacion);

	@Autowired
	public RespuestaHistoricoDTO consultaHistoricoReunion(String codigoNegocio, String tipoHistorico) throws Exception;
	
	@Autowired
	public RespuestaHistoricoReunionesDTO consultaHistoricoReuniones(PeticionHistorialReunionesDTO peticion, String tokenAuthorization) throws Exception;
	
	@Autowired
	public List<NegocioFiltroDTO> obtenerNegocios() throws Exception;

	
	@Autowired
	public RespuestaAccionConsultaCesionesSucesionesNegocioDTO[] buscarHistoricoCesionesSucesiones(String codigoNegocio,
			String fechaInicio, String fechaFin, String tokenAutorizacion);

	@Autowired
	RespuestaHistoricoCambiosDTO listaNegocioHistorialCambios(ParametrosFiltroDTO peticion, String tokenAutorizacion);
	
	@Autowired
	public List<Negocio> obtenerTodosNegocios();

	/**
	 * 
	 * @param codigoNegocio
	 * @param tokenAutorizacion
	 * @return
	 */
	@Autowired
	public List<DatosEquipoResponsableDTO> cargarEquipoResponsable(String codigoNegocio, String tokenAutorizacion);

	@Autowired
	public List<ValorDominioSimpleDTO> obtenerTiposNegocio(String codigoNegocio);

	@Autowired
	public List<Negocio> buscarNegocioPorNombrePaginado(String nombreNegocio, int pagina, int elementos);

	@Autowired
	public List<Negocio> buscarNegociosPorDocumentoTitular(String tipoDocumento, String numeroDocumento, int pagina,
			int elementos);
}
