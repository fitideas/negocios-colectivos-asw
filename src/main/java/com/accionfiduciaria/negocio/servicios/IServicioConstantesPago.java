package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.ConstantesPago;

public interface IServicioConstantesPago {
	
	@Autowired
	public ConstantesPago buscarConstantesPagoNegocio(String codigoNegocio);

	@Autowired
	public ConstantesPago guardarConstantesPago(ConstantesPago constantesPago);
	
}
