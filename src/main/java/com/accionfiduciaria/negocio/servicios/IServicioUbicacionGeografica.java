/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.UbicacionGeograficaDTO;
import com.accionfiduciaria.modelo.entidad.UbicacionGeografica;

/**
 * Servicio de manejo de logica de ubicación geográfica.
 * 
 * @author efarias
 *
 */
public interface IServicioUbicacionGeografica {

	/**
	 * Método que obtiene los paises en estado activo.
	 * 
	 * @author efarias
	 * @return Lista paises en estado activo.
	 */
	@Autowired
	public List<UbicacionGeograficaDTO> obtenerPaisesActivos();

	/**
	 * Método que recibe como parámetro un código de país y obtiene los
	 * departamentos asociados.
	 * 
	 * @author efarias
	 * @param codigoPais
	 * @return Lista de departamentos asociados a un país.
	 */
	@Autowired
	public List<UbicacionGeograficaDTO> obtenerDepartamentosPorPais(String codigoPais);

	/**
	 * Método que recibe como parámetro un código de departamento y obtiene los
	 * municipios asociados.
	 * 
	 * @author efarias
	 * @param codigoDepartamento
	 * @return Lista de departamentos asociados a un país.
	 */
	@Autowired
	public List<UbicacionGeograficaDTO> obtenerMunicipiosPorDepartamento(String codigoDepartamento);
	
	@Autowired
	public UbicacionGeografica obtenerUbicacionGeograficaPorCodigo(String codigo);

}
