package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.dtos.ReprocesoPagoBeneficiarioDTO;
import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersona;
import com.accionfiduciaria.modelo.entidad.Persona;

public interface IServicioMedioPagoPersona {

	@Autowired
	public MedioPagoPersona guardarMedioPagoPersona(MedioPagoPersona medioPagoPersona);
	
	@Autowired
	public MedioPagoPersona buscarMedioPagoPersona(MedioPagoPersona medioPagoPersona);
	
	@Autowired
	public MedioPagoPersona verificarCrearMedioPagoPersona(ReprocesoPagoBeneficiarioDTO datosBeneficiario,
			MedioPago medioPago);

	@Autowired
	public List<MedioPagoPersona> buscarTodosMediosPagoPersona(Persona persona);

}
