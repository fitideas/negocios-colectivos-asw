package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.NoExisteUsuarioException;
import com.accionfiduciaria.modelo.dtos.HistoricoCambioDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.SolicitudCambioDatosDTO;
import com.accionfiduciaria.modelo.dtos.SolicitudCambioDatosGuardadaDTO;
import com.accionfiduciaria.modelo.entidad.SolicitudCambiosDatos;

/**
 * Servicio encargado de manejar la logica de las solicitudes de cambios de datos de un vinculado.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioSolicitudCambiosDatos {
	
	/**
	 * Obtiene la lista coompleta de solicitudes de cambios de datos de un vinculado.
	 * 
	 * @param tipoDocumento El tipo de documento del vinculado.
	 * @param documento El número de documento del vinculado.
	 * @return Una lista con los datos de las solicitudes encontradas.
	 * @throws Exception
	 */
	@Autowired
	public List<SolicitudCambioDatosGuardadaDTO> obtenerSolicitudes(String tipoDocumento, String documento) throws Exception;

	/**
	 * Envía el correo con la solicitud de cambio de datos y guarda los registros correspondientes en la base de datos.
	 * 
	 * @param solicitudCambioDatos
	 */
	@Autowired
	public void enviarSolicitud(List<SolicitudCambioDatosDTO> solicitudCambioDatos) throws NoExisteUsuarioException, MessagingException, Exception;
	
	List<SolicitudCambiosDatos> consultarConFiltros(ParametrosFiltroDTO dto) ;
	
	List<HistoricoCambioDTO> consultarHistoricoSolicitudCambios(ParametrosFiltroDTO dto, String columna);
}
