/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.dtos.EncargoDTO;
import com.accionfiduciaria.modelo.dtos.ListaVinculadosDTO;
import com.accionfiduciaria.modelo.dtos.NovedadesJuridicasDTO;
import com.accionfiduciaria.modelo.dtos.NovedadesJuridicasDetalleDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaBusquedadVinculadoDTO;

/**
 * Servicio para el manejo de la logica de negocio de los vinculados a Acción Fiduciaria.
 * 
 * @author José Santiago Polo Acosta - 16/12/2019 - jpolo@asesoftware.com
 *
 */
@Service
public interface IServicioVinculados {

	/**
	 * Realiza la consulta de un listado de vinculados usando el nombre o un número de documento parcial. 
	 * 
	 * @param argumento Contiene el valor del criterio de busquedad.
	 * @param criterioBuscar Indica por cual criterio se realizara la búsqueda: nombre o documento.
	 * @param pagina la pagina inicial en la que se ubicara el listado de resultados.
	 * @param elementos El número de elementos que se mostrará por pagina.
	 * @param authorizationToken El token de autorización.
	 * @return Un DTO con el listado de resultados.
	 * @throws Exception
	 */
	@Autowired
	public ListaVinculadosDTO buscarListaVinculados(String argumento, String criterioBuscar, int pagina,
			int elementos, String tokenAutorizacion) throws Exception;
	
	/**
	 * Realiza la consulta de los datos de un vinculado usando su número de documento.
	 * 
	 * @param documento El número de documento del vinculado.
	 * @param authorizationToken El token de autorización.
	 * @return Un DTO con los datos del vinculado encontrado.
	 * @throws Exception
	 */
	@Autowired
	public RespuestaBusquedadVinculadoDTO buscarBeneficiarioPorDocumento(String documento, String authorizationToken) throws Exception;

	@Autowired
	public EncargoDTO validarEncargo(String numeroEncargo, String documento, String tokenAutorizacion);
	
	@Autowired
	public List<NovedadesJuridicasDTO> obtenerNovedadesJuridicas(String codigoNegocio, String tipoDocumento, String documento, int pagina,
			int elementos, String tokenAutorizacion);
	
	@Autowired
	public NovedadesJuridicasDetalleDTO detalleNovedadJuridica(String codigoNegocio, String tipoDocumento, String documento, String codigoEvento,
			String tokenAutorizacion);

}
