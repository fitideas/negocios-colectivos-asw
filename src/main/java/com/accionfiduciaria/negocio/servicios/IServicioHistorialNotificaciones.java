/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.HistorialNotificaciones;

/**
 * @author efarias
 *
 */
public interface IServicioHistorialNotificaciones {
	
	@Autowired
	public void guardarAuditoriaNotificaciones(HistorialNotificaciones historialNotificaciones);

}
