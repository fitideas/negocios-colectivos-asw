package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.BancoDTO;
import com.accionfiduciaria.modelo.dtos.DatosBasicosValorDominioDTO;
import com.accionfiduciaria.modelo.dtos.DominioCompuestoDTO;
import com.accionfiduciaria.modelo.dtos.DominioSimpleDTO;
import com.accionfiduciaria.modelo.dtos.EliminacionDominioDTO;
import com.accionfiduciaria.modelo.dtos.RelacionValorDominioDTO;
import com.accionfiduciaria.modelo.dtos.TipoDocumentoDTO;
import com.accionfiduciaria.modelo.dtos.ValorDominioCompuestoDTO;
import com.accionfiduciaria.modelo.dtos.ValorDominioDTO;
import com.accionfiduciaria.modelo.dtos.ValorDominioSimpleDTO;
import com.accionfiduciaria.modelo.entidad.Dominio;
import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.MiembroComite;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.modelo.entidad.RelacionEntreDominios;
import com.accionfiduciaria.modelo.entidad.RelacionEntreDominiosPK;
import com.accionfiduciaria.modelo.entidad.RetencionTipoNegocio;
import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.RetencionTitularPK;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioDominio;
import com.accionfiduciaria.negocio.repositorios.RepositorioValorDominioNegocio;

/**
 * Clase que implementa la interfaz {@link IServicioDominio}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
@Transactional
@PropertySource("classpath:propiedades.properties")
public class ServicioDominio implements IServicioDominio {

	@Value("${cod.dominio.tipo.documentos}")
	private String codigoTipoDocumentos;

	@Value("${cod.dominio.banco}")
	private String codigoDominioBanco;

	@Value("${codigo.tipo.banco.traslado}")
	private String codigoTipoBancoTraslado;

	@Value("${codigo.tipo.banco.cheque}")
	private String codigoTipoBancoCheque;

	@Value("${codigo.tipo.banco.otro}")
	private String codigoTipoBancoOtro;
	
	@Value("${cod.dominio.rol.tercero}")
	private String codigoRolTercero;
	
	@Value("${valor.true}")
	private String valorTrue;
	
	@Value("${cod.dominio.retenciones}")
	private String codigoDominioRetenciones;
	
	@Value("${cod.dominio.tipo.negocio}")
	private String codigoDominioTipoNegocio;
	
	@Value("${cod.dominio.costo.tipo.cuenta}")
	private String codigoDominioTipoCuenta;
	
	@Value("${cod.dominio.rol.tercero}")
	private String codigoDominioRolTercero;
	
	@Value("${cod.dominio.tipo.naturaleza}")
	private String codigoDominioTipoNaturaleza;
	
	@Value("${cod.dominio.tipo.gasto}")
	private String codigoDominioTipoGasto;
	
	@Value("${cod.dominio.tipo.fecha.clave}")
	private String codigoDominioFechaClave;

	@Autowired
	private IServicioRelacionEntreDominios servicioRelacionEntreDominios;
	
	@Autowired
	private IServicioRetencionTitular servicioRetencionTitular;
	
	@Autowired 
	private IServicioTitular servicioTitular;
	
	@Autowired
	private IServicioTipoNegocio servicioTipoNegocio;
	
	@Autowired
	private IServicioRetencionTipoNegocio servicioRetencionTipoNegocio;
	
	@Autowired
	private IServicioObligacionNegocio servicioObligacionNegocio;
	
	@Autowired
	private IServicioMiembrosComite servicioMiembroComite;
	
	@Autowired
	private IServicioFechaClave servicioFechaClave;
	
	@Autowired
	private RepositorioDominio repositorioDominio;

	@Autowired
	private RepositorioValorDominioNegocio repositorioValorDominioNegocio;

	@Override
	public List<DominioSimpleDTO> obtenerDominiosSimples() throws Exception {
		return repositorioDominio.obtenerDominiosSimples().stream().map(ServicioDominio::entidadADominioSimpleDTO)
				.collect(Collectors.toList());
	}

	@Override
	public List<DominioCompuestoDTO> obtenerDominiosCompuestos() throws Exception {
		return repositorioDominio.obtenerDominiosCompuestos().stream().map(ServicioDominio::entidadADominioCompuestoDTO)
				.collect(Collectors.toList());
	}

	@Override
	public void actualizarDominiosSimples(List<DominioSimpleDTO> dominiosSimples) throws Exception {
		procesarActualizacionDominiosSimples(dominiosSimples);
	}

	@Override
	public void actualizarDominiosCompuestos(List<DominioCompuestoDTO> dominiosCompuestos) throws Exception {
		procesarActualizacionDominiosCompuestos(dominiosCompuestos);
	}

	@Override
	public void eliminarValorDominio(EliminacionDominioDTO eliminacionDominioDTO) throws Exception {
		procesarEliminacionDominios(eliminacionDominioDTO);
	}

	@Override
	public List<DatosBasicosValorDominioDTO> obtenerValoresDominio(String codigoDominio) {
		return repositorioValorDominioNegocio.findByDominioCodDominioAndActivo(codigoDominio, Boolean.TRUE).stream()
				.map(ServicioDominio::entidadADatoBasicoDominioDTO).collect(Collectors.toList());
	}

	@Override
	public ValorDominioNegocio buscarValorDominio(Integer idValorDominio) {
		return repositorioValorDominioNegocio.findById(idValorDominio).orElse(null);
	}

	@Override
	public List<TipoDocumentoDTO> obtenerTiposDocumento() throws Exception {
		return repositorioValorDominioNegocio.findByDominioCodDominioAndActivo(codigoTipoDocumentos, Boolean.TRUE)
				.stream().map(valor -> new TipoDocumentoDTO(valor.getValor(), valor.getDescripcion()))
				.collect(Collectors.toList());
	}

	@Override
	public List<BancoDTO> obtenerbancos() {
		return repositorioValorDominioNegocio.findByDominioCodDominioAndActivo(codigoDominioBanco, Boolean.TRUE)
				.stream()
				.filter(banco -> !banco.getValor().equals(codigoTipoBancoCheque)
						&& !banco.getValor().equals(codigoTipoBancoOtro)
						&& !banco.getValor().equals(codigoTipoBancoTraslado))
				.map(ban -> new BancoDTO(ban.getIdValorDominio(), ban.getDescripcion(), ban.getValor()))
				.collect(Collectors.toList());
	}

	@Override
	public ValorDominioNegocio buscarPorCodigoDominioYValor(String codigoDominio, String valorDominio) {
		return repositorioValorDominioNegocio.findByDominioCodDominioAndValorIgnoreCaseAndActivo(codigoDominio, valorDominio, Boolean.TRUE);
	}
	
	@Override
	public ValorDominioNegocio buscarValorDominioPorCodDominioYValorDominio(String codigoDominio,
			Integer idValorDominio) {
		return repositorioValorDominioNegocio.buscarValorDominioPorCodDominioYValorDominio(codigoDominio,
				idValorDominio);
	}
	
	@Override
	public List<ValorDominioDTO> obtenerTipoTercero(String codigo) {
		List<ValorDominioNegocio> tiposTerceros = repositorioValorDominioNegocio
				.findByDominio_CodDominioAndRelacionEntreDominiosList1_ValorDominioNegocio_DescripcionAndRelacionEntreDominiosList1_Valor(
						codigoRolTercero, codigo, valorTrue);
		return tiposTerceros.stream().map(tipoTercero -> new ValorDominioDTO(tipoTercero.getIdValorDominio(),
				tipoTercero.getDescripcion(), tipoTercero.getActivo())).collect(Collectors.toList());
	}

	@Override
	public List<ValorDominioCompuestoDTO> obtenerValoresDominioCompuesto(String codigo) {
		return repositorioDominio.obtenerValoresDominioCompuesto(codigo).stream().map(ServicioDominio::entidadAValorDominioCompuestoDTO).filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	@Override
	public List<ValorDominioSimpleDTO> obtenerRetencionesAsociadas(Integer idValorDominio) {
		List<RelacionEntreDominios> retencionesAsociadas = servicioRelacionEntreDominios
				.buscarValoresAplicanRelacion(idValorDominio);
		return retencionesAsociadas.stream().filter(
				relacion -> relacion.getValor().equals(valorTrue) && relacion.getValorDominioNegocio().getActivo())
				.map(relacion -> new ValorDominioSimpleDTO(relacion.getValorDominioNegocio().getIdValorDominio(),
						relacion.getValorDominioNegocio().getDescripcion(),
						relacion.getValorDominioNegocio().getValor(), relacion.getValorDominioNegocio().getUnidad(),
						relacion.getValorDominioNegocio().getActivo()))
				.collect(Collectors.toList());
	}
	
	@Override
	public ValorDominioNegocio buscarPorCodigoDominioYDescripcion(String codigo, String descripcion, Boolean activo) {
		return repositorioValorDominioNegocio.findByDominio_codDominioAndDescripcionIgnoreCaseAndActivo(codigo, descripcion, activo);
	}

	/**
	 * Convierte un objeto {@link Dominio} a un objeto {@link DominioSimpleDTO}
	 * 
	 * @param dominio El objeto a convertir.
	 * @return El DTO correspondiente al objeto enviado.
	 */
	private static DominioSimpleDTO entidadADominioSimpleDTO(Dominio dominio) {
		DominioSimpleDTO dominioSimpleDTO = new DominioSimpleDTO();
		dominioSimpleDTO.setCodigo(dominio.getCodDominio());
		dominioSimpleDTO.setNombre(dominio.getNombreDominio().toUpperCase());
		dominioSimpleDTO.setPermiteEliminar(dominio.getPermiteEliminar());
		dominioSimpleDTO
				.setValores(dominio.getValoresDominio().stream().map(ServicioDominio::entidadAValorDominioSimpleDTO)
						.filter(Objects::nonNull).collect(Collectors.toList()));
		return dominioSimpleDTO;
	}

	/**
	 * Convierte un objeto {@link Dominio} a un objeto {@link DominioCompuestoDTO}
	 * 
	 * @param dominio El objeto a convertir.
	 * @return El DTO correspondiente al objeto enviado.
	 */
	private static DominioCompuestoDTO entidadADominioCompuestoDTO(Dominio dominio) {
		DominioCompuestoDTO dominioCompuestoDTO = new DominioCompuestoDTO();
		dominioCompuestoDTO.setCodigo(dominio.getCodDominio());
		dominioCompuestoDTO.setNombre(dominio.getNombreDominio().toUpperCase());
		dominioCompuestoDTO.setPermiteEliminar(dominio.getPermiteEliminar());
		dominioCompuestoDTO
				.setValores(dominio.getValoresDominio().stream().map(ServicioDominio::entidadAValorDominioCompuestoDTO)
						.filter(Objects::nonNull).collect(Collectors.toList()));
		return dominioCompuestoDTO;
	}

	/**
	 * Convierte un objeto {@link ValorDominioNegocio} a un objeto
	 * {@link ValorDominioSimpleDTO}
	 * 
	 * @param valorDominioNegocio El objeto a convertir
	 * @return EL DTO correspondiente al objeto enviado.
	 */
	public static ValorDominioSimpleDTO entidadAValorDominioSimpleDTO(ValorDominioNegocio valorDominioNegocio) {

		ValorDominioSimpleDTO valorDominioSimpleDTO = null;
		if (Boolean.TRUE.equals(valorDominioNegocio.getActivo())) {
			valorDominioSimpleDTO = new ValorDominioSimpleDTO();
			valorDominioSimpleDTO.setId(valorDominioNegocio.getIdValorDominio());
			valorDominioSimpleDTO.setDescripcion(valorDominioNegocio.getDescripcion().toUpperCase());
			valorDominioSimpleDTO.setValor(valorDominioNegocio.getValor() != null ? valorDominioNegocio.getValor().toUpperCase() : "");
			valorDominioSimpleDTO.setActivo(valorDominioNegocio.getActivo());
			valorDominioSimpleDTO.setUnidad(valorDominioNegocio.getUnidad());
		}
		return valorDominioSimpleDTO;
	}

	/**
	 * Convierte un objeto {@link ValorDominioNegocio} a un objeto
	 * {@link ValorDominioCompuestoDTO}
	 * 
	 * @param valorDominioNegocio El objeto a convertir
	 * @return EL DTO correspondiente al objeto enviado.
	 */
	private static ValorDominioCompuestoDTO entidadAValorDominioCompuestoDTO(ValorDominioNegocio valorDominioNegocio) {
		ValorDominioCompuestoDTO valorDominioCompuestoDTO = null;
		if (Boolean.TRUE.equals(valorDominioNegocio.getActivo())) {
			valorDominioCompuestoDTO = new ValorDominioCompuestoDTO();
			valorDominioCompuestoDTO.setId(valorDominioNegocio.getIdValorDominio());
			valorDominioCompuestoDTO.setDescripcion(valorDominioNegocio.getDescripcion().toUpperCase());
			valorDominioCompuestoDTO.setActivo(valorDominioNegocio.getActivo());
			valorDominioCompuestoDTO.setValores(valorDominioNegocio.getRelacionEntreDominiosList1().stream()
					.map(ServicioDominio::entidadARelacionValorDominioDTO).filter(Objects::nonNull).collect(Collectors.toList()));
		}
		return valorDominioCompuestoDTO;
	}

	/**
	 * Convierte un objeto {@link RelacionEntreDominios} a un objeto
	 * {@link RelacionValorDominioDTO}
	 * 
	 * @param relacionEntreDominios El objeto a convertir
	 * @return EL DTO correspondiente al objeto enviado.
	 */
	private static RelacionValorDominioDTO entidadARelacionValorDominioDTO(
			RelacionEntreDominios relacionEntreDominios) {
		RelacionValorDominioDTO relacionValorDominioDTO = null;
		if (Boolean.TRUE.equals(relacionEntreDominios.getValorDominioNegocio().getActivo())) {
			relacionValorDominioDTO = new RelacionValorDominioDTO();
			relacionValorDominioDTO.setId(relacionEntreDominios.getValorDominioNegocio().getIdValorDominio());
			relacionValorDominioDTO.setDescripcion(relacionEntreDominios.getValorDominioNegocio().getDescripcion());
			relacionValorDominioDTO.setValor(relacionEntreDominios.getValor());
		}
		return relacionValorDominioDTO;
	}

	/**
	 * Realiza el proceso de actualización de los dominios simples.
	 * 
	 * @param dominiosDTO Un listado de DTO con los datos de los dominios a
	 *                    actualizar.
	 */
	private void procesarActualizacionDominiosSimples(List<DominioSimpleDTO> dominiosDTO) {
		List<Dominio> dominios = dominiosDTO.stream().map(dominio -> dtoSimpleADominio(dominio))
				.filter(Objects::nonNull).collect(Collectors.toList());
		dominios = StreamSupport.stream(repositorioDominio.saveAll(dominios).spliterator(), false)
				.collect(Collectors.toList());
		servicioRelacionEntreDominios
				.agregarRelacionRetencion(dominios.get(dominios.indexOf(new Dominio(codigoDominioRetenciones))).getValoresDominio());
	}

	/**
	 * Realiza el proceso de actualización de los dominios compuestos.
	 * 
	 * @param dominiosDTO Un listado de DTO con los datos de los dominios a
	 *                    actualizar.
	 */
	private void procesarActualizacionDominiosCompuestos(List<DominioCompuestoDTO> dominiosDTO) {
		List<Dominio> dominios = dominiosDTO.stream().map(dominio -> dtoCompuestoADominio(dominio))
				.filter(Objects::nonNull).collect(Collectors.toList());
		repositorioDominio.saveAll(dominios);
	}

	/**
	 * Convierte un objeto {@link DominioSimpleDTO} a un objeto {@link Dominio}
	 * 
	 * @param dominioDTO El objeto a convertir.
	 * @return La entidad correspondiente al DTO enviado.
	 */
	private Dominio dtoSimpleADominio(DominioSimpleDTO dominioDTO) {
		Dominio dominio = repositorioDominio.findById(dominioDTO.getCodigo()).orElse(null);
		if (dominio != null) {
			dominio.setValoresDominio(dominioDTO.getValores().stream()
					.map(valorDTO -> dtoSimpleAValorDominioNegocio(valorDTO, dominio)).collect(Collectors.toList()));
		}
		return dominio;
	}

	/**
	 * Convierte un objeto {@link ValorDominioSimpleDTO} a un objeto
	 * {@link ValorDominioNegocio}
	 * 
	 * @param valorDominioSimpleDTO El objeto a convertir.
	 * @param dominio               El dominio al que pertenece el valor.
	 * @return La entidad correspondiente al DTO enviado.
	 */
	private static ValorDominioNegocio dtoSimpleAValorDominioNegocio(ValorDominioSimpleDTO valorDominioSimpleDTO,
			Dominio dominio) {
		ValorDominioNegocio valorDominioNegocio = new ValorDominioNegocio();
		valorDominioNegocio.setIdValorDominio(valorDominioSimpleDTO.getId());
		valorDominioNegocio.setDominio(dominio);
		valorDominioNegocio.setValor(valorDominioSimpleDTO.getValor());
		valorDominioNegocio.setUnidad(valorDominioSimpleDTO.getUnidad());
		valorDominioNegocio.setActivo(valorDominioSimpleDTO.getActivo());
		valorDominioNegocio.setDescripcion(valorDominioSimpleDTO.getDescripcion());
		return valorDominioNegocio;
	}

	/**
	 * Convierte un objeto {@link DominioCompuestoDTO} a un objeto {@link Dominio}.
	 * 
	 * @param dominioDTO El objeto a convertir.
	 * @return La entidad correspondiente al objeto enviado.
	 */
	private Dominio dtoCompuestoADominio(DominioCompuestoDTO dominioDTO) {
		Dominio dominio = repositorioDominio.findById(dominioDTO.getCodigo()).orElse(null);
		if (dominio != null) {
			dominio.setValoresDominio(dominioDTO.getValores().stream()
					.map(valorDTO -> obtenerValorDominioNegocio(valorDTO, dominio)).collect(Collectors.toList()));
		}
		return dominio;
	}

	/**
	 * Obtiene un objeto {@link ValorDominioNegocio} a partir de un DTO. Si La
	 * entidad no tiene ID lo alamacena en la base de datos y obtiene el id.
	 * 
	 * @param valorDominioCompuestoDTO El DTO a partir del cual se obtendra la
	 *                                 entidad.
	 * @param dominio                  El dominio al que pertence el valor.
	 * @return La entidad correpsondiente al DTO enviado.
	 */
	private ValorDominioNegocio obtenerValorDominioNegocio(ValorDominioCompuestoDTO valorDominioCompuestoDTO,
			Dominio dominio) {
		ValorDominioNegocio valorDominioNegocio = new ValorDominioNegocio();
		valorDominioNegocio.setIdValorDominio(valorDominioCompuestoDTO.getId());
		valorDominioNegocio.setDominio(dominio);
		valorDominioNegocio.setValor(valorDominioCompuestoDTO.getDescripcion());
		valorDominioNegocio.setDescripcion(valorDominioCompuestoDTO.getDescripcion());
		valorDominioNegocio.setActivo(valorDominioCompuestoDTO.getActivo());

		if (valorDominioNegocio.getIdValorDominio() == null) {
			valorDominioNegocio
					.setIdValorDominio(repositorioValorDominioNegocio.save(valorDominioNegocio).getIdValorDominio());
		}

		valorDominioNegocio.setRelacionEntreDominiosList1(valorDominioCompuestoDTO.getValores().stream().distinct()
				.map(relacionValorDTO -> obtenerRelacionEntreDominios(relacionValorDTO, valorDominioNegocio))
				.collect(Collectors.toList()));
		return valorDominioNegocio;
	}

	/**
	 * Obtiene un objeto {@link RelacionEntreDominios}
	 * 
	 * @param relacionDTO         El DTO que contiene los datos del registro.
	 * @param valorDominioNegocio El valor al que se asociara la relación.
	 * @return La entidad correspondiente.
	 */
	private static RelacionEntreDominios obtenerRelacionEntreDominios(RelacionValorDominioDTO relacionDTO,
			ValorDominioNegocio valorDominioNegocio) {
		RelacionEntreDominios relacionEntreDominios = new RelacionEntreDominios();
		relacionEntreDominios.setRelacionEntreDominiosPK(
				new RelacionEntreDominiosPK(valorDominioNegocio.getIdValorDominio(), relacionDTO.getId()));
		relacionEntreDominios.setValor(relacionDTO.getValor());
		return relacionEntreDominios;
	}

	/**
	 * Realiza el proceso de eliminación de valores de dominios: Inactiva el valor
	 * de dominio en la tabla y elimina las relaciones con las demás entidades. En
	 * caso de enviarse el parámetro idReemplazar realiza el reemplazo del valor en
	 * las tablas correspondientes.
	 * 
	 * 
	 * @param eliminacionDominioDTO DTO con los datos del valor de dominio a
	 *                              eliminar y el valor del dominio por el que se
	 *                              reemplazara el valor eliminado. Si este último
	 *                              llega nulo, solo se realizara el proceso de
	 *                              eliminación.
	 * @throws Exception 
	 */
	private void procesarEliminacionDominios(EliminacionDominioDTO eliminacionDominioDTO) throws Exception {
		
		ValorDominioNegocio valorDominio = repositorioValorDominioNegocio.findById(eliminacionDominioDTO.getIdEliminar()).orElse(null);
		
		if(valorDominio == null) {
			throw new Exception();
		}
		
		if(valorDominio.getDominio().getCodDominio().equals(codigoDominioTipoNaturaleza)) {
			procesarEliminacionTipoNaturaleza(eliminacionDominioDTO.getIdEliminar(), eliminacionDominioDTO.getIdReemplazar());
		} else if(valorDominio.getDominio().getCodDominio().equals(codigoDominioTipoNegocio)) {
			procesarEliminacionTipoNegocio(eliminacionDominioDTO.getIdEliminar(), eliminacionDominioDTO.getIdReemplazar());
		} else if(valorDominio.getDominio().getCodDominio().equals(codigoDominioTipoGasto)) {
			procesarEliminacionTiposGastos(eliminacionDominioDTO.getIdEliminar(), eliminacionDominioDTO.getIdReemplazar());
		} else if(valorDominio.getDominio().getCodDominio().equals(codigoDominioRolTercero)) {
			procesarEliminacionRolTercero(eliminacionDominioDTO.getIdEliminar(), eliminacionDominioDTO.getIdReemplazar());
		} else if(valorDominio.getDominio().getCodDominio().equals(codigoDominioRetenciones)) {
			procesarEliminacionRetenciones(eliminacionDominioDTO.getIdEliminar(), eliminacionDominioDTO.getIdReemplazar());
		} else if(valorDominio.getDominio().getCodDominio().equals(codigoDominioFechaClave)) {
			procesarEliminacionFechasClaves(eliminacionDominioDTO.getIdEliminar(), eliminacionDominioDTO.getIdReemplazar());
		}

		servicioRelacionEntreDominios.eliminarRelacionDominio(eliminacionDominioDTO.getIdEliminar());
		valorDominio.setActivo(Boolean.FALSE);
		repositorioValorDominioNegocio.save(valorDominio);
		
	}

	/**
	 * Convierte un objeto {@link ValorDominioNegocio} a un objeto
	 * {@link DatosBasicosValorDominioDTO}.
	 * 
	 * @param valorDominioNegocio La entidad a convertir.
	 * @return El DTO correspondiente a la entidad enviada.
	 */
	private static DatosBasicosValorDominioDTO entidadADatoBasicoDominioDTO(ValorDominioNegocio valorDominioNegocio) {
		return new DatosBasicosValorDominioDTO(valorDominioNegocio.getIdValorDominio(),
				valorDominioNegocio.getDescripcion(), valorDominioNegocio.getUnidad(), valorDominioNegocio.getValor() != null ? valorDominioNegocio.getValor().toUpperCase() : "");
	}
	
	private void procesarEliminacionTipoNaturaleza(Integer idEliminar, Integer idReemplazar) {
		procesarEliminacionRetencionTitular(idEliminar, idReemplazar);
		
		List<Titular> titulares = servicioTitular.buscarTitularesPorTipoNaturalezaNegocio(idEliminar);
		
		titulares.forEach(titular->{
			titular.setIdTipoNaturalezaJuridica(idReemplazar);
			servicioTitular.guardarTitular(titular);
		});
	}	
	
	private void procesarEliminacionTipoNegocio(Integer idTipoNegocioEliminar, Integer idTipoNegocioReemplazar) {

		List<TipoNegocio> tiposNegocioAEliminar = servicioTipoNegocio.buscarTiposNegocioPorTipo(idTipoNegocioEliminar);

		List<String> codigosNegocioProcesados = new ArrayList<>();

		tiposNegocioAEliminar.forEach(tipoNegocio -> {
			tipoNegocio.setActivo(Boolean.FALSE);
			servicioTipoNegocio.guardarTipoNegocio(tipoNegocio);
			if (!codigosNegocioProcesados.contains(tipoNegocio.getTipoNegocioPK().getCodSfc())) {
				codigosNegocioProcesados.add(tipoNegocio.getTipoNegocioPK().getCodSfc());
				servicioTipoNegocio.guardarTipoNegocio(
						new TipoNegocio(idTipoNegocioReemplazar, tipoNegocio.getTipoNegocioPK().getCodSfc(), Boolean.TRUE));
			}
		});

		List<RetencionTipoNegocio> retencionesTipoNegocioAEliminar = servicioRetencionTipoNegocio
				.buscarPorIdTipoNegocio(idTipoNegocioEliminar);

		retencionesTipoNegocioAEliminar.forEach(retencionTipoNegocio -> {
			retencionTipoNegocio.setActivo(Boolean.FALSE);
			servicioRetencionTipoNegocio.guardarRetencionTipoNegocio(retencionTipoNegocio);
		});

		List<RelacionEntreDominios> retencionesTipoNegocio = servicioRelacionEntreDominios
				.buscarValoresAplicanRelacion(idTipoNegocioReemplazar);

		retencionesTipoNegocio.forEach(relacionEntreDominios -> {
			codigosNegocioProcesados.forEach(codigoNegocio -> servicioRetencionTipoNegocio.guardarRetencionTipoNegocio(
					new RetencionTipoNegocio(relacionEntreDominios.getValorDominioNegocio().getIdValorDominio(),
							idTipoNegocioReemplazar, codigoNegocio, Boolean.TRUE)));
		});
		
		procesarEliminacionRetencionTitular(idTipoNegocioEliminar, idTipoNegocioReemplazar);
		
		List<ObligacionNegocio> obligacionesNegocio = servicioObligacionNegocio.buscarObligacionesNegocioPorIdTipoNegocio(idTipoNegocioEliminar);
		
		obligacionesNegocio.forEach(obligacionNegocio -> {
			obligacionNegocio.setActivo(Boolean.FALSE);
			servicioObligacionNegocio.guardarObligacionNegocio(obligacionNegocio);
			ObligacionNegocio obligacionNegocioNueva = new ObligacionNegocio(
					obligacionNegocio.getObligacionNegocioPK().getTipoPago(),
					idTipoNegocioReemplazar,
					obligacionNegocio.getObligacionNegocioPK().getCodSfc(),
					obligacionNegocio.getObligacionNegocioPK().getNombreGasto());
			obligacionNegocioNueva.setActivo(Boolean.TRUE);
			obligacionNegocioNueva.setDiaPago(obligacionNegocio.getDiaPago());
			obligacionNegocioNueva.setFechaVencimiento(obligacionNegocio.getFechaVencimiento());
			obligacionNegocioNueva.setNumeroRadicado(obligacionNegocio.getNumeroRadicado());
			obligacionNegocioNueva.setPeriodicidad(obligacionNegocio.getPeriodicidad());
			obligacionNegocioNueva.setTerceroNumeroDocumento(obligacionNegocio.getTerceroNumeroDocumento());
			obligacionNegocioNueva.setTerceroTipoDocumento(obligacionNegocio.getTerceroTipoDocumento());
			obligacionNegocioNueva.setUnidad(obligacionNegocio.getUnidad());
			obligacionNegocioNueva.setValor(obligacionNegocio.getValor());
			servicioObligacionNegocio.guardarObligacionNegocio(obligacionNegocioNueva);
		});
	}

	private void procesarEliminacionRetencionTitular(Integer idTipoNaturalezaNegocioEliminar, Integer idTipoNaturalezaNegocioReemplazar) {
		List<RetencionTitular> retencionTitularesAEliminar = servicioRetencionTitular
				.buscarRetencionesTitularPorTipoNaturalezaNegocio(idTipoNaturalezaNegocioEliminar);

		List<RetencionTitularPK> retencionesTitularesPK = new ArrayList<>();

		retencionTitularesAEliminar.forEach(retencionTitular -> {
			retencionTitular.setActivo(Boolean.FALSE);
			servicioRetencionTitular.guardarRetencionTitular(retencionTitular);
			if (!retencionesTitularesPK.contains(retencionTitular.getRetencionTitularPK())) {
				retencionesTitularesPK.add(retencionTitular.getRetencionTitularPK());
			}
		});

		List<RelacionEntreDominios> retencionesTipoNaturalezaNegocio = servicioRelacionEntreDominios
				.buscarValoresAplicanRelacion(idTipoNaturalezaNegocioReemplazar);

		retencionesTipoNaturalezaNegocio.forEach(relacionEntreDominios -> {
			retencionesTitularesPK.forEach(retencionTitularPK -> servicioRetencionTitular.guardarRetencionTitular(new RetencionTitular(
					relacionEntreDominios.getValorDominioNegocio().getIdValorDominio(), retencionTitularPK.getCodSfc(),
					retencionTitularPK.getNumeroDocumento(), retencionTitularPK.getTipoDocumento(), Boolean.TRUE,
					relacionEntreDominios.getValorDominioNegocio1().getIdValorDominio())));
		});
	}

	private void procesarEliminacionTiposGastos(Integer idTipoGastoEliminar, Integer idTipoGastoReemplazar) {
		
		List<ObligacionNegocio> obligacionesNegocio = servicioObligacionNegocio
				.buscarObligacionesNegocioPorIdTipoGasto(idTipoGastoEliminar);
		
		if (idTipoGastoReemplazar != null) {
			obligacionesNegocio.forEach(obligacionNegocio -> {
				obligacionNegocio.setActivo(Boolean.FALSE);
				servicioObligacionNegocio.guardarObligacionNegocio(obligacionNegocio);
				ObligacionNegocio obligacionNegocioNueva = new ObligacionNegocio(idTipoGastoReemplazar,
						obligacionNegocio.getObligacionNegocioPK().getTipoNegocio(),
						obligacionNegocio.getObligacionNegocioPK().getCodSfc(),
						obligacionNegocio.getObligacionNegocioPK().getNombreGasto());
				obligacionNegocioNueva.setActivo(Boolean.TRUE);
				obligacionNegocioNueva.setDiaPago(obligacionNegocio.getDiaPago());
				obligacionNegocioNueva.setFechaVencimiento(obligacionNegocio.getFechaVencimiento());
				obligacionNegocioNueva.setNumeroRadicado(obligacionNegocio.getNumeroRadicado());
				obligacionNegocioNueva.setPeriodicidad(obligacionNegocio.getPeriodicidad());
				obligacionNegocioNueva.setTerceroNumeroDocumento(obligacionNegocio.getTerceroNumeroDocumento());
				obligacionNegocioNueva.setTerceroTipoDocumento(obligacionNegocio.getTerceroTipoDocumento());
				obligacionNegocioNueva.setUnidad(obligacionNegocio.getUnidad());
				obligacionNegocioNueva.setValor(obligacionNegocio.getValor());
				servicioObligacionNegocio.guardarObligacionNegocio(obligacionNegocioNueva);
			});
		} else {
			obligacionesNegocio.forEach(obligacionNegocio -> {
				obligacionNegocio.setActivo(Boolean.FALSE);
				servicioObligacionNegocio.guardarObligacionNegocio(obligacionNegocio);
			});
		}

	}
	
	private void procesarEliminacionRolTercero(Integer idEliminar, Integer idReemplazar) {
		procesarEliminacionTiposGastos(idEliminar,idReemplazar);
		
		List<MiembroComite> miembrosComite = servicioMiembroComite.buscarMiembrosComitePorRol(idEliminar);
		
		if (idReemplazar == null) {
			miembrosComite.forEach(miembroComite -> {
				miembroComite.setEstado("INACTIVO");
				servicioMiembroComite.guardarMiembroComite(miembroComite);
			});
		} else {
			miembrosComite.forEach(miembroComite -> {
				miembroComite.setRol(idReemplazar);
				servicioMiembroComite.guardarMiembroComite(miembroComite);
			});
		}
	}
	
	private void procesarEliminacionRetenciones(Integer idEliminar, Integer idReemplazar) {
		List<RetencionTipoNegocio> retencionesTipoNegocioAEliminar = servicioRetencionTipoNegocio
				.buscarPorIdRetencion(idEliminar);

		List<RetencionTitular> retencionTitularesAEliminar = servicioRetencionTitular
				.buscarRetencionesTitularPorIdRetencion(idEliminar);

		retencionesTipoNegocioAEliminar.forEach(retencionTipoNegocio -> {
			retencionTipoNegocio.setActivo(Boolean.FALSE);
			servicioRetencionTipoNegocio.guardarRetencionTipoNegocio(retencionTipoNegocio);
			if (idReemplazar != null) {
				servicioRetencionTipoNegocio.guardarRetencionTipoNegocio(new RetencionTipoNegocio(idReemplazar,
						retencionTipoNegocio.getRetencionTipoNegocioPK().getTipoNegocio(),
						retencionTipoNegocio.getRetencionTipoNegocioPK().getCodSfc(), Boolean.TRUE));
			}
		});

		retencionTitularesAEliminar.forEach(retencionTitular -> {
			retencionTitular.setActivo(Boolean.FALSE);
			servicioRetencionTitular.guardarRetencionTitular(retencionTitular);
			if (idReemplazar != null) {
				servicioRetencionTitular.guardarRetencionTitular(
						new RetencionTitular(idReemplazar, retencionTitular.getRetencionTitularPK().getCodSfc(),
								retencionTitular.getRetencionTitularPK().getNumeroDocumento(),
								retencionTitular.getRetencionTitularPK().getTipoDocumento(), Boolean.TRUE,
								retencionTitular.getRetencionTitularPK().getTipoNaturalezaNegocio()));
			}
		});
	}
	
	private void procesarEliminacionFechasClaves(Integer idEliminar, Integer idReemplazar) {
		List<FechaClave> fechasClavesAEliminar = servicioFechaClave.buscarPorIdFechaClave(idEliminar);
		fechasClavesAEliminar.forEach(fechaClave -> {
			fechaClave.setActivo(Boolean.FALSE);
			servicioFechaClave.guardarFechaClave(fechaClave);
			if(idReemplazar != null) {
				FechaClave fechaClaveNueva = new FechaClave(fechaClave.getFechaClavePK().getCodSfc(), fechaClave.getFechaClavePK().getFecha());
				fechaClaveNueva.setActivo(Boolean.TRUE);
				fechaClaveNueva.setFechaDia(fechaClave.getFechaDia());
				fechaClaveNueva.setFechaMes(fechaClave.getFechaMes());
				fechaClave.setNombre(fechaClave.getNombre());
				fechaClave.setPeriodicidad(fechaClave.getPeriodicidad());
				fechaClave.setTipo(idReemplazar);
				servicioFechaClave.guardarFechaClave(fechaClaveNueva);
			}
		});
	}
}