package com.accionfiduciaria.negocio.servicios;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.enumeradores.TipoExtensionArchivo;
import com.accionfiduciaria.excepciones.ErrorAlConectarAccionException;
import com.accionfiduciaria.excepciones.PorcentajeGiroBeneficiariosNoValidoException;
import com.accionfiduciaria.excepciones.TitularSinBeneficiariosException;
import com.accionfiduciaria.modelo.dtos.BeneficiarioPagosDTO;
import com.accionfiduciaria.modelo.dtos.BeneficiarioTipoPagoDTO;
import com.accionfiduciaria.modelo.dtos.BeneficiarioTitularDTO;
import com.accionfiduciaria.modelo.dtos.EncargoDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoCambioDTO;
import com.accionfiduciaria.modelo.dtos.HistoricoCesionesCambioDTO;
import com.accionfiduciaria.modelo.dtos.InformacionFinancieraEnvioDTO;
import com.accionfiduciaria.modelo.dtos.InformacionFinancieraRecepcionDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.PeticionConsultaHistoricoPagos;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoCambiosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoCesionesCambiosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaPagosDTO;
import com.accionfiduciaria.modelo.dtos.TipoPagoDTO;
import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.BeneficiarioPK;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersona;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersonaPK;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.TitularPK;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.especificaciones.DistribucionBeneficiarioEspecificaciones;
import com.accionfiduciaria.negocio.repositorios.RepositorioBeneficiario;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucionBeneficiario;

@Service
@Transactional
@PropertySource("classpath:propiedades.properties")
public class ServicioBeneficiario implements IServicioBeneficiario {

	@Value("${cod.medio.pago.transferencia}")
	private String codigoMedioPagoTransferencia;

	@Value("${cod.medio.pago.cheque}")
	private String codigoMedioPagoCheque;

	@Value("${cod.medio.pago.cheque.gerencia}")
	private String codigoMedioPagoChequeGerencia;

	@Value("${cod.medio.pago.traslado}")
	private String codigoMedioPagoTraslado;

	@Value("${cod.medio.pago.otro}")
	private String codigoMedioPagoOtro;

	@Value("${cod.dominio.banco}")
	private String codigoDominioBanco;

	@Value("${cod.dominio.tipos.cuenta}")
	private String codigoDominioTipoCuenta;

	@Value("${codigo.cuenta.corriente}")
	private String codigoCuentaCorriente;

	@Value("${codigo.cuenta.ahorros}")
	private String codigoCuentaAhorros;

	@Value("${codigo.tipo.cuenta.cheque}")
	private String codigoTipoCuentaCheque;

	@Value("${codigo.tipo.cuenta.ahorros}")
	private String codigoTipoCuentaAhorros;

	@Value("${codigo.tipo.cuenta.corriente}")
	private String codigoTipoCuentaCorriente;

	@Value("${codigo.tipo.cuenta.traslado}")
	private String codigoTipoCuentaTraslado;

	@Value("${codigo.tipo.banco.traslado}")
	private String codigoTipoBancoTraslado;

	@Value("${codigo.tipo.banco.cheque}")
	private String codigoTipoBancoCheque;

	@Value("${codigo.tipo.banco.otro}")
	private String codigoTipoBancoOtro;

	@Value("${formato.ddmmyyyy}")
	private String formatoddMMyyyy;
	
	@Value("${formato.ddMMyyyyhhmmss}")
	private String formatoddMMyyyyhhmmss;

	@Autowired
	private RepositorioBeneficiario repositorioBeneficiario;

	@Autowired
	private IServicioPersona servicioPersona;

	@Autowired
	private IServicioTitular servicioTitular;

	@Autowired
	private Utilidades utilidades;

	@Autowired
	private IServicioMedioPagoPersona servicioMedioPagoPersona;

	@Autowired
	private IServicioDominio servicioDominio;

	@Autowired
	private IServicioMedioPago servicioMedioPago;

	@Autowired
	private IServicioVinculados servicioVinculados;

	@Autowired
	private RepositorioDistribucionBeneficiario repositorioDistribucionBeneficiario;

	@Autowired
	private IServicioArchivo servicioArchivo;
	
	@Autowired
	private IServicioAudHistorial servicioAuditoria;
	
	@Autowired
	private IServicioSolicitudCambiosDatos servicioSolicitudCambioDatos;

	@Override
	public List<Beneficiario> buscarBeneficiariosTitular(Titular titular, Boolean estado) {
		return repositorioBeneficiario.findByTitularAndEstado(titular, estado);
	}

	@Override
	public Beneficiario guardarBeneficiario(Beneficiario beneficiario) {
		return repositorioBeneficiario.save(beneficiario);
	}

	@Override
	public InformacionFinancieraEnvioDTO obtenerInformacionFinanciera(String tipoDocumentoTitular,
			String documentoTitular, String codigoNegocio, String token) {

		InformacionFinancieraEnvioDTO informacionFinancieraDTO = new InformacionFinancieraEnvioDTO();
		Titular titular = servicioTitular.buscarTitular(tipoDocumentoTitular, documentoTitular, codigoNegocio);

		informacionFinancieraDTO.setCodigoNegocio(codigoNegocio);
		informacionFinancieraDTO.setTipoDocumentoTitular(tipoDocumentoTitular);
		informacionFinancieraDTO.setDocumento(documentoTitular);
		informacionFinancieraDTO.setNumeroDerechosTitular(titular.getNumeroDerechos());
		informacionFinancieraDTO.setTotalDerechosNegocio(titular.getNegocio().getTotalParticipaciones());
		informacionFinancieraDTO.setBeneficiarios(cargarBeneficiariosTitular(titular, token));
		return informacionFinancieraDTO;
	}

	@Override
	public void guardarInformacionFinanciera(InformacionFinancieraRecepcionDTO informacionFinanciera)
			throws TitularSinBeneficiariosException, PorcentajeGiroBeneficiariosNoValidoException {
		if (informacionFinanciera.getBeneficiarios() == null || informacionFinanciera.getBeneficiarios().isEmpty()) {
			throw new TitularSinBeneficiariosException();
		}
		Titular titular = servicioTitular.buscarTitular(informacionFinanciera.getTipoDocumentoTitular(),
				informacionFinanciera.getDocumento(), informacionFinanciera.getCodigoNegocio());
		procesarBeneficiarios(informacionFinanciera.getBeneficiarios(), titular);
	}

	private void procesarBeneficiarios(List<BeneficiarioTipoPagoDTO> beneficiariosDTO, Titular titular)
			throws PorcentajeGiroBeneficiariosNoValidoException {
		BigDecimal sumaPorcentajes = beneficiariosDTO.stream().map(BeneficiarioTipoPagoDTO::getPorcentajeGiro)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		if (sumaPorcentajes.compareTo(BigDecimal.valueOf(100)) != 0) {
			throw new PorcentajeGiroBeneficiariosNoValidoException();
		}
		List<Beneficiario> beneficiarios = beneficiariosDTO.stream()
				.map(beneficiarioDTO -> beneficiarioTipoPagoDTOAEntidad(beneficiarioDTO, titular))
				.collect(Collectors.toList());
		if (titular.getBeneficiarios() != null)
			inactivarBeneficiarios(beneficiarios, titular);
		repositorioBeneficiario.saveAll(beneficiarios);
	}

	private void inactivarBeneficiarios(List<Beneficiario> beneficiarios, Titular titular) {
		List<Beneficiario> beneficiariosActuales = buscarBeneficiariosTitular(titular, Boolean.TRUE);

		// Actualización forma de inactivar beneficiario.
		beneficiariosActuales.forEach(beneficiarioActual -> {
			if (!beneficiarios.contains(beneficiarioActual)) {
				inactivarBeneficiario(beneficiarioActual);
			}
		});
	}

	private Beneficiario beneficiarioTipoPagoDTOAEntidad(BeneficiarioTipoPagoDTO beneficiarioTipoPagoDTO,
			Titular titular) {

		Beneficiario beneficiario = new Beneficiario();

		beneficiario.setBeneficiarioPK(generarLlavePrimariaBeneficiario(beneficiarioTipoPagoDTO, titular));
		beneficiario.setPorcentaje(beneficiarioTipoPagoDTO.getPorcentajeGiro().multiply(titular.getPorcentaje()).divide(BigDecimal.valueOf(100)));
		beneficiario.setPorcentajeGiro(beneficiarioTipoPagoDTO.getPorcentajeGiro());
		beneficiario.setNumeroDerechos(beneficiarioTipoPagoDTO.getNumeroDerechos());

		beneficiario.setMedioPagoPersona(obtenerMedioPago(beneficiario.getBeneficiarioPK(),
				beneficiarioTipoPagoDTO.getBeneficiario(), beneficiarioTipoPagoDTO.getDescripcionOtro()));
		beneficiario.setEstado(Boolean.TRUE);
		beneficiario.setTitular(titular);
		return beneficiario;
	}

	private BeneficiarioPK generarLlavePrimariaBeneficiario(BeneficiarioTipoPagoDTO beneficiarioTipoPagoDTO,
			Titular titular) {

		BeneficiarioPK beneficiarioPK = new BeneficiarioPK();
		beneficiarioPK.setCodSfc(titular.getTitularPK().getCodSfc());
		beneficiarioPK.setNumeroDocumentoBeneficiario(beneficiarioTipoPagoDTO.getNumeroDocumento());
		beneficiarioPK.setNumeroDocumentoTitular(titular.getTitularPK().getNumeroDocumento());
		beneficiarioPK.setTipoDocumentoBeneficiario(beneficiarioTipoPagoDTO.getTipoDocumento());
		beneficiarioPK.setTipoDocumentoTitular(titular.getTitularPK().getTipoDocumento());

		ValorDominioNegocio tipoCuenta;
		ValorDominioNegocio codigoBanco = new ValorDominioNegocio();
		String cuenta = "";

		if ((beneficiarioTipoPagoDTO.getCodigoBanco() != null
				&& beneficiarioTipoPagoDTO.getCodigoBanco().equals(codigoTipoBancoTraslado))
				|| (beneficiarioTipoPagoDTO.getTipoPago() != null
						&& beneficiarioTipoPagoDTO.getTipoPago().equals(codigoMedioPagoTraslado))) {
			codigoBanco = servicioDominio.buscarPorCodigoDominioYValor(codigoDominioBanco, codigoTipoBancoTraslado);
			tipoCuenta = servicioDominio.buscarPorCodigoDominioYValor(codigoDominioTipoCuenta,
					codigoTipoCuentaTraslado);
			cuenta = beneficiarioTipoPagoDTO.getNumeroEncargo();
		} else if ((beneficiarioTipoPagoDTO.getCodigoBanco() != null
				&& beneficiarioTipoPagoDTO.getCodigoBanco().equals(codigoTipoBancoCheque))
				|| (beneficiarioTipoPagoDTO.getTipoPago() != null
						&& (beneficiarioTipoPagoDTO.getTipoPago().equals(codigoMedioPagoCheque)
								|| beneficiarioTipoPagoDTO.getTipoPago().equals(codigoMedioPagoChequeGerencia)))) {

			codigoBanco = servicioDominio.buscarPorCodigoDominioYValor(codigoDominioBanco, codigoTipoBancoCheque);
			tipoCuenta = servicioDominio.buscarPorCodigoDominioYValor(codigoDominioTipoCuenta, codigoTipoCuentaCheque);
			cuenta = beneficiarioTipoPagoDTO.getNumeroDocumento();
		} else {
			if (beneficiarioTipoPagoDTO.getIdEntidadBancaria() == null) {
				codigoBanco = servicioDominio.buscarPorCodigoDominioYValor(codigoDominioBanco,
						beneficiarioTipoPagoDTO.getCodigoBanco());
			} else {
				codigoBanco.setIdValorDominio(Integer.parseInt(beneficiarioTipoPagoDTO.getIdEntidadBancaria()));
			}
			if (beneficiarioTipoPagoDTO.getTipoCuenta().equals(codigoTipoCuentaAhorros) ||
					beneficiarioTipoPagoDTO.getTipoCuenta().equals(codigoCuentaAhorros)) {
				tipoCuenta = servicioDominio.buscarPorCodigoDominioYValor(codigoDominioTipoCuenta,
						codigoTipoCuentaAhorros);
			} else {
				tipoCuenta = servicioDominio.buscarPorCodigoDominioYValor(codigoDominioTipoCuenta,
						codigoTipoCuentaCorriente);
			}
			cuenta = beneficiarioTipoPagoDTO.getNumeroCuentaBancaria();
		}

		beneficiarioPK.setBanco(codigoBanco.getIdValorDominio());
		beneficiarioPK.setTipoCuenta(tipoCuenta.getIdValorDominio());
		beneficiarioPK.setCuenta(cuenta);
		return beneficiarioPK;
	}

	private MedioPagoPersona obtenerMedioPago(BeneficiarioPK beneficiarioPK, String nombreBeneficiario,
			String observacion) {

		MedioPago medioPago = new MedioPago(beneficiarioPK.getBanco(), beneficiarioPK.getCuenta(),
				beneficiarioPK.getTipoCuenta());
		medioPago.setObservacion(observacion);

		MedioPago medioPago2 = servicioMedioPago.buscarMedioPagoPorID(medioPago.getMedioPagoPK());

		if (medioPago2 == null) {
			medioPago = servicioMedioPago.guardarMedioPago(medioPago);
		} else {
			medioPago = medioPago2;
		}

		MedioPagoPersona medioPagoPersona = new MedioPagoPersona(beneficiarioPK.getCuenta(),
				beneficiarioPK.getNumeroDocumentoBeneficiario(), beneficiarioPK.getTipoDocumentoBeneficiario(),
				beneficiarioPK.getBanco(), beneficiarioPK.getTipoCuenta());
		medioPagoPersona.setPersona(obtenerPersona(beneficiarioPK.getTipoDocumentoBeneficiario(),
				beneficiarioPK.getNumeroDocumentoBeneficiario(), nombreBeneficiario));
		medioPagoPersona.setMedioPago(medioPago);
		medioPagoPersona.setEstado(Boolean.TRUE);

		return servicioMedioPagoPersona.guardarMedioPagoPersona(medioPagoPersona);
	}








	private Persona obtenerPersona(String tipoDocumento, String numeroDocumento, String beneficiario) {
		Persona persona = servicioPersona.buscarPersona(tipoDocumento, numeroDocumento);
		if (persona == null) {
			persona = servicioPersona.guardarPersona(new Persona(tipoDocumento, numeroDocumento, beneficiario));
		}
		return persona;
	}

	/**
	 * 
	 * @param titular
	 * @return
	 */
	private List<BeneficiarioTitularDTO> cargarBeneficiariosTitular(Titular titular, String token) {
		List<Beneficiario> beneficiarios = buscarBeneficiariosTitular(titular, Boolean.TRUE);
		return beneficiarios.stream()
				.map(beneficiario -> beneficiarioABeneficiarioTitularDTO(beneficiario, token))
				.collect(Collectors.toList());
	}

	/**
	 * 
	 * @param beneficiario
	 * @param numeroDerechosTotalNegocio
	 * @param numeroDerechosTitular
	 * @return
	 */
	private BeneficiarioTitularDTO beneficiarioABeneficiarioTitularDTO(Beneficiario beneficiario, String token) {
		BeneficiarioTitularDTO beneficiarioTitularDTO = new BeneficiarioTitularDTO();
		beneficiarioTitularDTO.setBeneficiario(beneficiario.getMedioPagoPersona().getPersona().getNombreCompleto());
		beneficiarioTitularDTO.setNumeroDocumento(
				beneficiario.getMedioPagoPersona().getPersona().getPersonaPK().getNumeroDocumento());
		beneficiarioTitularDTO
				.setTipoDocumento(beneficiario.getMedioPagoPersona().getPersona().getPersonaPK().getTipoDocumento());
		beneficiarioTitularDTO.setPorcentajeGiro(beneficiario.getPorcentajeGiro());
		beneficiarioTitularDTO.setTipoPago(cargarDatosMediosPago(beneficiario.getMedioPagoPersona(),
				beneficiario.getMedioPagoPersona().getPersona().getPersonaPK().getNumeroDocumento(), token));
		return beneficiarioTitularDTO;
	}

	/**
	 * 
	 * @param medioPagoPersona
	 * @return
	 */
	private TipoPagoDTO cargarDatosMediosPago(MedioPagoPersona medioPagoPersona, String documento, String token) {
		TipoPagoDTO tipoPagoDTO = new TipoPagoDTO();

		ValorDominioNegocio tipoBanco = servicioDominio
				.buscarValorDominio(medioPagoPersona.getMedioPagoPersonaPK().getBanco());

		if (tipoBanco.getValor().equals(codigoTipoBancoTraslado)) {
			tipoPagoDTO.setCodigo(codigoMedioPagoTraslado);
			EncargoDTO encargoDTO = servicioVinculados
					.validarEncargo(medioPagoPersona.getMedioPagoPersonaPK().getCuenta(), documento, token);
			if (encargoDTO != null && encargoDTO.getActivo()) {
				tipoPagoDTO.setNumeroEncargo(medioPagoPersona.getMedioPagoPersonaPK().getCuenta());
				tipoPagoDTO.setFideicomisoDestino(encargoDTO.getFideicomiso());
			}
		} else if (tipoBanco.getValor().equals(codigoTipoBancoCheque)) {
			tipoPagoDTO.setCodigo(codigoMedioPagoCheque);
		} else if (tipoBanco.getValor().equals(codigoTipoBancoOtro)) {
			tipoPagoDTO.setCodigo(codigoMedioPagoOtro);
			tipoPagoDTO.setNumeroReferencia(medioPagoPersona.getMedioPago().getMedioPagoPK().getCuenta());
			tipoPagoDTO.setDescripcionOtro(medioPagoPersona.getMedioPago().getObservacion());
		} else {
			ValorDominioNegocio tipoCuenta = servicioDominio
					.buscarValorDominio(medioPagoPersona.getMedioPagoPersonaPK().getTipoCuenta());
			tipoPagoDTO.setCodigo(codigoMedioPagoTransferencia);
			if (tipoCuenta.getValor().equals(codigoTipoCuentaAhorros)) {
				tipoPagoDTO.setTipoCuenta(codigoCuentaAhorros);
			} else {
				tipoPagoDTO.setTipoCuenta(codigoCuentaCorriente);
			}
			tipoPagoDTO.setNumeroCuentaBancaria(medioPagoPersona.getMedioPagoPersonaPK().getCuenta());
			tipoPagoDTO.setIdEntidadBancaria(tipoBanco.getIdValorDominio());
		}
		return tipoPagoDTO;
	}

	/**
	 * Obtiene la lista del historia de pagos de los beneficiarios asociados a un
	 * titular
	 * 
	 * @param peticion                          Request de consulta.
	 * @param RespuestaPaginadaConsultaPagosDTO Respuesta paginada.
	 */
	@Override
	public RespuestaPaginadaConsultaPagosDTO listaPagos(PeticionConsultaHistoricoPagos peticion) {

		
		// Consultamos el historial de pago con los filtros solicitados

		Specification<DistribucionBeneficiario> especificacionCombinada = Specification
				.where(DistribucionBeneficiarioEspecificaciones
						.filtrarPorTipoDocumentoTitular(peticion.getTipoDocumento())
						.and(DistribucionBeneficiarioEspecificaciones
								.filtrarPorNumeroDocumentoTitular(peticion.getNumeroDocumento())))
				.and(peticion.getEstadoPago().isEmpty() ? null
						: DistribucionBeneficiarioEspecificaciones.filtrarPorEstado(peticion.getEstadoPago()))
				.and(peticion.getNegociosAsociados().isEmpty() ? null
						: DistribucionBeneficiarioEspecificaciones.filtrarPorNegocios(peticion.getNegociosAsociados()))
				.and(peticion.getTipoPagos().isEmpty() ? null
						: DistribucionBeneficiarioEspecificaciones.filtrarPorTiposPago(peticion.getTipoPagos()))
				.and(peticion.getFecha().isEmpty() ? null
						: DistribucionBeneficiarioEspecificaciones.filtrarPorFechaInicial(
								utilidades.convertirFecha(peticion.getFecha().get(0), formatoddMMyyyy)))
				.and(peticion.getFecha().isEmpty() ? null
						: DistribucionBeneficiarioEspecificaciones.filtrarPorFechaFinal(utilidades.convertirFecha(
								peticion.getFecha().get(peticion.getFecha().size() - 1) + " 23:59:59", formatoddMMyyyyhhmmss)));

		List<BeneficiarioPagosDTO> pagos;
		int cantidadRegistros = (int) repositorioDistribucionBeneficiario.count(especificacionCombinada);
		if (peticion.getElementos() != 0 && peticion.getPagina() != 0) {
			// Asignamos la paginacion de la consulta
			Pageable rango = PageRequest.of(peticion.getPagina() - 1, peticion.getElementos());
			Page<DistribucionBeneficiario> resultados = repositorioDistribucionBeneficiario
					.findAll(especificacionCombinada, rango);
			// Transformamos la informacion obtenida en el dto de respuesta.
			pagos = resultados.stream().map(pago -> procesarHistorialPagos(pago)).collect(Collectors.toList());
		} else {
			List<DistribucionBeneficiario> resultados = repositorioDistribucionBeneficiario
					.findAll(especificacionCombinada);
			pagos = resultados.stream().map(pago -> procesarHistorialPagos(pago)).collect(Collectors.toList());
		}

		IntStream.range(0, pagos.size()).forEach(indice -> pagos.get(indice).setId(indice + 1 + ""));
		RespuestaPaginadaConsultaPagosDTO respuesta = new RespuestaPaginadaConsultaPagosDTO();
		respuesta.setHistorialPagos(pagos);
		respuesta.setTotalRegistros(cantidadRegistros);
		return respuesta;
	}

	/**
	 * Metodo que transforma la entidad de distribucion de pagos a dto para pasarla
	 * al FRONT
	 * 
	 * @param pago entidad de distribucion a ser mapeada
	 * @return BeneficiarioPagosDTO DTO con la informacion obtenida
	 */
	@Override
	public BeneficiarioPagosDTO procesarHistorialPagos(DistribucionBeneficiario pago) {
		BeneficiarioPagosDTO pagoDto = new BeneficiarioPagosDTO();
		pagoDto.setFechaPago(Utilidades.formatearFecha(pago.getFechaHora(), "dd/MM/yyyy"));
		pagoDto.setFechaPago(Utilidades.formatearFecha(pago.getFechaHora(),"dd/MM/yyyy"));
		pagoDto.setNegocioVinculado(pago.getNegocio().getNombre());
		pagoDto.setNombreBeneficiario(pago.getBeneficiario().getMedioPagoPersona().getPersona().getNombreCompleto());
		pagoDto.setNumeroDocumentoBeneficiario(
				pago.getBeneficiario().getBeneficiarioPK().getNumeroDocumentoBeneficiario());
		
		ValorDominioNegocio banco = servicioDominio.buscarValorDominio(pago.getDistribucionBeneficiarioPK().getBanco());
		pagoDto.setCodigoEntidadBancaria(banco.getValor());
		pagoDto.setEntidadBancariaFidecomiso(banco.getDescripcion());
		pagoDto.setIdEntidadBancaria(banco.getIdValorDominio().toString());
		
		pagoDto.setEstadoPago(pago.getEstado());
		pagoDto.setGravamentMovimientoFinanciero(pago.getValorGMF());
		pagoDto.setNumeroCuentaEncargo(pago.getDistribucionBeneficiarioPK().getCuenta());
		pagoDto.setPorcentajeDistribuido(pago.getPorcentajeParticipacion());
		pagoDto.setPorcentajeGiro(pago.getPorcentajeGiro());
		pagoDto.setPorcentajeParticipacion(pago.getPorcentajeParticipacion());
		pagoDto.setSubTotalGirados(pago.getValorMenosDescuento());
		pagoDto.setTipoCuenta(pago.getDominioCuenta().getDescripcion());
		pagoDto.setIdTipoCuenta(pago.getDominioCuenta().getDescripcion());
		pagoDto.setTipoDocumentoBeneficiario(pago.getDistribucionBeneficiarioPK().getTipoDocumentoBeneficiario());
		pagoDto.setTipoNegocio(pago.getDistribucionTipoNegocio());
		pagoDto.setTipoPago(pago.getDominioCuenta().getRelacionEntreDominiosList1().get(0).getValorDominioNegocio().getDescripcion());
		pagoDto.setIdTipoPago(pago.getDominioCuenta().getRelacionEntreDominiosList1().get(0).getValorDominioNegocio().getIdValorDominio().toString());
		if(pagoDto.getTipoCuenta().equalsIgnoreCase("CHEQUE_GERENCIA"))
			pagoDto.setPrefijoTipoPago("CHEGER");
		else if(pagoDto.getTipoCuenta().equalsIgnoreCase("TRASLADO"))
			pagoDto.setPrefijoTipoPago("TRASL");
		else if(pagoDto.getTipoCuenta().equalsIgnoreCase("TRANSFERENCIA"))
			pagoDto.setPrefijoTipoPago("TRANS");
		else if(pagoDto.getTipoCuenta().equalsIgnoreCase("CHEQUE"))
			pagoDto.setPrefijoTipoPago("CHE");
		else if(pagoDto.getTipoCuenta().equalsIgnoreCase("OTRO"))
			pagoDto.setPrefijoTipoPago("OTR");
		else
			pagoDto.setPrefijoTipoPago("");
		pagoDto.setValorAGirar(pago.getValorPago());
		pagoDto.setValorTipoPago(
				pago.getDominioCuenta().getRelacionEntreDominiosList1().get(0).getValorDominioNegocio().getValor());
		pagoDto.setCodigoNegocio(pago.getDistribucionBeneficiarioPK().getCodigoSFC());
		return pagoDto;
	}

	/**
	 * Metodo que permite obtener un archivo de excel con el historial de pagos
	 * 
	 * @param peticion Datos de diligenciamiento del archivo.
	 * @return RespuestaArchivoDTO Dto que contiene el archivo en bytes generado.
	 */
	@Override
	public RespuestaArchivoDTO obtenerArchivoHistorialPagos(PeticionConsultaHistoricoPagos peticion) throws Exception {
		RespuestaPaginadaConsultaPagosDTO respuesta = listaPagos(peticion);
		List<String> etiquetas = new ArrayList<>();
		Field[] campos = respuesta.getHistorialPagos().get(0).getClass().getDeclaredFields();
		for (int i = 0; i < campos.length; i++) {
			etiquetas.add(campos[i].getName().toUpperCase());
		}
		return servicioArchivo.construirArchivoExcel(respuesta.getHistorialPagos(), etiquetas,
				TipoExtensionArchivo.EXCEL);
	}

	/**
	 * Método que busca todos los beneficiarios activos asociados a un negocio.
	 * 
	 * @author efarias
	 * @param codigoNegocio
	 * @return List<Beneficiario> lista de beneficiarios activos a un negocio
	 */
	@Override
	public List<Beneficiario> buscarBeneficiariosPorNegocio(String codigoNegocio) {
		return repositorioBeneficiario.buscarBeneficiariosPorNegocio(codigoNegocio);
	}

	/**
	 * Esté metodo inactiva el beneficiario con el MedioPagoPersona antiguo y crea
	 * un nuevo beneficiario con el nuevo MedioPagoPersona.
	 * 
	 * @author efarias
	 * @return Beneficiario creado
	 */
	@Override
	public Beneficiario verificarCrearBeneficiario(DistribucionBeneficiario distribucionBeneficiario,
			MedioPagoPersona medioPagoPersona) {

		// Inactivar beneficiario anterior
		inactivarBeneficiario(distribucionBeneficiario.getBeneficiario());

		// Validar y crear nuevo beneficiario.
		return verificarCrearBeneficiario(distribucionBeneficiario.getBeneficiario(), medioPagoPersona);
	}

	/**
	 * Este método inactiva de forma puntual un beneficiario.
	 * 
	 * @author efarias
	 * @param benficiario
	 */
	private void inactivarBeneficiario(Beneficiario benficiario) {
		benficiario.setEstado(Boolean.FALSE);
		repositorioBeneficiario.save(benficiario);
	}

	/**
	 * Método que realiza la creación de un beneficiario con diferente
	 * MedioPagoPersona.
	 * 
	 * @author efarias
	 * @param beneficiario
	 * @param medioPagoPersona
	 * @return Beneficiario creado
	 */
	private Beneficiario verificarCrearBeneficiario(Beneficiario beneficiario, MedioPagoPersona medioPagoPersona) {
		TitularPK titularPK = beneficiario.getTitular().getTitularPK();
		MedioPagoPersonaPK medioPagoPersonaPK = medioPagoPersona.getMedioPagoPersonaPK();
		BeneficiarioPK beneficiarioPK = new BeneficiarioPK(medioPagoPersonaPK, titularPK);
		Beneficiario beneficiarioNuevo = new Beneficiario(beneficiarioPK);
		beneficiarioNuevo.setPorcentaje(beneficiario.getPorcentaje());
		beneficiarioNuevo.setNumeroDerechos(beneficiario.getNumeroDerechos());
		beneficiarioNuevo.setFechaRegistro(beneficiario.getFechaRegistro());
		beneficiarioNuevo.setFechaVigencia(beneficiario.getFechaVigencia());
		beneficiarioNuevo.setFechaVencimiento(beneficiario.getFechaVencimiento());
		beneficiarioNuevo.setEstado(Boolean.TRUE);
		beneficiarioNuevo.setPorcentajeGiro(beneficiario.getPorcentajeGiro());
		beneficiarioNuevo.setTitular(beneficiario.getTitular());
		beneficiarioNuevo.setMedioPagoPersona(medioPagoPersona);
		return repositorioBeneficiario.saveAndFlush(beneficiarioNuevo);
	}
	
	@Override
	public RespuestaHistoricoCambiosDTO listaBeneficiarioHistorialCambios(ParametrosFiltroDTO filtros, String tokenAutorization) {
		List<HistoricoCambioDTO> historialAuditoria = servicioAuditoria.consultarHistoricoAuditoria(filtros, null, "BENEFICIARIO", tokenAutorization);
		int cantidadRegistros =  historialAuditoria.isEmpty() ? 0: historialAuditoria.size();
		RespuestaHistoricoCambiosDTO respuesta = new RespuestaHistoricoCambiosDTO();
		respuesta.setTotalRegistro(cantidadRegistros);
		respuesta.setListaCambios(historialAuditoria);
		return respuesta;
	}

	@Override
	public RespuestaHistoricoCambiosDTO listaBeneficiarioHistorialSolicitudCambios(ParametrosFiltroDTO filtros) {
		List<HistoricoCambioDTO> historialSolicitudes = servicioSolicitudCambioDatos.consultarHistoricoSolicitudCambios(filtros, null);
		int cantidadRegistros =  historialSolicitudes.isEmpty() ? 0: historialSolicitudes.size();
		RespuestaHistoricoCambiosDTO respuesta = new RespuestaHistoricoCambiosDTO();
		respuesta.setTotalRegistro(cantidadRegistros);
		respuesta.setListaCambios(historialSolicitudes);
		return respuesta;
	}

	@Override
	public RespuestaHistoricoCesionesCambiosDTO listaBeneficiarioHistorialCesionesCambios(ParametrosFiltroDTO filtros, String tokenAutorizacion) throws ErrorAlConectarAccionException {
		List<HistoricoCesionesCambioDTO> historialCesiones = servicioAuditoria.obtenerHistoricoCambiosCesiones(filtros, tokenAutorizacion);
		int cantidadRegistros =  historialCesiones.isEmpty() ? 0: historialCesiones.size();
		RespuestaHistoricoCesionesCambiosDTO respuesta = new RespuestaHistoricoCesionesCambiosDTO();
		respuesta.setTotalRegistros(cantidadRegistros);
		respuesta.setListaCambios(historialCesiones);
		return respuesta;
	}

}
