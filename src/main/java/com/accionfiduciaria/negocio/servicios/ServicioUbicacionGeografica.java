/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.UbicacionGeograficaDTO;
import com.accionfiduciaria.modelo.entidad.UbicacionGeografica;
import com.accionfiduciaria.negocio.repositorios.RepositorioUbicacionGeografica;

/**
 * @author efarias
 *
 */
@Service
@Transactional
@PropertySource("classpath:propiedades.properties")
public class ServicioUbicacionGeografica implements IServicioUbicacionGeografica {

	@Autowired
	private RepositorioUbicacionGeografica repositorioUbicacionGeografica;

	@Value("${tipo.ubicacion.pais}")
	private String tipoUbicacionPais;

	@Value("${tipo.ubicacion.departamento}")
	private String tipoUbicacionDepartamento;

	@Value("${tipo.ubicacion.municipio}")
	private String tipoUbicacionMunicipio;

	@Override
	public List<UbicacionGeograficaDTO> obtenerPaisesActivos() {
		List<UbicacionGeografica> paises = repositorioUbicacionGeografica
				.obtenerUbicacionesActivasPorTipo(tipoUbicacionPais);
		return listaEntidadAListaDTO(paises);
	}

	@Override
	public List<UbicacionGeograficaDTO> obtenerDepartamentosPorPais(String codigoPais) {
		UbicacionGeografica pais = repositorioUbicacionGeografica.findByIso2AndActivo(codigoPais,Boolean.TRUE);
		List<UbicacionGeografica> departamentos = repositorioUbicacionGeografica
				.obtenerUbicacionesPorCodigoPadre(tipoUbicacionDepartamento, pais.getCodigo());
		return listaEntidadAListaDTO(departamentos);
	}

	@Override
	public List<UbicacionGeograficaDTO> obtenerMunicipiosPorDepartamento(String codigoDepartamento) {
		List<UbicacionGeografica> municipios = repositorioUbicacionGeografica
				.obtenerUbicacionesPorCodigoPadre(tipoUbicacionMunicipio, codigoDepartamento);
		return listaEntidadAListaDTO(municipios);
	}
	
	@Override
	public UbicacionGeografica obtenerUbicacionGeograficaPorCodigo(String codigo) {
		return repositorioUbicacionGeografica.findById(codigo).orElse(null);
	}

	/**
	 * Transforma una lista de objetos UbicacionGeografica a una lista de objetos
	 * UbicacionGeograficaDTO, en caso de que la lista a transformar este vacía o
	 * nula, retornará una lista vacia.
	 * 
	 * @author efarias
	 * @param listaUbicaciones
	 * @return lista de UbicacionGeograficaDTO
	 */
	private List<UbicacionGeograficaDTO> listaEntidadAListaDTO(List<UbicacionGeografica> listaUbicaciones) {
		List<UbicacionGeograficaDTO> listaUbicacionDTO = new ArrayList<>();
		if (!listaUbicaciones.isEmpty()) {
			for (UbicacionGeografica ubicacion : listaUbicaciones) {
				listaUbicacionDTO.add(entidadADTO(ubicacion));
			}
		}
		return listaUbicacionDTO;
	}

	/**
	 * Transforma un objeto UbicacionGeografica a un objeto UbicacionGeograficaDTO.
	 * 
	 * @author efarias
	 * @param ubicacion
	 * @return UbicacionGeograficaDTO
	 */
	private UbicacionGeograficaDTO entidadADTO(UbicacionGeografica ubicacion) {
		return new UbicacionGeograficaDTO(ubicacion.getCodigo(), ubicacion.getNombre(), ubicacion.getIso2(),
				ubicacion.getTipo(), ubicacion.getCodigoPadre());
	}
}
