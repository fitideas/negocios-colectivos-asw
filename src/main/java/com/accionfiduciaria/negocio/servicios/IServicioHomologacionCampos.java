package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;

public interface IServicioHomologacionCampos {
	
	@Autowired
	String homologarCampo(String nombreCampo);

}
