/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.entidad.HistorialNotificaciones;
import com.accionfiduciaria.negocio.repositorios.RepositorioHistorialNotificaciones;

/**
 * @author efarias
 *
 */
@Service
@Transactional
public class ServicioHistorialNotificaciones implements IServicioHistorialNotificaciones {
	
	@Autowired
	private RepositorioHistorialNotificaciones repositorioHistorialNotificaciones;

	@Override
	public void guardarAuditoriaNotificaciones(HistorialNotificaciones historialNotificaciones) {
		repositorioHistorialNotificaciones.save(historialNotificaciones);
	}

}
