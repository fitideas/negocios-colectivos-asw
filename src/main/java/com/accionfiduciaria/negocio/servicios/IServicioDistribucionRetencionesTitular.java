/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitular;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

/**
 * @author efarias
 *
 */
public interface IServicioDistribucionRetencionesTitular {

	@Autowired
	public List<DistribucionRetencionesTitular> calcularRetencionTitularesPorTipoNegocio(TipoNegocio tipoNegocio,
			Distribucion distribucion);
	
	@Autowired
	public void guardarListaDistribucionRetencionesTitular(List<DistribucionRetencionesTitular> listaRetencionTitulares);

}
