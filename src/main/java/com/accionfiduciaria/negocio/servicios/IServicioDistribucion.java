/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.FaltanDatosObligatoriosException;
import com.accionfiduciaria.excepciones.NoExisteDistribucionDisponible;
import com.accionfiduciaria.excepciones.NoExistenIngresosException;
import com.accionfiduciaria.modelo.dtos.CalculoObligacionesTipoNegocioDTO;
import com.accionfiduciaria.modelo.dtos.ReprocesoPagosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoPagoZipDTO;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;

/**
 * @author efarias
 *
 */
public interface IServicioDistribucion {

	@Autowired
	public CalculoObligacionesTipoNegocioDTO calcularObligacion(String periodo, String codigoNegocio,
			Integer codigoTipoNegocio) throws FaltanDatosObligatoriosException, NoExistenIngresosException;

	@Autowired
	public List<Distribucion> buscarDistribucionPorEstado(PagoNegocio pagoNegocio, String estado);

	@Autowired
	public List<Distribucion> buscarDistribucionPorEstado(String codigoNegocio, Integer tipoNegocio, String periodo,
			String estado);

	@Autowired
	public Map<String, Object> calcularDistribucion(String periodo, String codigoNegocio, Integer codigoTipoNegocio)
			throws FaltanDatosObligatoriosException, NoExisteDistribucionDisponible;

	public CalculoObligacionesTipoNegocioDTO obtieneIngresosEgresos(String periodo, String codigoNegocio,
			String codigoTipoNegocio) throws FaltanDatosObligatoriosException, NoExistenIngresosException;

	@Autowired
	public RespuestaArchivoPagoZipDTO generarArchivoPago(String periodo, String codigoNegocio,
			Integer codigoTipoNegocio) throws IOException, FaltanDatosObligatoriosException,
			NoExisteDistribucionDisponible, BusquedadSinResultadosException;

	@Autowired
	public RespuestaArchivoPagoZipDTO reprocesarPagos(ReprocesoPagosDTO reprocesarPago)
			throws FaltanDatosObligatoriosException, NoExisteDistribucionDisponible, IOException,
			BusquedadSinResultadosException;
}
