package com.accionfiduciaria.negocio.servicios;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:application.properties")
public class ServicioEnvioCorreo implements IServicioEnvioCorreo {

	@Autowired
    private JavaMailSender javaMailSender;
	
	@Value("${direccion.correo}")
	private String correoOrigen;
	
	@Override
	public void enviarCorreo(String destinatario, String asunto, String cuerpoCorreo) throws MessagingException {
		MimeMessage msg = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		
        helper.setFrom(correoOrigen);
        
        helper.setTo(destinatario);

        helper.setSubject(asunto);

        helper.setText(cuerpoCorreo, true);

        javaMailSender.send(msg);
	}

}
