package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Persona;

/**
 * Clase que maneja la logica de las personas.
 * 
 * @author jpolo
 *
 */
public interface IServicioPersona {
	
	/**
	 * Guarda los datos de una persona.
	 * 
	 * @param persona La entidad a guardar.
	 * @return La entidad guardada.
	 */
	@Autowired
	public Persona guardarPersona(Persona persona);
	@Autowired
	public Iterable<Persona> guardarPersonas(List<Persona> personas);

	/**
	 * Busca los datos de una persona. 
	 * 
	 * @param tipoDocumento El tipo de documento de la persona.
	 * @param documento El número de documento de la persona.
	 * @return La entidad correspondiente a la persona buscada.
	 */
	@Autowired
	public Persona buscarPersona(String tipoDocumento, String documento);
}
