package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.DatosBasicosNegocioDTO;
import com.accionfiduciaria.modelo.dtos.DatosBasicosValorDominioDTO;
import com.accionfiduciaria.modelo.dtos.DatosDistribucionEnvioDTO;
import com.accionfiduciaria.modelo.dtos.DatosDistribucionRecepcionDTO;
import com.accionfiduciaria.modelo.dtos.ListaNegociosBeneficiarioDTO;
import com.accionfiduciaria.modelo.dtos.TipoNegocioDTO;
import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;

/**
 * Clase que implementa la interfaz {@link IServicioNegocioTitular}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
@PropertySource("classpath:propiedades.properties")
@Transactional
public class ServicioNegocioTitular implements IServicioNegocioTitular {

	@Autowired
	private IServicioTitular servicioTitular;

	@Autowired
	private IServicioRetencionTitular servicioRetencionTitular;

	@Autowired
	private IServicioDominio servicioDominio;

	@Value("${param.id.url.servicio.accion.buscar.beneficiario}")
	private Integer paramIdUrlServicioAccionBusquedadBeneficiario;

	@Value("${param.id.url.servicio.accion.buscar.negocios.beneficiario}")
	private Integer paramIdUrlServicioAccionBusquedadNegociosBeneficiario;

	@Value("${prop.nombre}")
	private String propNombre;

	@Value("${prop.documento}")
	private String propDocumento;

	@Value("${cod.dominio.retenciones}")
	private String codigoDominioRetencion;
	
	@Value("${retencion.invisible}")
	private String retencionInvisible;

	@Override
	public ListaNegociosBeneficiarioDTO obtenerNegocios(String tipoDocumento, String documento, int pagina,
			int elementos, String tokenAutorizacion) throws Exception {
		
		List<Titular> listaTitular = servicioTitular.buscarTitularesPorDocumento(documento);

		if (listaTitular == null || listaTitular.isEmpty())
			return new ListaNegociosBeneficiarioDTO(0, new ArrayList<DatosBasicosNegocioDTO>());

		List<DatosBasicosNegocioDTO> negocios = listaTitular.stream()
				.map(titular -> new DatosBasicosNegocioDTO(titular.getNegocio().getCodSfc(), titular.getNegocio().getNombre()))
				.collect(Collectors.toList());

		return new ListaNegociosBeneficiarioDTO(negocios.size(), negocios);
	}

	@Override
	public DatosDistribucionEnvioDTO obtenerDatosDistribucion(String tipoDocumentoVinculado, String documentoVinculado,
			String codigoNegocio, String tokenAutorizacion) {
		return procesarRespuestaConsultaDatosDistribucion(tipoDocumentoVinculado, documentoVinculado, codigoNegocio);
	}

	@Override
	public void guardarDatosDistribucion(DatosDistribucionRecepcionDTO datosDistribucionRecepcionDTO) {
		procesarGuardadoDatosDistribuccion(datosDistribucionRecepcionDTO);
	}

	/**
	 * Realiza el proceso de guardado de lso datos de distribución.
	 * 
	 * @param datosDistribucionRecepcionDTO DTO con los datos a almacenar.
	 */
	private void procesarGuardadoDatosDistribuccion(DatosDistribucionRecepcionDTO datosDistribucionRecepcionDTO) {
		Titular titular = servicioTitular.buscarTitular(datosDistribucionRecepcionDTO.getTipoDocumento(),
				datosDistribucionRecepcionDTO.getDocumento(), datosDistribucionRecepcionDTO.getCodigoNegocio());
		int tipoNaturalezaAnterior = titular.getIdTipoNaturalezaJuridica();
		titular.setAsesor(datosDistribucionRecepcionDTO.getAsesor());
		titular.setRadicado(datosDistribucionRecepcionDTO.getRadicadoVinculacion());
		titular.setIdTipoNaturalezaJuridica(datosDistribucionRecepcionDTO.getIdTipoNaturaleza());
		servicioTitular.guardarTitular(titular);
		guardarRetencionesTitular(datosDistribucionRecepcionDTO, titular,tipoNaturalezaAnterior);
	}

	/**
	 * Guarda las retenciones asociadas a un titular.
	 *  
	 * @param datosDistribucionRecepcionDTO DTO con los datos requeridos para
	 *                                      realizar el guardado.
	 */
	private void guardarRetencionesTitular(DatosDistribucionRecepcionDTO datosDistribucionRecepcionDTO, Titular titular,
			int tipoNaturalezaAnterior) {

		List<RetencionTitular> retencionesActuales = servicioRetencionTitular
				.buscarRetencionesTitularPorTipoNaturalezaNegocioYTitular(tipoNaturalezaAnterior,titular);

		List<RetencionTitular> retencionesEnviadas = datosDistribucionRecepcionDTO.getRetencionesTipoNaturaleza().stream()
				.map(idRetencion -> new RetencionTitular(idRetencion, datosDistribucionRecepcionDTO.getCodigoNegocio(),
						datosDistribucionRecepcionDTO.getDocumento(), datosDistribucionRecepcionDTO.getTipoDocumento(),
						Boolean.TRUE, datosDistribucionRecepcionDTO.getIdTipoNaturaleza()))
				.collect(Collectors.toList());

		retencionesActuales.forEach(retencionActual -> {
			if(!retencionesEnviadas.contains(retencionActual)){
				retencionActual.setActivo(Boolean.FALSE);
				servicioRetencionTitular.guardarRetencionTitular(retencionActual);
			}
		});
		
		servicioRetencionTitular.guardarRetencionesTitular(retencionesEnviadas);
		
		datosDistribucionRecepcionDTO.getRetencionesTipoNegocioEliminadas()
				.forEach(retencionesEliminadasTipoNegocio -> {
					retencionesEliminadasTipoNegocio.getRetenciones()
							.forEach(idRetencion -> servicioRetencionTitular.guardarRetencionTitular(
									new RetencionTitular(idRetencion, datosDistribucionRecepcionDTO.getCodigoNegocio(),
											datosDistribucionRecepcionDTO.getDocumento(),
											datosDistribucionRecepcionDTO.getTipoDocumento(), Boolean.FALSE,
											retencionesEliminadasTipoNegocio.getIdTipoNegocio())));
				});
	}

	/**
	 * Procesa la respuesta de la consulta para su envío al front.
	 * 
	 * @param tipoDocumentoVinculado
	 * @param documentoVinculado
	 * @param codigoNegocio
	 * @return
	 */
	private DatosDistribucionEnvioDTO procesarRespuestaConsultaDatosDistribucion(String tipoDocumentoVinculado,
			String documentoVinculado, String codigoNegocio) {

		Titular titular = servicioTitular.buscarTitular(tipoDocumentoVinculado, documentoVinculado, codigoNegocio);
		DatosDistribucionEnvioDTO datosDistribuccion = new DatosDistribucionEnvioDTO();

		datosDistribuccion.setTipoDocumento(tipoDocumentoVinculado);
		datosDistribuccion.setDocumento(documentoVinculado);
		datosDistribuccion.setCodigoNegocio(titular.getNegocio().getCodSfc());
		datosDistribuccion.setNombreNegocio(titular.getNegocio().getNombre());
		datosDistribuccion.setDerechos(titular.getNumeroDerechos() > 0 ? String.valueOf(titular.getNumeroDerechos()) : "NO APLICA");
		datosDistribuccion.setPorcentajeParticipacion(titular.getPorcentaje().toString());
		datosDistribuccion.setTipoTitularidad(titular.getEsCotitular().equals(true) ? "Cotitular" : "Titular");
		buscarRetencionesTitular(datosDistribuccion, titular);
		datosDistribuccion.setIdTipoNaturaleza(titular.getIdTipoNaturalezaJuridica());
		datosDistribuccion.setAsesor(titular.getAsesor());
		datosDistribuccion.setRadicadoVinculacion(titular.getRadicado());

		return datosDistribuccion;
	}

	/**
	 * Carga los datos del titular o cotitular del negocio, si el tituar no tiene
	 * retenciones en la tabla NEG_RETENCION_TITULAR se toman las retenciones del
	 * tipo de negocio y el tipo de naturaleza juridica de la persona.
	 * 
	 * @param datosDistribuccion DTO con el listado de retenciones encontradas
	 * @param titular            Entidad con los datos del titular o cotitular del
	 *                           negocio.
	 */
	private void buscarRetencionesTitular(DatosDistribucionEnvioDTO datosDistribuccion, Titular titular) {

		List<DatosBasicosValorDominioDTO> retencionesTitularDTO = new ArrayList<>();
		List<TipoNegocioDTO> retencionesNegocioDTO = new ArrayList<>();

		Map<Integer,List<RetencionTitular>> mapaRetencionesNegocio = new HashedMap<>();
		
		ValorDominioNegocio valorRetencionInvisible = servicioDominio
				.buscarPorCodigoDominioYDescripcion(codigoDominioRetencion, retencionInvisible, Boolean.FALSE);
		
		titular.getRetencionTitularList().stream().filter(retencion -> !retencion.getRetencionTitularPK()
				.getIdRetencion().equals(valorRetencionInvisible.getIdValorDominio())).forEach(retencion -> {
					if (Boolean.TRUE.equals(retencion.getActivo())) {
						if (retencion.getRetencionTitularPK().getTipoNaturalezaNegocio()
								.equals(titular.getIdTipoNaturalezaJuridica())) {
							retencionesTitularDTO.add(retencionTitularADTO(retencion));
						} else {
							if (mapaRetencionesNegocio
									.get(retencion.getRetencionTitularPK().getTipoNaturalezaNegocio()) == null) {
								mapaRetencionesNegocio.put(retencion.getRetencionTitularPK().getTipoNaturalezaNegocio(),
										new ArrayList<>());
							}
							mapaRetencionesNegocio.get(retencion.getRetencionTitularPK().getTipoNaturalezaNegocio())
									.add(retencion);
						}
					}
				});

		mapaRetencionesNegocio.forEach((idTipoNegocio, retencionesTitular) -> {
			ValorDominioNegocio tipoNegocio = servicioDominio.buscarValorDominio(idTipoNegocio);
			TipoNegocioDTO tipoNegocioDTO = new TipoNegocioDTO();
			tipoNegocioDTO.setId(idTipoNegocio);
			tipoNegocioDTO.setDescripcion(tipoNegocio.getDescripcion());
			tipoNegocioDTO.setRetenciones(retencionesTitular.stream().map(retencion -> retencionTitularADTO(retencion)).collect(Collectors.toList()));
			retencionesNegocioDTO.add(tipoNegocioDTO);
		});
		
		datosDistribuccion.setRetencionesTipoNaturaleza(retencionesTitularDTO);
		datosDistribuccion.setRetencionesNegocio(retencionesNegocioDTO);
	}

	/**
	 * Busca los datos de la retención asociada a un titular y la convierte a un DTO
	 * {@link DatosBasicosValorDominioDTO}
	 * 
	 * @param retencionTitular La entidad con el id de la retención del titular.
	 * @return El DTo correpondiente a la retención enviada.
	 */
	private DatosBasicosValorDominioDTO retencionTitularADTO(RetencionTitular retencionTitular) {
		ValorDominioNegocio retencion = servicioDominio
				.buscarValorDominio(retencionTitular.getRetencionTitularPK().getIdRetencion());
		return valorDominioNegocioADatosBasicosValorDominioDTO(retencion);
	}

	/**
	 * Convierte una entidad {@link ValorDominioNegocio} a un DTO
	 * {@link DatosBasicosValorDominioDTO}.
	 * 
	 * @param valorDominio La entidad a convertir.
	 * @return El DTO correspondiente a la entidad enviada.
	 */
	private DatosBasicosValorDominioDTO valorDominioNegocioADatosBasicosValorDominioDTO(
			ValorDominioNegocio valorDominio) {
		return new DatosBasicosValorDominioDTO(valorDominio.getIdValorDominio(), valorDominio.getDescripcion(),
				valorDominio.getUnidad(), valorDominio.getValor());
	}
}
