package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.TitularNoEncontradoException;
import com.accionfiduciaria.modelo.dtos.CotitularDTO;
import com.accionfiduciaria.modelo.dtos.DatosAGuardarCotitularDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaTitularesDTO;
import com.accionfiduciaria.modelo.entidad.Titular;

/**
 * Clase que maneja a logica de los titulares de un negocio.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioTitular {
	
	/**
	 * Busca los datos de un titular.
	 * 
	 * @param tipoDocumento El tipo de documento de titular.
	 * @param documento E número de documento del titular.
	 * @param codigoSFC El código del negocio asociado al titular.
	 * @return
	 */
	@Autowired
	public Titular buscarTitular(String tipoDocumento, String documento, String codigoSFC);
	
	@Autowired
	public List<RespuestaTitularesDTO> obtenerTitulares(String codigoNegocio);

	/**
	 * Guarda la infromación de un titular.
	 * 
	 * @param titular a entidad a guardar.
	 * @return El titular guardado.
	 */
	@Autowired
	public Titular guardarTitular(Titular titular);
	
	@Autowired
	public List<CotitularDTO> obtenerCotitular(String tipoDocumentoTitular, String documentotitular, String codigoNegocio,
			String argumento, String criterioBuscar) throws TitularNoEncontradoException;

	@Autowired
	public void administrarCotitular(DatosAGuardarCotitularDTO datosAGuardarCotitularDTO) throws TitularNoEncontradoException;

	@Autowired
	public List<Titular> obtenerTitularesPersona(String tipoDocumento, String numeroDocumento);
	
	@Autowired 
	public List<Titular> buscarTitularesPorDocumento(String numeroDocumento);
	
	@Autowired
	public List<Titular> buscarTitularesPorCodigoNegocio(String codigoNegocio);

	@Autowired
	public List<Titular> buscarTitularesPorTipoNaturalezaNegocio(Integer idTipoNaturalezaNegocio);
	
	@Autowired
	public List<Titular> buscarTitularNombrePaginado(String nombreTitular, int pagina, int elementos);
	
	@Autowired
	public List<Titular> buscarTitularesPersonaPaginado(String tipoDocumento, String numeroDocumento, int pagina, int elementos);

	@Autowired
	public List<Titular> buscarTitularesPorCodigoNegocioPaginado(String codigoNegocio, int pagina, int elementos);

	@Autowired
	public List<Titular> buscarTitularesPorDocumentoPaginado(String numeroDocumento, int pagina, int elementos);
}
