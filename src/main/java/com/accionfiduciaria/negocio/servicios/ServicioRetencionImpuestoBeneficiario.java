/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitular;
import com.accionfiduciaria.modelo.entidad.RetencionImpuestoBeneficiario;
import com.accionfiduciaria.modelo.entidad.RetencionImpuestoBeneficiarioPK;
import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioRetencionImpuestoBeneficiario;

/**
 * @author efarias
 *
 */
@Service
public class ServicioRetencionImpuestoBeneficiario implements IServicioRetencionImpuestoBeneficiario {

	@Autowired
	private RepositorioRetencionImpuestoBeneficiario repositorioRetencionImpuestoBeneficiario;

	@Autowired
	private Utilidades utilidades;
	
	/**
	 * Este método cálcula la retención de impuesto de beneficiario para cada
	 * distribución de beneficiario y sus respectivas retenciones de titular.
	 * 
	 * @author efarias
	 * @param listaDistribucion
	 * @param distribucion
	 */
	@Override
	public void calcularRetencionImpuestoBeneficiarios(List<DistribucionBeneficiario> listaDistribucion,
			Distribucion distribucion) {

		if (!listaDistribucion.isEmpty()) {
			List<RetencionImpuestoBeneficiario> listaRetencionImpuestoBeneficiario = new ArrayList<>();
			List<DistribucionRetencionesTitular> listaRetenciones = distribucion.getDistribucionRetencionesTitular();
			listaDistribucion.forEach(distBeneficiario -> {
				if (!listaRetenciones.isEmpty()) {

					// Se filtra las retenciones titular de acuerdo al titular asociado al
					// beneficiario.
					List<DistribucionRetencionesTitular> listaRetencionesFiltrada = listaRetenciones.stream()
							.filter(ret -> ret.getDistribucionRetencionesTitularPK().getRetTipoDocTitular().equals(
									distBeneficiario.getBeneficiario().getBeneficiarioPK().getTipoDocumentoTitular()))
							.filter(ret -> ret.getDistribucionRetencionesTitularPK().getRetNumDocTitular().equals(
									distBeneficiario.getBeneficiario().getBeneficiarioPK().getNumeroDocumentoTitular()))
							.collect(Collectors.toList());

					// Se calcula las retenciones del beneficiario con las retenciones del titular.
					if (!listaRetencionesFiltrada.isEmpty())
						listaRetencionImpuestoBeneficiario.addAll(
								calcularRetencionImpuestoPorBeneficiario(listaRetencionesFiltrada, distBeneficiario));
				}
			});
			// Guarda el detalle de retención impuesto beneficiario.
			if (!listaRetencionImpuestoBeneficiario.isEmpty())
				repositorioRetencionImpuestoBeneficiario.saveAll(listaRetencionImpuestoBeneficiario);
		}
	}

	/**
	 * Este método realiza el cálculo de retenciones de impuesto de benficiarios por
	 * cada distribución de beneficiario, basado en las distribución de retenciones
	 * de su respetivo titular.
	 * 
	 * @author efarias
	 * @param listaRetenciones
	 * @param distBeneficiario
	 * @return List<RetencionImpuestoNegocio> -> Detalle de retenciones por
	 *         distribución de beneficiario.
	 */
	public List<RetencionImpuestoBeneficiario> calcularRetencionImpuestoPorBeneficiario(
			List<DistribucionRetencionesTitular> listaRetenciones, DistribucionBeneficiario distBeneficiario) {

		List<RetencionImpuestoBeneficiario> listaRetencionImpuestoBeneficiario = new ArrayList<>();
		listaRetenciones.forEach(retencion -> {

			// Valor base de cálculo.
			BigDecimal valorBase = distBeneficiario.getValorPago();

			RetencionTitular retencionTitular = retencion.getRetencionTitular();
			RetencionImpuestoBeneficiario detalle = new RetencionImpuestoBeneficiario(
					retencionTitular.getRetencionTitularPK(), distBeneficiario.getDistribucionBeneficiarioPK());
			detalle.setUnidad(retencionTitular.getDominioRetencion().getUnidad());
			detalle.setValorConfiguradoImpuesto(new BigDecimal(retencionTitular.getDominioRetencion().getValor()));
			detalle.setValorBase(valorBase);
			detalle.setValorImpuesto(
					utilidades.calcularValorImpuesto(retencionTitular.getDominioRetencion(), valorBase));
			listaRetencionImpuestoBeneficiario.add(detalle);
		});
		return listaRetencionImpuestoBeneficiario;
	}

	/**
	 * Agrega los nuevos registros de retención de beneficiario actualizando los
	 * datos del nuevo registro de distribución.
	 * 
	 * @author efarias
	 * @param distribucionBeneficiarioModificado
	 * @param distribucionBeneficiario
	 */
	@Override
	public void agregarRetencionImpuestos(DistribucionBeneficiario distribucionBeneficiarioModificado,
			DistribucionBeneficiario distribucionBeneficiario) {
		List<RetencionImpuestoBeneficiario> listaRetenciones = distribucionBeneficiario
				.getRetencionImpuestosBeneficiarios();

		if (!listaRetenciones.isEmpty()) {
			List<RetencionImpuestoBeneficiario> nuevaListaRetencion = new ArrayList<>();
			listaRetenciones.forEach(retAnterior -> {
				Integer idRetencion = retAnterior.getRetencionImpuestoBeneficiarioPK().getIdRetencion();
				String retTitularCodSfc = retAnterior.getRetencionImpuestoBeneficiarioPK().getRetTitularCodSfc();
				String retNumDocTitular = retAnterior.getRetencionImpuestoBeneficiarioPK().getRetNumDocTitular();
				String retTipoDocTitular = retAnterior.getRetencionImpuestoBeneficiarioPK().getRetTipoDocTitular();
				RetencionImpuestoBeneficiarioPK retencionPK = new RetencionImpuestoBeneficiarioPK(idRetencion,
						retTitularCodSfc, retNumDocTitular, retTipoDocTitular,
						distribucionBeneficiarioModificado.getDistribucionBeneficiarioPK(), retAnterior.getRetencionImpuestoBeneficiarioPK().getRetencionTitularTipoNaturaleza());
				RetencionImpuestoBeneficiario retencionNueva = new RetencionImpuestoBeneficiario(retencionPK,
						retAnterior.getUnidad(), retAnterior.getValorConfiguradoImpuesto(), retAnterior.getValorBase(),
						retAnterior.getValorImpuesto());
				nuevaListaRetencion.add(retencionNueva);
			});
			repositorioRetencionImpuestoBeneficiario.saveAll(nuevaListaRetencion);
		}
	}

	/**
	 * Elimina los registros retención de la antigua distribución del beneficiario:
	 * 
	 * @author efarias
	 * @param distribucionBeneficiario
	 */
	@Override
	public void eliminarRetencionesPorDistribucionBeneficiario(DistribucionBeneficiario distribucionBeneficiario) {
		List<RetencionImpuestoBeneficiario> listaRetencion = distribucionBeneficiario
				.getRetencionImpuestosBeneficiarios();
		if (!listaRetencion.isEmpty()) {
			listaRetencion.forEach(retencion -> {
				repositorioRetencionImpuestoBeneficiario
						.eliminarRetencionImpuestoBeneficiario(retencion.getRetencionImpuestoBeneficiarioPK());
			});
		}
	}
}
