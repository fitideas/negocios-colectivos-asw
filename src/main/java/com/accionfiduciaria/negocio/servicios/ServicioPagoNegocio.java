/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.IngresosTipoOperacionDTO;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;
import com.accionfiduciaria.modelo.entidad.PagoNegocioPK;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioPagoNegocio;

/**
 * @author efarias
 *
 */
@Service
@Transactional
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:propiedades.properties")
public class ServicioPagoNegocio implements IServicioPagoNegocio {
	
	@Autowired
	private RepositorioPagoNegocio repositorioPagoNegocio;
	
	// Constantes
	public static final String DISTRIBUCION_EXITOSA = "1";
	public static final String DISTRIBUION_NO_REALIZADA = "0";
	

	/**
	 * Este método obtiene una lista de objetos de PagoNegocio con los parámetros
	 * lista de de tipos de negocio y el periodo.
	 * 
	 * @author efarias
	 * 
	 * @param listaTiposNegocio
	 * @param periodo
	 * @return List<PagoNegocio>
	 */
	@Override
	public List<PagoNegocio> obtenerListaIdsPagoNegocio(List<TipoNegocio> listaTiposNegocio, String periodo) {

		return listaTiposNegocio
				.stream().map(tipoNegocio -> obtenerPagoNegocio(periodo,
						tipoNegocio.getTipoNegocioPK().getTipoNegocio(), tipoNegocio.getTipoNegocioPK().getCodSfc()))
				.collect(Collectors.toList());
	}

	/**
	 * Este método contruye un objeto en base a los parámetros del constructor
	 * especificado.
	 * 
	 * @author efarias
	 * 
	 * @param periodo
	 * @param tipoNegocio
	 * @param codSfc
	 * @return PagoNegocio
	 */
	public PagoNegocio obtenerPagoNegocio(String periodo, Integer tipoNegocio, String codSfc) {
		return new PagoNegocio(periodo, tipoNegocio, codSfc);
	}

	/**
	 * Este método verifica si existe un un registro en la entidad pago negocio, consultando por su estado
	 * 
	 * @author efarias
	 * @param pagoNegocioPK
	 * @return boolean
	 *	
	 */
	@Override
	public boolean existePagoNegocio(PagoNegocioPK pagoNegocioPK) {
		return repositorioPagoNegocio.existsById(pagoNegocioPK);
	}

	/**
	 * Este método busca un registro de pago negocio por su PK, si no encuentra el
	 * registro devuelve un objecto null.
	 * 
	 * @author efarias
	 * @param pagoNegocioPK
	 * @return PagoNegocio
	 * 
	 */
	@Override
	public PagoNegocio buscarPagoNegocio(PagoNegocioPK pagoNegocioPK) {
		return repositorioPagoNegocio.findById(pagoNegocioPK).orElse(null);
	}

	/**
	 * Este método guarda un objeto de tipo pago negocio.
	 * 
	 * @author efarias
	 * @param PagoNegocio
	 * @return PagoNegocio
	 * 
	 */
	@Override
	public PagoNegocio guardarPagoNegocio(PagoNegocio pagoNegocio) {
		return repositorioPagoNegocio.save(pagoNegocio);
	}

	/**
	 * Este método trasnforma un ingresosTipoOperacionDTO a un objeto tipo
	 * PagoNegocio.
	 * 
	 * @author efarias
	 * @param ingresosTipoOperacionDTO
	 * @param Periodo
	 * @return PagoNegocio
	 * 
	 */
	@Override
	public PagoNegocio obtenerPagoNegocio(IngresosTipoOperacionDTO ingresosTipoOperacionDTO, String codigoNegocio,
			String periodo) {

		PagoNegocioPK pagoNegocioPK = new PagoNegocioPK(periodo, ingresosTipoOperacionDTO.getCodigoTipoNegocio(),
				codigoNegocio);
		PagoNegocio pagoNegocio;

		pagoNegocio = buscarPagoNegocio(pagoNegocioPK);

		if (pagoNegocio != null) {
			return pagoNegocio;
		} else {
			PagoNegocio pagoNegocioSave = new PagoNegocio(periodo, ingresosTipoOperacionDTO.getCodigoTipoNegocio(),
					codigoNegocio);
			pagoNegocioSave.setDistribucionExitosa(DISTRIBUION_NO_REALIZADA);
			pagoNegocioSave.setEncargoCodSfc(codigoNegocio);
			pagoNegocioSave.setCuenta(ingresosTipoOperacionDTO.getCuenta());
			return guardarPagoNegocio(pagoNegocioSave);
		}
	}
}
