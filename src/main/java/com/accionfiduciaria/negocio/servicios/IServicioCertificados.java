/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.io.IOException;
import java.util.List;

import com.accionfiduciaria.excepciones.ArchivoNoValidoException;
import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.ParametroNoEncontradoException;
import com.accionfiduciaria.modelo.dtos.ListaNegociosPorDerechos;
import com.accionfiduciaria.modelo.dtos.ListaTitularesDTO;
import com.accionfiduciaria.modelo.dtos.PeticionTitularesNegociosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoPagoZipDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaDatosCertificadosDTO;

import fr.opensagres.xdocreport.core.XDocReportException;

/**
 * @author fcher
 * servicio encargado e la logica de las consultas para los certificados
 * 
 */

public interface IServicioCertificados {

	public ListaTitularesDTO obtenerTitularesPaginado(PeticionTitularesNegociosDTO dto, String tokenAutorizacion);

	public ListaNegociosPorDerechos obtenerNegociosPaginado(PeticionTitularesNegociosDTO dto, String tokenAutorizacion);

	public List<RespuestaDatosCertificadosDTO> obtenerDatosDelCertificado(PeticionTitularesNegociosDTO[] dto, String paramTipo,
			String tokenAutorizacion) throws BusquedadSinResultadosException;

	public RespuestaArchivoPagoZipDTO generarCertificados(String tipo, PeticionTitularesNegociosDTO[] peticion,
			String tokenAutorizacion) throws IOException, XDocReportException, ParametroNoEncontradoException, ArchivoNoValidoException, BusquedadSinResultadosException;

	public RespuestaArchivoPagoZipDTO generarArchivoParticipacion(List<RespuestaDatosCertificadosDTO> dtoSalida)
			throws IOException, XDocReportException, ArchivoNoValidoException;

	public RespuestaArchivoPagoZipDTO generarArchivoIngRet(List<RespuestaDatosCertificadosDTO> dtoSalida)
			throws IOException, XDocReportException, ArchivoNoValidoException;

	public RespuestaArchivoPagoZipDTO generarArchivoRentaExc(List<RespuestaDatosCertificadosDTO> dtoSalida)
			throws IOException, XDocReportException, ArchivoNoValidoException;

}
