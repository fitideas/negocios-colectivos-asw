package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.HomologacionCampos;
import com.accionfiduciaria.negocio.repositorios.RepositorioHomologacionCampos;

@Service
public class ServicioHomologacionCampos implements IServicioHomologacionCampos {

	@Autowired
	private RepositorioHomologacionCampos repositorioHomologacionCampos;
	
	@Override
	public String homologarCampo(String nombreCampo) {
		HomologacionCampos homologacionCampos = repositorioHomologacionCampos.findById(nombreCampo).orElse(null);
		return homologacionCampos != null ? homologacionCampos.getNombreHomologado() : "";
	}

}
