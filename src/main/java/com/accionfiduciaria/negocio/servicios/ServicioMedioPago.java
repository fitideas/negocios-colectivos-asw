package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.datosFinancierosBeneficiarioDTO;
import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MedioPagoPK;
import com.accionfiduciaria.negocio.repositorios.RepositorioMedioPago;

@Service
public class ServicioMedioPago implements IServicioMedioPago {

	@Autowired
	RepositorioMedioPago repositorioMedioPago;

	// Constantes
	private static final String OBSERVACION_CREACION = "Medio pago creado reproceso de pagos";

	@Override
	public MedioPago guardarMedioPago(MedioPago medioPago) {
		return repositorioMedioPago.save(medioPago);
	}

	@Override
	public boolean existeMedioPago(MedioPagoPK medioPagoPK) {
		return repositorioMedioPago.existsById(medioPagoPK);
	}

	@Override
	public MedioPago buscarMedioPagoPorID(MedioPagoPK medioPagoPK) {
		return repositorioMedioPago.findById(medioPagoPK).orElse(null);
	}
	
	@Transactional
	@Override
	public MedioPago verificarCrearMedioPago(datosFinancierosBeneficiarioDTO datoNuevo) {

		MedioPago medioPago;
		MedioPagoPK medioPagoPK = new MedioPagoPK(datoNuevo.getCodigoBancario(), datoNuevo.getNumeroCuenta(),
				datoNuevo.getTipoCuenta());

		medioPago = repositorioMedioPago.findById(medioPagoPK).orElse(null);

		if (medioPago != null) {
			return medioPago;
		} else {
			MedioPago medioPagoNuevo = new MedioPago(medioPagoPK);
			medioPagoNuevo.setObservacion(OBSERVACION_CREACION);
			return repositorioMedioPago.saveAndFlush(medioPagoNuevo);
		}
	}

}
