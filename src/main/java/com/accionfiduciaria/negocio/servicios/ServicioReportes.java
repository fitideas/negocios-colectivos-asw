package com.accionfiduciaria.negocio.servicios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.modelo.dtos.ConceptosDistribucionDTO;
import com.accionfiduciaria.modelo.dtos.DatosPersonaUsuarioDTO;
import com.accionfiduciaria.modelo.dtos.PeticionInformesModificacionDTO;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.Usuario;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.especificaciones.DistribucionEspecificacion;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucion;
import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioUsuario;



@Service
@Transactional
@PropertySource("classpath:propiedades.properties")

//Clase que permite el manejo de los servicios correspondientes a los reportes del modulo 
//informes y reportes

public class ServicioReportes implements IServicioReportes{
	
	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	@Autowired
	private Utilidades utilidades;

	@Autowired
	private RepositorioDistribucion repositorioDistribucion;

	@Autowired
	IServicioUsuario servicioUsuario;
	
	
	private static final String DISTRIBUIDO = "DISTRIBUIDO";
	
	public List<DatosPersonaUsuarioDTO> obtenerUsuarios(){
		List<DatosPersonaUsuarioDTO> dto=new ArrayList<>();
			Iterator<Usuario> iterador = servicioUsuario.obtenerTodos().iterator();
		while(iterador.hasNext()) {
			Usuario usuario = iterador.next();
			dto.add(new DatosPersonaUsuarioDTO(usuario.getPersona().getNombreCompleto(), 
					usuario.getPersona().getPersonaPK().getTipoDocumento(), 
					usuario.getPersona().getPersonaPK().getNumeroDocumento(),
					usuario.getLogin()));
		}
			return dto;
			
	}

	@Override
	public List<Distribucion> consultarConFiltros(PeticionInformesModificacionDTO dto) {
		List<Distribucion> resultados;

		if ((dto.getCodigoNegocio().isEmpty() && dto.getCodigoTipoNegocio().isEmpty() && (dto.getRangoFecha().isEmpty()
				|| dto.getRangoFecha().get(0) == null || dto.getRangoFecha().get(1) == null))) {
			resultados = repositorioDistribucion.findAll();
		} else {
			Specification<Distribucion> especificacionCombinada = Specification
					.where(dto.getCodigoNegocio() == null || dto.getCodigoNegocio().isEmpty() ? null
							: DistribucionEspecificacion.filtrarPorNegocios(dto.getCodigoNegocio()))
					.and(dto.getCodigoTipoNegocio().isEmpty() ? null
							: DistribucionEspecificacion.filtrarPorTipoNegocio(dto.getCodigoTipoNegocio()))
					.and(dto.getRangoFecha().isEmpty() || dto.getRangoFecha().get(0) == null ? null
							: DistribucionEspecificacion.filtrarPorFechaInicial(
									utilidades.convertirFecha(dto.getRangoFecha().get(0), formatoddmmyyyy)))
					.and(dto.getRangoFecha().isEmpty() || dto.getRangoFecha().get(1) == null ? null
							: DistribucionEspecificacion.filtrarPorFechaFinal(
									utilidades.convertirFecha(dto.getRangoFecha().get(1), formatoddmmyyyy)));
			resultados = repositorioDistribucion.findAll(especificacionCombinada);
		}
		return resultados;

	}

	@Override
	public ConceptosDistribucionDTO obtenerConteoSolicitudes(PeticionInformesModificacionDTO dto) {
		List <Distribucion> conteo = consultarConFiltros(dto);
		BigDecimal totalValorIngresos = new BigDecimal(0);
		BigDecimal totalValorEgresos = new BigDecimal(0);
		BigDecimal totalValorRetenciones = new BigDecimal(0);
		BigDecimal totalValorDistribuciones = new BigDecimal(0);
		for(Distribucion d : conteo)  {
			if(d.getEstado().equals(DISTRIBUIDO)) {
				totalValorIngresos = totalValorIngresos.add(d.getTotal());
				totalValorEgresos = totalValorEgresos.add(d.getTotalObligaciones());
				totalValorRetenciones = totalValorRetenciones.add(d.getTotalRetenciones());
				totalValorDistribuciones = totalValorDistribuciones.add(d.getTotalDistribucion());
			}
		}
		
		ConceptosDistribucionDTO conceptoDistribuir = new ConceptosDistribucionDTO();
		
		conceptoDistribuir.setTotal(totalValorIngresos);
		conceptoDistribuir.setTotalDistribucion(totalValorDistribuciones);
		conceptoDistribuir.setTotalRetenciones(totalValorRetenciones);
		conceptoDistribuir.setTotalObligaciones(totalValorEgresos);
		
		return conceptoDistribuir;	
}

}
