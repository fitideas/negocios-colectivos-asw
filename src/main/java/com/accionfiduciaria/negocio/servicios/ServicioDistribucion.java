/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.enumeradores.EstadosDistribucion;
import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.FaltanDatosObligatoriosException;
import com.accionfiduciaria.excepciones.NoExisteDistribucionDisponible;
import com.accionfiduciaria.excepciones.NoExistenIngresosException;
import com.accionfiduciaria.modelo.dtos.CalculoObligacionesTipoNegocioDTO;
import com.accionfiduciaria.modelo.dtos.ConceptoDTO;
import com.accionfiduciaria.modelo.dtos.ReprocesoPagoBeneficiarioDTO;
import com.accionfiduciaria.modelo.dtos.ReprocesoPagosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoPagoZipDTO;
import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.DistribucionRetenciones;
import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitular;
import com.accionfiduciaria.modelo.entidad.IngresoOperacion;
import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersona;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;
import com.accionfiduciaria.modelo.entidad.PagoNegocioPK;
import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucion;
import com.accionfiduciaria.negocio.repositorios.RepositorioValorDominioNegocio;

/**
 * @author efarias
 *
 */
@Service
@Transactional
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:propiedades.properties")
public class ServicioDistribucion implements IServicioDistribucion {

	@Autowired
	private RepositorioDistribucion repositorioDistribucion;

	@Autowired
	private RepositorioValorDominioNegocio repositorioValorDominioNegocio;

	@Autowired
	private IServicioIngresoOperacion servicioIngresoOperacion;

	@Autowired
	private IServicioObligacionNegocio servicioObligacionNegocio;
	
	@Autowired
	private IServicioDistribucionBeneficiario servicioDistribucionBeneficiario;
	
	@Autowired
	private IServicioDistribucionObligaciones servicioDistribucionObligaciones;
	
	@Autowired
	private IServicioBeneficiario servicioBeneficiario; 
	
	@Autowired
	private IServicioDominio servicioDominio;

	@Autowired
	private IServicioDistribucionRetenciones servicioDistribucionRetenciones;
	
	@Autowired
	private IServicioDistribucionRetencionesTitular servicioDistribucionRetencionesTitular;
	
	@Autowired
	private IServicioPagoNegocio servicioPagoNegocio;
	
	@Autowired
	private IServicioRetencionImpuestoBeneficiario servicioRetencionImpuestoBeneficiario;
	
	@Autowired
	private IServicioMedioPago servicioMedioPago;
	
	@Autowired
	private IServicioMedioPagoPersona servicioMedioPagoPersona;

	@Autowired
	private IServicioTitular servicioTitular;

	@Autowired
	private IServicioRetencionTitular servicioRetencionTitular;
	
	@Autowired
	private Utilidades utilidades;

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	@Value("${cod.dominio.tipo.negocio}")
	private String codigoDominioTipoNegocio;
	
	@Value("${cod.dominio.costo.tipo.cuenta}")
	private String codigoDominioCosMedPag;
	
	@Value("${cod.dominio.retenciones}")
	private String codigoDominioRetencion;
	
	@Value("${retencion.invisible}")
	private String retencionInvisible;
	
	private static final Logger log = Logger.getLogger(ServicioDistribucion.class.getSimpleName());

	// Constantes Execpcion
	public static final String NO_EXISTEN_INGRESOS = "No se encontraron ingresos para el periodo y tipo de negocio especificado";
	public static final String NO_EXISTE_DISTRIBUCION_DISPONIBLE = "No existe un resgitro de distribución disponible.";
	public static final String NO_EXISTE_DISTRIBUCION_DISTRIBUIDO = "No existe un resgitro de distribución en estado Distribuido.";
	public static final String NO_EXISTE_REGISTROS_COSMEDPAG = "No existe registros del parámetro costo medio de pago, por favor verificar.";
	public static final String NO_EXISTEN_DIST_BENEFICIARIO = "No existen registros de distribución de beneficiarios para realizar la generación de archivo de pago.";
	public static final String PAGOS_SIN_MEDIO_PAGO = "Existen tipos de pago sin un costo de medio de pago, por favor verificar.";
	public static final String NO_EXISTE_PAGOS_A_REPROCESAR = "No existen registros de pago para reprocesar de acuerdo a los criterios enviados.";
	public static final String PARAMETROS_VACIOS = "Los parámetros de búsqueda no pueden ser nulos o vacios";
	public static final String REPROCESAR_PAGO_VACIO = "No existe información a reprocesar, por favor validar.";
	public static final String ERROR_ESCRIBIR_ZIP = "Se produce un error al escribir en el archivo ZIP.";
	public static final String ERROR_OBTENER_PERIODO = "Se produció un error en obtener el periodo de pago.";
	public static final String SIN_REGISTROS = " - sin regristros en la distribución: ";

	// Constantes negocio
	public static final String TIPO_PAGO_CHEQUE = "CHEQUE";
	public static final String TIPO_PAGO_TRANSFERENCIA = "TRANSFERENCIA";
	public static final String COD_BANCO_NO_TRASNFERENCIA = "999";
	public static final String CONTENT_TYPE = "application/zip";
	public static final String EXTENSION_ZIP = ".ZIP";
	public static final String EXTENSION_TXT = ".txt";
	public static final String DELIMITADOR = "\t";
	public static final String ACTIVO = "1";
	public static final String INACTIVO = "0";
	public static final String ETAPA_FIDEICOMISO = "1";
	public static final String DELIMITADOR_RADICADOS = "-";
	public static final String FORMATO_PERIODO_OBS = " MMMM yyyy";
	
	// Constantes
	public static final String EMPTY = "";
	public static final String UNDEFINED = "undefined";
	public static final String SLASH = "/";
	public static final String UNDER_LINE = "_";
	
	/**
	 * Este método realiza el cálculo de obligación.
	 * 
	 * @author efarias
	 * @param periodo
	 * @param codigoNegocio
	 * @param codigoTipoNegocio
	 * @return Map<String, DetalleDistribucionBeneficiarioDTO>
	 */
	@Override
	public CalculoObligacionesTipoNegocioDTO calcularObligacion(String periodo, String codigoNegocio,
			Integer codigoTipoNegocio) throws FaltanDatosObligatoriosException, NoExistenIngresosException {

		if (periodo.equals(EMPTY) || codigoNegocio.equals(EMPTY) || periodo.equals(UNDEFINED)
				|| codigoNegocio.equals(UNDEFINED) || codigoTipoNegocio == null)
			throw new FaltanDatosObligatoriosException(PARAMETROS_VACIOS);
		agregarRetencionInvisibleATitulares(codigoNegocio);
		// Actualización estado a Distribución cancelada.
		PagoNegocioPK pagoNegocioPK = new PagoNegocioPK(periodo, codigoTipoNegocio, codigoNegocio);
		PagoNegocio pagoNegocio = servicioPagoNegocio.buscarPagoNegocio(pagoNegocioPK);
		String estadoDistribucionDisponible = EstadosDistribucion.DISTRIBUCION_DISPONIBLE.getValor();
		actualizarEstadoDistribucion(pagoNegocio, estadoDistribucionDisponible);

		// Obtener Detalles ingreso y obligaciones
		List<IngresoOperacion> listaIngresos = servicioIngresoOperacion.buscarIngresosOperacion(pagoNegocio);
		List<ObligacionNegocio> listaObligaciones = obtenerListaObligacionesPorPeriodo(codigoTipoNegocio, codigoNegocio,
				periodo);

		// Calcular Obligaciones
		CalculoObligacionesTipoNegocioDTO calculoObligaciones = calcularObligacionesPorPeriodo(listaIngresos,
				listaObligaciones, pagoNegocio);

		// Calcular y guardar la distribución
		Distribucion distribucion = guardarDistribucion(calculoObligaciones, pagoNegocio);
		
		// Calcula y guarda la distribución de retenciones.
		calcularRetenciones(pagoNegocio, distribucion);

		// Calcular y guardar Distribucion
		guardarDistribucionObligaciones(distribucion, listaObligaciones, calculoObligaciones.getIngresosTotal(),
				periodo);
		return calculoObligaciones;
	}
	
	
	
	/**
	 * Este método realiza el cálculo de distribución.
	 * 
	 * @author efarias
	 * @param periodo
	 * @param codigoNegocio
	 * @param codigoTipoNegocio
	 * @return Map<String, Object>
	 * @throws FaltanDatosObligatoriosException 
	 * @throws NoExisteDistribucionDisponible 
	 */
	@Override
	public Map<String, Object> calcularDistribucion(String periodo, String codigoNegocio, Integer codigoTipoNegocio)
			throws FaltanDatosObligatoriosException, NoExisteDistribucionDisponible {

		if (periodo.equals(EMPTY) || codigoNegocio.equals(EMPTY) || periodo.equals(UNDEFINED)
				|| codigoNegocio.equals(UNDEFINED) || codigoTipoNegocio == null)
			throw new FaltanDatosObligatoriosException(PARAMETROS_VACIOS);
		

		// Actualización estado.
		PagoNegocioPK pagoNegocioPK = new PagoNegocioPK(periodo, codigoTipoNegocio, codigoNegocio);
		PagoNegocio pagoNegocio = servicioPagoNegocio.buscarPagoNegocio(pagoNegocioPK);
		String estadoDistribuido = EstadosDistribucion.DISTRIBUIDO.getValor();
		String estadoDistribucionDisponible = EstadosDistribucion.DISTRIBUCION_DISPONIBLE.getValor();
		actualizarEstadoDistribucion(pagoNegocio, estadoDistribuido);
		actualizarEstadoDistribucionPagoNegocio(pagoNegocio, INACTIVO);

		// Buscar Distribución en estado DISTRIBUCION_DISPONIBLE
		Distribucion distribucion = obtenerDistribucion(pagoNegocio, estadoDistribucionDisponible);
		if (distribucion == null)
			throw new NoExisteDistribucionDisponible(NO_EXISTE_DISTRIBUCION_DISPONIBLE);
		
		List<Beneficiario> listaBeneficiarios = servicioBeneficiario.buscarBeneficiariosPorNegocio(codigoNegocio);
		List<DistribucionBeneficiario> listaDistribucion = servicioDistribucionBeneficiario
				.guardarDistribucionBeneficiarios(listaBeneficiarios, distribucion);

		// Cálculo retención impuesto beneficiario.
		servicioRetencionImpuestoBeneficiario.calcularRetencionImpuestoBeneficiarios(listaDistribucion, distribucion);

		// Crea el HashMap con la respectiva lista de distribución
		Map<String, Object> respuesta = servicioDistribucionBeneficiario.listaDistribucionADTO(listaDistribucion);

		// Actualiza Estado a Distribuido
		distribucion.setEstado(estadoDistribuido);
		repositorioDistribucion.save(distribucion);
		actualizarEstadoDistribucionPagoNegocio(pagoNegocio, ACTIVO);
		return respuesta;
	}

	/**
	 * Este método realiza la generación de archivo de pago para un periodo, negocio
	 * y tipo de negocio especificado..
	 * 
	 * @author efarias
	 * @param periodo
	 * @param codigoNegocio
	 * @param codigoTipoNegocio
	 * @throws IOException
	 * @throws FaltanDatosObligatoriosException
	 * @throws NoExisteDistribucionDisponible
	 * @throws BusquedadSinResultadosException
	 */
	@Override
	public RespuestaArchivoPagoZipDTO generarArchivoPago(String periodo, String codigoNegocio,
			Integer codigoTipoNegocio) throws IOException, FaltanDatosObligatoriosException,
			NoExisteDistribucionDisponible, BusquedadSinResultadosException {

		// Valida los parámetros enviados al servicio, si alguno es vacio retorna
		// excepción.
		if (periodo.equals(EMPTY) || codigoNegocio.equals(EMPTY) || periodo.equals(UNDEFINED)
				|| codigoNegocio.equals(UNDEFINED) || codigoTipoNegocio == null)
			throw new FaltanDatosObligatoriosException(PARAMETROS_VACIOS);

		// Busca un registro en estado DISTRIBUIDO, si no existe retorna una excepción.
		PagoNegocio pagoNegocio = new PagoNegocio(periodo, codigoTipoNegocio, codigoNegocio);
		String estadoDistribuido = EstadosDistribucion.DISTRIBUIDO.getValor();
		Distribucion distribucion = obtenerDistribucion(pagoNegocio, estadoDistribuido);
		if (distribucion == null)
			throw new NoExisteDistribucionDisponible(NO_EXISTE_DISTRIBUCION_DISTRIBUIDO);

		return obtenerArchivoPago(distribucion, pagoNegocio);
	}
	
	/**
	 * @throws BusquedadSinResultadosException 
	 * @throws IOException 
	 *
	 */
	@Override
	public RespuestaArchivoPagoZipDTO reprocesarPagos(ReprocesoPagosDTO reprocesarPago)
			throws FaltanDatosObligatoriosException, NoExisteDistribucionDisponible, IOException,
			BusquedadSinResultadosException {

		if (reprocesarPago == null)
			throw new FaltanDatosObligatoriosException(REPROCESAR_PAGO_VACIO);

		String periodo = reprocesarPago.getPeriodo();
		String codigoNegocio = reprocesarPago.getCodigoNegocio();
		Integer codigoTipoNegocio = reprocesarPago.getCodigoTipoNegocio();

		// Valida los parámetros enviados al servicio, si alguno es vacio retorna
		// excepción.
		if (periodo.equals(EMPTY) || codigoNegocio.equals(EMPTY) || periodo.equals(UNDEFINED)
				|| codigoNegocio.equals(UNDEFINED) || codigoTipoNegocio == null)
			throw new FaltanDatosObligatoriosException(PARAMETROS_VACIOS);

		// Busca un registro en estado DISTRIBUIDO, si no existe retorna una excepción.
		PagoNegocio pagoNegocio = new PagoNegocio(periodo, codigoTipoNegocio, codigoNegocio);
		String estadoDistribuido = EstadosDistribucion.DISTRIBUIDO.getValor();
		Distribucion distribucion = obtenerDistribucion(pagoNegocio, estadoDistribuido);
		if (distribucion == null)
			throw new NoExisteDistribucionDisponible(NO_EXISTE_DISTRIBUCION_DISTRIBUIDO);

		distribucion = reprocesarPagosBeneficiarios(distribucion, reprocesarPago);
		return obtenerArchivoPago(distribucion, pagoNegocio);
	}
	
	

	/**
	 * Busca un registro de distribución por pagoNegocio y su estado.
	 * 
	 * @author efarias
	 * @param estado
	 * @param pagoNegocio
	 * @return Distribucion
	 */
	@Override
	public List<Distribucion> buscarDistribucionPorEstado(PagoNegocio pagoNegocio, String estado) {
		String codigoNegocio = pagoNegocio.getPagoNegocioPK().getCodSfc();
		Integer tipoNegocio = pagoNegocio.getPagoNegocioPK().getTipoNegocio();
		String periodo = pagoNegocio.getPagoNegocioPK().getPeriodo();
		return repositorioDistribucion.buscarDistribucionPorEstado(codigoNegocio, tipoNegocio, periodo, estado);
	}

	/**
	 * Busca un registro de distribución por código de negocio, código tipo negocio,
	 * periodo y su estado.
	 * 
	 * @author efarias
	 * @param estado
	 * @param pagoNegocio
	 * @return Distribucion
	 */
	@Override
	public List<Distribucion> buscarDistribucionPorEstado(String codigoNegocio, Integer tipoNegocio, String periodo,
			String estado) {
		return repositorioDistribucion.buscarDistribucionPorEstado(codigoNegocio, tipoNegocio, periodo, estado);
	}

	/**
	 * Este método recibe una lista de ingresosOperacion y una lista de ingresos
	 * obligaciones y lo transforma a un DTO para su respuesta al front.
	 * 
	 * @author efarias
	 * @param listaIngresos
	 * @param listaObligaciones
	 * @param pagoNegocio
	 * @return CalculoObligacionesTipoNegocioDTO
	 * @throws NoExistenIngresosException
	 */
	public CalculoObligacionesTipoNegocioDTO calcularObligacionesPorPeriodo(List<IngresoOperacion> listaIngresos,
			List<ObligacionNegocio> listaObligaciones, PagoNegocio pagoNegocio) throws NoExistenIngresosException {

		if (listaIngresos.isEmpty())
			throw new NoExistenIngresosException(NO_EXISTEN_INGRESOS);

		CalculoObligacionesTipoNegocioDTO calculoObligaciones = new CalculoObligacionesTipoNegocioDTO();

		// Consultar Tipo Negocio para obtener el nombre
		ValorDominioNegocio valor = repositorioValorDominioNegocio.buscarValorDominioPorCodDominioYValorDominio(
				codigoDominioTipoNegocio, pagoNegocio.getPagoNegocioPK().getTipoNegocio());

		String codigoNegocio = pagoNegocio.getPagoNegocioPK().getCodSfc();
		Integer codigoTipoNegocio = pagoNegocio.getPagoNegocioPK().getTipoNegocio();
		String nombreTipoNegocio = valor.getDescripcion();
		BigDecimal totalIngresos = listaIngresos.stream().map(IngresoOperacion::getValor).reduce(BigDecimal.ZERO, BigDecimal::add);
		List<ConceptoDTO> listaIngresosDTO = servicioIngresoOperacion.obtenerListaConceptosIngresosDTO(listaIngresos);
		List<ConceptoDTO> listaObligacionesDTO = servicioDistribucionObligaciones.obtenerListaConceptosObligacionesDTO(
				listaObligaciones, totalIngresos, pagoNegocio.getPagoNegocioPK().getPeriodo());
		BigDecimal totalObligaciones = calcularTotalObligaciones(listaObligacionesDTO);
		BigDecimal totalDistribucion = totalIngresos.subtract(totalObligaciones);
		boolean sePuedeDistribuir = calcularEstadoCalculoObligacion(totalDistribucion);

		calculoObligaciones.setCodigoNegocio(codigoNegocio);
		calculoObligaciones.setSePuedeDistribuir(sePuedeDistribuir);
		calculoObligaciones.setCodigoTipoNegocio(codigoTipoNegocio);
		calculoObligaciones.setNombreTipoNegocio(nombreTipoNegocio);
		calculoObligaciones.setIngresosTotal(totalIngresos);
		calculoObligaciones.setObligacionesTotal(totalObligaciones);
		calculoObligaciones.setTotalDistribuir(totalDistribucion);
		calculoObligaciones.setListaIngresos(listaIngresosDTO);
		calculoObligaciones.setListaObligaciones(listaObligacionesDTO);
		return calculoObligaciones;
	}

	/**
	 * Este método recibe una lista ConceptoDTO el cual contiene el cálculo de
	 * obligaciones, en dado caso que la lista este vacia, retornará cero (0)
	 * 
	 * @author efarias
	 * @param listaConceptosObligacionesDTO
	 * @return BigDecimal -> total obligaciones
	 */
	public BigDecimal calcularTotalObligaciones(List<ConceptoDTO> listaConceptosObligacionesDTO) {
		if (listaConceptosObligacionesDTO.isEmpty()) {
			return BigDecimal.ZERO;
		} else {
			return listaConceptosObligacionesDTO.stream().map(ConceptoDTO::getValor).reduce(BigDecimal.ZERO,
					BigDecimal::add);
		}
	}

	/**
	 * Devuelve una bandera al front indicando si el cálculo de obligaciones esta
	 * disponible para distribuir o no.
	 * 
	 * @author efarias
	 * @param totalDistribucion
	 * @return boolean
	 */
	public boolean calcularEstadoCalculoObligacion(BigDecimal totalDistribucion) {
		return totalDistribucion.compareTo(BigDecimal.ZERO) > 0 ? Boolean.TRUE : Boolean.FALSE;
	}

	/**
	 * Devuelve el estado del cálculo de distribución de acuerdo al total de
	 * distribución calculado.
	 * 
	 * @author efarias
	 * @param totalDistribucion
	 * @return String -> Estado
	 */
	public String obtenerEstadoCalculoObligacion(BigDecimal totalDistribucion) {
		return totalDistribucion.compareTo(BigDecimal.ZERO) > 0 ? EstadosDistribucion.DISTRIBUCION_DISPONIBLE.getValor()
				: EstadosDistribucion.INGRESOS_INSUFICIENTES.getValor();
	}

	/**
	 * Este método llamada los métodos de cálculo de distribución de retenciones por
	 * tipo de negocio y cálculo de retenciones por titular guarda el registro en la
	 * tabla NEG_DISTRIBUCION.
	 * 
	 * @author efarias
	 * @param calculoObligaciones
	 * @param pagoNegocio
	 * @return Distribucion -> registro almacenado
	 */
	public Distribucion guardarDistribucion(CalculoObligacionesTipoNegocioDTO calculoObligaciones,
			PagoNegocio pagoNegocio) {

		String formatoFecha = Utilidades.formatearFecha(new Date(), "dd/MM/yyyy HH:mm:ss");
		Date fechaHora = utilidades.convertirFecha(formatoFecha, "dd/MM/yyyy HH:mm:ss");
		String estado = obtenerEstadoCalculoObligacion(calculoObligaciones.getTotalDistribuir());

		Distribucion distribucion = new Distribucion(pagoNegocio.getPagoNegocioPK(), fechaHora,
				calculoObligaciones.getIngresosTotal(), estado);

		distribucion.setTotalObligaciones(calculoObligaciones.getObligacionesTotal());
		distribucion.setTotalDistribucion(calculoObligaciones.getTotalDistribuir());
		distribucion.setTotalRetenciones(BigDecimal.ZERO);
		return repositorioDistribucion.save(distribucion);
	}

	/**
	 * Actualiza el estado de DISTRIBUCION_DISPONIBLE o DISTRIBUIDO a
	 * DISTRIBUCION_CANCELADA para todos los casos que cumplan con el criterio de
	 * búsqueda.
	 * 
	 * @author efarias
	 * @param pagoNegocio
	 */
	public void actualizarEstadoDistribucion(PagoNegocio pagoNegocio, String estado) {
		String estadoDistribucionCancelada = EstadosDistribucion.DISTRIBUCION_CANCELADA.getValor();
		List<Distribucion> listDistribucion = buscarDistribucionPorEstado(pagoNegocio, estado);
		if (!listDistribucion.isEmpty()) {
			listDistribucion.forEach(distribucion -> {
				distribucion.setEstado(estadoDistribucionCancelada);
				repositorioDistribucion.save(distribucion);
			});
		}
	}
	
	/**
	 * Actualiza el estado de distribución exitosa del registro de la entidad Pago
	 * Negocio.
	 * 
	 * @author efarias
	 * @param pagoNegocio
	 * @param estado
	 */
	public void actualizarEstadoDistribucionPagoNegocio(PagoNegocio pagoNegocio, String estado) {
		pagoNegocio.setDistribucionExitosa(estado);
		servicioPagoNegocio.guardarPagoNegocio(pagoNegocio);
	}

	/**
	 * Este método obtiene una lista de las obligaciones financieras de un tipo de
	 * negocio para un periodo especificado.
	 * 
	 * @author efarias
	 * @param codigoTipoNegocio
	 * @param codigoNegocio
	 * @param periodo
	 * @return List<ObligacionNegocio>
	 */
	public List<ObligacionNegocio> obtenerListaObligacionesPorPeriodo(Integer codigoTipoNegocio, String codigoNegocio,
			String periodo) {
		TipoNegocio tipoNegocio = new TipoNegocio(codigoTipoNegocio, codigoNegocio);
		List<ObligacionNegocio> listaObligacionesTipoNegocio = servicioObligacionNegocio
				.buscarObligacionesTipoNegocio(tipoNegocio);
		return utilidades.filtrarObligacionesPorPeriodo(listaObligacionesTipoNegocio, periodo);
	}

	/**
	 * Guarda el detalle de la distribución de obligaciones.
	 * 
	 * @author efarias
	 * @param distribucion
	 * @param listaObligaciones
	 */
	public void guardarDistribucionObligaciones(Distribucion distribucion, List<ObligacionNegocio> listaObligaciones,
			BigDecimal totalIngresos, String periodo) {
		servicioDistribucionObligaciones.guardarDistribucionObligaciones(distribucion, listaObligaciones, totalIngresos,
				periodo);
	}
	
	/**
	 * Este método busca un registro en la tabla distribución con un estado
	 * especificado y un PagoNegocio que servirá de llave de búsqueda, en caso de
	 * encontrar coincidencias retornará el objeto, de lo contrario retornará null.
	 * 
	 * @author efarias
	 * @param pagoNegocio
	 * @param estado
	 * @return Distribucion o Null
	 */
	public Distribucion obtenerDistribucion(PagoNegocio pagoNegocio, String estado) {
		List<Distribucion> listaDistribucion = buscarDistribucionPorEstado(pagoNegocio, estado);
		if (!listaDistribucion.isEmpty()) {
			return listaDistribucion.get(0);
		} else {
			return null;
		}
	}

	@Override
	public CalculoObligacionesTipoNegocioDTO obtieneIngresosEgresos(String periodo, String codigoNegocio,
			String codigoTipoNegocio) throws FaltanDatosObligatoriosException, NoExistenIngresosException {
		if (periodo.equals(EMPTY) || codigoNegocio.equals(EMPTY) || periodo.equals(UNDEFINED)
				|| codigoNegocio.equals(UNDEFINED) || codigoTipoNegocio.equals(EMPTY))
			throw new FaltanDatosObligatoriosException(PARAMETROS_VACIOS);
		
		Integer idTipoNegocio = servicioDominio.buscarPorCodigoDominioYValor("TIPNEG", codigoTipoNegocio).getIdValorDominio();
		// Actualización estado.
		PagoNegocio pagoNegocio = new PagoNegocio(periodo, idTipoNegocio, codigoNegocio);
		
		// Obtener Detalles ingreso y obligaciones
		List<IngresoOperacion> listaIngresos = servicioIngresoOperacion.buscarIngresosOperacion(pagoNegocio);
		List<ObligacionNegocio> listaObligaciones = obtenerListaObligacionesPorPeriodo(idTipoNegocio, codigoNegocio,periodo);

		// Calcular Obligaciones
		return calcularObligacionesPorPeriodo(listaIngresos,listaObligaciones, pagoNegocio);
	}
	
	/**
	 * Este método construye el objeto de respuesta del servicio de generación
	 * archivo de pago.
	 * 
	 * @author efarias
	 * @param distribucion
	 * @param pagoNegocio
	 * @return RespuestaArchivoPagoZipDTO -> DTO Respuesta
	 * @throws IOException
	 * @throws BusquedadSinResultadosException
	 */
	public RespuestaArchivoPagoZipDTO obtenerArchivoPago(Distribucion distribucion, PagoNegocio pagoNegocio)
			throws IOException, BusquedadSinResultadosException {

		List<DistribucionBeneficiario> listaGeneralDistribucion = distribucion.getDistribucionBeneficiarios();
		if (listaGeneralDistribucion.isEmpty())
			throw new BusquedadSinResultadosException(NO_EXISTEN_DIST_BENEFICIARIO);

		// Filtra los registros no vacios
		List<DistribucionBeneficiario> listaTotalDistribucion = listaGeneralDistribucion.stream()
				.filter(dist -> dist.getDominioCuenta() != null).collect(Collectors.toList());
		if (listaTotalDistribucion.isEmpty())
			throw new BusquedadSinResultadosException(PAGOS_SIN_MEDIO_PAGO);

		// Consulta todos los parámetros COSMEDPAG activos.
		List<ValorDominioNegocio> listaDominioCosMedPag = repositorioValorDominioNegocio
				.buscarPorCodigoDominio(codigoDominioCosMedPag);

		// retorna una excepción si no existe al menos un registro en BD.
		if (listaDominioCosMedPag.isEmpty())
			throw new BusquedadSinResultadosException(NO_EXISTE_REGISTROS_COSMEDPAG);
		
		// Inicialización variables
		RespuestaArchivoPagoZipDTO respuestaArchivo = new RespuestaArchivoPagoZipDTO();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zipOut = new ZipOutputStream(baos);
		
		// Por cada costo de medio de pago se creará un archivo y se adicionará al ZIP
		listaDominioCosMedPag.forEach(dominio -> {
			
			List<DistribucionBeneficiario> listaFiltradaBeneficiarios = listaTotalDistribucion.stream()
					.filter(distBeneficierio -> distBeneficierio.getDominioCuenta().getRelacionEntreDominiosList1()
							.get(0).getValorDominioNegocio().getDescripcion().equals(dominio.getDescripcion()))
					.collect(Collectors.toList());
			
			if (!listaFiltradaBeneficiarios.isEmpty()) {
				
				String nombreArchivo = obtenerNombreTXT(pagoNegocio, dominio.getDescripcion());
				
				try {
					escribirArchivoZIP(nombreArchivo, listaFiltradaBeneficiarios, zipOut, distribucion);
				} catch (IOException e) {
					log.log(Level.SEVERE, ERROR_ESCRIBIR_ZIP, e);
				} catch (ParseException e) {
					log.log(Level.SEVERE, ERROR_OBTENER_PERIODO, e);
				}
				
			} else {
				log.info(dominio.getDescripcion() + SIN_REGISTROS + listaFiltradaBeneficiarios.size());
			}
		});

		zipOut.close();
		String archivoCodificado = Base64.getEncoder().encodeToString(baos.toByteArray());
		respuestaArchivo.setRecursoB64(archivoCodificado);
		respuestaArchivo.setNombreArchivoZip(obtenerNombreZIP(pagoNegocio));
		respuestaArchivo.setExtension(EXTENSION_ZIP);
		respuestaArchivo.setTipoArchivo(CONTENT_TYPE);
		return respuestaArchivo;
	}
	
	/**
	 * Este método crea el fichero correspondiente, y lo asocia al archivo .ZIP
	 * 
	 * @author efarias
	 * @param nombreArchivo
	 * @param listaFiltradaBeneficiarios
	 * @param zipOut
	 * @param distribucion
	 * @throws IOException
	 * @throws ParseException
	 */
	public void escribirArchivoZIP(String nombreArchivo, List<DistribucionBeneficiario> listaFiltradaBeneficiarios,
			ZipOutputStream zipOut, Distribucion distribucion) throws IOException, ParseException {
		
		Integer contador = 1;
		zipOut.putNextEntry(new ZipEntry(nombreArchivo));
	    PrintWriter writer = new PrintWriter(zipOut);
	    
	    // Se inserta el contenido del archivo.
	    for (DistribucionBeneficiario distribucionBeneficiario : listaFiltradaBeneficiarios) {
	    	writer.println(crearRegistroFicheroTXT(contador, distribucionBeneficiario, distribucion));
	    	contador += 1;
		}
	    writer.flush();
	    zipOut.closeEntry();
	}
	
	/**
	 * Este método construye el registro de pago que se asociará al archivo de pago
	 * correspondiente.
	 * 
	 * @author efarias
	 * @param contador
	 * @param distribucionBeneficiario
	 * @param distribucion
	 * @return String -> Registro pago
	 * @throws ParseException
	 */
	public String crearRegistroFicheroTXT(Integer contador, DistribucionBeneficiario distribucionBeneficiario,
			Distribucion distribucion) throws ParseException {

		StringJoiner sj = new StringJoiner(DELIMITADOR);

		sj.add(Integer.toString(contador));
		sj.add(distribucion.getNegocio().getContastantesPago().getTipoFideicomiso());
		sj.add(distribucion.getNegocio().getContastantesPago().getTipoSubtipoFideicomiso());
		sj.add(distribucion.getNegocio().getCodSfc());
		sj.add(ETAPA_FIDEICOMISO);
		sj.add(Utilidades.formatearFecha(new Date(), formatoddmmyyyy));
		sj.add(utilidades.redondearBigDecimal(distribucionBeneficiario.getValorMenosDescuento()).toString());
		sj.add(distribucion.getNegocio().getContastantesPago().getMovimiento());
		sj.add(distribucionBeneficiario.getDistribucionBeneficiarioPK().getNumeroDocumentoTitular());
		sj.add(distribucionBeneficiario.getDistribucionBeneficiarioPK().getTipoDocumentoTitular());
		sj.add(distribucionBeneficiario.getBeneficiario().getTitular().getPersona().getNombreCompleto());
		sj.add(Integer.toString(contador));
		sj.add(EMPTY);
		sj.add(EMPTY);
		sj.add(EMPTY);
		sj.add(obtenerObservacionRegistroTXT(distribucion));
		sj.add(obtenerDestinoPagoTXT(distribucionBeneficiario));
		sj.add(obtenerCuentaBancariaTXT(distribucionBeneficiario));
		sj.add(obtenerCodigoBancoTXT(distribucionBeneficiario));
		sj.add(distribucion.getNegocio().getContastantesPago().getCodigoPais());
		sj.add(distribucion.getNegocio().getContastantesPago().getCodigoDepartamento());
		sj.add(distribucion.getNegocio().getContastantesPago().getCodigoCiudad());
		sj.add(distribucionBeneficiario.getDominioCuenta().getValor());
		sj.add(distribucion.getNegocio().getContastantesPago().getTipoProceso());
		sj.add(distribucion.getPagoNegocio().getEncargo().getEncargoPK().getCuenta());
		sj.add(distribucion.getPagoNegocio().getEncargo().getMovimientoFondo());
		sj.add(distribucion.getPagoNegocio().getEncargo().getNumeroPagoOrdinario());
		sj.add(distribucionBeneficiario.getDistribucionBeneficiarioPK().getTipoDocumentoBeneficiario());
		sj.add(distribucionBeneficiario.getDistribucionBeneficiarioPK().getNumeroDocumentoBeneficiario());
		sj.add(distribucion.getNegocio().getContastantesPago().getTipoMovNegocio());
		return sj.toString();
	}
	
	/**
	 * Este método construye el nombre del archivo txt.
	 * 
	 * @author efarias
	 * @param pagoNegocio
	 * @param dominio
	 * @return String -> Nombre del archivo .txt
	 */
	public String obtenerNombreTXT(PagoNegocio pagoNegocio, String dominio) {

		String periodo = pagoNegocio.getPagoNegocioPK().getPeriodo().replace(SLASH, EMPTY);
		String codigoNegocio = pagoNegocio.getPagoNegocioPK().getCodSfc();
		String codigoTipoNegocio = Integer.toString(pagoNegocio.getPagoNegocioPK().getTipoNegocio());
		StringBuilder sb = new StringBuilder();
		sb.append(codigoNegocio).append(UNDER_LINE).append(codigoTipoNegocio).append(UNDER_LINE).append(periodo)
				.append(UNDER_LINE).append(dominio).append(EXTENSION_TXT);
		return sb.toString();
	}

	/**
	 * Este método construye el nombre del archivo ZIP.
	 * 
	 * @author efarias
	 * @param pagoNegocio
	 * @param dominio
	 * @return String -> Nombre del archivo .ZIP
	 */
	public String obtenerNombreZIP(PagoNegocio pagoNegocio) {

		String periodo = pagoNegocio.getPagoNegocioPK().getPeriodo().replace(SLASH, EMPTY);
		String codigoNegocio = pagoNegocio.getPagoNegocioPK().getCodSfc();
		String codigoTipoNegocio = Integer.toString(pagoNegocio.getPagoNegocioPK().getTipoNegocio());
		StringBuilder sb = new StringBuilder();
		sb.append(codigoNegocio).append(UNDER_LINE).append(codigoTipoNegocio).append(UNDER_LINE).append(periodo)
				.append(EXTENSION_ZIP);
		return sb.toString();
	}

	/**
	 * Este método construye el mensaje de observación del registro basado en los
	 * datos del código del negocio, código tipo de negocio, el periodo y los
	 * diversos números de radicados asociados al pago.
	 * 
	 * @author efarias
	 * @param distribucion
	 * @return String -> Observación
	 * @throws ParseException
	 */
	public String obtenerObservacionRegistroTXT(Distribucion distribucion) throws ParseException {

		StringBuilder sb = new StringBuilder();
		sb.append(utilidades.obtenerCodigoInterno(distribucion.getNegocio().getNombre())).append(" ORF ").append(obtenerRadicadosPagoTXT(distribucion))
				.append(obtenerPeriodoPagoTXT(distribucion.getDistribucionPK().getPeriodo()));
		return sb.toString();
	}

	/**
	 * Este método obtiene todos los radicados de pago activos asociados a la
	 * distribución.
	 * 
	 * @author efarias
	 * @param distribucion
	 * @return String -> radicados concatenados
	 */
	public String obtenerRadicadosPagoTXT(Distribucion distribucion) {

		List<IngresoOperacion> listaIngresos = distribucion.getPagoNegocio().getIngresosOperacion();
		List<IngresoOperacion> listaIngresosActivos = listaIngresos.stream()
				.filter(ingreso -> ingreso.getEstado().equals(ACTIVO)).collect(Collectors.toList());

		if (listaIngresosActivos.size() == 1) {
			return listaIngresosActivos.get(0).getIngresoOperacionPK().getRadicado();
		} else {
			StringJoiner sj = new StringJoiner(DELIMITADOR_RADICADOS);
			for (IngresoOperacion ingreso : listaIngresosActivos) {
				sj.add(ingreso.getIngresoOperacionPK().getRadicado());
			}
			return sj.toString();
		}
	}

	/**
	 * Este método obtiene el periodo de pago en formato MMMM yyyy ej: FEBRERO 2025
	 * 
	 * @author efarias
	 * @param periodo
	 * @return
	 * @throws ParseException
	 */
	public String obtenerPeriodoPagoTXT(String periodo) throws ParseException {
		Locale spanish = new Locale("es", "ES");
		SimpleDateFormat sdf = new SimpleDateFormat(formatoddmmyyyy, spanish);
		Date date = sdf.parse(periodo);
		sdf.applyPattern(FORMATO_PERIODO_OBS);
		return sdf.format(date).toUpperCase();
	}

	/**
	 * Método que devuelve el destino de pago de acuerdo al valor id de dominio
	 * asociado al tipo de cuenta del beneficiario.
	 * 
	 * @author efarias
	 * @param distribucionBeneficiario
	 * @return -> Destino Pago
	 */
	public String obtenerDestinoPagoTXT(DistribucionBeneficiario distribucionBeneficiario) {

		Integer valor = distribucionBeneficiario.getDominioCuenta().getRelacionEntreDominiosList1().get(0)
				.getValorDominioNegocio().getIdValorDominio();
		String destinoPago = EMPTY;

		if (valor >= 1 && valor <= 3) {
			destinoPago = "00" + Integer.toString(valor);
		} else if (valor >= 10) {
			destinoPago = Integer.toString(valor);
		}
		return destinoPago;
	}

	/**
	 * Método que devuelve el número de cuenta asociado al beneficiario, en caso de
	 * ser algún tipo de cheque, devolverá el respectivo tipo de cuenta como
	 * respuesta.
	 * 
	 * @author efarias
	 * @param distribucionBeneficiario
	 * @return String -> cuenta bancaria
	 */
	public String obtenerCuentaBancariaTXT(DistribucionBeneficiario distribucionBeneficiario) {
		String tipoCuenta = distribucionBeneficiario.getDominioCuenta().getRelacionEntreDominiosList1().get(0)
				.getValorDominioNegocio().getDescripcion();
		return tipoCuenta.contains(TIPO_PAGO_CHEQUE) ? tipoCuenta
				: distribucionBeneficiario.getDistribucionBeneficiarioPK().getCuenta();
	}

	/**
	 * Esté metodo valida el tipo cuenta si pertenece a una transferencia, y retorna
	 * el código de banco asociado al registro de distribución del beneficiario, de
	 * lo contrario retornará el código genérico especificado.
	 * 
	 * @author efarias
	 * @param distribucionBeneficiario
	 * @return String -> código banco
	 */
	public String obtenerCodigoBancoTXT(DistribucionBeneficiario distribucionBeneficiario) {

		String tipoCuenta = distribucionBeneficiario.getDominioCuenta().getRelacionEntreDominiosList1().get(0)
				.getValorDominioNegocio().getDescripcion();
		String codigoBanco;

		if (tipoCuenta.contains(TIPO_PAGO_TRANSFERENCIA)) {
			codigoBanco = distribucionBeneficiario.getDominioBanco().getValor();
		} else {
			codigoBanco = COD_BANCO_NO_TRASNFERENCIA;
		}
		return codigoBanco;
	}
	
	/**
	 * Este método cálcula las las retenciones por titular. Posteriormente, 
	 * actualiza el valor total de retenciones para la distribución.
	 * 
	 * @author efarias
	 * @param pagoNegocio
	 * @param distribucion
	 */
	public void calcularRetenciones(PagoNegocio pagoNegocio, Distribucion distribucion) {
		distribucion.setTotalRetenciones(calcularRetencionTitularesPorTipoNegocio(
				pagoNegocio.getTipoNegocio(), distribucion));
		repositorioDistribucion.save(distribucion);
	}
	
	/**
	 * Este método cálcula el total de distribución de retenciones y obtiene el
	 * valor total. Posteriormente, guarda el detalle de distribución de retenciones
	 * 
	 * @author efarias
	 * @param tipoNegocio
	 * @param distribucion
	 * @return BigDecimal -> total distribución retenciones por tipo negocio
	 */
	public BigDecimal calcularRetencionTipoNegocio(TipoNegocio tipoNegocio, Distribucion distribucion) {
		List<DistribucionRetenciones> listaDistribucionRetenciones = servicioDistribucionRetenciones
				.calcularDistribucionRetenciones(tipoNegocio, distribucion);
		if (!listaDistribucionRetenciones.isEmpty()) {
			servicioDistribucionRetenciones.guardarListaDistribucionretenciones(listaDistribucionRetenciones);
			return listaDistribucionRetenciones.stream().map(DistribucionRetenciones::getValorImpuesto)
					.reduce(BigDecimal.ZERO, BigDecimal::add);
		} else {
			return BigDecimal.ZERO;
		}
	}
	
	/**
	 * Este método cálcula el total de distribución de retenciones de todos los
	 * titulares asociados al negocio enviado por parametro y obtiene el valor
	 * total. Posteriormente, guarda el detalle de distribución de retenciones de
	 * titulares
	 * 
	 * @author efarias
	 * @param negocio
	 * @param distribucion
	 * @return BigDecimal -> total distribución retenciones titulares
	 */
	public BigDecimal calcularRetencionTitularesPorTipoNegocio(TipoNegocio tipoNegocio, Distribucion distribucion) {
		List<DistribucionRetencionesTitular> listaRetencionTitulares = servicioDistribucionRetencionesTitular
				.calcularRetencionTitularesPorTipoNegocio(tipoNegocio, distribucion);
		if (!listaRetencionTitulares.isEmpty()) {
			servicioDistribucionRetencionesTitular.guardarListaDistribucionRetencionesTitular(listaRetencionTitulares);
			return listaRetencionTitulares.stream().map(DistribucionRetencionesTitular::getValorImpuesto)
					.reduce(BigDecimal.ZERO, BigDecimal::add);
		} else {
			return BigDecimal.ZERO;
		}
	}
	
	/**
	 * Este método realiza el reprocesos de los pagos de los beneficiarios y retorna
	 * el objeto de Distribución con la lista de DistribucionBeneficiario la cual
	 * fue modificada
	 * 
	 * @author efarias
	 * @param distribucion
	 * @param reprocesarPago
	 * @return Distribucion con la lista de distribución de beneficiarios
	 *         reprocesados
	 * @throws BusquedadSinResultadosException
	 */
	public Distribucion reprocesarPagosBeneficiarios(Distribucion distribucion, ReprocesoPagosDTO reprocesarPago)
			throws BusquedadSinResultadosException {

		// Filtra la lista de disrtibución de beneficiarios los cuales serán modificados
		List<DistribucionBeneficiario> listaFiltradaReproceso = filtrarListaBeneficiariosAReprocesar(distribucion,
				reprocesarPago);

		List<DistribucionBeneficiario> nuevaListaDistribucion = new ArrayList<>();

		if (listaFiltradaReproceso.isEmpty())
			throw new BusquedadSinResultadosException(NO_EXISTE_PAGOS_A_REPROCESAR);

		for (DistribucionBeneficiario distribucionBeneficiario : listaFiltradaReproceso) {
			ReprocesoPagoBeneficiarioDTO datosBeneficiario = filtrarPagoReprocesado(distribucionBeneficiario,
					reprocesarPago);

			if (datosBeneficiario != null) {

				// Se crea el nuevo medio de pago
				MedioPago medioPago = servicioMedioPago.verificarCrearMedioPago(datosBeneficiario.getDatoNuevo());

				// Se crea un nuevo medio de pago persona
				MedioPagoPersona medioPagoPersona = servicioMedioPagoPersona
						.verificarCrearMedioPagoPersona(datosBeneficiario, medioPago);

				// Se crea un nuevo beneficiario nuevo con su nuevo medio de pago.
				Beneficiario beneficiario = servicioBeneficiario.verificarCrearBeneficiario(distribucionBeneficiario,
						medioPagoPersona);

				// Se crea un nuevo registro de distribución de beneficirio.
				DistribucionBeneficiario distribucionBeneficiarioModificado = servicioDistribucionBeneficiario
						.verificarModificarDistribucionBeneficiario(distribucionBeneficiario, beneficiario);

				// Se agrega la información asociada a la distribución del beneficiario.
				DistribucionBeneficiario nuevaDistribucionBen = servicioDistribucionBeneficiario
						.agregarInformacionDistribucionBeneficiario(distribucionBeneficiarioModificado,
								distribucionBeneficiario, beneficiario);

				// se agrega la nueva distribucion
				nuevaListaDistribucion.add(nuevaDistribucionBen);
			}
		}
		return clonarDistribucion(distribucion, nuevaListaDistribucion);
	}

	/**
	 * Este método realiza la clonación de la distribución original y actualiza la
	 * lista de DistribucionBeneficiarios a los cuales se les hizo reproceso de
	 * pagos.
	 * 
	 * @author efarias
	 * @param distribucion
	 * @param nuevaListaDistribucion
	 * @return Distribucion clonada
	 */
	public Distribucion clonarDistribucion(Distribucion distribucion,
			List<DistribucionBeneficiario> nuevaListaDistribucion) {

		Distribucion clonDistribucion = new Distribucion(distribucion.getDistribucionPK());
		if (!nuevaListaDistribucion.isEmpty()) {
			clonDistribucion.setDistribucionBeneficiarios(nuevaListaDistribucion);
		} else {
			clonDistribucion.setDistribucionBeneficiarios(new ArrayList<DistribucionBeneficiario>());
		}
		clonDistribucion.setDistribucionObligaciones(distribucion.getDistribucionObligaciones());
		clonDistribucion.setDistribucionRetenciones(distribucion.getDistribucionRetenciones());
		clonDistribucion.setDistribucionRetencionesTitular(distribucion.getDistribucionRetencionesTitular());
		clonDistribucion.setEstado(distribucion.getEstado());
		clonDistribucion.setLoginAuditoria(distribucion.getLoginAuditoria());
		clonDistribucion.setNegocio(distribucion.getNegocio());
		clonDistribucion.setPagoNegocio(distribucion.getPagoNegocio());
		clonDistribucion.setTotal(distribucion.getTotal());
		clonDistribucion.setTotalDistribucion(distribucion.getTotalDistribucion());
		clonDistribucion.setTotalObligaciones(distribucion.getTotalObligaciones());
		clonDistribucion.setTotalRetenciones(distribucion.getTotalRetenciones());
		return clonDistribucion;
	}

	/**
	 * Este método filtra la lista de distribución de beneficiarios en búsqueda de
	 * aquellos a ser modificados de acuerdo a la lista de pagos reprocesados
	 * enviados desde el front.
	 * 
	 * @author efarias
	 * @param listaDistribucionBeneficiarios
	 * @param listaReprocesoBeneficiarios
	 * @return List<DistribucionBeneficiario> - Lista filtrada para reprocesar
	 */
	public List<DistribucionBeneficiario> filtrarListaBeneficiariosAReprocesar(Distribucion distribucion,
			ReprocesoPagosDTO reprocesarPago) {

		List<DistribucionBeneficiario> listaDistribucionBeneficiarios = distribucion.getDistribucionBeneficiarios();
		List<ReprocesoPagoBeneficiarioDTO> listaReprocesoBeneficiarios = reprocesarPago
				.getListaReprocesoBeneficiarios();

		return listaDistribucionBeneficiarios.stream().filter(distribucionBeneficiario -> listaReprocesoBeneficiarios
				.stream()
				.anyMatch(reprocespPagoBeneficiario -> reprocespPagoBeneficiario.getTipoDocumentoTitular()
						.equals(distribucionBeneficiario.getDistribucionBeneficiarioPK().getTipoDocumentoTitular())
						&& reprocespPagoBeneficiario.getNumeroDocumentoTitular().equals(
								distribucionBeneficiario.getDistribucionBeneficiarioPK().getNumeroDocumentoTitular())
						&& reprocespPagoBeneficiario.getTipoDocumentoBeneficiario().equals(
								distribucionBeneficiario.getDistribucionBeneficiarioPK().getTipoDocumentoBeneficiario())
						&& reprocespPagoBeneficiario.getNumeroDocumentoBeneficiario().equals(distribucionBeneficiario
								.getDistribucionBeneficiarioPK().getNumeroDocumentoBeneficiario())))
				.collect(Collectors.toList());
	}

	/**
	 * Este método filtra y obtiene los datos del beneficiario los cuales serán
	 * procesados para modificar los datos de medio de pago.
	 * 
	 * @author efarias
	 * @param distribucionBeneficiario
	 * @param reprocesarPago
	 * @return ReprocesoPagoBeneficiarioDTO -> objeto con los datos reprocesar
	 */
	public ReprocesoPagoBeneficiarioDTO filtrarPagoReprocesado(DistribucionBeneficiario distribucionBeneficiario,
			ReprocesoPagosDTO reprocesarPago) {

		return reprocesarPago.getListaReprocesoBeneficiarios().stream()
				.filter(reproceso -> reproceso.getTipoDocumentoTitular()
						.equals(distribucionBeneficiario.getDistribucionBeneficiarioPK().getTipoDocumentoTitular()))
				.filter(reproceso -> reproceso.getNumeroDocumentoTitular()
						.equals(distribucionBeneficiario.getDistribucionBeneficiarioPK().getNumeroDocumentoTitular()))
				.filter(reproceso -> reproceso.getTipoDocumentoBeneficiario().equals(
						distribucionBeneficiario.getDistribucionBeneficiarioPK().getTipoDocumentoBeneficiario()))
				.filter(reproceso -> reproceso.getNumeroDocumentoBeneficiario().equals(
						distribucionBeneficiario.getDistribucionBeneficiarioPK().getNumeroDocumentoBeneficiario()))
				.filter(reproceso -> reproceso.getDatoAnterior().getNumeroCuenta()
						.equals(distribucionBeneficiario.getDistribucionBeneficiarioPK().getCuenta()))
				.filter(reproceso -> reproceso.getDatoAnterior().getCodigoBancario()
						.equals(distribucionBeneficiario.getDistribucionBeneficiarioPK().getBanco()))
				.filter(reproceso -> reproceso.getDatoAnterior().getTipoCuenta()
						.equals(distribucionBeneficiario.getDistribucionBeneficiarioPK().getTipoCuenta()))
				.findAny().orElse(null);
	}
	
	private void agregarRetencionInvisibleATitulares(String codigoNegocio) {
		List<Titular> titulares = servicioTitular.buscarTitularesPorCodigoNegocio(codigoNegocio);
		ValorDominioNegocio valorRetencionInvisible = servicioDominio
				.buscarPorCodigoDominioYDescripcion(codigoDominioRetencion, retencionInvisible, Boolean.FALSE);
		titulares.forEach(titular -> {
			if (titular.getRetencionTitularList().stream().noneMatch(
					retencionTitular -> retencionTitular.getActivo() && retencionTitular.getRetencionTitularPK()
							.getIdRetencion().equals(valorRetencionInvisible.getIdValorDominio()))) {
				RetencionTitular retencionTitular = new RetencionTitular(valorRetencionInvisible.getIdValorDominio(),
						codigoNegocio, titular.getTitularPK().getNumeroDocumento(),
						titular.getTitularPK().getTipoDocumento(), titular.getIdTipoNaturalezaJuridica());
				retencionTitular.setActivo(Boolean.TRUE);
				retencionTitular.setTitular(titular);
				retencionTitular.setDominioRetencion(valorRetencionInvisible);
				servicioRetencionTitular.guardarRetencionTitular(retencionTitular);
			}
		});
	}
			
}
