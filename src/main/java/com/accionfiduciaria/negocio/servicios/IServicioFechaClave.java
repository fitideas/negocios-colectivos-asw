package com.accionfiduciaria.negocio.servicios;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.Negocio;

public interface IServicioFechaClave {

	@Autowired
	List<FechaClave> buscarFechasClaveNegocio(Negocio negocio);

	@Autowired
	FechaClave guardarFechaClave(FechaClave fechaActual);
	
	@Autowired
	FechaClave guardarFechaClave(Date fecha, String codigoNegocio, boolean activo);

	@Autowired
	List<FechaClave> buscarPorIdFechaClave(Integer idEliminar);

	@Autowired
	List<FechaClave> buscarProximasFechasClaves(Date fechaActual);

}
