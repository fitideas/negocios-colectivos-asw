/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionRetenciones;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

/**
 * @author efarias
 *
 */
public interface IServicioDistribucionRetenciones {
	
	@Autowired
	public List<DistribucionRetenciones> calcularDistribucionRetenciones(TipoNegocio tipoNegocio, Distribucion distribucion);
	
	@Autowired
	public void guardarListaDistribucionretenciones(List<DistribucionRetenciones> listaDistribucionRetenciones);

}
