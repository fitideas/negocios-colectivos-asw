package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.excepciones.ErrorAlConectarAccionException;
import com.accionfiduciaria.excepciones.PorcentajeGiroBeneficiariosNoValidoException;
import com.accionfiduciaria.excepciones.TitularSinBeneficiariosException;
import com.accionfiduciaria.modelo.dtos.BeneficiarioPagosDTO;
import com.accionfiduciaria.modelo.dtos.InformacionFinancieraEnvioDTO;
import com.accionfiduciaria.modelo.dtos.InformacionFinancieraRecepcionDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.PeticionConsultaHistoricoPagos;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoCambiosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaHistoricoCesionesCambiosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaPagosDTO;
import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersona;
import com.accionfiduciaria.modelo.entidad.Titular;

public interface IServicioBeneficiario {

	@Autowired
	public List<Beneficiario> buscarBeneficiariosTitular(Titular titular, Boolean estado);

	@Autowired
	public Beneficiario guardarBeneficiario(Beneficiario beneficiario);

	@Autowired
	public InformacionFinancieraEnvioDTO obtenerInformacionFinanciera(String tipoDocumentoTitular,
			String documentoTitular, String codigoNegocio, String token);

	@Autowired
	public void guardarInformacionFinanciera(InformacionFinancieraRecepcionDTO informacionFinanciera)
			throws TitularSinBeneficiariosException, PorcentajeGiroBeneficiariosNoValidoException;

	@Autowired
	RespuestaPaginadaConsultaPagosDTO listaPagos(PeticionConsultaHistoricoPagos peticion);

	@Autowired
	RespuestaArchivoDTO obtenerArchivoHistorialPagos(PeticionConsultaHistoricoPagos peticion) throws Exception;

	@Autowired
	public List<Beneficiario> buscarBeneficiariosPorNegocio(String codigoNegocio);

	BeneficiarioPagosDTO procesarHistorialPagos(DistribucionBeneficiario pago);

	@Autowired
	public Beneficiario verificarCrearBeneficiario(DistribucionBeneficiario distribucionBeneficiario,
			MedioPagoPersona medioPagoPersona);

	
	@Autowired
	RespuestaHistoricoCambiosDTO listaBeneficiarioHistorialCambios(ParametrosFiltroDTO peticion,String tokenAutorization);
	@Autowired
	RespuestaHistoricoCambiosDTO listaBeneficiarioHistorialSolicitudCambios(ParametrosFiltroDTO peticion);
	@Autowired
	RespuestaHistoricoCesionesCambiosDTO listaBeneficiarioHistorialCesionesCambios(ParametrosFiltroDTO peticion, String tokenAutorizacion) throws ErrorAlConectarAccionException;
	
}
