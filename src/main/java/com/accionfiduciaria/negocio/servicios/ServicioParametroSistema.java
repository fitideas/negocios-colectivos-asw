/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.excepciones.ParametroNoEncontradoException;
import com.accionfiduciaria.modelo.entidad.ParametroSistema;
import com.accionfiduciaria.negocio.repositorios.RepositorioParametroSistema;

/**
 * @author José Santiago Polo Acosta - 11/12/2019 - jpolo@asesoftware.com
 *
 */
@Service
@PropertySource("classpath:mensajes.properties")
public class ServicioParametroSistema implements IServicioParametroSistema {
	
	@Autowired
	RepositorioParametroSistema repositorioParametroSistema;
	
	@Value("${parametro.no.encontrado}")
	private String mensajeParametroNoEncontrado;
	
	@Value("${param.id.cron}")
	private Integer cron;
	
	@Override
	public String obtenerValorParametro(Integer idParametro) throws ParametroNoEncontradoException {
		
		ParametroSistema parametro = repositorioParametroSistema.findById(idParametro).orElse(null);
		if(parametro == null) {
			throw new ParametroNoEncontradoException(mensajeParametroNoEncontrado + " " + idParametro);
		}
		return parametro.getValor();
	}
	
	@Bean
	@Override
	public String obtenerValorCron() {
		ParametroSistema parametro = repositorioParametroSistema.findById(cron).orElse(null);
		if(parametro == null) {
			throw new ParametroNoEncontradoException(mensajeParametroNoEncontrado + " " + cron);
		}
		return parametro.getValor();
	}
	
	
}
