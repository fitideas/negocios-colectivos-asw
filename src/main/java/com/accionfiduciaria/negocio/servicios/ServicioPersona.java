package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.PersonaPK;
import com.accionfiduciaria.negocio.repositorios.RepositorioPersona;

/**
 * Case que implementa la interfaz {@link IServicioPersona}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
public class ServicioPersona implements IServicioPersona {

	@Autowired
	RepositorioPersona repositorioPersona;
	
	@Override
	public Persona guardarPersona(Persona persona) {
		return repositorioPersona.save(persona);
	}
	
	@Override
	public Iterable<Persona> guardarPersonas(List<Persona> personas) {
		return repositorioPersona.saveAll(personas);
	}

	@Override
	public Persona buscarPersona(String tipoDocumento, String documento) {
		return repositorioPersona.findById(new PersonaPK(tipoDocumento, documento)).orElse(null);
	}
}
