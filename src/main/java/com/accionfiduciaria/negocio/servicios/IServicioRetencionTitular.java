package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.Titular;

/**
 * Clase para el manejo de las retenciones asignadas a un titular.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface IServicioRetencionTitular {

	/**
	 * Busca las retenciones asignadas a un titular.
	 * 
	 * @param titular El titular al que se le consultaran las retenciones.
	 * @return Los datos de las retenciones encontradas.
	 */
	@Autowired
	public List<RetencionTitular> buscarRetencionesTitular(Titular titular);

	/**
	 * Guarda las retenciones de un titular.
	 * @param retencionesTitular El listado de retenciones a guardar.
	 */
	@Autowired
	public void guardarRetencionesTitular(List<RetencionTitular> retencionesTitular);

	/**
	 * Guarda una retención de un titular.
	 * 
	 * @param retencionTitular La entidad a guardar.
	 * @return La entidad guardada.
	 */
	@Autowired
	public RetencionTitular guardarRetencionTitular(RetencionTitular retencionTitular);

	@Autowired
	public List<RetencionTitular> buscarRetencionesTitularPorTipoNaturalezaNegocio(Integer idTipoNaturalezaNegocio);

	@Autowired
	public List<RetencionTitular> buscarRetencionesTitularPorIdRetencion(Integer idRetencion);

	@Autowired
	public List<RetencionTitular> buscarRetencionesTitularPorCodigoNegocio(String codigoSfc);

	@Autowired
	public List<RetencionTitular> buscarRetencionesTitularPorTipoNaturalezaNegocioYTitular(int tipoNaturalezanegocio,
			Titular titular);

	@Autowired
	public List<RetencionTitular> buscarRetencionesTitularPorCodigoNegocioYTipoNegocio(String codigoSfc,
			Integer idTipoNegocio);
}
