/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.excepciones.NoContentException;
import com.accionfiduciaria.modelo.dtos.DatosBasicosVinculadoDTO;
import com.accionfiduciaria.modelo.dtos.EncargoDTO;
import com.accionfiduciaria.modelo.dtos.ListaVinculadosDTO;
import com.accionfiduciaria.modelo.dtos.NovedadesJuridicasDTO;
import com.accionfiduciaria.modelo.dtos.NovedadesJuridicasDetalleDTO;
import com.accionfiduciaria.modelo.dtos.NovedadesJuridicasAccionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionConsultaExactaDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAccionPaginada;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionBuscarEncargoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaAccionObtenerNovedadesJuridicasDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaBusquedadVinculadoDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaPaginadaConsultaVinculadosDTO;
import com.accionfiduciaria.modelo.dtos.TipoDocumentoDTO;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.servicios.externos.IAccionBack;

/**
 * Clase que implementa la interfaz {@link IServicioVinculados}
 * 
 * @author José Santiago Polo Acosta - 05/12/2019 - jpolo@asesoftware.com
 *
 */
@Service
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:propiedades.properties")
public class ServicioVinculados implements IServicioVinculados {

	@Autowired
	private IAccionBack accionBack;

	@Autowired
	private IServicioParametroSistema servicioParametroSistema;

	@Autowired
	private IServicioTitular servicioTitular;

	@Autowired
	private Utilidades utilidades;

	@Value("${param.id.url.servicio.accion.buscar.beneficiario}")
	private Integer paramIdUrlServicioAccionBusquedadBeneficiario;

	@Value("${param.id.url.servicio.accion.buscar.negocios.beneficiario}")
	private Integer paramIdUrlServicioAccionBusquedadNegociosBeneficiario;

	@Value("${param.id.url.servicio.accion.validar.encargo}")
	private Integer paramIdUrlServicioAccionValidarEncargo;

	@Value("${param.id.url.servicio.accion.novedades.juridicas}")
	private Integer paramIdUrlServicioAccionNovedadesJuridicas;

	@Value("${prop.nombre}")
	private String propNombre;

	@Value("${prop.documento}")
	private String propDocumento;

	@Value("${cod.encargo.activo}")
	private String codigoEncargoActivo;

	@Value("${cod.tipo.naturaleza.natural}")
	private String codigoTipoNaturalezaNatural;

	@Value("${cedula.ciudadania}")
	private String cedula;

	@Value("${nombre.cedula}")
	private String nombreCedula;

	@Value("${cod.encargo.activo}")
	private String codEncargoActivo;

	@Override
	public ListaVinculadosDTO buscarListaVinculados(String argumento, String criterioBuscar, int pagina, int elementos,
			String tokenAutorizacion) throws Exception {
		return procesarRespuestaConsultaVinculados(accionBack.buscarBeneficiario(generarPeticionBusquedaBeneficiario(
				argumento, criterioBuscar, false, pagina, elementos, tokenAutorizacion)));

	}

	@Override
	public RespuestaBusquedadVinculadoDTO buscarBeneficiarioPorDocumento(String documento, String tokenAutorizacion) {
		return procesarRespuestaConsultaPorDocumento(accionBack.buscarBeneficiarioPorDocumento(
				generarPeticionBusquedaBeneficiarioPorDocumento(documento, tokenAutorizacion)));
	}

	@Override
	public EncargoDTO validarEncargo(String numeroEncargo, String documento, String tokenAutorizacion) {
		try {
			return procesarRespuestaValidacionEncargo(accionBack.validarEncargoCliente(
					servicioParametroSistema.obtenerValorParametro(paramIdUrlServicioAccionValidarEncargo), documento,
					tokenAutorizacion), numeroEncargo);
		} catch (NoContentException e) {
			return new EncargoDTO(Boolean.FALSE, null);
		}
	}

	@Override
	public List<NovedadesJuridicasDTO> obtenerNovedadesJuridicas(String codigoNegocio, String tipoDocumento, String documento, int pagina,
			int elementos, String tokenAutorizacion) {
		try {
			return procesarRespuestaObtenerNovedadesJuridicas(
					accionBack.obtenerNovedadesJuridicas(generarPeticionBusquedaNovedadesJuridicas(codigoNegocio, documento, null,
							Boolean.FALSE, pagina, elementos, tokenAutorizacion)));
		} catch (NoContentException e) {
			return new ArrayList<>();
		}
		
	}

	@Override
	public NovedadesJuridicasDetalleDTO detalleNovedadJuridica(String codigoNegocio, String tipoDocumento, String documento,
			String codigoEvento, String tokenAutorizacion) {
		try {
			return procesarRespuestaDetalleNovedadesJuridicas(
					accionBack.obtenerDetalleNovedadesJuridicas(generarPeticionBusquedaNovedadesJuridicas(codigoNegocio, documento,
							codigoEvento, Boolean.TRUE, 1, 1, tokenAutorizacion)));
		} catch (NoContentException e) {
			return new NovedadesJuridicasDetalleDTO();
		}
	}

	private EncargoDTO procesarRespuestaValidacionEncargo(RespuestaAccionBuscarEncargoDTO[] encargos,
			String numeroEncargo) {

		RespuestaAccionBuscarEncargoDTO encargoValido = Arrays.stream(encargos)
				.filter(encargo -> Objects.equals(encargo.getNumeroEncargo(), numeroEncargo)
						&& Objects.equals(encargo.getEstado(), codEncargoActivo))
				.findAny().orElse(new RespuestaAccionBuscarEncargoDTO());

		return new EncargoDTO(Objects.equals(encargoValido.getEstado(), codEncargoActivo),
				encargoValido.getNombreFidecomiso());
	}

	/**
	 * Carga los parámetros necesarios para realizar la petición al servicio de
	 * AccionBack de busquedad de beneficiarios.
	 * 
	 * @param argumento         El valor del criterio por el cual se realizara la
	 *                          búsqueda.
	 * @param criterioBuscar    El criterio por el cual se realizara la busquedad:
	 *                          nombre o documento
	 * @param pagina            La pagina en la que se ubicara el listado de
	 *                          resultados.
	 * @param elementos         El número de elementos por pagina que se mostraran.
	 * @param tokenAutorizacion El token de autorización.
	 * @return Un DTOP con los datos de la consulta.
	 */
	private PeticionAccionPaginada generarPeticionBusquedaBeneficiario(String argumento, String criterioBuscar,
			boolean busquedaExacta, int pagina, int elementos, String tokenAutorizacion) {

		String nombre = criterioBuscar.equalsIgnoreCase(propNombre.trim()) ? argumento.trim() : "";
		String documento = criterioBuscar.equalsIgnoreCase(propDocumento.trim()) ? argumento.trim() : "";

		String urlServicioAccionBusquedadBeneficiario = servicioParametroSistema
				.obtenerValorParametro(paramIdUrlServicioAccionBusquedadBeneficiario);

		return new PeticionAccionPaginada(elementos, pagina - 1, tokenAutorizacion, busquedaExacta, nombre, documento,
				urlServicioAccionBusquedadBeneficiario);
	}

	private PeticionAccionConsultaExactaDTO generarPeticionBusquedaBeneficiarioPorDocumento(String documento,
			String tokenAutorizacion) {
		String urlServicioAccionBusquedadBeneficiario = servicioParametroSistema
				.obtenerValorParametro(paramIdUrlServicioAccionBusquedadBeneficiario);
		return new PeticionAccionConsultaExactaDTO(documento, urlServicioAccionBusquedadBeneficiario,
				tokenAutorizacion);
	}

	/**
	 * Procesa la respuesta de la consulta de vinculados a AccionBack para su envío
	 * al front.
	 * 
	 * @param respuesta El DTO con la respuesta de la consulta.
	 * @return Un DTO con el listado de resultados.
	 */
	private ListaVinculadosDTO procesarRespuestaConsultaVinculados(RespuestaPaginadaConsultaVinculadosDTO respuesta) {

		if (respuesta.getResults() == null || respuesta.getResults().isEmpty()) {
			return new ListaVinculadosDTO(0, new ArrayList<DatosBasicosVinculadoDTO>());
		}

		List<DatosBasicosVinculadoDTO> datosBasicosBeneficiarios = respuesta.getResults().stream()
				.map(r -> cargarDatosBasicosBeneficiarioDTO(r)).collect(Collectors.toList());

		return new ListaVinculadosDTO(respuesta.getTotalPages() * respuesta.getLimit(), datosBasicosBeneficiarios);
	}

	/**
	 * Procesa la respuesta de la consulta por numero de documento de un vinculado
	 * para su envío al front.
	 * 
	 * @param respuesta Un DTO de tipo
	 *                  {@link RespuestaPaginadaConsultaVinculadosDTO} con la
	 *                  respuesta de la petición.
	 * @return UN DTO de tipo {@link RespuestaBusquedadVinculadoDTO} con los datos
	 *         del vinculado.
	 */
	private RespuestaBusquedadVinculadoDTO procesarRespuestaConsultaPorDocumento(
			RespuestaBusquedadVinculadoDTO[] respuesta) {
		if (respuesta == null || respuesta.length < 1) {
			return null;
		}
		if (respuesta[0].getNaturalezaJuridicaVinculado().equalsIgnoreCase(codigoTipoNaturalezaNatural)) {
			respuesta[0].setRazonSocial(respuesta[0].getNombreCompleto());
		}

		respuesta[0].setFechaVinculacion(utilidades.formatearFechaServiciosAccion(respuesta[0].getFechaVinculacion()));
		respuesta[0].setEnvioCorrespondenciaTipo(respuesta[0].getEnvioCorrespondencia());

		if (respuesta[0].getTipoDocumento() != null) {
			respuesta[0].getTipoDocumento()
					.setId(utilidades.covertirTipoDocumento(respuesta[0].getTipoDocumento().getId()));
		} else {
			respuesta[0].setTipoDocumento(new TipoDocumentoDTO(cedula, nombreCedula));
		}
		List<Titular> titulares = servicioTitular.obtenerTitularesPersona(respuesta[0].getTipoDocumento().getId(),
				respuesta[0].getNumeroDocumento());
		respuesta[0].setTieneNegocio(titulares != null && !titulares.isEmpty());
		return respuesta[0];
	}

	/**
	 * Carga los datos basicos de un usuario: nombre completo, tipo y número de
	 * documento, a partir de la respuesta obtenida en la consulta de datos de
	 * vinculados.
	 * 
	 * @param respuestaBusquedadVinculadoDTO UN DTO de tipo
	 *                                       {@link RespuestaBusquedadVinculadoDTO}
	 *                                       con los datos del vinculado.
	 * @return un DTO del tipo {@link DatosBasicosVinculadoDTO} con los datos
	 *         basicos del vinculado.
	 */
	private DatosBasicosVinculadoDTO cargarDatosBasicosBeneficiarioDTO(
			RespuestaBusquedadVinculadoDTO respuestaBusquedadVinculadoDTO) {
		DatosBasicosVinculadoDTO datosBasicosVinculadoDTO = new DatosBasicosVinculadoDTO();
		datosBasicosVinculadoDTO.setNombreCompleto(respuestaBusquedadVinculadoDTO.getNombreCompleto());
		datosBasicosVinculadoDTO.setTipoDocumento(respuestaBusquedadVinculadoDTO.getTipoDocumento() != null
				? utilidades.covertirTipoDocumento(respuestaBusquedadVinculadoDTO.getTipoDocumento().getId())
				: "");
		datosBasicosVinculadoDTO.setNumeroDocumento(respuestaBusquedadVinculadoDTO.getNumeroDocumento());

		return datosBasicosVinculadoDTO;
	}

	private PeticionAccionPaginada generarPeticionBusquedaNovedadesJuridicas(String codigoNegocio, String documento,
			String codigoNovedad, boolean busquedaExacta, int pagina, int elementos, String tokenAutorizacion) {

		String urlServicioAccion = servicioParametroSistema
				.obtenerValorParametro(paramIdUrlServicioAccionNovedadesJuridicas);

		urlServicioAccion = urlServicioAccion.replace("{idnegocio}", codigoNegocio);
		
		if(codigoNovedad != null && !codigoNovedad.isEmpty()) {
			urlServicioAccion = urlServicioAccion + "/"+ codigoNovedad;
		}

		return new PeticionAccionPaginada(elementos, pagina - 1, tokenAutorizacion, documento,
				urlServicioAccion);
	}

	private List<NovedadesJuridicasDTO> procesarRespuestaObtenerNovedadesJuridicas(
			RespuestaAccionObtenerNovedadesJuridicasDTO respuesta) {
		return respuesta.getResults().stream()
				.map(novedad -> new NovedadesJuridicasDTO(novedad.getSecuencialEvento(), novedad.getTipoEvento(),
						novedad.getEstado(), utilidades.formatearFechaServiciosAccion(novedad.getFechaEvento())))
				.collect(Collectors.toList());
	}

	private NovedadesJuridicasDetalleDTO procesarRespuestaDetalleNovedadesJuridicas(
			NovedadesJuridicasAccionDTO respuesta) {
		NovedadesJuridicasDetalleDTO novedadesJuridicasDetalleDTO = new NovedadesJuridicasDetalleDTO();
		novedadesJuridicasDetalleDTO.setCodigoEvento(respuesta.getSecuencialEvento());
		novedadesJuridicasDetalleDTO.setEstado(respuesta.getEstado());
		novedadesJuridicasDetalleDTO.setFechaRegistro(respuesta.getFechaEvento());
		novedadesJuridicasDetalleDTO.setTipoEvento(respuesta.getTipoEvento());
		novedadesJuridicasDetalleDTO.setUsuarioRegistro(respuesta.getNombreFuncionario());
		return novedadesJuridicasDetalleDTO;
	}

}
