package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

public interface IServicioTipoNegocio {
	
	public List<TipoNegocio> buscarTiposNegocio(Negocio negocio);

	public TipoNegocio guardarTipoNegocio(TipoNegocio tipoNegocio);
	
	public List<TipoNegocio> buscarTiposNegocioPorCodigoNegocio(String codigoNegocio);

	public List<TipoNegocio> buscarTiposNegocioPorTipo(Integer idTipoNegocio);
	
}
