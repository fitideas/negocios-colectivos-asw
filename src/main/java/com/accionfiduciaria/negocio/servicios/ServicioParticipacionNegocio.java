package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.ParticipacionesNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioParticipacionesNegocio;

@Service
public class ServicioParticipacionNegocio implements IServicioParticipacionNegocio {

	@Autowired
	private RepositorioParticipacionesNegocio repositorio;
	
	@Override
	public ParticipacionesNegocio guardar(ParticipacionesNegocio participacionesNegocio) {
		return repositorio.save(participacionesNegocio);
	}

	@Override
	public ParticipacionesNegocio obtenerParticipacion(Integer idParticipacion) {
		return repositorio.findById(idParticipacion).orElse(null);
	}

}
