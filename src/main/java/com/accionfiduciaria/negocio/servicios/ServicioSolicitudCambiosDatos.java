package com.accionfiduciaria.negocio.servicios;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.excepciones.NoExisteUsuarioException;
import com.accionfiduciaria.excepciones.ParametroNoEncontradoException;
import com.accionfiduciaria.modelo.dtos.HistoricoCambioDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.SolicitudCambioDatosDTO;
import com.accionfiduciaria.modelo.dtos.SolicitudCambioDatosGuardadaDTO;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.PersonaPK;
import com.accionfiduciaria.modelo.entidad.SolicitudCambiosDatos;
import com.accionfiduciaria.modelo.entidad.Usuario;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.especificaciones.SolicitudCambioEspecificacion;
import com.accionfiduciaria.negocio.repositorios.RepositorioPersona;
import com.accionfiduciaria.negocio.repositorios.RepositorioSolicitudCambioDatos;
import com.accionfiduciaria.negocio.repositorios.RepositorioUsuario;

/**
 * Clase que implementa la interfaz {@link IServicioSolicitudCambiosDatos}.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Service
@PropertySource("classpath:propiedades.properties")
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:mensajes.properties")
@Transactional
public class ServicioSolicitudCambiosDatos implements IServicioSolicitudCambiosDatos {

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;
	
	@Value("${formato.ddMMyyyyhhmmss}")
	private String formatoddMMyyyyhhmmss;

	@Value("${param.id.direccion.correo.solicitud}")
	private int paramIdDireccionCorreoSolicitud;

	@Value("${param.id.asunto.correo.solicitud}")
	private int paramIdAsuntoCorreoSolicitud;

	@Value("${param.id.cuerpo.correo.solicitud}")
	private int paramIdCuerpoCorreoSolicitud;

	@Autowired
	RepositorioSolicitudCambioDatos repositorioSolicitudCambioDatos;

	@Autowired
	IServicioEnvioCorreo servicioEnvioCorreo;

	@Autowired
	RepositorioUsuario repositorioUsuario;

	@Autowired
	RepositorioPersona repositorioPersona;

	@Autowired
	IServicioParametroSistema servicioParametroSistema;
	
	@Autowired
	Utilidades utilidades;

	@Override
	public List<SolicitudCambioDatosGuardadaDTO> obtenerSolicitudes(String tipoDocumento, String documento)
			throws Exception {
		return procesarRespuestaBusquedaSolicitudes(
				repositorioSolicitudCambioDatos.findByPersonaVinculado(new Persona(tipoDocumento, documento)));
	}

	@Override
	public void enviarSolicitud(List<SolicitudCambioDatosDTO> solicitudCambioDatos)
			throws NoExisteUsuarioException, MessagingException, Exception {
		procesarEnvioSolicitud(solicitudCambioDatos);
	}

	/**
	 * Realiza las tareas necesarias para el envio de la solicitud de cambio de
	 * datos; envío de correo y registro de la solicitud en la base de datos.
	 * 
	 * @param solicitudesCambioDatos Un DTO con los datos de la solicitud.
	 * @throws NoExisteUsuarioException
	 * @throws MessagingException
	 * @throws Exception
	 */
	private void procesarEnvioSolicitud(List<SolicitudCambioDatosDTO> solicitudesCambioDatos)
			throws NoExisteUsuarioException, MessagingException, Exception {
		Usuario usuario = repositorioUsuario.findById(solicitudesCambioDatos.get(0).getProfesional()).orElse(null);
		if (usuario == null) {
			throw new NoExisteUsuarioException();
		}
		StringBuilder textoDatosAModificar = new StringBuilder();
		for (SolicitudCambioDatosDTO solicitudCambioDatos : solicitudesCambioDatos) {
			guardarRegistroSolicitud(solicitudCambioDatos, usuario);
			textoDatosAModificar.append("<br>").append(solicitudCambioDatos.getCampoAModificar()).append(": ")
					.append(solicitudCambioDatos.getDatoNuevo()).append(", Número radicado: ")
					.append(solicitudCambioDatos.getNumeroRadicado());
		}
		enviarCorreoSolicitud(solicitudesCambioDatos.get(0), textoDatosAModificar.toString(), usuario);
	}

	/**
	 * Procesa la respuesta de la consulta para su envío al front.
	 * 
	 * @param solicitudes El listado de solicitudes encontradas.
	 * @return Una lista de DTOs de tipo {@link SolicitudCambioDatosGuardadaDTO}.
	 */
	private List<SolicitudCambioDatosGuardadaDTO> procesarRespuestaBusquedaSolicitudes(
			List<SolicitudCambiosDatos> solicitudes) {
		return solicitudes.stream().map(solicitud -> entidadSolicitudADTO(solicitud)).collect(Collectors.toList());
	}

	/**
	 * Convierte una entidad de tipo {@link SolicitudCambiosDatos} a un DTO de tipo
	 * {@link SolicitudCambioDatosGuardadaDTO}.
	 * 
	 * @param solicitud LA entidad a convertir.
	 * @return El DTO correspondiente a la entidad enviada.
	 */
	private SolicitudCambioDatosGuardadaDTO entidadSolicitudADTO(SolicitudCambiosDatos solicitud) {
		SolicitudCambioDatosGuardadaDTO solicitudCambioDatosGuardadaDTO = new SolicitudCambioDatosGuardadaDTO();
		solicitudCambioDatosGuardadaDTO.setIdSolicitud(solicitud.getIdSolicitud());
		SimpleDateFormat sf = new SimpleDateFormat(formatoddmmyyyy);
		solicitudCambioDatosGuardadaDTO.setFechaSolicitud(sf.format(solicitud.getFechaSolicitud()));
		solicitudCambioDatosGuardadaDTO.setCampoAModificar(solicitud.getCampoModificado());
		solicitudCambioDatosGuardadaDTO.setDatoNuevo(solicitud.getDatoNuevo());
		solicitudCambioDatosGuardadaDTO.setProfesional(solicitud.getPersonaUsuario().getNombreCompleto());
		solicitudCambioDatosGuardadaDTO.setNumeroRadicado(solicitud.getNumeroRadicado());
		return solicitudCambioDatosGuardadaDTO;
	}

	/**
	 * Crea el mensaje con la soicitud de cambio a partir de los datos alamcenados
	 * en la base de datos y lo envía por correo.
	 * 
	 * @param solicitudCambioDatos Un DTO con los datos de la solicitud.
	 * @param textoDatosAModificar El listado de campos con el nuevo valor que se solicitan cambiar.
	 * @param usuario              El Usuario que realiza la solcitud.
	 * @throws MessagingException
	 */
	private void enviarCorreoSolicitud(SolicitudCambioDatosDTO solicitudCambioDatos, String textoDatosAModificar,
			Usuario usuario) throws MessagingException, ParametroNoEncontradoException {
		String direccionCorreoSolicitud = servicioParametroSistema.obtenerValorParametro(paramIdDireccionCorreoSolicitud);
		String asunto = servicioParametroSistema.obtenerValorParametro(paramIdAsuntoCorreoSolicitud);
		String cuerpoCorreo = servicioParametroSistema.obtenerValorParametro(paramIdCuerpoCorreoSolicitud);

		
		cuerpoCorreo = cuerpoCorreo.replace("@camposAModificar", textoDatosAModificar);
		cuerpoCorreo = cuerpoCorreo.replace("@tipoDoc", solicitudCambioDatos.getTipoDocumentoVinculado());
		cuerpoCorreo = cuerpoCorreo.replace("@documento", solicitudCambioDatos.getNumeroDocumentoVinculado());
		cuerpoCorreo = cuerpoCorreo.replace("@nombreVinculado", solicitudCambioDatos.getNombreCompletoVinculado());
		cuerpoCorreo = cuerpoCorreo.replace("@nombreSolicitante", usuario.getPersona().getNombreCompleto());
		cuerpoCorreo = cuerpoCorreo.replace("@fechaSolicitud", Utilidades.formatearFecha(new Date(), formatoddmmyyyy));

		servicioEnvioCorreo.enviarCorreo(direccionCorreoSolicitud, asunto,
				cuerpoCorreo);
	}

	/**
	 * Guarda los datos de la solicitud. Si la persna vinculada a la que se le hace
	 * la solicitud no existe en la base de datos la crea.
	 * 
	 * @param solicitudCambioDatos DTO con los datos de la solicitud.
	 * @param usuario              El usuario que realiza la solicitud.
	 * @throws NoExisteUsuarioException
	 */
	private void guardarRegistroSolicitud(SolicitudCambioDatosDTO solicitudCambioDatos, Usuario usuario) {
		Persona vinculado = repositorioPersona.findById(new PersonaPK(solicitudCambioDatos.getTipoDocumentoVinculado(),
				solicitudCambioDatos.getNumeroDocumentoVinculado())).orElse(null);

		if (vinculado == null) {
			vinculado = repositorioPersona.save(new Persona(solicitudCambioDatos.getTipoDocumentoVinculado(),
					solicitudCambioDatos.getNumeroDocumentoVinculado(),
					solicitudCambioDatos.getNombreCompletoVinculado()));
		}

		repositorioSolicitudCambioDatos
				.save(dtoSolicitudAEntidad(solicitudCambioDatos, usuario.getPersona(), vinculado));
	}

	/**
	 * Obtiene la entidad de tipo {@link SolicitudCambiosDatos} a partir de un DTO.
	 * 
	 * @param solicitudCambioDatos El DTO con los datos de la solicitud.
	 * @param usuario              El Usuario que realiza la solicitud.
	 * @param vinculado            La persona a la que se le hace la solicitud.
	 * @return La entidad correspondiente a los datos enviados.
	 */
	private SolicitudCambiosDatos dtoSolicitudAEntidad(SolicitudCambioDatosDTO solicitudCambioDatos, Persona usuario,
			Persona vinculado) {
		SolicitudCambiosDatos solicitud = new SolicitudCambiosDatos();
		solicitud.setFechaSolicitud(new Date());
		solicitud.setCampoModificado(solicitudCambioDatos.getCampoAModificar());
		solicitud.setDatoAnterior(solicitudCambioDatos.getDatoAnterior() == null ? " " : solicitudCambioDatos.getDatoAnterior());
		solicitud.setDatoNuevo(solicitudCambioDatos.getDatoNuevo());
		solicitud.setNumeroRadicado(solicitudCambioDatos.getNumeroRadicado());
		solicitud.setPersonaUsuario(usuario);
		solicitud.setPersonaVinculado(vinculado);
		return solicitud;
	}

	@Override
	public List<SolicitudCambiosDatos> consultarConFiltros(ParametrosFiltroDTO dto) {
		List<String> usuarios = null;
		if (!dto.getSolicitadoPor().isEmpty()) {
			usuarios = dto.getSolicitadoPor().stream()
					.map(persona -> persona.getTipoDocumento() + persona.getNumeroDocumento())
					.collect(Collectors.toList());
		}
		Specification<SolicitudCambiosDatos> especificacionCombinada = Specification
				.where(dto.getRangoFecha().isEmpty() ? null
						: SolicitudCambioEspecificacion
								.filtrarPorFechaInicial(
										utilidades.convertirFecha(dto.getRangoFecha().get(0), formatoddmmyyyy)))
				.and(dto.getRangoFecha().isEmpty() ? null
						: SolicitudCambioEspecificacion.filtrarPorFechaFinal(utilidades.convertirFecha(
								dto.getRangoFecha().get(dto.getRangoFecha().size() - 1) + " 23:59:59",
								formatoddMMyyyyhhmmss)))
				.and(usuarios == null || usuarios.isEmpty() ? null
						: SolicitudCambioEspecificacion.filtrarPorTipoNumeroDocumentoUsuario(usuarios))
				.and(dto.getNumeroDocumentoVinculado() == null || dto.getNumeroDocumentoVinculado().isEmpty() ? null
						: SolicitudCambioEspecificacion
								.filtrarPorNumeroDocumentoVinculado(dto.getNumeroDocumentoVinculado()))
				.and(dto.getTipoDocumentoVinculado() == null || dto.getTipoDocumentoVinculado().isEmpty() ? null
						: SolicitudCambioEspecificacion
								.filtrarPorTipoDocumentoVinculado(dto.getTipoDocumentoVinculado()));

		return repositorioSolicitudCambioDatos.findAll(especificacionCombinada, Sort.by(Sort.Direction.DESC, "fechaSolicitud"));
	}

	@Override
	public List<HistoricoCambioDTO> consultarHistoricoSolicitudCambios(ParametrosFiltroDTO dto, String columna) {
		 return consultarConFiltros(dto).stream()
		 .map(ServicioSolicitudCambiosDatos::solicitudCambiosAHistoricoCambio)
		 .collect(Collectors.toList());
	}
	
	private static HistoricoCambioDTO solicitudCambiosAHistoricoCambio(SolicitudCambiosDatos solicitud) {
		HistoricoCambioDTO historico = new HistoricoCambioDTO();
		historico.setCampoCambio(solicitud.getCampoModificado());
		historico.setFechaCambio(Utilidades.formatearFecha(solicitud.getFechaSolicitud(),"dd/MM/yyyy"));
		historico.setDireccionIp("");
		historico.setRegistradoPor(solicitud.getPersonaUsuario().getNombreCompleto());
		historico.setDatoAnterior(solicitud.getDatoAnterior());
		historico.setDatoNuevo(solicitud.getDatoNuevo());
		return historico;
	}
}
