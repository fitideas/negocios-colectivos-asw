package com.accionfiduciaria.negocio.servicios;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.ParticipacionesNegocio;

public interface IServicioParticipacionNegocio {

	@Autowired
	public ParticipacionesNegocio guardar(ParticipacionesNegocio participacionesNegocio);
	
	@Autowired
	public ParticipacionesNegocio obtenerParticipacion(Integer idParticipacion);
}
