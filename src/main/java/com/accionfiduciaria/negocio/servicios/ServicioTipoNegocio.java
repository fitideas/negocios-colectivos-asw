package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.negocio.repositorios.RepositorioTipoNegocio;

@Service
public class ServicioTipoNegocio implements IServicioTipoNegocio {

	@Autowired
	RepositorioTipoNegocio repositorioTipoNegocio;
	
	@Override
	public List<TipoNegocio> buscarTiposNegocio(Negocio negocio) {
		return repositorioTipoNegocio.findByNegocioAndActivo(negocio, Boolean.TRUE);
	}

	@Override
	public TipoNegocio guardarTipoNegocio(TipoNegocio tipoActual) {
		return repositorioTipoNegocio.save(tipoActual);
	}

	@Override
	public List<TipoNegocio> buscarTiposNegocioPorCodigoNegocio(String codigoNegocio) {
		return repositorioTipoNegocio.findByNegocioCodSfcAndActivo(codigoNegocio, Boolean.TRUE);
	}

	@Override
	public List<TipoNegocio> buscarTiposNegocioPorTipo(Integer idTipoNegocio) {
		return repositorioTipoNegocio.findByTipoNegocioPK_TipoNegocioAndActivo(idTipoNegocio, Boolean.TRUE);
	}
}
