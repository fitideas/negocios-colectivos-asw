package com.accionfiduciaria.negocio.servicios;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.modelo.dtos.TipoFideicomisoDTO;
import com.accionfiduciaria.negocio.repositorios.RepositorioSubTipoFideicomiso;
import com.accionfiduciaria.negocio.repositorios.RepositorioTipoFideicomiso;

@Service
public class ServicioTipoFideicomiso implements IServicioTipoFideicomiso {

	@Autowired
	private RepositorioTipoFideicomiso repositorioTipoFideicomiso;
	
	@Autowired
	private RepositorioSubTipoFideicomiso repositorioSubTipoFideicomiso;
	
	@Override
	public List<TipoFideicomisoDTO> obtenerTipoFideicomiso() {
		return repositorioTipoFideicomiso.findAll().stream()
				.map(tipo -> new TipoFideicomisoDTO(tipo.getCodigo(), tipo.getNombre())).collect(Collectors.toList());
	}

	@Override
	public List<TipoFideicomisoDTO> obtenerSubTipoFideicomiso(String tipoFideicomiso) {
		return repositorioSubTipoFideicomiso.findByTipoFideicomiso_codigo(tipoFideicomiso).stream()
				.map(tipo -> new TipoFideicomisoDTO(tipo.getSubtipoFideicomisoPK().getCodigo(), tipo.getNombre()))
				.collect(Collectors.toList());
	}

}
