/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.excepciones.FaltanDatosObligatoriosException;
import com.accionfiduciaria.modelo.dtos.ConceptoDTO;
import com.accionfiduciaria.modelo.dtos.IngresoOperacionDTO;
import com.accionfiduciaria.modelo.dtos.IngresosTipoOperacionDTO;
import com.accionfiduciaria.modelo.dtos.ListaIngresosOperacionDTO;
import com.accionfiduciaria.modelo.entidad.IngresoOperacion;
import com.accionfiduciaria.modelo.entidad.IngresoOperacionPK;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;
import com.accionfiduciaria.modelo.entidad.PagoNegocioPK;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.repositorios.RepositorioIngresoOperacion;
import com.accionfiduciaria.negocio.repositorios.RepositorioValorDominioNegocio;

/**
 * @author efarias
 *
 */
@Service
@Transactional
@PropertySource("classpath:parametrosSistema.properties")
@PropertySource("classpath:propiedades.properties")
public class ServicioIngresoOperacion implements IServicioIngresoOperacion {

	private Logger logger = LogManager.getLogger(ServicioIngresoOperacion.class);

	@Autowired
	private IServicioTipoNegocio servicioTipoNegocio;

	@Autowired
	private IServicioPagoNegocio servicioPagoNegocio;

	@Autowired
	private RepositorioIngresoOperacion repositorioIngresoOperacion;

	@Autowired
	private RepositorioValorDominioNegocio repositorioValorDominioNegocio;

	@Autowired
	private Utilidades utilidades;

	@Value("${cod.dominio.tipo.negocio}")
	private String codigoDominioTipoNegocio;

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	// Constantes Execpcion
	public static final String NOT_FOUND_EXCEPCION_TIPO_NEGOCIO = "No se encontró ningún tipo de negocio asociado";
	public static final String EMPTY_PARAMS = "Los parámetros de búsqueda no pueden ser nulos o vacios";

	// Constantes
	public static final String EMPTY = "";
	public static final String UNDEFINED = "undefined";
	public static final String ACTIVO = "1";
	public static final String INACTIVO = "0";

	@Override
	public ListaIngresosOperacionDTO obtenerListaIngresosOperacion(String tokenAutorizacion, String periodo,
			String codigoNegocio) throws BusquedadSinResultadosException, FaltanDatosObligatoriosException {

		ListaIngresosOperacionDTO listaIngresosOperacionDTO = new ListaIngresosOperacionDTO();
		List<IngresosTipoOperacionDTO> listaTipoNegocio;

		try {

			if (periodo.equals(EMPTY) || periodo.equals(UNDEFINED) || codigoNegocio.equals(EMPTY)
					|| codigoNegocio.equals(UNDEFINED))
				throw new FaltanDatosObligatoriosException(EMPTY_PARAMS);

			List<TipoNegocio> listaTiposNegocio = servicioTipoNegocio.buscarTiposNegocioPorCodigoNegocio(codigoNegocio);

			if (listaTiposNegocio.isEmpty())
				throw new BusquedadSinResultadosException(NOT_FOUND_EXCEPCION_TIPO_NEGOCIO);

			listaTipoNegocio = listaTiposNegocio.stream()
					.map(tipoNegocio -> consultarIngresosOperacionPorTipoNegocio(tipoNegocio, periodo))
					.collect(Collectors.toList());

			listaIngresosOperacionDTO.setCodigoNegocio(codigoNegocio);
			listaIngresosOperacionDTO.setPeriodo(periodo);
			listaIngresosOperacionDTO.setListaTipoNegocio(listaTipoNegocio);

		} catch (BusquedadSinResultadosException e) {
			logger.error(e);
			throw e;
		}

		return listaIngresosOperacionDTO;
	}

	@Override
	public void guardarListaIngresosOperacion(ListaIngresosOperacionDTO listaIngresosOperacion) {

		List<IngresosTipoOperacionDTO> listaPagoNegocio = listaIngresosOperacion.getListaTipoNegocio();
		String periodo = listaIngresosOperacion.getPeriodo();
		String codigoNegocio = listaIngresosOperacion.getCodigoNegocio();

		for (IngresosTipoOperacionDTO pagoNegocioDTO : listaPagoNegocio) {

			if (pagoNegocioDTO.getCuenta() != null && !pagoNegocioDTO.getCuenta().isEmpty()
					&& pagoNegocioDTO.getListaIngresosOperacion() != null
					&& !pagoNegocioDTO.getListaIngresosOperacion().isEmpty()) {

				// Transforma, valida y guarda un PagoNegocio, posteriormente lo retorna.
				PagoNegocio pagoNegocio = servicioPagoNegocio.obtenerPagoNegocio(pagoNegocioDTO, codigoNegocio,
						periodo);
				Integer codigoTipoNegocio = pagoNegocioDTO.getCodigoTipoNegocio();

				List<IngresoOperacion> listaIngresosOperacionExistentes = transformarYFiltrarListaDTO(Boolean.FALSE,
						pagoNegocioDTO, codigoNegocio, periodo, codigoTipoNegocio);

				// Inactiva los registros eliminados desde el front.
				if (!listaIngresosOperacionExistentes.isEmpty())
					inactivarIngresosOperacion(listaIngresosOperacionExistentes, pagoNegocio);

				List<IngresoOperacion> listaIngresosOperacionNuevos = transformarYFiltrarListaDTO(Boolean.TRUE,
						pagoNegocioDTO, codigoNegocio, periodo, codigoTipoNegocio);

				// Guarda los nuevos ingresos de operación.
				if (!listaIngresosOperacionNuevos.isEmpty())
					repositorioIngresoOperacion.saveAll(listaIngresosOperacionNuevos);
			}
		}
	}
	
	@Override
	public List<IngresoOperacion> buscarIngresosOperacion(PagoNegocio pagoNegocio) {
		return repositorioIngresoOperacion.buscarIngresosOperacion(pagoNegocio);
	}
	
	/**
	 * Este método convierte una lista IngresoOperacion a una lista de ConceptoDTO;
	 * 
	 * @author efarias
	 * 
	 * @param listaIngresos
	 * @return List<ConceptoDTO>
	 */
	@Override
	public List<ConceptoDTO> obtenerListaConceptosIngresosDTO(List<IngresoOperacion> listaIngresos) {
		List<ConceptoDTO> ingresosDTO = new ArrayList<>();
		listaIngresos.forEach(ingreso -> {
			ConceptoDTO conceptoDTO = new ConceptoDTO("Ingreso",
					Utilidades.formatearFecha(ingreso.getFecha(), formatoddmmyyyy), ingreso.getValor());
			ingresosDTO.add(conceptoDTO);
		});
		return ingresosDTO;
	}

	/**
	 * Este método consulta si existen registros en la entidad pago negocio bajo el
	 * codigo de negocio, codigo de tipo de negocio y periodo, en dado caso que no
	 * existan registros retorna el objeto sin ingresos de operacion, si existe el
	 * registro, adiciona los ingresos de operación asociados al registro de pago
	 * negocio.
	 * 
	 * @author efarias
	 * 
	 * @param tipoNegocio
	 * @param periodo
	 * @return IngresosTipoOperacionDTO
	 */
	public IngresosTipoOperacionDTO consultarIngresosOperacionPorTipoNegocio(TipoNegocio tipoNegocio, String periodo) {

		PagoNegocioPK pagoNegocioPK = new PagoNegocioPK(periodo, tipoNegocio.getTipoNegocioPK().getTipoNegocio(),
				tipoNegocio.getTipoNegocioPK().getCodSfc());

		if (servicioPagoNegocio.existePagoNegocio(pagoNegocioPK)) {
			return listaConIngresosOperacion(tipoNegocio, pagoNegocioPK);
		} else {
			return listaSinIngresoOperacion(tipoNegocio);
		}
	}

	/**
	 * Este metodo retorna un objeto tipo IngresosTipoOperacionDTO con detalle de
	 * ingresos de operación.
	 * 
	 * @author efarias
	 * 
	 * @param tipoNegocio
	 * @param codigoNegocio
	 * @return IngresosTipoOperacionDTO
	 */
	public IngresosTipoOperacionDTO listaConIngresosOperacion(TipoNegocio tipoNegocio, PagoNegocioPK pagoNegocioPK) {

		IngresosTipoOperacionDTO listaIngresosTipoOperacionDTO = new IngresosTipoOperacionDTO();
		List<IngresoOperacionDTO> listaIngresosOperacion;
		List<IngresoOperacion> listIngresoOperacion;

		listaIngresosTipoOperacionDTO.setCodigoTipoNegocio(tipoNegocio.getTipoNegocioPK().getTipoNegocio());
		ValorDominioNegocio valor = repositorioValorDominioNegocio.buscarValorDominioPorCodDominioYValorDominio(
				codigoDominioTipoNegocio, tipoNegocio.getTipoNegocioPK().getTipoNegocio());

		if (valor != null)
			listaIngresosTipoOperacionDTO.setNombreTipoNegocio(valor.getDescripcion());

		PagoNegocio pagoNegocio = servicioPagoNegocio.buscarPagoNegocio(pagoNegocioPK);

		if (pagoNegocio != null)
			listaIngresosTipoOperacionDTO.setCuenta(pagoNegocio.getCuenta());

		listIngresoOperacion = repositorioIngresoOperacion.buscarIngresosOperacion(pagoNegocio);
		listaIngresosOperacion = crearListaIngresoOperacionDTO(listIngresoOperacion);
		listaIngresosTipoOperacionDTO.setListaIngresosOperacion(listaIngresosOperacion);

		return listaIngresosTipoOperacionDTO;
	}

	/**
	 * Este método retorna un objeto tipo IngresosTipoOperacionDTO sin detalle de
	 * ingresos de operación.
	 * 
	 * @author efarias
	 * 
	 * @param tipoNegocio
	 * @param codigoNegocio
	 * @return IngresosTipoOperacionDTO
	 */
	public IngresosTipoOperacionDTO listaSinIngresoOperacion(TipoNegocio tipoNegocio) {

		IngresosTipoOperacionDTO listaIngresosTipoOperacionDTO = new IngresosTipoOperacionDTO();
		listaIngresosTipoOperacionDTO.setListaIngresosOperacion( new ArrayList<IngresoOperacionDTO>());
		listaIngresosTipoOperacionDTO.setCuenta(EMPTY);
		listaIngresosTipoOperacionDTO.setCodigoTipoNegocio(tipoNegocio.getTipoNegocioPK().getTipoNegocio());
		ValorDominioNegocio valor = repositorioValorDominioNegocio.buscarValorDominioPorCodDominioYValorDominio(
				codigoDominioTipoNegocio, tipoNegocio.getTipoNegocioPK().getTipoNegocio());

		if (valor != null)
			listaIngresosTipoOperacionDTO.setNombreTipoNegocio(valor.getDescripcion());

		return listaIngresosTipoOperacionDTO;
	}

	/**
	 * Este método transforma una lista de objetos IngresoOperacion a una lista de
	 * objetos IngresoOperacionDTO para su presentación en front.
	 * 
	 * @author efarias
	 * 
	 * @param listIngresoOperacion
	 * @return List<IngresoOperacionDTO>
	 */
	public List<IngresoOperacionDTO> crearListaIngresoOperacionDTO(List<IngresoOperacion> listIngresoOperacion) {

		List<IngresoOperacionDTO> listaIngresoOperacionDTO = new ArrayList<>();

		if (listIngresoOperacion.isEmpty())
			return listaIngresoOperacionDTO;

		for (IngresoOperacion ingresoOperacion : listIngresoOperacion) {

			IngresoOperacionDTO ingresoOperacionDTO = new IngresoOperacionDTO();
			ingresoOperacionDTO
					.setFechaSistema(Utilidades.formatearFecha(ingresoOperacion.getFechaSistema(), formatoddmmyyyy));
			ingresoOperacionDTO
					.setFechaIngreso(Utilidades.formatearFecha(ingresoOperacion.getFecha(), formatoddmmyyyy));
			ingresoOperacionDTO.setEsEditable(Boolean.FALSE);
			ingresoOperacionDTO.setPeriodo(ingresoOperacion.getIngresoOperacionPK().getPeriodo());
			ingresoOperacionDTO.setValorIngreso(ingresoOperacion.getValor());
			ingresoOperacionDTO.setRadicadoIngreso(ingresoOperacion.getIngresoOperacionPK().getRadicado());
			listaIngresoOperacionDTO.add(ingresoOperacionDTO);
		}

		return listaIngresoOperacionDTO;
	}

	/**
	 * Este método devuelve una lista de IngresoOperacion, recibe como parámetros el
	 * código de negocio, el código de tipo de negocio, el periodo y las lista de
	 * DTO a transformar.
	 * 
	 * @author efarias
	 * 
	 * @param esEditado         -> Filtra la lista por registros nuevos o ya
	 *                          existentes.
	 * @param pagoNegocioDTO
	 * @param codigoNegocio
	 * @param periodo
	 * @param codigoTipoNegocio
	 * @return List<IngresoOperacion>
	 */
	public List<IngresoOperacion> transformarYFiltrarListaDTO(boolean esEditado,
			IngresosTipoOperacionDTO pagoNegocioDTO, String codigoNegocio, String periodo, Integer codigoTipoNegocio) {

		List<IngresoOperacionDTO> listaIngresoOperacionDTO;

		// Filtra la lista por registros nuevos o existentes.
		if (Boolean.TRUE.equals(esEditado)) {
			listaIngresoOperacionDTO = pagoNegocioDTO.getListaIngresosOperacion().stream()
					.filter(ingreso -> Boolean.TRUE.equals(ingreso.isEsEditable())).collect(Collectors.toList());
		} else {
			listaIngresoOperacionDTO = pagoNegocioDTO.getListaIngresosOperacion().stream()
					.filter(ingreso -> Boolean.FALSE.equals(ingreso.isEsEditable())).collect(Collectors.toList());
		}

		return listaDtoAListaIngresoOperacion(listaIngresoOperacionDTO, codigoNegocio, periodo, codigoTipoNegocio);

	}

	/**
	 * Este método transforma una lista listaIngresoOperacionDTO a una lista de
	 * Ingresos de operación, teniendo el cuenta el código de negocio y el periodo.
	 * 
	 * @author efarias
	 * 
	 * @param listaIngresoOperacionDTO
	 * @param codigoNegocio
	 * @param periodo
	 * @return List<IngresoOperacion>
	 */
	public List<IngresoOperacion> listaDtoAListaIngresoOperacion(List<IngresoOperacionDTO> listaIngresoOperacionDTO,
			String codigoNegocio, String periodo, Integer codigoTipoNegocio) {

		List<IngresoOperacion> listaIngresosOperacion = new ArrayList<>();

		if (listaIngresoOperacionDTO.isEmpty()) {
			
			return listaIngresosOperacion;
			
		} else {

			for (IngresoOperacionDTO dto : listaIngresoOperacionDTO) {
				IngresoOperacion ingresoOPeracion = new IngresoOperacion(periodo, codigoTipoNegocio, codigoNegocio,
						dto.getRadicadoIngreso());
				ingresoOPeracion.setFecha(utilidades.convertirFecha(dto.getFechaIngreso(), formatoddmmyyyy));
				ingresoOPeracion.setValor(dto.getValorIngreso());
				ingresoOPeracion.setFechaSistema(utilidades.convertirFecha(dto.getFechaSistema(), formatoddmmyyyy));
				ingresoOPeracion.setEstado(ACTIVO);
				listaIngresosOperacion.add(ingresoOPeracion);
			}
			
			return listaIngresosOperacion;
		}
	}

	/**
	 * Este método inactiva los registros de ingreso de operación por tipo de
	 * negocio los cuales se hayan eliminado desde el fronted.
	 * 
	 * @author efarias
	 * 
	 * @param ingresosOperacion
	 * @param pagoNegocio
	 */
	public void inactivarIngresosOperacion(List<IngresoOperacion> ingresosOperacion, PagoNegocio pagoNegocio) {

		List<IngresoOperacion> ingresosOperacionActuales = repositorioIngresoOperacion
				.buscarIngresosOperacion(pagoNegocio);

		// Se obtienen una lista IngresosOperacionPK para identificar si existe o no.
		List<IngresoOperacionPK> listaIngresosActualesPK = obtenerListaIngresosPK(ingresosOperacionActuales);
		List<IngresoOperacionPK> listaIngresosPK = obtenerListaIngresosPK(ingresosOperacion);

		// Se verifica cuales ingresos han sido eliminados por el front y se elimina.
		if (!listaIngresosActualesPK.isEmpty()) {
			listaIngresosActualesPK.forEach(ingresoActualPk -> {
				if (!listaIngresosPK.contains(ingresoActualPk)) {
					logger.info("No existe lo inactiva.");
					IngresoOperacion ingreso = repositorioIngresoOperacion.findById(ingresoActualPk).orElse(null);
					if (ingreso != null) {
						ingreso.setEstado(INACTIVO);
						repositorioIngresoOperacion.save(ingreso);
					}
				}
			});
		}
	}
	
	/**
	 * Este método obtiene una lista de ingresos de operación y retorna las PK de
	 * cada ingreso de operación.
	 * 
	 * @author efarias
	 * 
	 * @param ingresosOperacion
	 * @return List<IngresoOperacionPK>
	 */
	public List<IngresoOperacionPK> obtenerListaIngresosPK(List<IngresoOperacion> ingresosOperacion) {
		
		List<IngresoOperacionPK> listaIngresosPK = new ArrayList<>();
		
		if (ingresosOperacion.isEmpty()) {
			return listaIngresosPK;
		} else {
			ingresosOperacion.forEach(ingreso -> 
				listaIngresosPK.add(ingreso.getIngresoOperacionPK())
			);
			return listaIngresosPK;
		}
	}
}
