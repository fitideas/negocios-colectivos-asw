package com.accionfiduciaria.negocio.servicios;

import java.util.List;


import com.accionfiduciaria.modelo.dtos.ConceptosDistribucionDTO;
import com.accionfiduciaria.modelo.dtos.PeticionInformesModificacionDTO;
import com.accionfiduciaria.modelo.entidad.Distribucion;


//Interface para atender consulta con filtros o sin ellos para la entidad distribución

public interface IServicioReportes {
	
	public List<Distribucion> consultarConFiltros(PeticionInformesModificacionDTO dto) ;

	public ConceptosDistribucionDTO obtenerConteoSolicitudes(PeticionInformesModificacionDTO dto);

}
