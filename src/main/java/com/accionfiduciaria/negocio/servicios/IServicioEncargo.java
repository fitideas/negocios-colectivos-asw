package com.accionfiduciaria.negocio.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accionfiduciaria.modelo.entidad.Encargo;

public interface IServicioEncargo {
	
	@Autowired
	public List<Encargo> buscarEncargoNegocio(String codigoNegocio);

	@Autowired
	public Encargo guardarEncargo(Encargo encargo);
	
}
