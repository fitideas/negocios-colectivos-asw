/**
 * 
 */
package com.accionfiduciaria.negocio.servicios;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.accionfiduciaria.excepciones.ArchivoNoValidoException;
import com.accionfiduciaria.excepciones.BusquedadSinResultadosException;
import com.accionfiduciaria.modelo.dtos.DatosEquipoResponsableDTO;
import com.accionfiduciaria.modelo.dtos.ListaNegociosPorDerechos;
import com.accionfiduciaria.modelo.dtos.ListaTitularesDTO;
import com.accionfiduciaria.modelo.dtos.NegociosPorDerechosDTO;
import com.accionfiduciaria.modelo.dtos.PersonaPorDerechosDTO;
import com.accionfiduciaria.modelo.dtos.PeticionTitularesNegociosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoPagoZipDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaDatosCertificadosDTO;
import com.accionfiduciaria.modelo.entidad.AudHistorial;
import com.accionfiduciaria.modelo.entidad.AudHistorialDetalle;
import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitular;
import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.UbicacionGeografica;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;
import com.accionfiduciaria.negocio.componentes.Utilidades;
import com.accionfiduciaria.negocio.especificaciones.AudHistorialEspecificacion;
import com.accionfiduciaria.negocio.especificaciones.DistribucionRetencionesTitularEspecificacion;
import com.accionfiduciaria.negocio.repositorios.RepositorioAudHistorial;
import com.accionfiduciaria.negocio.repositorios.RepositorioDistribucionRetencionesTitular;
import com.accionfiduciaria.negocio.repositorios.RepositorioValorDominioNegocio;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.ConverterTypeVia;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

/**
 * @author fcher
 *
 */
@Service
@PropertySource("classpath:mensajes.properties")
public class ServicioCertificados implements IServicioCertificados {

	@Value("${param.id.ruta.almacenamiento.archivos.certificados.participacion}")
	private Integer paramIdRutaCertificadoParticipacion;

	@Value("${param.id.ruta.almacenamiento.archivos.certificados.rentaexcenta}")
	private Integer paramIdRutaCertificadoRentaExcenta;

	@Value("${param.id.ruta.almacenamiento.archivos.certificados.ingresosretenciones}")
	private Integer paramIdRutaCertificadoIngresosRetenciones;

	@Value("${param.id.ruta.almacenamiento.archivos}")
	private Integer paramIdRutaAlmacenamientoArchivos;

	@Value("${param.id.retenfuente}")
	private Integer paramIdRetenFuente;

	@Value("${formato.ddMMyyyyhhmmss}")
	private String formatoddMMyyyyhhmmss;
	
	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	@Value("${msg.error.fallo.al.crear.carpeta}")
	private String errorCrearCarpeta;
	
	@Value("${cod.dominio.tipo.negocio}")
	private String  dominioTipoNegocio;
	
	@Value("${error.negocio.sin.administrador}")
	private String noAdministrador;
	
	@Value("${cod.dominio.retenciones}")
	private String dominioRetenciones;
	
	@Autowired
	private Utilidades utilidades;

	@Autowired
	private RepositorioAudHistorial repositorioAudHistorial;

	@Autowired
	private RepositorioValorDominioNegocio repositorioValorDominioNegocio;
	
	@Autowired
	private ServicioAudHistorial servicioAudHistorial;
	
	@Autowired
	private IServicioTitular servicioTitular;
	
	@Autowired
	private IServicioDominio servicioDominio;
	
	@Autowired
	private IServicioUbicacionGeografica servicioUbicacionGeografica;
	

	public static final String CONTENT_TYPE = "application/zip";
	public static final String EXTENSION_ZIP = ".ZIP";
	public static final String PARAMETROS_VACIOS = "Los parámetros de búsqueda no pueden ser nulos o vacios";
	
	//DATOS COMUNES ENTRE REPORTES 
	private static final String NOMBRENEGOCIO = "nombreNegocio";
	private static final String TIPODOCUMENTO = "tipoDocumento";
	private static final String NUMERODOCUMENTO = "numeroDocumento";
	private static final String NOMBRECOMPLETO = "nombreCompleto";
	private static final String CODIGOSFC ="codigoSFC";
	
	//DATOS REPORTE RENTA EXCENTA
	private static final String FECHACONSTITUCION = "fechaConstitucion";
	private static final String MATRICULAINMOBILIARIA = "matriculaInmobiliaria";
	private static final String DESCRIPCIONTIPOS = "descripcionTipos";
	private static final String LICENCIACONSTRUCCION = "licenciaConstruccion"; 
	private static final String CIUDADUBICACION = "ciudadUbicacion";
	private static final String ROLFIDUCIARIA = "rolFiduciaria";
	private static final String ANOGRABABLE = "anoGrabable";
	private static final String VALORENLETRAS = "valorEnLetras";
	private static final String VALOR = "valor";
	private static final String DIAS = "dias";
	private static final String MESENLETRAS = "mesEnLetras";
	private static final String ANO = "ano";
	
	//DATOS CERTIFICADO INGRESOS Y RETENCIONES
	
	private static final String CODIGOINTERNO = "codigoInterno";
	private static final String PARTICIPACION = "participacion";
	private static final String BASERETENCION = "baseRetencion";
	private static final String TOTALRETENIDO = "totalRetenido";
	private static final String CIUDADADUANA = "ciudadAduana";
	private static final String FECHAGENERACION = "fechaGeneracion";
	private static final String CIUDADUBICACIONRET = "ciudadUbicacion";
	
	//DATOS CERTIFICADO RENTA EXCENTA
	private static final String NUMERODERECHOS = "numeroDerechos";
	private static final String DIASMESLETRAS = "diasMesLetras";
	private static final String NOMBREMESLETRAS = "nombreMesLetras";
	private static final String ANOENLETRAS = "anoEnLetras";
	private static final String RESPONSABLE = "responsable";
	
	//ROL DE ADMINISTRADO 
	private static final String ADMINISTRADOR = "ADMINISTRADOR";
	
	//COLUMNAS A CONSULTAR
	private static final String FECHA_CONSTITUCION = "FECHA_CONSTITUCION";
	private static final String MATRICULA_INMOBILIARIA = "MATRICULA_INMOBILIARIA";
	private static final String LICENCIA_CONSTRUCCION = "LICENCIA_CONSTRUCCION";
	private static final String CIUDAD_UBICACION = "CIUDAD_UBICACION";
	private static final String ROL_FIDUCIARIA="ROL_FIDUCIARIA";
	private static final String SECCIONAL_CIUDAD_ADUANA="SECCIONAL_ADUANA_CIUDAD";
	private static final String PORCENTAJE="PORCENTAJE";
	private static final String NUMERO_DERECHOS="NUMERO_DERECHOS";
	
	private static final String NEGNEGOCIO = "NEG_NEGOCIO";
	private static final String NEG_TITULAR = "NEG_TITULAR";
	
	private static final String TIPO_PARTICIPACION = "PART";
	private static final String TIPO_INGRESOS_RETENCIONES = "INGRET";
	private static final String TIPO_RENTA_EXCENTA = "RENTAEXC";
	private static final String CERTIFICADOS = "certificados";
	private static final String ESTADO_DISTRIBUIDO = "DISTRIBUIDO";	
	
	@Autowired
	private RepositorioDistribucionRetencionesTitular repositorioDistribucionRetencionesTitular;
	@Autowired
	private IServicioParametroSistema servicioParametros;

	@Autowired
	private IServicioNegocio servicioNegocio;

	@Override
	public ListaTitularesDTO obtenerTitularesPaginado(PeticionTitularesNegociosDTO dto, String tokenAutorizacion) {
		
		Set<PersonaPorDerechosDTO> titularesUnicos = new HashSet<>();
		List<RespuestaDatosCertificadosDTO> datosCertificadosDTO = null;
		PeticionTitularesNegociosDTO[] arrayDTO = {dto};
		
		if (dto.getTipoCertificado().equals(TIPO_PARTICIPACION)) {
			List<Titular> titulares;
			if (dto.getNombreTitular() != null && !dto.getNombreTitular().isEmpty()) {
				titulares = servicioTitular.buscarTitularNombrePaginado(dto.getNombreTitular(), dto.getPagina(),
						dto.getElementos());
			} else if (dto.getCodigoNegocio() != null && !dto.getCodigoNegocio().isEmpty()) {
				titulares = servicioTitular.buscarTitularesPorCodigoNegocioPaginado(dto.getCodigoNegocio(),
						dto.getPagina(), dto.getElementos());
			} else {
				if(dto.getTipoDocumento() != null && !dto.getTipoDocumento().isEmpty()) {
					titulares = servicioTitular.buscarTitularesPersonaPaginado(dto.getTipoDocumento(),
							dto.getNumeroDocumento(), dto.getPagina(), dto.getElementos());
				} else {
					titulares = servicioTitular.buscarTitularesPorDocumentoPaginado(dto.getNumeroDocumento(), dto.getPagina(), 
							dto.getElementos());
				}
			}

			titulares.forEach(titular -> {
				String codSFC = titular.getTitularPK().getCodSfc();
				String tipoDoc = titular.getTitularPK().getTipoDocumento();
				String numDoc = titular.getTitularPK().getNumeroDocumento();
				String llave = "{" + codSFC + "}{" + tipoDoc + "}{" + numDoc + "}";
				Date periodo = obtenerPeriodo(dto.getAnoGrabable());
				String participacion = obtenerDatosDeTabla(codSFC, llave, NEG_TITULAR, PORCENTAJE, periodo);
				String numeroDerechos = obtenerDatosDeTabla(codSFC, llave, NEG_TITULAR, NUMERO_DERECHOS, periodo);
				if (!participacion.isEmpty()) {
					titularesUnicos.add(new PersonaPorDerechosDTO(titular.getPersona().getNombreCompleto(), tipoDoc,
							numDoc, utilidades.formatearParticipacion(participacion), numeroDerechos, codSFC));
				}
			});
		} else if (dto.getTipoCertificado().equals(TIPO_RENTA_EXCENTA)) {
			datosCertificadosDTO = obtenerDatosRentaExcenta(arrayDTO);
		} else if (dto.getTipoCertificado().equals(TIPO_INGRESOS_RETENCIONES)) {
			datosCertificadosDTO = obtenerDatosIngresosRetenciones(arrayDTO);
		}
		
		if(datosCertificadosDTO != null) {
			datosCertificadosDTO.forEach(datosCertificado -> {
				PersonaPorDerechosDTO personaPorDerechosDTO = new PersonaPorDerechosDTO(datosCertificado.getNombreCompleto(), datosCertificado.getTipoDocumento(),
						datosCertificado.getNumeroDocumento());
				personaPorDerechosDTO.setTotalRetenido(datosCertificado.getTotalRetenido());
				personaPorDerechosDTO.setBaseRetencion(datosCertificado.getBaseRetencion());
				personaPorDerechosDTO.setRentaExcenta(datosCertificado.getRentaExcenta());
				titularesUnicos.add(personaPorDerechosDTO);
			});
		}
		int totalRegistros =  titularesUnicos.size();
		return new ListaTitularesDTO(totalRegistros, titularesUnicos);
		
	}

	// metodo usado solamente para obtener los negocios
	@Override
	public ListaNegociosPorDerechos obtenerNegociosPaginado(PeticionTitularesNegociosDTO dto,
			String tokenAutorizacion) {
		
		Set<NegociosPorDerechosDTO> negociosPorDerechos = new HashSet<>();
		List<RespuestaDatosCertificadosDTO> datosCertificadosDTO = null;
		PeticionTitularesNegociosDTO[] arrayDTO = {dto};
		
		if (dto.getTipoCertificado().equals(TIPO_PARTICIPACION)) {
			List<Negocio> negocios = new ArrayList<>();
			if (dto.getNombreNegocio() != null && !dto.getNombreNegocio().isEmpty()) {
				negocios = servicioNegocio.buscarNegocioPorNombrePaginado(dto.getNombreNegocio(), dto.getPagina(),
						dto.getElementos());
			} else if (dto.getCodigoNegocio() != null && !dto.getCodigoNegocio().isEmpty()) {
				negocios.add(servicioNegocio.obtenerNegocio(dto.getCodigoNegocio()));
			} else {
				negocios = servicioNegocio.buscarNegociosPorDocumentoTitular(dto.getTipoDocumento(),
						dto.getNumeroDocumento(), dto.getPagina(), dto.getElementos());
			}

			negocios.forEach(negocio -> {
				String llave = "{" + negocio.getCodSfc() + "}{" + dto.getTipoDocumento() + "}{"
						+ dto.getNumeroDocumento() + "}";
				Date periodo = obtenerPeriodo(dto.getAnoGrabable());
				String participacion = obtenerDatosDeTabla(negocio.getCodSfc(), llave, NEG_TITULAR, PORCENTAJE,
						periodo);
				String numeroDerechos = obtenerDatosDeTabla(negocio.getCodSfc(), llave, NEG_TITULAR, NUMERO_DERECHOS,
						periodo);
				NegociosPorDerechosDTO negociosPorDerechosDTO = new NegociosPorDerechosDTO(negocio.getNombre(),
						negocio.getCodSfc(), utilidades.obtenerCodigoInterno(negocio.getNombre()),
						dto.getTipoDocumento(), dto.getNumeroDocumento());
				
				negociosPorDerechosDTO.setParticipacion(utilidades.formatearParticipacion(participacion));
				negociosPorDerechosDTO.setNumeroDerechos(numeroDerechos);
				negociosPorDerechos.add(negociosPorDerechosDTO);
			});
		} else if (dto.getTipoCertificado().equals(TIPO_RENTA_EXCENTA)) {
			datosCertificadosDTO = obtenerDatosRentaExcenta(arrayDTO);
		} else if (dto.getTipoCertificado().equals(TIPO_INGRESOS_RETENCIONES)) {
			datosCertificadosDTO = obtenerDatosIngresosRetenciones(arrayDTO);
		}
		
		if(datosCertificadosDTO != null) {
			datosCertificadosDTO.forEach(datosCertificado -> {
				NegociosPorDerechosDTO negociosPorDerechosDTO = new NegociosPorDerechosDTO(datosCertificado.getNombreNegocio(),
						datosCertificado.getCodSFC(), utilidades.obtenerCodigoInterno(datosCertificado.getNombreNegocio()),
						dto.getTipoDocumento(), dto.getNumeroDocumento());
				negociosPorDerechosDTO.setTotalRetenido(datosCertificado.getTotalRetenido());
				negociosPorDerechosDTO.setBaseRetencion(datosCertificado.getBaseRetencion());
				negociosPorDerechosDTO.setRentaExcenta(datosCertificado.getRentaExcenta());
				negociosPorDerechos.add(negociosPorDerechosDTO);
			});
		}
		
		ListaNegociosPorDerechos respuesta = new ListaNegociosPorDerechos();

		int totalRegistros =  negociosPorDerechos.size();
		respuesta.setTotalNegocios(totalRegistros);
		 respuesta.setNegocios(negociosPorDerechos);
		return respuesta;
	}

	public NegociosPorDerechosDTO aNegocioDTO(String codNegocio, String nombreNegocio, String tipoDocumento,
			String numeroDocumento, String token) {
		NegociosPorDerechosDTO n = new NegociosPorDerechosDTO();
		n.setCodSFC(codNegocio);
		n.setNombre(nombreNegocio);
		n.setNumeroDocumento(numeroDocumento);
		n.setTipoDocumento(tipoDocumento);
		 n.setCodigoInterno(servicioNegocio.buscarNegocioPorCodigo(codNegocio,
		 token).getCodigo());

		return n;
	}

	@Override
	public RespuestaArchivoPagoZipDTO generarCertificados(String tipo, PeticionTitularesNegociosDTO[] peticion,
			String tokenAutorizacion)
			throws IOException, XDocReportException, ArchivoNoValidoException, BusquedadSinResultadosException {
		RespuestaArchivoPagoZipDTO archivoZip = null;
		List<RespuestaDatosCertificadosDTO> dtoSalida;
		if (tipo.equals(TIPO_PARTICIPACION)) {
			dtoSalida = obtenerDatosParticipacion(peticion, tokenAutorizacion);
			archivoZip = generarArchivoParticipacion(dtoSalida);
		} else if (tipo.equals(TIPO_INGRESOS_RETENCIONES)) {
			dtoSalida = obtenerDatosIngresosRetenciones(peticion);
			archivoZip = generarArchivoIngRet(dtoSalida);
		} else if (tipo.equals(TIPO_RENTA_EXCENTA)) {
			dtoSalida = obtenerDatosRentaExcenta(peticion);
			archivoZip = generarArchivoRentaExc(dtoSalida);

		}

		return archivoZip;
	}

	@Override
	public RespuestaArchivoPagoZipDTO generarArchivoRentaExc(List<RespuestaDatosCertificadosDTO> dtoSalida)
			throws IOException, XDocReportException, ArchivoNoValidoException {
		String rutaArchivo = servicioParametros.obtenerValorParametro(paramIdRutaAlmacenamientoArchivos);

		LocalDate fechaActual = LocalDate.now();
		String subCarpetaMesAno = obtenerSubCarpetaReporte();

		String rutaCertificadoRentaExcenta = servicioParametros
				.obtenerValorParametro(paramIdRutaCertificadoRentaExcenta);
		

		File file = new File(rutaCertificadoRentaExcenta);

		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		ZipOutputStream zipOut = new ZipOutputStream(fos);

		InputStream in = new FileInputStream(file);

		// Prepare the IXDocReport instance based on the template, using
		// Freemarker template engine
		IXDocReport report = null;
		report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Freemarker);

		Options options = Options.getTo(ConverterTypeTo.PDF).via(ConverterTypeVia.XWPF);
		// Add properties to the context
		for (RespuestaDatosCertificadosDTO c : dtoSalida) {
			String carpetaDestino = rutaArchivo + File.separator + c.getNombreNegocio().trim() + File.separator
					+ subCarpetaMesAno + File.separator + CERTIFICADOS + File.separator + "renta_excenta" + File.separator;
			File f = null;
			f = new File(carpetaDestino);
			if (!f.exists()) {
				boolean created = f.mkdirs();
				if (!created) {
					throw new ArchivoNoValidoException(errorCrearCarpeta);
				}
			}
			IContext ctx = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Negocio negocio = servicioNegocio.obtenerNegocio(c.getCodSFC());
			ctx = report.createContext();
			ctx.put(NOMBRENEGOCIO, c.getNombreNegocio());
			ctx.put(CODIGOSFC, c.getCodSFC());
			ctx.put(FECHACONSTITUCION, Utilidades.formatearFecha(negocio.getFechaConstitucion(), formatoddmmyyyy));
			ctx.put(MATRICULAINMOBILIARIA, negocio.getMatriculaInmobiliaria());
			ctx.put(DESCRIPCIONTIPOS, c.getDescripcionTipos());
			ctx.put(LICENCIACONSTRUCCION, negocio.getLicenciaConstruccion());
			UbicacionGeografica ubicacion = servicioUbicacionGeografica.obtenerUbicacionGeograficaPorCodigo(negocio.getCiudadUbicacion());
			ctx.put(CIUDADUBICACION, ubicacion != null ? ubicacion.getNombre() : "");
			ctx.put(NOMBRECOMPLETO, c.getNombreCompleto());
			ctx.put(ROLFIDUCIARIA, negocio.getCalidadFideicomitente());
			ctx.put(TIPODOCUMENTO, c.getTipoDocumento());
			ctx.put(NUMERODOCUMENTO, c.getNumeroDocumento());
			ctx.put(ANOGRABABLE, c.getAnoGrabable());
			ctx.put(VALORENLETRAS,utilidades.obtenerValorDelNumeroEnLetras(c.getBaseRetencion()));
			ctx.put(VALOR, new BigDecimal(c.getBaseRetencion()));
			ctx.put(DIAS, fechaActual.getDayOfMonth());
			ctx.put(MESENLETRAS, fechaActual.getMonth().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("es-ES")));
			ctx.put(ANO, fechaActual.getYear());
			// Write the PDF file to output stream, se hace un ciclo for por cada uno de los
			// registros
			report.convert(ctx, options, baos);
			String nombrePdf = c.getCodSFC() + "_" + c.getNombreNegocio() + "_RENTA_EXENTA" + "_"
					+ c.getTipoDocumento() + c.getNumeroDocumento() + ".pdf";
			nombrePdf = nombrePdf.replace(" ", "_");
			// se inserta el archivo en el pdf
			zipOut.putNextEntry(new ZipEntry(nombrePdf));
			zipOut.write(baos.toByteArray(), 0, baos.toByteArray().length);
			zipOut.closeEntry();
			// despues de insertado el archivo en el pdf se crea en la ruta de destino
			Path path = Paths.get(carpetaDestino + nombrePdf);
			Files.write(path, baos.toByteArray());
		}

		zipOut.close();

		RespuestaArchivoPagoZipDTO respuestaArchivo = new RespuestaArchivoPagoZipDTO();

		String archivoCodificado = Base64.getEncoder().encodeToString(fos.toByteArray());
		respuestaArchivo.setRecursoB64(archivoCodificado);
		respuestaArchivo.setNombreArchivoZip(subCarpetaMesAno + "_RENTA_EXENTA" + ".zip");
		respuestaArchivo.setExtension(EXTENSION_ZIP);
		respuestaArchivo.setTipoArchivo(CONTENT_TYPE);
		return respuestaArchivo;
	}

	@Override
	public RespuestaArchivoPagoZipDTO generarArchivoIngRet(List<RespuestaDatosCertificadosDTO> dtoSalida)
			throws IOException, XDocReportException, ArchivoNoValidoException {
		
		String rutaArchivo = servicioParametros.obtenerValorParametro(paramIdRutaAlmacenamientoArchivos);

		String subCarpetaMesAno = obtenerSubCarpetaReporte();

		String rutaCertificadoIngRet = servicioParametros
				.obtenerValorParametro(paramIdRutaCertificadoIngresosRetenciones);
		File file = new File(rutaCertificadoIngRet);

		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		ZipOutputStream zipOut = new ZipOutputStream(fos);

		InputStream in = new FileInputStream(file);

		IXDocReport report = null;
		report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Freemarker);

		Options options = Options.getTo(ConverterTypeTo.PDF).via(ConverterTypeVia.XWPF);		
		for (RespuestaDatosCertificadosDTO c : dtoSalida) {
			String carpetaDestino = rutaArchivo + File.separator + c.getNombreNegocio().trim() + File.separator + subCarpetaMesAno
					+ File.separator + CERTIFICADOS + File.separator + "ingresos_retenciones" + File.separator;
			File f = null;
			f = new File(carpetaDestino);
			if (!f.exists()) {
				boolean created = f.mkdirs();
				if (!created) {
					throw new ArchivoNoValidoException(errorCrearCarpeta);
				}
			}
			Negocio negocio = servicioNegocio.obtenerNegocio(c.getCodSFC());
			UbicacionGeografica ubicacion = servicioUbicacionGeografica.obtenerUbicacionGeograficaPorCodigo(negocio.getSeccionalAduanaCiudad());
			IContext ctx = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ctx = report.createContext();
			ctx.put(ANOGRABABLE, c.getAnoGrabable());
			ctx.put(CODIGOSFC, c.getCodSFC());
			ctx.put(CODIGOINTERNO, c.getCodigoInterno());
			ctx.put(NOMBRENEGOCIO, c.getNombreNegocio());
			ctx.put(NOMBRECOMPLETO, c.getNombreCompleto());
			ctx.put(NUMERODOCUMENTO, c.getNumeroDocumento());
			ctx.put(TIPODOCUMENTO, c.getTipoDocumento());
			ctx.put(PARTICIPACION, c.getParticipacion());
			ctx.put(BASERETENCION, c.getBaseRetencion());
			ctx.put(TOTALRETENIDO, c.getTotalRetenido());
			ctx.put(CIUDADADUANA, ubicacion != null ? ubicacion.getNombre() : "");
			ctx.put(FECHAGENERACION, c.getFechaGeneracion());
			ubicacion = servicioUbicacionGeografica.obtenerUbicacionGeograficaPorCodigo(negocio.getContastantesPago().getCodigoCiudad());
			ctx.put(CIUDADUBICACIONRET, ubicacion != null ? ubicacion.getNombre() : "");
			
			// Write the PDF file to output stream, se hace un ciclo for por cada uno de los
			// registros
			report.convert(ctx, options, baos);

			String nombrePdf = c.getCodSFC() + "_" + c.getNombreNegocio() + "_INGRESOS_RETENCIONES" + "_"
					+ c.getTipoDocumento() + c.getNumeroDocumento() + ".pdf";
			nombrePdf = nombrePdf.replace(" ", "_");
			// se inserta el archivo en el pdf
			zipOut.putNextEntry(new ZipEntry(nombrePdf));
			zipOut.write(baos.toByteArray(), 0, baos.toByteArray().length);
			zipOut.closeEntry();
			// despues de insertado el archivo en el pdf se crea en la ruta de destino
			Path path = Paths.get(carpetaDestino + nombrePdf);
			Files.write(path, baos.toByteArray());
		}

		zipOut.close();

		RespuestaArchivoPagoZipDTO respuestaArchivo = new RespuestaArchivoPagoZipDTO();

		String archivoCodificado = Base64.getEncoder().encodeToString(fos.toByteArray());
		respuestaArchivo.setRecursoB64(archivoCodificado);
		respuestaArchivo.setNombreArchivoZip(subCarpetaMesAno + "_INGRESOS_RETENCIONES" + ".zip");
		respuestaArchivo.setExtension(EXTENSION_ZIP);
		respuestaArchivo.setTipoArchivo(CONTENT_TYPE);
		return respuestaArchivo;
	}

	@Override
	// recibe todos los certificados a ser generados
	public RespuestaArchivoPagoZipDTO generarArchivoParticipacion(List<RespuestaDatosCertificadosDTO> dtoSalida)
			throws IOException, XDocReportException, ArchivoNoValidoException {
		
		String rutaArchivo = servicioParametros.obtenerValorParametro(paramIdRutaAlmacenamientoArchivos);

		String subCarpetaMesAno = obtenerSubCarpetaReporte();

		String rutaCertificadoParticipacion = servicioParametros
				.obtenerValorParametro(paramIdRutaCertificadoParticipacion);

		File file = new File(rutaCertificadoParticipacion);

		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		ZipOutputStream zipOut = new ZipOutputStream(fos);

		InputStream in = new FileInputStream(file);

		// Prepare the IXDocReport instance based on the template, using
		// Freemarker template engine
		IXDocReport report = null;
		report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Freemarker);

		Options options = Options.getTo(ConverterTypeTo.PDF).via(ConverterTypeVia.XWPF);
		// Add properties to the context
		for (RespuestaDatosCertificadosDTO c : dtoSalida) {
			String carpetaDestino = rutaArchivo + c.getNombreNegocio().trim() + File.separator + subCarpetaMesAno
					+ File.separator + CERTIFICADOS + File.separator + "porcentaje_participacion"+ File.separator;
			File f = null;
			f = new File(carpetaDestino);
			if (!f.exists()) {
				boolean created = f.mkdirs();
				if (!created) {
					throw new ArchivoNoValidoException(errorCrearCarpeta);
				}
			}
			IContext ctx = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ctx = report.createContext();

			ctx.put(CODIGOSFC, c.getCodSFC());
			ctx.put(CODIGOINTERNO, c.getCodigoInterno());
			ctx.put(NOMBRENEGOCIO, c.getNombreNegocio());
			ctx.put(NOMBRECOMPLETO, c.getNombreCompleto());
			ctx.put(TIPODOCUMENTO, c.getTipoDocumento() );
			ctx.put(NUMERODOCUMENTO, c.getNumeroDocumento() );
			ctx.put(NUMERODERECHOS, c.getNumeroDerechos());
			ctx.put(PARTICIPACION, c.getParticipacion());
			ctx.put(DIASMESLETRAS, c.getDiasMesLetras());
			ctx.put(NOMBREMESLETRAS, utilidades.obtieneNombreMes(Integer.parseInt(c.getNombreMesLetras())));
			ctx.put(ANOENLETRAS, c.getAnoEnLetras());
			ctx.put(RESPONSABLE, c.getResponsable());
			// Write the PDF file to output stream, se hace un ciclo for por cada uno de los
			// registros
			report.convert(ctx, options, baos);
			String nombrePdf = c.getCodSFC() + "_" + c.getNombreNegocio() + "_PARTICIPACIONES" + "_"
					+ c.getTipoDocumento() + c.getNumeroDocumento() + ".pdf";
			nombrePdf = nombrePdf.replace(" ", "_");
			// se inserta el archivo en el pdf
			zipOut.putNextEntry(new ZipEntry(nombrePdf));
			zipOut.write(baos.toByteArray(), 0, baos.toByteArray().length);
			zipOut.closeEntry();
			// despues de insertado el archivo en el pdf se crea en la ruta de destino
			Path path = Paths.get(carpetaDestino + nombrePdf);
			Files.write(path, baos.toByteArray());
		}

		zipOut.close();

		RespuestaArchivoPagoZipDTO respuestaArchivo = new RespuestaArchivoPagoZipDTO();

		String archivoCodificado = Base64.getEncoder().encodeToString(fos.toByteArray());
		respuestaArchivo.setRecursoB64(archivoCodificado);
		respuestaArchivo.setNombreArchivoZip(subCarpetaMesAno + "_PARTICIPACION" + ".zip");
		respuestaArchivo.setExtension(EXTENSION_ZIP);
		respuestaArchivo.setTipoArchivo(CONTENT_TYPE);
		return respuestaArchivo;

	}

	private String obtenerSubCarpetaReporte() {
		LocalDate fechaActual = LocalDate.now();
		return fechaActual.getYear() + File.separator + fechaActual.getMonth().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("es-ES"));
	}
	

	@Override
	public List<RespuestaDatosCertificadosDTO> obtenerDatosDelCertificado(PeticionTitularesNegociosDTO[] dto,
			String paramTipo, String tokenAutorizacion) throws BusquedadSinResultadosException {
		List<RespuestaDatosCertificadosDTO> dtoSalida = new ArrayList<>();
		if (paramTipo.equals(TIPO_PARTICIPACION)) {
			dtoSalida = obtenerDatosParticipacion(dto, tokenAutorizacion);
			
		} else if (paramTipo.equals(TIPO_INGRESOS_RETENCIONES)) {
			dtoSalida = obtenerDatosIngresosRetenciones(dto);
			
		} else if (paramTipo.equals(TIPO_RENTA_EXCENTA)) {
			dtoSalida = obtenerDatosRentaExcenta(dto);
			
		}
		return dtoSalida;
	}

	/**
	 * metodo que busca en la base de datos los capos para el certificado de renta excenta
	 * @param dto
	 * @param tokenAutorizacion
	 * @return dto con la informacion de renta excenta
	 */
	private List<RespuestaDatosCertificadosDTO> obtenerDatosRentaExcenta(PeticionTitularesNegociosDTO[] dto) {
		String descripcionRetefuente = servicioParametros.obtenerValorParametro(paramIdRetenFuente);

		int idRetefuente = servicioDominio
				.buscarPorCodigoDominioYDescripcion(dominioRetenciones, descripcionRetefuente, Boolean.TRUE)
				.getIdValorDominio();

		String anoGrabable = dto[0].getAnoGrabable();
		List<RespuestaDatosCertificadosDTO> dtoSalida = new ArrayList<>();

		for (PeticionTitularesNegociosDTO reg : dto) {

			List<DistribucionRetencionesTitular> rta = obtenerDatosDistribuciones(reg);

			Map<String, List<DistribucionRetencionesTitular>> titulares = agruparDistribucionRetencionTitularPorTitular(
					rta);

			titulares.forEach((documento, distribucionesRetencionTitular) -> {
				BigDecimal baseRetencion = new BigDecimal("0");
				RespuestaDatosCertificadosDTO dtoTemp = new RespuestaDatosCertificadosDTO();
				if (distribucionesRetencionTitular.stream().noneMatch(
						drt -> drt.getDistribucionRetencionesTitularPK().getIdRetencion().equals(idRetefuente))) {
					DistribucionRetencionesTitular d = null;
					for (DistribucionRetencionesTitular dst : distribucionesRetencionTitular) {
						baseRetencion = baseRetencion.add(dst.getValorBase());
						d = dst;
					}
					String codSFC = d.getDistribucionRetencionesTitularPK().getRetTitularCodSfc();
					String tipoDoc = d.getDistribucionRetencionesTitularPK().getRetTipoDocTitular();
					String numDoc = d.getDistribucionRetencionesTitularPK().getRetNumDocTitular();
					String llave = "{" + codSFC + "}";

					String fechaConstitucion = obtenerDatosDeTabla(codSFC, llave, NEGNEGOCIO, FECHA_CONSTITUCION,
							utilidades.convertirFecha(d.getDistribucionRetencionesTitularPK().getPeriodo(),
									formatoddmmyyyy));

					String matriculaInmobiliaria = obtenerDatosDeTabla(codSFC, llave, NEGNEGOCIO,
							MATRICULA_INMOBILIARIA, utilidades.convertirFecha(
									d.getDistribucionRetencionesTitularPK().getPeriodo(), formatoddmmyyyy));

					String licenciaConstruccion = obtenerDatosDeTabla(codSFC, llave, NEGNEGOCIO, LICENCIA_CONSTRUCCION,
							utilidades.convertirFecha(d.getDistribucionRetencionesTitularPK().getPeriodo(),
									formatoddmmyyyy));

					String rolFiduciaria = obtenerDatosDeTabla(codSFC, llave, NEGNEGOCIO, ROL_FIDUCIARIA, utilidades
							.convertirFecha(d.getDistribucionRetencionesTitularPK().getPeriodo(), formatoddmmyyyy));

					String ciudadUbicacion = obtenerDatosDeTabla(codSFC, llave, NEGNEGOCIO, CIUDAD_UBICACION, utilidades
							.convertirFecha(d.getDistribucionRetencionesTitularPK().getPeriodo(), formatoddmmyyyy));

					String descripcionTipos = obtenerDescipcionTiposNegocio(codSFC, anoGrabable);
					dtoTemp.setNombreNegocio(d.getRetencionTitular().getTitular().getNegocio().getNombre());
					dtoTemp.setCodSFC(codSFC);
					dtoTemp.setFechaConstitucion(fechaConstitucion);
					dtoTemp.setMatriculaInmobiliaria(matriculaInmobiliaria);
					dtoTemp.setLicenciaConstruccion(licenciaConstruccion);
					dtoTemp.setCiudadUbicacion(ciudadUbicacion);
					dtoTemp.setNombreCompleto(d.getRetencionTitular().getTitular().getPersona().getNombreCompleto());
					dtoTemp.setRolFiduciaria(rolFiduciaria);
					dtoTemp.setTipoDocumento(tipoDoc);
					dtoTemp.setNumeroDocumento(numDoc);
					dtoTemp.setAnoGrabable(anoGrabable);
					dtoTemp.setRentaExcenta(baseRetencion.toString());
					dtoTemp.setBaseRetencion(baseRetencion.toString());
					dtoTemp.setDescripcionTipos(descripcionTipos);
					dtoSalida.add(dtoTemp);
				}
			});
		}
		return dtoSalida;
	}

	
	/**
	 * dado un conjunto de datos de titulares y negocios obtiene los datos de sus ingresos y retencionesz 
	 * @param dto
	 * @param tokenAutorizacion
	 * @return List<RespuestaDatosCertificadosDTO>
	 */
	private List<RespuestaDatosCertificadosDTO> obtenerDatosIngresosRetenciones(PeticionTitularesNegociosDTO[] dto) {

		String anoGrabable = dto[0].getAnoGrabable();
		List<RespuestaDatosCertificadosDTO> dtoSalida = new ArrayList<>();
		LocalDate fechaActual = LocalDate.now();
		String dia = String.valueOf(fechaActual.getDayOfMonth());
		String mes = String.valueOf(fechaActual.getMonthValue());
		String ano = String.valueOf(fechaActual.getYear());
		String fechaGeneracion = dia + "/" + mes + "/" + ano;

		String descripcionRetefuente = servicioParametros.obtenerValorParametro(paramIdRetenFuente);
		ValorDominioNegocio valorDominioRetefuente = servicioDominio
				.buscarPorCodigoDominioYDescripcion(dominioRetenciones, descripcionRetefuente, Boolean.TRUE);

		for (PeticionTitularesNegociosDTO reg : dto) {

			reg.setIdRetencion(valorDominioRetefuente.getIdValorDominio());
			List<DistribucionRetencionesTitular> rta = obtenerDatosDistribuciones(reg);
			Map<String, List<DistribucionRetencionesTitular>> titulares = agruparDistribucionRetencionTitularPorTitular(
					rta);

			titulares.forEach((documento, distribucionesRetencionTitular) -> {
				BigDecimal baseRetencion = new BigDecimal("0");
				BigDecimal totalRetenido = new BigDecimal("0");
				DistribucionRetencionesTitular d = null;
				for (DistribucionRetencionesTitular dst : distribucionesRetencionTitular) {
					baseRetencion = baseRetencion.add(dst.getValorBase());
					totalRetenido = totalRetenido.add(dst.getValorImpuesto());
					d = dst;
				}
				String codSFC = d.getDistribucionRetencionesTitularPK().getRetTitularCodSfc();
				String tipoDoc = d.getDistribucionRetencionesTitularPK().getRetTipoDocTitular();
				String numDoc = d.getDistribucionRetencionesTitularPK().getRetNumDocTitular();
				String llave = "{" + codSFC + "}";
				Date periodo = obtenerPeriodo(anoGrabable);
				String ciudadAduana = obtenerDatosDeTabla(codSFC, llave, NEGNEGOCIO, SECCIONAL_CIUDAD_ADUANA, periodo);
				String ciudadUbicacion = obtenerDatosDeTabla(codSFC, llave, NEGNEGOCIO, CIUDAD_UBICACION, periodo);
				RespuestaDatosCertificadosDTO dtoTemp = new RespuestaDatosCertificadosDTO();
				dtoTemp.setNombreNegocio(d.getRetencionTitular().getTitular().getNegocio().getNombre());
				dtoTemp.setAnoGrabable(anoGrabable);
				dtoTemp.setNombreCompleto(d.getRetencionTitular().getTitular().getPersona().getNombreCompleto());
				dtoTemp.setTipoDocumento(tipoDoc);
				dtoTemp.setNumeroDocumento(numDoc);
				dtoTemp.setBaseRetencion(utilidades.formatearBigdecimalDosDecimales(baseRetencion));
				dtoTemp.setParticipacion(valorDominioRetefuente.getValor().replace(".", ","));
				dtoTemp.setTotalRetenido(utilidades.formatearBigdecimalDosDecimales(totalRetenido));
				dtoTemp.setCiudadAduana(ciudadAduana);
				dtoTemp.setCiudadUbicacion(ciudadUbicacion);
				dtoTemp.setFechaGeneracion(fechaGeneracion);
				dtoTemp.setCodSFC(codSFC);
				dtoSalida.add(dtoTemp);
			});
		}

		return dtoSalida;
	}

	private Map<String, List<DistribucionRetencionesTitular>> agruparDistribucionRetencionTitularPorTitular(
			List<DistribucionRetencionesTitular> distribucionRetencionTitular) {
		Map<String, List<DistribucionRetencionesTitular>> titulares = new HashMap<>();
		
		distribucionRetencionTitular.forEach(drt -> {
			if (titulares.get(drt.getDistribucionRetencionesTitularPK().getRetNumDocTitular()) == null) {
				titulares.put(drt.getDistribucionRetencionesTitularPK().getRetNumDocTitular(), new ArrayList<>());
			}
			titulares.get(drt.getDistribucionRetencionesTitularPK().getRetNumDocTitular()).add(drt);
		});
		return titulares;
	}

	/**
	 * Dada una lista de datos de titular y su negocio devuelve una lista con su informacion de participacion
	 * @param  PeticionTitularesNegociosDTO
	 * @param tokenAutorizacion
	 * @return ist<RespuestaDatosCertificadosDTO>
	 * @throws BusquedadSinResultadosException
	 */
	private List<RespuestaDatosCertificadosDTO> obtenerDatosParticipacion(PeticionTitularesNegociosDTO[] dto,
			String tokenAutorizacion) throws BusquedadSinResultadosException {
		String anoGrabable = dto[0].getAnoGrabable();
		List<RespuestaDatosCertificadosDTO> dtoSalida = new ArrayList<>();
		for (PeticionTitularesNegociosDTO reg : dto) {
			Titular titular = servicioTitular.buscarTitular(reg.getTipoDocumento(), reg.getNumeroDocumento(), reg.getCodigoNegocio());
			
			RespuestaDatosCertificadosDTO dtoTemp = new RespuestaDatosCertificadosDTO();
			String codSFC = reg.getCodigoNegocio();
			String tipoDoc = reg.getTipoDocumento();
			String numDoc = reg.getNumeroDocumento();
			String llave = "{" + codSFC + "}{" + tipoDoc + "}{" + numDoc + "}";
			Date periodo = obtenerPeriodo(reg.getAnoGrabable());
			String participacion = obtenerDatosDeTabla(codSFC, llave, NEG_TITULAR, PORCENTAJE, periodo);
			String numeroDerechos = obtenerDatosDeTabla(codSFC, llave, NEG_TITULAR, NUMERO_DERECHOS, periodo);
			String codigoInterno = servicioNegocio.buscarNegocioPorCodigo(codSFC, tokenAutorizacion).getCodigo();
			//se obtiene el responsable del negocio
			
			List<DatosEquipoResponsableDTO> responsables = servicioNegocio.cargarEquipoResponsable(codSFC, tokenAutorizacion);
			responsables = responsables
									.stream().filter(r ->r.getRol() != null && r.getRol().equalsIgnoreCase(ADMINISTRADOR)).collect(Collectors.toList());
			String responsable="";
			if (responsables.isEmpty()) {
				throw new BusquedadSinResultadosException(noAdministrador);
			}else {
			responsable= responsables.get(0).getNombreCompleto();
			
			}
			String nombreNegocio = titular.getNegocio().getNombre();
			String nombreCompleto = titular.getPersona().getNombreCompleto();
			LocalDate fechaActual = LocalDate.now();
			String dia = String.valueOf(fechaActual.getDayOfMonth());
			String mes = String.valueOf(fechaActual.getMonthValue());
			String ano = String.valueOf(fechaActual.getYear());

			// se setean los valores de los parametros
			dtoTemp.setCodSFC(codSFC);
			dtoTemp.setCodigoInterno(codigoInterno);
			dtoTemp.setNombreNegocio(nombreNegocio);
			dtoTemp.setNombreCompleto(nombreCompleto);
			dtoTemp.setNumeroDerechos(numeroDerechos);
			dtoTemp.setParticipacion(utilidades.formatearParticipacion(participacion));
			dtoTemp.setTipoDocumento(tipoDoc);
			dtoTemp.setNumeroDocumento(numDoc);
			dtoTemp.setAnoGrabable(anoGrabable);
			dtoTemp.setAnoEnLetras(ano);
			dtoTemp.setDiasMesLetras(dia);
			dtoTemp.setNombreMesLetras(mes);
			dtoTemp.setResponsable(responsable);
			dtoSalida.add(dtoTemp);
		}

		return dtoSalida;
	}

	private Date obtenerPeriodo(String anoGrabable) {
		Calendar calendar = Calendar.getInstance();
		Calendar actual = Calendar.getInstance();
		actual.setTime(new Date());
		calendar.setTime(utilidades.convertirFecha(anoGrabable, "yyyy"));
		int anioPeriodo = calendar.get(Calendar.YEAR);
		int anioActual = actual.get(Calendar.YEAR);
		
		if(anioPeriodo < anioActual) {
			calendar.set(Calendar.MONTH,11);
			calendar.set(Calendar.DAY_OF_MONTH,31);
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			return calendar.getTime();
		} else {
			actual.set(Calendar.HOUR_OF_DAY, 23);
			actual.set(Calendar.MINUTE, 59);
			actual.set(Calendar.SECOND, 59);
			return actual.getTime();
		}
	}

	/**
	 * metodo para hayar los registros de la tabla distri reten ttular que coinciden exacteamente con el titular y el 
	 * negocio
	 * @param dto
	 * @return  List<DistribucionRetencionesTitular>
	 */
	private List<DistribucionRetencionesTitular> obtenerDatosDistribuciones(PeticionTitularesNegociosDTO dto) {
		Specification<DistribucionRetencionesTitular> especificacionCombinada = Specification
				.where(dto.getAnoGrabable() == null || dto.getAnoGrabable().isEmpty() ? null
						: DistribucionRetencionesTitularEspecificacion.filtrarPorPeriodo(dto.getAnoGrabable()))
				 .and(dto.getNombreTitular() == null || dto.getNombreTitular().isEmpty() ? null
						 : DistribucionRetencionesTitularEspecificacion.filtrarPorLikeNombre(dto.getNombreTitular()))
				.and(dto.getCodigoNegocio() == null || dto.getCodigoNegocio().isEmpty() ? null
						: DistribucionRetencionesTitularEspecificacion.filtroEqualPorCodSFC(dto.getCodigoNegocio()))
				.and(dto.getTipoDocumento() == null || dto.getTipoDocumento().trim().isEmpty() ? null
						: DistribucionRetencionesTitularEspecificacion
								.filtroEqualPorTipoNumeroDocumento(dto.getTipoDocumento(), dto.getNumeroDocumento()))
				.and((dto.getTipoDocumento() == null || dto.getTipoDocumento().trim().isEmpty()) &&
						(dto.getNumeroDocumento() != null && !dto.getNumeroDocumento().isEmpty()) ? 
								DistribucionRetencionesTitularEspecificacion.filtrarLikePorNumeroDocumento(dto.getNumeroDocumento())
						: null)
				.and(dto.getNombreNegocio() == null || dto.getNombreNegocio().isEmpty() ? null
						 : DistribucionRetencionesTitularEspecificacion.filtrarLikePorNombreNegocio(dto.getNombreNegocio()))
				.and(dto.getIdRetencion() == null ? null : 
								DistribucionRetencionesTitularEspecificacion.filtrarPorRetencion(dto.getIdRetencion()))
				.and(DistribucionRetencionesTitularEspecificacion.filtrarPorEstadoDistribucion(ESTADO_DISTRIBUIDO));
		
		return repositorioDistribucionRetencionesTitular.findAll(especificacionCombinada);
	}

	
	/**
	 *  metodo usado para encontrar el valor mas actual de los campos de la tabla
	 *  requerida y el campo requerido hasta el periodo dado
	 * @param codNegocio
	 * @param llave
	 * @param tabla
	 * @param columna
	 * @param periodo
	 * @return
	 */
	private String obtenerDatosDeTabla(String codNegocio, String llave, String tabla, String columna, Date periodo) {
		Specification<AudHistorial> especificacionCombinada = Specification
				.where(AudHistorialEspecificacion.filtrarPorNegocios(Arrays.asList(codNegocio)))
				.and(AudHistorialEspecificacion
						.filtrarPorFechaFinal(periodo))
				.and(AudHistorialEspecificacion.filtrarPorTabla(Arrays.asList(tabla)))
				.and(AudHistorialEspecificacion.filtrarPorColumna(Arrays.asList(columna)))
				.and(AudHistorialEspecificacion.filtrarPorLLave(llave))

				.and(AudHistorialEspecificacion.filtrarPorOperacion(Arrays.asList("INSERT", "UPDATE")));
		List<AudHistorial> listaRes = repositorioAudHistorial.findAll(especificacionCombinada);

		// despues de organizarlos se toma el ultimo el mas reciente
		listaRes = listaRes.stream()
				.sorted((r1, r2 )->r1.getAudHistorialPK().getFecha()
				.compareTo(r2.getAudHistorialPK().getFecha()))
				.collect(Collectors.toList());
		String respuesta ="";
		if (!listaRes.isEmpty()) {
		AudHistorialDetalle detalle = listaRes.get(0).getHistorialDetalles().stream()
				.filter(d -> d.getAudHistoriaDetallePK().getCampo().equals(columna))// se filtran por los detalles que
																					// son de la columna buscada
				.collect(Collectors.toList()).get(0);// se toma el cero ya que debe devolver un solo registro
		// devovlvemos el dato nuevo resultado del intert o update
		 respuesta = detalle.getDatoNuevo();
	}
		
		return respuesta;
	}
	
	
	/**
	 * metodo que consulta los tipos que tenia el negocio al anorabable para el certificado de renta excenta
	 * @param codSFC
	 * @param anoGrabable
	 * @return
	 */
	public String  obtenerDescipcionTiposNegocio(String codSFC, String anoGrabable) {
		//se construye la fecha para obtener los tips de negocio del año grabable
		Date periodoFecha = utilidades.construirFecha(Integer.parseInt(anoGrabable), 12, 31);
		
		HashMap<String, List<String>> hashMaplistaTipos = servicioAudHistorial.obtenerTiposDelNegocio(null, codSFC, periodoFecha);
		//como es un hashmap se obtiene el valor para el codigo de negocio
		StringBuilder desTipos = new StringBuilder();
		if (!hashMaplistaTipos.isEmpty()) {
		
			List<String> listaTipos =hashMaplistaTipos.get(codSFC);
		
		for( String v : listaTipos) {
			desTipos= desTipos.append(repositorioValorDominioNegocio.
					buscarValorDominioPorCodDominioYValorDominio(dominioTipoNegocio,
							Integer.parseInt(v)).getDescripcion() +", ");
		}
		}
		return desTipos.toString();
	}

}
