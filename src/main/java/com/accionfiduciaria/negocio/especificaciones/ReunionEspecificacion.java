package com.accionfiduciaria.negocio.especificaciones;


import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.Decision;
import com.accionfiduciaria.modelo.entidad.Reunion;

public class ReunionEspecificacion {
	
	private static final String REUNION_PK = "reunionPk";
	private static final String FECHA = "fecha";

	public static Specification<Reunion> filtrarPorTipoReunion(String tipoReunion) {
		return (root, query, cb) -> 
			 cb.like(root.get("tipo"),tipoReunion);
	}
	
	public static Specification<Reunion> filtrarPorNegocio(String codigoNegocio) {
		return (root, query, cb) -> 
			 cb.equal(root.get(REUNION_PK).get("codigoSfc"),codigoNegocio);
	}
	
	public static Specification<Reunion> filtrarPorDecision(String palabraClave) {
		return new Specification<Reunion>() {
			private static final long serialVersionUID = 1L;
			@Override
			public Predicate toPredicate(Root<Reunion> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Join<Reunion, Decision> descisiones = root.join("decisiones");
				return cb.and(cb.like(descisiones.get("descripcion"), "%"+palabraClave+"%"), 
					   cb.equal(descisiones.get("estado"), 1)); 
			}
		};
	}
	
	public static Specification<Reunion> filtrarPorPendientes(String palabraClave) {
		return new Specification<Reunion>() {
			private static final long serialVersionUID = 1L;
			@Override
			public Predicate toPredicate(Root<Reunion> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Join<Reunion, Decision> pendientes = root.join("decisiones");
				return cb.and(cb.like(pendientes.get("descripcion"), "%"+palabraClave+"%"), 
					   cb.equal(pendientes.get("estado"), 0)); 
			}
		};
	}
	
	public static Specification<Reunion> filtrarPorFechaInicial(LocalDateTime fechaInicial) {
		return (root, query, cb) -> 
			 cb.greaterThanOrEqualTo(root.get(REUNION_PK).get(FECHA), fechaInicial);
	}

	public static Specification<Reunion> filtrarPorFechaFinal(LocalDateTime fechaFinal) {
		return (root, query, cb) -> {
			query.orderBy(cb.asc(root.get(REUNION_PK).get(FECHA) ));
			return cb.lessThanOrEqualTo(root.get(REUNION_PK).get(FECHA), fechaFinal);
		};
	}
	
	public static Specification<Reunion> filtrarPorRadicado(String numeroRadicado) {
		return (root, query, cb) -> 
			 cb.like(root.get("idActa"), "%"+numeroRadicado+"%");
	}
	
	public static Specification<Reunion> filtrarPorUsuario(
			List<String> solicitadoPor) {		
		return (root, query, cb) -> 
			 cb.concat(root.get("tipoDocumentoUsuario"), 
					root.get("numeroDocumentoUsuario")).in(solicitadoPor);
	}

}
