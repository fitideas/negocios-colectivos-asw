package com.accionfiduciaria.negocio.especificaciones;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.AudHistorial;
import com.accionfiduciaria.modelo.entidad.AudHistorialDetalle;
import com.accionfiduciaria.modelo.entidad.Distribucion;


public class DistribucionEspecificacion {

	private static final String DISTRIBUCION_PK = "distribucionPK";
	
	private DistribucionEspecificacion() {
	}
	
	public static Specification<Distribucion> filtrarPorUsuario(List<String> usuario) {
		return (root, query, cb) -> {
			return root.get("usuario").in(usuario);
		};
	}

	

	public static Specification<Distribucion> filtrarPorFechaInicial(Date fechaInicial) {
		return (root, query, cb) -> {
			return cb.greaterThanOrEqualTo(
					cb.function("TO_DATE", 
							Date.class,
							root.get(DISTRIBUCION_PK).get("periodo"),  cb.literal("dd/mm/yyyy") 
							), fechaInicial);
		};
	}

	public static Specification<Distribucion> filtrarPorFechaFinal(Date fechaFinal) {
		return (root, query, cb) -> {
			return cb.lessThanOrEqualTo(cb.function("TO_DATE", 
					Date.class,
					root.get(DISTRIBUCION_PK).get("periodo"),  cb.literal("dd/mm/yyyy") 
					), fechaFinal);
		};
	}

	public static Specification<Distribucion> filtrarPorTipoNegocio(List<String> tipos) {
		
		return (root, query, cb) -> {
			return root.get(DISTRIBUCION_PK).get("tipoNegocio").in(tipos);
		};
	}

	public static Specification<Distribucion> filtrarPorNegocios(List<String> negocios) {
		return (root, query, cb) -> {
			return root.get(DISTRIBUCION_PK).get("codSfc").in(negocios);
		};
	}
	
	public static Specification<AudHistorial> filtrarPorTabla(String tabla) {
		return (root, query, cb)->{
			return cb.equal(root.get(DISTRIBUCION_PK).get("nombreTabla"), tabla);
		};
	}
	
	public static Specification<AudHistorial>  filtrarPorColumna(String columna) {
	
		return new Specification<AudHistorial>() {
			private static final long serialVersionUID = 1L;
	

			@Override
			public Predicate toPredicate(Root<AudHistorial> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<AudHistorial, AudHistorialDetalle> detalle = root.join("historialDetalles");

				return criteriaBuilder.equal(detalle.get("campo"), columna);
			}

		};
	}

	
	
}


