package com.accionfiduciaria.negocio.especificaciones;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;

public class PagoNegocioEspecificaciones {
	
	private static final String PAGO_NEGOCIO_PK = "pagoNegocioPK";
	
	private PagoNegocioEspecificaciones() {
	}
	
	public static Specification<PagoNegocio> filtrarPorNegocio(String codigoSFC){
		return (root, query, cb) ->
			 cb.equal(root.get(PAGO_NEGOCIO_PK).get("codSfc"), codigoSFC);
	}
	
	public static Specification<PagoNegocio> filtrarPorEstadoDistribucion(String estado){
		return new Specification<PagoNegocio>() {
			private static final long serialVersionUID = 1L;
			@Override
			public Predicate toPredicate(Root<PagoNegocio> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<PagoNegocio, Distribucion> distribucion = root.join("distribuciones");
				return criteriaBuilder.equal(distribucion.get("estado"), estado);
			}
		};
	}
	
	public static Specification<PagoNegocio> filtrarPorPeriodo(List<String> periodo){
		return (root, query, cb) ->
			root.get(PAGO_NEGOCIO_PK).get("periodo").in(periodo);
	}
	
	public static Specification<PagoNegocio> filtrarPorTipoNegocio(List<String> tiposNegocio){
		return (root, query, cb) -> 
			root.get(PAGO_NEGOCIO_PK).get("tipoNegocio").in(tiposNegocio);
	}
	
	public static Specification<PagoNegocio> filtrarPorEncargo(String encargo){
		return (root, query, cb) ->
			cb.equal(root.get("cuenta"), encargo);
	}
}
