package com.accionfiduciaria.negocio.especificaciones;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitular;
import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.Titular;

public class DistribucionRetencionesTitularEspecificacion {
	private static final String DISTRIRETENCIONESTITULARPK = "distribucionRetencionesTitularPK";
	private static final String RETTITULARPK = "retTitularCodSfc";
	private static final String RETNUMDOCTITULAR = "retNumDocTitular";
	private static final String RETTIPDOCTITULAR = "retTipoDocTitular";
	private static final String PERIODO = "periodo";
	private static final String RETENCIONTITULAR = "retencionTitular";
	private static final String TITULAR = "titular";
	private static final String NEGOCIO = "negocio";
	private static final String NOMBRE = "nombre";
	private static final String PERSONA = "persona";
	private static final String NOMBRECOMPLETO = "nombreCompleto";
	private static final String IDRETENCION = "idRetencion";
	private static final String DISTRIBUCION = "distribucion";
	private static final String ESTADO = "estado";


	private DistribucionRetencionesTitularEspecificacion() {
		
	}
	
	public static Specification<DistribucionRetencionesTitular> filtrarLikePorCodSFC(String codNegocio){
		return (root, query, cb) ->
		 cb.like(root.get(DISTRIRETENCIONESTITULARPK).get(RETTITULARPK), "%"+ codNegocio+"%");
	}
	
	public static Specification<DistribucionRetencionesTitular> filtroEqualPorCodSFC(String codNegocio){
		return (root, query, cb) ->
		 cb.equal(root.get(DISTRIRETENCIONESTITULARPK).get(RETTITULARPK),  codNegocio);
	}

	

	public static Specification<DistribucionRetencionesTitular> filtrarLikePorNumeroDocumento(String numDocumento){
		return (root, query, cb) ->
		 cb.like(root.get(DISTRIRETENCIONESTITULARPK).get(RETNUMDOCTITULAR), "%"+ numDocumento+"%"); 
	}
	
	public static Specification<DistribucionRetencionesTitular> filtrarPorPeriodo(String periodo){
		return (root, query, cb) ->
		 cb.equal(cb.substring(root.get(DISTRIRETENCIONESTITULARPK).get(PERIODO), 7,4), periodo); 
	}

	public static Specification<DistribucionRetencionesTitular> filtroEqualPorTipoNumeroDocumento(String tipoDocumento, String numeroDocumento) {
		return (root, query, cb) ->
		 cb.and(cb.equal(root.get(DISTRIRETENCIONESTITULARPK).get(RETTIPDOCTITULAR),  tipoDocumento),
				 cb.equal(root.get(DISTRIRETENCIONESTITULARPK).get(RETNUMDOCTITULAR),  numeroDocumento));
	}

	public static Specification<DistribucionRetencionesTitular> filtrarPorLikeNombre(String nombreTitular) {
		
		return new Specification<DistribucionRetencionesTitular>() {
			private static final long serialVersionUID = 1L;
	
		
			@Override
			public Predicate toPredicate(Root<DistribucionRetencionesTitular> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<DistribucionRetencionesTitular, RetencionTitular> retencionTitular = root.join(RETENCIONTITULAR);
				Join<RetencionTitular, Titular> titular = retencionTitular
						.join(TITULAR);
				Join<Titular, Persona> persona = titular
						.join(PERSONA);
				return criteriaBuilder.like(criteriaBuilder.upper(persona.get(NOMBRECOMPLETO)),
						"%"+ nombreTitular.toUpperCase()+"%"); 

			}

		};
	}

	public static Specification<DistribucionRetencionesTitular> filtrarLikePorNombreNegocio(String nombreNegocio) {
		return new Specification<DistribucionRetencionesTitular>() {
			private static final long serialVersionUID = 1L;
	
			@Override
			public Predicate toPredicate(Root<DistribucionRetencionesTitular> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<DistribucionRetencionesTitular, RetencionTitular> retencionTitular = root.join(RETENCIONTITULAR);
				Join<RetencionTitular, Titular> titular = retencionTitular
						.join(TITULAR);
				Join<Titular, Negocio> negocio = titular
						.join(NEGOCIO);
				return criteriaBuilder.like(criteriaBuilder.upper(negocio.get(NOMBRE)),
						"%"+ nombreNegocio.toUpperCase()+"%"); 

			}

		};
	}

	public static Specification<DistribucionRetencionesTitular> filtrarPorRetencion(Integer idRetencion) {
		return (root, query, cb) ->
		 cb.equal(root.get(DISTRIRETENCIONESTITULARPK).get(IDRETENCION), idRetencion); 
	}

	public static Specification<DistribucionRetencionesTitular> filtrarPorEstadoDistribucion(String estado) {
		return (root, query, cb) ->
		cb.equal(root.get(DISTRIBUCION).get(ESTADO), estado);
	}

}
