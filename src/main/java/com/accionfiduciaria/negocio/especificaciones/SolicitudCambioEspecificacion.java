package com.accionfiduciaria.negocio.especificaciones;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.SolicitudCambiosDatos;

public class SolicitudCambioEspecificacion {
	
	private static final String PERSONA_USUARIO = "personaUsuario";
	private static final String PERSONA_PK = "personaPK";
	private static final String PERSONA_VINCULADO = "personaVinculado";
	private static final String TIPO_DOCUMENTO = "tipoDocumento";
	private static final String NUMERO_DOCUMENTO = "numeroDocumento";
	
	private SolicitudCambioEspecificacion() {
		
	}
	
	public static Specification<SolicitudCambiosDatos> filtrarPorTipoDocumentoUsuario(
			String tipoDocumento) {
		return (root, query, cb) -> 
			 cb.equal(root.get(PERSONA_USUARIO).get(PERSONA_PK).get(TIPO_DOCUMENTO),tipoDocumento);	
	}
	
	public static Specification<SolicitudCambiosDatos> filtrarPorNumeroDocumentoUsuario(
			String numeroDocumento) {
		return (root, query, cb) -> 
			 cb.equal(root.get(PERSONA_USUARIO).get(PERSONA_PK).get(NUMERO_DOCUMENTO),numeroDocumento);
		
	}
	
	public static Specification<SolicitudCambiosDatos> filtrarPorTipoDocumentoVinculado(
			String tipoDocumento) {
		return (root, query, cb) -> 
			 cb.equal(root.get(PERSONA_VINCULADO).get(PERSONA_PK).get(TIPO_DOCUMENTO),tipoDocumento);	
	}
	
	public static Specification<SolicitudCambiosDatos> filtrarPorNumeroDocumentoVinculado(
			String numeroDocumento) {
		return (root, query, cb) -> 
			 cb.equal(root.get(PERSONA_VINCULADO).get(PERSONA_PK).get(NUMERO_DOCUMENTO),numeroDocumento);
		
	}
	
	public static Specification<SolicitudCambiosDatos> filtrarPorTipoNumeroDocumentoUsuario(
			List<String> solicitadoPor) {		
		return (root, query, cb) -> 
			 cb.concat(root.get(PERSONA_USUARIO).get(PERSONA_PK).get(TIPO_DOCUMENTO), root.get(PERSONA_USUARIO).get(PERSONA_PK).get(NUMERO_DOCUMENTO)).in(solicitadoPor);
	}

	public static Specification<SolicitudCambiosDatos> filtrarPorTipoNumeroDocumentoVinculado(
			List<String> solicitadoPor) {		
		return (root, query, cb) -> 
			 cb.concat(root.get(PERSONA_VINCULADO).get(PERSONA_PK).get(TIPO_DOCUMENTO), root.get(PERSONA_VINCULADO).get(PERSONA_PK).get(NUMERO_DOCUMENTO)).in(solicitadoPor);
	}


	public static Specification<SolicitudCambiosDatos> filtrarPorFechaInicial(Date fechaInicial) {
		return (root, query, cb) -> 
			 cb.greaterThanOrEqualTo(root.get("fechaSolicitud"), fechaInicial);
	}

	public static Specification<SolicitudCambiosDatos> filtrarPorFechaFinal(Date fechaFinal) {
		return (root, query, cb) -> 
			 cb.lessThanOrEqualTo(root.get("fechaSolicitud"), fechaFinal);
	}
	
	public static Specification<SolicitudCambiosDatos>  filtrarPorColumna(String columna) {
		return (root, query, cb) -> 
		 cb.like(root.get("campoModificado"),columna);
	}
}
