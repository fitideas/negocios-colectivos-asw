package com.accionfiduciaria.negocio.especificaciones;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.AudHistorial;
import com.accionfiduciaria.modelo.entidad.AudHistorialDetalle;

public class AudHistorialEspecificacion {
	private static final String USUARIO = "usuario";
	private static final String TIPODOCUMENTOUSUARIO = "tipoDocumentoUsuario";
	private static final String NUMERODOCUMENTOUSUARIO = "numeroDocumentoUsuario";
	private static final String AUDHISTORIALPK = "audHistorialPK";
	private static final String FECHA = "fecha";
	private static final String CODSFC = "codSFC";
	private static final String CAMPO = "campo";
	private static final String NOMBRETABLA = "nombreTabla";
	private static final String ESTADO = "estado";
	private static final String HISTORIALDETALLES = "historialDetalles";
	private static final String AUDHISTORIALDETALLESK = "audHistoriaDetallePK";
	private static final String LLAVE = "llave";
	private static final String DATONUEVO = "datoNuevo";
	private static final String OPERACION = "operacion";

	private AudHistorialEspecificacion() {
		
	}
	public static Specification<AudHistorial> filtrarPorUsuario(List<String> usuario) {
		return (root, query, cb) -> {
			return root.get(USUARIO).in(usuario);
		};
	}

	public static Specification<AudHistorial> filtrarPorTipoDocumento(
			String tipoDocumento) {
		return (root, query, cb) -> {
			return cb.equal(root.get(TIPODOCUMENTOUSUARIO),
					tipoDocumento);
		};
	}
	
	public static Specification<AudHistorial> filtrarPorNumeroDocumento(
			String numeroDocumento) {
		return (root, query, cb) -> {
			return cb.equal(root.get(NUMERODOCUMENTOUSUARIO),
					numeroDocumento);
		};
	}
	
	public static Specification<AudHistorial> filtrarPorMaximaFecha() {
		return (root, query, cb) -> {
			return root.get(AUDHISTORIALPK).in(query.
					multiselect(
							root.get(AUDHISTORIALPK), 
							cb.max(root.get(AUDHISTORIALPK).get(FECHA)))
					.groupBy(root.get(AUDHISTORIALPK).get(CODSFC)));
		};
	}
	
	public static Specification<AudHistorial> filtrarPorTipoNumeroDocumento(
			List<String> solicitadoPor) {		
		return (root, query, cb) -> {
			return cb.concat(root.get(TIPODOCUMENTOUSUARIO), 
					root.get(NUMERODOCUMENTOUSUARIO)).in(solicitadoPor);
		};
	}

	public static Specification<AudHistorial> filtrarPorFechaFinal(Date fechaFinal) {
		return (root, query, cb) -> {
			query.orderBy(cb.asc(root.get(AUDHISTORIALPK).get(FECHA) ));
			return cb.lessThanOrEqualTo(root.get(AUDHISTORIALPK).get(FECHA), fechaFinal);
		};
	}

	public static Specification<AudHistorial> filtrarPorTipoNegocio(List<String> tipos) {
		
		return (root, query, cb) -> {
			return root.get(ESTADO).in(tipos);
		};
	}

	public static Specification<AudHistorial> filtrarPorNegocios(List<String> negocios) {
		return (root, query, cb) -> {
			return root.get(AUDHISTORIALPK).get(CODSFC).in(negocios);
		};
	}
	
	public static Specification<AudHistorial> filtrarPorTabla(List<String> tablas) {
		return (root, query, cb)->
			root.get(AUDHISTORIALPK).get(NOMBRETABLA).in(tablas);
	}
	
	public static Specification<AudHistorial>  filtrarPorColumna(List<String> columnas) {
	
		return new Specification<AudHistorial>() {
			private static final long serialVersionUID = 1L;
	

			@Override
			public Predicate toPredicate(Root<AudHistorial> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<AudHistorial, AudHistorialDetalle> detalle = root.join(HISTORIALDETALLES);
				if(columnas.size() == 1)
					return criteriaBuilder.equal(detalle.get(AUDHISTORIALDETALLESK).get(CAMPO), columnas.get(0));
				else
					return detalle.get(AUDHISTORIALDETALLESK).get(CAMPO).in(columnas);
			}

		};
	}
	
	public static Specification<AudHistorial>  filtrarPorOperacion(List<String> tipoOperacion) {
		
		return new Specification<AudHistorial>() {
			private static final long serialVersionUID = 1L;
	

			@Override
			public Predicate toPredicate(Root<AudHistorial> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<AudHistorial, AudHistorialDetalle> detalle = root.join(HISTORIALDETALLES);

				return detalle.get(AUDHISTORIALDETALLESK).get(OPERACION).in(tipoOperacion);
				
			}

		};
	}

public static Specification<AudHistorial>  filtrarPorCampoValor(String columna, String valor ) {
		
		return new Specification<AudHistorial>() {
			private static final long serialVersionUID = 1L;
	

			@Override
			public Predicate toPredicate(Root<AudHistorial> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<AudHistorial, AudHistorialDetalle> detalle = root.join(HISTORIALDETALLES);

				return criteriaBuilder.and( 
						criteriaBuilder.equal(detalle.get(AUDHISTORIALDETALLESK).get(CAMPO), columna),
						criteriaBuilder.equal(detalle.get(DATONUEVO), valor));
			}

		};
	}

public static Specification<AudHistorial> filtrarPorLLave(String llave) {
	return (root, query, cb) -> {
		return cb.equal(root.get(LLAVE),
				llave);
	};
}

public static Specification<AudHistorial> filtrarPorFecha(Date fechaInicial, Date fechaFinal) {
	Calendar c = Calendar.getInstance(); 
	c.setTime(fechaFinal); 
	c.add(Calendar.DATE, 1);
	Date fechaFinalMasUno = c.getTime();
	return (root, query, cb) -> {
		query.orderBy(cb.asc(root.get(AUDHISTORIALPK).get(FECHA) ));
		return cb.between(root.get(AUDHISTORIALPK).get(FECHA), fechaInicial, fechaFinalMasUno);
	};
}

}
