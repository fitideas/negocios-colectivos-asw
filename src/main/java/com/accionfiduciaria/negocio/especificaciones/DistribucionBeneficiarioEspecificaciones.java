package com.accionfiduciaria.negocio.especificaciones;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.RelacionEntreDominios;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;

public class DistribucionBeneficiarioEspecificaciones {

	public static Specification<DistribucionBeneficiario> filtrarPorTipoDocumentoTitular(String tipoDocumentoTitular) {
		return (root, query, cb) -> {
			return cb.equal(root.get("distribucionBeneficiarioPK").get("tipoDocumentoTitular"), tipoDocumentoTitular);
		};
	}

	public static Specification<DistribucionBeneficiario> filtrarPorNumeroDocumentoTitular(
			String numeroDocumentoTitular) {
		return (root, query, cb) -> {
			return cb.equal(root.get("distribucionBeneficiarioPK").get("numeroDocumentoTitular"),
					numeroDocumentoTitular);
		};
	}

	public static Specification<DistribucionBeneficiario> filtrarPorFechaInicial(Date fechaInicial) {
		return (root, query, cb) -> {
			return cb.greaterThanOrEqualTo(root.get("fechaHora"), fechaInicial);
		};
	}

	public static Specification<DistribucionBeneficiario> filtrarPorFechaFinal(Date fechaFinal) {
		return (root, query, cb) -> {
			return cb.lessThanOrEqualTo(root.get("fechaHora"), fechaFinal);
		};
	}

	public static Specification<DistribucionBeneficiario> filtrarPorEstado(List<String> estado) {
		return (root, query, cb) -> {
			return root.get("estado").in(estado);
		};
	}

	public static Specification<DistribucionBeneficiario> filtrarPorNegocios(List<String> negocios) {
		return (root, query, cb) -> {
			return root.get("negocio").get("codSfc").in(negocios);
		};
	}

	public static Specification<DistribucionBeneficiario> filtrarPorTiposPago(List<String> tipos) {
		return new Specification<DistribucionBeneficiario>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<DistribucionBeneficiario> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Join<DistribucionBeneficiario, ValorDominioNegocio> tipoCuenta = root.join("dominioCuenta");
				Join<ValorDominioNegocio, RelacionEntreDominios> dominio = tipoCuenta
						.join("relacionEntreDominiosList1");
				return dominio.get("valorDominioNegocio").get("idValorDominio").in(tipos);
			}

		};
	}

}
