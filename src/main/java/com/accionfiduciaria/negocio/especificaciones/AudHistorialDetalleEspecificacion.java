package com.accionfiduciaria.negocio.especificaciones;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.domain.Specification;

import com.accionfiduciaria.modelo.entidad.AudHistorialDetalle;

public class AudHistorialDetalleEspecificacion {
	
	private static final String FECHA = "fecha";
	private static final String ATRIBUTO_OPERACION="operacion";
	private static final String AUD_HISTORIA_DETALLE_PK="audHistoriaDetallePK";
	private static final String ATRIBUTO_NOMBRE_TABLA="nombreTabla";
	private static final String ATRIBUTO_CAMPO="campo";
	private static final String ATRIBUTO_CODSFC="codSFC";
	private static final String DATO_NUEVO="datoNuevo";






	private AudHistorialDetalleEspecificacion() {
		super();
	}

	public static Specification<AudHistorialDetalle> filtrarPorFechaInicial(Date fechaInicial) {
		return (root, query, cb) -> {
			return cb.greaterThanOrEqualTo(root.get(AUD_HISTORIA_DETALLE_PK).get(FECHA), fechaInicial);
		};
	}
	
	public static Specification<AudHistorialDetalle> filtrarPorFechaFinal(Date fechaFinal) {
		return (root, query, cb) -> {
			return cb.lessThanOrEqualTo(root.get(AUD_HISTORIA_DETALLE_PK).get(FECHA), fechaFinal);
		};
	}
	

	public static Specification<AudHistorialDetalle> filtrarPorOperaciones(List<String> operaciones) {
		return (root, query, cb) -> {
			return root.get(AUD_HISTORIA_DETALLE_PK).get(ATRIBUTO_OPERACION).in(operaciones);
		};
	}
	
	
	public static Specification<AudHistorialDetalle> filtrarPorNombreTabla(String nombreTabla) {
		return (root, query, cb) -> {
			return cb.equal(root.get(AUD_HISTORIA_DETALLE_PK).get(ATRIBUTO_NOMBRE_TABLA), nombreTabla);
		};
	}
	
	
	public static Specification<AudHistorialDetalle> filtrarPorNombreCampo(String nombreCampo) {
		return (root, query, cb) -> {
			return cb.equal(root.get(AUD_HISTORIA_DETALLE_PK).get(ATRIBUTO_CAMPO), nombreCampo);
		};
	}
	
	public static Specification<AudHistorialDetalle> filtrarPorCodSFC(String codSFC) {
		return (root, query, cb) -> {
			return cb.equal(root.get(AUD_HISTORIA_DETALLE_PK).get(ATRIBUTO_CODSFC), codSFC);
		};
	}
	
	public static Specification<AudHistorialDetalle> filtrarPorCodSFC(List<String> negocios) {
		return (root, query, cb) -> {
			return root.get(AUD_HISTORIA_DETALLE_PK).get(ATRIBUTO_CODSFC).in(negocios);
		};
	}
	
	public static Specification<AudHistorialDetalle> filtrarPorNegocios(List<String> tipos) {
		
		return (root, query, cb) -> {
			return root.get(AUD_HISTORIA_DETALLE_PK).get(ATRIBUTO_CODSFC).in(tipos);
		};
	}
	
	
	
	public static Specification<AudHistorialDetalle> filtrarPorTipoNegocio(List<String> tipos) {
		
		return (root, query, cb) -> {
			return root.get(AUD_HISTORIA_DETALLE_PK).in(tipos);
		};
	}

	public static Specification<AudHistorialDetalle>  filtrarPorCampoValor(String campo, String valor) {
		return (root, query, cb) -> {
			return cb.and(
					cb.equal(root.get(AUD_HISTORIA_DETALLE_PK).get(ATRIBUTO_CAMPO),
					campo),
					cb.equal(root.get(DATO_NUEVO),valor) );
		};
	}
	

	



	
	
}

