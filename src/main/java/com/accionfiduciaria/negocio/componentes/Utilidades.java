package com.accionfiduciaria.negocio.componentes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;

/**
 * Clase con métodos utilitarios
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Component
@PropertySource("classpath:propiedades.properties")
public class Utilidades {

	@Value("${codigo.tipo.documento.cedula}")
	private String codigoTipoCedula;
	@Value("${codigo.tipo.documento.nit}")
	private String codigoTipoNit;
	@Value("${codigo.tipo.documento.cedula.extranjeria}")
	private String codigoTipoCedulaExtranjeria;
	@Value("${codigo.tipo.documento.tarjeta.identidad}")
	private String codigoTipoTarjetaIdentidad;
	@Value("${codigo.tipo.documento.fideicomiso}")
	private String codigoTipoFideicomiso;
	@Value("${codigo.tipo.documento.nit.persona.natural}")
	private String codigoTipoNitPersonaNatural;
	@Value("${codigo.tipo.documento.pasaporte}")
	private String codigoTipoPasaporte;
	@Value("${codigo.tipo.documento.registro.civil}")
	private String codigoTipoRegistroCivil;
	@Value("${codigo.tipo.documento.cuenta.exterior}")
	private String codigoTipoCuentaExterior;
	@Value("${codigo.tipo.documento.registro.unico.contribuyentes}")
	private String codigoTipoRegistroUnicoContribuyentes;
	@Value("${codigo.tipo.documento.codigo.interno}")
	private String codigoTipoInterno;
	@Value("${codigo.tipo.documento.documento.extranjero}")
	private String codigoTipoDocumentoExtranjero;
	@Value("${codigo.tipo.documento.entidades.exterior}")
	private String codigoTipoEntidadesExterior;
	@Value("${codigo.tipo.documento.carne.diplomatico}")
	private String codigoTipoCarneDiplomatico;

	@Value("${cedula.ciudadania}")
	private String cedula;
	@Value("${nit}")
	private String nit;
	@Value("${cedula.extranjeria}")
	private String cedulaExtranjeria;
	@Value("${tarjeta.identidad}")
	private String tarjetaIdentidad;
	@Value("${codigo.fideicomiso}")
	private String fideicomiso;
	@Value("${nit.persona.natural}")
	private String nitPersonaNatural;
	@Value("${pasaporte}")
	private String pasaporte;
	@Value("${registro.civil}")
	private String registroCivil;
	@Value("${cuenta.exterior}")
	private String cuentaExterior;
	@Value("${registro.unico.contribuyentes}")
	private String registroUnicoContribuyentes;
	@Value("${codigo.interno}")
	private String codigoInterno;
	@Value("${documento.extranjero}")
	private String documentoExtranjero;
	@Value("${entidades.exterior}")
	private String entidadesExterior;
	@Value("${carne.diplomatico}")
	private String carneDiplomatico;

	@Value("${formato.ddmmyyyy}")
	private String formatoddmmyyyy;

	// Constantes
	public static final String COMMA = ",";
	public static final String EMPTY = "";
	public static final String ESPACIO = " ";
	public static final String PATRON_CODIGO_INTERNO = "FA-";

	// Constantes cálculos
	private static final String UNIDAD_PORCENTAJE = "%";
	private static final String UNIDAD_COP = "COP";
	private static final Double CIEN = (double) 100;

	// Constantes Regex
	private static final String REGEX_EMAIL = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
	private static final String REGEX_COD_INTERNO = "^.*FA-.*$";
	private static final Pattern PATTERN_CORREO = Pattern.compile(REGEX_EMAIL);
	private static final Pattern PATTERN_COD_INTERNO = Pattern.compile(REGEX_COD_INTERNO);

	/**
	 * Cacula el porcentaje de un número.
	 * 
	 * @param fraccion la Fracción del número.
	 * @param total    El número completo.
	 * @return El porcentaje del número que representa la fracción enviada.
	 */
	public float calcularPorcentaje(float fraccion, float total) {
		return (fraccion * 100) / total;
	}

	public float calcularProporcionPorcentaje(float porcentaje, float total) {
		return (porcentaje * total) / 100;
	}

	public String formatearFechaServiciosAccion(String fechaFormatoIso8601) {
		String fechaFormateada = "";
		if (fechaFormatoIso8601 != null && !fechaFormatoIso8601.isEmpty()) {
			Calendar calendarFecha = javax.xml.bind.DatatypeConverter
					.parseDateTime(fechaFormatoIso8601.split("\\+")[0]);
			Date dateFecha = calendarFecha.getTime();
			DateFormat sf = new SimpleDateFormat(formatoddmmyyyy);
			fechaFormateada = sf.format(dateFecha);
		}
		return fechaFormateada;
	}

	public String covertirTipoDocumento(String codigoAccion) {
		
		HashMap<String, String> tiposDocumento = new HashMap<>();
		tiposDocumento.put(codigoTipoCedula, cedula);
		tiposDocumento.put(codigoTipoNit, nit);
		tiposDocumento.put(codigoTipoCedulaExtranjeria, cedulaExtranjeria);
		tiposDocumento.put(codigoTipoTarjetaIdentidad, tarjetaIdentidad);
		tiposDocumento.put(codigoTipoFideicomiso, fideicomiso);
		tiposDocumento.put(codigoTipoNitPersonaNatural, nitPersonaNatural);
		tiposDocumento.put(codigoTipoPasaporte, pasaporte);
		tiposDocumento.put(codigoTipoRegistroCivil, registroCivil);
		tiposDocumento.put(codigoTipoCuentaExterior, cuentaExterior);
		tiposDocumento.put(codigoTipoRegistroUnicoContribuyentes, registroUnicoContribuyentes);
		tiposDocumento.put(codigoTipoInterno, codigoInterno);
		tiposDocumento.put(codigoTipoDocumentoExtranjero, documentoExtranjero);
		tiposDocumento.put(codigoTipoEntidadesExterior, entidadesExterior);
		tiposDocumento.put(codigoTipoCarneDiplomatico, carneDiplomatico);
		
		return tiposDocumento.get(codigoAccion);
	}

	public static String formatearFecha(Date fecha, String formato) {
		String fechaFormateada;
		try {
			DateFormat sf = new SimpleDateFormat(formato);
			fechaFormateada = sf.format(fecha);
		} catch (Exception e) {
			fechaFormateada = EMPTY;
		}
		return fechaFormateada;
	}

	public Date convertirFecha(String fecha, String formato) {
		Date date;
		try {
			DateFormat sf = new SimpleDateFormat(formato);
			date = sf.parse(fecha);
		} catch (Exception e) {
			date = null;
		}
		return date;
	}

	public int obtieneDiaDeLaSemana(Date fecha) {
		Calendar fechaCalendario = Calendar.getInstance();
		fechaCalendario.setTime(fecha);
		return fechaCalendario.get(Calendar.DAY_OF_WEEK + 1);
	}

	public int obtieneDiaDelMes(Date fecha) {
		Calendar fechaCalendario = Calendar.getInstance();
		fechaCalendario.setTime(fecha);
		return fechaCalendario.get(Calendar.MONTH);
	}

	public LocalDateTime convierteDateToLocalDateTime(Date fecha) {
		Calendar fechaCalendario = Calendar.getInstance();
		fechaCalendario.setTime(fecha);
		return LocalDateTime.ofInstant(fechaCalendario.toInstant(), fechaCalendario.getTimeZone().toZoneId());
	}
	
	public LocalDateTime convierteSimpleDateToLocalDateTime(String fecha, String formato, int hora, int min, int seg){
		DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern(formato);
		LocalDate localDate = LocalDate.parse(fecha, formatoFecha);
		return localDate.atTime(hora, min, seg);
	}

	public String convierteLocalDateTimeToString(LocalDateTime fecha) {
		DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		return fecha.format(formatoFecha);
	}
	
	public String convierteLocalDateTimeToString(LocalDateTime fecha, String formato){
		DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern(formato);
		return fecha.format(formatoFecha);
	}
	
	
	public byte[] convierteBase64ToBytes(String archivo) {
		return Base64.getDecoder().decode(archivo);
	}
	
	public String convierteBytesToBase64(byte[] archivo) {
		return Base64.getEncoder().encodeToString(archivo);
	}

	/**
	 * Esté método construye un objeto tipo date con un día, mes y año específico.
	 * 
	 * @author efarias
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @return Date
	 */
	public Date construirFecha(int year, int month, int day) {
		LocalDate localDate = LocalDate.of(year, month, day);
		return java.sql.Date.valueOf(localDate);
	}

	/**
	 * Este método retorna una lista de obligaciones de negocio filtrada por el
	 * perido solicitado.
	 * 
	 * @author efarias
	 * 
	 * @param listaObligaciones
	 * @param periodo
	 * @return List<ObligacionNegocio>
	 */
	public List<ObligacionNegocio> filtrarObligacionesPorPeriodo(List<ObligacionNegocio> listaObligaciones,
			String periodo) {

		List<ObligacionNegocio> listaObligacionesPeriodo = new ArrayList<>();
		if (!listaObligaciones.isEmpty()) {
			Integer periodoActual = obtenerMesPeriodoInteger(periodo);
			listaObligaciones.forEach(obligacion -> {
				String periocidad = obligacion.getPeriodicidad();
				if (periocidad != null && !periocidad.equals(EMPTY)) {
					List<Integer> periodos = obtenerListaPeriodoInteger(periocidad);
					if (periodos.contains(periodoActual)) {
						listaObligacionesPeriodo.add(obligacion);
					}
				}
			});
			return listaObligacionesPeriodo;
		} else {
			return listaObligacionesPeriodo;
		}
	}

	/**
	 * Este método recibe un string periodo en formato dd/MM/yyyy, extrae el
	 * respectivo mes y retorna un entero.
	 * 
	 * @author efarias
	 * 
	 * @param periodo
	 * @return Integer
	 */
	public Integer obtenerMesPeriodoInteger(String periodo) {
		return Integer.parseInt(periodo.substring(3, 5));
	}

	/**
	 * Este método recibe un string periodo en formato dd/MM/yyyy, extrae el
	 * respectivo año y retorna un entero.
	 * 
	 * @author efarias
	 * 
	 * @param periodo
	 * @return Integer
	 */
	public Integer obtenerAnioPeriodoInteger(String periodo) {
		return Integer.parseInt(periodo.substring(6));
	}

	/**
	 * Este método reciba una string con periodos concatenados por coma y retorna
	 * una lista de periodos de tipo integer.
	 * 
	 * @author efarias
	 * 
	 * @param periocidad
	 * @return List<Integer>
	 */
	public List<Integer> obtenerListaPeriodoInteger(String periocidad) {
		return Arrays.asList(periocidad.split(COMMA)).stream().map(Integer::parseInt).collect(Collectors.toList());
	}

	public boolean isRowEmpty(Row row) {
		for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellType() != CellType.BLANK)
				return false;
		}
		return true;
	}

	public CellStyle estiloDefecto(XSSFWorkbook libro) {
		CellStyle estiloCelda = libro.createCellStyle();
		Font fuente = libro.createFont();
		fuente.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
		estiloCelda.setFont(fuente);
		return estiloCelda;
	}

	public boolean esNumerico(String cadena) {
		try {
			Double.parseDouble(cadena);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public boolean esDecimal(String cadena) {
		try {
			double numero = Double.parseDouble(cadena);
			return numero % 1 != 0;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * Cálcula el valor del impuesto de acuerdo al valor configurado en la tabla
	 * NEG_VALOR_DOMINIO_NEGOCIO
	 * 
	 * @author efarias
	 * @see ValorDominioNegocio
	 * @param dominio
	 * @param totalIngresos
	 * @return BigDecimal -> valor impuesto
	 */
	public BigDecimal calcularValorImpuesto(ValorDominioNegocio dominio, BigDecimal totalIngresos) {
		BigDecimal valor = BigDecimal.ZERO;
		if (dominio.getUnidad().equals(UNIDAD_COP)) {
			valor = new BigDecimal(dominio.getValor());
		} else if (dominio.getUnidad().equals(UNIDAD_PORCENTAJE)) {
			valor = totalIngresos.multiply(new BigDecimal(dominio.getValor()).divide(new BigDecimal(CIEN)));
		}
		return valor;
	}

	/**
	 * Se redondea hacia el "vecino más cercano" a menos que ambos vecinos sean
	 * equidistantes, en cuyo caso se redondea hacia arriba.
	 * 
	 * @author efarias
	 * @param number
	 * @return número redondeado.
	 */
	public BigDecimal redondearBigDecimal(BigDecimal number) {
		return number.setScale(0, RoundingMode.HALF_UP);
	}

	/**
	 * Método que valida si es o no un correo valido
	 * 
	 * @author efarias
	 * @param correo
	 * @return verdadero si es un correo valida, si no, retorna falso
	 */
	public boolean validarCorreo(String correo) {
		if (correo != null) {
			Matcher matcher = PATTERN_CORREO.matcher(correo);
			return matcher.matches();
		} else {
			return Boolean.FALSE;
		}
	}
	

	public HashMap<String, Float> obtieneDiferenciaDeMesAno(Date fechaInicial, Date fechaFinal) {
		HashMap<String, Float> diferencias = new HashMap<String, Float>();

		long diff = fechaFinal.getTime() - fechaInicial.getTime();
		float days = (diff / (1000 * 60 * 60 * 24));
		float months = days / 30;
		float ano = months / 12;

		if (months>1) {
			diferencias.put("difMes", months);
		}else {
			diferencias.put("difMes",(float) 0);

		}
		if (ano>1) {
			diferencias.put("difAno", ano);

		}else {
			diferencias.put("difAno", (float) 0);
			
		}
		return diferencias;
	}

	//obtiene el nombre del mes segun un entero
	public String obtieneNombreMes(int month) {
		return new DateFormatSymbols(Locale.forLanguageTag("es-ES")).getMonths()[month-1];
	}
	
	public String obtenerValorDelNumeroEnLetras (String s) {
		String valorEntero="";
		
		if(s.contains(".")) {
			valorEntero = ConvertidorALetras.cantidadConLetra(s.split("\\.")[0]) +" de pesos ";
		}else {
			valorEntero = ConvertidorALetras.cantidadConLetra(s) +" de pesos ";
		}
		return valorEntero.toUpperCase();
	}
	
	/**
	 * Transforma un Date a LocalDate
	 * 
	 * @author efarias
	 * @param fecha -> fecha a transformar
	 * @return Fecha transformada
	 */
	public LocalDate transformarDateALocalDate(Date fecha) {
		return fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	/**
	 * Este método retorna el código interno de negocio el cual se encuentra en el
	 * nombre del negocio.
	 * 
	 * @author efarias
	 * @param nombreNegocio
	 * @return código interno negocio
	 */
	public String obtenerCodigoInterno(String nombreNegocio) {
		String codInterno = EMPTY;
		if (nombreNegocio.contains(PATRON_CODIGO_INTERNO)) {
			String[] partes = nombreNegocio.split(ESPACIO);
			for (String str : partes) {
				Matcher matcher = PATTERN_COD_INTERNO.matcher(str);
				if (matcher.matches()) {
					codInterno = str;
					break;
				}
			}
		}
		return codInterno;
	}
	
	public List<String> obtenerFechasEntreRango(String fechaInicial, String fechaFinal) {
		Calendar calendarInicial = Calendar.getInstance();
		Calendar calendarFinal = Calendar.getInstance();
		calendarInicial.setTime(convertirFecha(fechaInicial, formatoddmmyyyy));
		calendarFinal.setTime(convertirFecha(fechaFinal, formatoddmmyyyy));
		SimpleDateFormat formater = new SimpleDateFormat(formatoddmmyyyy);
		List<String> fechasEntreRango = new ArrayList<>();
		fechasEntreRango.add(fechaInicial);
		while(calendarInicial.before(calendarFinal)) {
			calendarInicial.add(Calendar.DATE, 1);
			fechasEntreRango.add(formater.format(calendarInicial.getTime()));
		}
		return fechasEntreRango;
	}

	public String formatearBigdecimalDosDecimales(BigDecimal bd) {
		bd = bd.setScale(2, RoundingMode.DOWN);
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(2);
		symbols.setGroupingSeparator('.');
		symbols.setDecimalSeparator(',');
		formatter.setDecimalFormatSymbols(symbols);
		return formatter.format(bd.doubleValue());
	}

	public String formatearParticipacion(String participacion) {
		DecimalFormat df = new DecimalFormat("##.##");
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
		String part = "0";
		try {
			Number number = format.parse(participacion);
			part= df.format(number.doubleValue());
		} catch (ParseException e) {
			part = df.format(Double.parseDouble(participacion));
		}
		return part;
	}
}
