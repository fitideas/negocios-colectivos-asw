/**
 * 
 */
package com.accionfiduciaria.negocio.componentes;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import com.accionfiduciaria.excepciones.ErrorAlConectarAccionException;
import com.accionfiduciaria.excepciones.NoContentException;
import com.accionfiduciaria.excepciones.ServiceNotFoundException;
import com.accionfiduciaria.excepciones.UnauthorizedException;

/**
 * @author José Santiago Polo Acosta - 11/12/2019 - jpolo@asesoftware.com
 *
 */
@Component
public class RestTemplateResponseErrorHandler 
  implements ResponseErrorHandler {
 
    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return httpResponse.getStatusCode().equals(HttpStatus.BAD_REQUEST) 
        		|| httpResponse.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)
        		|| httpResponse.getStatusCode().equals(HttpStatus.UNAUTHORIZED)
    			|| httpResponse.getStatusCode().equals(HttpStatus.GATEWAY_TIMEOUT)
    			|| httpResponse.getStatusCode().equals(HttpStatus.NOT_FOUND) 
    			|| httpResponse.getStatusCode().equals(HttpStatus.NO_CONTENT);
    }
    
    @Override
    public void handleError(ClientHttpResponse httpResponse) 
      throws IOException {
    	if(httpResponse.getStatusCode().equals(HttpStatus.UNAUTHORIZED)){
    		throw new UnauthorizedException();
    	}
    	if(httpResponse.getStatusCode().equals(HttpStatus.NOT_FOUND)){
    		throw new ServiceNotFoundException();
    	}
    	if(httpResponse.getStatusCode().equals(HttpStatus.NO_CONTENT) 
    			|| httpResponse.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
    		throw new NoContentException();
    	}
    	if(httpResponse.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
    		throw new ErrorAlConectarAccionException();
    	}
    }
}