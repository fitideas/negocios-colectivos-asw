/**
 * 
 */
package com.accionfiduciaria.negocio.controladores;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.modelo.dtos.PeticionTitularesNegociosDTO;
import com.accionfiduciaria.negocio.servicios.IServicioCertificados;

/**
 * @author fcherrera
 * Controlador encargado de los servicios que tienen
 * que ver con la generacion y/o consulta de certificados
 * creado el 5/05/2020 1:52
 */
@RestController
@CrossOrigin
@RequestMapping("/certificados")
@PropertySource("classpath:mensajes.properties")
public class RestCertificados {

private Logger logger = LogManager.getLogger(RestCertificados.class);

	@Value("${sesion.caducada}")
	private String mensajeSesionCaducada ;
	@Value("${error.general}")
	private String mensajeErrorGeneral;
	
	@Autowired
	private IServicioCertificados servicioCertificados;

	
	@PostMapping(path ="/obtenertitulares", consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarTitularesPaginado (@RequestHeader("Authorization") String tokenAutorizacion,
														 @RequestBody PeticionTitularesNegociosDTO dto){
		try {
			return ResponseEntity.ok(servicioCertificados.obtenerTitularesPaginado(dto, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e);
		}
	
	}
	
	@PostMapping(path ="/obtenernegocios", consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarNegociosPaginado (@RequestHeader("Authorization") String tokenAutorizacion,
														 @RequestBody PeticionTitularesNegociosDTO dto){
		try {
			return ResponseEntity.ok(servicioCertificados.obtenerNegociosPaginado(dto, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e);
		}
	
	}
	@PostMapping(path ="/obtenerdatoscertificados", consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarNegociosPaginado (@RequestHeader("Authorization") String tokenAutorizacion,
														 @RequestBody PeticionTitularesNegociosDTO[] dto,
														 @RequestParam("paramTipo") String paramTipo){
		
		try {
			return ResponseEntity.ok(servicioCertificados.obtenerDatosDelCertificado(dto, paramTipo, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e);
		}
	
	}
	
	@PostMapping(path ="/decargarcertificados")
	public ResponseEntity<?> generarCertificado (@RequestHeader("Authorization") String tokenAutorizacion,
			 @RequestBody PeticionTitularesNegociosDTO[] dto,
			 @RequestParam("paramTipo") String paramTipo){

		try {
			return ResponseEntity.ok(servicioCertificados.generarCertificados(paramTipo, dto,  tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e);
		}
	
	}
}
