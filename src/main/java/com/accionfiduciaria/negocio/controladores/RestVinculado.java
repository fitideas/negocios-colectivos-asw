/**
 * 
 */
package com.accionfiduciaria.negocio.controladores;

import java.net.ConnectException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import com.accionfiduciaria.excepciones.NoExisteUsuarioException;
import com.accionfiduciaria.excepciones.PorcentajeGiroBeneficiariosNoValidoException;
import com.accionfiduciaria.excepciones.TitularNoEncontradoException;
import com.accionfiduciaria.excepciones.TitularSinBeneficiariosException;
import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.modelo.dtos.DatosAGuardarCotitularDTO;
import com.accionfiduciaria.modelo.dtos.DatosDistribucionRecepcionDTO;
import com.accionfiduciaria.modelo.dtos.InformacionFinancieraRecepcionDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.PeticionConsultaHistoricoPagos;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;
import com.accionfiduciaria.modelo.dtos.SolicitudCambioDatosDTO;
import com.accionfiduciaria.negocio.servicios.IServicioArchivo;
import com.accionfiduciaria.negocio.servicios.IServicioBeneficiario;
import com.accionfiduciaria.negocio.servicios.IServicioNegocioTitular;
import com.accionfiduciaria.negocio.servicios.IServicioSolicitudCambiosDatos;
import com.accionfiduciaria.negocio.servicios.IServicioTitular;
import com.accionfiduciaria.negocio.servicios.IServicioVinculados;

/**
 * @author José Santiago Polo Acosta - 05/12/2019 - jpolo@asesoftware.com
 *
 */
@RestController
@RequestMapping("/beneficiario")
@CrossOrigin
@PropertySource("classpath:mensajes.properties")
public class RestVinculado {

	private Logger logger = LogManager.getLogger(RestVinculado.class);

	@Value("${sesion.caducada}")
	private String mensajeSesionCaducada;
	@Value("${error.general}")
	private String mensajeErrorGeneral;
	@Value("${usuario.no.existe}")
	private String mensajeUsuarioNoexiste;
	@Value("${titular.sin.beneficiarios}")
	private String mensajeTitularSinBeneficiarios;
	@Value("${porcentaje.giro.beneficiario.invalido}")
	private String mensajePorcentajeGiroBeneficiarioNoValido;
	@Value("${titular.no.encontrado}")
	private String mensajeTitularNoEncontrado;

	@Autowired
	IServicioVinculados servicioVinculados;

	@Autowired
	IServicioSolicitudCambiosDatos solicitudCambiosDatos;

	@Autowired
	IServicioNegocioTitular servicioNegocioTitular;

	@Autowired
	IServicioBeneficiario servicioBeneficiario;

	@Autowired
	IServicioTitular servicioTitular;
	
	@Autowired
	IServicioArchivo servicioArchivo;

	@GetMapping(path = "/buscar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarVinculado(@RequestParam String argumento, @RequestParam String criterioBuscar,
			@RequestParam int pagina, @RequestParam int elementos,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioVinculados.buscarListaVinculados(argumento, criterioBuscar, pagina,
					elementos, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (RestClientException e) {
			  if (e.getCause() instanceof ConnectException) {
				    return ResponseEntity.status(HttpStatus.CONFLICT).body("Could not connect");
				  }
			  return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/buscarpordocumento", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarPorDocumento(@RequestParam String documento,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioVinculados.buscarBeneficiarioPorDocumento(documento, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenersolicitudes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerSolicitudes(@RequestParam(name = "tipodocumento") String tipoDocumento,
			@RequestParam String documento) {
		try {
			return ResponseEntity.ok(solicitudCambiosDatos.obtenerSolicitudes(tipoDocumento, documento));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param solicitudCambioDatos
	 * @return
	 */
	@PutMapping(path = "/enviarsolicitud", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> enviarSolicitud(@RequestBody List<SolicitudCambioDatosDTO> solicitudCambioDatos) {
		try {
			solicitudCambiosDatos.enviarSolicitud(solicitudCambioDatos);
			return ResponseEntity.ok("");
		} catch (NoExisteUsuarioException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeUsuarioNoexiste);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenernegocios", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerNegocios(@RequestParam String tipoDocumento, @RequestParam String documento,
			@RequestParam int pagina, @RequestParam int elementos,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioNegocioTitular.obtenerNegocios(tipoDocumento, documento, pagina, elementos,
					tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenerdatosdistribucion", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerDatosDistribucion(@RequestParam("tipoDocumento") String tipoDocumentoVinculado,
			@RequestParam("documento") String documentoVinculado, @RequestParam("codigo") String codigoNegocio,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioNegocioTitular.obtenerDatosDistribucion(tipoDocumentoVinculado,
					documentoVinculado, codigoNegocio, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PutMapping(path = "/guardardatosdistribucion", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardardatosdistribucion(
			@RequestBody DatosDistribucionRecepcionDTO datosDistribucionRecepcionDTO,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioNegocioTitular.guardarDatosDistribucion(datosDistribucionRecepcionDTO);
			return ResponseEntity.ok().build();
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenerinformacionfinanciera", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerInformacionFinanciera(@RequestParam("tipodocumento") String tipoDocumentoTitular,
			@RequestParam("numerodocumento") String documentoTitular,
			@RequestParam("codigonegocio") String codigoNegocio,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioBeneficiario.obtenerInformacionFinanciera(tipoDocumentoTitular,
					documentoTitular, codigoNegocio, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/validarencargo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validarEncargo(@RequestParam("numeroencargo") String numeroEncargo, @RequestParam String documento,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioVinculados.validarEncargo(numeroEncargo, documento, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PutMapping(path = "/guardarinformacionfinanciera", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarInformacionFinanciera(
			@RequestBody InformacionFinancieraRecepcionDTO informacionFinanciera,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioBeneficiario.guardarInformacionFinanciera(informacionFinanciera);
			return ResponseEntity.ok().build();
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (TitularSinBeneficiariosException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mensajeTitularSinBeneficiarios);
		} catch (PorcentajeGiroBeneficiariosNoValidoException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mensajePorcentajeGiroBeneficiarioNoValido);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenercotitular", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerCotitular(@RequestParam String tipoDocumentoTitular,
			@RequestParam String documentoTitular, @RequestParam String codigoNegocio, @RequestParam String argumento,
			@RequestParam String criterioBuscar, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioTitular.obtenerCotitular(tipoDocumentoTitular, documentoTitular,
					codigoNegocio, argumento, criterioBuscar));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (TitularNoEncontradoException e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mensajeTitularNoEncontrado);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/administrarcotitular", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> administrarCotitular(@RequestBody DatosAGuardarCotitularDTO datosAGuardarCotitularDTO,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioTitular.administrarCotitular(datosAGuardarCotitularDTO);
			return ResponseEntity.ok().build();
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@PostMapping(path = "/historialpagos", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerHistorialPagos(@RequestBody PeticionConsultaHistoricoPagos peticion, @RequestHeader("Authorization")String tokenAutorizacion){
		try {
			return ResponseEntity.ok(servicioBeneficiario.listaPagos(peticion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	
	@PostMapping(path = "/descargararchivohistorialpagos", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> descargaHistorialPagos (@RequestBody PeticionConsultaHistoricoPagos peticion, @RequestHeader("Authorization") String tokenAutorizacion){
		try {
			RespuestaArchivoDTO respuesta = servicioBeneficiario.obtenerArchivoHistorialPagos(peticion);
			HttpHeaders header = new HttpHeaders();
			header.setContentType(new MediaType("application", "force-download"));
			header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=ProductTemplate.xlsx");
			return new ResponseEntity<>(respuesta.getRecurso(), header, HttpStatus.OK);
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenernovedadesjuridicas", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerNovedadesJuridicas(@RequestParam String codigoNegocio, @RequestParam String tipoDocumento,
			@RequestParam String documento, @RequestParam int pagina, @RequestParam int elementos,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioVinculados.obtenerNovedadesJuridicas(codigoNegocio, tipoDocumento, documento, pagina,
					elementos, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/detallenovedadjuridica", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> detalleNovedadJuridica(@RequestParam String codigoNegocio, @RequestParam String tipoDocumento, @RequestParam String documento,
			@RequestParam String codigoEvento, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioVinculados.detalleNovedadJuridica(codigoNegocio, tipoDocumento, documento, codigoEvento,
					tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@PostMapping(path = "/obtenerhistoricomodificaciones", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerBeneficiarioHistorialCambios(@RequestBody ParametrosFiltroDTO filtros, @RequestHeader("Authorization")String tokenAutorizacion){
		try {
			return ResponseEntity.ok(servicioBeneficiario.listaBeneficiarioHistorialCambios(filtros, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@PostMapping(path = "/obtenerhistoricsolicitudesomodificaciones", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerBeneficiarioHistorialSolicitudCambios(@RequestBody ParametrosFiltroDTO filtros, @RequestHeader("Authorization")String tokenAutorizacion){
		try {
			return ResponseEntity.ok(servicioBeneficiario.listaBeneficiarioHistorialSolicitudCambios(filtros));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@PostMapping(path = "/obtenerhistoricocesionesmodificaciones", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerBeneficiarioHistorialCesionesCambios(@RequestBody ParametrosFiltroDTO filtros, @RequestHeader("Authorization")String tokenAutorizacion){
		try {
			return ResponseEntity.ok(servicioBeneficiario.listaBeneficiarioHistorialCesionesCambios(filtros, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
}