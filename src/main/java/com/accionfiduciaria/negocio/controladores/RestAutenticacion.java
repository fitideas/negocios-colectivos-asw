package com.accionfiduciaria.negocio.controladores;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accionfiduciaria.excepciones.ParametroNoEncontradoException;
import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.excepciones.UsuarioSinPermisosException;
import com.accionfiduciaria.excepciones.UsuarioSinRolesException;
import com.accionfiduciaria.modelo.dtos.UsuarioDTO;
import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioAutenticacion;

/**
 * Clase con los end points para realizar la autenticación del usuario.
 * 
 * @author José Santiago Polo Acosta - 03/12/2019 - jpolo@asesoftware.com
 *
 */

@RestController
@CrossOrigin
@RequestMapping("/autenticacion")
@PropertySource("classpath:mensajes.properties")
public class RestAutenticacion {

	private Logger logger = LogManager.getLogger(RestAutenticacion.class);

	@Value("${credenciales.invalidas}")
	private String mensajeCredencialesInvalidas;
	@Value("${usuario.sin.roles}")
	private String mensajeUsuarioSinRoles;
	@Value("${usuario.sin.permisos}")
	private String mensajeUsuarioSinPermisos;
	@Value("${error.general}")
	private String mensajeErrorGeneral;

	@Autowired
	IServicioAutenticacion servicioAutenticacion;

	@PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> login(@RequestBody UsuarioDTO usuarioDTO) {
		try {
			return ResponseEntity.ok(servicioAutenticacion.autenticarUsuario(usuarioDTO));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeCredencialesInvalidas);
		} catch (UsuarioSinRolesException | UsuarioSinPermisosException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
		} catch (ParametroNoEncontradoException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		} catch (Exception e) {
			logger.error("", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/cerrarsesion", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cerrarSesion(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioAutenticacion.cerrarSesion(tokenAutorizacion);
			return ResponseEntity.ok().build();
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
		} catch (Exception e) {
			logger.error("", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
}
