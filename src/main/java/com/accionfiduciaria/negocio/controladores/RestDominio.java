package com.accionfiduciaria.negocio.controladores;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accionfiduciaria.modelo.dtos.DominioCompuestoDTO;
import com.accionfiduciaria.modelo.dtos.DominioSimpleDTO;
import com.accionfiduciaria.modelo.dtos.EliminacionDominioDTO;
import com.accionfiduciaria.negocio.servicios.IServicioDominio;

@RestController
@CrossOrigin
@RequestMapping("/parametros")
public class RestDominio {
	
	private static final Logger log = LogManager.getLogger(RestDominio.class);
	
	@Autowired
	private IServicioDominio servicioDominio;

	@GetMapping(path = "/obtenersimples", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerDominiosSimples(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioDominio.obtenerDominiosSimples());
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenercompuestos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerParametrosCompuestos(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioDominio.obtenerDominiosCompuestos());
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@PatchMapping(path = "/actualizarsimples", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarParametrosSimples(@RequestBody List<DominioSimpleDTO> dominiosSimples, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioDominio.actualizarDominiosSimples(dominiosSimples);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@PatchMapping(path = "/actualizarcompuestos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarParametrosCompuestos(@RequestBody List<DominioCompuestoDTO> dominios, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioDominio.actualizarDominiosCompuestos(dominios);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@PostMapping(path = "/eliminar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> eliminarParametros(@RequestBody EliminacionDominioDTO eliminacionDominioDTO, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioDominio.eliminarValorDominio(eliminacionDominioDTO);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping(path = "/valoresdominio", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerValoresDominio(@RequestParam("codigo") String codigoDominio, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioDominio.obtenerValoresDominio(codigoDominio));
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenertipotercero", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerTipoTercero(@RequestParam("codigo") String codigo, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioDominio.obtenerTipoTercero(codigo));
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping(path = "/valorestipocompuesto", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> valoresTipoCompuesto(@RequestParam("codigo") String codigo, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioDominio.obtenerValoresDominioCompuesto(codigo));
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenerretencionesasociadas", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerRetencionesAsociadas(@RequestParam("idValorDominio") Integer idValorDominio, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioDominio.obtenerRetencionesAsociadas(idValorDominio));
		} catch (Exception e) {
			log.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
}