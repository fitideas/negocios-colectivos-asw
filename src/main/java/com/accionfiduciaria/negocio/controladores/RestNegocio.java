package com.accionfiduciaria.negocio.controladores;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.accionfiduciaria.enumeradores.TiposArchivo;
import com.accionfiduciaria.excepciones.NoContentException;
import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.modelo.dtos.ComiteRegistroDTO;
import com.accionfiduciaria.modelo.dtos.MiembroComiteDTO;
import com.accionfiduciaria.modelo.dtos.NegocioCompletoDTO;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.PeticionAsambleasDTO;
import com.accionfiduciaria.modelo.dtos.PeticionHistorialReunionesDTO;
import com.accionfiduciaria.modelo.dtos.PeticionInformacionGeneralPagosDTO;
import com.accionfiduciaria.modelo.dtos.RespuestaArchivoDTO;
import com.accionfiduciaria.negocio.servicios.IServicioDistribucion;
import com.accionfiduciaria.negocio.servicios.IServicioMiembrosComite;
import com.accionfiduciaria.negocio.servicios.IServicioNegocio;
import com.accionfiduciaria.negocio.servicios.IServicioResponsablesNotificacion;
import com.accionfiduciaria.negocio.servicios.IServicioReunion;
import com.accionfiduciaria.negocio.servicios.IServicioTipoFideicomiso;
import com.accionfiduciaria.negocio.servicios.IServicioTitular;
import com.accionfiduciaria.negocio.servicios.IServicioUbicacionGeografica;

@RestController
@RequestMapping("/negocio")
@CrossOrigin
@PropertySource("classpath:mensajes.properties")
public class RestNegocio {
	private Logger logger = LogManager.getLogger(RestNegocio.class);

	@Value("${sesion.caducada}")
	private String mensajeSesionCaducada;
	@Value("${error.general}")
	private String mensajeErrorGeneral;

	@Autowired
	private IServicioNegocio servicioNegocio;
	
	@Autowired
	private IServicioReunion servicioReunion;
	
	@Autowired
	private IServicioTitular servicioTitular;

	@Autowired
	private IServicioDistribucion servicioDistribucion;
	
	@Autowired
	private IServicioResponsablesNotificacion servicioResponsablesNotificacion;

	@Autowired
	private IServicioMiembrosComite servicioMiembroComite;
	
	@Autowired
	private IServicioUbicacionGeografica servicioUbicacionGeografica;
	
	@Autowired
	private IServicioTipoFideicomiso servicioTipoFideicomiso;
	
	@GetMapping(path = "/buscar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarNegocios(@RequestParam String argumento, @RequestParam String criterioBuscar,
			@RequestParam int pagina, @RequestParam int elementos,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(
					servicioNegocio.buscarNegocios(argumento, criterioBuscar, pagina, elementos, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (NoContentException e) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(value = "/cargararchivoclientes")
	public ResponseEntity<?> enviarArchivoBeneficiarios(@RequestParam("file") MultipartFile archivo,
			@RequestParam("codigoNegocio") String codigoNegocio,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			RespuestaArchivoDTO respuesta = servicioNegocio.asociarBeneficiarios(archivo, tokenAutorizacion,
					codigoNegocio);
			HttpHeaders header = new HttpHeaders();
			header.setContentType(new MediaType("application", "force-download"));
			header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=ProductTemplate.xlsx");
			if (respuesta.getTipoArchivo().equals(TiposArchivo.INVALIDO.toString()))
				return new ResponseEntity<>(respuesta.getRecurso(), header, HttpStatus.BAD_REQUEST);
			else
				return new ResponseEntity<>(respuesta.getRecurso(), header, HttpStatus.OK);
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenertitulares", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerTitulares(@RequestParam String codigoSFC) {
		try {
			return ResponseEntity.ok(servicioTitular.obtenerTitulares(codigoSFC));
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenerinformacionnegocio", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarNegocioPorCodigo(@RequestParam String codigonegocio,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioNegocio.obtenerDatosNegocio(codigonegocio, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PutMapping(path = "/guardarinformacionnegocio", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarInformacionNegocio(@RequestBody NegocioCompletoDTO negocioCompletoDTO,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioNegocio.guardarInformacionNegocio(negocioCompletoDTO, tokenAutorizacion);
			return ResponseEntity.ok().build();
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/registrarasamblea", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarasambleas(@RequestHeader("Authorization") String tokenAutorizacion,@RequestBody PeticionAsambleasDTO asambleasDTO) {
		try {
			servicioNegocio.guardaInformacionAsambleas(asambleasDTO, tokenAutorizacion);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/obtenerinformaciongeneralpagos", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerInformacionGeneralPagos(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestBody PeticionInformacionGeneralPagosDTO peticion) {
		try {
			return ResponseEntity.ok(servicioNegocio.obtenerPagosNegocio(peticion, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenerinformaciondistribucionpagos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerInformacionDistribucionPagos(
			@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam("codigoNegocio") String codigoNegocio,
			@RequestParam("codigoTipoNegocio") String codigoTipoNegocio, @RequestParam("periodo") String periodo) {
		try {
			return ResponseEntity
					.ok(servicioDistribucion.obtieneIngresosEgresos(periodo, codigoNegocio, codigoTipoNegocio));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);

		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/detallebeneficiariospago", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerDetalleBeneficiariosPago(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam("codigoNegocio") String codigoNegocio,
			@RequestParam("codigoTipoNegocio") String codigoTipoNegocio, @RequestParam("periodo") String periodo) {
		try {
			return ResponseEntity
					.ok(servicioNegocio.obtenerBeneficiariosPorPago(codigoNegocio, codigoTipoNegocio, periodo));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);

		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}

	}

	@GetMapping(path = "/archivodetallebeneficiariospago")
	public ResponseEntity<?> descargaDetallePagosBeneficiarios(@RequestParam("codigoNegocio") String codigoNegocio,
			@RequestParam("codigoTipoNegocio") String codigoTipoNegocio, @RequestParam("periodo") String periodo,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			RespuestaArchivoDTO respuesta = servicioNegocio.obtenerArchivoDetallePagos(codigoNegocio, codigoTipoNegocio,
					periodo);
			HttpHeaders header = new HttpHeaders();
			header.setContentType(new MediaType("application", "force-download"));
			header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=ProductTemplate.xlsx");
			return new ResponseEntity<>(respuesta.getRecurso(), header, HttpStatus.OK);
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenerlistacomite", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerlistacomite(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam("codigoNegocio") String codSFC) {
		try {
			return ResponseEntity.ok(servicioMiembroComite.obtenerMiembrosComiteNegocio(codSFC));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PutMapping(path = "/guardarmiembrocomite", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarmiembrocomite(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestBody MiembroComiteDTO mc) {
		try {
			servicioMiembroComite.guardarMiembroComite(mc);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/sincronizardatos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> sincronizarDatos(@RequestParam String codigoNegocio,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioNegocio.sincronizarYValidarDatos(codigoNegocio, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	

	@PostMapping(path = "/registrardatoscomite", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarcomite(@RequestBody ComiteRegistroDTO comitedto, @RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			servicioMiembroComite.guardaInformacionComite(comitedto, tokenAutorizacion);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@PostMapping(path = "/obtenerreunion", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerreunion(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam("codigoNegocio") String codSFC, @RequestParam("tipoHistorico") String tipoHistorico) {
		try {
			return ResponseEntity.ok(servicioNegocio.consultaHistoricoReunion(codSFC, tipoHistorico));
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@PostMapping(path = "/obtenerreuniones", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerReuniones(@RequestHeader("Authorization") String tokenAuthorizacion, @RequestBody PeticionHistorialReunionesDTO peticion) {
		try {
			return ResponseEntity.ok(servicioNegocio.consultaHistoricoReuniones(peticion, tokenAuthorizacion));
		}catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenerasamblea", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerDetalleAsamblea(@RequestHeader("Authorization") String tokenAuthorizacion,
			@RequestParam("codigoNegocio") String codigoNegocio, @RequestParam("fechaHora") String fechaHora, @RequestParam("numeroRadicado") String numeroRadicado) {
		try {
			return ResponseEntity.ok(servicioReunion.obtenerReunionAsamblea(codigoNegocio, fechaHora, numeroRadicado));
		}catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenercomite", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerDetalleComite(@RequestHeader("Authorization") String tokenAuthorizacion,
			@RequestParam("codigoNegocio") String codigoNegocio, @RequestParam("fechaHora") String fechaHora, @RequestParam("numeroRadicado") String numeroRadicado) {
		try {
			return ResponseEntity.ok(servicioReunion.obtenerReunionComite(codigoNegocio, fechaHora, numeroRadicado));
		}catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	//Método que permite obtener todos los negocios de una aplicación
	
	@GetMapping(path = "/obtenernegocios", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenernegocios(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioNegocio.obtenerNegocios());
		} catch (Exception e) {
			logger.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@PostMapping(path = "/obtenerhistoricomodificaciones", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerNegocioHistorialCambios(@RequestBody ParametrosFiltroDTO filtros, @RequestHeader("Authorization")String tokenAutorizacion){
		try {
			return ResponseEntity.ok(servicioNegocio.listaNegocioHistorialCambios(filtros, tokenAutorizacion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenerresponsablesnegocio", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerResponsablesNegocio(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioResponsablesNotificacion.obtenerResponsablesNegocioActivos());
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenerpaises", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerPaisesActivos(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioUbicacionGeografica.obtenerPaisesActivos());
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenerdepartamentos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerDepartamentosPorPais(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam("codigoPais") String codigoPais) {
		try {
			return ResponseEntity.ok(servicioUbicacionGeografica.obtenerDepartamentosPorPais(codigoPais));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(path = "/obtenermunicipios", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerMunicipiosPorDepartamento(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam("codigoDepartamento") String codigoDepartamento) {
		try {
			return ResponseEntity.ok(servicioUbicacionGeografica.obtenerMunicipiosPorDepartamento(codigoDepartamento));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenertiposnegocio", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerTiposNegocio(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam String codigoNegocio) {
		try {
			return ResponseEntity.ok(servicioNegocio.obtenerTiposNegocio(codigoNegocio));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenertipofideicomiso", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerTipoFideicomiso(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioTipoFideicomiso.obtenerTipoFideicomiso());
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenersubtipofideicomiso", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerSubTipofideicomiso(@RequestHeader("Authorization") String tokenAutorizacion,
			String tipoFideicomiso) {
		try {
			return ResponseEntity.ok(servicioTipoFideicomiso.obtenerSubTipoFideicomiso(tipoFideicomiso));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
}
 