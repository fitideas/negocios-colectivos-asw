package com.accionfiduciaria.negocio.controladores;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.negocio.servicios.IServicioDominio;

@RestController
@RequestMapping("/util")
@CrossOrigin
@PropertySource("classpath:mensajes.properties")
public class RestUtil {

	private Logger logger = LogManager.getLogger(RestUtil.class);
	
	@Autowired
	private IServicioDominio servicioDominio;
	
	@Value("${sesion.caducada}")
	private String mensajeSesionCaducada;
	@Value("${error.general}")
	private String mensajeErrorGeneral;
	
	@GetMapping(path = "/obtenerbancos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerBancos(@RequestHeader("Authorization") String tokenAutorizacion){
		try {
			return ResponseEntity.ok(servicioDominio.obtenerbancos());
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenertiposdocumento", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerTiposDocumento(@RequestHeader("Authorization") String tokenAutorizacion){
		try {
			return ResponseEntity.ok(servicioDominio.obtenerTiposDocumento());
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}
}
