/**
 * 
 */
package com.accionfiduciaria.negocio.controladores;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.modelo.dtos.ListaIngresosOperacionDTO;
import com.accionfiduciaria.modelo.dtos.ReprocesoPagosDTO;
import com.accionfiduciaria.negocio.servicios.IServicioDistribucion;
import com.accionfiduciaria.negocio.servicios.IServicioIngresoOperacion;

/**
 * @author efarias
 *
 */
@RestController
@RequestMapping("/pagos")
@CrossOrigin
@PropertySource("classpath:mensajes.properties")
public class RestPagos {

	private Logger logger = LogManager.getLogger(RestPagos.class);

	@Autowired
	private IServicioIngresoOperacion servicioIngresoOperacion;

	@Autowired
	private IServicioDistribucion servicioDistribucion;

	@Value("${sesion.caducada}")
	private String mensajeSesionCaducada;

	@Value("${error.general}")
	private String mensajeErrorGeneral;

	@GetMapping(path = "/obtenerlistaingresosoperacion", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerListaIngresosOperacion(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam String periodo, @RequestParam String codigoNegocio) {
		try {
			return ResponseEntity.ok(
					servicioIngresoOperacion.obtenerListaIngresosOperacion(tokenAutorizacion, periodo, codigoNegocio));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PutMapping(path = "/guardarlistaingresosoperacionales", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarListaIngresosOperacion(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestBody ListaIngresosOperacionDTO listaIngresosOperacion) {
		try {
			servicioIngresoOperacion.guardarListaIngresosOperacion(listaIngresosOperacion);
			Map<String, String> response = new HashMap<>();
			response.put("respuesta", "Datos guardados satisfactoriamenete.");
			return ResponseEntity.status(HttpStatus.CREATED).body(response);
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(value = "/calcularobligacion")
	public ResponseEntity<?> calcularObligacion(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam String periodo, @RequestParam String codigoNegocio, @RequestParam Integer codigoTipoNegocio) {
		try {
			return ResponseEntity
					.ok(servicioDistribucion.calcularObligacion(periodo, codigoNegocio, codigoTipoNegocio));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(value = "/calculardistribucion")
	public ResponseEntity<?> calcularDistribucion(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam String periodo, @RequestParam String codigoNegocio, @RequestParam Integer codigoTipoNegocio) {
		try {
			return ResponseEntity
					.ok(servicioDistribucion.calcularDistribucion(periodo, codigoNegocio, codigoTipoNegocio));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@GetMapping(value = "/generararchivopago")
	public ResponseEntity<?> generarArchivoPago(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestParam String periodo, @RequestParam String codigoNegocio, @RequestParam Integer codigoTipoNegocio) {
		try {
			return ResponseEntity
					.ok(servicioDistribucion.generarArchivoPago(periodo, codigoNegocio, codigoTipoNegocio));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(value = "/reprocesarpagos")
	public ResponseEntity<?> reprocesarPagos(@RequestHeader("Authorization") String tokenAutorizacion,
			@RequestBody ReprocesoPagosDTO reprocesarPago) {
		try {
			return ResponseEntity.ok(servicioDistribucion.reprocesarPagos(reprocesarPago));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

}
