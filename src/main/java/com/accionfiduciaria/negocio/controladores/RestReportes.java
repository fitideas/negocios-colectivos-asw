package com.accionfiduciaria.negocio.controladores;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accionfiduciaria.excepciones.UnauthorizedException;
import com.accionfiduciaria.modelo.dtos.ParametrosFiltroDTO;
import com.accionfiduciaria.modelo.dtos.PeticionInformesModificacionDTO;
import com.accionfiduciaria.negocio.servicios.IServicioAudHistorial;
import com.accionfiduciaria.negocio.servicios.IServicioReportes;

@RestController
@RequestMapping("/reportes")
@CrossOrigin
@PropertySource("classpath:mensajes.properties")
public class RestReportes {
	private Logger logger = LogManager.getLogger(RestReportes.class);
	@Autowired
	IServicioAudHistorial servicioAudHistorial;

	@Autowired
	IServicioReportes servicioReportes;

	@Value("${sesion.caducada}")
	private String mensajeSesionCaducada;
	@Value("${error.general}")
	private String mensajeErrorGeneral;

	@GetMapping(path = "/obtenerusuarios", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerUsuarios(@RequestHeader("Authorization") String tokenAutorizacion) {
		try {

			return ResponseEntity.ok(servicioAudHistorial.obtenerUsuarios());
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/reportesolicitudesdecambios", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerReporteSolucitudesCambios(@RequestBody ParametrosFiltroDTO dto,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioAudHistorial.obtenerConteoSolicitiduesCambio(dto));
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/reportesdistribucion", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerReporteDistribucion(@RequestBody ParametrosFiltroDTO dto,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioAudHistorial.obtenerConteoCambiosDistribucion(dto));
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/reportebeneficiarios", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerReporteBeneficiarios(@RequestBody ParametrosFiltroDTO dto,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioAudHistorial.obtenerConteoCambiosBeneficiarios(dto));
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/reportecesiones", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerReporteCesiones(@RequestBody ParametrosFiltroDTO dto,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioAudHistorial.obtenerConteoCambiosCesiones(dto, tokenAutorizacion));
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/reportetiposnegocio", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerReporteTiposNegocio(@RequestBody ParametrosFiltroDTO dto,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {

			return ResponseEntity.ok(servicioAudHistorial.obtenerConteoDeTiposDeNegocio(dto, tokenAutorizacion));
		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/reportemodificacionesvalores", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerReporteModificacionesValores(@RequestBody ParametrosFiltroDTO dto,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {

			return ResponseEntity.ok(servicioAudHistorial.obtenerDatosInformacionNegocio(dto));//

		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}
	}

	@PostMapping(path = "/modificacionpagos", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerModificacionCifraPago(@RequestBody PeticionInformesModificacionDTO peticion,
			@RequestHeader("Authorization") String tokenAutorizacion) {
		try {
			return ResponseEntity.ok(servicioReportes.obtenerConteoSolicitudes(peticion));
		} catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mensajeSesionCaducada);

		} catch (Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(mensajeErrorGeneral + " " + e.getMessage());
		}

	}
}
