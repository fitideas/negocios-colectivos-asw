package com.accionfiduciaria.negocio.controladores;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accionfiduciaria.excepciones.FaltanDatosObligatoriosException;
import com.accionfiduciaria.modelo.dtos.RolDTO;
import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioPermiso;
import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioRol;
import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioRolPermiso;

@RestController
@CrossOrigin
@RequestMapping("/roles")
@PropertySource("classpath:mensajes.properties")
public class RestRoles {
	
	private Logger logger = LogManager.getLogger(RestRoles.class);
	
	@Value("${faltan.datos.obligatorios}")
	private String mensajeFaltanDatosObligatorios;
	
	@Autowired
	private IServicioRol servicioRol;
	
	@Autowired
	private IServicioPermiso servicioPermiso;
	
	@Autowired
	private IServicioRolPermiso servicioRolPermiso;
	
	@GetMapping(path = "/obtenerroles", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerTodosRoles(){
		try {
			return ResponseEntity.ok(servicioRol.obtenerRoles());
		} catch (Exception e) {
			logger.error("", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping(path = "/obtenerpermisos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerPermisosActivos(){
		try {
			return ResponseEntity.ok(servicioPermiso.obtenerPermisosActivos());
		} catch (Exception e) {
			logger.error("", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@PatchMapping(path = "/actualizarroles", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarRoles(@RequestBody List<RolDTO> roles){
		try {
			servicioRolPermiso.actualizarPermisosRoles(roles);
			return ResponseEntity.ok("");
		} catch (FaltanDatosObligatoriosException e) {
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensajeFaltanDatosObligatorios); 
		} catch (Exception e) {
			logger.error("",e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
}
