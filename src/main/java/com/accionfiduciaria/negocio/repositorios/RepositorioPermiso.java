package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Permiso;

/**
 * 
 * Repositorio para la entidad Permiso
 * 
 * @author José Santiago Polo Acosta - 05/12/2019 - jpolo@asesoftware.com
 *
 */
@Repository
public interface RepositorioPermiso extends CrudRepository<Permiso, Integer> {

	/**
	 * Busca todas las opciones de menu de la aplicación.
	 * 
	 * @return Una lista con los datos de las opciones de menú encontradas.
	 */
	public List<Permiso> findAllByPermisoPadreIsNull();
}
