package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.MedioPagoPersona;
import com.accionfiduciaria.modelo.entidad.MedioPagoPersonaPK;
import com.accionfiduciaria.modelo.entidad.Persona;

@Repository
public interface RepositorioMedioPagoPersona extends CrudRepository<MedioPagoPersona, MedioPagoPersonaPK>,
		JpaRepository<MedioPagoPersona, MedioPagoPersonaPK> {

	public List<MedioPagoPersona> findByPersona(Persona persona);

}
