/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Rol;

/**
 * @author José Santiago Polo Acosta - 12/12/2019 - jpolo@asesoftware.com
 *
 */
@Repository
public interface RepositorioRol extends CrudRepository<Rol, Integer> {
	
	public List<Rol> findDistinctByrolPermisosPermisoEstado(int estado);
	
	public List<Rol> findAll();

	public boolean existsByNombreRol(String rol);
	
}
