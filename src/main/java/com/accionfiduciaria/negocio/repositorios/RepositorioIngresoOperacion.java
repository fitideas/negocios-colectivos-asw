/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.IngresoOperacion;
import com.accionfiduciaria.modelo.entidad.IngresoOperacionPK;
import com.accionfiduciaria.modelo.entidad.PagoNegocio;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioIngresoOperacion extends CrudRepository<IngresoOperacion, IngresoOperacionPK> {
	
	@Query(value = "SELECT I FROM IngresoOperacion I WHERE pagoNegocio = :pagoNegocio AND estado = 1")
	public List<IngresoOperacion> buscarIngresosOperacion(@Param("pagoNegocio") PagoNegocio pagoNegocio);

}
