package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiario;
import com.accionfiduciaria.modelo.entidad.DistribucionBeneficiarioPK;

@Repository
public interface RepositorioDistribucionBeneficiario
		extends PagingAndSortingRepository<DistribucionBeneficiario, DistribucionBeneficiarioPK>,
		JpaSpecificationExecutor<DistribucionBeneficiario>,
		JpaRepository<DistribucionBeneficiario, DistribucionBeneficiarioPK> {

	@Modifying
	@Query(value = "DELETE DistribucionBeneficiario WHERE distribucionBeneficiarioPK = :distribucionBeneficiarioPK")
	public void eliminarDistribucionBeneficiario(
			@Param("distribucionBeneficiarioPK") DistribucionBeneficiarioPK distribucionBeneficiarioPK);

}
