package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;

/**
 * Repositorio para el manejo de la tabla VALOR_DOMINIO_NEGOCIO
 * 
 * @author jpolo
 *
 */
public interface RepositorioValorDominioNegocio extends CrudRepository<ValorDominioNegocio, Integer> {

	/**
	 * Consulta todos los valores de dominio cuyo valor es de un tipo de dominio
	 * determinado.
	 * 
	 * @param codigoDominio El codigo del dominio.
	 * @return la lista de valores de dominio cuyo valor es del tipo del dominio
	 *         enviado.
	 */
	@Query(value = "SELECT DISTINCT r.valorDominioNegocio1 FROM ValorDominioNegocio v LEFT JOIN v.relacionEntreDominiosList r WHERE v.dominio.codDominio = :codDominio AND v.activo = true")
	public List<ValorDominioNegocio> buscarValoresQueUsanDominio(@Param("codDominio") String codigoDominio);

	/**
	 * Consulta todos los valores de dominio cuyo valor es de un tipo de dominio
	 * determinado.
	 * 
	 * @param codigoDominio El codigo del dominio.
	 * @return la lista de valores de dominio cuyo valor es del tipo del dominio
	 *         enviado.
	 */
	@Query(value = "SELECT v FROM ValorDominioNegocio v WHERE v.dominio.codDominio = :codDominio AND v.idValorDominio = :idValorDominio AND v.activo = true")
	public ValorDominioNegocio buscarValorDominioPorCodDominioYValorDominio(
			@Param("codDominio") String codigoDominio, @Param("idValorDominio") Integer idValorDominio);
	
	/**
	 * Busca todos los registros activos por el codigo de dominio solicitado.
	 * 
	 * @author efarias
	 * @param codigoDominio
	 * @return
	 */
	@Query(value = "SELECT v FROM ValorDominioNegocio v WHERE v.dominio.codDominio = :codDominio AND v.activo = true")
	public List<ValorDominioNegocio> buscarPorCodigoDominio(@Param("codDominio") String codigoDominio);

	public List<ValorDominioNegocio> findByDominioCodDominioAndActivo(String codDominio, Boolean activo);

	public ValorDominioNegocio findByDominioCodDominioAndValorIgnoreCaseAndActivo(String codigoDominio, String valorDominio,
			Boolean activo);

	public ValorDominioNegocio findByDominioCodDominioAndIdValorDominio(String codigoDominio, Integer idValorDominio);

	public List<ValorDominioNegocio> findByDominio_CodDominioAndRelacionEntreDominiosList1_ValorDominioNegocio_DescripcionAndRelacionEntreDominiosList1_Valor(
			String codigoRolTercero, String codigo, String valor);

	public ValorDominioNegocio findByDominio_codDominioAndDescripcionIgnoreCaseAndActivo(String codigo, String descripcion,
			Boolean activo);

}
