package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.MiembroComite;
import com.accionfiduciaria.modelo.entidad.MiembroComitePK;
@Repository
public interface RepositorioMiembroComite extends CrudRepository<MiembroComite, MiembroComitePK> {
	
	@Query(value = "SELECT mc FROM MiembroComite mc WHERE mc.miembroComitePK.codSfc = :codigoNegocio AND mc.estado='ACTIVO'")
	public List <MiembroComite> buscarMiembroPorNegocio(@Param("codigoNegocio") String codSFC);

	public List<MiembroComite> findByRolAndEstadoIgnoreCase(Integer idRol, String string);
}
