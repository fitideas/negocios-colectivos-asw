package com.accionfiduciaria.negocio.repositorios;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.FechaClave;
import com.accionfiduciaria.modelo.entidad.FechaClavePK;
import com.accionfiduciaria.modelo.entidad.Negocio;

@Repository
public interface RepositorioFechaClave extends CrudRepository<FechaClave, FechaClavePK> {

	List<FechaClave> findByNegocioAndActivo(Negocio negocio, Boolean activo);

	List<FechaClave> findByTipoAndActivo(Integer idEliminar, Boolean activo);

	List<FechaClave> findByFechaClavePK_fechaGreaterThanEqualAndActivo(Date fechaActual, Boolean activo);

}
