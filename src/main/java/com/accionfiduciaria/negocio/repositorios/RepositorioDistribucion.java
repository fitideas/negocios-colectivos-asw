/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Distribucion;
import com.accionfiduciaria.modelo.entidad.DistribucionPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioDistribucion extends CrudRepository<Distribucion, DistribucionPK> ,
 JpaSpecificationExecutor<Distribucion>{

	@Query(value = "SELECT d FROM Distribucion d WHERE d.distribucionPK.codSfc = :codigoNegocio AND d.distribucionPK.tipoNegocio = :tipoNegocio AND d.distribucionPK.periodo = :periodo AND estado = :estado")
	public List<Distribucion> buscarDistribucionPorEstado(@Param("codigoNegocio") String codigoNegocio,
			@Param("tipoNegocio") Integer tipoNegocio, @Param("periodo") String periodo,
			@Param("estado") String estado);
	
	public List<Distribucion> findAll();
	
	
}
