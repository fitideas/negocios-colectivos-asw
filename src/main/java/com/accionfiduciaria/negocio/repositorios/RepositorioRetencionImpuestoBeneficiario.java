/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.accionfiduciaria.modelo.entidad.RetencionImpuestoBeneficiario;
import com.accionfiduciaria.modelo.entidad.RetencionImpuestoBeneficiarioPK;

/**
 * @author efarias
 *
 */
public interface RepositorioRetencionImpuestoBeneficiario
		extends CrudRepository<RetencionImpuestoBeneficiario, RetencionImpuestoBeneficiarioPK> {

	@Modifying
	@Query(value = "DELETE RetencionImpuestoBeneficiario WHERE retencionImpuestoBeneficiarioPK = :retencionImpuestoBeneficiarioPK")
	public void eliminarRetencionImpuestoBeneficiario(
			@Param("retencionImpuestoBeneficiarioPK") RetencionImpuestoBeneficiarioPK retencionImpuestoBeneficiarioPK);

}
