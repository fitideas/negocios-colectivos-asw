package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.TipoFideicomiso;

@Repository
public interface RepositorioTipoFideicomiso extends CrudRepository<TipoFideicomiso, String>{
	
	List<TipoFideicomiso> findAll();
	
}
