package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;

import com.accionfiduciaria.modelo.entidad.ParticipacionesNegocio;
import com.accionfiduciaria.modelo.entidad.TitularPK;

public interface RepositorioParticipacionesNegocio extends CrudRepository<ParticipacionesNegocio, Integer> {
	
	public ParticipacionesNegocio findByTitularesTitularPK(TitularPK titularPk);
}
