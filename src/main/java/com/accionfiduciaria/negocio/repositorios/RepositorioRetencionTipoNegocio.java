package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.RetencionTipoNegocio;
import com.accionfiduciaria.modelo.entidad.RetencionTipoNegocioPK;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

@Repository
public interface RepositorioRetencionTipoNegocio extends CrudRepository<RetencionTipoNegocio, RetencionTipoNegocioPK> {

	public List<RetencionTipoNegocio> findByTipoNegocioAndActivo(TipoNegocio tipoNegocio, Boolean activo);

	public List<RetencionTipoNegocio> findByTipoNegocio_NegocioAndActivo(Negocio negocio, Boolean activo);

	public List<RetencionTipoNegocio> findByRetencionTipoNegocioPK_TipoNegocioAndActivo(Integer idTipoNegocio,
			Boolean activo);

	public List<RetencionTipoNegocio> findByRetencionTipoNegocioPK_idRetencionAndActivo(Integer idRetencion,
			Boolean activo);

}
