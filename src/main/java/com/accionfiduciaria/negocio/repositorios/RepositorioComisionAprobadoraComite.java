package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.ComisionAprobadoraComite;
import com.accionfiduciaria.modelo.entidad.ComisionAprobadoraComitePK;

@Repository
public interface RepositorioComisionAprobadoraComite  extends CrudRepository<ComisionAprobadoraComite, ComisionAprobadoraComitePK> {

}
