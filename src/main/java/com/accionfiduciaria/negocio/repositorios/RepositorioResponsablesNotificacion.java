/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.ResponsableNotificacion;
import com.accionfiduciaria.modelo.entidad.ResponsableNotificacionPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioResponsablesNotificacion
		extends CrudRepository<ResponsableNotificacion, ResponsableNotificacionPK> {

	@Query(value = "SELECT r FROM ResponsableNotificacion r WHERE r.activo = :estado")
	public List<ResponsableNotificacion> obtenerResponsablesPorEstado(@Param("estado") boolean estado);

}
