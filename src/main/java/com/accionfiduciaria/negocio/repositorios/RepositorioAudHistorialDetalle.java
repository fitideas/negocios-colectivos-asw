package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.AudHistorialDetalle;
import com.accionfiduciaria.modelo.entidad.AudHistorialDetallePK;

@Repository
public interface  RepositorioAudHistorialDetalle extends CrudRepository <AudHistorialDetalle , AudHistorialDetallePK>, JpaSpecificationExecutor<AudHistorialDetalle>{



}
