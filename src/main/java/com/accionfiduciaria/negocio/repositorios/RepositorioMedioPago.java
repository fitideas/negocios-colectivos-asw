package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.MedioPago;
import com.accionfiduciaria.modelo.entidad.MedioPagoPK;

@Repository
public interface RepositorioMedioPago
		extends CrudRepository<MedioPago, MedioPagoPK>, JpaRepository<MedioPago, MedioPagoPK> {

}
