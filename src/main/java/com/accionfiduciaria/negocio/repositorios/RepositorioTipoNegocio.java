package com.accionfiduciaria.negocio.repositorios;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;
import com.accionfiduciaria.modelo.entidad.TipoNegocioPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioTipoNegocio extends CrudRepository<TipoNegocio, TipoNegocioPK> {
	
	public List<TipoNegocio> findByNegocioAndActivo(Negocio negocio, Boolean activo);
	
	public List<TipoNegocio> findByNegocioCodSfcAndActivo(String codigoNegocio, Boolean activo);
	 
	@Query(value = "SELECT t FROM TipoNegocio t where t.tipoNegocioPK.tipoNegocio  IN :tipos and t.tipoNegocioPK.codSfc in :negocios")
	public List<TipoNegocio> findTipoNegocioCodsfc(@Param("tipos") Collection<Integer> tipo,
			@Param("negocios") Collection<String > codSFC);
	
	@Query(value = "SELECT t FROM TipoNegocio t where t.tipoNegocioPK.tipoNegocio  IN :tipos")
	public List<TipoNegocio> findTipoNegocio(@Param("tipos") Collection<Integer> tipo);

	public List<TipoNegocio> findByTipoNegocioPK_TipoNegocioAndActivo(Integer idTipoNegocio, Boolean activo);
	
}
