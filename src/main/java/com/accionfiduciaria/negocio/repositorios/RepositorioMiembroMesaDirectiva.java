package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.MiembroMesaDirectiva;


@Repository
public interface RepositorioMiembroMesaDirectiva extends CrudRepository<MiembroMesaDirectiva, Integer> {

}
