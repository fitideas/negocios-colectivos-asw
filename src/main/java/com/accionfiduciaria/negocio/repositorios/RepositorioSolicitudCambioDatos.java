package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.SolicitudCambiosDatos;

/**
 * Repositorio encargado de realizar las operaciones CRUD a la tabla {@code BEN_SOLICITUD_CAMBIOS_DATOS}.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public interface RepositorioSolicitudCambioDatos extends CrudRepository<SolicitudCambiosDatos, Integer>, JpaSpecificationExecutor<SolicitudCambiosDatos>{

	/**
	 * Consulta todas las solicitudes de cambios de datos de un vinculado.
	 * 
	 * @param vinculado La persona a la que se le desea consultar el listado de solicitudes.
	 * @return La lista de solicitudes del vinculado enviado.
	 */
	public List<SolicitudCambiosDatos> findByPersonaVinculado(Persona vinculado);
	
	public SolicitudCambiosDatos findFirstByOrderByFechaSolicitudDesc();
	
	public SolicitudCambiosDatos findFirstByOrderByFechaSolicitudAsc();

}
