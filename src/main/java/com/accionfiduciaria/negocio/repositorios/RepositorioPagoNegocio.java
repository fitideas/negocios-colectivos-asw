/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.PagoNegocio;
import com.accionfiduciaria.modelo.entidad.PagoNegocioPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioPagoNegocio extends PagingAndSortingRepository<PagoNegocio, PagoNegocioPK>, JpaSpecificationExecutor<PagoNegocio> {
	

}
