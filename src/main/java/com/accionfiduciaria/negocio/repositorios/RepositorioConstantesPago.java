package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;

import com.accionfiduciaria.modelo.entidad.ConstantesPago;

public interface RepositorioConstantesPago extends CrudRepository<ConstantesPago, String> {

}
