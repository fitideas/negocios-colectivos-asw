package com.accionfiduciaria.negocio.repositorios;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Reunion;
import com.accionfiduciaria.modelo.entidad.ReunionPK;

@Repository
public interface RepositorioReunion extends CrudRepository<Reunion, ReunionPK>,  JpaSpecificationExecutor<Reunion> {

	public List<Reunion> findByNegocio_codSfcAndTipo(String codigoNegocio, String tipo);
	
	Reunion findFirstByIdActaAndTipoAndReunionPk_CodigoSfcAndReunionPk_FechaGreaterThanEqual(String idActa, String tipo, String codigoSFC, LocalDateTime fecha);

}
