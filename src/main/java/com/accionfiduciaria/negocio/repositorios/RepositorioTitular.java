package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.accionfiduciaria.modelo.entidad.ParticipacionesNegocio;
import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.Titular;
import com.accionfiduciaria.modelo.entidad.TitularPK;

public interface RepositorioTitular extends JpaRepository<Titular, TitularPK> { 

	public List<Titular> findByParticipacionAndEsCotitular(ParticipacionesNegocio participacion, boolean esCotitular);

	public List<Titular> findByParticipacionAndPersonaNombreCompletoContainingIgnoreCase(
			ParticipacionesNegocio participacion, String nombre);

	public List<Titular> findByParticipacionAndPersona_PersonaPK_NumeroDocumentoContainingIgnoreCase(
			ParticipacionesNegocio participacion, String documento);
	
	public List<Titular> findByTitularPK_CodSfcContainingIgnoreCase(String codSfc);
	
	public List<Titular> findByTitularPK_NumeroDocumento(String numeroDocumento);
	
	@Query(value = "SELECT t FROM Titular t WHERE persona = :persona AND participacion = :participacion")
	public List<Titular> buscarPorPersonaParticipacionNegocio(@Param("persona") Persona persona,
			@Param("participacion") ParticipacionesNegocio participacion);

	public List<Titular> findByPersona(Persona persona);
	
	public List<Titular> findByTitularPK_CodSfcAndActivo (String codigoNegocio, Boolean activo);
	
	public Titular findByTitularPK_CodSfcAndTitularPK_TipoDocumentoAndTitularPK_NumeroDocumento (String codigoNegocio, 
															String tipoDocumento, String numeroDocumento);

	public List<Titular> findByIdTipoNaturalezaJuridicaAndActivo(Integer idTipoNaturalezaNegocio, Boolean activo);

	public Page<Titular> findByPersona_nombreCompletoContainingIgnoreCase(String nombre, Pageable pageable);
	
	public Page<Titular> findByPersona(Persona persona, Pageable pageable);

	public Page<Titular> findByTitularPK_CodSfc(String codigoNegocio, Pageable pageable);

	public Page<Titular> findByTitularPK_numeroDocumento(String numeroDocumento, Pageable pageable);
	
}
