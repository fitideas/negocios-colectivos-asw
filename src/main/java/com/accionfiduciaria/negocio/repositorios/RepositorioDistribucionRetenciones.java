/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.DistribucionRetenciones;
import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioDistribucionRetenciones
		extends CrudRepository<DistribucionRetenciones, DistribucionRetencionesPK> {

}
