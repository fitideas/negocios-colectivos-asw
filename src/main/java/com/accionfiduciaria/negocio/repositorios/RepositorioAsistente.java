package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Asistente;
import com.accionfiduciaria.modelo.entidad.AsistentePK;

@Repository
public interface RepositorioAsistente extends CrudRepository<Asistente, AsistentePK> {

}
