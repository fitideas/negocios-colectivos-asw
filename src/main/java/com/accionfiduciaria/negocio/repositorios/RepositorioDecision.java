package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Decision;

@Repository
public interface RepositorioDecision extends CrudRepository<Decision, Integer> {

}
