package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;

import com.accionfiduciaria.modelo.entidad.Persona;
import com.accionfiduciaria.modelo.entidad.PersonaPK;

public interface RepositorioPersona extends CrudRepository<Persona, PersonaPK> {

}
