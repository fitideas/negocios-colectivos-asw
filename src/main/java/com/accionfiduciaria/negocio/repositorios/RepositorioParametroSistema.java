/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import com.accionfiduciaria.modelo.entidad.ParametroSistema;

/**
 * @author José Santiago Polo Acosta - 11/12/2019 - jpolo@asesoftware.com
 *
 */
public interface RepositorioParametroSistema extends CrudRepository<ParametroSistema, Integer> {	
}
