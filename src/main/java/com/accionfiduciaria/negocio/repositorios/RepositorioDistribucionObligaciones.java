/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.DistribucionObligacion;
import com.accionfiduciaria.modelo.entidad.DistribucionObligacionPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioDistribucionObligaciones extends CrudRepository<DistribucionObligacion, DistribucionObligacionPK> {

}
