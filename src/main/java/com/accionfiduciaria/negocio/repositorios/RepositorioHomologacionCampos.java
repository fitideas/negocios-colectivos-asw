package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.HomologacionCampos;

@Repository
public interface RepositorioHomologacionCampos extends CrudRepository<HomologacionCampos, String> {

}
