package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Negocio;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocio;
import com.accionfiduciaria.modelo.entidad.ObligacionNegocioPK;
import com.accionfiduciaria.modelo.entidad.TipoNegocio;

@Repository
public interface RepositorioObligacionNegocio extends CrudRepository<ObligacionNegocio, ObligacionNegocioPK> {
	
	List<ObligacionNegocio> findByTipoNegocioNegocioAndActivo(Negocio negocio, Boolean activo);

	List<ObligacionNegocio> findByTipoNegocioAndActivo(TipoNegocio tipoNegocio, Boolean activo);

	List<ObligacionNegocio> findByObligacionNegocioPK_tipoNegocioAndActivo(Integer idTipoNegocio, Boolean activo);

	List<ObligacionNegocio> findByObligacionNegocioPK_tipoPagoAndActivo(Integer idTipoGasto, Boolean activo);
}
