package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.accionfiduciaria.modelo.entidad.RelacionEntreDominios;
import com.accionfiduciaria.modelo.entidad.RelacionEntreDominiosPK;

/**
 * Repositorio para el manejo de la tabla RELACION_ENTRE_DOMINIOS
 * 
 * @author jpolo
 *
 */
public interface RepositorioRelacionEntreDominios extends CrudRepository<RelacionEntreDominios, RelacionEntreDominiosPK>{

	/**
	 * Verifica si un valor de dominio esta relacionado con algún otro valor.
	 * 
	 * @param id El id del valor de dominio a verificar.
	 * @return Un booleano indicando si el valor de dominio tiene alguna relación con otro valor. 
	 */
	public boolean existsRelacionEntreDominiosByValorDominioNegocioIdValorDominio(Integer id);

	@Query(value="SELECT R FROM RelacionEntreDominios R WHERE valorDominioNegocio.idValorDominio = :idValorDominio OR valorDominioNegocio1.idValorDominio = :idValorDominio")
	public List<RelacionEntreDominios> obtenerDominiosEliminar(@Param("idValorDominio") Integer idValorDominio);
	
	/**
	 * Elimina los registros relacionados a un valor de dominio.
	 * 
	 * @param idValorDominio El id del valor de dominio a eliminar.
	 */
	@Modifying
	@Query(value="DELETE RelacionEntreDominios WHERE valorDominioNegocio.idValorDominio = :idValorDominio OR valorDominioNegocio1.idValorDominio = :idValorDominio")
	public void eliminarRelacionDominio(@Param("idValorDominio") Integer idValorDominio);


	/**
	 * Busca los registros relacionados a un valor de dominio.
	 * 
	 * @param idValorDominio El id del valor de dominio a consultar.
	 * @return La lista de registros relacionados al valor de dominio enviado.
	 */
	@Query(value="SELECT R FROM RelacionEntreDominios R WHERE valorDominioNegocio.idValorDominio = :idValorDominio OR valorDominioNegocio1.idValorDominio = :idValorDominio")
	public List<RelacionEntreDominios> buscarRelacionesValorDominio(@Param("idValorDominio") Integer idValorDominio);

	/**
	 * 
	 * @param idValorDominio
	 * @return
	 */
	public List<RelacionEntreDominios> findByValorDominioNegocio1IdValorDominioAndValor(Integer idValorDominio, String valor);
	
	@Query(value = "SELECT r FROM RelacionEntreDominios r WHERE r.relacionEntreDominiosPK.idValorDominio = :idValorDominio AND r.valorDominioNegocio1.dominio.codDominio = :codDominio")
	public List<RelacionEntreDominios> buscarRelacionDominioPorIdValorDominioYCodigoDominio(
			@Param("idValorDominio") Integer idValorDominio, @Param("codDominio") String codDominio);
}
