package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Encargo;
import com.accionfiduciaria.modelo.entidad.EncargoPK;

@Repository
public interface RepositorioEncargo extends CrudRepository<Encargo, EncargoPK> {
	
	public List<Encargo> findByNegocio_codSfc(String codigoNegocio);
	
}
