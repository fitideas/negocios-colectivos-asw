package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Negocio;

/**
 * Repositorio de la entidad {@link Negocio}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Repository
public interface RepositorioNegocio extends CrudRepository<Negocio, String> {
	
	/**
	 * Busca un negocio por el codigo SFC.
	 * 
	 * @param codigoSFC El codigo SFC del negocio.
	 * @return Los datos del negocio encontrado. Null en caso de que no se encuentre.
	 */
	public Negocio findByCodSfc(String codigoSFC);
	
	
	//Método que permite traer todos los negocios encontrados en la base de datos
	
	public List<Negocio> findAll();

	Page<Negocio> findByNombreContainingIgnoreCase(String nombreNegocio, Pageable pageable);

	Page<Negocio> findByTitulares_TitularPK_TipoDocumentoAndTitulares_TitularPK_NumeroDocumento(
			String tipoDocumento, String numeroDocumento, Pageable pageable);	
}
