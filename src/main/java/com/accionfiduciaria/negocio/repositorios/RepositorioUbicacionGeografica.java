/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.UbicacionGeografica;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioUbicacionGeografica extends CrudRepository<UbicacionGeografica, String> {

	@Query(value = "SELECT u FROM UbicacionGeografica u WHERE u.tipo = :tipoUbicacion AND u.activo = true")
	public List<UbicacionGeografica> obtenerUbicacionesActivasPorTipo(@Param("tipoUbicacion") String tipoUbicacion);

	@Query(value = "SELECT u FROM UbicacionGeografica u WHERE u.tipo = :tipoUbicacion AND u.codigoPadre = :codigoPadre AND u.activo = true")
	public List<UbicacionGeografica> obtenerUbicacionesPorCodigoPadre(@Param("tipoUbicacion") String tipoUbicacion,
			@Param("codigoPadre") String codigoPadre);

	public UbicacionGeografica findByIso2AndActivo(String iso2, Boolean activo);

}
