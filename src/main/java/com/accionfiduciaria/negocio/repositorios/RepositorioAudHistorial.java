package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.AudHistorial;
import com.accionfiduciaria.modelo.entidad.AudHistorialPK;
@Repository
public interface RepositorioAudHistorial extends JpaRepository<AudHistorial, AudHistorialPK> , JpaSpecificationExecutor<AudHistorial>{

	public AudHistorial findFirstByOrderByAudHistorialPKFechaAsc();
	public AudHistorial findFirstByOrderByAudHistorialPKFechaDesc();

	
	
}
