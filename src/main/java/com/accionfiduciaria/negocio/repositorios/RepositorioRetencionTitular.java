package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.accionfiduciaria.modelo.entidad.RetencionTitular;
import com.accionfiduciaria.modelo.entidad.RetencionTitularPK;
import com.accionfiduciaria.modelo.entidad.Titular;

public interface RepositorioRetencionTitular extends CrudRepository<RetencionTitular, RetencionTitularPK> {
	
	public List<RetencionTitular> findByTitularAndActivo(Titular titular, Boolean activo);

	public List<RetencionTitular> findByRetencionTitularPK_tipoNaturalezaNegocioAndActivo(
			Integer idTipoNaturalezaNegocio, Boolean activo);

	public List<RetencionTitular> findByRetencionTitularPK_idRetencionAndActivo(Integer idRetencion, Boolean activo);

	public List<RetencionTitular> findByRetencionTitularPK_codSfcAndActivo(String codigoSfc, Boolean activo);

	public List<RetencionTitular> findByRetencionTitularPK_tipoNaturalezaNegocioAndTitularAndActivo(
			int idTipoNaturalezaNegocio, Titular titular, Boolean activo);

	public List<RetencionTitular> findByRetencionTitularPK_codSfcAndRetencionTitularPK_tipoNaturalezaNegocioAndActivo(
			String codigoSfc, Integer idTipoNegocio, Boolean activo);
}
