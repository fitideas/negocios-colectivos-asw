package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.accionfiduciaria.modelo.entidad.SubtipoFideicomiso;
import com.accionfiduciaria.modelo.entidad.SubtipoFideicomisoPK;

public interface RepositorioSubTipoFideicomiso extends CrudRepository<SubtipoFideicomiso, SubtipoFideicomisoPK> {

	public List<SubtipoFideicomiso> findByTipoFideicomiso_codigo(String codigoTipoFideicomiso);
	
}
