package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Beneficiario;
import com.accionfiduciaria.modelo.entidad.BeneficiarioPK;
import com.accionfiduciaria.modelo.entidad.Titular;

@Repository
public interface RepositorioBeneficiario
		extends CrudRepository<Beneficiario, BeneficiarioPK>, JpaRepository<Beneficiario, BeneficiarioPK> {

	public List<Beneficiario> findByTitularAndEstado(Titular titular, Boolean estado);

	@Query(value = "SELECT b FROM Beneficiario b WHERE b.beneficiarioPK.codSfc = :codigoNegocio AND b.estado = true")
	public List<Beneficiario> buscarBeneficiariosPorNegocio(@Param("codigoNegocio") String codigoNegocio);

}
