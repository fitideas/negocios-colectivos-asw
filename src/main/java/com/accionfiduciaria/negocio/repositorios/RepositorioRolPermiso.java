package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.RolPermiso;
import com.accionfiduciaria.modelo.entidad.RolPermisoPK;

@Repository
public interface RepositorioRolPermiso extends CrudRepository<RolPermiso, RolPermisoPK> {

	/**
	 * Consulta los permisos asignados a un rol.
	 * 
	 * @param idRol         El id del rol a consultar.
	 * @param estadoPermiso El estado de los permisos
	 *                      <ul>
	 *                      <li>true: activo</li>
	 *                      <li>false: inactivo</li>
	 *                      </ul>
	 * @return Una lista con los datos de los permisos asignados al rol enviado.
	 */
	public List<RolPermiso> findByPkIdRolAndPermisoEstado(Integer idRol, Boolean estadoPermiso);

	/**
	 * Consulta las funcionalidades asignados a un rol cuyo padre no sea nulo.
	 * 
	 * @param rol    El rol a consultar
	 * @param estado el estado de los permisos a consultar:
	 *               <ul>
	 *               <li>true: activo</li>
	 *               <li>false: inactivo</li>
	 *               </ul>
	 * @return Una lista con los permisos asignados al rol.
	 */
	public List<RolPermiso> findByRolNombreRolIgnoreCaseAndPermisoEstadoAndPermisoPermisoPadreNotNull(String rol, boolean estado);

}
