/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitular;
import com.accionfiduciaria.modelo.entidad.DistribucionRetencionesTitularPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioDistribucionRetencionesTitular
		extends JpaRepository<DistribucionRetencionesTitular, DistribucionRetencionesTitularPK>,
		JpaSpecificationExecutor<DistribucionRetencionesTitular>{

}
