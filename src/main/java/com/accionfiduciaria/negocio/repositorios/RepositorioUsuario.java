/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.Usuario;

/**
 * @author José Santiago Polo Acosta - 13/12/2019 - jpolo@asesoftware.com
 *
 */
@Repository
public interface RepositorioUsuario extends CrudRepository<Usuario, String> {

	Usuario findFirstByToken(String token);
}
