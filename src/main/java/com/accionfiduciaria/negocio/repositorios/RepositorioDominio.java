package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.accionfiduciaria.modelo.entidad.Dominio;
import com.accionfiduciaria.modelo.entidad.ValorDominioNegocio;

/**
 * Repositorio para el manejo de la tabla NEG_DOMINIOS
 * 
 * @author @author José Polo - jpolo@asesoftware.com
 *
 */
public interface RepositorioDominio extends CrudRepository<Dominio, String> {
	
	/**
	 * Consulta todos los dominios simples que se pueden mostrar en pantalla.
	 * 
	 * @return
	 * @throws Exception
	 */
	@Query(value = "SELECT DISTINCT d FROM Dominio d LEFT JOIN d.valoresDominio v WHERE v NOT IN (SELECT r.valorDominioNegocio1 FROM RelacionEntreDominios r) AND d.visible = true")
	public List<Dominio> obtenerDominiosSimples() throws Exception;
	
	/**
	 * Consulta todos los dominios compuestos que se pueden mostrar en pantalla, junto con las relaciones entre sus valores.
	 * 
	 * @return
	 * @throws Exception
	 */
	@Query(value = "SELECT DISTINCT d FROM Dominio d LEFT JOIN d.valoresDominio v WHERE v IN (SELECT r.valorDominioNegocio1 FROM RelacionEntreDominios r) AND d.visible = true")
	public List<Dominio> obtenerDominiosCompuestos() throws Exception;
	
	/**
	 * Consulta el dominio relacionado a un valor de dominio.
	 * 
	 * @param idValorDominio El id del valor de dominio.
	 * @return El Dominio relacionado al id del valor de dominio enviado.
	 */
	public Dominio findByValoresDominioIdValorDominio(Integer idValorDominio);
	
	@Query(value = "SELECT DISTINCT v FROM Dominio d LEFT JOIN d.valoresDominio v WHERE v IN (SELECT r.valorDominioNegocio1 FROM RelacionEntreDominios r) AND d.visible = true AND d.codDominio = :codigo")
	public List<ValorDominioNegocio> obtenerValoresDominioCompuesto(@Param("codigo") String codigo);
}
