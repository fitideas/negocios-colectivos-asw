/**
 * 
 */
package com.accionfiduciaria.negocio.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.HistorialNotificaciones;
import com.accionfiduciaria.modelo.entidad.HistorialNotificacionesPK;

/**
 * @author efarias
 *
 */
@Repository
public interface RepositorioHistorialNotificaciones
		extends CrudRepository<HistorialNotificaciones, HistorialNotificacionesPK> {

}
