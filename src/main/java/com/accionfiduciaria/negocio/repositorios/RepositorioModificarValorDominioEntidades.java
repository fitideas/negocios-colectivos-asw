package com.accionfiduciaria.negocio.repositorios;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repositorio encargado de realizar las modificaciones de los valores de
 * dominios en las tablas que hacen uso de esos dominios.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Component
@Transactional
public class RepositorioModificarValorDominioEntidades {

	@Autowired
	EntityManager entityManager;

	/**
	 * Inactiva un valor de dominio en una tabla.
	 * 
	 * @param nombreTabla El nombre de la tabla a la que se le intactivara el dominio.
	 * @param idValorDominio El id del valor de dominio a inactivar.
	 */
	public void inactivarValorDominiosDeEntidades(String nombreTabla, Integer idValorDominio) {
		Query query = entityManager
				.createNativeQuery("UPDATE " + nombreTabla + " SET ACTIVO = 0 WHERE ID_VALOR_DOMINIO = ?");
		query.setParameter(1, idValorDominio);
		query.executeUpdate();
	}
}