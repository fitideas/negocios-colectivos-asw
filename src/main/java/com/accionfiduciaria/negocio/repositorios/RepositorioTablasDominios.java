package com.accionfiduciaria.negocio.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accionfiduciaria.modelo.entidad.TablasDominios;
import com.accionfiduciaria.modelo.entidad.TablasDominiosPK;

@Repository
public interface RepositorioTablasDominios extends CrudRepository<TablasDominios, TablasDominiosPK> {
	
	/**
	 * Busca las tablas que hacen uso de un dominio determinado.
	 * 
	 * @param codDominio El código del dominio a consultar.
	 * @return La lista de tablas que utilizan el dominio enviado.
	 */
	public List<TablasDominios> findByTablasDominiosPK_CodDominio(String codDominio);
}
