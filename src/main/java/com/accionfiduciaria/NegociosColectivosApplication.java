package com.accionfiduciaria;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Clase encargada de iniciar la aplicación.
 * 
 * @author José Santiago Polo Acosta - 02/12/2019 - jpolo@asesoftware.com
 *
 */
@SpringBootApplication
@EnableScheduling
public class NegociosColectivosApplication {
		
	private static Logger log = LogManager.getLogger(NegociosColectivosApplication.class);

	public static void main(String[] args) {
		
		log.info("Starting NegociosColectivosApplication");
		log.info("===========================================");
		
		SpringApplication.run(NegociosColectivosApplication.class, args);
	}

}
