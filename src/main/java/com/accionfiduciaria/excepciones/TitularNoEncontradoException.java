package com.accionfiduciaria.excepciones;

public class TitularNoEncontradoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8564632217769098665L;

	public TitularNoEncontradoException() {
		super();
	}
	
	public TitularNoEncontradoException(String mensaje) {
		super(mensaje);
	}
}
