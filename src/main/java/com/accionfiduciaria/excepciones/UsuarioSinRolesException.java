/**
 * 
 */
package com.accionfiduciaria.excepciones;

/**
 * @author José Santiago Polo Acosta - 12/12/2019 - jpolo@asesoftware.com
 *
 */
public class UsuarioSinRolesException extends Exception {
	
	private static final long serialVersionUID = -7071300623844837525L;

	public UsuarioSinRolesException() {
		super();
	}
	
	public UsuarioSinRolesException(String mensaje) {
		super(mensaje);
	}
}
