package com.accionfiduciaria.excepciones;

public class ParametroNoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = -7897022411500349472L;

	public ParametroNoEncontradoException() {
		super();
	}
	
	public ParametroNoEncontradoException(String mensaje) {
		super(mensaje);
	}

}
