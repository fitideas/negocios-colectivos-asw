package com.accionfiduciaria.excepciones;

public class ServiceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServiceNotFoundException() {
		super("No se ha encontrado el servicio");
	}

	public ServiceNotFoundException(String mensaje) {
		super(mensaje);
	}

}
