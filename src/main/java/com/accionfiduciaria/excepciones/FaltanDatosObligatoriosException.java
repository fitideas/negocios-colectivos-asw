package com.accionfiduciaria.excepciones;

/**
 * Excepción que se lanzara cuando algún campo obligatorio llegue en nulo.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class FaltanDatosObligatoriosException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6865019454781701116L;

	public FaltanDatosObligatoriosException() {
		super();
	}

	public FaltanDatosObligatoriosException(String mensaje) {
		super(mensaje);
	}
}
