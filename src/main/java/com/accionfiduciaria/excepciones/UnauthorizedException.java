/**
 * 
 */
package com.accionfiduciaria.excepciones;

/**
 * @author José Santiago Polo Acosta - 11/12/2019 - jpolo@asesoftware.com
 *
 */
public class UnauthorizedException extends RuntimeException {

	private static final long serialVersionUID = 8832590032113238756L;

	public UnauthorizedException() {
		super();
	}
	
	public UnauthorizedException(String mensaje) {
		super(mensaje);
	}

}