/**
 * 
 */
package com.accionfiduciaria.excepciones;

/**
 * @author José Santiago Polo Acosta - 05/12/2019 - jpolo@asesoftware.com
 *
 */
public class ErrorAlConectarAccionException extends RuntimeException {
	
	private static final long serialVersionUID = 1134537503412134902L;

	public ErrorAlConectarAccionException() {
		super("Error al consultar los servicios del bus, por favor comuniquese con mesa de ayuda.");
	}
	
	public ErrorAlConectarAccionException(String mensaje) {
		super(mensaje);
	}
}
