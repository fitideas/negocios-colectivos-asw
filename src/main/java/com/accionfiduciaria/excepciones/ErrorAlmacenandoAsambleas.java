package com.accionfiduciaria.excepciones;

public class ErrorAlmacenandoAsambleas extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ErrorAlmacenandoAsambleas() {
		super();
	}
	
	public ErrorAlmacenandoAsambleas(String mensaje) {
		super(mensaje);
	}

}
