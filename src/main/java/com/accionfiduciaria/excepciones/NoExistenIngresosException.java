/**
 * 
 */
package com.accionfiduciaria.excepciones;

/**
 * Excepción que se lanzará cuando no existan ingresos para un periodo y tipo de
 * negocio determinado.
 * 
 * @author efarias
 *
 */
public class NoExistenIngresosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoExistenIngresosException() {
		super();
	}

	public NoExistenIngresosException(String mensaje) {
		super(mensaje);
	}

}
