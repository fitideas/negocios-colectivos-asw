/**
 * 
 */
package com.accionfiduciaria.excepciones;

/**
 * Excepción que se lanzará cuando no existan un registro en la entidad
 * NEG_DISTRIBUCION en estado DISTRIBUCION DISPONIBLE.
 * 
 * @author efarias
 *
 */
public class NoExisteDistribucionDisponible extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoExisteDistribucionDisponible() {
		super();
	}

	public NoExisteDistribucionDisponible(String mensaje) {
		super(mensaje);
	}

}
