package com.accionfiduciaria.excepciones;

public class PorcentajeGiroBeneficiariosNoValidoException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6722870552015792888L;

	public PorcentajeGiroBeneficiariosNoValidoException() {
		super();
	}
	
	public PorcentajeGiroBeneficiariosNoValidoException(String mensaje) {
		super(mensaje);
	}
}
