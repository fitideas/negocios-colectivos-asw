package com.accionfiduciaria.excepciones;

public class TitularSinBeneficiariosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5739534537963044155L;

	public TitularSinBeneficiariosException() {
	}

	public TitularSinBeneficiariosException(String message) {
		super(message);
	}

}
