package com.accionfiduciaria.excepciones;

/**
 * Excepción que se lanza cuando el rol consultado no tiene permisos asignados.
 * 
 * @author José Santiago Polo Acosta - 05/12/2019 - jpolo@asesoftware.com
 *
 */
public class UsuarioSinPermisosException extends Exception {
	
	private static final long serialVersionUID = 7585796349124684720L;

	/**
	 * 
	 */
	public UsuarioSinPermisosException() {
		super();
	}
	
	/**
	 * Constructor de la clase.
	 * 
	 * @param mensaje Mensaje que se enviara en la excepción.
	 */
	public UsuarioSinPermisosException(String mensaje) {
		super(mensaje);
	}
}
