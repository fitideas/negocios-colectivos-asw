/**
 * 
 */
package com.accionfiduciaria.excepciones;

/**
 * @author José Santiago Polo Acosta - 16/12/2019 - jpolo@asesoftware.com
 *
 */
public class BusquedadSinResultadosException extends Exception{

	private static final long serialVersionUID = 2838089145010452759L;

	public BusquedadSinResultadosException() {
		super();
	}
	
	public BusquedadSinResultadosException(String mensaje) {
		super(mensaje);
	}

}
