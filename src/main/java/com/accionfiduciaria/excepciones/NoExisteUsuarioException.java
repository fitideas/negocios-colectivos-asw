package com.accionfiduciaria.excepciones;

/**
 * Excepción que se lanzara en los casos que se consulte un usuario que no exista
 * en la base de datos.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class NoExisteUsuarioException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4624464060147070130L;

	public NoExisteUsuarioException() {
		super();
	}
	
	public NoExisteUsuarioException(String mensaje) {
		super(mensaje);
	}
	
}
