package com.accionfiduciaria.excepciones;

public class ArchivoNoValidoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6060458879375774192L;
	
	/**
	 * 
	 */
	public ArchivoNoValidoException() {
		super();
	}
	
	/**
	 * Constructor de la clase.
	 * 
	 * @param mensaje Mensaje que se enviara en la excepción.
	 */
	public ArchivoNoValidoException(String mensaje) {
		super(mensaje);
	}

}
