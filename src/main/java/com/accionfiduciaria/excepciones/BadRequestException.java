/**
 * 
 */
package com.accionfiduciaria.excepciones;

/**
 * @author José Santiago Polo Acosta - 11/12/2019 - jpolo@asesoftware.com
 *
 */
public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = 4180324197895497147L;

	public BadRequestException() {
		super();
	}
	
	/**
	 * @param mensaje
	 */
	public BadRequestException(String mensaje) {
		super(mensaje);
	}

}
