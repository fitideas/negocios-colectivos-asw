package com.accionfiduciaria.excepciones;

/**
 * 
 * @author jpolo
 *
 */
public class SesionCaducadaException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6308326632453155207L;

	public SesionCaducadaException() {
		super();
	}
	
	public SesionCaducadaException(String mensaje) {
		super(mensaje);
	}
}
