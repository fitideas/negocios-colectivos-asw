package com.accionfiduciaria.excepciones;

public class NoContentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoContentException() {
		super();
	}

	public NoContentException(String mensaje) {
		super(mensaje);
	}

}
