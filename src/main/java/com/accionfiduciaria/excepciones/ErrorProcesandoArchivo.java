package com.accionfiduciaria.excepciones;

public class ErrorProcesandoArchivo extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4932724100672566318L;
	
	public ErrorProcesandoArchivo() {
		super();
	}
	
	public ErrorProcesandoArchivo(String mensaje) {
		super(mensaje);
	}

}
