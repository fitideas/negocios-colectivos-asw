/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * @author efarias
 *
 */
public class ListaIngresosOperacionDTO {

	private String codigoNegocio;
	private String periodo;
	private List<IngresosTipoOperacionDTO> listaTipoNegocio;

	/**
	 * @return the codigoTipoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	/**
	 * @param codigoTipoNegocio the codigoTipoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	/**
	 * @return the listaTipoNegocio
	 */
	public List<IngresosTipoOperacionDTO> getListaTipoNegocio() {
		return listaTipoNegocio;
	}

	/**
	 * @param listaTipoNegocio the listaTipoNegocio to set
	 */
	public void setListaTipoNegocio(List<IngresosTipoOperacionDTO> listaTipoNegocio) {
		this.listaTipoNegocio = listaTipoNegocio;
	}

	/**
	 * @return the periodo
	 */
	public String getPeriodo() {
		return periodo;
	}

	/**
	 * @param periodo the periodo to set
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

}
