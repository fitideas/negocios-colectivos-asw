package com.accionfiduciaria.modelo.dtos;

/**
 * DTO utilizado para el envío de los parámetros necesarios para realizar la
 * consulta al servicio de Acción Fiduciaria de consulta de datos de un negocio
 * de un beneficiario.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class PeticionAccionDatosDistribucionDTO {

	/**
	 * El código SFC del negocio. 
	 */
	private String codigoSFC;
	/**
	 * El token de autenticacióin asignado al usuario.
	 */
	private String token;
	/**
	 * La url del servicio a consumir. Dentro de la url se envía el número de documento del titulat o cotitular del negocio: {@code /clientes/{idCliente}/negocios} 
	 */
	private String urlServicio;

	/**
	 * Constructor por defecto.
	 */
	public PeticionAccionDatosDistribucionDTO() {
	}

	/**
	 * Constructor que inicializa todos los eementos de la clase.
	 * 
	 * @param codigoSFC {@link #codigoSFC}
	 * @param token {@link #token}
	 * @param urlServicio {@link #urlServicio}
	 */
	public PeticionAccionDatosDistribucionDTO(String codigoSFC, String token, String urlServicio) {
		this.codigoSFC = codigoSFC;
		this.token = token;
		this.urlServicio = urlServicio;
	}

	/**
	 * @return the codigoSFC
	 */
	public String getCodigoSFC() {
		return codigoSFC;
	}

	/**
	 * @param codigoSFC the codigoSFC to set
	 */
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the urlServicio
	 */
	public String getUrlServicio() {
		return urlServicio;
	}

	/**
	 * @param urlServicio the urlServicio to set
	 */
	public void setUrlServicio(String urlServicio) {
		this.urlServicio = urlServicio;
	}
}
