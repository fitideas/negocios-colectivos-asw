package com.accionfiduciaria.modelo.dtos;

public class HistoricoReunionDTO {

	private String codigoSFC;
	private String fechaReunion;
	private String numeroRadicado;
	private String usuario;
	
	
	public String getCodigoSFC() {
		return codigoSFC;
	}
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}
	public String getFechaReunion() {
		return fechaReunion;
	}
	public void setFechaReunion(String fechaReunion) {
		this.fechaReunion = fechaReunion;
	}
	public String getNumeroRadicado() {
		return numeroRadicado;
	}
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	
}
