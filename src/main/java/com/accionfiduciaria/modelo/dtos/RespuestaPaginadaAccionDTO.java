package com.accionfiduciaria.modelo.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;

/**
 * DTO utilizado para obtener las respuestas paginadas de los servicios de Acción.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class RespuestaPaginadaAccionDTO {
	
	/**
	 * Inidica el máximo número de resultados 
	 */
	private int limit;
	
	/**
	 * La cantidad de objetos solicitados por página.
	 */
	private int size;
	
	/**
	 * Define cuál objeto es el primero en mostrarse de la lista, si la paginación
	 * es de 5 objetos por página y se establece en 0 se dice que se está en la
	 * primera página de 5 objetos. Por tanto para llamar la siguiente página este
	 * parámetro deberá definirse en 5 ya que no es inclusivo el registro.
	 */
	private int start;
	
	/**
	 * Es el LinkModel objeto que define los links para determinar el path actual,
	 * el recurso solicitado y el listado siguiente de objetos a retornar o el
	 * anterior
	 */
	@JsonAlias("_links")
	private LinksDTO links;
	
	private int totalPages;

	/**
	 * Constructor por defecto.
	 */
	public RespuestaPaginadaAccionDTO() {
	}

	/**
	 * Constructor que inicializa los campos: limit, size, start, links, results.
	 * 
	 * @param limit {@link #limit}
	 * @param size {@link #size}
	 * @param start {@link #start}
	 * @param links {@link #links}
	 */
	public RespuestaPaginadaAccionDTO(int limit, int size, int start, LinksDTO links) {
		this.limit = limit;
		this.size = size;
		this.start = start;
		this.links = links;
	}

	/**
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}

	/**
	 * @return the links
	 */
	public LinksDTO getLinks() {
		return links;
	}

	/**
	 * @param links the links to set
	 */
	public void setLinks(LinksDTO links) {
		this.links = links;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
