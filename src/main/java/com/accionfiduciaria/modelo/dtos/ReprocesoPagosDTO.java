/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * @author efarias
 *
 */
public class ReprocesoPagosDTO {

	private String codigoNegocio;
	private Integer codigoTipoNegocio;
	private String periodo;
	private List<ReprocesoPagoBeneficiarioDTO> listaReprocesoBeneficiarios;

	/**
	 * @return the codigoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	/**
	 * @param codigoNegocio the codigoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	/**
	 * @return the codigoTipoNegocio
	 */
	public Integer getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}

	/**
	 * @param codigoTipoNegocio the codigoTipoNegocio to set
	 */
	public void setCodigoTipoNegocio(Integer codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}

	/**
	 * @return the periodo
	 */
	public String getPeriodo() {
		return periodo;
	}

	/**
	 * @param periodo the periodo to set
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/**
	 * @return the listaReprocesoBeneficiarios
	 */
	public List<ReprocesoPagoBeneficiarioDTO> getListaReprocesoBeneficiarios() {
		return listaReprocesoBeneficiarios;
	}

	/**
	 * @param listaReprocesoBeneficiarios the listaReprocesoBeneficiarios to set
	 */
	public void setListaReprocesoBeneficiarios(List<ReprocesoPagoBeneficiarioDTO> listaReprocesoBeneficiarios) {
		this.listaReprocesoBeneficiarios = listaReprocesoBeneficiarios;
	}

}
