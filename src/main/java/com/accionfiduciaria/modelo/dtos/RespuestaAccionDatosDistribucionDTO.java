package com.accionfiduciaria.modelo.dtos;

/**
 * DTO utilizado para almacenar la respuesta a la consulta de datos de
 * distribución de un titular asociado a un negocio especifico.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class RespuestaAccionDatosDistribucionDTO {

	/**
	 * EL código de negocio
	 */
	private String codigo;
	/**
	 * El nombre de negocio.
	 */
	private String nombre;
	/**
	 * el código SFC del negocio.
	 */
	private String sfc;
	/**
	 * EL total de derechos del negocio.
	 */
	private String totalDerechos;
	/**
	 * El total de derechos del benficiario sobre el negocio.
	 */
	private String totalDerechosBeneficiario;
	/**
	 * El tipo de titularidad del beneficiario.
	 */
	private String tipoTitularidad;
	/**
	 * El tipo de naturaleza juridica del beneficiario.
	 */
	private String tipoNaturalezaJuridica;
	/**
	 * El estado del negocio.
	 */
	private EstadoNegocioDTO estadoNegocio;

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the sfc
	 */
	public String getSfc() {
		return sfc;
	}

	/**
	 * @param sfc the sfc to set
	 */
	public void setSfc(String sfc) {
		this.sfc = sfc;
	}

	/**
	 * @return the totalDerechos
	 */
	public String getTotalDerechos() {
		return totalDerechos;
	}

	/**
	 * @param totalDerechos the totalDerechos to set
	 */
	public void setTotalDerechos(String totalDerechos) {
		this.totalDerechos = totalDerechos;
	}

	/**
	 * @return the totalDerechosBeneficiario
	 */
	public String getTotalDerechosBeneficiario() {
		return totalDerechosBeneficiario;
	}

	/**
	 * @param totalDerechosBeneficiario the totalDerechosBeneficiario to set
	 */
	public void setTotalDerechosBeneficiario(String totalDerechosBeneficiario) {
		this.totalDerechosBeneficiario = totalDerechosBeneficiario;
	}

	/**
	 * @return the tipoTitularidad
	 */
	public String getTipoTitularidad() {
		return tipoTitularidad;
	}

	/**
	 * @param tipoTitularidad the tipoTitularidad to set
	 */
	public void setTipoTitularidad(String tipoTitularidad) {
		this.tipoTitularidad = tipoTitularidad;
	}

	/**
	 * @return the tipoNaturalezaJuridica
	 */
	public String getTipoNaturalezaJuridica() {
		return tipoNaturalezaJuridica;
	}

	/**
	 * @param tipoNaturalezaJuridica the tipoNaturalezaJuridica to set
	 */
	public void setTipoNaturalezaJuridica(String tipoNaturalezaJuridica) {
		this.tipoNaturalezaJuridica = tipoNaturalezaJuridica;
	}

	/**
	 * @return the estadoNegocio
	 */
	public EstadoNegocioDTO getEstadoNegocio() {
		return estadoNegocio;
	}

	/**
	 * @param estadoNegocio the estadoNegocio to set
	 */
	public void setEstadoNegocio(EstadoNegocioDTO estadoNegocio) {
		this.estadoNegocio = estadoNegocio;
	}

}
