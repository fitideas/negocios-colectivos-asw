/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

/**
 * DTO usado para enviar los datos de un usuario.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class UsuarioDTO {
	
	/**
	 * El login del usuario.
	 */
	@JsonAlias("usuario")
	private String nombreUsuario;
	/**
	 * La clave del usuario.
	 */
	private String clave;
	/**
	 * El nombre completo del usuario.
	 */
	private String nombreCompleto;
	/**
	 * La fecha de ultimo acceso al sistema del usuario.
	 */
	private String fechaUltimoAcceso;
	/**
	 * El token de autenticación del usuario.
	 */
	private String token;
	/*+
	 * El tiempo de vida del token actual del usuario.
	 */
	private String tiempoVidaToken;
	/**
	 * El listado de roles del usuario.
	 */
	private List<RolMenuDTO> roles;
	
	/**
	 * Constructor por defecto.
	 */
	public UsuarioDTO() {
	}
	
	/**
	 * Constructor que inicializa los campos login y clave.
	 * 
	 * @param nombreUsuario {@link #nombreUsuario}
	 * @param clave {@link #clave}
	 */
	public UsuarioDTO(String login, String clave) {
		this.nombreUsuario = login;
		this.clave = clave;
	}

	/**
	 * Constructor que inicializa los campos nombreCompleto, fechaUltimoAcceso y roles.
	 * 
	 * @param nombreCompleto {@link #nombreCompleto}
	 * @param fechaUltimoAcceso {@link #fechaUltimoAcceso}
	 * @param roles {@link #roles}
	 */
	public UsuarioDTO(String nombreCompleto, String fechaUltimoAcceso, List<RolMenuDTO> roles) {
		this.nombreCompleto = nombreCompleto;
		this.fechaUltimoAcceso = fechaUltimoAcceso;
		this.roles = roles;
	}

	/**
	 * Constructor que inicializa los campos nombreCompleto, fechaUltimoAcceso, roles, token y tiempoVidaToken.
	 * 
	 * @param nombreCompleto {@link #nombreCompleto}
	 * @param fechaUltimoAcceso {@link #fechaUltimoAcceso}
	 * @param roles {@link #roles}
	 * @param token {@link #token}
	 * @param tiempoVidaToken {@link #tiempoVidaToken}
	 */
	public UsuarioDTO(String nombreCompleto, String fechaUltimoAcceso, List<RolMenuDTO> roles, String token,
			String tiempoVidaToken) {
		this.nombreCompleto = nombreCompleto;
		this.fechaUltimoAcceso = fechaUltimoAcceso;
		this.roles = roles;
		this.token = token;
		this.tiempoVidaToken = tiempoVidaToken;
	}


	/**
	 * @return the roles
	 */
	public List<RolMenuDTO> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<RolMenuDTO> roles) {
		this.roles = roles;
	}

	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	/**
	 * @param nombreUsuario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the nombreCompleto
	 */
	public String getNombreCompleto() {
		return nombreCompleto;
	}

	/**
	 * @param nombreCompleto the nombreCompleto to set
	 */
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	/**
	 * @return the fechaUltimoAcceso
	 */
	public String getFechaUltimoAcceso() {
		return fechaUltimoAcceso;
	}

	/**
	 * @param fechaUltimoAcceso the fechaUltimoAcceso to set
	 */
	public void setFechaUltimoAcceso(String fechaUltimoAcceso) {
		this.fechaUltimoAcceso = fechaUltimoAcceso;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the tiempoVidaToken
	 */
	public String getTiempoVidaToken() {
		return tiempoVidaToken;
	}

	/**
	 * @param tiempoVidaToken the tiempoVidaToken to set
	 */
	public void setTiempoVidaToken(String tiempoVidaToken) {
		this.tiempoVidaToken = tiempoVidaToken;
	}

}