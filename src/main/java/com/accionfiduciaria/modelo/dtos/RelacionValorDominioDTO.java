package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

/**
 * Contiene los datos de un valor de dominio relacionado con otro valor de dominio. 
 * 
 * @author jpolo
 *
 */
public class RelacionValorDominioDTO {
	/**
	 * El id del valor de dominio
	 */
	private Integer id;
	/**
	 * La descripción del valor de dominio
	 */
	private String descripcion;
	/**
	 * El valor de la relación con el otro valor de dominio.
	 */
	private String valor;
	
	/**
	 * Constructor por defecto.
	 */
	public RelacionValorDominioDTO() {
	}
	
	/**
	 * Constructor que inicializa el id.
	 * 
	 * @param id {@link #id}
	 */
	public RelacionValorDominioDTO(Integer id) {
		this.id = id;
	}
	
	/**
	 * Constructor que inicializa los campos id, descripcion y valor. 
	 * 
	 * @param id {@link #id}
	 * @param descripcion {@link #descripcion}
	 * @param valor {@link #valor}
	 */
	public RelacionValorDominioDTO(Integer id, String descripcion, String valor) {
		this.id = id;
		this.descripcion = descripcion;
		this.valor = valor;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelacionValorDominioDTO other = (RelacionValorDominioDTO) obj;
		return Objects.equals(id, other.id);
	}
}
