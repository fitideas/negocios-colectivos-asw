package com.accionfiduciaria.modelo.dtos;


/**
 * DTO usado para el envío de los datos usados en el proceso de eliminación de valores de dominios (parámetros en la aplciación).
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class EliminacionDominioDTO {
	
	/**
	 * El id del valor de dominio a eliminar.
	 */
	private Integer idEliminar;
	/**
	 * El id del valor de dominio a reemplazar por el valor eliminado.
	 */
	private Integer idReemplazar;
	
	/**
	 * Constructor por defecto.
	 */
	public EliminacionDominioDTO() {
	}

	/**
	 * Constructor que inicializa los atributos idEliminar y idReemplazar.
	 * 
	 * @param idEliminar {@link #idEliminar}
	 * @param idReemplazar {@link #idReemplazar}
	 */
	public EliminacionDominioDTO(Integer idEliminar, Integer idReemplazar) {
		this.idEliminar = idEliminar;
		this.idReemplazar = idReemplazar;
	}

	/**
	 * @return the idEliminar
	 */
	public Integer getIdEliminar() {
		return idEliminar;
	}

	/**
	 * @param idEliminar the idEliminar to set
	 */
	public void setIdEliminar(Integer idEliminar) {
		this.idEliminar = idEliminar;
	}

	/**
	 * @return the idReemplazar
	 */
	public Integer getIdReemplazar() {
		return idReemplazar;
	}

	/**
	 * @param idReemplazar the idReemplazar to set
	 */
	public void setIdReemplazar(Integer idReemplazar) {
		this.idReemplazar = idReemplazar;
	}
}
