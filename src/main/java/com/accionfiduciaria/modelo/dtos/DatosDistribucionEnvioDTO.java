package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * DTO ultiizado para el envío de los datos de distribución al front.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DatosDistribucionEnvioDTO extends DatosDistribucionDTO {
	/**
	 * El nombre del negocio.
	 */
	private String nombreNegocio;
	
	/**
	 * El istado de retencionesTipoNaturaleza que le aplican.
	 */
	private List<DatosBasicosValorDominioDTO> retencionesTipoNaturaleza;
	
	private List<TipoNegocioDTO> retencionesTipoNegocio;

	/**
	 * @return the nombreNegocio
	 */
	public String getNombreNegocio() {
		return nombreNegocio;
	}

	/**
	 * @param nombreNegocio the nombreNegocio to set
	 */
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}

	/**
	 * @return the retencionesTipoNaturaleza
	 */
	public List<DatosBasicosValorDominioDTO> getRetencionesTipoNaturaleza() {
		return retencionesTipoNaturaleza;
	}

	/**
	 * @param retencionesTipoNaturaleza the retencionesTipoNaturaleza to set
	 */
	public void setRetencionesTipoNaturaleza(List<DatosBasicosValorDominioDTO> retenciones) {
		this.retencionesTipoNaturaleza = retenciones;
	}

	public List<TipoNegocioDTO> getRetencionesTipoNegocio() {
		return retencionesTipoNegocio;
	}

	public void setRetencionesNegocio(List<TipoNegocioDTO> retencionesNegocio) {
		this.retencionesTipoNegocio = retencionesNegocio;
	}
}
