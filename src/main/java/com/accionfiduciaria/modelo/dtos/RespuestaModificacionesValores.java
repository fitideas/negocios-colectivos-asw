package com.accionfiduciaria.modelo.dtos;

public class RespuestaModificacionesValores {
	private float proyectosActivos;
	private float proyectosEnLiquidacion;
	private float proyectosLiquidados;
	private float cambiosProyecto;
	private float cambiosMensual;
	private float cambiosAnual;
	private float avaluoInmuebles;
	private float valorPrediales;
	private float valorPredialesMensual;
	private float valorPredialesAnual;
	private float valorValorizacion;
	private float valorValorizacionMensual;
	private float valorValorizacionAnual;

	public RespuestaModificacionesValores() {
		super();
	}

	public float getProyectosActivos() {
		return proyectosActivos;
	}

	public void setProyectosActivos(float proyectosActivos) {
		this.proyectosActivos = proyectosActivos;
	}

	public float getProyectosEnLiquidacion() {
		return proyectosEnLiquidacion;
	}

	public void setProyectosEnLiquidacion(float proyectosEnLiquidacion) {
		this.proyectosEnLiquidacion = proyectosEnLiquidacion;
	}

	public float getProyectosLiquidados() {
		return proyectosLiquidados;
	}

	public void setProyectosLiquidados(float proyectosLiquidados) {
		this.proyectosLiquidados = proyectosLiquidados;
	}

	public float getCambiosProyecto() {
		return cambiosProyecto;
	}

	public void setCambiosProyecto(float cambiosProyecto) {
		this.cambiosProyecto = cambiosProyecto;
	}

	public float getCambiosMensual() {
		return cambiosMensual;
	}

	public void setCambiosMensual(float cambiosMensual) {
		this.cambiosMensual = cambiosMensual;
	}

	public float getCambiosAnual() {
		return cambiosAnual;
	}

	public void setCambiosAnual(float cambiosAnual) {
		this.cambiosAnual = cambiosAnual;
	}

	public float getAvaluoInmuebles() {
		return avaluoInmuebles;
	}

	public void setAvaluoInmuebles(float avaluoInmuebles) {
		this.avaluoInmuebles = avaluoInmuebles;
	}

	public float getValorPrediales() {
		return valorPrediales;
	}

	public void setValorPrediales(float valorPrediales) {
		this.valorPrediales = valorPrediales;
	}

	public float getValorPredialesMensual() {
		return valorPredialesMensual;
	}

	public void setValorPredialesMensual(float valorPredialesMensual) {
		this.valorPredialesMensual = valorPredialesMensual;
	}

	public float getValorPredialesAnual() {
		return valorPredialesAnual;
	}

	public void setValorPredialesAnual(float valorPredialesAnual) {
		this.valorPredialesAnual = valorPredialesAnual;
	}

	public float getValorValorizacion() {
		return valorValorizacion;
	}

	public void setValorValorizacion(float valorValorizacion) {
		this.valorValorizacion = valorValorizacion;
	}

	public float getValorValorizacionMensual() {
		return valorValorizacionMensual;
	}

	public void setValorValorizacionMensual(float valorValorizacionMensual) {
		this.valorValorizacionMensual = valorValorizacionMensual;
	}

	public float getValorValorizacionAnual() {
		return valorValorizacionAnual;
	}

	public void setValorValorizacionAnual(float valorValorizacionAnual) {
		this.valorValorizacionAnual = valorValorizacionAnual;
	}

	@Override
	public String toString() {
		return "RespuestaModificacionesValores [proyectosActivos=" + proyectosActivos + ", proyectosEnLiquidacion="
				+ proyectosEnLiquidacion + ", proyectosLiquidados=" + proyectosLiquidados + ", cambiosProyecto="
				+ cambiosProyecto + ", cambiosMensual=" + cambiosMensual + ", cambiosAnual=" + cambiosAnual
				+ ", avaluoInmuebles=" + avaluoInmuebles + ", valorPrediales=" + valorPrediales
				+ ", valorPredialesMensual=" + valorPredialesMensual + ", valorPredialesAnual=" + valorPredialesAnual
				+ ", valorValorizacion=" + valorValorizacion + ", valorValorizacionMensual=" + valorValorizacionMensual
				+ ", valorValorizacionAnual=" + valorValorizacionAnual + "]";
	}

}
