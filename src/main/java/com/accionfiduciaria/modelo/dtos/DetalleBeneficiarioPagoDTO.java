package com.accionfiduciaria.modelo.dtos;

import java.io.Serializable;

public class DetalleBeneficiarioPagoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombreTitular;
	private String tipoDocumentoTitular;
	private String numeroDocumentoTitular;
	private Float porcentajeParticipacion;
	private String nombreBeneficiario;
	private String tipoDocumentoBeneficiario;
	private String numeroDocumentoBeneficiario;
	private Float porcentajeGiro;
	private Float porcentajeDistribuido;
	private Float valorAGirar;
	private String fechaGiro;
	private String estado;

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	public String getNumeroDocumentoTitular() {
		return numeroDocumentoTitular;
	}

	public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
		this.numeroDocumentoTitular = numeroDocumentoTitular;
	}

	public Float getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(Float porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getTipoDocumentoBeneficiario() {
		return tipoDocumentoBeneficiario;
	}

	public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
		this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
	}

	public String getNumeroDocumentoBeneficiario() {
		return numeroDocumentoBeneficiario;
	}

	public void setNumeroDocumentoBeneficiario(String numeroDocumentoBeneficiario) {
		this.numeroDocumentoBeneficiario = numeroDocumentoBeneficiario;
	}

	public Float getPorcentajeGiro() {
		return porcentajeGiro;
	}

	public void setPorcentajeGiro(Float porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	public Float getPorcentajeDistribuido() {
		return porcentajeDistribuido;
	}

	public void setPorcentajeDistribuido(Float porcentajeDistribuido) {
		this.porcentajeDistribuido = porcentajeDistribuido;
	}

	public Float getValorAGirar() {
		return valorAGirar;
	}

	public void setValorAGirar(Float valorAGirar) {
		this.valorAGirar = valorAGirar;
	}

	public String getFechaGiro() {
		return fechaGiro;
	}

	public void setFechaGiro(String fechaGiro) {
		this.fechaGiro = fechaGiro;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
