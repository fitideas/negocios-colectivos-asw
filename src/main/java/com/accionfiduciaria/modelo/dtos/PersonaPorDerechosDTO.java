package com.accionfiduciaria.modelo.dtos;

public class PersonaPorDerechosDTO extends DatosBasicosPersonaDTO {

	private String participacion;
	private String numeroDerechos;
	private String baseRetencion;
	private String totalRetenido;
	private String rentaExcenta;
	private String codSFC;

	/**
	 * @param nombreCompleto
	 * @param tipoDocumento
	 * @param numeroDocumento
	 */
	public PersonaPorDerechosDTO(String nombreCompleto, String tipoDocumento, String numeroDocumento) {
		super(nombreCompleto, tipoDocumento, numeroDocumento);
	}

	public PersonaPorDerechosDTO(String nombreCompleto, String tipoDocumento, String numeroDocumento, String codSFC) {
		super(nombreCompleto, tipoDocumento, numeroDocumento);
		this.codSFC = codSFC;
	}

	/**
	 * @param nombreCompleto
	 * @param tipoDocumento
	 * @param numeroDocumento
	 * @param participacion
	 * @param numeroDerechos
	 * @param codSFC
	 */
	public PersonaPorDerechosDTO(String nombreCompleto, String tipoDocumento, String numeroDocumento,
			String participacion, String numeroDerechos, String codSFC) {
		super(nombreCompleto, tipoDocumento, numeroDocumento);
		this.participacion = participacion;
		this.numeroDerechos = numeroDerechos;
		this.codSFC = codSFC;
	}

	public String getParticipacion() {
		return participacion;
	}

	public void setParticipacion(String participacion) {
		this.participacion = participacion;
	}

	public String getNumeroDerechos() {
		return numeroDerechos;
	}

	public void setNumeroDerechos(String numeroDerechos) {
		this.numeroDerechos = numeroDerechos;
	}

	public String getBaseRetencion() {
		return baseRetencion;
	}

	public void setBaseRetencion(String baseRetencion) {
		this.baseRetencion = baseRetencion;
	}

	public String getTotalRetenido() {
		return totalRetenido;
	}

	public void setTotalRetenido(String totalRetenido) {
		this.totalRetenido = totalRetenido;
	}

	public String getRentaExcenta() {
		return rentaExcenta;
	}

	public void setRentaExcenta(String rentaExcenta) {
		this.rentaExcenta = rentaExcenta;
	}

	public String getCodSFC() {
		return codSFC;
	}

	public void setCodSFC(String codSFC) {
		this.codSFC = codSFC;
	}
}
