package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaPaginadaConsultaEncargosNegocioDTO extends RespuestaPaginadaAccionDTO {
	
	private List<RespuestaAccionBuscarEncargoDTO> results;

	public List<RespuestaAccionBuscarEncargoDTO> getResults() {
		return results;
	}

	public void setResults(List<RespuestaAccionBuscarEncargoDTO> results) {
		this.results = results;
	}
}
