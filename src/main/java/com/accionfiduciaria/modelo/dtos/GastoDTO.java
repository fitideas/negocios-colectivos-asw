package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class GastoDTO {
	private String nombre;
	private Integer idTipo;
	private String unidad;
	private Float valor;
	private List<Integer> periodicidad;
	private Integer diaPago;
	private String numeroRadicado;
	private String fechaVencimiento;

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the idTipo
	 */
	public Integer getIdTipo() {
		return idTipo;
	}

	/**
	 * @param idTipo the idTipo to set
	 */
	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}

	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	/**
	 * @return the valor
	 */
	public Float getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(Float valor) {
		this.valor = valor;
	}

	public List<Integer> getPeriodicidad() {
		return periodicidad;
	}

	public void setPeriodicidad(List<Integer> periodicidad) {
		this.periodicidad = periodicidad;
	}

	/**
	 * @return the diaPago
	 */
	public Integer getDiaPago() {
		return diaPago;
	}

	/**
	 * @param diaPago the diaPago to set
	 */
	public void setDiaPago(Integer diaPago) {
		this.diaPago = diaPago;
	}

	/**
	 * @return the numeroRadicado
	 */
	public String getNumeroRadicado() {
		return numeroRadicado;
	}

	/**
	 * @param numeroRadicado the numeroRadicado to set
	 */
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}
	
	/**
	 * @return the fechaVencimiento
	 */
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
}
