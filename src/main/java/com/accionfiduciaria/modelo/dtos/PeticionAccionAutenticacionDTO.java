/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author José Santiago Polo Acosta - 10/12/2019 - jpolo@asesoftware.com
 *
 */
public class PeticionAccionAutenticacionDTO {
	
	private String authorization;
	private String userName;
	private String password;
	private String appKey;
	private String urlServidorAutenticacion;
	
	/**
	 * 
	 */
	public PeticionAccionAutenticacionDTO() {
	}

	/**
	 * @param authorization
	 * @param userName
	 * @param password
	 * @param appKey
	 * @param urlServidorAutenticacion
	 */
	public PeticionAccionAutenticacionDTO(String authorization, String userName,
			String password, String appKey, String urlServidorAutenticacion) {
		this.authorization = authorization;
		this.userName = userName;
		this.password = password;
		this.appKey = appKey;
		this.urlServidorAutenticacion = urlServidorAutenticacion;
	}

	/**
	 * @return the authorization
	 */
	public String getAuthorization() {
		return authorization;
	}

	/**
	 * @param authorization the authorization to set
	 */
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the appKey
	 */
	public String getAppKey() {
		return appKey;
	}
	
	/**
	 * @param appKey the appKey to set
	 */
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	/**
	 * @return the urlServidorAutenticacion
	 */
	public String getUrlServidorAutenticacion() {
		return urlServidorAutenticacion;
	}

	/**
	 * @param urlServidorAutenticacion the urlServidorAutenticacion to set
	 */
	public void setUrlServidorAutenticacion(String urlServidorAutenticacion) {
		this.urlServidorAutenticacion = urlServidorAutenticacion;
	}

}
