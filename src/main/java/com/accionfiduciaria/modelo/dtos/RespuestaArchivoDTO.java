package com.accionfiduciaria.modelo.dtos;

import org.springframework.core.io.ByteArrayResource;

/**
 * @author Asesoftware
 *
 */
public class RespuestaArchivoDTO {
	
	private ByteArrayResource recurso;
	
	private String tipoArchivo;
	
	public RespuestaArchivoDTO(ByteArrayResource recurso, String tipoArchivo) {
		super();
		this.recurso = recurso;
		this.tipoArchivo = tipoArchivo;
	}
	
	public RespuestaArchivoDTO() {}

	public ByteArrayResource getRecurso() {
		return recurso;
	}

	public void setRecurso(ByteArrayResource recurso) {
		this.recurso = recurso;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	
	

}
