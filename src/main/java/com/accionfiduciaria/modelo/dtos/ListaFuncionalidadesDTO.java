package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * DTO usado para el envío del listado de permisos de un rol. 
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class ListaFuncionalidadesDTO {
	
	/**
	 * El listado de permisos.
	 */
	private List<PermisoDTO> funcionalidades;

	/**
	 * Constructor por defecto.
	 */
	public ListaFuncionalidadesDTO() {
	}

	/**
	 * Constructor que inicializa el campo funcionalidades.
	 * 
	 * @param funcionalidades {@link #funcionalidades}
	 */
	public ListaFuncionalidadesDTO(List<PermisoDTO> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

	/**
	 * @return the funcionalidades
	 */
	public List<PermisoDTO> getFuncionalidades() {
		return funcionalidades;
	}

	/**
	 * @param funcionalidades the funcionalidades to set
	 */
	public void setFuncionalidades(List<PermisoDTO> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}
}
