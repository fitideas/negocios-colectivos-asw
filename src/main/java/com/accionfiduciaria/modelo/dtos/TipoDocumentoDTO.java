/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author José Santiago Polo Acosta - 16/12/2019 - jpolo@asesoftware.com
 *
 */
public class TipoDocumentoDTO {
	
	String id;
	String descripcion;
	
	/**
	 * 
	 */
	public TipoDocumentoDTO() {
	}
	
	/**
	 * @param id
	 * @param descripcion
	 */
	public TipoDocumentoDTO(String id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
