package com.accionfiduciaria.modelo.dtos;

import java.util.ArrayList;
import java.util.List;

public class ResultadoSincronizacionTitularesYEncargosDTO {
	
	private boolean tieneEncargosNuevos;
	private List<CambioDatosTitularesNegocioDTO> cambioDatosTitularesNegocio;
	private List<DatosBasicosPersonaDTO> titularesNuevos;
	private boolean negocioActivo;

	/**
	 * 
	 */
	public ResultadoSincronizacionTitularesYEncargosDTO() {
		this.cambioDatosTitularesNegocio = new ArrayList<>();
		this.titularesNuevos = new ArrayList<>();
	}

	/**
	 * @param cambioDatosTitularesNegocio
	 * @param titularesNuevos
	 */
	public ResultadoSincronizacionTitularesYEncargosDTO(List<CambioDatosTitularesNegocioDTO> cambioDatosTitularesNegocio,
			List<DatosBasicosPersonaDTO> titularesNuevos) {
		this.cambioDatosTitularesNegocio = cambioDatosTitularesNegocio;
		this.titularesNuevos = titularesNuevos;
	}

	public boolean isTieneEncargosNuevos() {
		return tieneEncargosNuevos;
	}

	public void setTieneEncargosNuevos(boolean tieneEncargosNuevos) {
		this.tieneEncargosNuevos = tieneEncargosNuevos;
	}

	public List<CambioDatosTitularesNegocioDTO> getCambioDatosTitularesNegocio() {
		return cambioDatosTitularesNegocio;
	}

	public void setCambioDatosTitularesNegocio(List<CambioDatosTitularesNegocioDTO> cambioDatosTitularesNegocio) {
		this.cambioDatosTitularesNegocio = cambioDatosTitularesNegocio;
	}

	public List<DatosBasicosPersonaDTO> getTitularesNuevos() {
		return titularesNuevos;
	}

	public void setTitularesNuevos(List<DatosBasicosPersonaDTO> titularesNuevos) {
		this.titularesNuevos = titularesNuevos;
	}

	public boolean isNegocioActivo() {
		return negocioActivo;
	}

	public void setNegocioActivo(boolean negocioActivo) {
		this.negocioActivo = negocioActivo;
	}
}
