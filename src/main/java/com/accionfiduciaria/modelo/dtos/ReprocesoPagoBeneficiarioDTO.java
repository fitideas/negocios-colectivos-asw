/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author efarias
 *
 */
public class ReprocesoPagoBeneficiarioDTO {

	private String tipoDocumentoTitular;
	private String numeroDocumentoTitular;
	private String tipoDocumentoBeneficiario;
	private String numeroDocumentoBeneficiario;
	private datosFinancierosBeneficiarioDTO datoAnterior;
	private datosFinancierosBeneficiarioDTO datoNuevo;

	/**
	 * @return the tipoDocumentoTitular
	 */
	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	/**
	 * @param tipoDocumentoTitular the tipoDocumentoTitular to set
	 */
	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	/**
	 * @return the numeroDocumentoTitular
	 */
	public String getNumeroDocumentoTitular() {
		return numeroDocumentoTitular;
	}

	/**
	 * @param numeroDocumentoTitular the numeroDocumentoTitular to set
	 */
	public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
		this.numeroDocumentoTitular = numeroDocumentoTitular;
	}

	/**
	 * @return the tipoDocumentoBeneficiario
	 */
	public String getTipoDocumentoBeneficiario() {
		return tipoDocumentoBeneficiario;
	}

	/**
	 * @param tipoDocumentoBeneficiario the tipoDocumentoBeneficiario to set
	 */
	public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
		this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
	}

	/**
	 * @return the numeroDocumentoBeneficiario
	 */
	public String getNumeroDocumentoBeneficiario() {
		return numeroDocumentoBeneficiario;
	}

	/**
	 * @param numeroDocumentoBeneficiario the numeroDocumentoBeneficiario to set
	 */
	public void setNumeroDocumentoBeneficiario(String numeroDocumentoBeneficiario) {
		this.numeroDocumentoBeneficiario = numeroDocumentoBeneficiario;
	}

	/**
	 * @return the datoAnterior
	 */
	public datosFinancierosBeneficiarioDTO getDatoAnterior() {
		return datoAnterior;
	}

	/**
	 * @param datoAnterior the datoAnterior to set
	 */
	public void setDatoAnterior(datosFinancierosBeneficiarioDTO datoAnterior) {
		this.datoAnterior = datoAnterior;
	}

	/**
	 * @return the datoNuevo
	 */
	public datosFinancierosBeneficiarioDTO getDatoNuevo() {
		return datoNuevo;
	}

	/**
	 * @param datoNuevo the datoNuevo to set
	 */
	public void setDatoNuevo(datosFinancierosBeneficiarioDTO datoNuevo) {
		this.datoNuevo = datoNuevo;
	}

}
