package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * DTO usado para el envio de los datos d eun rol junto con las opciones del
 * menú a las que este tiene acceso.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class RolMenuDTO {

	/**
	 * El id del rol.
	 */
	private Integer idRol;
	/*
	 * El nombre del rol.
	 */
	private String rol;
	/**
	 * El listado de opciones de menu a las que tiene acceso el rol.
	 */
	private List<OpcionesMenuDTO> menu;

	/**
	 * Constructor por defecto.
	 */
	public RolMenuDTO() {
	}

	/**
	 * Constructor que inicializa los campos idRol y rol.
	 * 
	 * @param idRol {@link #idRol}
	 * @param rol   {@link #rol}
	 */
	public RolMenuDTO(Integer idRol, String rol) {
		this.idRol = idRol;
		this.rol = rol;
	}

	/**
	 * Constructor que inicializa los campos idRol, rol y menu.
	 * 
	 * @param idRol {@link #idRol}
	 * @param rol {@link #rol}
	 * @param menu {@link #menu}
	 */
	public RolMenuDTO(Integer idRol, String rol, List<OpcionesMenuDTO> menu) {
		this.idRol = idRol;
		this.rol = rol;
		this.menu = menu;
	}

	/**
	 * @return the idRol
	 */
	public Integer getIdRol() {
		return idRol;
	}

	/**
	 * @param idRol the idRol to set
	 */
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	/**
	 * @return the rol
	 */
	public String getRol() {
		return rol;
	}

	/**
	 * @param rol the rol to set
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}

	/**
	 * @return the menu
	 */
	public List<OpcionesMenuDTO> getMenu() {
		return menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(List<OpcionesMenuDTO> menu) {
		this.menu = menu;
	}

}
