package com.accionfiduciaria.modelo.dtos;

/**
 * Contiene los valores de los dominios simples.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class ValorDominioSimpleDTO extends ValorDominioDTO {
	
	/**
	 * La unidad en la que esta el valor del dominio.
	 */
	private String unidad;
	/**
	 * El valor del dominio
	 */
	private String valor;
	
	/**
	 * Constructor por defecto.
	 */
	public ValorDominioSimpleDTO() {
		super();
	}

	/**
	 * Constructor que inicializa el id.
	 * 
	 * @param id Id del valor de dominio
	 */
	public ValorDominioSimpleDTO(Integer id) {
		super(id);
	}

	/**
	 * Constructor que inicializa los cmapos id, descripcion, valor, unidad y estado.
	 * 
	 * @param id El id del valor de dominio.
	 * @param descripcion La descripción del valor de dominio.
	 * @param valor {@link #valor}
	 * @param unidad {@link #unidad}
	 * @param estado El estado del valor de dominio.
	 */
	public ValorDominioSimpleDTO(Integer id, String descripcion, String valor, String unidad, Boolean estado) {
		super(id,descripcion, estado);
		this.valor = valor;
		this.unidad = unidad;
	}

	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	
	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
}
