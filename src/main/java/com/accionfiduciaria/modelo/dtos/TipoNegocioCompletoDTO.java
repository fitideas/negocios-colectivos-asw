package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class TipoNegocioCompletoDTO extends TipoNegocioDTO {
	private List<GastoDTO> gastos;
	private List<TerceroDTO> terceros;

	/**
	 * @return the gastos
	 */
	public List<GastoDTO> getGastos() {
		return gastos;
	}

	/**
	 * @param gastos the gastos to set
	 */
	public void setGastos(List<GastoDTO> gastos) {
		this.gastos = gastos;
	}

	/**
	 * 
	 * @return
	 */
	public List<TerceroDTO> getTerceros() {
		return terceros;
	}

	/**
	 * 
	 * @param terceros
	 */
	public void setTerceros(List<TerceroDTO> terceros) {
		this.terceros = terceros;
	}
}
