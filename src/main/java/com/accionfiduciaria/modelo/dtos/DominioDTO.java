package com.accionfiduciaria.modelo.dtos;

import com.accionfiduciaria.modelo.entidad.Dominio;

/**
 * DTO base que contiene el codigo y el nombre de una entidad {@link Dominio}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DominioDTO {
	/**
	 * El codigo del dominio
	 */
	private String codigo;
	/**
	 * El nombre del dominio
	 */
	private String nombre;
	/**
	 * Indica si los valores del dominio se pueden eliminar o se deben reemplazar.
	 */
	private Boolean permiteEliminar;
	
	/**
	 * Constructor por defecto.
	 */
	public DominioDTO() {
	}
	
	/**
	 * Constructor que inicializa el codigo.
	 * 
	 * @param codigo {@link #codigo}
	 */
	public DominioDTO(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Constructor que inicializa el codigo y el nombre del dominio.
	 * 
	 * @param codigo {@link #codigo}
	 * @param nombre {@link #nombre}
	 */
	public DominioDTO(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the permiteEliminar
	 */
	public Boolean getPermiteEliminar() {
		return permiteEliminar;
	}

	/**
	 * @param permiteEliminar the permiteEliminar to set
	 */
	public void setPermiteEliminar(Boolean permiteEliminar) {
		this.permiteEliminar = permiteEliminar;
	}
}
