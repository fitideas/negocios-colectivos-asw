package com.accionfiduciaria.modelo.dtos;

public class TipoBeneficiarioDTO {

	private Integer idTipoBeneficiario;
	private String descripcion;
	private String generarCertificado;

	public Integer getIdTipoBeneficiario() {
		return idTipoBeneficiario;
	}

	public void setIdTipoBeneficiario(Integer idTipoBeneficiario) {
		this.idTipoBeneficiario = idTipoBeneficiario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getGenerarCertificado() {
		return generarCertificado;
	}

	public void setGenerarCertificado(String generarCertificado) {
		this.generarCertificado = generarCertificado;
	}
}
