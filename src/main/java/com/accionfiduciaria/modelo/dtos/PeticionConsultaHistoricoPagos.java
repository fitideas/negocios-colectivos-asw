package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class PeticionConsultaHistoricoPagos {

	private String numeroDocumento;
	private String tipoDocumento;
	private List<String> fecha;
	private List<String> tipoPagos;
	private List<String> negociosAsociados;
	private List<String> estadoPago;
	private Integer pagina;
	private Integer elementos;
	
	public PeticionConsultaHistoricoPagos() {}
	
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public List<String> getFecha() {
		return fecha;
	}
	public void setFecha(List<String> fecha) {
		this.fecha = fecha;
	}
	public List<String> getTipoPagos() {
		return tipoPagos;
	}
	public void setTipoPagos(List<String> tipoPagos) {
		this.tipoPagos = tipoPagos;
	}
	public List<String> getNegociosAsociados() {
		return negociosAsociados;
	}
	public void setNegociosAsociados(List<String> negociosAsociados) {
		this.negociosAsociados = negociosAsociados;
	}
	public List<String> getEstadoPago() {
		return estadoPago;
	}
	public void setEstadoPago(List<String> estadoPago) {
		this.estadoPago = estadoPago;
	}
	public Integer getPagina() {
		return pagina;
	}
	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}
	public Integer getElementos() {
		return elementos;
	}
	public void setElementos(Integer elementos) {
		this.elementos = elementos;
	}
	
	
	
}
