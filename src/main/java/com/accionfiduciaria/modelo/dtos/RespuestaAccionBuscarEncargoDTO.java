package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

public class RespuestaAccionBuscarEncargoDTO {
	
	private String numeroEncargo;
	private String nombreFidecomiso;
	private String estado;
	
	

	/**
	 * 
	 */
	public RespuestaAccionBuscarEncargoDTO() {
	}

	/**
	 * @param numeroEncargo
	 */
	public RespuestaAccionBuscarEncargoDTO(String numeroEncargo) {
		this.numeroEncargo = numeroEncargo;
	}

	/**
	 * @return the numeroEncargo
	 */
	public String getNumeroEncargo() {
		return numeroEncargo;
	}

	/**
	 * @param numeroEncargo the numeroEncargo to set
	 */
	public void setNumeroEncargo(String numeroEncargo) {
		this.numeroEncargo = numeroEncargo;
	}

	/**
	 * @return the nombreFidecomiso
	 */
	public String getNombreFidecomiso() {
		return nombreFidecomiso;
	}

	/**
	 * @param nombreFidecomiso the nombreFidecomiso to set
	 */
	public void setNombreFidecomiso(String nombreFidecomiso) {
		this.nombreFidecomiso = nombreFidecomiso;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		return Objects.hash(numeroEncargo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RespuestaAccionBuscarEncargoDTO other = (RespuestaAccionBuscarEncargoDTO) obj;
		return Objects.equals(numeroEncargo, other.numeroEncargo);
	}
}
