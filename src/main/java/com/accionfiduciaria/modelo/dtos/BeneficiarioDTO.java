package com.accionfiduciaria.modelo.dtos;

public class BeneficiarioDTO {

	private String beneficiario;
	private String tipoDocumentoBeneficiario;
	private String numeroDocumentoBeneficiario;
	private String tipoCuenta;
	private String numeroCuenta;
	private String fidecomiso;
	private String ficDestino;
	private String codigoMovimiento;
	
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getTipoDocumentoBeneficiario() {
		return tipoDocumentoBeneficiario;
	}
	public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
		this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
	}
	public String getNumeroDocumentoBeneficiario() {
		return numeroDocumentoBeneficiario;
	}
	public void setNumeroDocumentoBeneficiario(String numeroDocumentoBeneficiario) {
		this.numeroDocumentoBeneficiario = numeroDocumentoBeneficiario;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getFidecomiso() {
		return fidecomiso;
	}
	public void setFidecomiso(String fidecomiso) {
		this.fidecomiso = fidecomiso;
	}
	public String getFicDestino() {
		return ficDestino;
	}
	public void setFicDestino(String ficDestino) {
		this.ficDestino = ficDestino;
	}
	public String getCodigoMovimiento() {
		return codigoMovimiento;
	}
	public void setCodigoMovimiento(String codigoMovimiento) {
		this.codigoMovimiento = codigoMovimiento;
	}
	
	
}
