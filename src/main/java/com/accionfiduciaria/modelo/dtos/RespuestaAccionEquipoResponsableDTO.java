package com.accionfiduciaria.modelo.dtos;

public class RespuestaAccionEquipoResponsableDTO {

    private String codigoFideicomiso;
    private String tipoResponsable;
	private RespuestaBusquedadVinculadoDTO informacion;

	public String getCodigoFideicomiso() {
		return codigoFideicomiso;
	}

	public void setCodigoFideicomiso(String codigoFideicomiso) {
		this.codigoFideicomiso = codigoFideicomiso;
	}

	public String getTipoResponsable() {
		return tipoResponsable;
	}

	public void setTipoResponsable(String tipoResponsable) {
		this.tipoResponsable = tipoResponsable;
	}

	public RespuestaBusquedadVinculadoDTO getInformacion() {
		return informacion;
	}

	public void setInformacion(RespuestaBusquedadVinculadoDTO informacion) {
		this.informacion = informacion;
	}
}
