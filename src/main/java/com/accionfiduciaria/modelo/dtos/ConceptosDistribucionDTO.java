package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

//Clase que permite obtener los conceptos de la tabla neg distribución

public class ConceptosDistribucionDTO {
	
	private BigDecimal total;

	private BigDecimal totalObligaciones;

	private BigDecimal totalDistribucion;

	private BigDecimal totalRetenciones;

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalObligaciones() {
		return totalObligaciones;
	}

	public void setTotalObligaciones(BigDecimal totalObligaciones) {
		this.totalObligaciones = totalObligaciones;
	}

	public BigDecimal getTotalDistribucion() {
		return totalDistribucion;
	}

	public void setTotalDistribucion(BigDecimal totalDistribucion) {
		this.totalDistribucion = totalDistribucion;
	}

	public BigDecimal getTotalRetenciones() {
		return totalRetenciones;
	}

	public void setTotalRetenciones(BigDecimal totalRetenciones) {
		this.totalRetenciones = totalRetenciones;
	}
	
	
	
	
}
