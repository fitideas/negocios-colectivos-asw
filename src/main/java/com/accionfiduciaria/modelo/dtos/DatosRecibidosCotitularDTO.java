package com.accionfiduciaria.modelo.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DatosRecibidosCotitularDTO {

	@JsonProperty("documentotitular")
	private String documentoTitular;
	@JsonProperty("tipodocumentotitular")
	private String tipoDocumentoTitular;
	private String codigoNegocio;
	private List<CoTitularDatosBasicosDTO> cotitulares;
	/**
	 * @return the documentoTitular
	 */
	public String getDocumentoTitular() {
		return documentoTitular;
	}
	/**
	 * @param documentoTitular the documentoTitular to set
	 */
	public void setDocumentoTitular(String documentoTitular) {
		this.documentoTitular = documentoTitular;
	}
	/**
	 * @return the tipoDocumentoTitular
	 */
	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}
	/**
	 * @param tipoDocumentoTitular the tipoDocumentoTitular to set
	 */
	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}
	/**
	 * @return the codigoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}
	/**
	 * @param codigoNegocio the codigoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}
	/**
	 * @return the cotitulares
	 */
	public List<CoTitularDatosBasicosDTO> getCotitulares() {
		return cotitulares;
	}
	/**
	 * @param cotitulares the cotitulares to set
	 */
	public void setCotitulares(List<CoTitularDatosBasicosDTO> cotitulares) {
		this.cotitulares = cotitulares;
	}
}
