package com.accionfiduciaria.modelo.dtos;

public class PeticionBasicaPagoNegocio {

	private String periodo;
	private String codigoNegocio;
	private Integer codigoTipoNegocio;
	
	public PeticionBasicaPagoNegocio() {
	}
	
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getCodigoNegocio() {
		return codigoNegocio;
	}
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}
	public Integer getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}
	public void setCodigoTipoNegocio(Integer codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}
	
	
}
