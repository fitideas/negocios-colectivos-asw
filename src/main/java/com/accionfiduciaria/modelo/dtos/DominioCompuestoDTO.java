package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * Contiene los valores de los dominios compuestos, extiende a {@link DominioDTO}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DominioCompuestoDTO extends DominioDTO {
	
	/**
	 * La lista de los valores del dominio junto con sus relaciones.
	 */
	private List<ValorDominioCompuestoDTO> valores;

	/**
	 * Constructor por defecto.
	 */
	public DominioCompuestoDTO() {
		super();
	}
	
	/**
	 * Constructor que inicializa el codigo.
	 * 
	 * @param codigo El codigo del dominio.
	 */
	public DominioCompuestoDTO(String codigo) {
		super(codigo);
	}

	/**
	 * Constructor que inicializa el codigo, el nombre y los valores del dominio.
	 * 
	 * @param codigo El codigo del dominio.
	 * @param nombre El nombre del dominio.
	 * @param valores {@link #valores}.
	 */
	public DominioCompuestoDTO(String codigo, String nombre, List<ValorDominioCompuestoDTO> valores) {
		super(codigo, nombre);
		this.valores = valores;
	}

	/**
	 * @return the valores
	 */
	public List<ValorDominioCompuestoDTO> getValores() {
		return valores;
	}

	/**
	 * @param valores the valores to set
	 */
	public void setValores(List<ValorDominioCompuestoDTO> valores) {
		this.valores = valores;
	}
}
