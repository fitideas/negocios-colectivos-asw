package com.accionfiduciaria.modelo.dtos;

public class NegocioDTO {

	private String nombre;
	private String codigoSFC;
	private String codigoInterno;
	private boolean completo;
	private String estado;
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the codigoSFC
	 */
	public String getCodigoSFC() {
		return codigoSFC;
	}
	/**
	 * @param codigoSFC the codigoSFC to set
	 */
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}
	/**
	 * @return the codigoInterno
	 */
	public String getCodigoInterno() {
		return codigoInterno;
	}
	/**
	 * @param codigoInterno the codigoInterno to set
	 */
	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
	/**
	 * @return the completo
	 */
	public boolean getCompleto() {
		return completo;
	}
	/**
	 * @param completo the completo to set
	 */
	public void setCompleto(boolean completo) {
		this.completo = completo;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
