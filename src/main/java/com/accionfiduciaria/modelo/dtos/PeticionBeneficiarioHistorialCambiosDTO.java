package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class PeticionBeneficiarioHistorialCambiosDTO {
	
	private String numeroDocumento;
	private String tipoDocumento;
	private List<String> fecha;
	private List<DatosBasicosPersonaDTO> usuarios;
	private List<String> negocio;
	private String campo;
	
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public List<String> getFecha() {
		return fecha;
	}
	public void setFecha(List<String> fecha) {
		this.fecha = fecha;
	}
	
	public List<DatosBasicosPersonaDTO> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<DatosBasicosPersonaDTO> usuarios) {
		this.usuarios = usuarios;
	}
	
	public List<String> getNegocio() {
		return negocio;
	}
	public void setNegocio(List<String> negocio) {
		this.negocio = negocio;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
}
