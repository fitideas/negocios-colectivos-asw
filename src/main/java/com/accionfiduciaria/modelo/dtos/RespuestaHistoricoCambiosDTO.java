package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaHistoricoCambiosDTO {

	private Integer totalRegistro;
	private List<HistoricoCambioDTO> listaCambios;
	
	
	public Integer getTotalRegistro() {
		return totalRegistro;
	}
	public void setTotalRegistro(Integer totalRegistro) {
		this.totalRegistro = totalRegistro;
	}
	public List<HistoricoCambioDTO> getListaCambios() {
		return listaCambios;
	}
	public void setListaCambios(List<HistoricoCambioDTO> listaCambios) {
		this.listaCambios = listaCambios;
	}
	
	
}
