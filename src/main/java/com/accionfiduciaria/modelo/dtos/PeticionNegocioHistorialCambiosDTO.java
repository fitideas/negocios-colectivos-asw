package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class PeticionNegocioHistorialCambiosDTO {
	
	private List<String> fecha;
	private List<DatosBasicosPersonaDTO> usuarios;
	private String negocio;
	private String campo;
	
	
	
	public List<String> getFecha() {
		return fecha;
	}
	public void setFecha(List<String> fecha) {
		this.fecha = fecha;
	}
	
	public List<DatosBasicosPersonaDTO> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<DatosBasicosPersonaDTO> usuarios) {
		this.usuarios = usuarios;
	}
	public String getNegocio() {
		return negocio;
	}
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	
	

}
