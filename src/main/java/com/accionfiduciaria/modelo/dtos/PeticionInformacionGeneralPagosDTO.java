package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class PeticionInformacionGeneralPagosDTO {

	private List<String> rangoFecha;
	private List<String> codigoTipoNegocio;
	private String numeroEncargo;
	private String codigoNegocio;
	private int pagina;
	private int elementos;

	public PeticionInformacionGeneralPagosDTO() {
		super();
	}

	public List<String> getRangoFecha() {
		return rangoFecha;
	}

	public void setRangoFecha(List<String> rangoFecha) {
		this.rangoFecha = rangoFecha;
	}

	public List<String> getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}

	public void setCodigoTipoNegocio(List<String> codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}

	public String getNumeroEncargo() {
		return numeroEncargo;
	}

	public void setNumeroEncargo(String numeroEncargo) {
		this.numeroEncargo = numeroEncargo;
	}

	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	public int getPagina() {
		return pagina;
	}

	public void setPagina(int pagina) {
		this.pagina = pagina;
	}

	public int getElementos() {
		return elementos;
	}

	public void setElementos(int elementos) {
		this.elementos = elementos;
	}

}
