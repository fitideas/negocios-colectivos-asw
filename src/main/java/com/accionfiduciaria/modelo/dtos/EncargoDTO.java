package com.accionfiduciaria.modelo.dtos;

public class EncargoDTO {
	
	private Boolean activo;
	private String estado;
	private String fideicomiso;

	/**
	 * 
	 */
	public EncargoDTO() {
	}

	/**
	 * @param activo
	 * @param fideicomiso
	 */
	public EncargoDTO(Boolean activo, String fideicomiso) {
		this.activo = activo;
		this.fideicomiso = fideicomiso;
	}

	/**
	 * @param activo
	 * @param estado
	 * @param fideicomiso
	 */
	public EncargoDTO(Boolean activo, String estado, String fideicomiso) {
		this.activo = activo;
		this.estado = estado;
		this.fideicomiso = fideicomiso;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fideicomiso
	 */
	public String getFideicomiso() {
		return fideicomiso;
	}

	/**
	 * @param fideicomiso the fideicomiso to set
	 */
	public void setFideicomiso(String fideicomiso) {
		this.fideicomiso = fideicomiso;
	}
}
