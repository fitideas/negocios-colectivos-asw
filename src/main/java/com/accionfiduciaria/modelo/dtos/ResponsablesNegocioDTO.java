/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

/**
 * @author efarias
 *
 */
public class ResponsablesNegocioDTO {

	private String nombre;
	private String numeroDocumento;
	private String tipoDocumento;

	public ResponsablesNegocioDTO() {

	}

	/**
	 * @param nombre
	 * @param numeroDocumento
	 * @param tipoDocumento
	 */
	public ResponsablesNegocioDTO(String nombre, String numeroDocumento, String tipoDocumento) {
		this.nombre = nombre;
		this.numeroDocumento = numeroDocumento;
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public int hashCode() {
		return Objects.hash(numeroDocumento, tipoDocumento);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponsablesNegocioDTO other = (ResponsablesNegocioDTO) obj;
		return Objects.equals(numeroDocumento, other.numeroDocumento)
				&& Objects.equals(tipoDocumento, other.tipoDocumento);
	}

	@Override
	public String toString() {
		return "ResponsablesNegocioDTO [nombre=" + nombre + ", numeroDocumento=" + numeroDocumento + ", tipoDocumento="
				+ tipoDocumento + "]";
	}

}
