package com.accionfiduciaria.modelo.dtos;

public class DetalleDatosCambiadosDTO {

	private String dato;
	private String datoNuevo;
	private String datoAnterior;
	
	/**
	 * 
	 */
	public DetalleDatosCambiadosDTO() {
		super();
	}

	/**
	 * @param dato
	 * @param datoNuevo
	 * @param datoAnterior
	 */
	public DetalleDatosCambiadosDTO(String dato, String datoNuevo, String datoAnterior) {
		this.dato = dato;
		this.datoNuevo = datoNuevo;
		this.datoAnterior = datoAnterior;
	}

	public String getDato() {
		return dato;
	}

	public void setDato(String dato) {
		this.dato = dato;
	}

	public String getDatoNuevo() {
		return datoNuevo;
	}

	public void setDatoNuevo(String datoNuevo) {
		this.datoNuevo = datoNuevo;
	}

	public String getDatoAnterior() {
		return datoAnterior;
	}

	public void setDatoAnterior(String datoAnterior) {
		this.datoAnterior = datoAnterior;
	}
}
