package com.accionfiduciaria.modelo.dtos;

import java.util.ArrayList;
import java.util.List;

public class RespuestaSincronizacionDatosNegocioDTO {

	private boolean error;
	private String titulo;
	private String mensaje;
	private List<CambioDatosTitularesNegocioDTO> cambioDatosTitularesNegocio;
	private List<String> constantesIncompletas;
	private List<InformacionTitularesIncompletaDTO> informacionTitularesIncompleta;
	private List<DatosBasicosPersonaDTO> titularesNuevos;

	/*
	 * 
	 */
	public RespuestaSincronizacionDatosNegocioDTO() {
		this.cambioDatosTitularesNegocio = new ArrayList<>();
		this.constantesIncompletas = new ArrayList<>();
		this.informacionTitularesIncompleta = new ArrayList<>();
		this.titularesNuevos = new ArrayList<>();
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<CambioDatosTitularesNegocioDTO> getCambioDatosTitularesNegocio() {
		return cambioDatosTitularesNegocio;
	}

	public void setCambioDatosTitularesNegocio(List<CambioDatosTitularesNegocioDTO> cambioDatosTitularesNegocio) {
		this.cambioDatosTitularesNegocio = cambioDatosTitularesNegocio;
	}

	public List<String> getConstantesIncompletas() {
		return constantesIncompletas;
	}

	public void setConstantesIncompletas(List<String> constantesIncompletas) {
		this.constantesIncompletas = constantesIncompletas;
	}

	public List<InformacionTitularesIncompletaDTO> getInformacionTitularesIncompleta() {
		return informacionTitularesIncompleta;
	}

	public void setInformacionTitularesIncompleta(
			List<InformacionTitularesIncompletaDTO> informacionTitularesIncompleta) {
		this.informacionTitularesIncompleta = informacionTitularesIncompleta;
	}

	public List<DatosBasicosPersonaDTO> getTitularesNuevos() {
		return titularesNuevos;
	}

	public void setTitularesNuevos(List<DatosBasicosPersonaDTO> titularesNuevos) {
		this.titularesNuevos = titularesNuevos;
	}
}
