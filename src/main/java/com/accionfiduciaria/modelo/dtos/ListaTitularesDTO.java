package com.accionfiduciaria.modelo.dtos;

import java.util.Set;

public class ListaTitularesDTO {
	
	private int totalTitulares;
	
	private Set<PersonaPorDerechosDTO> titulares;

	public int getTotalTitulares() {
		return totalTitulares;
	}

	public void setTotalTitulares(int totalTitulares) {
		this.totalTitulares = totalTitulares;
	}

	public Set<PersonaPorDerechosDTO> getTitulares() {
		return titulares;
	}

	public void setTitulares(Set<PersonaPorDerechosDTO> titulares) {
		this.titulares = titulares;
	}

	public ListaTitularesDTO() {
		super();
	}

	public ListaTitularesDTO(int totalTitulares, Set<PersonaPorDerechosDTO> titulares) {
		super();
		this.totalTitulares = totalTitulares;
		this.titulares = titulares;
	}
	
	
	
}
