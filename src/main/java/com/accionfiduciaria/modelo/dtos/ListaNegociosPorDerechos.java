package com.accionfiduciaria.modelo.dtos;

import java.util.Set;

public class ListaNegociosPorDerechos {


	private int totalNegocios;
	private Set<NegociosPorDerechosDTO> negocios;
	
	
	public ListaNegociosPorDerechos() {
		super();
	}
	public ListaNegociosPorDerechos(int totalNegocios, Set<NegociosPorDerechosDTO> negocios) {
		super();
		this.totalNegocios = totalNegocios;
		this.negocios = negocios;
	}
	public int getTotalNegocios() {
		return totalNegocios;
	}
	public void setTotalNegocios(int totalNegocios) {
		this.totalNegocios = totalNegocios;
	}
	
	public Set<NegociosPorDerechosDTO> getNegocios() {
		return negocios;
	}
	public void setNegocios(Set<NegociosPorDerechosDTO> negocios) {
		this.negocios = negocios;
	}
	
	

	
	
}
