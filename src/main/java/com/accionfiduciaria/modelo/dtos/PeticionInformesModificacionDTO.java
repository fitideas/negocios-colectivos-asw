package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class PeticionInformesModificacionDTO {

	private List<String> rangoFecha;
	private List<String> codigoTipoNegocio;
	//private List<DatosBasicosPersonaDTO> usuarioCambio;
	private List<String> codigoNegocio;
	//private Integer pagina;
	//private Integer elementos;
	
	
	/*public List<DatosBasicosPersonaDTO> getUsuarioCambio() {
		return usuarioCambio;
	}
	public void setUsuarioCambio(List<DatosBasicosPersonaDTO> usuarioCambio) {
		this.usuarioCambio = usuarioCambio;
	}
	*/
	public List<String> getRangoFecha() {
		return rangoFecha;
	}
	public void setRangoFecha(List<String> fechaSolicitud) {
		this.rangoFecha = fechaSolicitud;
	}
	public List<String> getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}
	public void setCodigoTipoNegocio(List<String> codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}
	
	public List<String> getCodigoNegocio() {
		return codigoNegocio;
	}
	public void setCodigoNegocio(List<String> codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}
	@Override
	public String toString() {
		return "PeticionInformesModificacionDTO [fechaSolicitud=" + rangoFecha + ", codigoTipoNegocio="
				+ codigoTipoNegocio + ", codigoNegocio=" + codigoNegocio + "]";
	}

	
	
}
