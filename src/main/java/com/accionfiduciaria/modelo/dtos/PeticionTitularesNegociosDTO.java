package com.accionfiduciaria.modelo.dtos;

public class PeticionTitularesNegociosDTO {

	private String codigoNegocio;
	private String nombreNegocio;
	private String numeroDocumento;
	private String tipoDocumento;
	private String nombreTitular;
	private String anoGrabable;
	private int pagina;
	private int elementos;
	private String tipoCertificado;
	private Integer idRetencion;

	public PeticionTitularesNegociosDTO() {
		super();
	}

	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	public String getNombreNegocio() {
		return nombreNegocio;
	}

	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public int getPagina() {
		return pagina;
	}

	public void setPagina(int pagina) {
		this.pagina = pagina;
	}

	public int getElementos() {
		return elementos;
	}

	public void setElementos(int elementos) {
		this.elementos = elementos;
	}

	public String getAnoGrabable() {
		return anoGrabable;
	}

	public void setAnoGrabable(String anoGrabable) {
		this.anoGrabable = anoGrabable;
	}

	public String getTipoCertificado() {
		return tipoCertificado;
	}

	public void setTipoCertificado(String tipoCertificado) {
		this.tipoCertificado = tipoCertificado;
	}

	public Integer getIdRetencion() {
		return idRetencion;
	}

	public void setIdRetencion(Integer idRetencion) {
		this.idRetencion = idRetencion;
	}
}
