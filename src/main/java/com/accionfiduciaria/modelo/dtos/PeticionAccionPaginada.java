package com.accionfiduciaria.modelo.dtos;

/**
 * DTO utilizado para el envio de los parámetros usados en las consultas
 * paginadas a los servicios de Acción Fiduciaria.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class PeticionAccionPaginada {

	/**
	 * Limite de registros a mostrar.
	 */
	private Integer limit;
	/**
	 * Indica en que numero de objeto se iniciara la lista.
	 */
	private Integer start;
	/**
	 * El token de autenticación asignado al usuario.
	 */
	private String token;
	/**
	 * Indica si la consulta debe hacerse de forma exacta o parcial.
	 */
	private boolean exactMatch;
	/**
	 * Parámetro utilizado para las consultas por nombre.
	 */
	private String nombre;
	/**
	 * Parámetro utilizado para las consultas por identificación o código.
	 */
	private String identificacion;
	/**
	 * La url del servicio a utilizar.
	 */
	private String urlServicioAccion;
	/**
	 * Constructor por defecto.
	 */
	public PeticionAccionPaginada() {
	}

	/**
	 * Constructor que inicializa los campos limit, start, token, urlServicioAccion.
	 * 
	 * @param limit {@link #limit}
	 * @param start {@link #start}
	 * @param token {@link #token}
	 * @param urlServicioAccion {@link #urlServicioAccion}
	 */
	public PeticionAccionPaginada(Integer limit, Integer start, Boolean exactMatch, String token, String urlServicioAccion) {
		this.limit = limit;
		this.start = start;
		this.exactMatch = exactMatch;
		this.token = token;
		this.urlServicioAccion = urlServicioAccion;
	}
	
	/**
	 * 
	 * Constructor que inicializa todos los campos del objeto.
	 * 
	 * @param limit {@link #limit}
	 * @param start {@link #start}
	 * @param token {@link #token}
	 * @param exactMatch {@link #exactMatch}
	 * @param nombre {@link #nombre}
	 * @param identificacion {@link #identificacion}
	 * @param urlServicioAccion {@link #urlServicioAccion}
	 */
	public PeticionAccionPaginada(Integer limit, Integer start, String token, boolean exactMatch, String nombre,
			String identificacion, String urlServicioAccion) {
		this.limit = limit;
		this.start = start;
		this.token = token;
		this.exactMatch = exactMatch;
		this.nombre = nombre;
		this.identificacion = identificacion;
		this.urlServicioAccion = urlServicioAccion;
	}

	/**
	 * Constructor que inicializa los cmapos limit, start, token y urlServicioAccion.
	 * 
	 * @param limit {@link #limit}
	 * @param start {@link #start}
	 * @param token {@link #token}
	 * @param urlServicioAccion {@link #urlServicioAccion}
	 */
	public PeticionAccionPaginada(Integer limit, Integer start, String token,
			String urlServicioAccion) {
		this.limit = limit;
		this.start = start;
		this.token = token;
		this.urlServicioAccion = urlServicioAccion;
	}
	
	/**
	 * @param limit
	 * @param start
	 * @param token
	 * @param identificacion
	 * @param urlServicioAccion
	 */
	public PeticionAccionPaginada(Integer limit, Integer start, String token, String identificacion,
			String urlServicioAccion) {
		super();
		this.limit = limit;
		this.start = start;
		this.token = token;
		this.identificacion = identificacion;
		this.urlServicioAccion = urlServicioAccion;
	}

	/**
	 * @return the limit
	 */
	public Integer getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	/**
	 * @return the start
	 */
	public Integer getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(Integer start) {
		this.start = start;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the exactMatch
	 */
	public boolean isExactMatch() {
		return exactMatch;
	}

	/**
	 * @param exactMatch the exactMatch to set
	 */
	public void setExactMatch(boolean exactMatch) {
		this.exactMatch = exactMatch;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombreCliente) {
		this.nombre = nombreCliente;
	}

	/**
	 * @return the identificacion
	 */
	public String getIdentificacion() {
		return identificacion;
	}

	/**
	 * @param identificacion the identificacion to set
	 */
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	/**
	 * @return the urlServicioAccion
	 */
	public String getUrlServicioAccion() {
		return urlServicioAccion;
	}

	/**
	 * @param urlServicioAccion the urlServicioAccion to set
	 */
	public void setUrlServicioAccion(String urlServicioAccion) {
		this.urlServicioAccion = urlServicioAccion;
	}

}
