package com.accionfiduciaria.modelo.dtos;

public class NegocioFiltroDTO {
	
	// DTO que trae codigoSFC Y nombre de un negocio
	
	private String nombre;
	private String codigoSFC;
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigoSFC() {
		return codigoSFC;
	}
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}


}
