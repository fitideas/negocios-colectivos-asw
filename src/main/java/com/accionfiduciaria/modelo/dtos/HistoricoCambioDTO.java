package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

import com.accionfiduciaria.enumeradores.OperacionesAuditoria;

public class HistoricoCambioDTO {

	private String fechaCambio;
	private String registradoPor;
	private String direccionIp;
	private String operacion;
	private String codigoNegocio;
	private String nombreNegocio;
	private String campoCambio;
	private String datoNuevo;
	private String datoAnterior;

	public String getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(String registradoPor) {
		this.registradoPor = registradoPor;
	}

	public String getDireccionIp() {
		return direccionIp;
	}

	public void setDireccionIp(String direccionIp) {
		this.direccionIp = direccionIp;
	}

	public String getDatoNuevo() {
		return datoNuevo;
	}

	public void setDatoNuevo(String datoNuevo) {
		this.datoNuevo = datoNuevo;
	}

	public String getDatoAnterior() {
		return datoAnterior;
	}

	public void setDatoAnterior(String datoAnterior) {
		this.datoAnterior = datoAnterior;
	}

	public String getFechaCambio() {
		return fechaCambio;
	}

	public void setFechaCambio(String fechaCambio) {
		this.fechaCambio = fechaCambio;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = OperacionesAuditoria.buscarOperacion(operacion).getHomologacion();
	}

	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	public String getNombreNegocio() {
		return nombreNegocio;
	}

	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}

	public String getCampoCambio() {
		return campoCambio;
	}

	public void setCampoCambio(String campoCambio) {
		this.campoCambio = campoCambio;
	}

	@Override
	public int hashCode() {
		return Objects.hash(campoCambio, codigoNegocio, datoAnterior, datoNuevo, direccionIp, fechaCambio,
				nombreNegocio, operacion, registradoPor);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoCambioDTO other = (HistoricoCambioDTO) obj;
		return Objects.equals(campoCambio, other.campoCambio) && Objects.equals(codigoNegocio, other.codigoNegocio)
				&& Objects.equals(datoAnterior, other.datoAnterior) && Objects.equals(datoNuevo, other.datoNuevo)
				&& Objects.equals(direccionIp, other.direccionIp) && Objects.equals(fechaCambio, other.fechaCambio)
				&& Objects.equals(nombreNegocio, other.nombreNegocio) && Objects.equals(operacion, other.operacion)
				&& Objects.equals(registradoPor, other.registradoPor);
	}
}
