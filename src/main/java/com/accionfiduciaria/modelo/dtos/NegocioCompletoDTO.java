package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class NegocioCompletoDTO extends NegocioDTO {
	private String fechaApertura;
	private Integer numeroDerechosTotales;
	private String fechaLiquidacionNegocioPromotor;
	private String fechaConstitucionNegocio;
	private String estadoProyecto;
	private Float avaluoInmueble;
	private Float valorUltimoPredial;
	private Float valorUltimaValorizacion;
	private String fechaCobroPredial;
	private String fechaCobroValorizacion;
	private Boolean archivoCargado;
	private String seccionalAduanaDepartamento;
	private String seccionalAduanaCiudad;
	private String matriculaInmobiliaria;
	private String licenciaConstruccion;
	private String ciudadUbicacionPais;
	private String ciudadUbicacionDepartamento;
	private String ciudadUbicacion;
	private String fideicomitenteNombreCompleto;
	private String fideicomitenteTipoDocumento;
	private String fideicomitenteNumeroDocumento;
	private String calidadFidecomitente;
	private String calidadFiduciariaFideicomiso;
	private ConstantesPagoDTO constanteNegocio;
	private List<EncargoNegocioDTO> encargos;
	private List<TipoNegocioCompletoDTO> tiposNegocio;
	private List<FechaClaveDTO> fechasClaves;
	private List<DatosEquipoResponsableDTO> equipoResponsable;

	/**
	 * @return the fechaApertura
	 */
	public String getFechaApertura() {
		return fechaApertura;
	}

	/**
	 * @param fechaApertura the fechaApertura to set
	 */
	public void setFechaApertura(String fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	/**
	 * @return the numeroDerechosTotales
	 */
	public Integer getNumeroDerechosTotales() {
		return numeroDerechosTotales;
	}

	/**
	 * @param numeroDerechosTotales the numeroDerechosTotales to set
	 */
	public void setNumeroDerechosTotales(Integer numeroDerechosTotales) {
		this.numeroDerechosTotales = numeroDerechosTotales;
	}

	/**
	 * @return the fechaLiquidacionNegocioPromotor
	 */
	public String getFechaLiquidacionNegocioPromotor() {
		return fechaLiquidacionNegocioPromotor;
	}

	/**
	 * @param fechaLiquidacionNegocioPromotor the fechaLiquidacionNegocioPromotor to
	 *                                        set
	 */
	public void setFechaLiquidacionNegocioPromotor(String fechaLiquidacionNegocioPromotor) {
		this.fechaLiquidacionNegocioPromotor = fechaLiquidacionNegocioPromotor;
	}

	/**
	 * @return the fechaConstitucionNegocio
	 */
	public String getFechaConstitucionNegocio() {
		return fechaConstitucionNegocio;
	}

	/**
	 * @param fechaConstitucionNegocio the fechaConstitucionNegocio to set
	 */
	public void setFechaConstitucionNegocio(String fechaConstitucionNegocio) {
		this.fechaConstitucionNegocio = fechaConstitucionNegocio;
	}

	/**
	 * @return the estadoProyecto
	 */
	public String getEstadoProyecto() {
		return estadoProyecto;
	}

	/**
	 * @param estadoProyecto the estadoProyecto to set
	 */
	public void setEstadoProyecto(String estadoProyecto) {
		this.estadoProyecto = estadoProyecto;
	}

	/**
	 * @return the avaluoInmueble
	 */
	public Float getAvaluoInmueble() {
		return avaluoInmueble;
	}

	/**
	 * @param avaluoInmueble the avaluoInmueble to set
	 */
	public void setAvaluoInmueble(Float avaluoInmueble) {
		this.avaluoInmueble = avaluoInmueble;
	}

	/**
	 * @return the valorUltimoPredial
	 */
	public Float getValorUltimoPredial() {
		return valorUltimoPredial;
	}

	/**
	 * @param valorUltimoPredial the valorUltimoPredial to set
	 */
	public void setValorUltimoPredial(Float valorUltimoPredial) {
		this.valorUltimoPredial = valorUltimoPredial;
	}

	public Float getValorUltimaValorizacion() {
		return valorUltimaValorizacion;
	}

	public void setValorUltimaValorizacion(Float valorUltimaValorizacion) {
		this.valorUltimaValorizacion = valorUltimaValorizacion;
	}

	/**
	 * @return the fechaCobroPredial
	 */
	public String getFechaCobroPredial() {
		return fechaCobroPredial;
	}

	/**
	 * @param fechaCobroPredial the fechaCobroPredial to set
	 */
	public void setFechaCobroPredial(String fechaCobroPredial) {
		this.fechaCobroPredial = fechaCobroPredial;
	}

	/**
	 * @return the fechaCobroValorizacion
	 */
	public String getFechaCobroValorizacion() {
		return fechaCobroValorizacion;
	}

	/**
	 * @param fechaCobroValorizacion the fechaCobroValorizacion to set
	 */
	public void setFechaCobroValorizacion(String fechaCobroValorizacion) {
		this.fechaCobroValorizacion = fechaCobroValorizacion;
	}

	public Boolean getArchivoCargado() {
		return archivoCargado;
	}

	public void setArchivoCargado(Boolean archivoCargado) {
		this.archivoCargado = archivoCargado;
	}	
	
	public String getSeccionalAduanaDepartamento() {
		return seccionalAduanaDepartamento;
	}

	public void setSeccionalAduanaDepartamento(String seccionalAduanaDepartamento) {
		this.seccionalAduanaDepartamento = seccionalAduanaDepartamento;
	}

	public String getSeccionalAduanaCiudad() {
		return seccionalAduanaCiudad;
	}

	public void setSeccionalAduanaCiudad(String seccionalAduanaCiudad) {
		this.seccionalAduanaCiudad = seccionalAduanaCiudad;
	}

	public String getMatriculaInmobiliaria() {
		return matriculaInmobiliaria;
	}

	public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
		this.matriculaInmobiliaria = matriculaInmobiliaria;
	}

	public String getLicenciaConstruccion() {
		return licenciaConstruccion;
	}

	public void setLicenciaConstruccion(String licenciaConstruccion) {
		this.licenciaConstruccion = licenciaConstruccion;
	}

	public String getCiudadUbicacionPais() {
		return ciudadUbicacionPais;
	}

	public void setCiudadUbicacionPais(String ciudadUbicacionPais) {
		this.ciudadUbicacionPais = ciudadUbicacionPais;
	}

	public String getCiudadUbicacionDepartamento() {
		return ciudadUbicacionDepartamento;
	}

	public void setCiudadUbicacionDepartamento(String ciudadUbicacionDepartamento) {
		this.ciudadUbicacionDepartamento = ciudadUbicacionDepartamento;
	}

	public String getCiudadUbicacion() {
		return ciudadUbicacion;
	}

	public void setCiudadUbicacion(String ciudadUbicacion) {
		this.ciudadUbicacion = ciudadUbicacion;
	}

	public String getFideicomitenteNombreCompleto() {
		return fideicomitenteNombreCompleto;
	}

	public void setFideicomitenteNombreCompleto(String fideicomitenteNombreCompleto) {
		this.fideicomitenteNombreCompleto = fideicomitenteNombreCompleto;
	}

	public String getFideicomitenteTipoDocumento() {
		return fideicomitenteTipoDocumento;
	}

	public void setFideicomitenteTipoDocumento(String fideicomitenteTipoDocumento) {
		this.fideicomitenteTipoDocumento = fideicomitenteTipoDocumento;
	}

	public String getFideicomitenteNumeroDocumento() {
		return fideicomitenteNumeroDocumento;
	}

	public void setFideicomitenteNumeroDocumento(String fideicomitenteNumeroDocumento) {
		this.fideicomitenteNumeroDocumento = fideicomitenteNumeroDocumento;
	}

	public String getCalidadFidecomitente() {
		return calidadFidecomitente;
	}

	public void setCalidadFidecomitente(String calidadFidecomitente) {
		this.calidadFidecomitente = calidadFidecomitente;
	}

	public String getCalidadFiduciariaFideicomiso() {
		return calidadFiduciariaFideicomiso;
	}

	public void setCalidadFiduciariaFideicomiso(String calidadFiduciariaFideicomiso) {
		this.calidadFiduciariaFideicomiso = calidadFiduciariaFideicomiso;
	}

	/**
	 * @return the encargos
	 */
	public List<EncargoNegocioDTO> getEncargos() {
		return encargos;
	}

	/**
	 * @param encargos the encargos to set
	 */
	public void setEncargos(List<EncargoNegocioDTO> encargos) {
		this.encargos = encargos;
	}

	/**
	 * @return the tiposNegocio
	 */
	public List<TipoNegocioCompletoDTO> getTiposNegocio() {
		return tiposNegocio;
	}

	/**
	 * @param tiposNegocio the tiposNegocio to set
	 */
	public void setTiposNegocio(List<TipoNegocioCompletoDTO> tiposNegocio) {
		this.tiposNegocio = tiposNegocio;
	}

	/**
	 * @return the fechasClaves
	 */
	public List<FechaClaveDTO> getFechasClaves() {
		return fechasClaves;
	}

	/**
	 * @param fechasClaves the fechasClaves to set
	 */
	public void setFechasClaves(List<FechaClaveDTO> fechasClaves) {
		this.fechasClaves = fechasClaves;
	}

	public ConstantesPagoDTO getConstanteNegocio() {
		return constanteNegocio;
	}

	public void setConstanteNegocio(ConstantesPagoDTO constanteNegocio) {
		this.constanteNegocio = constanteNegocio;
	}

	public List<DatosEquipoResponsableDTO> getEquipoResponsable() {
		return equipoResponsable;
	}

	public void setEquipoResponsable(List<DatosEquipoResponsableDTO> equipoResponsable) {
		this.equipoResponsable = equipoResponsable;
	}
}
