package com.accionfiduciaria.modelo.dtos;

public class MiembroComiteDTO {
	
	private String codigoSFC;
	private String tipoDocumento;
	private String numDocumento;
	private String nombre;
	private String estado;
	private int rol;
	private String nombreRol;
	private String fecVigenciaRol;
	private String radicado;
	private boolean agregado;
	
	
	/**
	 * @return the codigoSFC
	 */
	public String getCodigoSFC() {
		return codigoSFC;
	}
	/**
	 * @param codigoSFC the codigoSFC to set
	 */
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	
	/**
	 * @return the numDocumento
	 */
	public String getNumDocumento() {
		return numDocumento;
	}
	/**
	 * @param numDocumento the numDocumento to set
	 */
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the rol
	 */
	public int getRol() {
		return rol;
	}
	/**
	 * @param rol the rol to set
	 */
	public void setRol(int rol) {
		this.rol = rol;
	}
	public String getNombreRol() {
		return nombreRol;
	}
	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}
	/**
	 * @return the fecVigenciaRol
	 */
	public String getFecVigenciaRol() {
		return fecVigenciaRol;
	}
	/**
	 * @param fecVigenciaRol the fecVigenciaRol to set
	 */
	public void setFecVigenciaRol(String fecVigenciaRol) {
		this.fecVigenciaRol = fecVigenciaRol;
	}
	/**
	 * @return the radicado
	 */
	public String getRadicado() {
		return radicado;
	}
	/**
	 * @param radicado the radicado to set
	 */
	public void setRadicado(String radicado) {
		this.radicado = radicado;
	}
	/**
	 * @return the agregado
	 */
	public boolean getAgregado() {
		return agregado;
	}
	/**
	 * @param agregado the agregado to set
	 */
	public void setAgregado(boolean agregado) {
		this.agregado = agregado;
	}
	@Override
	public String toString() {
		return "MiembroComiteDTO [codigoSFC=" + codigoSFC + ", tipoDocumento=" + tipoDocumento + ", numeroDocumento="
				+ numDocumento + ", nombre=" + nombre + ", estado=" + estado + ", rol=" + rol + ", fecVigenciaRol="
				+ fecVigenciaRol + ", radicado=" + radicado + ", agregado=" + agregado + "]";
	}
	
	

}
