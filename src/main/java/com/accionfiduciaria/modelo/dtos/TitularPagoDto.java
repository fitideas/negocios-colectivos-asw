package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class TitularPagoDto{

	

	private String nombreTitular;
	private String tipoDocumentoTitular;
	private String numeroDocumentoTitular;
	private Float porcentajeParticipacionTitular;
	private Integer numeroDerechoFiduciarios;
	private List<BeneficiarioPagosDTO> beneficiarios;

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	public String getNumeroDocumentoTitular() {
		return numeroDocumentoTitular;
	}

	public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
		this.numeroDocumentoTitular = numeroDocumentoTitular;
	}

	public Float getPorcentajeParticipacionTitular() {
		return porcentajeParticipacionTitular;
	}

	public void setPorcentajeParticipacionTitular(Float porcentajeParticipacionTitular) {
		this.porcentajeParticipacionTitular = porcentajeParticipacionTitular;
	}

	public Integer getNumeroDerechoFiduciarios() {
		return numeroDerechoFiduciarios;
	}

	public void setNumeroDerechoFiduciarios(Integer numeroDerechoFiduciarios) {
		this.numeroDerechoFiduciarios = numeroDerechoFiduciarios;
	}

	public List<BeneficiarioPagosDTO> getBeneficiarios() {
		return beneficiarios;
	}

	public void setBeneficiarios(List<BeneficiarioPagosDTO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}

}
