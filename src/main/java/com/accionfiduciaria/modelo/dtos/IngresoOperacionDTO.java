/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

/**
 * @author efarias
 *
 */
public class IngresoOperacionDTO {

	private String fechaSistema;
	private String fechaIngreso;
	private boolean esEditable;
	private String periodo;
	private BigDecimal valorIngreso;
	private String radicadoIngreso;

	/**
	 * @return the fechaSistema
	 */
	public String getFechaSistema() {
		return fechaSistema;
	}

	/**
	 * @param fechaSistema the fechaSistema to set
	 */
	public void setFechaSistema(String fechaSistema) {
		this.fechaSistema = fechaSistema;
	}

	/**
	 * @return the fechaIngreso
	 */
	public String getFechaIngreso() {
		return fechaIngreso;
	}

	/**
	 * @param fechaIngreso the fechaIngreso to set
	 */
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	/**
	 * @return the esEditable
	 */
	public boolean isEsEditable() {
		return esEditable;
	}

	/**
	 * @param esEditable the esEditable to set
	 */
	public void setEsEditable(boolean esEditable) {
		this.esEditable = esEditable;
	}

	/**
	 * @return the periodo
	 */
	public String getPeriodo() {
		return periodo;
	}

	/**
	 * @param periodo the periodo to set
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/**
	 * @return the valorIngreso
	 */
	public BigDecimal getValorIngreso() {
		return valorIngreso;
	}

	/**
	 * @param valorIngreso the valorIngreso to set
	 */
	public void setValorIngreso(BigDecimal valorIngreso) {
		this.valorIngreso = valorIngreso;
	}

	/**
	 * @return the radicadoIngreso
	 */
	public String getRadicadoIngreso() {
		return radicadoIngreso;
	}

	/**
	 * @param radicadoIngreso the radicadoIngreso to set
	 */
	public void setRadicadoIngreso(String radicadoIngreso) {
		this.radicadoIngreso = radicadoIngreso;
	}

}
