package com.accionfiduciaria.modelo.dtos;

public class DatosAsociacionBeneficiarios {

	private String beneficiario;
	private String documento;
	private String tipoDocumentoBeneficiario;
	private String porcentajeGiro;
	private String titularBeneficiarioGiro;
	private String documentoTitular;
	private String tipoDocumentoTitular;
	private String porcentajeParticipacion;
	private String numeroEncargoIndividual;
	private String tipoGiro;
	private String codigoBanco;
	private String tipoCuenta;
	private String numeroCuenta;

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getTipoDocumentoBeneficiario() {
		return tipoDocumentoBeneficiario;
	}

	public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
		this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
	}

	public String getPorcentajeGiro() {
		return porcentajeGiro;
	}

	public void setPorcentajeGiro(String porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	public String getTitularBeneficiarioGiro() {
		return titularBeneficiarioGiro;
	}

	public void setTitularBeneficiarioGiro(String titularBeneficiarioGiro) {
		this.titularBeneficiarioGiro = titularBeneficiarioGiro;
	}

	public String getDocumentoTitular() {
		return documentoTitular;
	}

	public void setDocumentoTitular(String documentoTitular) {
		this.documentoTitular = documentoTitular;
	}

	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public String getNumeroEncargoIndividual() {
		return numeroEncargoIndividual;
	}

	public void setNumeroEncargoIndividual(String numeroEncargoIndividual) {
		this.numeroEncargoIndividual = numeroEncargoIndividual;
	}

	public String getTipoGiro() {
		return tipoGiro;
	}

	public void setTipoGiro(String tipoGiro) {
		this.tipoGiro = tipoGiro;
	}

	public String getCodigoBanco() {
		return codigoBanco;
	}

	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

}
