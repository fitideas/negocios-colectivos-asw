/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author efarias
 *
 */
public class CalculoObligacionesTipoNegocioDTO {

	private String codigoNegocio;
	private boolean sePuedeDistribuir;
	private Integer codigoTipoNegocio;
	private String nombreTipoNegocio;
	private BigDecimal ingresosTotal;
	private BigDecimal obligacionesTotal;
	private BigDecimal totalDistribuir;
	private List<ConceptoDTO> listaIngresos;
	private List<ConceptoDTO> listaObligaciones;

	/**
	 * @return the codigoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	/**
	 * @param codigoNegocio the codigoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	/**
	 * @return the sePuedeDistribuir
	 */
	public boolean isSePuedeDistribuir() {
		return sePuedeDistribuir;
	}

	/**
	 * @param sePuedeDistribuir the sePuedeDistribuir to set
	 */
	public void setSePuedeDistribuir(boolean sePuedeDistribuir) {
		this.sePuedeDistribuir = sePuedeDistribuir;
	}

	/**
	 * @return the codigoTipoNegocio
	 */
	public Integer getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}

	/**
	 * @param codigoTipoNegocio the codigoTipoNegocio to set
	 */
	public void setCodigoTipoNegocio(Integer codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}

	/**
	 * @return the nombreTipoNegocio
	 */
	public String getNombreTipoNegocio() {
		return nombreTipoNegocio;
	}

	/**
	 * @param nombreTipoNegocio the nombreTipoNegocio to set
	 */
	public void setNombreTipoNegocio(String nombreTipoNegocio) {
		this.nombreTipoNegocio = nombreTipoNegocio;
	}

	/**
	 * @return the ingresosTotal
	 */
	public BigDecimal getIngresosTotal() {
		return ingresosTotal;
	}

	/**
	 * @param ingresosTotal the ingresosTotal to set
	 */
	public void setIngresosTotal(BigDecimal ingresosTotal) {
		this.ingresosTotal = ingresosTotal;
	}

	/**
	 * @return the obligacionesTotal
	 */
	public BigDecimal getObligacionesTotal() {
		return obligacionesTotal;
	}

	/**
	 * @param obligacionesTotal the obligacionesTotal to set
	 */
	public void setObligacionesTotal(BigDecimal obligacionesTotal) {
		this.obligacionesTotal = obligacionesTotal;
	}

	/**
	 * @return the totalDistribuir
	 */
	public BigDecimal getTotalDistribuir() {
		return totalDistribuir;
	}

	/**
	 * @param totalDistribuir the totalDistribuir to set
	 */
	public void setTotalDistribuir(BigDecimal totalDistribuir) {
		this.totalDistribuir = totalDistribuir;
	}

	/**
	 * @return the listaIngresos
	 */
	public List<ConceptoDTO> getListaIngresos() {
		return listaIngresos;
	}

	/**
	 * @param listaIngresos the listaIngresos to set
	 */
	public void setListaIngresos(List<ConceptoDTO> listaIngresos) {
		this.listaIngresos = listaIngresos;
	}

	/**
	 * @return the listaObligaciones
	 */
	public List<ConceptoDTO> getListaObligaciones() {
		return listaObligaciones;
	}

	/**
	 * @param listaObligaciones the listaObligaciones to set
	 */
	public void setListaObligaciones(List<ConceptoDTO> listaObligaciones) {
		this.listaObligaciones = listaObligaciones;
	}

}
