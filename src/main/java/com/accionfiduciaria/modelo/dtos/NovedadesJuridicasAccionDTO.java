package com.accionfiduciaria.modelo.dtos;

public class NovedadesJuridicasAccionDTO {

	private String claseEvento;
	private String codigoFideicomiso;
	private String conclusiones;
	private String estado;
	private String fechaEvento;
	private String nombreFuncionario;
	private String nroDocumento;
	private String secuencialEvento;
	private String tipoCalculoComision;
	private String tipoEvento;

	public String getClaseEvento() {
		return claseEvento;
	}

	public void setClaseEvento(String claseEvento) {
		this.claseEvento = claseEvento;
	}

	public String getCodigoFideicomiso() {
		return codigoFideicomiso;
	}

	public void setCodigoFideicomiso(String codigoFideicomiso) {
		this.codigoFideicomiso = codigoFideicomiso;
	}

	public String getConclusiones() {
		return conclusiones;
	}

	public void setConclusiones(String conclusiones) {
		this.conclusiones = conclusiones;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(String fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public String getNombreFuncionario() {
		return nombreFuncionario;
	}

	public void setNombreFuncionario(String nombreFuncionario) {
		this.nombreFuncionario = nombreFuncionario;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getSecuencialEvento() {
		return secuencialEvento;
	}

	public void setSecuencialEvento(String secuencialEvento) {
		this.secuencialEvento = secuencialEvento;
	}

	public String getTipoCalculoComision() {
		return tipoCalculoComision;
	}

	public void setTipoCalculoComision(String tipoCalculoComision) {
		this.tipoCalculoComision = tipoCalculoComision;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
}
