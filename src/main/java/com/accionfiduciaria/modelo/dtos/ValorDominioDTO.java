package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

/**
 * DTO base contiene el el id, la descripción y el estado de un valor de dominio. 
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class ValorDominioDTO {
	
	/**
	 * El id del valor de dominio. 
	 */
	private Integer id;
	/**
	 * La descripción del valor de dominio.
	 */
	private String descripcion;
	/**
	 * El estado del valor de dominio.
	 */
	private Boolean activo;
	
	/**
	 * Constructor por defecto.
	 */
	public ValorDominioDTO() {
	}

	/**
	 * Constructor que inicializa el id.
	 * 
	 * @param id {@link #id}
	 */
	public ValorDominioDTO(Integer id) {
		this.id = id;
	}

	/**
	 * Constructor que inicializa los campos id, descripcion y activo.
	 * 
	 * @param id {@link #id}
	 * @param descripcion {@link #descripcion}
	 * @param activo {@link #activo}
	 */
	public ValorDominioDTO(Integer id, String descripcion, Boolean activo) {
		this.id = id;
		this.descripcion = descripcion;
		this.activo = activo;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValorDominioDTO other = (ValorDominioDTO) obj;
		return Objects.equals(id, other.id);
	}
	
}
