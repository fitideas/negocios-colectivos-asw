/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

/**
 * @author efarias
 *
 */
public class ConceptoDTO {

	private String concepto;
	private String fecha;
	private BigDecimal valor;

	public ConceptoDTO() {
	}

	/**
	 * @param concepto
	 * @param fecha
	 * @param valor
	 */
	public ConceptoDTO(String concepto, String fecha, BigDecimal valor) {
		this.concepto = concepto;
		this.fecha = fecha;
		this.valor = valor;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

}
