package com.accionfiduciaria.modelo.dtos;

/**
 * DTO utilizado para guardar los datos de un negocio en las consultas a Accionback.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class RepuestaConsultaNegocioVinculadoDTO {
	
	/**
	 * El codigo interno del negocio.
	 */
	private String codigo;
	/**
	 * El nombre del negocio.
	 */
	private String nombre;
	/**
	 * El código sfc del negocio.
	 */
	private String sfc;
	/**
	 * El total de derechos del negocio.
	 */
	private String totalDerechos;
	/**
	 * El estado de negocio.
	 */
	private EstadoNegocioDTO estadoNegocio;
	
	/**
	 * Constructor por defecto.
	 */
	public RepuestaConsultaNegocioVinculadoDTO() {
	}

	/**
	 * Constructor que inicializa los campos, codigo, nombre, sfc, totalDerechos, estadoNegocio.
	 * 
	 * @param codigo {@link #codigo}
	 * @param nombre {@link #nombre}
	 * @param sfc {@link #sfc}
	 * @param totalDerechos {@link #totalDerechos}
	 * @param estadoNegocio {@link #estadoNegocio}
	 */
	public RepuestaConsultaNegocioVinculadoDTO(String codigo, String nombre, String sfc, String totalDerechos,
			EstadoNegocioDTO estadoNegocio) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.sfc = sfc;
		this.totalDerechos = totalDerechos;
		this.estadoNegocio = estadoNegocio;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the sfc
	 */
	public String getSfc() {
		return sfc;
	}

	/**
	 * @param sfc the sfc to set
	 */
	public void setSfc(String sfc) {
		this.sfc = sfc;
	}

	/**
	 * @return the totalDerechos
	 */
	public String getTotalDerechos() {
		return totalDerechos;
	}

	/**
	 * @param totalDerechos the totalDerechos to set
	 */
	public void setTotalDerechos(String totalDerechos) {
		this.totalDerechos = totalDerechos;
	}

	/**
	 * @return the estadoNegocio
	 */
	public EstadoNegocioDTO getEstadoNegocio() {
		return estadoNegocio;
	}

	/**
	 * @param estadoNegocio the estadoNegocio to set
	 */
	public void setEstadoNegocio(EstadoNegocioDTO estadoNegocio) {
		this.estadoNegocio = estadoNegocio;
	}
}
