package com.accionfiduciaria.modelo.dtos;

public class ConstantesPagoDTO {

	private String tipoFideicomiso;
	private String subtipoFideicomiso;
	private String movimientoPago;
	private String codigoPais;
	private String codigoDepartamento;
	private String codigoCiudad;
	private String tipoProceso;
	private String tipoMovimiento;

	public String getTipoFideicomiso() {
		return tipoFideicomiso;
	}

	public void setTipoFideicomiso(String tipoFideicomiso) {
		this.tipoFideicomiso = tipoFideicomiso;
	}

	public String getSubtipoFideicomiso() {
		return subtipoFideicomiso;
	}

	public void setSubtipoFideicomiso(String subtipoFideicomiso) {
		this.subtipoFideicomiso = subtipoFideicomiso;
	}

	public String getMovimientoPago() {
		return movimientoPago;
	}

	public void setMovimientoPago(String movimientoPago) {
		this.movimientoPago = movimientoPago;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	public String getCodigoDepartamento() {
		return codigoDepartamento;
	}

	public void setCodigoDepartamento(String codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}

	public String getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

}
