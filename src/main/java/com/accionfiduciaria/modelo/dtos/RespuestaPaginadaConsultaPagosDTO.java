package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaPaginadaConsultaPagosDTO {

	private int totalRegistros;
	private List<BeneficiarioPagosDTO> historialPagos;
	
	public RespuestaPaginadaConsultaPagosDTO() {}
	
	public int getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	public List<BeneficiarioPagosDTO> getHistorialPagos() {
		return historialPagos;
	}
	public void setHistorialPagos(List<BeneficiarioPagosDTO> historialPagos) {
		this.historialPagos = historialPagos;
	}
	
	
	
}
