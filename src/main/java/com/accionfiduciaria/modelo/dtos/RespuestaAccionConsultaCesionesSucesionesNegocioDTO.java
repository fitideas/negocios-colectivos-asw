package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

public class RespuestaAccionConsultaCesionesSucesionesNegocioDTO {

	private String idNegocio;
	private String idContrato;
	private String nroCesion;
	private BigDecimal porceActualCedente;
	private BigDecimal porceNuevoCedente;
	private BigDecimal diferenciaPorcentaje;
	private String fechaRegistro;
	private ClaseBeneficioDTO claseBeneficio;
	private TipoBeneficiarioDTO tipoBeneficiario;
	private RespuestaBusquedadVinculadoDTO propietarioNuevo;
	private RespuestaBusquedadVinculadoDTO propietarioPrevio;

	public String getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(String idNegocio) {
		this.idNegocio = idNegocio;
	}

	public String getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(String idContrato) {
		this.idContrato = idContrato;
	}

	public String getNroCesion() {
		return nroCesion;
	}

	public void setNroCesion(String nroCesion) {
		this.nroCesion = nroCesion;
	}

	public BigDecimal getPorceActualCedente() {
		return porceActualCedente;
	}

	public void setPorceActualCedente(BigDecimal porceActualCedente) {
		this.porceActualCedente = porceActualCedente;
	}

	public BigDecimal getPorceNuevoCedente() {
		return porceNuevoCedente;
	}

	public void setPorceNuevoCedente(BigDecimal porceNuevoCedente) {
		this.porceNuevoCedente = porceNuevoCedente;
	}

	public BigDecimal getDiferenciaPorcentaje() {
		return diferenciaPorcentaje;
	}

	public void setDiferenciaPorcentaje(BigDecimal diferenciaPorcentaje) {
		this.diferenciaPorcentaje = diferenciaPorcentaje;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public ClaseBeneficioDTO getClaseBeneficio() {
		return claseBeneficio;
	}

	public void setClaseBeneficio(ClaseBeneficioDTO claseBeneficio) {
		this.claseBeneficio = claseBeneficio;
	}

	public TipoBeneficiarioDTO getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	public void setTipoBeneficiario(TipoBeneficiarioDTO tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	public RespuestaBusquedadVinculadoDTO getPropietarioNuevo() {
		return propietarioNuevo;
	}

	public void setPropietarioNuevo(RespuestaBusquedadVinculadoDTO propietarioNuevo) {
		this.propietarioNuevo = propietarioNuevo;
	}

	public RespuestaBusquedadVinculadoDTO getPropietarioPrevio() {
		return propietarioPrevio;
	}

	public void setPropietarioPrevio(RespuestaBusquedadVinculadoDTO propietarioPrevio) {
		this.propietarioPrevio = propietarioPrevio;
	}

	@Override
	public String toString() {
		return "RespuestaAccionConsultaCesionesSucesionesNegocioDTO [idNegocio=" + idNegocio + ", idContrato="
				+ idContrato + ", nroCesion=" + nroCesion + ", porceActualCedente=" + porceActualCedente
				+ ", porceNuevoCedente=" + porceNuevoCedente + ", diferenciaPorcentaje=" + diferenciaPorcentaje
				+ ", fechaRegistro=" + fechaRegistro + ", claseBeneficio=" + claseBeneficio + ", tipoBeneficiario="
				+ tipoBeneficiario + ", propietarioNuevo=" + propietarioNuevo + ", propietarioPrevio="
				+ propietarioPrevio + "]";
	}
	
}
