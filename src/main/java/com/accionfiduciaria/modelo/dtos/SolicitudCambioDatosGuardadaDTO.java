package com.accionfiduciaria.modelo.dtos;

/**
 * DTO para el manejo de los datos de las solicitudes de cambios guardadas que 
 * se enviaran al front.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class SolicitudCambioDatosGuardadaDTO {
	
	/**
	 * El id de la solicitud.
	 */
	private Integer idSolicitud;
	/**
	 * la fecha en la que se realizo la solicitud.
	 */
	private String fechaSolicitud;
	/**
	 * El nombre del campo que se solicito modificar.
	 */
	private String campoAModificar;
	/**
	 * El valor que se solicito asignar.
	 */
	private String datoNuevo;
	/**
	 * El usuario que realizo la solicitud.
	 */
	private String profesional;
	/**
	 * El numero de radicado de la solicitud.
	 */
	private String numeroRadicado;
	
	/**
	 * Constructor por defecto. 
	 */
	public SolicitudCambioDatosGuardadaDTO() {
	}
	
	/**
	 * Cosntructor que inicializa el campo idSolicitud.
	 * 
	 * @param idSolicitud {@link #idSolicitud}
	 */
	public SolicitudCambioDatosGuardadaDTO(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	
	/**
	 * Cosntructor que inicializa los campos idSolicitud, fechaSolicitud, campoAModificar,
	 * datoNuevo, profesional, numeroRadicado
	 * 
	 * @param idSolicitud {@link #idSolicitud}
	 * @param fechaSolicitud {@link #fechaSolicitud}
	 * @param campoAModificar {@link #campoAModificar}
	 * @param datoNuevo {@link #datoNuevo}
	 * @param profesional {@link #profesional}
	 * @param numeroRadicado {@link #numeroRadicado}
	 */
	public SolicitudCambioDatosGuardadaDTO(Integer idSolicitud, String fechaSolicitud, String campoAModificar,
			String datoNuevo, String profesional, String numeroRadicado) {
		this.idSolicitud = idSolicitud;
		this.fechaSolicitud = fechaSolicitud;
		this.campoAModificar = campoAModificar;
		this.datoNuevo = datoNuevo;
		this.profesional = profesional;
		this.numeroRadicado = numeroRadicado;
	}
	/**
	 * @return the idSolicitud
	 */
	public Integer getIdSolicitud() {
		return idSolicitud;
	}
	/**
	 * @param idSolicitud the idSolicitud to set
	 */
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	/**
	 * @return the fechaSolicitud
	 */
	public String getFechaSolicitud() {
		return fechaSolicitud;
	}
	/**
	 * @param fechaSolicitud the fechaSolicitud to set
	 */
	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	/**
	 * @return the campoAModificar
	 */
	public String getCampoAModificar() {
		return campoAModificar;
	}
	/**
	 * @param campoAModificar the campoAModificar to set
	 */
	public void setCampoAModificar(String campoAModificar) {
		this.campoAModificar = campoAModificar;
	}
	/**
	 * @return the datoNuevo
	 */
	public String getDatoNuevo() {
		return datoNuevo;
	}
	/**
	 * @param datoNuevo the datoNuevo to set
	 */
	public void setDatoNuevo(String datoNuevo) {
		this.datoNuevo = datoNuevo;
	}
	/**
	 * @return the profesional
	 */
	public String getProfesional() {
		return profesional;
	}
	/**
	 * @param profesional the profesional to set
	 */
	public void setProfesional(String profesional) {
		this.profesional = profesional;
	}
	/**
	 * @return the numeroRadicado
	 */
	public String getNumeroRadicado() {
		return numeroRadicado;
	}
	/**
	 * @param numeroRadicado the numeroRadicado to set
	 */
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}
}
