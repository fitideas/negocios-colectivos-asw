/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

/**
 * @author efarias
 *
 */
public class DetalleDistribucionBeneficiarioDTO {

	private String nombreTitular;
	private String tipoDocumentoTitular;
	private String documentoTitular;
	private BigDecimal porcentajeParticipacion;
	private BigDecimal numeroDerechos;
	private String nombreBeneficiario;
	private String tipoDocumento;
	private String documento;
	private BigDecimal porcentajeGiro;
	private BigDecimal porcentajedistribuido;
	private BigDecimal valorGiro;
	private BigDecimal porcentajeParticipacionDistribuido;
	private BigDecimal subtotalPago;
	private BigDecimal gravamenMovimientoFinanciero;
	private String tipoPago;
	private BigDecimal valorTipoPago;
	private BigDecimal subtotalGirado;
	private String tipoCuenta;
	private String cuentaEncargo;
	private String entidadBancariaFideicomiso;
	private String codigoBanco;

	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}

	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	/**
	 * @return the tipoDocumentoTitular
	 */
	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	/**
	 * @param tipoDocumentoTitular the tipoDocumentoTitular to set
	 */
	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	/**
	 * @return the documentoTitular
	 */
	public String getDocumentoTitular() {
		return documentoTitular;
	}

	/**
	 * @param documentoTitular the documentoTitular to set
	 */
	public void setDocumentoTitular(String documentoTitular) {
		this.documentoTitular = documentoTitular;
	}

	/**
	 * @return the porcentajeParticipacion
	 */
	public BigDecimal getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	/**
	 * @param porcentajeParticipacion the porcentajeParticipacion to set
	 */
	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	/**
	 * @return the numeroDerechos
	 */
	public BigDecimal getNumeroDerechos() {
		return numeroDerechos;
	}

	/**
	 * @param numeroDerechos the numeroDerechos to set
	 */
	public void setNumeroDerechos(BigDecimal numeroDerechos) {
		this.numeroDerechos = numeroDerechos;
	}

	/**
	 * @return the nombreBeneficiario
	 */
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	/**
	 * @param nombreBeneficiario the nombreBeneficiario to set
	 */
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the documento
	 */
	public String getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	/**
	 * @return the porcentajeGiro
	 */
	public BigDecimal getPorcentajeGiro() {
		return porcentajeGiro;
	}

	/**
	 * @param porcentajeGiro the porcentajeGiro to set
	 */
	public void setPorcentajeGiro(BigDecimal porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	/**
	 * @return the porcentajedistribuido
	 */
	public BigDecimal getPorcentajedistribuido() {
		return porcentajedistribuido;
	}

	/**
	 * @param porcentajedistribuido the porcentajedistribuido to set
	 */
	public void setPorcentajedistribuido(BigDecimal porcentajedistribuido) {
		this.porcentajedistribuido = porcentajedistribuido;
	}

	/**
	 * @return the valorGiro
	 */
	public BigDecimal getValorGiro() {
		return valorGiro;
	}

	/**
	 * @param valorGiro the valorGiro to set
	 */
	public void setValorGiro(BigDecimal valorGiro) {
		this.valorGiro = valorGiro;
	}

	/**
	 * @return the porcentajeParticipacionDistribuido
	 */
	public BigDecimal getPorcentajeParticipacionDistribuido() {
		return porcentajeParticipacionDistribuido;
	}

	/**
	 * @param porcentajeParticipacionDistribuido the
	 *                                           porcentajeParticipacionDistribuido
	 *                                           to set
	 */
	public void setPorcentajeParticipacionDistribuido(BigDecimal porcentajeParticipacionDistribuido) {
		this.porcentajeParticipacionDistribuido = porcentajeParticipacionDistribuido;
	}

	/**
	 * @return the subtotalPago
	 */
	public BigDecimal getSubtotalPago() {
		return subtotalPago;
	}

	/**
	 * @param subtotalPago the subtotalPago to set
	 */
	public void setSubtotalPago(BigDecimal subtotalPago) {
		this.subtotalPago = subtotalPago;
	}

	/**
	 * @return the gravamenMovimientoFinanciero
	 */
	public BigDecimal getGravamenMovimientoFinanciero() {
		return gravamenMovimientoFinanciero;
	}

	/**
	 * @param gravamenMovimientoFinanciero the gravamenMovimientoFinanciero to set
	 */
	public void setGravamenMovimientoFinanciero(BigDecimal gravamenMovimientoFinanciero) {
		this.gravamenMovimientoFinanciero = gravamenMovimientoFinanciero;
	}

	/**
	 * @return the tipoPago
	 */
	public String getTipoPago() {
		return tipoPago;
	}

	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 * @return the valorTipoPago
	 */
	public BigDecimal getValorTipoPago() {
		return valorTipoPago;
	}

	/**
	 * @param valorTipoPago the valorTipoPago to set
	 */
	public void setValorTipoPago(BigDecimal valorTipoPago) {
		this.valorTipoPago = valorTipoPago;
	}

	/**
	 * @return the subtotalGirado
	 */
	public BigDecimal getSubtotalGirado() {
		return subtotalGirado;
	}

	/**
	 * @param subtotalGirado the subtotalGirado to set
	 */
	public void setSubtotalGirado(BigDecimal subtotalGirado) {
		this.subtotalGirado = subtotalGirado;
	}

	/**
	 * @return the tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * @param tipoCuenta the tipoCuenta to set
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * @return the cuentaEncargo
	 */
	public String getCuentaEncargo() {
		return cuentaEncargo;
	}

	/**
	 * @param cuentaEncargo the cuentaEncargo to set
	 */
	public void setCuentaEncargo(String cuentaEncargo) {
		this.cuentaEncargo = cuentaEncargo;
	}

	/**
	 * @return the entidadBancariaFideicomiso
	 */
	public String getEntidadBancariaFideicomiso() {
		return entidadBancariaFideicomiso;
	}

	/**
	 * @param entidadBancariaFideicomiso the entidadBancariaFideicomiso to set
	 */
	public void setEntidadBancariaFideicomiso(String entidadBancariaFideicomiso) {
		this.entidadBancariaFideicomiso = entidadBancariaFideicomiso;
	}

	/**
	 * @return the codigoBanco
	 */
	public String getCodigoBanco() {
		return codigoBanco;
	}

	/**
	 * @param codigoBanco the codigoBanco to set
	 */
	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}

}
