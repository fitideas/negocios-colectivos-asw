package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class TipoNegocioDTO {

	private Integer id;
	private String descripcion;
	private List<DatosBasicosValorDominioDTO> retenciones;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the retenciones
	 */
	public List<DatosBasicosValorDominioDTO> getRetenciones() {
		return retenciones;
	}

	/**
	 * @param retenciones the retenciones to set
	 */
	public void setRetenciones(List<DatosBasicosValorDominioDTO> retenciones) {
		this.retenciones = retenciones;
	}	
}
