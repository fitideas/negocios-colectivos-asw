/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author José Santiago Polo Acosta - 18/12/2019 - jpolo@asesoftware.com
 *
 */
public class PeticionAccionIntrospeccionToken {
	
	private String urlServicio;
	private String token;
	private String authorization;
	/**
	 * 
	 */
	public PeticionAccionIntrospeccionToken() {
	}
	/**
	 * @param urlServicioInvalidarToken
	 * @param tokenAutorizacion
	 * @param authorization
	 */
	public PeticionAccionIntrospeccionToken(String urlServicio, String token,
			String authorization) {
		this.urlServicio = urlServicio;
		this.token = token;
		this.authorization = authorization;
	}
	public String getUrlServicio() {
		return urlServicio;
	}
	public void setUrlServicio(String urlServicio) {
		this.urlServicio = urlServicio;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
}
