package com.accionfiduciaria.modelo.dtos;

import java.io.Serializable;
import java.util.List;

public class PeticionAsambleasDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String codigoSFC;
	private String fechaReunion;
	private String direccionReunion;
	private String ordenDia;
	private String nombreArchivo;
	private List<DecisionDTO> decisionTomada;
	private List<DecisionDTO> tareaPendiente;
	private List<AsistenteDTO> mesaDirectiva;
	private List<AsistenteDTO> expositor;
	private int numeroAsistentes;
	private String numeroRadicado;
	private String archivo;

	public PeticionAsambleasDTO() {
		super();
	}

	public String getCodigoSFC() {
		return codigoSFC;
	}

	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}

	public String getFechaReunion() {
		return fechaReunion;
	}

	public void setFechaReunion(String fechaReunion) {
		this.fechaReunion = fechaReunion;
	}

	public String getDireccionReunion() {
		return direccionReunion;
	}

	public void setDireccionReunion(String direccionReunion) {
		this.direccionReunion = direccionReunion;
	}

	public String getOrdenDia() {
		return ordenDia;
	}

	public void setOrdenDia(String ordenDia) {
		this.ordenDia = ordenDia;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public List<DecisionDTO> getDecisionTomada() {
		return decisionTomada;
	}

	public void setDecisionTomada(List<DecisionDTO> decisionTomada) {
		this.decisionTomada = decisionTomada;
	}

	public List<DecisionDTO> getTareaPendiente() {
		return tareaPendiente;
	}

	public void setTareasPendientes(List<DecisionDTO> tareaPendiente) {
		this.tareaPendiente = tareaPendiente;
	}

	public List<AsistenteDTO> getMesaDirectiva() {
		return mesaDirectiva;
	}

	public void setMesaDirectiva(List<AsistenteDTO> mesaDirectiva) {
		this.mesaDirectiva = mesaDirectiva;
	}

	public List<AsistenteDTO> getExpositor() {
		return expositor;
	}

	public void setExpositor(List<AsistenteDTO> expositor) {
		this.expositor = expositor;
	}

	public int getNumeroAsistentes() {
		return numeroAsistentes;
	}

	public void setNumeroAsistentes(int numeroAsistentes) {
		this.numeroAsistentes = numeroAsistentes;
	}

	public String getNumeroRadicado() {
		return numeroRadicado;
	}

	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	
	

}

