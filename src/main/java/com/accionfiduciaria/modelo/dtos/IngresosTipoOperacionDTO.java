/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * @author efarias
 *
 */
public class IngresosTipoOperacionDTO {

	private Integer codigoTipoNegocio;
	private String nombreTipoNegocio;
	private String cuenta;
	private List<IngresoOperacionDTO> listaIngresosOperacion;

	/**
	 * @return the codigoTipoNegocio
	 */
	public Integer getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}

	/**
	 * @param codigoTipoNegocio the codigoTipoNegocio to set
	 */
	public void setCodigoTipoNegocio(Integer codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}

	/**
	 * @return the nombreTipoNegocio
	 */
	public String getNombreTipoNegocio() {
		return nombreTipoNegocio;
	}

	/**
	 * @param nombreTipoNegocio the nombreTipoNegocio to set
	 */
	public void setNombreTipoNegocio(String nombreTipoNegocio) {
		this.nombreTipoNegocio = nombreTipoNegocio;
	}

	/**
	 * @return the nombreEncargo
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param nombreEncargo the codigoEncargo to set
	 */
	public void setCuenta(String nombreEncargo) {
		this.cuenta = nombreEncargo;
	}

	/**
	 * @return the listaIngresosOperacion
	 */
	public List<IngresoOperacionDTO> getListaIngresosOperacion() {
		return listaIngresosOperacion;
	}

	/**
	 * @param listaIngresosOperacion the listaIngresosOperacion to set
	 */
	public void setListaIngresosOperacion(List<IngresoOperacionDTO> listaIngresosOperacion) {
		this.listaIngresosOperacion = listaIngresosOperacion;
	}

}
