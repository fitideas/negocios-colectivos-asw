package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class DatosAGuardarCotitularDTO {
	
	private String documentoTitular;
	private String tipoDocumentoTitular;
	private String  codigoNegocio;
	private List<DatosBasicosCotitularDTO> cotitulares;
	/**
	 * @return the documentoTitular
	 */
	public String getDocumentoTitular() {
		return documentoTitular;
	}
	/**
	 * @param documentoTitular the documentoTitular to set
	 */
	public void setDocumentoTitular(String documentoTitular) {
		this.documentoTitular = documentoTitular;
	}
	/**
	 * @return the tipoDocumentoTitular
	 */
	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}
	/**
	 * @param tipoDocumentoTitular the tipoDocumentoTitular to set
	 */
	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}
	/**
	 * @return the codigoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}
	/**
	 * @param codigoNegocio the codigoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}
	/**
	 * @return the cotitulares
	 */
	public List<DatosBasicosCotitularDTO> getCotitulares() {
		return cotitulares;
	}
	/**
	 * @param cotitulares the cotitulares to set
	 */
	public void setCotitulares(List<DatosBasicosCotitularDTO> cotitulares) {
		this.cotitulares = cotitulares;
	}
}
