package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * DTO utilizado para guardar los datos obtenidos en la consulta de negocios de un vinculado.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class RespuestaPaginadaConsultaNegociosVinculadoDTO extends RespuestaPaginadaAccionDTO {

	/**
	 * Contiene los datos de los negocios de vinculado.
	 */
	private List<RepuestaConsultaNegocioVinculadoDTO> results;

	/**
	 * Constructor por defecto.
	 */
	public RespuestaPaginadaConsultaNegociosVinculadoDTO() {
		super();
	}

	/**
	 * Constructor que inicializa los campos: limit, size, start, links, results.
	 * 
	 * @param limit   Inidica el máximo número de resultados
	 * @param size    La cantidad de objetos solicitados por página.
	 * @param start   Define cuál objeto es el primero en mostrarse de la lista, si
	 *                la paginación es de 5 objetos por página y se establece en 0
	 *                se dice que se está en la primera página de 5 objetos. Por
	 *                tanto para llamar la siguiente página este parámetro deberá
	 *                definirse en 5 ya que no es inclusivo el registro.
	 * @param links   Es el LinkModel objeto que define los links para determinar el
	 *                path actual, el recurso solicitado y el listado siguiente de
	 *                objetos a retornar o el anterior
	 * @param results {@link #results}
	 */
	public RespuestaPaginadaConsultaNegociosVinculadoDTO(int limit, int size, int start, LinksDTO links,
			List<RepuestaConsultaNegocioVinculadoDTO> results) {
		super(limit, size, start, links);
		this.results = results;
	}

	/**
	 * @return the results
	 */
	public List<RepuestaConsultaNegocioVinculadoDTO> getResults() {
		return results;
	}

	/**
	 * @param results the results to set
	 */
	public void setResults(List<RepuestaConsultaNegocioVinculadoDTO> results) {
		this.results = results;
	}
}
