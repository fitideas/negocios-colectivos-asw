package com.accionfiduciaria.modelo.dtos;

/**
 * DTO utilizado para recibir las peticiones de envío de solicitud de cambio de
 * dato.
 * 
 * @author José Polo - jpolo@asesoftware.com
 */
public class SolicitudCambioDatosDTO {
	
	/**
	 * La fecha en la que se realiza la solicitud.
	 * En un principio llegará vacia.
	 */
	private String fechaSolicitud;
	/**
	 * El nombre del campo que se solicita modificar.
	 */
	private String campoAModificar;
	/**
	 * El nuevo valor del campo.
	 */
	private String datoNuevo;
	/**
	 * El valor anterior del campo.
	 */
	private String datoAnterior;
	/**
	 * El login del usuario que hace la solicitud.
	 */
	private String profesional;
	/**
	 * El número de radicado de la solicitud.
	 */
	private String numeroRadicado;
	/**
	 * El tipo de documento del vinculado-
	 */
	private String tipoDocumentoVinculado;
	/**
	 * El número de documento del vinculado.
	 */
	private String numeroDocumentoVinculado;
	/**
	 * El nombre completo del vinculado.
	 */
	private String nombreCompletoVinculado;
	/**
	 * @return the fechaSolicitud
	 */
	public String getFechaSolicitud() {
		return fechaSolicitud;
	}
	/**
	 * @param fechaSolicitud the fechaSolicitud to set
	 */
	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	/**
	 * @return the campoAModificar
	 */
	public String getCampoAModificar() {
		return campoAModificar;
	}
	/**
	 * @param campoAModificar the campoAModificar to set
	 */
	public void setCampoAModificar(String campoAModificar) {
		this.campoAModificar = campoAModificar;
	}
	/**
	 * @return the datoNuevo
	 */
	public String getDatoNuevo() {
		return datoNuevo;
	}
	/**
	 * @param datoNuevo the datoNuevo to set
	 */
	public void setDatoNuevo(String datoNuevo) {
		this.datoNuevo = datoNuevo;
	}
	/**
	 * @return the datoAnterior
	 */
	public String getDatoAnterior() {
		return datoAnterior;
	}
	/**
	 * @param datoAnterior the datoAnterior to set
	 */
	public void setDatoAnterior(String datoAnterior) {
		this.datoAnterior = datoAnterior;
	}
	/**
	 * @return the profesional
	 */
	public String getProfesional() {
		return profesional;
	}
	/**
	 * @param profesional the profesional to set
	 */
	public void setProfesional(String profesional) {
		this.profesional = profesional;
	}
	/**
	 * @return the numeroRadicado
	 */
	public String getNumeroRadicado() {
		return numeroRadicado;
	}
	/**
	 * @param numeroRadicado the numeroRadicado to set
	 */
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}
	/**
	 * @return the tipoDocumentoVinculado
	 */
	public String getTipoDocumentoVinculado() {
		return tipoDocumentoVinculado;
	}
	/**
	 * @param tipoDocumentoVinculado the tipoDocumentoVinculado to set
	 */
	public void setTipoDocumentoVinculado(String tipoDocumentoVinculado) {
		this.tipoDocumentoVinculado = tipoDocumentoVinculado;
	}
	/**
	 * @return the numeroDocumentoVinculado
	 */
	public String getNumeroDocumentoVinculado() {
		return numeroDocumentoVinculado;
	}
	/**
	 * @param numeroDocumentoVinculado the numeroDocumentoVinculado to set
	 */
	public void setNumeroDocumentoVinculado(String numeroDocumentoVinculado) {
		this.numeroDocumentoVinculado = numeroDocumentoVinculado;
	}
	/**
	 * @return the nombreCompletoVinculado
	 */
	public String getNombreCompletoVinculado() {
		return nombreCompletoVinculado;
	}
	/**
	 * @param nombreCompletoVinculado the nombreCompletoVinculado to set
	 */
	public void setNombreCompletoVinculado(String nombreCompletoVinculado) {
		this.nombreCompletoVinculado = nombreCompletoVinculado;
	}
}
