package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * Contiene los valores de un dominio compuesto junto con sus relaciones.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class ValorDominioCompuestoDTO extends ValorDominioDTO {
	
	/**
	 * Las relaciones del valor de dominio.
	 */
	private List<RelacionValorDominioDTO> valores;
	
	/**
	 * Constructor por defecto.
	 */
	public ValorDominioCompuestoDTO() {
	}
	
	/**
	 * Constructor que inicializa el id.
	 * 
	 * @param id El id del valor de dominio
	 */
	public ValorDominioCompuestoDTO(Integer id) {
		super(id);
	}
	
	/**
	 * Constructor que inicializa los campos id, descripcion, estado y valores.
	 * 
	 * @param id El id del valor de dominio
	 * @param descripcion La descripcion del valor de dominio.
	 * @param estado El estado del valor de dominio.
	 * @param valores {@link #valores}
	 */
	public ValorDominioCompuestoDTO(Integer id, String descripcion, Boolean estado, List<RelacionValorDominioDTO> valores) {
		super(id, descripcion, estado);
		this.valores = valores;
	}

	/**
	 * @return the valores
	 */
	public List<RelacionValorDominioDTO> getValores() {
		return valores;
	}

	/**
	 * @param valores the valores to set
	 */
	public void setValores(List<RelacionValorDominioDTO> valores) {
		this.valores = valores;
	}

}
