package com.accionfiduciaria.modelo.dtos;

public class InformacionFinancieraDTO {

	private String codigoNegocio;
	private String tipoDocumentoTitular;
	private String documento;
	private Integer totalDerechosNegocio;
	private Integer numeroDerechosTitular;

	/**
	 * @return the codigoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	/**
	 * @param codigoNegocio the codigoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	/**
	 * @return the tipoDocumentoTitular
	 */
	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	/**
	 * @param tipoDocumentoTitular the tipoDocumentoTitular to set
	 */
	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	/**
	 * @return the documento
	 */
	public String getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	/**
	 * @return the totalDerechosNegocio
	 */
	public Integer getTotalDerechosNegocio() {
		return totalDerechosNegocio;
	}

	/**
	 * @param totalDerechosNegocio the totalDerechosNegocio to set
	 */
	public void setTotalDerechosNegocio(Integer totalDerechosNegocio) {
		this.totalDerechosNegocio = totalDerechosNegocio;
	}

	/**
	 * @return the numeroDerechosTitular
	 */
	public Integer getNumeroDerechosTitular() {
		return numeroDerechosTitular;
	}

	/**
	 * @param numeroDerechosTitular the numeroDerechosTitular to set
	 */
	public void setNumeroDerechosTitular(Integer numeroDerechosTitular) {
		this.numeroDerechosTitular = numeroDerechosTitular;
	}
}
