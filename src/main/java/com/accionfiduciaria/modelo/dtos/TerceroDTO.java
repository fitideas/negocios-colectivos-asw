package com.accionfiduciaria.modelo.dtos;

public class TerceroDTO extends GastoDTO {

	private Integer tipoDocumento;
	private String numeroDocumento;

	/**
	 * @return the tipoDocumento
	 */
	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(Integer tipoDocumentoTercero) {
		this.tipoDocumento = tipoDocumentoTercero;
	}

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumentoTercero) {
		this.numeroDocumento = numeroDocumentoTercero;
	}
}
