/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * DTO usado para almacenar los datos basicos de un vinculado.
 * 
 * @author José Santiago Polo Acosta - 13/12/2019 - jpolo@asesoftware.com
 *
 */
public class DatosBasicosVinculadoDTO {
	
	/**
	 * El nombre completo del vinculado. 
	 */
	private String nombreCompleto;
	/**
	 * El tipo de documento del vinculado.
	 */
	private String tipoDocumento;
	/**
	 * Numero de documento del vinculado.
	 */
	private String numeroDocumento;
	
	/**
	 * Constructor por defecto.
	 */
	public DatosBasicosVinculadoDTO() {
	}
	
	/**
	 * Constructor que inicializa los campos: nombreCompleto, tipoDocumento, numeroDocumento.
	 *
	 * @param nombreCompleto {@link #nombreCompleto}
	 * @param tipoDocumento {@link #tipoDocumento}
	 * @param numeroDocumento {@link #numeroDocumento}
	 */
	public DatosBasicosVinculadoDTO(String nombreCompleto, String tipoDocumento, String numeroDocumento) {
		this.nombreCompleto = nombreCompleto;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the nombreCompleto
	 */
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	/**
	 * @param nombreCompleto the nombreCompleto to set
	 */
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
}
