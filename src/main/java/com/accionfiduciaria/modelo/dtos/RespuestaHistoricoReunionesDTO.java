package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaHistoricoReunionesDTO {

	private int totalRegistros;
	private List<HistoricoReunionDTO> historicoReuniones;
	
	
	public int getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	public List<HistoricoReunionDTO> getHistoricoReuniones() {
		return historicoReuniones;
	}
	public void setHistoricoReuniones(List<HistoricoReunionDTO> historicoReuniones) {
		this.historicoReuniones = historicoReuniones;
	}
	
	
}
