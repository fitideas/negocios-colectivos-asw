package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class InformacionFinancieraRecepcionDTO extends InformacionFinancieraDTO {
	
	private List<BeneficiarioTipoPagoDTO> beneficiarios;

	/**
	 * @return the beneficiarios
	 */
	public List<BeneficiarioTipoPagoDTO> getBeneficiarios() {
		return beneficiarios;
	}

	/**
	 * @param beneficiarios the beneficiarios to set
	 */
	public void setBeneficiarios(List<BeneficiarioTipoPagoDTO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
}
