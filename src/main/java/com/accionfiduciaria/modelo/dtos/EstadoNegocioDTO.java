package com.accionfiduciaria.modelo.dtos;

/**
 * DTO utilizado para almacenar los datos del estado de un negocio enviado por
 * Acción Fiduciaria.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class EstadoNegocioDTO {

	/**
	 * El id del estado.
	 */
	private String id;
	
	/**
	 * La descripción del estado.
	 */
	private String descripcion;

	/**
	 * Constructor por defecto.
	 */
	public EstadoNegocioDTO() {
	}

	/**
	 * Constructor que inicializa los campos id y descripcion.
	 * 
	 * @param id {@link #id}
	 * @param descripcion {@link #descripcion}
	 */
	public EstadoNegocioDTO(String id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
