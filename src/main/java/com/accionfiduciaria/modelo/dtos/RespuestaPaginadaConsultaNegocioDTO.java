package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaPaginadaConsultaNegocioDTO extends RespuestaPaginadaAccionDTO{

	private List<RepuestaConsultaNegocioDTO> results;

	/**
	 * 
	 */
	public RespuestaPaginadaConsultaNegocioDTO() {
		super();
	}

	/**
	 * @param results
	 */
	public RespuestaPaginadaConsultaNegocioDTO(int limit, int size, int start, LinksDTO links, List<RepuestaConsultaNegocioDTO> results) {
		super(limit, size, start, links);
		this.results = results;
	}

	/**
	 * @return the results
	 */
	public List<RepuestaConsultaNegocioDTO> getResults() {
		return results;
	}

	/**
	 * @param results the results to set
	 */
	public void setResults(List<RepuestaConsultaNegocioDTO> results) {
		this.results = results;
	}
}
