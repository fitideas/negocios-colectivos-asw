package com.accionfiduciaria.modelo.dtos;

public class PeticionAccionConsultaExactaDTO {
	
	private String documento;
	private String url;
	private String token;

	/**
	 * @param documento
	 * @param url
	 * @param token
	 */
	public PeticionAccionConsultaExactaDTO(String documento, String url, String token) {
		super();
		this.documento = documento;
		this.url = url;
		this.token = token;
	}

	/**
	 * @return the documento
	 */
	public String getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
}
