package com.accionfiduciaria.modelo.dtos;

public class HistoricoCesionesCambioDTO {

	private String codigoNegocio;
	private String nombreNegocio;
	private String fechaCambio;
	private String porcentajeActualCedente;
	private String porcentajeNuevoCedente;
	private String diferenciaPorcentaje;
	private String nombrePropietarioNuevo;
	private String nombrePropietarioAnterior;
	
	public String getCodigoNegocio() {
		return codigoNegocio;
	}
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}
	public String getNombreNegocio() {
		return nombreNegocio;
	}
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	public String getFechaCambio() {
		return fechaCambio;
	}
	public void setFechaCambio(String fechaCambio) {
		this.fechaCambio = fechaCambio;
	}
	public String getPorcentajeActualCedente() {
		return porcentajeActualCedente;
	}
	public void setPorcentajeActualCedente(String porcentajeActualCedente) {
		this.porcentajeActualCedente = porcentajeActualCedente;
	}
	public String getPorcentajeNuevoCedente() {
		return porcentajeNuevoCedente;
	}
	public void setPorcentajeNuevoCedente(String porcentajeNuevoCedente) {
		this.porcentajeNuevoCedente = porcentajeNuevoCedente;
	}
	public String getDiferenciaPorcentaje() {
		return diferenciaPorcentaje;
	}
	public void setDiferenciaPorcentaje(String diferenciaPorcentaje) {
		this.diferenciaPorcentaje = diferenciaPorcentaje;
	}
	public String getNombrePropietarioNuevo() {
		return nombrePropietarioNuevo;
	}
	public void setNombrePropietarioNuevo(String nombrePropietarioNuevo) {
		this.nombrePropietarioNuevo = nombrePropietarioNuevo;
	}
	public String getNombrePropietarioAnterior() {
		return nombrePropietarioAnterior;
	}
	public void setNombrePropietarioAnterior(String nombrePropietarioAnterior) {
		this.nombrePropietarioAnterior = nombrePropietarioAnterior;
	}
}
