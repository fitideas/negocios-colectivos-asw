package com.accionfiduciaria.modelo.dtos;

public class DatosArchivoCargueDTO {
	
	private String grupo;
	private String porcentajeParticipacion;
	private String nombreTitular;
	private String tipoDoc;
	private String documentoTitular;
	private String porcentajeGiro;
	private String beneficiario;
	private String tipoDocBeneficiario;
	private String documento;
	private String tipoGiro;
	private String codBanco;
	private String tipoCuenta;
	private String numeroCuenta;
	
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public String getNombreTitular() {
		return nombreTitular;
	}
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getDocumentoTitular() {
		return documentoTitular;
	}
	public void setDocumentoTitular(String documentoTitular) {
		this.documentoTitular = documentoTitular;
	}
	public String getPorcentajeGiro() {
		return porcentajeGiro;
	}
	public void setPorcentajeGiro(String porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getTipoDocBeneficiario() {
		return tipoDocBeneficiario;
	}
	public void setTipoDocBeneficiario(String tipoDocBeneficiario) {
		this.tipoDocBeneficiario = tipoDocBeneficiario;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getTipoGiro() {
		return tipoGiro;
	}
	public void setTipoGiro(String tipoGiro) {
		this.tipoGiro = tipoGiro;
	}
	public String getCodBanco() {
		return codBanco;
	}
	public void setCodBanco(String codBanco) {
		this.codBanco = codBanco;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	

}
