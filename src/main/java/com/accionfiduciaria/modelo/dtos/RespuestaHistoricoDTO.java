package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaHistoricoDTO {

	private List<AsambleasDTO> historicoAsambleas;
	private List<ComiteDTO> historicoComites;

	/**
	 * @return the historicoAsambleas
	 */
	public List<AsambleasDTO> getHistoricoAsambleas() {
		return historicoAsambleas;
	}

	/**
	 * @param historicoAsambleas the historicoAsambleas to set
	 */
	public void setHistoricoAsambleas(List<AsambleasDTO> historicoAsambleas) {
		this.historicoAsambleas = historicoAsambleas;
	}

	/**
	 * @return the historicoComites
	 */
	public List<ComiteDTO> getHistoricoComites() {
		return historicoComites;
	}

	/**
	 * @param historicoComites the historicoComites to set
	 */
	public void setHistoricoComites(List<ComiteDTO> historicoComites) {
		this.historicoComites = historicoComites;
	}

}
