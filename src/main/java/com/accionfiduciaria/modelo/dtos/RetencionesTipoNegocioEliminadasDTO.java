package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RetencionesTipoNegocioEliminadasDTO {

	private Integer idTipoNegocio;
	private List<Integer> retenciones;

	public Integer getIdTipoNegocio() {
		return idTipoNegocio;
	}

	public void setIdTipoNegocio(Integer idTipoNegocio) {
		this.idTipoNegocio = idTipoNegocio;
	}

	public List<Integer> getRetenciones() {
		return retenciones;
	}

	public void setRetenciones(List<Integer> retenciones) {
		this.retenciones = retenciones;
	}

}
