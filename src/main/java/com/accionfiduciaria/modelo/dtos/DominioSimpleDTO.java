package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * Contiene los valores de los dominios simples, extiende a {@link DominioDTO}
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DominioSimpleDTO extends DominioDTO {
	
	/**
	 * El listado de valores del dominio.
	 */
	private List<ValorDominioSimpleDTO> valores;

	/**
	 * Contructor por defecto.
	 */
	public DominioSimpleDTO() {
		super();
	}
	
	/**
	 * Constructor que inicializa el codigo.
	 * 
	 * @param codigo El codigo del dominio.
	 */
	public DominioSimpleDTO(String codigo) {
		super(codigo);
	}

	/**
	 * Constructor que inicializa el codigo, el nombre y los valores del dominio.
	 * 
	 * @param codigo El codigo del dominio.
	 * @param nombre El nombre del dominio.
	 * @param valores {@link #valores}.
	 */
	public DominioSimpleDTO(String codigo, String nombre, List<ValorDominioSimpleDTO> valores) {
		super(codigo, nombre);
		this.valores = valores;
	}

	/**
	 * @return the valores
	 */
	public List<ValorDominioSimpleDTO> getValores() {
		return valores;
	}

	/**
	 * @param valores the valores to set
	 */
	public void setValores(List<ValorDominioSimpleDTO> valores) {
		this.valores = valores;
	}
}
