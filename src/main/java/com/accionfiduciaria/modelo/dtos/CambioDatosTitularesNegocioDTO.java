package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class CambioDatosTitularesNegocioDTO extends DatosBasicosPersonaDTO {

	private String resumenDatosCambiados;
	private List<DetalleDatosCambiadosDTO> detalleDatosCambiados;

	public String getResumenDatosCambiados() {
		return resumenDatosCambiados;
	}

	public void setResumenDatosCambiados(String resumenDatosCambiados) {
		this.resumenDatosCambiados = resumenDatosCambiados;
	}

	public List<DetalleDatosCambiadosDTO> getDetalleDatosCambiados() {
		return detalleDatosCambiados;
	}

	public void setDetalleDatosCambiados(List<DetalleDatosCambiadosDTO> detalleDatosCambiados) {
		this.detalleDatosCambiados = detalleDatosCambiados;
	}
}
