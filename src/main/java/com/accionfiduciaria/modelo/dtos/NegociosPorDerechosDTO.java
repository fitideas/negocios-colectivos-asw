package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

public class NegociosPorDerechosDTO {
	private String nombre;
	private String codSFC;
	private String codigoInterno;
	private String tipoDocumento;
	private String numeroDocumento;
	private String participacion;
	private String numeroDerechos;
	private String baseRetencion;
	private String totalRetenido;
	private String rentaExcenta;

	public NegociosPorDerechosDTO(String nombre, String codigoSFC, String codigoInterno, String tipoDocumento,
			String numeroDocumento) {
		super();
		this.nombre = nombre;
		this.codSFC = codigoSFC;
		this.codigoInterno = codigoInterno;
		this.setTipoDocumento(tipoDocumento);
		this.setNumeroDocumento(numeroDocumento);

	}

	public NegociosPorDerechosDTO() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getParticipacion() {
		return participacion;
	}

	public void setParticipacion(String participacion) {
		this.participacion = participacion;
	}

	public String getNumeroDerechos() {
		return numeroDerechos;
	}

	public void setNumeroDerechos(String numeroDerechos) {
		this.numeroDerechos = numeroDerechos;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getCodSFC() {
		return codSFC;
	}

	public void setCodSFC(String codSFC) {
		this.codSFC = codSFC;
	}

	public String getBaseRetencion() {
		return baseRetencion;
	}

	public void setBaseRetencion(String baseRetencion) {
		this.baseRetencion = baseRetencion;
	}

	public String getTotalRetenido() {
		return totalRetenido;
	}

	public void setTotalRetenido(String totalRetenido) {
		this.totalRetenido = totalRetenido;
	}

	public String getRentaExcenta() {
		return rentaExcenta;
	}

	public void setRentaExcenta(String rentaExcenta) {
		this.rentaExcenta = rentaExcenta;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSFC);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NegociosPorDerechosDTO other = (NegociosPorDerechosDTO) obj;
		return Objects.equals(codSFC, other.codSFC);
	}

	@Override
	public String toString() {
		return "NegociosPorDerechosDTO [nombre=" + nombre + ", codSFC=" + codSFC + ", codigoInterno=" + codigoInterno
				+ ", tipoDocumento=" + tipoDocumento + ", numeroDocumento=" + numeroDocumento + ", participacion="
				+ participacion + ", numeroDerechos=" + numeroDerechos + "]";
	}
}
