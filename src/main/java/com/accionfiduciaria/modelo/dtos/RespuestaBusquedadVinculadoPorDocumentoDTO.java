package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaBusquedadVinculadoPorDocumentoDTO {
	private List<RespuestaBusquedadVinculadoDTO> beneficiarios;

	/**
	 * @return the beneficiarios
	 */
	public List<RespuestaBusquedadVinculadoDTO> getBeneficiarios() {
		return beneficiarios;
	}

	/**
	 * @param beneficiarios the beneficiarios to set
	 */
	public void setBeneficiarios(List<RespuestaBusquedadVinculadoDTO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
}
