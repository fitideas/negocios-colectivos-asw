package com.accionfiduciaria.modelo.dtos;

/**
 * DTO base utilizado para el manejo de los datos de distribución de un titular o cotitular de un negocio.
 *  
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DatosDistribucionDTO {

	/**
	 * El tipo de documento del titular o cotitular.
	 */
	private String tipoDocumento;
	/**
	 * E número de documento del titular o cotitular.
	 */
	private String documento;
	/**
	 * El código SFC del negocio
	 */
	private String codigoNegocio;
	/**
	 * El nombre del asesor que hizo la vinculación a negocio.
	 */
	private String asesor;
	/**
	 * El número de radicado de la vinculación.
	 */
	private String radicadoVinculacion;
	/**
	 * El tipo de titularidad del del titular o cotitular.
	 */
	private String tipoTitularidad;
	/**
	 * El id del tipo de naturaleza del titular o cotitular.
	 */
	private Integer idTipoNaturaleza;
	
	/**
	 * El número de derechos del titular o cotitular.
	 */
	private String derechos;
	
	/**
	 * El porcentaje de participación del titular o cotitular.
	 */
	private String porcentajeParticipacion;
	
	
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the documento
	 */
	public String getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	/**
	 * @return the codigoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	/**
	 * @param codigoNegocio the codigoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	/**
	 * @return the asesor
	 */
	public String getAsesor() {
		return asesor;
	}

	/**
	 * @param asesor the asesor to set
	 */
	public void setAsesor(String asesor) {
		this.asesor = asesor;
	}

	/**
	 * @return the radicadoVinculacion
	 */
	public String getRadicadoVinculacion() {
		return radicadoVinculacion;
	}

	/**
	 * @param radicadoVinculacion the radicadoVinculacion to set
	 */
	public void setRadicadoVinculacion(String radicadoVinculacion) {
		this.radicadoVinculacion = radicadoVinculacion;
	}

	/**
	 * @return the tipoTitularidad
	 */
	public String getTipoTitularidad() {
		return tipoTitularidad;
	}

	/**
	 * @param tipoTitularidad the tipoTitularidad to set
	 */
	public void setTipoTitularidad(String tipoTitularidad) {
		this.tipoTitularidad = tipoTitularidad;
	}

	/**
	 * @return the idTipoNaturaleza
	 */
	public Integer getIdTipoNaturaleza() {
		return idTipoNaturaleza;
	}

	/**
	 * @param idTipoNaturaleza the idTipoNaturaleza to set
	 */
	public void setIdTipoNaturaleza(Integer idTipoNaturaleza) {
		this.idTipoNaturaleza = idTipoNaturaleza;
	}

	/**
	 * @return the derechos
	 */
	public String getDerechos() {
		return derechos;
	}

	/**
	 * @param derechos the derechos to set
	 */
	public void setDerechos(String derechos) {
		this.derechos = derechos;
	}

	/**
	 * @return the porcentajeParticipacion
	 */
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	/**
	 * @param porcentajeParticipacion the porcentajeParticipacion to set
	 */
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}	
}
