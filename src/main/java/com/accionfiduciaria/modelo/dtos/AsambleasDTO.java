package com.accionfiduciaria.modelo.dtos;

import java.io.Serializable;
import java.util.List;

public class AsambleasDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codigoSFC;
	private String fechaReunion;
	private String direccionReunion;
	private String ordenDia;
	private String radicado;
	private String nombreArchivo;
	private String usuario;
	private List<DecisionDTO> decision;
	private List<AsistenteDTO> mesaDirectiva;
	private int numeroAsistentes;
	private String numeroRadicado;
	private String archivo;

	public AsambleasDTO() {
		super();
	}

	/**
	 * @return the codigoSFC
	 */
	public String getCodigoSFC() {
		return codigoSFC;
	}

	/**
	 * @param codigoSFC the codigoSFC to set
	 */
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}

	/**
	 * @return the fechaReunion
	 */
	public String getFechaReunion() {
		return fechaReunion;
	}

	/**
	 * @param fechaReunion the fechaReunion to set
	 */
	public void setFechaReunion(String fechaReunion) {
		this.fechaReunion = fechaReunion;
	}

	/**
	 * @return the direccionReunion
	 */
	public String getDireccionReunion() {
		return direccionReunion;
	}

	/**
	 * @param direccionReunion the direccionReunion to set
	 */
	public void setDireccionReunion(String direccionReunion) {
		this.direccionReunion = direccionReunion;
	}

	/**
	 * @return the ordenDia
	 */
	public String getOrdenDia() {
		return ordenDia;
	}

	/**
	 * @param ordenDia the ordenDia to set
	 */
	public void setOrdenDia(String ordenDia) {
		this.ordenDia = ordenDia;
	}

	/**
	 * @return the radicado
	 */
	public String getRadicado() {
		return radicado;
	}

	/**
	 * @param radicado the radicado to set
	 */
	public void setRadicado(String radicado) {
		this.radicado = radicado;
	}

	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the decision
	 */
	public List<DecisionDTO> getDecision() {
		return decision;
	}

	/**
	 * @param decision the decision to set
	 */
	public void setDecision(List<DecisionDTO> decision) {
		this.decision = decision;
	}

	/**
	 * @return the mesaDirectiva
	 */
	public List<AsistenteDTO> getMesaDirectiva() {
		return mesaDirectiva;
	}

	/**
	 * @param mesaDirectiva the mesaDirectiva to set
	 */
	public void setMesaDirectiva(List<AsistenteDTO> mesaDirectiva) {
		this.mesaDirectiva = mesaDirectiva;
	}

	/**
	 * @return the numeroAsistentes
	 */
	public int getNumeroAsistentes() {
		return numeroAsistentes;
	}

	/**
	 * @param numeroAsistentes the numeroAsistentes to set
	 */
	public void setNumeroAsistentes(int numeroAsistentes) {
		this.numeroAsistentes = numeroAsistentes;
	}

	/**
	 * @return the numeroRadicado
	 */
	public String getNumeroRadicado() {
		return numeroRadicado;
	}

	/**
	 * @param numeroRadicado the numeroRadicado to set
	 */
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}

	/**
	 * @return the archivo
	 */
	public String getArchivo() {
		return archivo;
	}

	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

}