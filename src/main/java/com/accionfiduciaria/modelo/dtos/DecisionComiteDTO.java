package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class DecisionComiteDTO {
	private String descripcion;
	
	private List<DecisionResponsableDTO> responsables;
	
	private boolean tipo;
	
	
	
	/**
	 * @return the responsables
	 */
	public List<DecisionResponsableDTO> getResponsables() {
		return responsables;
	}
	/**
	 * @param responsables the responsables to set
	 */
	public void setResponsables(List<DecisionResponsableDTO> responsables) {
		this.responsables = responsables;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public String toString() {
		return "DecisionComiteDTO [descripcion=" + descripcion + ", responsables=" + responsables + "]";
	}
	public boolean isTipo() {
		return tipo;
	}
	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}

	
	
	
}
