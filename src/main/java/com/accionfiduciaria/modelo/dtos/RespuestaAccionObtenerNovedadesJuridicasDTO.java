package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaAccionObtenerNovedadesJuridicasDTO extends RespuestaPaginadaAccionDTO {
	
	private List<NovedadesJuridicasAccionDTO> results;

	/**
	 * @return the result
	 */
	public List<NovedadesJuridicasAccionDTO> getResults() {
		return results;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(List<NovedadesJuridicasAccionDTO> results) {
		this.results = results;
	}
}
