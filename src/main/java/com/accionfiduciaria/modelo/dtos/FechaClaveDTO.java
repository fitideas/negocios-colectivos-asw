package com.accionfiduciaria.modelo.dtos;

public class FechaClaveDTO {
	private String nombreFechaClave;
	private Integer idTipoFechaClave;
	private String fechaCalendario;

	/**
	 * 
	 */
	public FechaClaveDTO() {
	}

	/**
	 * @param nombreFechaClave
	 * @param fechaCalendario
	 */
	public FechaClaveDTO(String nombreFechaClave, String fechaCalendario) {
		this.nombreFechaClave = nombreFechaClave;
		this.fechaCalendario = fechaCalendario;
	}

	/**
	 * @return the nombreFechaClave
	 */
	public String getNombreFechaClave() {
		return nombreFechaClave;
	}

	/**
	 * @param nombreFechaClave the nombreFechaClave to set
	 */
	public void setNombreFechaClave(String nombreFechaClave) {
		this.nombreFechaClave = nombreFechaClave;
	}

	/**
	 * @return the idTipoFechaClave
	 */
	public Integer getIdTipoFechaClave() {
		return idTipoFechaClave;
	}

	/**
	 * @param idTipoFechaClave the idTipoFechaClave to set
	 */
	public void setIdTipoFechaClave(Integer idTipoFechaClave) {
		this.idTipoFechaClave = idTipoFechaClave;
	}

	/**
	 * @return the fechaCalendario
	 */
	public String getFechaCalendario() {
		return fechaCalendario;
	}

	/**
	 * @param fechaCalendario the fechaCalendario to set
	 */
	public void setFechaCalendario(String fechaCalendario) {
		this.fechaCalendario = fechaCalendario;
	}
}
