package com.accionfiduciaria.modelo.dtos;

public class RespuestaConteoDTO {

	private int compiladoTotal;
	private float compiladoMensual;
	private float compiladoAnual;
	

	private int titularidadTotal;
	private float titularidadMes;
	private float titularidadAnual;

	private int tipoNaturalezaTotal;
	private float tipoNaturalezaMes;
	private float tipoNaturalezAnual;

	private int retencionesTotal;
	private float retencionesMes;
	private float retencionesAnual;
	
	
	 private int numeroBeneficiarios;
	 private int cambioBeneficiarios;
	 private float cambioBeneficiariosMes;
	 private float cambioBeneficiariosAnual;
	 
	 private float porcentajeGiro;
	 private float porcentajeGiroMes;
	 private float porcentajeGiroAnual;
	 
	 private float tipoPago;
	 private float tipoPagoMes;
	 private float tipoPagoAnual;
	 
	 private float porcentajeDist;
	 private float porcentajeDistMes;
	 private float porcentajeDistAnual;
	   
	   	
	 private float cambioTotalPorcentajes;
	 private float cambioMensualPorcentajes;
	 private float cambioAnualPorcentajes;
	 
	 private float cambioTotalTitulares;
	 private float cambioMensualTitulares;
	 private float cambioAnualTitulares;
	 
	
	public RespuestaConteoDTO() {
		super();
	}

	public int getCompiladoTotal() {
		return compiladoTotal;
	}
	public void setCompiladoTotal(int compiladoTotal) {
		this.compiladoTotal = compiladoTotal;
	}
	public float getCompiladoMensual() {
		return compiladoMensual;
	}
	public void setCompiladoMensual(float compiladoMensual) {
		this.compiladoMensual = compiladoMensual;
	}
	public float getCompiladoAnual() {
		return compiladoAnual;
	}
	public void setCompiladoAnual(float compiladoAnual) {
		this.compiladoAnual = compiladoAnual;
	}

	public int getTitularidadTotal() {
		return titularidadTotal;
	}

	public void setTitularidadTotal(int titularidadTotal) {
		this.titularidadTotal = titularidadTotal;
	}

	public float getTitularidadMes() {
		return titularidadMes;
	}

	public void setTitularidadMes(float titularidadMes) {
		this.titularidadMes = titularidadMes;
	}

	public float getTitularidadAnual() {
		return titularidadAnual;
	}

	public void setTitularidadAnual(float titularidadAnual) {
		this.titularidadAnual = titularidadAnual;
	}

	public int getTipoNaturalezaTotal() {
		return tipoNaturalezaTotal;
	}

	public void setTipoNaturalezaTotal(int tipoNaturalezaTotal) {
		this.tipoNaturalezaTotal = tipoNaturalezaTotal;
	}

	public float getTipoNaturalezaMes() {
		return tipoNaturalezaMes;
	}

	public void setTipoNaturalezaMes(float tipoNaturalezaMes) {
		this.tipoNaturalezaMes = tipoNaturalezaMes;
	}



	public int getRetencionesTotal() {
		return retencionesTotal;
	}

	public void setRetencionesTotal(int retencionesTotal) {
		this.retencionesTotal = retencionesTotal;
	}

	public float getRetencionesMes() {
		return retencionesMes;
	}

	public void setRetencionesMes(float retencionesMes) {
		this.retencionesMes = retencionesMes;
	}

	public float getRetencionesAnual() {
		return retencionesAnual;
	}

	public void setRetencionesAnual(float retencionesAnual) {
		this.retencionesAnual = retencionesAnual;
	}

	public float getTipoNaturalezAnual() {
		return tipoNaturalezAnual;
	}

	public void setTipoNaturalezAnual(float tipoNaturalezAnual) {
		this.tipoNaturalezAnual = tipoNaturalezAnual;
	}

	public int getNumeroBeneficiarios() {
		return numeroBeneficiarios;
	}

	public void setNumeroBeneficiarios(int numeroBeneficiarios) {
		this.numeroBeneficiarios = numeroBeneficiarios;
	}

	public int getCambioBeneficiarios() {
		return cambioBeneficiarios;
	}

	public void setCambioBeneficiarios(int cambioBeneficiarios) {
		this.cambioBeneficiarios = cambioBeneficiarios;
	}

	public float getCambioBeneficiariosMes() {
		return cambioBeneficiariosMes;
	}

	public void setCambioBeneficiariosMes(float cambioBeneficiariosMes) {
		this.cambioBeneficiariosMes = cambioBeneficiariosMes;
	}

	public float getCambioBeneficiariosAnual() {
		return cambioBeneficiariosAnual;
	}

	public void setCambioBeneficiariosAnual(float cambioBeneficiariosAnual) {
		this.cambioBeneficiariosAnual = cambioBeneficiariosAnual;
	}

	public float getPorcentajeGiro() {
		return porcentajeGiro;
	}

	public void setPorcentajeGiro(float porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	public float getPorcentajeGiroMes() {
		return porcentajeGiroMes;
	}

	public void setPorcentajeGiroMes(float porcentajeGiromes) {
		this.porcentajeGiroMes = porcentajeGiromes;
	}

	public float getPorcentajeGiroAnual() {
		return porcentajeGiroAnual;
	}

	public void setPorcentajeGiroAnual(float porcentajeGiroAnual) {
		this.porcentajeGiroAnual = porcentajeGiroAnual;
	}

	public float getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(float tipoPago) {
		this.tipoPago = tipoPago;
	}

	public float getTipoPagoMes() {
		return tipoPagoMes;
	}

	public void setTipoPagoMes(float tipoPagoMes) {
		this.tipoPagoMes = tipoPagoMes;
	}

	public float getTipoPagoAnual() {
		return tipoPagoAnual;
	}

	public void setTipoPagoAnual(float tipoPagoAnual) {
		this.tipoPagoAnual = tipoPagoAnual;
	}

	public float getPorcentajeDist() {
		return porcentajeDist;
	}

	public void setPorcentajeDist(float porcentajeDist) {
		this.porcentajeDist = porcentajeDist;
	}

	public float getPorcentajeDistMes() {
		return porcentajeDistMes;
	}

	public void setPorcentajeDistMes(float porcentajeDistMes) {
		this.porcentajeDistMes = porcentajeDistMes;
	}

	public float getPorcentajeDistAnual() {
		return porcentajeDistAnual;
	}

	public void setPorcentajeDistAnual(float porcentajeDistAnual) {
		this.porcentajeDistAnual = porcentajeDistAnual;
	}
	
	
	public float getCambioTotalPorcentajes() {
		return cambioTotalPorcentajes;
	}

	public void setCambioTotalPorcentajes(float cambioTotalPorcentajes) {
		this.cambioTotalPorcentajes = cambioTotalPorcentajes;
	}

	public float getCambioMensualPorcentajes() {
		return cambioMensualPorcentajes;
	}

	public void setCambioMensualPorcentajes(float cambioMensualPorcentajes) {
		this.cambioMensualPorcentajes = cambioMensualPorcentajes;
	}

	public float getCambioAnualPorcentajes() {
		return cambioAnualPorcentajes;
	}

	public void setCambioAnualPorcentajes(float cambioAnualPorcentajes) {
		this.cambioAnualPorcentajes = cambioAnualPorcentajes;
	}

	public float getCambioTotalTitulares() {
		return cambioTotalTitulares;
	}

	public void setCambioTotalTitulares(float cambioTotalTitulares) {
		this.cambioTotalTitulares = cambioTotalTitulares;
	}

	public float getCambioMensualTitulares() {
		return cambioMensualTitulares;
	}

	public void setCambioMensualTitulares(float cambioMensualTitulares) {
		this.cambioMensualTitulares = cambioMensualTitulares;
	}

	public float getCambioAnualTitulares() {
		return cambioAnualTitulares;
	}

	public void setCambioAnualTitulares(float cambioAnualTitulares) {
		this.cambioAnualTitulares = cambioAnualTitulares;
	}

	
	
}
