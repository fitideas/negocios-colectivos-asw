package com.accionfiduciaria.modelo.dtos;

public class TipoPagoDTO {
	
	/**
	 * 
	 */
	private String codigo;
	/**
	 * 
	 */
	private Integer idEntidadBancaria;
	/**
	 * 
	 */
	private String tipoCuenta;
	/**
	 * 
	 */
	private String numeroCuentaBancaria;
	private String numeroEncargo;
	private String fideicomisoDestino;
	private String codigoPago;
	private String numeroReferencia;
	private String descripcionOtro;

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the idEntidadBancaria
	 */
	public Integer getIdEntidadBancaria() {
		return idEntidadBancaria;
	}

	/**
	 * @param idEntidadBancaria the idEntidadBancaria to set
	 */
	public void setIdEntidadBancaria(Integer idEntidadBancaria) {
		this.idEntidadBancaria = idEntidadBancaria;
	}

	/**
	 * @return the tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * @param tipoCuenta the tipoCuenta to set
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * @return the numeroCuentaBancaria
	 */
	public String getNumeroCuentaBancaria() {
		return numeroCuentaBancaria;
	}

	/**
	 * @param numeroCuentaBancaria the numeroCuentaBancaria to set
	 */
	public void setNumeroCuentaBancaria(String numeroCuentaBancaria) {
		this.numeroCuentaBancaria = numeroCuentaBancaria;
	}

	/**
	 * @return the numeroEncargo
	 */
	public String getNumeroEncargo() {
		return numeroEncargo;
	}

	/**
	 * @param numeroEncargo the numeroEncargo to set
	 */
	public void setNumeroEncargo(String numeroEncargo) {
		this.numeroEncargo = numeroEncargo;
	}

	/**
	 * @return the fideicomisoDestino
	 */
	public String getFideicomisoDestino() {
		return fideicomisoDestino;
	}

	/**
	 * @param fideicomisoDestino the fideicomisoDestino to set
	 */
	public void setFideicomisoDestino(String fideicomisoDestino) {
		this.fideicomisoDestino = fideicomisoDestino;
	}

	/**
	 * @return the codigoPago
	 */
	public String getCodigoPago() {
		return codigoPago;
	}

	/**
	 * @param codigoPago the codigoPago to set
	 */
	public void setCodigoPago(String codigoPago) {
		this.codigoPago = codigoPago;
	}

	/**
	 * @return the numeroReferencia
	 */
	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	/**
	 * @param numeroReferencia the numeroReferencia to set
	 */
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	/**
	 * @return the descripcionOtro
	 */
	public String getDescripcionOtro() {
		return descripcionOtro;
	}

	/**
	 * @param descripcionOtro the descripcionOtro to set
	 */
	public void setDescripcionOtro(String descripcionOtro) {
		this.descripcionOtro = descripcionOtro;
	}
}
