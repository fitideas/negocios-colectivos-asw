package com.accionfiduciaria.modelo.dtos;

public class DatosEquipoResponsableDTO {

	private String nombreCompleto;
	private String rol;

	/**
	 * 
	 */
	public DatosEquipoResponsableDTO() {
	}

	/**
	 * @param nombreCompleto
	 * @param rol
	 */
	public DatosEquipoResponsableDTO(String nombreCompleto, String rol) {
		this.nombreCompleto = nombreCompleto;
		this.rol = rol;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	@Override
	public String toString() {
		return "DatosEquipoResponsableDTO [nombreCompleto=" + nombreCompleto + ", rol=" + rol + "]";
	}
	
	

}
