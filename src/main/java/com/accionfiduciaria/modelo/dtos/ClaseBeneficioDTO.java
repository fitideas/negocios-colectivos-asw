package com.accionfiduciaria.modelo.dtos;

public class ClaseBeneficioDTO {
	private Integer idClaseBeneficio;
	private String descripcion;

	public Integer getIdClaseBeneficio() {
		return idClaseBeneficio;
	}

	public void setIdClaseBeneficio(Integer idClaseBeneficio) {
		this.idClaseBeneficio = idClaseBeneficio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
