package com.accionfiduciaria.modelo.dtos;

import com.accionfiduciaria.modelo.entidad.ResponsableNotificacion;

public class ResponsableNotificacionDTO extends ResponsablesNegocioDTO {

	private String correo;
	private String tipoResponsable;

	/**
	 * 
	 */
	public ResponsableNotificacionDTO() {
	}

	/**
	 * 
	 * @param responsableNegocio
	 */
	public ResponsableNotificacionDTO(ResponsableNotificacion responsableNegocio) {
		super(responsableNegocio.getNombre(), responsableNegocio.getResponsablesNotificacionPK().getDocumento(),
				responsableNegocio.getResponsablesNotificacionPK().getTipoDocumento());
		this.correo = responsableNegocio.getCorreo();
		this.tipoResponsable = responsableNegocio.getTipoResponsable();
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public String getTipoResponsable() {
		return tipoResponsable;
	}

	public void setTipoResponsable(String tipoResponsable) {
		this.tipoResponsable = tipoResponsable;
	}

	@Override
	public String toString() {
		return "ResponsableNotificacionDTO [correo=" + correo + ", tipoResponsable=" + tipoResponsable
				+ ", nombre()=" + getNombre() + ", numeroDocumento()=" + getNumeroDocumento()
				+ ", tipoDocumento()=" + getTipoDocumento() + "]";
	}

}
