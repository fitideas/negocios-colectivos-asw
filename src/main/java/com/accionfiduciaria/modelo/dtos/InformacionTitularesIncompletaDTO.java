package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class InformacionTitularesIncompletaDTO extends DatosBasicosPersonaDTO {
	
	List<String> datosFaltantes;

	public List<String> getDatosFaltantes() {
		return datosFaltantes;
	}

	public void setDatosFaltantes(List<String> datosFaltantes) {
		this.datosFaltantes = datosFaltantes;
	}
	
}
