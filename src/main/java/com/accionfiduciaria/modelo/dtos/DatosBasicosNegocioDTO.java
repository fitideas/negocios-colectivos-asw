package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

/**
 * DTO utilizado para envíar los datos basicos de un negocio.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DatosBasicosNegocioDTO {
	/**
	 * El código SFC del negocio.
	 */
	private String codigo;
	/**
	 * El nombre del negocio.
	 */
	private String nombre;
	
	/**
	 * Constructor por defecto.
	 */
	public DatosBasicosNegocioDTO() {
	}
	
	/**
	 * Constructor que inicializa los campos codigo y nombre.
	 * 
	 * @param codigo {@link #codigo}
	 * @param nombre {@link #nombre}
	 */
	public DatosBasicosNegocioDTO(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatosBasicosNegocioDTO other = (DatosBasicosNegocioDTO) obj;
		return Objects.equals(codigo, other.codigo);
	}}
