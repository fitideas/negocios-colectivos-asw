package com.accionfiduciaria.modelo.dtos;

public class DecisionDTO {

	private String descripcion;
	private String responsable;
	private int quorumAprobatorio;
	private boolean tipo;

	public DecisionDTO() {
	}

	public DecisionDTO(String descripcion, String responsable, int quorumAprobatorio) {
		super();
		this.descripcion = descripcion;
		this.responsable = responsable;
		this.quorumAprobatorio = quorumAprobatorio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public int getQuorumAprobatorio() {
		return quorumAprobatorio;
	}

	public void setQuorumAprobatorio(int quorumAprobatorio) {
		this.quorumAprobatorio = quorumAprobatorio;
	}

	public boolean getTipo() {
		return tipo;
	}

	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}
	
	

}
