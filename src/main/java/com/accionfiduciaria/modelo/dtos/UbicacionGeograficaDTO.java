/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author efarias
 *
 */
public class UbicacionGeograficaDTO {

	private String codigo;
	private String nombre;
	private String iso2;
	private String tipo;
	private String codigoPadre;

	public UbicacionGeograficaDTO() {

	}

	public UbicacionGeograficaDTO(String codigo, String nombre, String iso2, String tipo, String codigoPadre) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.iso2 = iso2;
		this.tipo = tipo;
		this.codigoPadre = codigoPadre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIso2() {
		return iso2;
	}

	public void setIso2(String iso2) {
		this.iso2 = iso2;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigoPadre() {
		return codigoPadre;
	}

	public void setCodigoPadre(String codigoPadre) {
		this.codigoPadre = codigoPadre;
	}

}
