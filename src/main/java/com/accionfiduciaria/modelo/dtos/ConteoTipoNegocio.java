package com.accionfiduciaria.modelo.dtos;

public class ConteoTipoNegocio {

	
	 private String tipo;
	 private int numero;

	 
	 

	 public ConteoTipoNegocio(String tipo, int numero) {
		super();
		this.tipo = tipo;
		this.numero = numero;
	}

	// Getter Methods 

	 public String getTipo() {
	  return tipo;
	 }

	 public int getNumero() {
	  return numero;
	 }

	 // Setter Methods 

	 public void setTipo(String tipo) {
	  this.tipo = tipo;
	 }

	 public void setNumero(int numero) {
	  this.numero = numero;
	 }

	@Override
	public String toString() {
		return "ConteoTipoNegocio [tipo=" + tipo + ", numero=" + numero + "]";
	}
	 
	 
}
