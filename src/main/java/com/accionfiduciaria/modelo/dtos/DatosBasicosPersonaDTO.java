package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

public class DatosBasicosPersonaDTO {

	private String nombreCompleto;
	private String tipoDocumento;
	private String numeroDocumento;
	
	/**
	 * 
	 */
	public DatosBasicosPersonaDTO() {
	}

	/**
	 * @param nombreCompleto
	 * @param tipoDocumento
	 * @param numeroDocumento
	 */
	public DatosBasicosPersonaDTO(String nombreCompleto, String tipoDocumento, String numeroDocumento) {
		this.nombreCompleto = nombreCompleto;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@Override
	public int hashCode() {
		return Objects.hash(numeroDocumento, tipoDocumento);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatosBasicosPersonaDTO other = (DatosBasicosPersonaDTO) obj;
		return Objects.equals(numeroDocumento, other.numeroDocumento)
				&& Objects.equals(tipoDocumento, other.tipoDocumento);
	}
}
