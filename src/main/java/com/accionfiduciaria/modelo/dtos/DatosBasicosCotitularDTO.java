package com.accionfiduciaria.modelo.dtos;

public class DatosBasicosCotitularDTO {
	
	private String tipoDocumentoCotitular;
	private String documentoCotitular;
	private boolean asociado;
	/**
	 * @return the tipoDocumentoCotitular
	 */
	public String getTipoDocumentoCotitular() {
		return tipoDocumentoCotitular;
	}
	/**
	 * @param tipoDocumentoCotitular the tipoDocumentoCotitular to set
	 */
	public void setTipoDocumentoCotitular(String tipoDocumentoCotitular) {
		this.tipoDocumentoCotitular = tipoDocumentoCotitular;
	}
	/**
	 * @return the documentoCotitular
	 */
	public String getDocumentoCotitular() {
		return documentoCotitular;
	}
	/**
	 * @param documentoCotitular the documentoCotitular to set
	 */
	public void setDocumentoCotitular(String documentoCotitular) {
		this.documentoCotitular = documentoCotitular;
	}
	/**
	 * @return the asociado
	 */
	public boolean isAsociado() {
		return asociado;
	}
	/**
	 * @param asociado the asociado to set
	 */
	public void setAsociado(boolean asociado) {
		this.asociado = asociado;
	}
}
