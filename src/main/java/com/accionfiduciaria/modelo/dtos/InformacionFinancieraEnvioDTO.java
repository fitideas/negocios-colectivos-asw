package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class InformacionFinancieraEnvioDTO extends InformacionFinancieraDTO{
	private List<BeneficiarioTitularDTO> beneficiarios;

	/**
	 * @return the beneficiarios
	 */
	public List<BeneficiarioTitularDTO> getBeneficiarios() {
		return beneficiarios;
	}

	/**
	 * @param beneficiarios the beneficiarios to set
	 */
	public void setBeneficiarios(List<BeneficiarioTitularDTO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
}
