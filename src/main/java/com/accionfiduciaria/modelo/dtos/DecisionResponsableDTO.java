package com.accionfiduciaria.modelo.dtos;

public class DecisionResponsableDTO {
	private String nomResponsable;
	private String tipoDocumento;
	private String numeroDocumento;
	/**
	 * @return the responsable
	 */
	public String getNomResponsable() {
		return nomResponsable;
	}
	/**
	 * @param responsable the responsable to set
	 */
	public void setNomResponsable(String responsable) {
		this.nomResponsable = responsable;
	}
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	@Override
	public String toString() {
		return "DecisionResponsableDTO [nomResponsable=" + nomResponsable + ", tipoDocumento=" + tipoDocumento
				+ ", numeroDocumento=" + numeroDocumento + "]";
	}

	
}
