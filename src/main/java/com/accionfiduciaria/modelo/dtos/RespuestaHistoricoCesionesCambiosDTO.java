package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class RespuestaHistoricoCesionesCambiosDTO {
	
	private int totalRegistros;
	private List<HistoricoCesionesCambioDTO> listaCambios;
	public int getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	public List<HistoricoCesionesCambioDTO> getListaCambios() {
		return listaCambios;
	}
	public void setListaCambios(List<HistoricoCesionesCambioDTO> listaCambios) {
		this.listaCambios = listaCambios;
	}
}
