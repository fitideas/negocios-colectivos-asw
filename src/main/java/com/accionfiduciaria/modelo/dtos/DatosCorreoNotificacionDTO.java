/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author efarias
 *
 */
public class DatosCorreoNotificacionDTO {

	private String asuntoCorreoNotificacion;
	private String cuerpoCorreoNotificacion;
	private String direccionCorreoNotificacionFallida;

	public DatosCorreoNotificacionDTO() {
	}

	public DatosCorreoNotificacionDTO(String asuntoCorreoNotificacion, String cuerpoCorreoNotificacion,
			String direccionCorreoNotificacionFallida) {
		this.asuntoCorreoNotificacion = asuntoCorreoNotificacion;
		this.cuerpoCorreoNotificacion = cuerpoCorreoNotificacion;
		this.direccionCorreoNotificacionFallida = direccionCorreoNotificacionFallida;
	}

	public String getAsuntoCorreoNotificacion() {
		return asuntoCorreoNotificacion;
	}

	public void setAsuntoCorreoNotificacion(String asuntoCorreoNotificacion) {
		this.asuntoCorreoNotificacion = asuntoCorreoNotificacion;
	}

	public String getCuerpoCorreoNotificacion() {
		return cuerpoCorreoNotificacion;
	}

	public void setCuerpoCorreoNotificacion(String cuerpoCorreoNotificacion) {
		this.cuerpoCorreoNotificacion = cuerpoCorreoNotificacion;
	}

	public String getDireccionCorreoNotificacionFallida() {
		return direccionCorreoNotificacionFallida;
	}

	public void setDireccionCorreoNotificacionFallida(String direccionCorreoNotificacionFallida) {
		this.direccionCorreoNotificacionFallida = direccionCorreoNotificacionFallida;
	}

}
