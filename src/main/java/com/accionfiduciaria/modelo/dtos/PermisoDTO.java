/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

/**
 * DTO usado para el envío de los datos de un permiso de un rol.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class PermisoDTO {
	/**
	 * El id de la funcionalidad (permiso).
	 */
	private Integer idFuncionalidad;
	/**
	 * EL nommbre de la funcionalidad.
	 */
	private String funcionalidad;
	/**
	 * Indica si la funcionalidad admite la opción de escritura o no.
	 */
	private Boolean permiteEscribir;
	
	/**
	 * Contructor por defecto.
	 */
	public PermisoDTO() {
	}
	
	/**
	 * Constructor que inicializa el campo idFuncionalidad
	 * 
	 * @param idFuncionalidad {@link #idFuncionalidad}
	 */
	public PermisoDTO(Integer idFuncionalidad) {
		this.idFuncionalidad = idFuncionalidad;
	}
	
	/**
	 * Constructor que inicializa los campos idFuncionalidad, funcionalidad y permiteEscribir.
	 * 
	 * @param idFuncionalidad {@link #idFuncionalidad}
	 * @param funcionalidad {@link #funcionalidad}
	 * @param permiteEscribir {@link #permiteEscribir}
	 */
	public PermisoDTO(Integer idFuncionalidad, String funcionalidad, Boolean permiteEscribir) {
		this.idFuncionalidad = idFuncionalidad;
		this.funcionalidad = funcionalidad;
		this.permiteEscribir = permiteEscribir;
	}

	/**
	 * @return the idFuncionalidad
	 */
	public Integer getIdFuncionalidad() {
		return idFuncionalidad;
	}

	/**
	 * @param idFuncionalidad the idFuncionalidad to set
	 */
	public void setIdFuncionalidad(Integer idFuncionalidad) {
		this.idFuncionalidad = idFuncionalidad;
	}

	/**
	 * @return the funcionalidad
	 */
	public String getFuncionalidad() {
		return funcionalidad;
	}

	/**
	 * @param funcionalidad the funcionalidad to set
	 */
	public void setFuncionalidad(String funcionalidad) {
		this.funcionalidad = funcionalidad;
	}

	/**
	 * @return the permiteEscribir
	 */
	public Boolean isPermiteEscribir() {
		return permiteEscribir;
	}

	/**
	 * @param permiteEscribir the permiteEscribir to set
	 */
	public void setPermiteEscribir(Boolean permiteEscribir) {
		this.permiteEscribir = permiteEscribir;
	}

	@Override
	public int hashCode() {
		return Objects.hash(funcionalidad, idFuncionalidad, permiteEscribir);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermisoDTO other = (PermisoDTO) obj;
		return Objects.equals(funcionalidad, other.funcionalidad)
				&& Objects.equals(idFuncionalidad, other.idFuncionalidad) && permiteEscribir == other.permiteEscribir;
	}        
}
