package com.accionfiduciaria.modelo.dtos;

public class CotitularDTO extends CoTitularDatosBasicosDTO {
	private String documentoTitular;
	private String tipoDocumentoTitular;
	private String codigoNegocio;
	private String nombreApellidoCotitular;
	
	/**
	 * @return the documentoTitular
	 */
	public String getDocumentoTitular() {
		return documentoTitular;
	}

	/**
	 * @param documentoTitular the documentoTitular to set
	 */
	public void setDocumentoTitular(String documentoTitular) {
		this.documentoTitular = documentoTitular;
	}

	/**
	 * @return the tipoDocumentoTitular
	 */
	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	/**
	 * @param tipoDocumentoTitular the tipoDocumentoTitular to set
	 */
	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	/**
	 * @return the codigoNegocio
	 */
	public String getCodigoNegocio() {
		return codigoNegocio;
	}

	/**
	 * @param codigoNegocio the codigoNegocio to set
	 */
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}

	/**
	 * @return the nombreApellidoCotitular
	 */
	public String getNombreApellidoCotitular() {
		return nombreApellidoCotitular;
	}

	/**
	 * @param nombreApellidoCotitular the nombreApellidoCotitular to set
	 */
	public void setNombreApellidoCotitular(String nombreApellidoCotitular) {
		this.nombreApellidoCotitular = nombreApellidoCotitular;
	}
}
