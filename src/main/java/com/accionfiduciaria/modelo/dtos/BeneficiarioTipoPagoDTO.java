package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;
import java.util.Objects;

public class BeneficiarioTipoPagoDTO {

	private String beneficiario;
	private String tipoDocumento;
	private String numeroDocumento;
	private BigDecimal porcentajeGiro;
	private BigDecimal porcentajeDistribucion;
	private Float numeroDerechos;
	private String tipoPago;
	private String tipoCuenta;
	private String idEntidadBancaria;
	private String codigoBanco;
	private String numeroCuentaBancaria;
	private String numeroEncargo;
	private String fideicomisoDestino;
	private String codigoPago;
	private String numeroReferencia;
	private String descripcionOtro;
	private BigDecimal porcentaje;

	/**
	 * 
	 */
	public BeneficiarioTipoPagoDTO() {
	}

	/**
	 * @param tipoDocumento
	 * @param numeroDocumento
	 */
	public BeneficiarioTipoPagoDTO(String tipoDocumento, String numeroDocumento, String tipoPago) {
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.tipoPago = tipoPago;
	}

	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the porcentajeGiro
	 */
	public BigDecimal getPorcentajeGiro() {
		return porcentajeGiro;
	}

	/**
	 * @param porcentajeGiro the porcentajeGiro to set
	 */
	public void setPorcentajeGiro(BigDecimal porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	/**
	 * @return the porcentajeDistribucion
	 */
	public BigDecimal getPorcentajeDistribucion() {
		return porcentajeDistribucion;
	}

	/**
	 * @param porcentajeDistribucion the porcentajeDistribucion to set
	 */
	public void setPorcentajeDistribucion(BigDecimal porcentajeDistribuccion) {
		this.porcentajeDistribucion = porcentajeDistribuccion;
	}

	/**
	 * @return the tipoPago
	 */
	public String getTipoPago() {
		return tipoPago;
	}

	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 * @return the tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * @param tipoCuenta the tipoCuenta to set
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * @return the idEntidadBancaria
	 */
	public String getIdEntidadBancaria() {
		return idEntidadBancaria;
	}

	/**
	 * @param idEntidadBancaria the idEntidadBancaria to set
	 */
	public void setIdEntidadBancaria(String idEntidadBancaria) {
		this.idEntidadBancaria = idEntidadBancaria;
	}

	/**
	 * @return the codigoBanco
	 */
	public String getCodigoBanco() {
		return codigoBanco;
	}

	/**
	 * @param codigoBanco the codigoBanco to set
	 */
	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}

	/**
	 * @return the numeroCuentaBancaria
	 */
	public String getNumeroCuentaBancaria() {
		return numeroCuentaBancaria;
	}

	/**
	 * @param numeroCuentaBancaria the numeroCuentaBancaria to set
	 */
	public void setNumeroCuentaBancaria(String numeroCuentaBancaria) {
		this.numeroCuentaBancaria = numeroCuentaBancaria;
	}

	/**
	 * @return the numeroEncargo
	 */
	public String getNumeroEncargo() {
		return numeroEncargo;
	}

	/**
	 * @param numeroEncargo the numeroEncargo to set
	 */
	public void setNumeroEncargo(String numeroEncargo) {
		this.numeroEncargo = numeroEncargo;
	}

	/**
	 * @return the fideicomisoDestino
	 */
	public String getFideicomisoDestino() {
		return fideicomisoDestino;
	}

	/**
	 * @param fideicomisoDestino the fideicomisoDestino to set
	 */
	public void setFideicomisoDestino(String fideicomisoDestino) {
		this.fideicomisoDestino = fideicomisoDestino;
	}

	/**
	 * @return the codigoPago
	 */
	public String getCodigoPago() {
		return codigoPago;
	}

	/**
	 * @param codigoPago the codigoPago to set
	 */
	public void setCodigoPago(String codigoPago) {
		this.codigoPago = codigoPago;
	}

	/**
	 * @return the numeroReferencia
	 */
	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	/**
	 * @param numeroReferencia the numeroReferencia to set
	 */
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	/**
	 * @return the descripcionOtro
	 */
	public String getDescripcionOtro() {
		return descripcionOtro;
	}

	/**
	 * @param descripcionOtro the descripcionOtro to set
	 */
	public void setDescripcionOtro(String descripcionOtro) {
		this.descripcionOtro = descripcionOtro;
	}

	/**
	 * @return the numeroDerechos
	 */
	public Float getNumeroDerechos() {
		return numeroDerechos;
	}

	/**
	 * @param numeroDerechos the numeroDerechos to set
	 */
	public void setNumeroDerechos(Float numeroDerechos) {
		this.numeroDerechos = numeroDerechos;
	}
	
	

	public BigDecimal getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	@Override
	public int hashCode() {
		return Objects.hash(numeroDocumento, tipoDocumento, tipoPago);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BeneficiarioTipoPagoDTO other = (BeneficiarioTipoPagoDTO) obj;
		return Objects.equals(numeroDocumento, other.numeroDocumento)
				&& Objects.equals(tipoDocumento, other.tipoDocumento) && Objects.equals(tipoPago, other.tipoPago);
	}
}
