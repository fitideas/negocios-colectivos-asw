package com.accionfiduciaria.modelo.dtos;

public class RepuestaConsultaNegocioDTO {

	private String sfc;
	private String codigo;
	private String nombre;
	private EstadoNegocioDTO estadoNegocio;

	/**
	 * @return the sfc
	 */
	public String getSfc() {
		return sfc;
	}

	/**
	 * @param sfc the sfc to set
	 */
	public void setSfc(String sfc) {
		this.sfc = sfc;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the estadoNegocio
	 */
	public EstadoNegocioDTO getEstadoNegocio() {
		return estadoNegocio;
	}

	/**
	 * @param estadoNegocio the estadoNegocio to set
	 */
	public void setEstadoNegocio(EstadoNegocioDTO estadoNegocio) {
		this.estadoNegocio = estadoNegocio;
	}

}
