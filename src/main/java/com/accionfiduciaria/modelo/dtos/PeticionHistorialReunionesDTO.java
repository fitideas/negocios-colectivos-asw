package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class PeticionHistorialReunionesDTO {
	
	private String codigoSFC;
	private String tipoReunion;
	private List<DatosBasicosPersonaDTO> usuario; 
	private String numeroRadicado;
	private String decision;
	private String tareaPendiente;
	private List<String> rangoFechas;
	
	
	
	
	public String getCodigoSFC() {
		return codigoSFC;
	}
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}
	public String getTipoReunion() {
		return tipoReunion;
	}
	public void setTipoReunion(String tipoReunion) {
		this.tipoReunion = tipoReunion;
	}
	public List<DatosBasicosPersonaDTO> getUsuario() {
		return usuario;
	}
	public void setUsuario(List<DatosBasicosPersonaDTO> usuario) {
		this.usuario = usuario;
	}
	public String getNumeroRadicado() {
		return numeroRadicado;
	}
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public String getTareaPendiente() {
		return tareaPendiente;
	}
	public void setTareaPendiente(String tareaPendiente) {
		this.tareaPendiente = tareaPendiente;
	}
	public List<String> getRangoFechas() {
		return rangoFechas;
	}
	public void setRangoFechas(List<String> rangoFechas) {
		this.rangoFechas = rangoFechas;
	}
	
	

}
