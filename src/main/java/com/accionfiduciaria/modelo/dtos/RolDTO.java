/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * DTO usado para el envío de los datos de un rol y sus permisos.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class RolDTO {
	
	/**
	 * El id del rol.
	 */
	private Integer idRol;
	/**
	 * EL nombre del rol.
	 */
	private String rol;
	/**
	 * El lista de funcionalidades (permisos) de un rol.
	 */
	private ListaFuncionalidadesDTO listaFuncionalidades;
	
	/**
	 * Constructor por defecto.
	 */
	public RolDTO() {
	}

	/**
	 * Constructor que inicializa los campos idRol, rol y lsitaFuncionalidades.
	 * 
	 * @param idRol {@link #idRol}
	 * @param rol {@link #rol}
	 * @param listaFuncionalidades {@link #listaFuncionalidades}
	 */
	public RolDTO(Integer idRol, String rol, ListaFuncionalidadesDTO listaFuncionalidades) {
		this.idRol = idRol;
		this.rol = rol;
		this.listaFuncionalidades = listaFuncionalidades;
	}

	/**
	 * @return the idRol
	 */
	public Integer getIdRol() {
		return idRol;
	}

	/**
	 * @param idRol the idRol to set
	 */
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	/**
	 * @return the rol
	 */
	public String getRol() {
		return rol;
	}

	/**
	 * @param rol the rol to set
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}

	/**
	 * @return the listaFuncionalidades
	 */
	public ListaFuncionalidadesDTO getListaFuncionalidades() {
		return listaFuncionalidades;
	}

	/**
	 * @param listaFuncionalidades the listaFuncionalidades to set
	 */
	public void setListaFuncionalidades(ListaFuncionalidadesDTO listaFuncionalidades) {
		this.listaFuncionalidades = listaFuncionalidades;
	}	
	
}
