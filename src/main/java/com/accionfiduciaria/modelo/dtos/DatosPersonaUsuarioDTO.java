package com.accionfiduciaria.modelo.dtos;

public class DatosPersonaUsuarioDTO {
	private String nombreCompleto;
	private String tipoDocumento;
	private String numeroDocumento;
	private String login;
	
	
	
	public DatosPersonaUsuarioDTO() {
		super();
	}
	public DatosPersonaUsuarioDTO(String nombreCompleto, String tipoDocumento, String numeroDocumento, String login) {
		super();
		this.nombreCompleto = nombreCompleto;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.login = login;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	
	

}
