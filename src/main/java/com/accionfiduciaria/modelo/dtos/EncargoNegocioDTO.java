package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

public class EncargoNegocioDTO {
	
	private String estado;
	private String numeroEncargo;
	private String movimientoFondo;
	private String numeroPagoOrdinario;

	/**
	 * 
	 */
	public EncargoNegocioDTO() {
	}
	
	/**
	 * @param estado
	 * @param numeroEncargo
	 */
	public EncargoNegocioDTO(String estado, String numeroEncargo) {
		super();
		this.estado = estado;
		this.numeroEncargo = numeroEncargo;
	}

	/**
	 * @param estado
	 * @param numeroEncargo
	 * @param movimientoFondo
	 * @param numeroPagoOrdinario
	 */
	public EncargoNegocioDTO(String estado, String numeroEncargo, String movimientoFondo, String numeroPagoOrdinario) {
		this.estado = estado;
		this.numeroEncargo = numeroEncargo;
		this.movimientoFondo = movimientoFondo;
		this.numeroPagoOrdinario = numeroPagoOrdinario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNumeroEncargo() {
		return numeroEncargo;
	}

	public void setNumeroEncargo(String numeroEncargo) {
		this.numeroEncargo = numeroEncargo;
	}

	public String getMovimientoFondo() {
		return movimientoFondo;
	}

	public void setMovimientoFondo(String movimientoFondo) {
		this.movimientoFondo = movimientoFondo;
	}

	public String getNumeroPagoOrdinario() {
		return numeroPagoOrdinario;
	}

	public void setNumeroPagoOrdinario(String numeroPagoOrdinario) {
		this.numeroPagoOrdinario = numeroPagoOrdinario;
	}

	@Override
	public int hashCode() {
		return Objects.hash(numeroEncargo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EncargoNegocioDTO other = (EncargoNegocioDTO) obj;
		return Objects.equals(numeroEncargo, other.numeroEncargo);
	}	
}
