package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class ParametrosFiltroDTO {

	private List<String> rangoFecha;
	private List<Integer> tipoNegocio;
	private List<String> negocios;
	private List<String> responsablesNegocio;
	private List<DatosBasicosPersonaDTO> solicitadoPor;
	private String tipoDocumentoVinculado;
	private String numeroDocumentoVinculado;

	public ParametrosFiltroDTO() {

	}

	public List<String> getRangoFecha() {
		return rangoFecha;
	}

	public void setRangoFecha(List<String> rangoFecha) {
		this.rangoFecha = rangoFecha;
	}

	public List<Integer> getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(List<Integer> tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public List<String> getNegocios() {
		return negocios;
	}

	public void setNegocios(List<String> negocios) {
		this.negocios = negocios;
	}

	public List<DatosBasicosPersonaDTO> getSolicitadoPor() {
		return solicitadoPor;
	}

	public void setSolicitadoPor(List<DatosBasicosPersonaDTO> solicitadoPor) {
		this.solicitadoPor = solicitadoPor;
	}

	public String getTipoDocumentoVinculado() {
		return tipoDocumentoVinculado;
	}

	public void setTipoDocumentoVinculado(String tipoDocumentoVinculado) {
		this.tipoDocumentoVinculado = tipoDocumentoVinculado;
	}

	public String getNumeroDocumentoVinculado() {
		return numeroDocumentoVinculado;
	}

	public void setNumeroDocumentoVinculado(String numeroDocumentoVinculado) {
		this.numeroDocumentoVinculado = numeroDocumentoVinculado;
	}

	public List<String> getResponsablesNegocio() {
		return responsablesNegocio;
	}

	public void setResponsablesNegocio(List<String> responsablesNegocio) {
		this.responsablesNegocio = responsablesNegocio;
	}

	@Override
	public String toString() {
		return "ParametrosFiltroDTO [rangoFecha=" + rangoFecha + ", tipoNegocio=" + tipoNegocio + ", negocios="
				+ negocios + ", profesionales=" + responsablesNegocio + ", solicitadoPor=" + solicitadoPor
				+ ", tipoDocumentoVinculado=" + tipoDocumentoVinculado + ", numeroDocumentoVinculado="
				+ numeroDocumentoVinculado + "]";
	}

}
