/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author José Santiago Polo Acosta - 13/12/2019 - jpolo@asesoftware.com
 *
 */
public class ListaVinculadosDTO {
	
	@JsonProperty("totalbeneficiarios")
	private int totalBeneficiarios;
	private List<DatosBasicosVinculadoDTO> beneficiarios;
	
	/**
	 * 
	 */
	public ListaVinculadosDTO() {
	}

	/**
	 * @param totalBeneficiarios
	 * @param beneficiarios
	 */
	public ListaVinculadosDTO(int totalBeneficiarios, List<DatosBasicosVinculadoDTO> beneficiarios) {
		this.totalBeneficiarios = totalBeneficiarios;
		this.beneficiarios = beneficiarios;
	}

	/**
	 * @return the totalBeneficiarios
	 */
	public int getTotalBeneficiarios() {
		return totalBeneficiarios;
	}

	/**
	 * @param totalBeneficiarios the totalBeneficiarios to set
	 */
	public void setTotalBeneficiarios(int totalBeneficiarios) {
		this.totalBeneficiarios = totalBeneficiarios;
	}

	/**
	 * @return the beneficiarios
	 */
	public List<DatosBasicosVinculadoDTO> getBeneficiarios() {
		return beneficiarios;
	}

	/**
	 * @param beneficiarios the beneficiarios to set
	 */
	public void setBeneficiarios(List<DatosBasicosVinculadoDTO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
	
	
}
