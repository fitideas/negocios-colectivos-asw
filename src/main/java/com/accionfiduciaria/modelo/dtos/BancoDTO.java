package com.accionfiduciaria.modelo.dtos;

public class BancoDTO {
	
	private Integer id;
	private String descripcion;
	private String codigo;
	
	/**
	 * 
	 */
	public BancoDTO() {
	}

	/**
	 * @param id
	 * @param descripcion
	 */
	public BancoDTO(Integer id, String nombre, String codigo) {
		this.id = id;
		this.descripcion = nombre;
		this.codigo = codigo;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer codigo) {
		this.id = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String nombre) {
		this.descripcion = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
