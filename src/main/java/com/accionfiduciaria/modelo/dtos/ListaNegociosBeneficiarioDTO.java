package com.accionfiduciaria.modelo.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListaNegociosBeneficiarioDTO {
	
	@JsonProperty("totalnegocios")
	private int totalNegocios;
	private List<DatosBasicosNegocioDTO> negocios;
	
	/**
	 * 
	 */
	public ListaNegociosBeneficiarioDTO() {
	}
	
	/**
	 * @param totalNegocios
	 * @param negocios
	 */
	public ListaNegociosBeneficiarioDTO(int totalNegocios, List<DatosBasicosNegocioDTO> negocios) {
		this.totalNegocios = totalNegocios;
		this.negocios = negocios;
	}

	/**
	 * @return the totalNegocios
	 */
	public int getTotalNegocios() {
		return totalNegocios;
	}

	/**
	 * @param totalNegocios the totalNegocios to set
	 */
	public void setTotalNegocios(int totalNegocios) {
		this.totalNegocios = totalNegocios;
	}

	/**
	 * @return the negocios
	 */
	public List<DatosBasicosNegocioDTO> getNegocios() {
		return negocios;
	}

	/**
	 * @param negocios the negocios to set
	 */
	public void setNegocios(List<DatosBasicosNegocioDTO> negocios) {
		this.negocios = negocios;
	}
	
}
