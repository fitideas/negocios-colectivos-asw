package com.accionfiduciaria.modelo.dtos;

import java.io.Serializable;
import java.util.List;

public class RespuestaDetalleBeneficiarioPagoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer totalRegistros;
	private List<TitularPagoDto> listaDeBeneficiarios;

	public Integer getTotalRegistros() {
		return totalRegistros;
	}

	public void setTotalRegistros(Integer totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	public List<TitularPagoDto> getListaDeBeneficiarios() {
		return listaDeBeneficiarios;
	}

	public void setListaDeBeneficiarios(List<TitularPagoDto> listaDeBeneficiarios) {
		this.listaDeBeneficiarios = listaDeBeneficiarios;
	}

}
