package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

public class BeneficiarioPagosDTO {

	private String id;
	private String negocioVinculado;
	private Integer tipoNegocio;
	private BigDecimal porcentajeParticipacion;
	private String nombreBeneficiario;
	private String tipoDocumentoBeneficiario;
	private String numeroDocumentoBeneficiario;
	private BigDecimal porcentajeGiro;
	private BigDecimal porcentajeDistribuido;
	private BigDecimal valorAGirar;
	private BigDecimal gravamentMovimientoFinanciero;
	private String tipoPago;
	private String idTipoPago;
	private String valorTipoPago;
	private String prefijoTipoPago;
	private BigDecimal subTotalGirados;
	private String tipoCuenta;
	private String idTipoCuenta;
	private String entidadBancariaFidecomiso;
	private String codigoEntidadBancaria;
	private String idEntidadBancaria;
	private String numeroCuentaEncargo;
	private String estadoPago;
	private String fechaPago;
	private String codigoNegocio;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNegocioVinculado() {
		return negocioVinculado;
	}
	public void setNegocioVinculado(String negocioVinculado) {
		this.negocioVinculado = negocioVinculado;
	}
	public Integer getTipoNegocio() {
		return tipoNegocio;
	}
	public void setTipoNegocio(Integer tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}
	public BigDecimal getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getTipoDocumentoBeneficiario() {
		return tipoDocumentoBeneficiario;
	}
	public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
		this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
	}
	public String getNumeroDocumentoBeneficiario() {
		return numeroDocumentoBeneficiario;
	}
	public void setNumeroDocumentoBeneficiario(String numeroDocumentoBeneficiario) {
		this.numeroDocumentoBeneficiario = numeroDocumentoBeneficiario;
	}
	public BigDecimal getPorcentajeGiro() {
		return porcentajeGiro;
	}
	public void setPorcentajeGiro(BigDecimal porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}
	public BigDecimal getPorcentajeDistribuido() {
		return porcentajeDistribuido;
	}
	public void setPorcentajeDistribuido(BigDecimal porcentajeDistribuido) {
		this.porcentajeDistribuido = porcentajeDistribuido;
	}
	public BigDecimal getValorAGirar() {
		return valorAGirar;
	}
	public void setValorAGirar(BigDecimal valorAGirar) {
		this.valorAGirar = valorAGirar;
	}
	public BigDecimal getGravamentMovimientoFinanciero() {
		return gravamentMovimientoFinanciero;
	}
	public void setGravamentMovimientoFinanciero(BigDecimal gravamentMovimientoFinanciero) {
		this.gravamentMovimientoFinanciero = gravamentMovimientoFinanciero;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getValorTipoPago() {
		return valorTipoPago;
	}
	public void setValorTipoPago(String valorTipoPago) {
		this.valorTipoPago = valorTipoPago;
	}
	public BigDecimal getSubTotalGirados() {
		return subTotalGirados;
	}
	public void setSubTotalGirados(BigDecimal subTotalGirados) {
		this.subTotalGirados = subTotalGirados;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getEntidadBancariaFidecomiso() {
		return entidadBancariaFidecomiso;
	}
	public void setEntidadBancariaFidecomiso(String entidadBancariaFidecomiso) {
		this.entidadBancariaFidecomiso = entidadBancariaFidecomiso;
	}
	public String getCodigoEntidadBancaria() {
		return codigoEntidadBancaria;
	}
	public void setCodigoEntidadBancaria(String codigoEntidadBancaria) {
		this.codigoEntidadBancaria = codigoEntidadBancaria;
	}
	public String getNumeroCuentaEncargo() {
		return numeroCuentaEncargo;
	}
	public void setNumeroCuentaEncargo(String numeroCuentaEncargo) {
		this.numeroCuentaEncargo = numeroCuentaEncargo;
	}
	public String getEstadoPago() {
		return estadoPago;
	}
	public void setEstadoPago(String estadoPago) {
		this.estadoPago = estadoPago;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	
	public String getIdTipoPago() {
		return idTipoPago;
	}
	public void setIdTipoPago(String idTipoPago) {
		this.idTipoPago = idTipoPago;
	}
	public String getIdTipoCuenta() {
		return idTipoCuenta;
	}
	public void setIdTipoCuenta(String idTipoCuenta) {
		this.idTipoCuenta = idTipoCuenta;
	}
	public String getIdEntidadBancaria() {
		return idEntidadBancaria;
	}
	public void setIdEntidadBancaria(String idEntidadBancaria) {
		this.idEntidadBancaria = idEntidadBancaria;
	}
	public String getPrefijoTipoPago() {
		return prefijoTipoPago;
	}
	public void setPrefijoTipoPago(String prefijoTipoPago) {
		this.prefijoTipoPago = prefijoTipoPago;
	}
	public String getCodigoNegocio() {
		return codigoNegocio;
	}
	public void setCodigoNegocio(String codigoNegocio) {
		this.codigoNegocio = codigoNegocio;
	}
}
