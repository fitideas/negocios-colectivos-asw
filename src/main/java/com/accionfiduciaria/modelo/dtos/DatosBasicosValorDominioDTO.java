package com.accionfiduciaria.modelo.dtos;

import java.util.Objects;

/**
 * DTO utilizado para el envío de los datos basicos de un valor de dominio.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DatosBasicosValorDominioDTO {
	/**
	 * El id del valor de dominio.
	 */
	private Integer id;
	
	/**
	 * La descripción del valor de dominio.
	 */
	private String descripcion;
	
	/**
	 * El tipo de unidad que utiliza el valor de dominio
	 */
	private String unidad;
	
	/**
	 * El valor del registro.
	 */
	private String valor; 
	
	/**
	 * Constructor por defecto.
	 */
	public DatosBasicosValorDominioDTO() {
	}
	
	/**
	 * 
	 * @param id
	 */
	public DatosBasicosValorDominioDTO(Integer id) {
		this.id = id;
	}
	
	/**
	 * Constructor que inicializa los campos id y descripcion.
	 * 
	 * @param id {@link #id}
	 * @param descripcion {@link #descripcion}
	 */
	public DatosBasicosValorDominioDTO(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}
	
	/**
	 * Constructor que inicializa los campos id, descripcion, unidad y valor.
	 * 
	 * @param id {@link #id}
	 * @param descripcion {@link #descripcion}
	 * @param unidad {@link #unidad}
	 * @param valor {@link #valor}
	 */
	public DatosBasicosValorDominioDTO(Integer id, String descripcion, String unidad, String valor) {
		this.id = id;
		this.descripcion = descripcion;
		this.unidad = unidad;
		this.valor = valor;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatosBasicosValorDominioDTO other = (DatosBasicosValorDominioDTO) obj;
		return Objects.equals(id, other.id);
	}
}
