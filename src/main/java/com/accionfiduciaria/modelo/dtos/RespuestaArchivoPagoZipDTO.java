/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author efarias
 *
 */
public class RespuestaArchivoPagoZipDTO {

	private String recursoB64;
	private String nombreArchivoZip;
	private String extension;
	private String tipoArchivo;

	public RespuestaArchivoPagoZipDTO() {

	}

	public RespuestaArchivoPagoZipDTO(String recursoB64, String nombreArchivoZip, String extension,
			String tipoArchivo) {
		this.recursoB64 = recursoB64;
		this.nombreArchivoZip = nombreArchivoZip;
		this.extension = extension;
		this.tipoArchivo = tipoArchivo;
	}

	/**
	 * @return the recursoB64
	 */
	public String getRecursoB64() {
		return recursoB64;
	}

	/**
	 * @param recursoB64 the recursoB64 to set
	 */
	public void setRecursoB64(String recursoB64) {
		this.recursoB64 = recursoB64;
	}

	/**
	 * @return the nombreArchivoZip
	 */
	public String getNombreArchivoZip() {
		return nombreArchivoZip;
	}

	/**
	 * @param nombreArchivoZip the nombreArchivoZip to set
	 */
	public void setNombreArchivoZip(String nombreArchivoZip) {
		this.nombreArchivoZip = nombreArchivoZip;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the tipoArchivo
	 */
	public String getTipoArchivo() {
		return tipoArchivo;
	}

	/**
	 * @param tipoArchivo the tipoArchivo to set
	 */
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

}
