package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;
import java.util.List;

public class RespuestaTitularesDTO {
	
	private String tipoTitularidad;
	private String nombreTitular;
	private String tipoDocumentoTitular;
	private String documentoTitular;
	private String numeroEncargoInd;
	private BigDecimal porcentajeParticipacion;
	private Integer numeroDerechosFiduciarios;
	private List<BeneficiarioDTO> beneficiarios;

	public String getTipoTitularidad() {
		return tipoTitularidad;
	}


	public void setTipoTitularidad(String tipoTitularidad) {
		this.tipoTitularidad = tipoTitularidad;
	}


	public String getNombreTitular() {
		return nombreTitular;
	}


	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}


	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}


	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}


	public String getDocumentoTitular() {
		return documentoTitular;
	}


	public void setDocumentoTitular(String documentoTitular) {
		this.documentoTitular = documentoTitular;
	}


	public String getNumeroEncargoInd() {
		return numeroEncargoInd;
	}


	public void setNumeroEncargoInd(String numeroEncargoInd) {
		this.numeroEncargoInd = numeroEncargoInd;
	}


	public BigDecimal getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}


	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}


	public Integer getNumeroDerechosFiduciarios() {
		return numeroDerechosFiduciarios;
	}


	public void setNumeroDerechosFiduciarios(Integer numeroDerechosFiduciarios) {
		this.numeroDerechosFiduciarios = numeroDerechosFiduciarios;
	}


	public List<BeneficiarioDTO> getBeneficiarios() {
		return beneficiarios;
	}


	public void setBeneficiarios(List<BeneficiarioDTO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
	
	
	
	
	

}
