/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonAlias;

/**
 * DTO usado para almacenar los datos de un vinculado.
 * 
 * @author José Santiago Polo Acosta - 16/12/2019 - jpolo@asesoftware.com
 *
 */
public class RespuestaBusquedadVinculadoDTO {

	/**
	 * La fecha en la que se realizo la vinculación.
	 */
	String fechaVinculacion;
	/**
	 * La ciudad en la que se realizo la vinculación.
	 */
	String ciudadVinculacion;
	/**
	 * La oficina en la que se realizo la vinculación.
	 */
	String oficinaVinculacion;
	/**
	 * El tipo de vinculación.
	 */
	String tipoVinculacion;
	/**
	 * La naturaleza juridica del vinculado.
	 */
	String naturalezaJuridicaVinculado;
	/**
	 * El nombre completo del vinculado o del representante legal si la naturaleza
	 * juridica del vinculado es diferente a natural.
	 */
	String nombreCompleto;
	/**
	 * El número de docmuento del vinculado
	 */
	String numeroDocumento;
	/**
	 * Contiene los datos del tipo de documento.
	 */
	TipoDocumentoDTO tipoDocumento;
	/**
	 * Indica a donde se enviara la correspondencia.
	 */
	@JsonAlias("enviocorrespondenciaTipo")
	String envioCorrespondenciaTipo;
	/**
	 * La razón social del vinculado si aplica.
	 */
	String razonSocial;
	/**
	 * El teléfono de contacto del vinculado.
	 */
	String telefono;
	/**
	 * El email de contacto del vinculado.
	 */
	String email;
	/**
	 * El país de residencia del vinculado.
	 */
	String paisResidencia;
	/**
	 * La ciudad de residencia del vinculado.
	 */
	String ciudadResidencia;
	/**
	 * Indica si el vinculado es titular o cotitular de algún negocio.
	 */
	boolean tieneNegocio;
	/**
	 * El porcentaje de participaciónque tiene el cliente sobre el negocio. 
	 */
	private BigDecimal participacion;
	
	private String envioCorrespondencia;

	/**
	 * @return the fechaVinculacion
	 */
	public String getFechaVinculacion() {
		return fechaVinculacion;
	}

	/**
	 * @param fechaVinculacion the fechaVinculacion to set
	 */
	public void setFechaVinculacion(String fechaVinculacion) {
		this.fechaVinculacion = fechaVinculacion;
	}

	/**
	 * @return the ciudadVinculacion
	 */
	public String getCiudadVinculacion() {
		return ciudadVinculacion != null ? ciudadVinculacion.trim() : ciudadVinculacion;
	}

	/**
	 * @param ciudadVinculacion the ciudadVinculacion to set
	 */
	public void setCiudadVinculacion(String ciudadVinculacion) {
		this.ciudadVinculacion = ciudadVinculacion;
	}

	/**
	 * @return the oficinaVinculacion
	 */
	public String getOficinaVinculacion() {
		return oficinaVinculacion != null ? oficinaVinculacion.trim() : oficinaVinculacion;
	}

	/**
	 * @param oficinaVinculacion the oficinaVinculacion to set
	 */
	public void setOficinaVinculacion(String oficinaVinculacion) {
		this.oficinaVinculacion = oficinaVinculacion;
	}

	/**
	 * @return the tipoVinculacion
	 */
	public String getTipoVinculacion() {
		return tipoVinculacion != null ? tipoVinculacion.trim() : tipoVinculacion;
	}

	/**
	 * @param tipoVinculacion the tipoVinculacion to set
	 */
	public void setTipoVinculacion(String tipoVinculacion) {
		this.tipoVinculacion = tipoVinculacion;
	}

	/**
	 * @return the naturalezaJuridicaVinculado
	 */
	public String getNaturalezaJuridicaVinculado() {
		return naturalezaJuridicaVinculado;
	}

	/**
	 * @param naturalezaJuridicaVinculado the naturalezaJuridicaVinculado to set
	 */
	public void setNaturalezaJuridicaVinculado(String naturalezaJuridicaVinculado) {
		this.naturalezaJuridicaVinculado = naturalezaJuridicaVinculado;
	}

	/**
	 * @return the nombreCompleto
	 */
	public String getNombreCompleto() {
		return nombreCompleto != null ? nombreCompleto.trim() : nombreCompleto;
	}

	/**
	 * @param nombreCompleto the nombreCompleto to set
	 */
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the tipoDocumento
	 */
	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(TipoDocumentoDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the envioCorrespondenciaTipo
	 */
	public String getEnvioCorrespondenciaTipo() {
		return envioCorrespondenciaTipo != null ? envioCorrespondenciaTipo.trim() : envioCorrespondenciaTipo;
	}

	/**
	 * @param envioCorrespondenciaTipo the envioCorrespondenciaTipo to set
	 */
	public void setEnvioCorrespondenciaTipo(String envioCorrespondenciaTipo) {
		this.envioCorrespondenciaTipo = envioCorrespondenciaTipo;
	}

	/**
	 * @return the razonSocial
	 */
	public String getRazonSocial() {
		return razonSocial != null ? razonSocial.trim() : razonSocial;
	}

	/**
	 * @param razonSocial the razonSocial to set
	 */
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono != null ? telefono.trim() : telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email != null ? email.trim() : email;
	}

	/**
	 * @return the paisResidencia
	 */
	public String getPaisResidencia() {
		return paisResidencia;
	}

	/**
	 * @param paisResidencia the paisResidencia to set
	 */
	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	/**
	 * @return the ciudadResidencia
	 */
	public String getCiudadResidencia() {
		return ciudadResidencia != null ? ciudadResidencia.trim() : ciudadResidencia;
	}

	/**
	 * @param ciudadResidencia the ciudadResidencia to set
	 */
	public void setCiudadResidencia(String ciudadResidencia) {
		this.ciudadResidencia = ciudadResidencia;
	}

	/**
	 * @return the tieneNegocio
	 */
	public boolean isTieneNegocio() {
		return tieneNegocio;
	}

	/**
	 * @param tieneNegocio the tieneNegocio to set
	 */
	public void setTieneNegocio(boolean tieneNegocio) {
		this.tieneNegocio = tieneNegocio;
	}

	public BigDecimal getParticipacion() {
		return participacion;
	}

	public void setParticipacion(BigDecimal participacion) {
		this.participacion = participacion;
	}

	public String getEnvioCorrespondencia() {
		return envioCorrespondencia;
	}

	public void setEnvioCorrespondencia(String envioCorrespondencia) {
		this.envioCorrespondencia = envioCorrespondencia;
	}
}
