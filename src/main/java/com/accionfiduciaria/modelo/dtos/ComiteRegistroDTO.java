package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class ComiteRegistroDTO {

	private String codigoSFC;
	private String fechaHoraReunion;
	private String direccionReunion;
	private String ordenDia;
	private String temasTratados;
	private String nombreArchivo;
	private List<DecisionComiteDTO> decisionTomada;
	private List<DecisionComiteDTO> tareaPendiente;
	private List<AsistenteDTO> asistentes;

	private String numeroRadicado;
	private String archivo;
	/**
	 * @return the codigoSFC
	 */
	public String getCodigoSFC() {
		return codigoSFC;
	}
	/**
	 * @param codigoSFC the codigoSFC to set
	 */
	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}
	/**
	 * @return the fechaHoraReunion
	 */
	public String getFechaHoraReunion() {
		return fechaHoraReunion;
	}
	/**
	 * @param fechaHoraReunion the fechaHoraReunion to set
	 */
	public void setFechaHoraReunion(String fechaHoraReunion) {
		this.fechaHoraReunion = fechaHoraReunion;
	}
	/**
	 * @return the direccionReunion
	 */
	public String getDireccionReunion() {
		return direccionReunion;
	}
	/**
	 * @param direccionReunion the direccionReunion to set
	 */
	public void setDireccionReunion(String direccionReunion) {
		this.direccionReunion = direccionReunion;
	}
	/**
	 * @return the ordenDia
	 */
	public String getOrdenDia() {
		return ordenDia;
	}
	/**
	 * @param ordenDia the ordenDia to set
	 */
	public void setOrdenDia(String ordenDia) {
		this.ordenDia = ordenDia;
	}
	/**
	 * @return the temasTratados
	 */
	public String getTemasTratados() {
		return temasTratados;
	}
	/**
	 * @param temasTratados the temasTratados to set
	 */
	public void setTemasTratados(String temasTratados) {
		this.temasTratados = temasTratados;
	}
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * @return the decisionTomada
	 */
	public List<DecisionComiteDTO> getDecisionTomada() {
		return decisionTomada;
	}
	/**
	 * @param decisionTomada the decisionTomada to set
	 */
	public void setDecisionTomada(List<DecisionComiteDTO> decisionTomada) {
		this.decisionTomada = decisionTomada;
	}
	/**
	 * @return the tareaPendiente
	 */
	public List<DecisionComiteDTO> getTareaPendiente() {
		return tareaPendiente;
	}
	/**
	 * @param tareaPendiente the tareaPendiente to set
	 */
	public void setTareaPendiente(List<DecisionComiteDTO> tareaPendiente) {
		this.tareaPendiente = tareaPendiente;
	}
	/**
	 * @return the asistentes
	 */
	public List<AsistenteDTO> getAsistentes() {
		return asistentes;
	}
	/**
	 * @param asistentes the asistentes to set
	 */
	public void setAsistentes(List<AsistenteDTO> asistentes) {
		this.asistentes = asistentes;
	}
	/**
	 * @return the numeroRadicado
	 */
	public String getNumeroRadicado() {
		return numeroRadicado;
	}
	/**
	 * @param numeroRadicado the numeroRadicado to set
	 */
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}
	/**
	 * @return the archivo
	 */
	public String getArchivo() {
		return archivo;
	}
	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	@Override
	public String toString() {
		return "ComiteRegistroDTO [codigoSFC=" + codigoSFC + ", fechaReunion=" + fechaHoraReunion +  ", direccionReunion=" + direccionReunion + ", ordenDia=" + ordenDia + ", temasTratados="
				+ temasTratados + ", nombreArchivo=" + nombreArchivo + ", decisionTomada=" + decisionTomada
				+ ", tareaPendiente=" + tareaPendiente + ", asistentes=" + asistentes + ", numeroRadicado="
				+ numeroRadicado + ", archivo=" + archivo + "]";
	}
	
	
}
