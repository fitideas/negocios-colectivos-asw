/**
 * 
 */
package com.accionfiduciaria.modelo.dtos;

/**
 * @author efarias
 *
 */
public class datosFinancierosBeneficiarioDTO {

	// COD_DOMINIO - COSMEDPAG
	private Integer tipoPago;
	// COD_DOMINIO - TIPCUE
	private Integer tipoCuenta;
	// COD_DOMINIO - BAN
	private Integer codigoBancario;
	private String numeroCuenta;
	private String fechaGiro;
	
	public datosFinancierosBeneficiarioDTO() {
		
	}
	
	public datosFinancierosBeneficiarioDTO(Integer tipoPago, Integer tipoCuenta, Integer codigoBancario,
			String numeroCuenta, String fechaGiro) {
		this.tipoPago = tipoPago;
		this.tipoCuenta = tipoCuenta;
		this.codigoBancario = codigoBancario;
		this.numeroCuenta = numeroCuenta;
		this.fechaGiro = fechaGiro;
	}

	/**
	 * @return the tipoPago
	 */
	public Integer getTipoPago() {
		return tipoPago;
	}

	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 * @return the tipoCuenta
	 */
	public Integer getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * @param tipoCuenta the tipoCuenta to set
	 */
	public void setTipoCuenta(Integer tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * @return the codigoBancario
	 */
	public Integer getCodigoBancario() {
		return codigoBancario;
	}

	/**
	 * @param codigoBancario the codigoBancario to set
	 */
	public void setCodigoBancario(Integer codigoBancario) {
		this.codigoBancario = codigoBancario;
	}

	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta the numeroCuenta to set
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return the fechaGiro
	 */
	public String getFechaGiro() {
		return fechaGiro;
	}

	/**
	 * @param fechaGiro the fechaGiro to set
	 */
	public void setFechaGiro(String fechaGiro) {
		this.fechaGiro = fechaGiro;
	}

}
