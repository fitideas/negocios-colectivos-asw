package com.accionfiduciaria.modelo.dtos;

import java.math.BigDecimal;

public class BeneficiarioTitularDTO {

	private String beneficiario;
	private String tipoDocumento;
	private String numeroDocumento;
	private BigDecimal porcentajeGiro;
	private BigDecimal porcentajeDistribucion;
	private TipoPagoDTO tipoPago;

	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the porcentajeGiro
	 */
	public BigDecimal getPorcentajeGiro() {
		return porcentajeGiro;
	}

	/**
	 * @param porcentajeGiro the porcentajeGiro to set
	 */
	public void setPorcentajeGiro(BigDecimal porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	/**
	 * @return the porcentajeDistribucion
	 */
	public BigDecimal getPorcentajeDistribucion() {
		return porcentajeDistribucion;
	}

	/**
	 * @param porcentajeDistribucion the porcentajeDistribucion to set
	 */
	public void setPorcentajeDistribucion(BigDecimal porcentajeDistribucion) {
		this.porcentajeDistribucion = porcentajeDistribucion;
	}

	/**
	 * @return the tipoPago
	 */
	public TipoPagoDTO getTipoPago() {
		return tipoPago;
	}

	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(TipoPagoDTO tipoPago) {
		this.tipoPago = tipoPago;
	}
}
