package com.accionfiduciaria.modelo.dtos;

import java.util.List;

public class ListaNegociosDTO {

	private int totalNegocios;
	private List<NegocioDTO> negocios;

	/**
	 * 
	 */
	public ListaNegociosDTO() {
	}

	/**
	 * @param totalNegocios
	 * @param negocios
	 */
	public ListaNegociosDTO(int totalNegocios, List<NegocioDTO> negocios) {
		super();
		this.totalNegocios = totalNegocios;
		this.negocios = negocios;
	}

	/**
	 * @return the totalNegocios
	 */
	public int getTotalNegocios() {
		return totalNegocios;
	}

	/**
	 * @param totalNegocios the totalNegocios to set
	 */
	public void setTotalNegocios(int totalNegocios) {
		this.totalNegocios = totalNegocios;
	}

	/**
	 * @return the negocios
	 */
	public List<NegocioDTO> getNegocios() {
		return negocios;
	}

	/**
	 * @param negocios the negocios to set
	 */
	public void setNegocios(List<NegocioDTO> negocios) {
		this.negocios = negocios;
	}
}
