package com.accionfiduciaria.modelo.dtos;

public class NovedadesJuridicasDTO {

	private String codigoEvento;
	private String tipoEvento;
	private String estado;
	private String fechaRegistro;

	/**
	 * 
	 */
	public NovedadesJuridicasDTO() {
	}

	/**
	 * @param codigoEvento
	 * @param tipoEvento
	 * @param estado
	 * @param fechaRegistro
	 */
	public NovedadesJuridicasDTO(String codigoEvento, String tipoEvento, String estado, String fechaRegistro) {
		this.codigoEvento = codigoEvento;
		this.tipoEvento = tipoEvento;
		this.estado = estado;
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the codigoEvento
	 */
	public String getCodigoEvento() {
		return codigoEvento;
	}

	/**
	 * @param codigoEvento the codigoEvento to set
	 */
	public void setCodigoEvento(String codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	/**
	 * @return the tipoEvento
	 */
	public String getTipoEvento() {
		return tipoEvento;
	}

	/**
	 * @param tipoEvento the tipoEvento to set
	 */
	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fechaRegistro
	 */
	public String getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro the fechaRegistro to set
	 */
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
}
