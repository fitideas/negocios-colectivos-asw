package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * DTo utiizado para recibir los datos de dsitribución desde el front.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class DatosDistribucionRecepcionDTO extends DatosDistribucionDTO {
	
	private String nombre;
	
	/**
	 * Contiene el istado de los id de las retenciones por tipo de naturaleza.
	 */
	private List<Integer> retencionesTipoNaturaleza;
	
	private List<RetencionesTipoNegocioEliminadasDTO> retencionesTipoNegocioEliminadas;
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Integer> getRetencionesTipoNaturaleza() {
		return retencionesTipoNaturaleza;
	}

	public void setRetencionesTipoNaturaleza(List<Integer> retencionesTipoNaturaleza) {
		this.retencionesTipoNaturaleza = retencionesTipoNaturaleza;
	}

	public List<RetencionesTipoNegocioEliminadasDTO> getRetencionesTipoNegocioEliminadas() {
		return retencionesTipoNegocioEliminadas;
	}

	public void setRetencionesTipoNegocioEliminadas(
			List<RetencionesTipoNegocioEliminadasDTO> retencionesTipoNegocioEliminadas) {
		this.retencionesTipoNegocioEliminadas = retencionesTipoNegocioEliminadas;
	}
}
