package com.accionfiduciaria.modelo.dtos;

import java.util.List;

/**
 * DTO usado para almacenar los datos de una opción de menú con sus funcionalidades.
 * 
 *  Estas opciones son aquellos registros de la tabla {@code AUT_PERMISO} cuyo campo {@code AUT_PERMISO_IDPERMISO} es nulo.
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class OpcionesMenuDTO {
	/**
	 * El id de la opción.
	 */
	private Integer idOpcion;
	/**
	 * El nombre de la opcioó.
	 */
	private String opcion;
	/**
	 * El listado de funcionalidades de la opción de menu.
	 */
	private List<PermisoDTO> funcionalidades;
	
	/**
	 * Constructor por defecto.
	 */
	public OpcionesMenuDTO() {
	}

	/**
	 * Constructor que inicializa los campos idOpcion y opcion.
	 * 
	 * @param idOpcion {@link #idOpcion}
	 * @param opcion {@link #opcion}
	 */
	public OpcionesMenuDTO(Integer idOpcion, String opcion) {
		this.idOpcion = idOpcion;
		this.opcion = opcion;
	}

	/**
	 * Constructor que inicializa los campos idOpcion, opcion y funcionalidades.
	 * 
	 * @param idOpcion {@link #idOpcion}
	 * @param opcion {@link #opcion}
	 * @param funcionalidades {@link #funcionalidades}
	 */
	public OpcionesMenuDTO(Integer idOpcion, String opcion, List<PermisoDTO> funcionalidades) {
		this.idOpcion = idOpcion;
		this.opcion = opcion;
		this.funcionalidades = funcionalidades;
	}

	/**
	 * @return the idOpcion
	 */
	public Integer getIdOpcion() {
		return idOpcion;
	}

	/**
	 * @param idOpcion the idOpcion to set
	 */
	public void setIdOpcion(Integer idOpcion) {
		this.idOpcion = idOpcion;
	}

	/**
	 * @return the opcion
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * @param opcion the opcion to set
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * @return the funcionalidades
	 */
	public List<PermisoDTO> getFuncionalidades() {
		return funcionalidades;
	}

	/**
	 * @param funcionalidades the funcionalidades to set
	 */
	public void setFuncionalidades(List<PermisoDTO> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}
}
