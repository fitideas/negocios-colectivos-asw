package com.accionfiduciaria.modelo.dtos;

public class RespuestaDatosCertificadosDTO {
	//el nombre del titular
	private String nombreCompleto;
	private String tipoDocumento;
	private String numeroDocumento;
	
	private String nombreNegocio;
	private String codSFC;
	
	private String participacion;
	private String numeroDerechos;

	private String baseRetencion;
	private String totalRetenido;
	private String rentaExcenta;
	
	private String anoGrabable;
	private String ciudadAduana;
	private String fechaGeneracion;
	
	private String codigoInterno;
	private String fechaConstitucion;
	private String matriculaInmobiliaria;
	private String licenciaConstruccion;
	private String ciudadUbicacion;
	private String rolFiduciaria;
	
	
	private String diasMesLetras;
	private String nombreMesLetras;
	private String anoEnLetras;
	
	private String responsable;
	private String descripcionTipos;
	
	
	public RespuestaDatosCertificadosDTO() {
		super();
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNombreNegocio() {
		return nombreNegocio;
	}
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	public String getCodSFC() {
		return codSFC;
	}
	public void setCodSFC(String codSFC) {
		this.codSFC = codSFC;
	}
	public String getParticipacion() {
		return participacion;
	}
	public void setParticipacion(String participacion) {
		this.participacion = participacion;
	}
	public String getNumeroDerechos() {
		return numeroDerechos;
	}
	public void setNumeroDerechos(String numeroDerechos) {
		this.numeroDerechos = numeroDerechos;
	}
	public String getBaseRetencion() {
		return baseRetencion;
	}
	public void setBaseRetencion(String baseRetencion) {
		this.baseRetencion = baseRetencion;
	}
	public String getTotalRetenido() {
		return totalRetenido;
	}
	public void setTotalRetenido(String totalRetenido) {
		this.totalRetenido = totalRetenido;
	}
	public String getRentaExcenta() {
		return rentaExcenta;
	}
	public void setRentaExcenta(String rentaExcenta) {
		this.rentaExcenta = rentaExcenta;
	}
	public String getAnoGrabable() {
		return anoGrabable;
	}
	public void setAnoGrabable(String anoGrabable) {
		this.anoGrabable = anoGrabable;
	}
	public String getCiudadAduana() {
		return ciudadAduana;
	}
	public void setCiudadAduana(String ciudadAduana) {
		this.ciudadAduana = ciudadAduana;
	}
	public String getFechaGeneracion() {
		return fechaGeneracion;
	}
	public void setFechaGeneracion(String fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	public String getCodigoInterno() {
		return codigoInterno;
	}
	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
	public String getFechaConstitucion() {
		return fechaConstitucion;
	}
	public void setFechaConstitucion(String fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}
	public String getMatriculaInmobiliaria() {
		return matriculaInmobiliaria;
	}
	public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
		this.matriculaInmobiliaria = matriculaInmobiliaria;
	}
	public String getLicenciaConstruccion() {
		return licenciaConstruccion;
	}
	public void setLicenciaConstruccion(String licenciaConstruccion) {
		this.licenciaConstruccion = licenciaConstruccion;
	}
	public String getCiudadUbicacion() {
		return ciudadUbicacion;
	}
	public void setCiudadUbicacion(String ciudadUbicacion) {
		this.ciudadUbicacion = ciudadUbicacion;
	}
	public String getRolFiduciaria() {
		return rolFiduciaria;
	}
	public void setRolFiduciaria(String rolFiduciaria) {
		this.rolFiduciaria = rolFiduciaria;
	}
	public String getDiasMesLetras() {
		return diasMesLetras;
	}
	public void setDiasMesLetras(String diasMesLetras) {
		this.diasMesLetras = diasMesLetras;
	}
	public String getNombreMesLetras() {
		return nombreMesLetras;
	}
	public void setNombreMesLetras(String nombreMesLetras) {
		this.nombreMesLetras = nombreMesLetras;
	}
	public String getAnoEnLetras() {
		return anoEnLetras;
	}
	public void setAnoEnLetras(String anoEnLetras) {
		this.anoEnLetras = anoEnLetras;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getDescripcionTipos() {
		return descripcionTipos;
	}
	public void setDescripcionTipos(String descripcionTipos) {
		this.descripcionTipos = descripcionTipos;
	}
	
	
}
