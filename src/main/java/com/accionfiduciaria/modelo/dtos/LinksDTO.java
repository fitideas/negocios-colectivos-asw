package com.accionfiduciaria.modelo.dtos;

/**
 * DTO usado para recibir los datos del campo _links en la respuesta paginada a la consulta de datos de vinculados en AccionBack
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
public class LinksDTO {
	
	/**
	 * Define el path del API consumida
	 */
	private String base;
	/**
	 * Es el path relativo que apunta a la siguiente lista de objetos EN CASO DE EXISTIR, 
	 * este atributo esta en nulo quiere decir que se ha llegado al último grupo de objetos.
	 */
	private String next;
	/**
	 * Es el path relativo que apunta a la anterior lista de objetos EN CASO DE EXISTIR, 
	 * si este atributo esta en nulo quiere decir que se está en el grupo de objetos.
	 */
	private String prev;
	/**
	 * Determina el recurso actual solicitado
	 */
	private String self;
	
	/**
	 * Constructor por defecto.
	 */
	public LinksDTO() {
	}

	/**
	 * Construtor que inicializa los campos: base, next, prev, self.
	 * 
	 * @param base {@link #base}
	 * @param next {@link #next}
	 * @param prev {@link #prev}
	 * @param self {@link #self}
	 */
	public LinksDTO(String base, String next, String prev, String self) {
		this.base = base;
		this.next = next;
		this.prev = prev;
		this.self = self;
	}

	/**
	 * @return the base
	 */
	public String getBase() {
		return base;
	}

	/**
	 * @param base the base to set
	 */
	public void setBase(String base) {
		this.base = base;
	}

	/**
	 * @return the next
	 */
	public String getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(String next) {
		this.next = next;
	}

	/**
	 * @return the prev
	 */
	public String getPrev() {
		return prev;
	}

	/**
	 * @param prev the prev to set
	 */
	public void setPrev(String prev) {
		this.prev = prev;
	}

	/**
	 * @return the self
	 */
	public String getSelf() {
		return self;
	}

	/**
	 * @param self the self to set
	 */
	public void setSelf(String self) {
		this.self = self;
	}	
}
