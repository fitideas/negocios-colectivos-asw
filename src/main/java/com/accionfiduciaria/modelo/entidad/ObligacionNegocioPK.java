package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class ObligacionNegocioPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
	@Column(name = "TIPO_PAGO")
	private Integer tipoPago;
	@Basic(optional = false)
	@Column(name = "TIPO_NEGOCIO")
	private Integer tipoNegocio;
	@Basic(optional = false)
	@Column(name = "COD_SFC")
	private String codSfc;
	@Column(name = "NOMBRE_GASTO")
    private String nombreGasto;

	public ObligacionNegocioPK() {
	}	

	/**
	 * @param tipoPago
	 * @param tipoNegocio
	 * @param codSfc
	 * @param nombreGasto
	 */
	public ObligacionNegocioPK(Integer tipoPago, Integer tipoNegocio, String codSfc, String nombreGasto) {
		super();
		this.tipoPago = tipoPago;
		this.tipoNegocio = tipoNegocio;
		this.codSfc = codSfc;
		this.nombreGasto = nombreGasto;
	}



	public Integer getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Integer getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(Integer tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public String getNombreGasto() {
		return nombreGasto;
	}

	public void setNombreGasto(String nombreGasto) {
		this.nombreGasto = nombreGasto;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, nombreGasto, tipoNegocio, tipoPago);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObligacionNegocioPK other = (ObligacionNegocioPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(nombreGasto, other.nombreGasto)
				&& Objects.equals(tipoNegocio, other.tipoNegocio) && Objects.equals(tipoPago, other.tipoPago);
	}

	@Override
	public String toString() {
		return "ObligacionNegocioPK [tipoPago=" + tipoPago + ", tipoNegocio=" + tipoNegocio + ", codSfc=" + codSfc
				+ ", nombreGasto=" + nombreGasto + "]";
	}
}
