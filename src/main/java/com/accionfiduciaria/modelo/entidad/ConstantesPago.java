package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_CONSTANTES_PAGO")
@XmlRootElement
public class ConstantesPago extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "COD_SFC")
	private String codSfc;
	@Size(max = 6)
	@Column(name = "TIPO_FIDEICOMISO")
	private String tipoFideicomiso;
	@Size(max = 6)
	@Column(name = "TIPO_SUBTIPO_FIDEICOMISO")
	private String tipoSubtipoFideicomiso;
	@Size(max = 6)
	@Column(name = "MOVIMIENTO")
	private String movimiento;
	@Size(max = 6)
	@Column(name = "TIPO_PROCESO")
	private String tipoProceso;
	@Size(max = 6)
	@Column(name = "TIPO_MOV_NEGOCIO")
	private String tipoMovNegocio;
	@Size(max = 6)
	@Column(name = "CODIGO_PAIS")
	private String codigoPais;
	@Size(max = 6)
	@Column(name = "CODIGO_DEPARTAMENTO")
	private String codigoDepartamento;
	@Size(max = 6)
	@Column(name = "CODIGO_CIUDAD")
	private String codigoCiudad;
	@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private Negocio negocio;

	public ConstantesPago() {
	}

	public ConstantesPago(String codSfc) {
		this.codSfc = codSfc;
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public String getTipoFideicomiso() {
		return tipoFideicomiso;
	}

	public void setTipoFideicomiso(String tipoFideicomiso) {
		this.tipoFideicomiso = tipoFideicomiso;
	}

	public String getTipoSubtipoFideicomiso() {
		return tipoSubtipoFideicomiso;
	}

	public void setTipoSubtipoFideicomiso(String tipoSubtipoFideicomiso) {
		this.tipoSubtipoFideicomiso = tipoSubtipoFideicomiso;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public String getTipoMovNegocio() {
		return tipoMovNegocio;
	}

	public void setTipoMovNegocio(String tipoMovNegocio) {
		this.tipoMovNegocio = tipoMovNegocio;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	public String getCodigoDepartamento() {
		return codigoDepartamento;
	}

	public void setCodigoDepartamento(String codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}

	public String getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConstantesPago other = (ConstantesPago) obj;
		return Objects.equals(codSfc, other.codSfc);
	}

	@Override
	public String toString() {
		return "ConstantesPago [codSfc=" + codSfc + ", tipoFideicomiso=" + tipoFideicomiso + ", tipoSubtipoFideicomiso="
				+ tipoSubtipoFideicomiso + ", movimiento=" + movimiento + ", tipoProceso=" + tipoProceso
				+ ", tipoMovNegocio=" + tipoMovNegocio + ", codigoPais=" + codigoPais + ", codigoDepartamento=" 
				+ codigoDepartamento + ", codigoCiudad=" + codigoCiudad + ", negocio=" + negocio + "]";
	}
}
