package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.data.annotation.Transient;

/**
 *
 * @author jpolo
 */
@Entity

@Table(name = "NEG_DOMINIOS")
@XmlRootElement
public class Dominio extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Transient
    @Basic(optional = false)
    @Column(name = "COD_DOMINIO")
    private String codDominio;
    @Transient
    @Column(name = "NOMBRE_DOMINIO")
    private String nombreDominio;
    @Transient
    @Basic(optional = false)
    @Column(name = "VISIBLE")
    private Boolean visible;
    @Transient
    @Column(name = "PERMITE_ELIMINAR")
    private Boolean permiteEliminar;
    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dominio", fetch = FetchType.LAZY)
    private List<ValorDominioNegocio> valoresDominio;

    public Dominio() {
    }

    public Dominio(String codDominio) {
        this.codDominio = codDominio;
    }

    public Dominio(String codDominio, String nombreDominio) {
    	this.codDominio = codDominio;
    	this.nombreDominio = nombreDominio;
	}

	public Dominio(String codDominio, String nombreDominio, Boolean visible, Boolean permiteEliminar) {
		this.codDominio = codDominio;
    	this.nombreDominio = nombreDominio;
    	this.visible = visible;
    	this.permiteEliminar = permiteEliminar;
	}

	public String getCodDominio() {
        return codDominio;
    }

    public void setCodDominio(String codDominio) {
        this.codDominio = codDominio;
    }

    public String getNombreDominio() {
        return nombreDominio;
    }

    public void setNombreDominio(String nombreDominio) {
        this.nombreDominio = nombreDominio;
    }
    
    /**
	 * @return the visible
	 */
	public Boolean getVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	@XmlTransient
    public List<ValorDominioNegocio> getValoresDominio() {
        return valoresDominio;
    }

    /**
     * 
     * @param valoresDominio
     */
	public void setValoresDominio(List<ValorDominioNegocio> valoresDominio) {
        this.valoresDominio = valoresDominio;
    }
    
    /**
	 * @return the permiteEliminar
	 */
	public Boolean getPermiteEliminar() {
		return permiteEliminar;
	}

	/**
	 * @param permiteEliminar the permiteEliminar to set
	 */
	public void setPermiteEliminar(Boolean permiteEliminar) {
		this.permiteEliminar = permiteEliminar;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codDominio);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dominio other = (Dominio) obj;
		return Objects.equals(codDominio, other.codDominio);
	}

	@Override
    public String toString() {
        return "modelos.NegDominios[ codDominio=" + codDominio + " ]";
    }   
}