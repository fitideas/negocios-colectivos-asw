package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "NEG_MIEMBROS_COMITE")
@XmlRootElement
public class MiembroComite implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	protected  MiembroComitePK miembroComitePK;
	
	@Column (name = "ESTADO")
	private String estado;
	
	@Column (name ="ROL" )
	private int rol;
	
	@Column (name ="FECHA_VIGENCIA_ROL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaVigenciaRol;
	
	@Column(name ="RADICADO")
	private String radicado;
	
    @JoinColumns({
        @JoinColumn(name = "NUMERO_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Persona persona;
	

	public MiembroComite(MiembroComitePK miembroComitePK) {
		this.miembroComitePK = miembroComitePK;
	}
	
	
	public MiembroComite(String codSfc, String tipoDocumento, String numeroDocumento) {
		this.miembroComitePK = new MiembroComitePK(codSfc, tipoDocumento, numeroDocumento);
	}
	
	
	

	public MiembroComite() {
	}

	

	/**
	 * @return the miembroComitePK
	 */
	public MiembroComitePK getMiembroComitePK() {
		return miembroComitePK;
	}


	/**
	 * @param miembroComitePK the miembroComitePK to set
	 */
	public void setMiembroComitePK(MiembroComitePK miembroComitePK) {
		this.miembroComitePK = miembroComitePK;
	}


	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getRol() {
		return rol;
	}

	public void setRol(int rol) {
		this.rol = rol;
	}

	public Date getFechaVigenciaRol() {
		return fechaVigenciaRol;
	}

	public void setFechaVigenciaRol(Date fechaVigenciaRol) {
		this.fechaVigenciaRol = fechaVigenciaRol;
	}

	public String getRadicado() {
		return radicado;
	}

	public void setRadicado(String radicado) {
		this.radicado = radicado;
	}


	/**
	 * @return the persona
	 */
	public Persona getPersona() {
		return persona;
	}


	/**
	 * @param persona the persona to set
	 */
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((miembroComitePK == null) ? 0 : miembroComitePK.hashCode());
		return result;
	}


	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MiembroComite other = (MiembroComite) obj;
		if (miembroComitePK == null) {
			if (other.miembroComitePK != null)
				return false;
		} else if (!miembroComitePK.equals(other.miembroComitePK))
			return false;
		return true;
	}
	
	
	
	

}
