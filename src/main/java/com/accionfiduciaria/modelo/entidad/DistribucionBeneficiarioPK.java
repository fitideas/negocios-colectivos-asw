package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DistribucionBeneficiarioPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
	@Column(name = "NEG_BEN_COD_SFC")
	private String codigoSFC;
	@Basic(optional = false)
	@Column(name = "TIPO_DOC_TITULAR")
	private String tipoDocumentoTitular;
	@Basic(optional = false)
	@Column(name = "NUM_DOC_TITULAR")
	private String numeroDocumentoTitular;
	@Basic(optional = false)
	@Column(name = "NUM_DOC_BENEFICIARIO")
	private String numeroDocumentoBeneficiario;
	@Basic(optional = false)
	@Column(name = "TIPO_DOC_BENEFICIARIO")
	private String tipoDocumentoBeneficiario;
	@Basic(optional = false)
	@Column(name = "BANCO")
	private Integer banco;
	@Basic(optional = false)
	@Column(name = "CUENTA")
	private String cuenta;
	@Basic(optional = false)
	@Column(name = "TIPO_CUENTA")
	private Integer tipoCuenta;

	public DistribucionBeneficiarioPK() {
	}

	/**
	 * @param titularPK
	 * @param medioPagoPersonaPK
	 */
	public DistribucionBeneficiarioPK(TitularPK titularPK, MedioPagoPersonaPK medioPagoPersonaPK) {
		this.codigoSFC = titularPK.getCodSfc();
		this.tipoDocumentoTitular = titularPK.getTipoDocumento();
		this.numeroDocumentoTitular = titularPK.getNumeroDocumento();
		this.numeroDocumentoBeneficiario = medioPagoPersonaPK.getNumeroDocumento();
		this.tipoDocumentoBeneficiario = medioPagoPersonaPK.getTipoDocumento();
		this.banco = medioPagoPersonaPK.getBanco();
		this.cuenta = medioPagoPersonaPK.getCuenta();
		this.tipoCuenta = medioPagoPersonaPK.getTipoCuenta();
	}
	
	/**
	 * @param beneficiarioPK
	 */
	public DistribucionBeneficiarioPK(BeneficiarioPK beneficiarioPK) {
		this.codigoSFC = beneficiarioPK.getCodSfc();
		this.tipoDocumentoTitular = beneficiarioPK.getTipoDocumentoTitular();
		this.numeroDocumentoTitular = beneficiarioPK.getNumeroDocumentoTitular();
		this.numeroDocumentoBeneficiario = beneficiarioPK.getNumeroDocumentoBeneficiario();
		this.tipoDocumentoBeneficiario = beneficiarioPK.getTipoDocumentoBeneficiario();
		this.banco = beneficiarioPK.getBanco();
		this.cuenta = beneficiarioPK.getCuenta();
		this.tipoCuenta = beneficiarioPK.getTipoCuenta();
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigoSFC, tipoDocumentoTitular, numeroDocumentoTitular, numeroDocumentoBeneficiario,
				tipoDocumentoBeneficiario, banco, cuenta, tipoCuenta);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistribucionBeneficiarioPK other = (DistribucionBeneficiarioPK) obj;
		return Objects.equals(codigoSFC, other.codigoSFC)
				&& Objects.equals(tipoDocumentoTitular, other.tipoDocumentoTitular)
				&& Objects.equals(numeroDocumentoTitular, other.numeroDocumentoTitular)
				&& Objects.equals(numeroDocumentoBeneficiario, other.numeroDocumentoBeneficiario)
				&& Objects.equals(tipoDocumentoBeneficiario, other.tipoDocumentoBeneficiario)
				&& Objects.equals(banco, other.banco) && Objects.equals(cuenta, other.cuenta)
				&& Objects.equals(tipoCuenta, other.tipoCuenta);
	}

	@Override
	public String toString() {
		return "DistribucionBeneficiarioPK [codigoSFC=" + codigoSFC + ", tipoDocumentoTitular=" + tipoDocumentoTitular
				+ ", numeroDocumentoTitular=" + numeroDocumentoTitular + ", numeroDocumentoBeneficiario="
				+ numeroDocumentoBeneficiario + ", tipoDocumentoBeneficiario=" + tipoDocumentoBeneficiario + ", banco="
				+ banco + ", cuenta=" + cuenta + ", tipoCuenta=" + tipoCuenta + "]";
	}

	public String getCodigoSFC() {
		return codigoSFC;
	}

	public void setCodigoSFC(String codigoSFC) {
		this.codigoSFC = codigoSFC;
	}

	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	public String getNumeroDocumentoTitular() {
		return numeroDocumentoTitular;
	}

	public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
		this.numeroDocumentoTitular = numeroDocumentoTitular;
	}

	public String getNumeroDocumentoBeneficiario() {
		return numeroDocumentoBeneficiario;
	}

	public void setNumeroDocumentoBeneficiario(String numeroDocumentoBeneficiario) {
		this.numeroDocumentoBeneficiario = numeroDocumentoBeneficiario;
	}

	public String getTipoDocumentoBeneficiario() {
		return tipoDocumentoBeneficiario;
	}

	public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
		this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
	}

	public Integer getBanco() {
		return banco;
	}

	public void setBanco(Integer banco) {
		this.banco = banco;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public Integer getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(Integer tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

}
