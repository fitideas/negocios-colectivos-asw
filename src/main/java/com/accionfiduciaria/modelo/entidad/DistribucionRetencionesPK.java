/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author efarias
 *
 */
@Embeddable
public class DistribucionRetencionesPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "ID_RETENCION")
	private Integer idRetencion;
	@Basic(optional = false)
	@Column(name = "NEG_RET_TIPO_NEGOCIO")
	private Integer retTipoNegocio;
	@Basic(optional = false)
	@Column(name = "NEG_RET_COD_SFC")
	private String retCodSfc;
	@Basic(optional = false)
	@Column(name = "NEG_DIST_COD_SFC")
	private String distCodSfc;
	@Basic(optional = false)
	@Column(name = "NEG_DIST_TIPO_NEGOCIO")
	private Integer distTipoNegocio;
	@Basic(optional = false)
	@Column(name = "PERIODO")
	private String periodo;
	@Basic(optional = false)
	@Column(name = "FECHA_HORA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHora;

	public DistribucionRetencionesPK() {

	}

	public DistribucionRetencionesPK(RetencionTipoNegocioPK retencionTipoNegocioPK, DistribucionPK distribucionPK) {
		this.idRetencion = retencionTipoNegocioPK.getIdRetencion();
		this.retTipoNegocio = retencionTipoNegocioPK.getTipoNegocio();
		this.retCodSfc = retencionTipoNegocioPK.getCodSfc();
		this.distCodSfc = distribucionPK.getCodSfc();
		this.distTipoNegocio = distribucionPK.getTipoNegocio();
		this.periodo = distribucionPK.getPeriodo();
		this.fechaHora = distribucionPK.getFechaHora();
	}

	public DistribucionRetencionesPK(Integer idRetencion, Integer retTipoNegocio, String retCodSfc, String distCodSfc,
			Integer distTipoNegocio, String periodo, Date fechaHora) {
		this.idRetencion = idRetencion;
		this.retTipoNegocio = retTipoNegocio;
		this.retCodSfc = retCodSfc;
		this.distCodSfc = distCodSfc;
		this.distTipoNegocio = distTipoNegocio;
		this.periodo = periodo;
		this.fechaHora = fechaHora;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idRetencion, retTipoNegocio, retCodSfc, distCodSfc, distTipoNegocio, periodo, fechaHora);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistribucionRetencionesPK other = (DistribucionRetencionesPK) obj;
		return Objects.equals(idRetencion, other.idRetencion) && Objects.equals(retTipoNegocio, other.retTipoNegocio)
				&& Objects.equals(retCodSfc, other.retCodSfc) && Objects.equals(distCodSfc, other.distCodSfc)
				&& Objects.equals(distTipoNegocio, other.distTipoNegocio) && Objects.equals(periodo, other.periodo)
				&& Objects.equals(fechaHora, other.fechaHora);
	}

	@Override
	public String toString() {
		return "DistribucionRetencionesPK [idRetencion=" + idRetencion + ", retTipoNegocio=" + retTipoNegocio
				+ ", retCodSfc=" + retCodSfc + ", distCodSfc=" + distCodSfc + ", distTipoNegocio=" + distTipoNegocio
				+ ", periodo=" + periodo + ", fechaHora=" + fechaHora + "]";
	}

	public Integer getIdRetencion() {
		return idRetencion;
	}

	public void setIdRetencion(Integer idRetencion) {
		this.idRetencion = idRetencion;
	}

	public Integer getRetTipoNegocio() {
		return retTipoNegocio;
	}

	public void setRetTipoNegocio(Integer retTipoNegocio) {
		this.retTipoNegocio = retTipoNegocio;
	}

	public String getRetCodSfc() {
		return retCodSfc;
	}

	public void setRetCodSfc(String retCodSfc) {
		this.retCodSfc = retCodSfc;
	}

	public String getDistCodSfc() {
		return distCodSfc;
	}

	public void setDistCodSfc(String distCodSfc) {
		this.distCodSfc = distCodSfc;
	}

	public Integer getDistTipoNegocio() {
		return distTipoNegocio;
	}

	public void setDistTipoNegocio(Integer distTipoNegocio) {
		this.distTipoNegocio = distTipoNegocio;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

}
