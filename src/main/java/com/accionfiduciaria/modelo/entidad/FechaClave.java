package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_FECHA_CLAVE")
@XmlRootElement
public class FechaClave implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected FechaClavePK fechaClavePK;
	@Basic(optional = false)
	@Column(name = "ACTIVO")
	private Boolean activo;
	@Basic(optional = false)
	@Column(name = "FECHA_MES")
	private Integer fechaMes;
	@Basic(optional = false)
	@Column(name = "FECHA_DIA")
	private Integer fechaDia;
	@Column(name = "PERIODICIDAD")
	private Integer periodicidad;
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "TIPO")
	private Integer tipo;
	@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
	@ManyToOne(optional = false)
	private Negocio negocio;
	@JoinColumn(name = "TIPO", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioFechaClave;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "fechaClave")
	private List<HistorialNotificaciones> historialNotificaciones;

	public FechaClave() {
	}

	public FechaClave(FechaClavePK negFechaClavePK) {
		this.fechaClavePK = negFechaClavePK;
	}

	public FechaClave(FechaClavePK negFechaClavePK, Boolean activo, Integer fechaMes, Integer fechaDia) {
		this.fechaClavePK = negFechaClavePK;
		this.activo = activo;
		this.fechaMes = fechaMes;
		this.fechaDia = fechaDia;
	}
	
	public FechaClave(String codSfc, Date fecha) {
		this.fechaClavePK = new FechaClavePK(codSfc, fecha);
	}

	public FechaClavePK getFechaClavePK() {
		return fechaClavePK;
	}

	public void setFechaClavePK(FechaClavePK fechaClavePK) {
		this.fechaClavePK = fechaClavePK;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getFechaMes() {
		return fechaMes;
	}

	public void setFechaMes(Integer fechaMes) {
		this.fechaMes = fechaMes;
	}

	public Integer getFechaDia() {
		return fechaDia;
	}

	public void setFechaDia(Integer fechaDia) {
		this.fechaDia = fechaDia;
	}

	public Integer getPeriodicidad() {
		return periodicidad;
	}

	public void setPeriodicidad(Integer periodicidad) {
		this.periodicidad = periodicidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public ValorDominioNegocio getDominioFechaClave() {
		return dominioFechaClave;
	}

	public void setDominioFechaClave(ValorDominioNegocio dominioFechaClave) {
		this.dominioFechaClave = dominioFechaClave;
	}

	public List<HistorialNotificaciones> getHistorialNotificaciones() {
		return historialNotificaciones;
	}

	public void setHistorialNotificaciones(List<HistorialNotificaciones> historialNotificaciones) {
		this.historialNotificaciones = historialNotificaciones;
	}

	@Override
	public int hashCode() {
		return Objects.hash(fechaClavePK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FechaClave other = (FechaClave) obj;
		return Objects.equals(fechaClavePK, other.fechaClavePK);
	}

	@Override
	public String toString() {
		return "FechaClave [fechaClavePK=" + fechaClavePK + "]";
	}
}
