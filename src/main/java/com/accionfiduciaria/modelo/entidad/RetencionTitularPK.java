package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class RetencionTitularPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3416214167261208647L;
	
	@Basic(optional = false)
    @Column(name = "ID_RETENCION")
    private Integer idRetencion;
    @Basic(optional = false)
    @Column(name = "COD_SFC")
    private String codSfc;
    @Basic(optional = false)
    @Column(name = "NUMERO_DOCUMENTO")
    private String numeroDocumento;
    @Basic(optional = false)
    @Column(name = "TIPO_DOCUMENTO")
    private String tipoDocumento;
    @Basic(optional = false)
    @Column(name = "TIPO_NATURALEZA_NEGOCIO")
    private Integer tipoNaturalezaNegocio;

    public RetencionTitularPK() {
    }

    public RetencionTitularPK(Integer idRetencion, String codSfc, String numeroDocumento, String tipoDocumento, Integer tipoNaturalezaNegocio) {
        this.idRetencion = idRetencion;
        this.codSfc = codSfc;
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumento = tipoDocumento;
        this.tipoNaturalezaNegocio = tipoNaturalezaNegocio;
    }

    public Integer getIdRetencion() {
        return idRetencion;
    }

    public void setIdRetencion(Integer idRetencion) {
        this.idRetencion = idRetencion;
    }

    public String getCodSfc() {
        return codSfc;
    }

    public void setCodSfc(String codSfc) {
        this.codSfc = codSfc;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getTipoNaturalezaNegocio() {
		return tipoNaturalezaNegocio;
	}

	public void setTipoNaturalezaNegocio(Integer tipoNaturalezaNegocio) {
		this.tipoNaturalezaNegocio = tipoNaturalezaNegocio;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, idRetencion, numeroDocumento, tipoDocumento, tipoNaturalezaNegocio);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetencionTitularPK other = (RetencionTitularPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(idRetencion, other.idRetencion)
				&& Objects.equals(numeroDocumento, other.numeroDocumento)
				&& Objects.equals(tipoDocumento, other.tipoDocumento)
				&& Objects.equals(tipoNaturalezaNegocio, other.tipoNaturalezaNegocio);
	}

	@Override
	public String toString() {
		return "RetencionTitularPK [idRetencion=" + idRetencion + ", codSfc=" + codSfc + ", numeroDocumento="
				+ numeroDocumento + ", tipoDocumento=" + tipoDocumento + ", tipoNaturalezaNegocio="
				+ tipoNaturalezaNegocio + "]";
	}    
}
