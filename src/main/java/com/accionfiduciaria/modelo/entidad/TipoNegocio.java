package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Entity
@Table(name = "NEG_TIPO_NEGOCIO")
@XmlRootElement
public class TipoNegocio extends Auditoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3853880193866639960L;
	
	@EmbeddedId
    protected TipoNegocioPK tipoNegocioPK;
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private Boolean activo;
    @JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Negocio negocio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoNegocio")
    private List<ObligacionNegocio> obligacionesNegocio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoNegocio")
    private List<RetencionTipoNegocio> retencionesTipoNegocio;

    public TipoNegocio() {
    }

    public TipoNegocio(TipoNegocioPK negTipoNegocioPK) {
        this.tipoNegocioPK = negTipoNegocioPK;
    }

    public TipoNegocio(TipoNegocioPK tipoNegocioPK, Boolean activo) {
        this.tipoNegocioPK = tipoNegocioPK;
        this.activo = activo;
    }

    public TipoNegocio(Integer idValorDominio, String codSfc) {
        this.tipoNegocioPK = new TipoNegocioPK(idValorDominio, codSfc);
    }

    public TipoNegocio(Integer idValorDominio, String codSfc, Boolean activo) {
    	this.tipoNegocioPK = new TipoNegocioPK(idValorDominio, codSfc);
    	this.activo = activo;
	}

	public TipoNegocioPK getTipoNegocioPK() {
        return tipoNegocioPK;
    }

    public void setTipoNegocioPK(TipoNegocioPK tipoNegocioPK) {
        this.tipoNegocioPK = tipoNegocioPK;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Negocio getNegocio() {
        return negocio;
    }

    public void setNegocio(Negocio negocio) {
        this.negocio = negocio;
    }
    
    public List<ObligacionNegocio> getObligacionesNegocio() {
		return obligacionesNegocio;
	}

	public void setObligacionesNegocio(List<ObligacionNegocio> obligacionesNegocio) {
		this.obligacionesNegocio = obligacionesNegocio;
	}

	public List<RetencionTipoNegocio> getRetencionesTipoNegocio() {
		return retencionesTipoNegocio;
	}

	public void setRetencionesTipoNegocio(List<RetencionTipoNegocio> retencionesTipoNegocio) {
		this.retencionesTipoNegocio = retencionesTipoNegocio;
	}

	@Override
	public int hashCode() {
		return Objects.hash(tipoNegocioPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoNegocio other = (TipoNegocio) obj;
		return Objects.equals(tipoNegocioPK, other.tipoNegocioPK);
	}

	@Override
    public String toString() {
        return "modelos.NegTipoNegocio[ tipoNegocioPK=" + tipoNegocioPK + " ]";
    }
    
}
