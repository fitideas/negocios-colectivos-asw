package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="NEG_COMISION_APROBADORA_COMITE")
@XmlRootElement
public class ComisionAprobadoraComite implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@EmbeddedId
	private ComisionAprobadoraComitePK comitePK;
	
	@Column (name ="NOMBRE_COMPLETO")
	private String nomResponsable;
	
    @JoinColumn(name = "ID_DECISION", referencedColumnName = "ID_DECISION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Decision decision;

	
	public ComisionAprobadoraComite() {
		super();
	}

	public ComisionAprobadoraComite( long id , String tipoDocumento, String numDocumento, String nomResponsable) {
		this.comitePK = new ComisionAprobadoraComitePK(  id,  tipoDocumento, numDocumento);
		this.nomResponsable = nomResponsable;
	}

	/**
	 * @return the comitePK
	 */
	public ComisionAprobadoraComitePK getComitePK() {
		return comitePK;
	}

	/**
	 * @param comitePK the comitePK to set
	 */
	public void setComitePK(ComisionAprobadoraComitePK comitePK) {
		this.comitePK = comitePK;
	}

	/**
	 * @return the nomResponsable
	 */
	public String getNomResponsable() {
		return nomResponsable;
	}

	/**
	 * @param nomResponsable the nomResponsable to set
	 */
	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}
	
	
	
}
