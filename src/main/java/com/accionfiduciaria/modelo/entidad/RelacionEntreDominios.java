/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_RELACION_ENTRE_DOMINIOS")
@XmlRootElement
@DynamicUpdate
public class RelacionEntreDominios extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RelacionEntreDominiosPK relacionEntreDominiosPK;
    @Column(name = "VALOR")
    private String valor;
    @JoinColumn(name = "ID_VALOR_DOMINIO1", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ValorDominioNegocio valorDominioNegocio;
    @JoinColumn(name = "ID_VALOR_DOMINIO", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ValorDominioNegocio valorDominioNegocio1;

    public RelacionEntreDominios() {
    }

    public RelacionEntreDominios(RelacionEntreDominiosPK relacionEntreDominiosPK) {
        this.relacionEntreDominiosPK = relacionEntreDominiosPK;
    }

    public RelacionEntreDominios(Integer idValorDominio, Integer idValorDominio2) {
        this.relacionEntreDominiosPK = new RelacionEntreDominiosPK(idValorDominio, idValorDominio2);
    }

    public RelacionEntreDominios(Integer idValorDominio, Integer idValorDominio2, String valor) {
        this.relacionEntreDominiosPK = new RelacionEntreDominiosPK(idValorDominio, idValorDominio2);
        this.valor = valor;
    }

	public RelacionEntreDominiosPK getRelacionEntreDominiosPK() {
        return relacionEntreDominiosPK;
    }

    public void setRelacionEntreDominiosPK(RelacionEntreDominiosPK relacionEntreDominiosPK) {
        this.relacionEntreDominiosPK = relacionEntreDominiosPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ValorDominioNegocio getValorDominioNegocio() {
        return valorDominioNegocio;
    }

    public void setValorDominioNegocio(ValorDominioNegocio valorDominioNegocio) {
        this.valorDominioNegocio = valorDominioNegocio;
    }

    public ValorDominioNegocio getValorDominioNegocio1() {
        return valorDominioNegocio1;
    }

    public void setValorDominioNegocio1(ValorDominioNegocio valorDominioNegocio1) {
        this.valorDominioNegocio1 = valorDominioNegocio1;
    }

    @Override
	public int hashCode() {
		return Objects.hash(relacionEntreDominiosPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelacionEntreDominios other = (RelacionEntreDominios) obj;
		return Objects.equals(relacionEntreDominiosPK, other.relacionEntreDominiosPK);
	}

	@Override
    public String toString() {
        return "modelos.RelacionEntreDominios[ relacionEntreDominiosPK=" + relacionEntreDominiosPK + " Valor = " + valor + "]";
    }
    
}
