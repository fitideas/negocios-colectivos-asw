package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "NEG_DISTRIBUCION_BENEFICIARIO")
public class DistribucionBeneficiario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected DistribucionBeneficiarioPK distribucionBeneficiarioPK;
	@Basic(optional = false)
	@Column(name = "ESTADO")
	private String estado;
	@Basic(optional = false)
	@Column(name = "PORCENTAGE_PARTICIPACION")
	private BigDecimal porcentajeParticipacion;
	@Basic(optional = false)
	@Column(name = "PORCENTAGE_GIRO")
	private BigDecimal porcentajeGiro;
	@Basic(optional = false)
	@Column(name = "VALOR_PAGO")
	private BigDecimal valorPago;
	@Basic(optional = false)
	@Column(name = "VALOR_GMF")
	private BigDecimal valorGMF;
	@Basic(optional = false)
	@Column(name = "VALOR_TRASLADO")
	private BigDecimal valorTraslado;
	@Basic(optional = false)
	@Column(name = "VALOR_MENOS_DESCUENTOS")
	private BigDecimal valorMenosDescuento;
	@Basic(optional = false)
	@Column(name = "NEG_DIST_COD_SFC")
	private String distribucionCodigoSFC;
	@Basic(optional = false)
	@Column(name = "NEG_DIST_TIPO_NEGOCIO")
	private Integer distribucionTipoNegocio;
	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_HORA")
	private Date fechaHora;
	@Basic(optional = false)
	@Column(name = "PERIODO")
	private String periodo;
	@JoinColumns({
			@JoinColumn(name = "NEG_DIST_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "NEG_DIST_TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "FECHA_HORA", referencedColumnName = "FECHA_HORA", insertable = false, updatable = false),
			@JoinColumn(name = "PERIODO", referencedColumnName = "PERIODO", insertable = false, updatable = false), })
	@ManyToOne(optional = false)
	private Distribucion distribucion;
	@JoinColumns({ @JoinColumn(name = "CUENTA", referencedColumnName = "CUENTA", insertable = false, updatable = false),
			@JoinColumn(name = "BANCO", referencedColumnName = "BANCO", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_CUENTA", referencedColumnName = "TIPO_CUENTA", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_DOC_TITULAR", referencedColumnName = "TIPO_DOC_TITULAR", insertable = false, updatable = false),
			@JoinColumn(name = "NUM_DOC_TITULAR", referencedColumnName = "NUM_DOC_TITULAR", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_DOC_BENEFICIARIO", referencedColumnName = "TIPO_DOC_BENEFICIARIO", insertable = false, updatable = false),
			@JoinColumn(name = "NUM_DOC_BENEFICIARIO", referencedColumnName = "NUM_DOC_BENEFICIARIO", insertable = false, updatable = false),
			@JoinColumn(name = "NEG_BEN_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private Beneficiario beneficiario;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "distribucionBeneficiario")
    private List<RetencionImpuestoBeneficiario> retencionImpuestosBeneficiarios;	
	
	@JoinColumn(name = "NEG_BEN_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private Negocio negocio;

	@JoinColumn(name = "BANCO", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioBanco;

	@JoinColumn(name = "TIPO_CUENTA", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioCuenta;

	public DistribucionBeneficiario() {

	}

	/**
	 * @param distribucionBeneficiarioPK
	 * @param distribucionPK
	 */
	public DistribucionBeneficiario(BeneficiarioPK beneficiarioPK, DistribucionPK distribucionPK) {
		this.distribucionBeneficiarioPK = new DistribucionBeneficiarioPK(beneficiarioPK);
		this.distribucionCodigoSFC = distribucionPK.getCodSfc();
		this.distribucionTipoNegocio = distribucionPK.getTipoNegocio();
		this.fechaHora = distribucionPK.getFechaHora();
		this.periodo = distribucionPK.getPeriodo();
	}

	public DistribucionBeneficiarioPK getDistribucionBeneficiarioPK() {
		return distribucionBeneficiarioPK;
	}

	public void setDistribucionBeneficiarioPK(DistribucionBeneficiarioPK distribucionBeneficiarioPK) {
		this.distribucionBeneficiarioPK = distribucionBeneficiarioPK;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public BigDecimal getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public BigDecimal getPorcentajeGiro() {
		return porcentajeGiro;
	}

	public void setPorcentajeGiro(BigDecimal porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	public BigDecimal getValorPago() {
		return valorPago;
	}

	public void setValorPago(BigDecimal valorPago) {
		this.valorPago = valorPago;
	}

	public BigDecimal getValorGMF() {
		return valorGMF;
	}

	public void setValorGMF(BigDecimal valorGMF) {
		this.valorGMF = valorGMF;
	}

	public BigDecimal getValorTraslado() {
		return valorTraslado;
	}

	public void setValorTraslado(BigDecimal valorTraslado) {
		this.valorTraslado = valorTraslado;
	}

	public BigDecimal getValorMenosDescuento() {
		return valorMenosDescuento;
	}

	public void setValorMenosDescuento(BigDecimal valorMenosDescuento) {
		this.valorMenosDescuento = valorMenosDescuento;
	}

	public String getDistribucionCodigoSFC() {
		return distribucionCodigoSFC;
	}

	public void setDistribucionCodigoSFC(String distribucionCodigoSFC) {
		this.distribucionCodigoSFC = distribucionCodigoSFC;
	}

	public Integer getDistribucionTipoNegocio() {
		return distribucionTipoNegocio;
	}

	public void setDistribucionTipoNegocio(Integer distribucionTipoNegocio) {
		this.distribucionTipoNegocio = distribucionTipoNegocio;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public ValorDominioNegocio getDominioBanco() {
		return dominioBanco;
	}

	public void setDominioBanco(ValorDominioNegocio dominioBanco) {
		this.dominioBanco = dominioBanco;
	}

	public ValorDominioNegocio getDominioCuenta() {
		return dominioCuenta;
	}

	public void setDominioCuenta(ValorDominioNegocio dominioCuenta) {
		this.dominioCuenta = dominioCuenta;
	}

	public Distribucion getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(Distribucion distribucion) {
		this.distribucion = distribucion;
	}

	public List<RetencionImpuestoBeneficiario> getRetencionImpuestosBeneficiarios() {
		return retencionImpuestosBeneficiarios;
	}

	public void setRetencionImpuestosBeneficiarios(
			List<RetencionImpuestoBeneficiario> retencionImpuestosBeneficiarios) {
		this.retencionImpuestosBeneficiarios = retencionImpuestosBeneficiarios;
	}

}
