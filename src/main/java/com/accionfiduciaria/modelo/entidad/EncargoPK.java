package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class EncargoPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4379431333420553402L;
	
	@Basic(optional = false)
    @Column(name = "COD_SFC")
    private String codSfc;
    @Basic(optional = false)
    @Column(name = "CUENTA")
    private String cuenta;

    public EncargoPK() {
    }

    public EncargoPK(String codSfc, String cuenta) {
        this.codSfc = codSfc;
        this.cuenta = cuenta;
    }

    public String getCodSfc() {
        return codSfc;
    }

    public void setCodSfc(String codSfc) {
        this.codSfc = codSfc;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, cuenta);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EncargoPK other = (EncargoPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(cuenta, other.cuenta);
	}

	@Override
	public String toString() {
		return "EncargoPK [codSfc=" + codSfc + ", cuenta=" + cuenta + "]";
	}
}
