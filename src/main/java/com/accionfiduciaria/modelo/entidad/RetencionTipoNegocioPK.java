package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class RetencionTipoNegocioPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
    @Column(name = "ID_RETENCION")
    private Integer idRetencion;
    @Basic(optional = false)
    @Column(name = "TIPO_NEGOCIO")
    private Integer tipoNegocio;
    @Basic(optional = false)
    @Column(name = "COD_SFC")
    private String codSfc;

    public RetencionTipoNegocioPK() {
    }

    public RetencionTipoNegocioPK(Integer idRetencion, Integer tipoNegocio, String codSfc) {
        this.idRetencion = idRetencion;
        this.tipoNegocio = tipoNegocio;
        this.codSfc = codSfc;
    }

    public Integer getIdRetencion() {
        return idRetencion;
    }

    public void setIdRetencion(Integer idRetencion) {
        this.idRetencion = idRetencion;
    }

    public Integer getTipoNegocio() {
        return tipoNegocio;
    }

    public void setTipoNegocio(Integer tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

    public String getCodSfc() {
        return codSfc;
    }

    public void setCodSfc(String codSfc) {
        this.codSfc = codSfc;
    }

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, idRetencion, tipoNegocio);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetencionTipoNegocioPK other = (RetencionTipoNegocioPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(idRetencion, other.idRetencion)
				&& Objects.equals(tipoNegocio, other.tipoNegocio);
	}

	@Override
	public String toString() {
		return "RetencionTipoNegocioPK [idRetencion=" + idRetencion + ", tipoNegocio=" + tipoNegocio + ", codSfc=" + codSfc
				+ "]";
	}
}
