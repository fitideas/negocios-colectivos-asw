package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "NEG_REUNION")
public class Reunion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ReunionPK reunionPk;
	@Column(name = "FECHA_REUNION", columnDefinition = "TIMESTAMP")
	private LocalDateTime fechaReunion;
	@Column(name = "QUORUM")
	private Integer quorum;
	@Column(name = "ID_ACTA")
	private String idActa;
	@Column(name = "PRESENTACION")
	private String presentacion;
	@Column(name = "RESUMEN_REUNION")
	private String resumenReunion;
	@Column(name = "TIPO")
	private String tipo;
	@Column(name = "DIRECCION")
	private String direccion;
	@Column(name = "ORDEN_DIA")
	private String ordenDia;
	@Column(name = "USUARIO")
	private String usuario;
	@Column(name = "TIPO_DOCUMENTO_USUARIO")
	private String tipoDocumentoUsuario;
	@Column(name = "NUMERO_DOCUMENTO_USUARIO")
	private String numeroDocumentoUsuario;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "reunion", fetch = FetchType.LAZY, orphanRemoval = false)
	private List<MiembroMesaDirectiva> miembros;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "reunion", fetch = FetchType.LAZY, orphanRemoval = false)
	private List<Decision> decisiones;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "reunion", fetch = FetchType.LAZY, orphanRemoval = false)
	private List<Asistente> asistentesReunion;

	@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
	@ManyToOne(optional = false)
	private Negocio negocio;

	public ReunionPK getReunionPk() {
		return reunionPk;
	}

	public void setReunionPk(ReunionPK reunionPk) {
		this.reunionPk = reunionPk;
	}

	public LocalDateTime getFechaReunion() {
		return fechaReunion;
	}

	public void setFechaReunion(LocalDateTime fechaReunion) {
		this.fechaReunion = fechaReunion;
	}

	public Integer getQuorum() {
		return quorum;
	}

	public void setQuorum(Integer quorum) {
		this.quorum = quorum;
	}

	public String getIdActa() {
		return idActa;
	}

	public void setIdActa(String idActa) {
		this.idActa = idActa;
	}

	public String getPresentacion() {
		return presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public String getResumenReunion() {
		return resumenReunion;
	}

	public void setResumenReunion(String resumenReunion) {
		this.resumenReunion = resumenReunion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getOrdenDia() {
		return ordenDia;
	}

	public void setOrdenDia(String ordenDia) {
		this.ordenDia = ordenDia;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTipoDocumentoUsuario() {
		return tipoDocumentoUsuario;
	}

	public void setTipoDocumentoUsuario(String tipoDocumentoUsuario) {
		this.tipoDocumentoUsuario = tipoDocumentoUsuario;
	}

	public String getNumeroDocumentoUsuario() {
		return numeroDocumentoUsuario;
	}

	public void setNumeroDocumentoUsuario(String numeroDocumentoUsuario) {
		this.numeroDocumentoUsuario = numeroDocumentoUsuario;
	}

	public List<Decision> getDecisiones() {
		return decisiones;
	}

	public void setDecisiones(List<Decision> decisiones) {
		this.decisiones = decisiones;
	}

	public List<MiembroMesaDirectiva> getMiembros() {
		return miembros;
	}

	public void setMiembros(List<MiembroMesaDirectiva> miembros) {
		this.miembros = miembros;
	}

	/**
	 * @return the asistentesReunion
	 */
	public List<Asistente> getAsistentesReunion() {
		return asistentesReunion;
	}

	/**
	 * @param asistentesReunion the asistentesReunion to set
	 */
	public void setAsistentesReunion(List<Asistente> asistentesReunion) {
		this.asistentesReunion = asistentesReunion;
	}
	
	/**
	 * @return the negocio
	 */
	public Negocio getNegocio() {
		return negocio;
	}

	/**
	 * @param negocio the negocio to set
	 */
	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	@Override
	public int hashCode() {
		return Objects.hash(reunionPk);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reunion other = (Reunion) obj;
		return Objects.equals(reunionPk, other.reunionPk);
	}

	@Override
	public String toString() {
		return "Reunion [reunionPk=" + reunionPk + "]";
	}

}
