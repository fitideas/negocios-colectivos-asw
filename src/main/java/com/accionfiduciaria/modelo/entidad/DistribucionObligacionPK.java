/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author efarias
 *
 */
@Embeddable
public class DistribucionObligacionPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "TIPO_PAGO")
	private Integer tipoPago;

	@Basic(optional = false)
	@Column(name = "NEG_OBLI_NEG_COD_SFC")
	private String obligacionCodSfc;

	@Basic(optional = false)
	@Column(name = "NEG_OBLI_NEG_TIPO_NEGOCIO")
	private Integer obligacionTipoNegocio;

	@Basic(optional = false)
	@Column(name = "NEG_DIST_COD_SFC")
	private String distribucionCodSfc;

	@Basic(optional = false)
	@Column(name = "NEG_DIST_TIPO_NEGOCIO")
	private Integer distribucionTipoNegocio;
	
	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_HORA")
	private Date fechaHora;

	@Basic(optional = false)
	@Column(name = "PERIODO")
	private String periodo;

	public DistribucionObligacionPK() {
	}

	public DistribucionObligacionPK(Integer tipoPago, String obligacionCodSfc, Integer obligacionTipoNegocio,
			String distribucionCodSfc, Integer distribucionTipoNegocio, Date fechaHora, String periodo) {
		this.tipoPago = tipoPago;
		this.obligacionCodSfc = obligacionCodSfc;
		this.obligacionTipoNegocio = obligacionTipoNegocio;
		this.distribucionCodSfc = distribucionCodSfc;
		this.distribucionTipoNegocio = distribucionTipoNegocio;
		this.fechaHora = fechaHora;
		this.periodo = periodo;
	}
	
	public DistribucionObligacionPK(DistribucionPK distribucionPK, ObligacionNegocioPK obligacionNegocioPK) {
		this.tipoPago = obligacionNegocioPK.getTipoPago();
		this.obligacionCodSfc = obligacionNegocioPK.getCodSfc();
		this.obligacionTipoNegocio = obligacionNegocioPK.getTipoNegocio();
		this.distribucionCodSfc = distribucionPK.getCodSfc();
		this.distribucionTipoNegocio = distribucionPK.getTipoNegocio();
		this.fechaHora = distribucionPK.getFechaHora();
		this.periodo = distribucionPK.getPeriodo();
	}

	@Override
	public int hashCode() {
		return Objects.hash(tipoPago, obligacionCodSfc, obligacionTipoNegocio, distribucionCodSfc,
				distribucionTipoNegocio, fechaHora, periodo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistribucionObligacionPK other = (DistribucionObligacionPK) obj;
		return Objects.equals(tipoPago, other.tipoPago) && Objects.equals(obligacionCodSfc, other.obligacionCodSfc)
				&& Objects.equals(obligacionTipoNegocio, other.obligacionTipoNegocio)
				&& Objects.equals(distribucionCodSfc, other.distribucionCodSfc)
				&& Objects.equals(distribucionTipoNegocio, other.distribucionTipoNegocio)
				&& Objects.equals(fechaHora, other.fechaHora) && Objects.equals(periodo, other.periodo);
	}

	@Override
	public String toString() {
		return "DistribucionObligacionPK [tipoPago=" + tipoPago + ", obligacionCodSfc=" + obligacionCodSfc
				+ ", obligacionTipoNegocio=" + obligacionTipoNegocio + ", distribucionCodSfc=" + distribucionCodSfc
				+ ", distribucionTipoNegocio=" + distribucionTipoNegocio + ", fechaHora=" + fechaHora + ", periodo="
				+ periodo + "]";
	}

	public Integer getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}

	public String getObligacionCodSfc() {
		return obligacionCodSfc;
	}

	public void setObligacionCodSfc(String obligacionCodSfc) {
		this.obligacionCodSfc = obligacionCodSfc;
	}

	public Integer getObligacionTipoNegocio() {
		return obligacionTipoNegocio;
	}

	public void setObligacionTipoNegocio(Integer obligacionTipoNegocio) {
		this.obligacionTipoNegocio = obligacionTipoNegocio;
	}

	public String getDistribucionCodSfc() {
		return distribucionCodSfc;
	}

	public void setDistribucionCodSfc(String distribucionCodSfc) {
		this.distribucionCodSfc = distribucionCodSfc;
	}

	public Integer getDistribucionTipoNegocio() {
		return distribucionTipoNegocio;
	}

	public void setDistribucionTipoNegocio(Integer distribucionTipoNegocio) {
		this.distribucionTipoNegocio = distribucionTipoNegocio;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

}
