/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_RETENCION_IMPUESTO_BENE")
@XmlRootElement
public class RetencionImpuestoBeneficiario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RetencionImpuestoBeneficiarioPK retencionImpuestoBeneficiarioPK;
	@Basic(optional = false)
	@Column(name = "UNIDAD")
	private String unidad;
	@Basic(optional = false)
	@Column(name = "VALOR_CONFIGURADO_IMPUESTO")
	private BigDecimal valorConfiguradoImpuesto;
	@Basic(optional = false)
	@Column(name = "VALOR_BASE")
	private BigDecimal valorBase;
	@Basic(optional = false)
	@Column(name = "VALOR_IMPUESTO")
	private BigDecimal valorImpuesto;
	@JoinColumns({
        @JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_RETENCION", insertable = false, updatable = false)
        , @JoinColumn(name = "NEG_RET_TITU_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
        , @JoinColumn(name = "NEG_RET_TITU_NUM_DOC", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "NEG_RET_TITU_TIPO_DOC", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "NEG_RET_TITU_TIPO_NAT_NEG", referencedColumnName = "TIPO_NATURALEZA_NEGOCIO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
	private RetencionTitular retencionTitular;
	@JoinColumns({
			@JoinColumn(name = "NEG_DIS_BEN_COD_SFC", referencedColumnName = "NEG_BEN_COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_DOC_TITULAR", referencedColumnName = "TIPO_DOC_TITULAR", insertable = false, updatable = false),
			@JoinColumn(name = "NUM_DOC_TITULAR", referencedColumnName = "NUM_DOC_TITULAR", insertable = false, updatable = false),
			@JoinColumn(name = "NUM_DOC_BENEFICIARIO", referencedColumnName = "NUM_DOC_BENEFICIARIO", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_DOC_BENEFICIARIO", referencedColumnName = "TIPO_DOC_BENEFICIARIO", insertable = false, updatable = false),
			@JoinColumn(name = "BANCO", referencedColumnName = "BANCO", insertable = false, updatable = false),
			@JoinColumn(name = "CUENTA", referencedColumnName = "CUENTA", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_CUENTA", referencedColumnName = "TIPO_CUENTA", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private DistribucionBeneficiario distribucionBeneficiario;
	@JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioRetencion;
	@JoinColumn(name = "BANCO", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioBanco;
	@JoinColumn(name = "TIPO_CUENTA", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioTipoCuenta;

	public RetencionImpuestoBeneficiario() {

	}

	public RetencionImpuestoBeneficiario(RetencionTitularPK retencionTitularPK,
			DistribucionBeneficiarioPK distribucionBeneficiarioPK) {
		this.retencionImpuestoBeneficiarioPK = new RetencionImpuestoBeneficiarioPK(retencionTitularPK,
				distribucionBeneficiarioPK);
	}

	public RetencionImpuestoBeneficiario(RetencionTitularPK retencionTitularPK,
			DistribucionBeneficiarioPK distribucionBeneficiarioPK, String unidad, BigDecimal valorConfiguradoImpuesto,
			BigDecimal valorBase, BigDecimal valorImpuesto) {
		this.retencionImpuestoBeneficiarioPK = new RetencionImpuestoBeneficiarioPK(retencionTitularPK,
				distribucionBeneficiarioPK);
		this.unidad = unidad;
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
		this.valorBase = valorBase;
		this.valorImpuesto = valorImpuesto;
	}

	public RetencionImpuestoBeneficiario(RetencionImpuestoBeneficiarioPK retencionImpuestoBeneficiarioPK, String unidad,
			BigDecimal valorConfiguradoImpuesto, BigDecimal valorBase, BigDecimal valorImpuesto) {
		this.retencionImpuestoBeneficiarioPK = retencionImpuestoBeneficiarioPK;
		this.unidad = unidad;
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
		this.valorBase = valorBase;
		this.valorImpuesto = valorImpuesto;
	}

	public RetencionImpuestoBeneficiarioPK getRetencionImpuestoBeneficiarioPK() {
		return retencionImpuestoBeneficiarioPK;
	}

	public void setRetencionImpuestoBeneficiarioPK(RetencionImpuestoBeneficiarioPK retencionImpuestoBeneficiarioPK) {
		this.retencionImpuestoBeneficiarioPK = retencionImpuestoBeneficiarioPK;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public BigDecimal getValorConfiguradoImpuesto() {
		return valorConfiguradoImpuesto;
	}

	public void setValorConfiguradoImpuesto(BigDecimal valorConfiguradoImpuesto) {
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public BigDecimal getValorImpuesto() {
		return valorImpuesto;
	}

	public void setValorImpuesto(BigDecimal valorImpuesto) {
		this.valorImpuesto = valorImpuesto;
	}

	public RetencionTitular getRetencionTitular() {
		return retencionTitular;
	}

	public DistribucionBeneficiario getDistribucionBeneficiario() {
		return distribucionBeneficiario;
	}

	public ValorDominioNegocio getDominioRetencion() {
		return dominioRetencion;
	}

	public ValorDominioNegocio getDominioBanco() {
		return dominioBanco;
	}

	public ValorDominioNegocio getDominioTipoCuenta() {
		return dominioTipoCuenta;
	}

}
