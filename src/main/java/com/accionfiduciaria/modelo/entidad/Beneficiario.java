package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_BENEFICIARIO")
@XmlRootElement
public class Beneficiario extends Auditoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3051506582822881024L;
	
	@EmbeddedId
    protected BeneficiarioPK beneficiarioPK;
    @Column(name = "PORCENTAJE_PARTICIPACION")
    private BigDecimal porcentaje;
    @Column(name = "NUMERO_DERECHOS")
    private Float numeroDerechos;
    @Column(name = "FECHA_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "FECHA_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVigencia;
    @Column(name = "FECHA_VENCIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimiento;
    @Column(name = "ESTADO")
    private Boolean estado;
    @Column(name = "PORCENTAJE_GIRO")
    private BigDecimal porcentajeGiro;
    @JoinColumns({
        @JoinColumn(name = "CUENTA", referencedColumnName = "CUENTA", insertable = false, updatable = false)
        , @JoinColumn(name = "BANCO", referencedColumnName = "BANCO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_CUENTA", referencedColumnName = "TIPO_CUENTA", insertable = false, updatable = false)
        , @JoinColumn(name = "NUM_DOC_BENEFICIARIO", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_DOC_BENEFICIARIO", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private MedioPagoPersona medioPagoPersona;
    @JoinColumns({
        @JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
        , @JoinColumn(name = "NUM_DOC_TITULAR", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_DOC_TITULAR", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Titular titular;

    public Beneficiario() {
    }

    public Beneficiario(BeneficiarioPK beneficiarioPK) {
        this.beneficiarioPK = beneficiarioPK;
    }

    public BeneficiarioPK getBeneficiarioPK() {
        return beneficiarioPK;
    }

    public void setBeneficiarioPK(BeneficiarioPK beneficiarioPK) {
        this.beneficiarioPK = beneficiarioPK;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Float getNumeroDerechos() {
        return numeroDerechos;
    }

    public void setNumeroDerechos(Float numeroDerechos) {
        this.numeroDerechos = numeroDerechos;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaVigencia() {
        return fechaVigencia;
    }

    public void setFechaVigencia(Date fechaVigencia) {
        this.fechaVigencia = fechaVigencia;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    
    
    public BigDecimal getPorcentajeGiro() {
		return porcentajeGiro;
	}

	public void setPorcentajeGiro(BigDecimal porcentajeGiro) {
		this.porcentajeGiro = porcentajeGiro;
	}

	public MedioPagoPersona getMedioPagoPersona() {
        return medioPagoPersona;
    }

    public void setMedioPagoPersona(MedioPagoPersona medioPagoPersona) {
        this.medioPagoPersona = medioPagoPersona;
    }

    public Titular getTitular() {
        return titular;
    }

    public void setTitular(Titular titular) {
        this.titular = titular;
    }

	@Override
	public int hashCode() {
		return Objects.hash(beneficiarioPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Beneficiario other = (Beneficiario) obj;
		return Objects.equals(beneficiarioPK, other.beneficiarioPK);
	}
}
