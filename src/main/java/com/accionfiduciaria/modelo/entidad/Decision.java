package com.accionfiduciaria.modelo.entidad;



import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "NEG_DECISIONES")
@DynamicUpdate
public class Decision implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "SEQ_DECISIONES_ASAMBLEA")
    @GenericGenerator(name = "SEQ_DECISIONES_ASAMBLEA", strategy = "increment")
	@Column(name="ID_DECISION")
	private Long decisionesId;
	@Column(name="DESCRIPCION")
	private String descripcion;
	@Column(name="ESTADO")
	private boolean estado;
	@Column(name="RESPONSABLE")
	private String responsable;
	@Column(name="QUORUMAPROBATORIO")
	private Integer quorumAprobatorio;
	@Basic(optional = false)
    @Column(name = "FECHA", columnDefinition = "TIMESTAMP")
	private LocalDateTime fecha;
	@Basic(optional = false)
    @Column(name = "COD_SFC")
	private String codSfc;
	
	@JoinColumns({
        @JoinColumn(name = "FECHA", referencedColumnName = "FECHA", insertable = false, updatable = false)
        , @JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)})
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
	private Reunion reunion;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "decision", fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ComisionAprobadoraComite> tomadorDecision;
	
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	
	public Long getDecisionesId() {
		return decisionesId;
	}

	public void setDecisionesId(Long decisionesId) {
		this.decisionesId = decisionesId;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public Reunion getReunion() {
		return reunion;
	}

	public void setReunion(Reunion reunion) {
		this.reunion = reunion;
	}
	
	public Integer getQuorumAprobatorio() {
		return quorumAprobatorio;
	}

	public void setQuorumAprobatorio(Integer quorumAprobatorio) {
		this.quorumAprobatorio = quorumAprobatorio;
	}
	/**
	 * @return the tomadorDecision
	 */
	public List<ComisionAprobadoraComite> getTomadorDecision() {
		return tomadorDecision;
	}
	/**
	 * @param tomadorDecision the tomadorDecision to set
	 */
	public void setTomadorDecision(List<ComisionAprobadoraComite> tomadorDecision) {
		this.tomadorDecision = tomadorDecision;
	}
	
	
}
