package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class SubtipoFideicomisoPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5654194766667840465L;
	@Basic(optional = false)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "CODIGO_TIPO_FIDEICOMISO")
    private String codigoTipoFideicomiso;

    public SubtipoFideicomisoPK() {
    }

    public SubtipoFideicomisoPK(String codigo, String codigoTipoFideicomiso) {
        this.codigo = codigo;
        this.codigoTipoFideicomiso = codigoTipoFideicomiso;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoTipoFideicomiso() {
        return codigoTipoFideicomiso;
    }

    public void setCodigoTipoFideicomiso(String codigoTipoFideicomiso) {
        this.codigoTipoFideicomiso = codigoTipoFideicomiso;
    }

	@Override
	public int hashCode() {
		return Objects.hash(codigo, codigoTipoFideicomiso);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubtipoFideicomisoPK other = (SubtipoFideicomisoPK) obj;
		return Objects.equals(codigo, other.codigo)
				&& Objects.equals(codigoTipoFideicomiso, other.codigoTipoFideicomiso);
	}

	@Override
	public String toString() {
		return "SubtipoFideicomisoPK [codigo=" + codigo + ", codigoTipoFideicomiso=" + codigoTipoFideicomiso + "]";
	}
}
