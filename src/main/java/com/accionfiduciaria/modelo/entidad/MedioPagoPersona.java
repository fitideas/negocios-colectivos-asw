package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_MEDIO_PAGO_PERSONA")
@XmlRootElement
public class MedioPagoPersona extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MedioPagoPersonaPK medioPagoPersonaPK;
    @Basic(optional = false)
    @Column(name = "ESTADO")
    private Boolean estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medioPagoPersona")
    private List<Beneficiario> beneficiarios;
    @JoinColumns({
        @JoinColumn(name = "NUMERO_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Persona persona;
    @JoinColumns({
        @JoinColumn(name = "CUENTA", referencedColumnName = "CUENTA", insertable = false, updatable = false)
        , @JoinColumn(name = "BANCO", referencedColumnName = "BANCO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_CUENTA", referencedColumnName = "TIPO_CUENTA", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private MedioPago medioPago;

    public MedioPagoPersona() {
    }

    public MedioPagoPersona(MedioPagoPersonaPK medioPagoPersonaPK) {
        this.medioPagoPersonaPK = medioPagoPersonaPK;
    }

    public MedioPagoPersona(MedioPagoPersonaPK medioPagoPersonaPK, Boolean estado) {
        this.medioPagoPersonaPK = medioPagoPersonaPK;
        this.estado = estado;
    }

    public MedioPagoPersona(String cuenta, String numeroDocumento, String tipoDocumento, Integer banco, Integer tipoCuenta) {
        this.medioPagoPersonaPK = new MedioPagoPersonaPK(cuenta, numeroDocumento, tipoDocumento, banco, tipoCuenta);
    }

    public MedioPagoPersonaPK getMedioPagoPersonaPK() {
        return medioPagoPersonaPK;
    }

    public void setMedioPagoPersonaPK(MedioPagoPersonaPK medioPagoPersonaPK) {
        this.medioPagoPersonaPK = medioPagoPersonaPK;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<Beneficiario> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

    @Override
	public int hashCode() {
		return Objects.hash(medioPagoPersonaPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedioPagoPersona other = (MedioPagoPersona) obj;
		return Objects.equals(medioPagoPersonaPK, other.medioPagoPersonaPK);
	}

	@Override
    public String toString() {
        return "modelos.NegMedioPagoPersona[ negMedioPagoPersonaPK=" + medioPagoPersonaPK + " ]";
    }
    
}
