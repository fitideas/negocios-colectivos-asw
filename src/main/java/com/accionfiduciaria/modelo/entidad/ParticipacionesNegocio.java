package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_PARTICIPACIONES_NEGOCIO")
@XmlRootElement
public class ParticipacionesNegocio extends Auditoria implements Serializable {

	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@Id
	@Basic(optional = false)
    @GeneratedValue(generator = "SEQ_PARTICIPACIONES_NEGOCIO")
    @GenericGenerator(name = "SEQ_PARTICIPACIONES_NEGOCIO", strategy = "increment")
	@Column(name = "ID_PARTICIPACION")
	private Integer idParticipacion;
	@Basic(optional = false)
	@Column(name = "NUMERO_DERECHOS")
	private Integer numeroDerechos;	
	@Basic(optional = false)
	@Column(name = "PORCENTAJE_PARTICIPACION")
	private BigDecimal porcentajeParticipacion ;	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "participacion")
	private List<Titular> titulares;
	@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC")
	@ManyToOne(optional = false)
	private Negocio negocio;

	public ParticipacionesNegocio() {
	}

	public ParticipacionesNegocio(Integer idParticipacion) {
		this.idParticipacion = idParticipacion;
	}

	public ParticipacionesNegocio(Integer idParticipacion, Integer numeroDerechos) {
		this.idParticipacion = idParticipacion;
		this.numeroDerechos = numeroDerechos;
	}

	public Integer getIdParticipacion() {
		return idParticipacion;
	}

	public void setIdParticipacion(Integer idParticipacion) {
		this.idParticipacion = idParticipacion;
	}

	public Integer getNumeroDerechos() {
		return numeroDerechos;
	}

	public void setNumeroDerechos(Integer numeroDerechos) {
		this.numeroDerechos = numeroDerechos;
	}
	
	public BigDecimal getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	@XmlTransient
	public List<Titular> getTitulares() {
		return titulares;
	}

	public void setTitulares(List<Titular> titulares) {
		this.titulares = titulares;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idParticipacion);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticipacionesNegocio other = (ParticipacionesNegocio) obj;
		return Objects.equals(idParticipacion, other.idParticipacion);
	}

	@Override
	public String toString() {
		return "ParticipacionesNegocio [idParticipacion=" + idParticipacion + ", numeroDerechos=" + numeroDerechos
				+ "]";
	}
}
