package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class AudHistorialDetallePK implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
    @Column(name="AUD_HISTORIAL_COD_SFC")
  	private String codSFC;
	
	@Basic(optional = false)
	@Column(name="AUD_HISTORIAL_NOMBRE_TABLA")
    private String nombreTabla;
	
	@Basic(optional = false)
	@Column(name="AUD_HISTORIAL_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
	
	
	@Column(name="AUD_HISTORIAL_OPERACION")
    private String operacion;
	
	
	@Basic(optional = false)
	@Column(name="CAMPO")                    
    private String campo;

	
	
	public AudHistorialDetallePK() {
		super();
	}

	public AudHistorialDetallePK(String codSFC, String nombreTabla, Date fecha, String operacion) {
		super();
		this.codSFC = codSFC;
		this.nombreTabla = nombreTabla;
		this.fecha = fecha;
		this.operacion = operacion;
	}

	public String getCodSFC() {
		return codSFC;
	}

	public void setCodSFC(String codSFC) {
		this.codSFC = codSFC;
	}

	public String getNombreTabla() {
		return nombreTabla;
	}

	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	
	
	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(campo, codSFC, fecha, nombreTabla, operacion);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AudHistorialDetallePK other = (AudHistorialDetallePK) obj;
		return Objects.equals(campo, other.campo) && Objects.equals(codSFC, other.codSFC)
				&& Objects.equals(fecha, other.fecha) && Objects.equals(nombreTabla, other.nombreTabla)
				&& Objects.equals(operacion, other.operacion);
	}

	@Override
	public String toString() {
		return "AudHistorialDetallePK [codSFC=" + codSFC + ", nombreTabla=" + nombreTabla + ", fecha=" + fecha
				+ ", operacion=" + operacion + ", campo=" + campo + "]";
	}	
}
