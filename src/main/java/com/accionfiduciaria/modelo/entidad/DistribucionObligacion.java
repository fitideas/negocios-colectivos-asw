/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_DISTRIBUCION_OBLIGACIONES")
@XmlRootElement
public class DistribucionObligacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected DistribucionObligacionPK distribucionObligacionPK;
	@Basic(optional = false)
	@Column(name = "UNIDAD")
	private String unidad;
	@Basic(optional = false)
	@Column(name = "VALOR")
	private BigDecimal valor;
	@Basic(optional = false)
	@Column(name = "VALOR_PAGO")
	private BigDecimal valorPago;
	@JoinColumns({
			@JoinColumn(name = "NEG_DIST_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "NEG_DIST_TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "FECHA_HORA", referencedColumnName = "FECHA_HORA", insertable = false, updatable = false),
			@JoinColumn(name = "PERIODO", referencedColumnName = "PERIODO", insertable = false, updatable = false), })
	@ManyToOne(optional = false)
	private Distribucion distribucion;
	@JoinColumns({
        @JoinColumn(name = "TIPO_PAGO", referencedColumnName = "TIPO_PAGO", insertable = false, updatable = false)
        , @JoinColumn(name = "NEG_OBLI_NEG_TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false)
        , @JoinColumn(name = "NEG_OBLI_NEG_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
        , @JoinColumn(name = "NEG_OBLI_NEG_NOMBRE_GASTO", referencedColumnName = "NOMBRE_GASTO", insertable = false, updatable = false)})
	@ManyToOne(optional = false)
	private ObligacionNegocio obligacionNegocio;

	public DistribucionObligacion() {
	}

	public DistribucionObligacion(DistribucionObligacionPK distribucionObligacionPK, String unidad, BigDecimal valor,
			BigDecimal valorPago) {
		this.distribucionObligacionPK = distribucionObligacionPK;
		this.unidad = unidad;
		this.valor = valor;
		this.valorPago = valorPago;
	}

	@Override
	public int hashCode() {
		return Objects.hash(distribucionObligacionPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistribucionObligacion other = (DistribucionObligacion) obj;
		return Objects.equals(distribucionObligacionPK, other.distribucionObligacionPK);
	}

	@Override
	public String toString() {
		return "modelos.DistribucionObligacion [distribucionObligacionPK=" + distribucionObligacionPK + "]";
	}

	public DistribucionObligacionPK getDistribucionObligacionPK() {
		return distribucionObligacionPK;
	}

	public void setDistribucionObligacionPK(DistribucionObligacionPK distribucionObligacionPK) {
		this.distribucionObligacionPK = distribucionObligacionPK;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValorPago() {
		return valorPago;
	}

	public void setValorPago(BigDecimal valorPago) {
		this.valorPago = valorPago;
	}

	public Distribucion getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(Distribucion distribucion) {
		this.distribucion = distribucion;
	}

	public ObligacionNegocio getObligacionNegocio() {
		return obligacionNegocio;
	}

	public void setObligacionNegocio(ObligacionNegocio obligacionNegocio) {
		this.obligacionNegocio = obligacionNegocio;
	}

}
