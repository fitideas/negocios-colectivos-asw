package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReunionPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "FECHA", columnDefinition = "TIMESTAMP")
	private LocalDateTime fecha;
	@Column(name = "COD_SFC")
	private String codigoSfc;

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getCodigoSfc() {
		return codigoSfc;
	}

	public void setCodigoSfc(String codigoSfc) {
		this.codigoSfc = codigoSfc;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigoSfc, fecha);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReunionPK other = (ReunionPK) obj;
		return Objects.equals(codigoSfc, other.codigoSfc) && Objects.equals(fecha, other.fecha);
	}

	@Override
	public String toString() {
		return "ReunionPK [fecha=" + fecha + ", codigoSfc=" + codigoSfc + "]";
	}

}
