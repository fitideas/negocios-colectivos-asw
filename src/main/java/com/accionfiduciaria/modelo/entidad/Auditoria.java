/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

/**
 * Super clase de auditora toda entidad que deba ser auditada deberá heredar de
 * esta clase.
 * 
 * @author efarias
 *
 */
@MappedSuperclass
@EntityListeners(AbstractEntityListener.class)
public class Auditoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Basic(optional = false)
	@Column(name = "LOGIN_AUDITORIA")
	private String loginAuditoria;

	public String getLoginAuditoria() {
		return loginAuditoria;
	}

	public void setLoginAuditoria(String loginAuditoria) {
		this.loginAuditoria = loginAuditoria;
	}
}
