/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_DISTRIBUCION")
@XmlRootElement
public class Distribucion extends Auditoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected DistribucionPK distribucionPK;
	@Basic(optional = false)
	@Column(name = "TOTAL")
	private BigDecimal total;
	@Basic(optional = false)
	@Column(name = "ESTADO")
	private String estado;
	@Column(name = "TOTAL_OBLIGACIONES")
	private BigDecimal totalObligaciones;
	@Column(name = "TOTAL_DISTRIBUCION")
	private BigDecimal totalDistribucion;
	@Column(name = "TOTAL_RETENCIONES")
	private BigDecimal totalRetenciones;
	@JoinColumns({
			@JoinColumn(name = "TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "PERIODO", referencedColumnName = "PERIODO", insertable = false, updatable = false),
			@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private PagoNegocio pagoNegocio;
	@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private Negocio negocio;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "distribucion")
	private List<DistribucionBeneficiario> distribucionBeneficiarios;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "distribucion")
	private List<DistribucionRetenciones> distribucionRetenciones;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "distribucion")
	private List<DistribucionObligacion> distribucionObligaciones;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "distribucion")
	private List<DistribucionRetencionesTitular> distribucionRetencionesTitular;

	public Distribucion() {
	}

	public Distribucion(DistribucionPK distribucionPK) {
		this.distribucionPK = distribucionPK;
	}

	public Distribucion(String codSfc, Integer tipoNegocio, String periodo, Date fechaHora) {
		this.distribucionPK = new DistribucionPK(codSfc, tipoNegocio, periodo, fechaHora);
	}

	public Distribucion(DistribucionPK distribucionPK, BigDecimal total, String estado) {
		this.distribucionPK = distribucionPK;
		this.total = total;
		this.estado = estado;
	}

	public Distribucion(String codSfc, Integer tipoNegocio, String periodo, Date fechaHora, BigDecimal total,
			String estado) {
		this.distribucionPK = new DistribucionPK(codSfc, tipoNegocio, periodo, fechaHora);
		this.total = total;
		this.estado = estado;
	}

	public Distribucion(PagoNegocioPK pagoNegocioPK, Date fechaHora, BigDecimal total, String estado) {
		this.distribucionPK = new DistribucionPK(pagoNegocioPK.getCodSfc(), pagoNegocioPK.getTipoNegocio(),
				pagoNegocioPK.getPeriodo(), fechaHora);
		this.total = total;
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		return Objects.hash(distribucionPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Distribucion other = (Distribucion) obj;
		return Objects.equals(distribucionPK, other.distribucionPK);
	}

	@Override
	public String toString() {
		return "modelos.Distribucion [distribucionPK=" + distribucionPK + "]";
	}

	public DistribucionPK getDistribucionPK() {
		return distribucionPK;
	}

	public void setDistribucionPK(DistribucionPK distribucionPK) {
		this.distribucionPK = distribucionPK;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public BigDecimal getTotalObligaciones() {
		return totalObligaciones;
	}

	public void setTotalObligaciones(BigDecimal totalObligaciones) {
		this.totalObligaciones = totalObligaciones;
	}

	public BigDecimal getTotalDistribucion() {
		return totalDistribucion;
	}

	public void setTotalDistribucion(BigDecimal totalDistribucion) {
		this.totalDistribucion = totalDistribucion;
	}

	public BigDecimal getTotalRetenciones() {
		return totalRetenciones;
	}

	public void setTotalRetenciones(BigDecimal totalRetenciones) {
		this.totalRetenciones = totalRetenciones;
	}

	public PagoNegocio getPagoNegocio() {
		return pagoNegocio;
	}

	public void setPagoNegocio(PagoNegocio pagoNegocio) {
		this.pagoNegocio = pagoNegocio;
	}

	public List<DistribucionBeneficiario> getDistribucionBeneficiarios() {
		return distribucionBeneficiarios;
	}

	public void setDistribucionBeneficiarios(List<DistribucionBeneficiario> distribucionBeneficiarios) {
		this.distribucionBeneficiarios = distribucionBeneficiarios;
	}

	public List<DistribucionRetenciones> getDistribucionRetenciones() {
		return distribucionRetenciones;
	}

	public void setDistribucionRetenciones(List<DistribucionRetenciones> distribucionRetenciones) {
		this.distribucionRetenciones = distribucionRetenciones;
	}

	public List<DistribucionObligacion> getDistribucionObligaciones() {
		return distribucionObligaciones;
	}

	public void setDistribucionObligaciones(List<DistribucionObligacion> distribucionObligaciones) {
		this.distribucionObligaciones = distribucionObligaciones;
	}

	public List<DistribucionRetencionesTitular> getDistribucionRetencionesTitular() {
		return distribucionRetencionesTitular;
	}

	public void setDistribucionRetencionesTitular(List<DistribucionRetencionesTitular> distribucionRetencionesTitular) {
		this.distribucionRetencionesTitular = distribucionRetencionesTitular;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

}
