package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */

//TODO borrar cuando el servicio de autenticación de Accion Fiduciaria este completo
@Entity
@Table(name = "USUARIO_TEMPORAL")
@XmlRootElement

public class UsuarioTemporal implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5704189582450656828L;
	
	@Id
    @Basic(optional = false)
    @Column(name = "LOGIN")
    private String login;
    @Basic(optional = false)
    @Column(name = "ROL")
    private String rol;
    @Basic(optional = false)
    @Column(name = "NOMBRES")
    private String nombres;
    @Basic(optional = false)
    @Column(name = "APELLIDOS")
    private String apellidos;
    @Basic(optional = false)
    @Column(name = "NUMERO_DOCUMENTO")
    private String numeroDocumento;
    @Basic(optional = false)
    @Column(name = "TIPO_DOCUMENTO")
    private String tipoDocumento;

    public UsuarioTemporal() {
    }

    public UsuarioTemporal(String login) {
        this.login = login;
    }

    public UsuarioTemporal(String login, String rol, String nombres, String apellidos, String numeroDocumento, String tipoDocumento) {
        this.login = login;
        this.rol = rol;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumento = tipoDocumento;
    }

    public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

	@Override
	public int hashCode() {
		return Objects.hash(login);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioTemporal other = (UsuarioTemporal) obj;
		return Objects.equals(login, other.login);
	}

	@Override
	public String toString() {
		return "UsuarioTemporal [login=" + login + ", rol=" + rol + ", nombres=" + nombres + ", apellidos=" + apellidos
				+ ", numeroDocumento=" + numeroDocumento + ", tipoDocumento=" + tipoDocumento + "]";
	}
    
}
