package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "CONF_HOMOLOGACION_CAMPOS")
@XmlRootElement
public class HomologacionCampos implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7037526470621915423L;
	@Id
    @Basic(optional = false)
    @Column(name = "NOMBRE_CAMPO_TABLA")
    private String nombreCampoTabla;
    @Basic(optional = false)
    @Column(name = "NOMBRE_HOMOLOGADO")
    private String nombreHomologado;

    public HomologacionCampos() {
    }

    public HomologacionCampos(String nombreCampoTabla) {
        this.nombreCampoTabla = nombreCampoTabla;
    }

    public HomologacionCampos(String nombreCampoTabla, String nombreHomologado) {
        this.nombreCampoTabla = nombreCampoTabla;
        this.nombreHomologado = nombreHomologado;
    }

    public String getNombreCampoTabla() {
        return nombreCampoTabla;
    }

    public void setNombreCampoTabla(String nombreCampoTabla) {
        this.nombreCampoTabla = nombreCampoTabla;
    }

    public String getNombreHomologado() {
        return nombreHomologado;
    }

    public void setNombreHomologado(String nombreHomologado) {
        this.nombreHomologado = nombreHomologado;
    }

	@Override
	public int hashCode() {
		return Objects.hash(nombreCampoTabla);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HomologacionCampos other = (HomologacionCampos) obj;
		return Objects.equals(nombreCampoTabla, other.nombreCampoTabla);
	}

	@Override
	public String toString() {
		return "HomologacionCampos [nombreCampoTabla=" + nombreCampoTabla + ", nombreHomologado=" + nombreHomologado
				+ "]";
	}
}