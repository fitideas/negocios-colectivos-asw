package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "NEG_MIEMBRO_MESA_DIRECTIVA")
public class MiembroMesaDirectiva implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "SEQ_MIEMBRO_MESA_DIRECTIVA")
    @GenericGenerator(name = "SEQ_MIEMBRO_MESA_DIRECTIVA", strategy = "increment")
	@Column(name="ID_MIEMBRO_MESA_DIRECTIVA")
	private Integer miembroMesaDirectivaId;
	@Column(name="NUMERO_DOCUMENTO")
	private String numeroDocumento;
	@Column(name="TIPO_DOCUMENTO")
	private String tipoDocumento;
	@Column(name="FECHA",columnDefinition = "TIMESTAMP")
	private LocalDateTime fecha;
	@Column(name="COD_SFC")
	private String codigoSfc;
	@Column(name="DIRECTOR")
	private boolean directivo;
	@Column(name="EXPOSITOR")
	private boolean expositor;
	@Column(name="ID_ROL")
	private String idRol;
	
	@JoinColumns({
        @JoinColumn(name = "FECHA", referencedColumnName = "FECHA", insertable = false, updatable = false)
        , @JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
	private Reunion reunion;
	
	@JoinColumns({
        @JoinColumn(name = "NUMERO_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
	private Persona persona;
	
	public Integer getMiembroMesaDirectivaId() {
		return miembroMesaDirectivaId;
	}
	public void setMiembroMesaDirectivaId(Integer miembroMesaDirectivaId) {
		this.miembroMesaDirectivaId = miembroMesaDirectivaId;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	public String getCodigoSfc() {
		return codigoSfc;
	}
	public void setCodigoSfc(String codigoSfc) {
		this.codigoSfc = codigoSfc;
	}
	public boolean isDirectivo() {
		return directivo;
	}
	public void setDirectivo(boolean directivo) {
		this.directivo = directivo;
	}
	public boolean isExpositor() {
		return expositor;
	}
	public void setExpositor(boolean expositor) {
		this.expositor = expositor;
	}

	public Reunion getReunion() {
		return reunion;
	}

	public void setReunion(Reunion reunion) {
		this.reunion = reunion;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public String getIdRol() {
		return idRol;
	}
	public void setIdRol(String idRol) {
		this.idRol = idRol;
	}
	
	
	
	
}
