package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class RelacionEntreDominiosPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6521602750443939698L;
	
	@Basic(optional = false)
    @Column(name = "ID_VALOR_DOMINIO")
    private Integer idValorDominio;
    @Basic(optional = false)
    @Column(name = "ID_VALOR_DOMINIO1")
    private Integer idValorDominio2;

    public RelacionEntreDominiosPK() {
    }

    public RelacionEntreDominiosPK(Integer idValorDominio, Integer idValorDominio2) {
        this.idValorDominio = idValorDominio;
        this.idValorDominio2 = idValorDominio2;
    }

    public Integer getIdValorDominio() {
        return idValorDominio;
    }

    public void setIdValorDominio(Integer idValorDominio) {
        this.idValorDominio = idValorDominio;
    }

    public Integer getIdValorDominio2() {
        return idValorDominio2;
    }

    public void setIdValorDominio2(Integer idValorDominio2) {
        this.idValorDominio2 = idValorDominio2;
    }

    @Override
	public int hashCode() {
		return Objects.hash(idValorDominio, idValorDominio2);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelacionEntreDominiosPK other = (RelacionEntreDominiosPK) obj;
		return Objects.equals(idValorDominio, other.idValorDominio)
				&& Objects.equals(idValorDominio2, other.idValorDominio2);
	}

	@Override
    public String toString() {
        return "modelos.RelacionEntreDominiosPK[ idValorDominio=" + idValorDominio + ", idValorDominio2=" + idValorDominio2 + " ]";
    }
    
}
