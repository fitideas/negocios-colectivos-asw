/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Entity
@Table(name = "NEG_NEGOCIO")
@XmlRootElement
public class Negocio extends Auditoria implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "COD_SFC")
	private String codSfc;
	@Basic(optional = false)
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "ESTADO")
	private String estado;
	@Column(name = "ETAPA")
	private String etapa;
	@Column(name = "FECHA_LIQ_NEG_PROM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaLiqNegProm;
	@Column(name = "FECHA_CONSTITUCION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaConstitucion;
	@Column(name = "ESTADO_JURIDICO")
	private String estadoJuridico;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@Column(name = "TOTAL_PARTICIPACIONES")
	private Integer totalParticipaciones;
	@Column(name = "VALOR_ULTIMO_PREDIAL")
	private Float valorUltimoPredial;
	@Column(name = "VALOR_VALORIZACION")
	private Float valorValorizacion;
	@Column(name = "VALOR_PREDIAL")
	private Float valorPredial;
	@Column(name = "FECHA_APERTURA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaApertura;
	@Column(name = "AVALUO_INMUEBLE")
	private Float avaluoInmueble;
	@Column(name = "FECHA_COBRO_VALORIZACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCobroValorizacion;
	@Column(name = "FECHA_COBRO_PREDIAL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCobroPredial;
	@Basic(optional = false)
	@Column(name = "ARCHIVO_CARGADO")
	private Boolean archivoCargado;
	@Basic(optional = false)
	@Column(name = "FECHA_SINCRONIZACION_BUS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaSincronizacionBus;
	@Basic(optional = false)
    @Column(name = "MATRICULA_INMOBILIARIA")
    private String matriculaInmobiliaria;
    @Basic(optional = false)
    @Column(name = "LICENCIA_CONSTRUCCION")
    private String licenciaConstruccion;
    @Basic(optional = false)
    @Column(name = "SECCIONAL_ADUANA_DEPARTAMENTO")
    private String seccionalAduanaDepartamento;
    @Basic(optional = false)
    @Column(name = "SECCIONAL_ADUANA_CIUDAD")
    private String seccionalAduanaCiudad;
    @Basic(optional = false)
    @Column(name = "CIUDAD_UBICACION_PAIS")
    private String ciudadUbicacionPais;
    @Basic(optional = false)
    @Column(name = "CIUDAD_UBICACION_DEPARTAMENTO")
    private String ciudadUbicacionDepartamento;
    @Basic(optional = false)
    @Column(name = "CIUDAD_UBICACION")
    private String ciudadUbicacion;
    @Column(name = "CALIDAD_FIDEICOMITENTE")
    private String calidadFideicomitente;
    @Basic(optional = false)
    @Column(name = "CALIDAD_FIDUCIARIA_FIDEICOMISO")
    private String calidadFiduciariaFideicomiso;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "negocio")
	private List<TipoNegocio> tiposNegocio;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "negocio")
	private List<FechaClave> fechasClave;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "negocio")
	private List<Titular> titulares;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "negocio")
	private List<Encargo> encargos;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "negocio")
	private List<ResponsableNotificacion> responsablesNegocio;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "negocio")
	private ConstantesPago constantesPago;
	@JoinColumns({
        @JoinColumn(name = "FIDEICOMITENTE_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO")
        , @JoinColumn(name = "FIDEICOMITENTE_TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO")})
    @ManyToOne
    private Persona fideicomitente;

	public Negocio() {
	}

	public Negocio(String codSfc) {
		this.codSfc = codSfc;
	}

	public Negocio(String codSfc, String nombre, Boolean archivoCargado) {
		this.codSfc = codSfc;
		this.nombre = nombre;
		this.archivoCargado = archivoCargado;
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public Date getFechaLiqNegProm() {
		return fechaLiqNegProm;
	}

	public void setFechaLiqNegProm(Date fechaLiqNegProm) {
		this.fechaLiqNegProm = fechaLiqNegProm;
	}

	public Date getFechaConstitucion() {
		return fechaConstitucion;
	}

	public void setFechaConstitucion(Date fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}

	public String getEstadoJuridico() {
		return estadoJuridico;
	}

	public void setEstadoJuridico(String estadoJuridico) {
		this.estadoJuridico = estadoJuridico;
	}

	public Integer getTotalParticipaciones() {
		return totalParticipaciones;
	}

	public void setTotalParticipaciones(Integer totalParticipaciones) {
		this.totalParticipaciones = totalParticipaciones;
	}

	public Float getValorUltimoPredial() {
		return valorUltimoPredial;
	}

	public void setValorUltimoPredial(Float valorUltimoPredial) {
		this.valorUltimoPredial = valorUltimoPredial;
	}

	public Float getValorValorizacion() {
		return valorValorizacion;
	}

	public void setValorValorizacion(Float valorValorizacion) {
		this.valorValorizacion = valorValorizacion;
	}

	public Float getValorPredial() {
		return valorPredial;
	}

	public void setValorPredial(Float valorPredial) {
		this.valorPredial = valorPredial;
	}

	public Date getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public Float getAvaluoInmueble() {
		return avaluoInmueble;
	}

	public void setAvaluoInmueble(Float avaluoInmueble) {
		this.avaluoInmueble = avaluoInmueble;
	}

	public Date getFechaCobroValorizacion() {
		return fechaCobroValorizacion;
	}

	public void setFechaCobroValorizacion(Date fechaCobroValorizacion) {
		this.fechaCobroValorizacion = fechaCobroValorizacion;
	}

	public Date getFechaCobroPredial() {
		return fechaCobroPredial;
	}

	public void setFechaCobroPredial(Date fechaCobroPredial) {
		this.fechaCobroPredial = fechaCobroPredial;
	}

	public Boolean getArchivoCargado() {
		return archivoCargado;
	}

	public void setArchivoCargado(Boolean archivoCargado) {
		this.archivoCargado = archivoCargado;
	}

	public Date getFechaSincronizacionBus() {
		return fechaSincronizacionBus;
	}

	public void setFechaSincronizacionBus(Date fechaSincronizacionBus) {
		this.fechaSincronizacionBus = fechaSincronizacionBus;
	}

	@XmlTransient
	public List<TipoNegocio> getTiposNegocio() {
		return tiposNegocio;
	}

	public void setTiposNegocio(List<TipoNegocio> tiposNegocio) {
		this.tiposNegocio = tiposNegocio;
	}

	@XmlTransient
	public List<FechaClave> getFechasClave() {
		return fechasClave;
	}

	public void setFechasClave(List<FechaClave> fechasClave) {
		this.fechasClave = fechasClave;
	}

	@XmlTransient
	public List<Titular> getTitulares() {
		return titulares;
	}

	public void setTitulares(List<Titular> titulares) {
		this.titulares = titulares;
	}

	@XmlTransient
	public List<Encargo> getEncargos() {
		return encargos;
	}

	public void setEncargos(List<Encargo> encargos) {
		this.encargos = encargos;
	}

	@XmlTransient
	public ConstantesPago getContastantesPago() {
		return constantesPago;
	}

	public void setContastantesPago(ConstantesPago constantesPago) {
		this.constantesPago = constantesPago;
	}

	public List<ResponsableNotificacion> getResponsablesNegocio() {
		return responsablesNegocio;
	}

	public void setResponsablesNegocio(List<ResponsableNotificacion> responsablesNegocio) {
		this.responsablesNegocio = responsablesNegocio;
	}

	public String getMatriculaInmobiliaria() {
		return matriculaInmobiliaria;
	}

	public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
		this.matriculaInmobiliaria = matriculaInmobiliaria;
	}

	public String getLicenciaConstruccion() {
		return licenciaConstruccion;
	}

	public void setLicenciaConstruccion(String licenciaConstruccion) {
		this.licenciaConstruccion = licenciaConstruccion;
	}

	public String getSeccionalAduanaDepartamento() {
		return seccionalAduanaDepartamento;
	}

	public void setSeccionalAduanaDepartamento(String seccionalAduanaDepartamento) {
		this.seccionalAduanaDepartamento = seccionalAduanaDepartamento;
	}

	public String getSeccionalAduanaCiudad() {
		return seccionalAduanaCiudad;
	}

	public void setSeccionalAduanaCiudad(String seccionalAduanaCiudad) {
		this.seccionalAduanaCiudad = seccionalAduanaCiudad;
	}

	public String getCiudadUbicacionPais() {
		return ciudadUbicacionPais;
	}

	public void setCiudadUbicacionPais(String ciudadUbicacionPais) {
		this.ciudadUbicacionPais = ciudadUbicacionPais;
	}

	public String getCiudadUbicacionDepartamento() {
		return ciudadUbicacionDepartamento;
	}

	public void setCiudadUbicacionDepartamento(String ciudadUbicacionDepartamento) {
		this.ciudadUbicacionDepartamento = ciudadUbicacionDepartamento;
	}

	public String getCiudadUbicacion() {
		return ciudadUbicacion;
	}

	public void setCiudadUbicacion(String ciudadUbicacion) {
		this.ciudadUbicacion = ciudadUbicacion;
	}

	public String getCalidadFideicomitente() {
		return calidadFideicomitente;
	}

	public void setCalidadFideicomitente(String calidadFideicomitente) {
		this.calidadFideicomitente = calidadFideicomitente;
	}

	public String getCalidadFiduciariaFideicomiso() {
		return calidadFiduciariaFideicomiso;
	}

	public void setCalidadFiduciariaFideicomiso(String calidadFiduciariaFideicomiso) {
		this.calidadFiduciariaFideicomiso = calidadFiduciariaFideicomiso;
	}

	public Persona getFideicomitente() {
		return fideicomitente;
	}

	public void setFideicomitente(Persona persona) {
		this.fideicomitente = persona;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Negocio other = (Negocio) obj;
		return Objects.equals(codSfc, other.codSfc);
	}

	@Override
	public String toString() {
		return "modelos.Negocio[ codSfc=" + codSfc + " ]";
	}

}
