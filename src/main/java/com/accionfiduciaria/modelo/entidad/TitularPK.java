package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class TitularPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2325850927710702265L;
	
	@Basic(optional = false)
    @Column(name = "COD_SFC")
    private String codSfc;
    @Basic(optional = false)
    @Column(name = "TIPO_DOCUMENTO")
    private String tipoDocumento;
    @Basic(optional = false)
    @Column(name = "NUMERO_DOCUMENTO")
    private String numeroDocumento;

    public TitularPK() {
    }

    public TitularPK(String codSfc, String tipocDocumento, String numeroDocumento) {
        this.codSfc = codSfc;
        this.tipoDocumento = tipocDocumento;
        this.numeroDocumento = numeroDocumento;
    }

    public String getCodSfc() {
        return codSfc;
    }

    public void setCodSfc(String codSfc) {
        this.codSfc = codSfc;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipocDocumento) {
        this.tipoDocumento = tipocDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, numeroDocumento, tipoDocumento);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitularPK other = (TitularPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(numeroDocumento, other.numeroDocumento)
				&& Objects.equals(tipoDocumento, other.tipoDocumento);
	}

	@Override
	public String toString() {
		return "TitularPK [codSfc=" + codSfc + ", tipoDocumento=" + tipoDocumento + ", numeroDocumento="
				+ numeroDocumento + "]";
	}
}
