
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AUD_HISTORIAL")
public class AudHistorial implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	AudHistorialPK audHistorialPK;

	@Column(name = "LLAVE")
	private String llave;

	@Column(name = "USUARIO")
	private String usuario;

	@Column(name = "TIPO_DOCUMENTO_USUARIO")
	private String tipoDocumentoUsuario;

	@Column(name = "NUMERO_DOCUMENTO_USUARIO")
	private String numeroDocumentoUsuario;

	@Column(name = "IP")
	private String ip;

	@Column(name = "ROL")
	private String rol;
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy= "audHistorial", fetch =FetchType.LAZY, orphanRemoval = false )
	private List<AudHistorialDetalle> historialDetalles;
	 
	public AudHistorial() {
		super();
	}

	public AudHistorialPK getAudHistorialPK() {
		return audHistorialPK;
	}

	public void setAudHistorialPK(AudHistorialPK audHistorialPK) {
		this.audHistorialPK = audHistorialPK;
	}

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTipoDocumentoUsuario() {
		return tipoDocumentoUsuario;
	}

	public void setTipoDocumentoUsuario(String tipoDocumentoUsuario) {
		this.tipoDocumentoUsuario = tipoDocumentoUsuario;
	}

	public String getNumeroDocumentoUsuario() {
		return numeroDocumentoUsuario;
	}

	public void setNumeroDocumentoUsuario(String numeroDocumentoUsuario) {
		this.numeroDocumentoUsuario = numeroDocumentoUsuario;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public List<AudHistorialDetalle> getHistorialDetalles() {
		return historialDetalles;
	}

	public void setHistorialDetalles(List<AudHistorialDetalle> historialDetalles) {
		this.historialDetalles = historialDetalles;
	}

	@Override
	public int hashCode() {
		return Objects.hash(audHistorialPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AudHistorial other = (AudHistorial) obj;
		return Objects.equals(audHistorialPK, other.audHistorialPK);
	}

	@Override
	public String toString() {
		return "AudHistorial [audHistorialPK=" + audHistorialPK + ", llave=" + llave + ", usuario=" + usuario
				+ ", tipoDocumentoUsuario=" + tipoDocumentoUsuario + ", numeroDocumentoUsuario="
				+ numeroDocumentoUsuario + ", ip=" + ip + ", rol=" + rol + ", historialDetalles=" + historialDetalles
				+ "]";
	}
}
