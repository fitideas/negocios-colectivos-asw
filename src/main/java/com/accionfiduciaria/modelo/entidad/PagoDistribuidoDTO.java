package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;

public class PagoDistribuidoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String periodo;
	private String tipoNegocio;
	private String encargoDebitado;
	private BigDecimal valorIngreso;
	private BigDecimal valorDistribuido;
	
	public PagoDistribuidoDTO() {
		
	}
	
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getTipoNegocio() {
		return tipoNegocio;
	}
	public void setTipoNegocio(String tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}
	public String getEncargoDebitado() {
		return encargoDebitado;
	}
	public void setEncargoDebitado(String encargoDebitado) {
		this.encargoDebitado = encargoDebitado;
	}
	public BigDecimal getValorIngreso() {
		return valorIngreso;
	}
	public void setValorIngreso(BigDecimal valorIngreso) {
		this.valorIngreso = valorIngreso;
	}
	public BigDecimal getValorDistribuido() {
		return valorDistribuido;
	}
	public void setValorDistribuido(BigDecimal valorDistribuido) {
		this.valorDistribuido = valorDistribuido;
	}
	
	
	
	

	
}
