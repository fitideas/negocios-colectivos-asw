package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_TIPO_FIDEICOMISO")
@XmlRootElement
public class TipoFideicomiso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoFideicomiso")
    private List<SubtipoFideicomiso> subtiposFideicomiso;

    public TipoFideicomiso() {
    }

    public TipoFideicomiso(String codigo) {
        this.codigo = codigo;
    }

    public TipoFideicomiso(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<SubtipoFideicomiso> getSubtiposFideicomiso() {
        return subtiposFideicomiso;
    }

    public void setSubtiposFideicomiso(List<SubtipoFideicomiso> subtiposFideicomiso) {
        this.subtiposFideicomiso = subtiposFideicomiso;
    }

	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoFideicomiso other = (TipoFideicomiso) obj;
		return Objects.equals(codigo, other.codigo);
	}

	@Override
	public String toString() {
		return "TipoFideicomiso [codigo=" + codigo + ", nombre=" + nombre + "]";
	}
}
