package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class RolPermisoPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4758712146896849906L;
	
	@Basic(optional = false)
    @Column(name = "IDROL")
    private Integer idRol;
    @Basic(optional = false)
    @Column(name = "IDPERMISO")
    private Integer idPermiso;

    public RolPermisoPK() {
    }

    public RolPermisoPK(Integer idRol, Integer idpermiso) {
        this.idRol = idRol;
        this.idPermiso = idpermiso;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public Integer getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(Integer idPermiso) {
        this.idPermiso = idPermiso;
    }
    
	@Override
	public int hashCode() {
		return Objects.hash(idPermiso, idRol);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolPermisoPK other = (RolPermisoPK) obj;
		return Objects.equals(idPermiso, other.idPermiso) && Objects.equals(idRol, other.idRol);
	}

	@Override
	public String toString() {
		return "RolPermisoPK [idRol=" + idRol + ", idPermiso=" + idPermiso + "]";
	}
    
}
