package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Embeddable
public class TipoNegocioPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8374262715003218446L;
	
	@Basic(optional = false)
    @Column(name = "TIPO_NEGOCIO")
    private Integer tipoNegocio;
    @Basic(optional = false)
    @Column(name = "COD_SFC")
    private String codSfc;

    public TipoNegocioPK() {
    }

    public TipoNegocioPK(Integer tipoNegocio, String codSfc) {
        this.tipoNegocio = tipoNegocio;
        this.codSfc = codSfc;
    }

    public Integer getTipoNegocio() {
        return tipoNegocio;
    }

    public void setTipoNegocio(Integer tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

    public String getCodSfc() {
        return codSfc;
    }

    public void setCodSfc(String codSfc) {
        this.codSfc = codSfc;
    }

    @Override
	public int hashCode() {
		return Objects.hash(codSfc, tipoNegocio);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoNegocioPK other = (TipoNegocioPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(tipoNegocio, other.tipoNegocio);
	}

	@Override
    public String toString() {
        return "modelos.TipoNegocioPK[ tipoNegocio=" + tipoNegocio + ", codSfc=" + codSfc + " ]";
    }
    
}
