package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_VALOR_DOMINIO_NEGOCIO")
@XmlRootElement
@DynamicUpdate
public class ValorDominioNegocio extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "SEQ_VALOR_DOMINIO")
    @GenericGenerator(name = "SEQ_VALOR_DOMINIO", strategy = "increment")
    @Column(name = "ID_VALOR_DOMINIO")
    private Integer idValorDominio;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "VALOR")
    private String valor;
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private Boolean activo;
    @Column(name = "UNIDAD")
    private String unidad;
    @JoinColumn(name = "COD_DOMINIO", referencedColumnName = "COD_DOMINIO")
    @ManyToOne(optional = false)
    private Dominio dominio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "valorDominioNegocio", fetch = FetchType.LAZY)
    private List<RelacionEntreDominios> relacionEntreDominiosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "valorDominioNegocio1", fetch = FetchType.LAZY)
    private List<RelacionEntreDominios> relacionEntreDominiosList1;

    public ValorDominioNegocio() {
    }

    public ValorDominioNegocio(Integer idValorDominio) {
        this.idValorDominio = idValorDominio;
    }

    public ValorDominioNegocio(Integer idValorDominio, String valor, Boolean activo) {
        this.idValorDominio = idValorDominio;
        this.valor = valor;
        this.activo = activo;
    }

    public Integer getIdValorDominio() {
        return idValorDominio;
    }

    public void setIdValorDominio(Integer idValorDominio) {
        this.idValorDominio = idValorDominio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public Dominio getDominio() {
        return dominio;
    }

    public void setDominio(Dominio dominio) {
        this.dominio = dominio;
    }

    @XmlTransient
    public List<RelacionEntreDominios> getRelacionEntreDominiosList() {
        return relacionEntreDominiosList;
    }

    public void setRelacionEntreDominiosList(List<RelacionEntreDominios> relacionEntreDominiosList) {
        this.relacionEntreDominiosList = relacionEntreDominiosList;
    }

    @XmlTransient
    public List<RelacionEntreDominios> getRelacionEntreDominiosList1() {
        return relacionEntreDominiosList1;
    }

    public void setRelacionEntreDominiosList1(List<RelacionEntreDominios> relacionEntreDominiosList1) {
        this.relacionEntreDominiosList1 = relacionEntreDominiosList1;
    }

    @Override
	public int hashCode() {
		return Objects.hash(idValorDominio);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValorDominioNegocio other = (ValorDominioNegocio) obj;
		return Objects.equals(idValorDominio, other.idValorDominio);
	}

	@Override
    public String toString() {
        return "modelos.ValorDominioNegocio[ idValorDominio=" + idValorDominio + " ]";
    }
    
}
