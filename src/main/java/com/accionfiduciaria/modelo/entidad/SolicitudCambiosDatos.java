package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "BEN_SOLICITUD_CAMBIOS_DATOS")
@XmlRootElement
public class SolicitudCambiosDatos implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_SOLICITUD")
    @GeneratedValue(generator = "SEQ_SOLICITUD")
    @GenericGenerator(name = "SEQ_SOLICITUD", strategy = "increment")
    private Integer idSolicitud;
    @Basic(optional = false)
    @Column(name = "CAMPO_MODIFICADO")
    private String campoModificado;
    @Basic(optional = false)
    @Column(name = "FECHA_SOLICITUD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSolicitud;
    @Basic(optional = false)
    @Column(name = "DATOS_ANTERIOR")
    private String datoAnterior;
    @Basic(optional = false)
    @Column(name = "DATO_NUEVO")
    private String datoNuevo;
    @Basic(optional = false)
    @Column(name = "NUMERO_RADICADO")
    private String numeroRadicado;
    @JoinColumns({
        @JoinColumn(name = "NUMERO_DOCUMENTO_USUARIO", referencedColumnName = "NUMERO_DOCUMENTO")
        , @JoinColumn(name = "TIPO_DOCUMENTO_USUARIO", referencedColumnName = "TIPO_DOCUMENTO")})
    @ManyToOne(optional = false)
    private Persona personaUsuario;
    @JoinColumns({
        @JoinColumn(name = "NUMERO_DOCUMENTO_VINCULADO", referencedColumnName = "NUMERO_DOCUMENTO")
        , @JoinColumn(name = "TIPO_DOCUMENTO_VINCULADO", referencedColumnName = "TIPO_DOCUMENTO")})
    @ManyToOne(optional = false)
    private Persona personaVinculado;

    public SolicitudCambiosDatos() {
    }

    public SolicitudCambiosDatos(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public SolicitudCambiosDatos(Integer idSolicitud, String campoModificado, Date fechaSolicitud, String datoAnterior, String datoNuevo, String numeroRadicado) {
        this.idSolicitud = idSolicitud;
        this.campoModificado = campoModificado;
        this.fechaSolicitud = fechaSolicitud;
        this.datoAnterior = datoAnterior;
        this.datoNuevo = datoNuevo;
        this.numeroRadicado = numeroRadicado;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getCampoModificado() {
        return campoModificado;
    }

    public void setCampoModificado(String campoModificado) {
        this.campoModificado = campoModificado;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getDatoAnterior() {
        return datoAnterior;
    }

    public void setDatoAnterior(String datoAnterior) {
        this.datoAnterior = datoAnterior;
    }

    public String getDatoNuevo() {
        return datoNuevo;
    }

    public void setDatoNuevo(String datoNuevo) {
        this.datoNuevo = datoNuevo;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public Persona getPersonaUsuario() {
        return personaUsuario;
    }

    public void setPersonaUsuario(Persona personaUsuario) {
        this.personaUsuario = personaUsuario;
    }

    public Persona getPersonaVinculado() {
        return personaVinculado;
    }

    public void setPersonaVinculado(Persona personaVinculado) {
        this.personaVinculado = personaVinculado;
    }

    @Override
	public int hashCode() {
		return Objects.hash(idSolicitud);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitudCambiosDatos other = (SolicitudCambiosDatos) obj;
		return Objects.equals(idSolicitud, other.idSolicitud);
	}

	@Override
    public String toString() {
        return "modelos.BenSolicitudCambiosDatos[ idSolicitud=" + idSolicitud + " ]";
    }
    
}
