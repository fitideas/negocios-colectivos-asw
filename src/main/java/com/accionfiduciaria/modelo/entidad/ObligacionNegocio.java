package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_OBLIGACION_NEGOCIO")
@XmlRootElement
public class ObligacionNegocio extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected ObligacionNegocioPK obligacionNegocioPK;    
    @Basic(optional = false)
    @Column(name = "PERIODICIDAD")
    private String periodicidad;
    @Column(name = "DIA_PAGO")
    private Integer diaPago;
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private Boolean activo;
    @Basic(optional = false)
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Basic(optional = false)
    @Column(name = "UNIDAD")
    private String unidad;
    @Column(name = "TERCERO_NUMERO_DOCUMENTO")
    private String terceroNumeroDocumento;
    @Column(name = "TERCERO_TIPO_DOCUMENTO")
    private Integer terceroTipoDocumento;
    @Column(name = "FECHA_VENCIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimiento;    
    @Column(name = "NUMERO_RADICADO")
    private String numeroRadicado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "obligacionNegocio")
    private List<DistribucionObligacion> distribucionObligaciones;
    @JoinColumns({
        @JoinColumn(name = "TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false)
        , @JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private TipoNegocio tipoNegocio;

    public ObligacionNegocio() {
    }

    public ObligacionNegocio(Integer tipoPago, Integer tipoNegocio, String codSfc, String nombreGasto) {
    	this.obligacionNegocioPK = new ObligacionNegocioPK(tipoPago, tipoNegocio, codSfc, nombreGasto);
    }
    
    public ObligacionNegocio(ObligacionNegocioPK obligacionNegocioPK) {
    	this.obligacionNegocioPK = obligacionNegocioPK;
    }

    public ObligacionNegocioPK getObligacionNegocioPK() {
		return obligacionNegocioPK;
	}

	public void setObligacionNegocioPK(ObligacionNegocioPK obligacionNegocioPK) {
		this.obligacionNegocioPK = obligacionNegocioPK;
	}
	
    public String getPeriodicidad() {
        return periodicidad;
    }

    public void setPeriodicidad(String periodicidad) {
        this.periodicidad = periodicidad;
    }

    public Integer getDiaPago() {
        return diaPago;
    }

    public void setDiaPago(Integer diaPago) {
        this.diaPago = diaPago;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public TipoNegocio getTipoNegocio() {
        return tipoNegocio;
    }

    public void setTipoNegocio(TipoNegocio tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

	public String getTerceroNumeroDocumento() {
		return terceroNumeroDocumento;
	}

	public void setTerceroNumeroDocumento(String terceroNumeroDocumento) {
		this.terceroNumeroDocumento = terceroNumeroDocumento;
	}

	public Integer getTerceroTipoDocumento() {
		return terceroTipoDocumento;
	}

	public void setTerceroTipoDocumento(Integer terceroTipoDocumento) {
		this.terceroTipoDocumento = terceroTipoDocumento;
	}

	public List<DistribucionObligacion> getDistribucionObligaciones() {
		return distribucionObligaciones;
	}

	public void setDistribucionObligaciones(List<DistribucionObligacion> distribucionObligaciones) {
		this.distribucionObligaciones = distribucionObligaciones;
	}

	@Override
	public int hashCode() {
		return Objects.hash(obligacionNegocioPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObligacionNegocio other = (ObligacionNegocio) obj;
		return Objects.equals(obligacionNegocioPK, other.obligacionNegocioPK);
	}

	@Override
	public String toString() {
		return "ObligacionNegocio [obligacionNegocioPK=" + obligacionNegocioPK + "]";
	}
}
