package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;

public class InformacionGeneralPagosDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int totalRegistros;
	private List<PagoDistribuidoDTO> historialPagos;
	
	public int getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	public List<PagoDistribuidoDTO> getHistorialPagos() {
		return historialPagos;
	}
	public void setHistorialPagos(List<PagoDistribuidoDTO> historialPagos) {
		this.historialPagos = historialPagos;
	}
	
}
