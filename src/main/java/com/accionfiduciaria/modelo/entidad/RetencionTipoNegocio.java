package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_RETENCION_TIPO_NEGOCIO")
@XmlRootElement
public class RetencionTipoNegocio extends Auditoria implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected RetencionTipoNegocioPK retencionTipoNegocioPK;
	@Column(name = "ACTIVO")
	private Boolean activo;
	@JoinColumns({
			@JoinColumn(name = "TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private TipoNegocio tipoNegocio;
	@JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioRetencion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "retencionTipoNegocio")
	private List<DistribucionRetenciones> distribucionRetenciones;

	public RetencionTipoNegocio() {
	}

	public RetencionTipoNegocio(RetencionTipoNegocioPK retencionTipoNegocioPK) {
		this.retencionTipoNegocioPK = retencionTipoNegocioPK;
	}

	public RetencionTipoNegocio(Integer retencion, Integer tipoNegocio, String codSfc) {
		this.retencionTipoNegocioPK = new RetencionTipoNegocioPK(retencion, tipoNegocio, codSfc);
	}

	public RetencionTipoNegocio(Integer retencion, Integer tipoNegocio, String codSfc, Boolean activo) {
		this.retencionTipoNegocioPK = new RetencionTipoNegocioPK(retencion, tipoNegocio, codSfc);
		this.activo = activo;
	}

	public RetencionTipoNegocioPK getRetencionTipoNegocioPK() {
		return retencionTipoNegocioPK;
	}

	public void setRetencionTipoNegocioPK(RetencionTipoNegocioPK retencionTipoNegocioPK) {
		this.retencionTipoNegocioPK = retencionTipoNegocioPK;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public TipoNegocio getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(TipoNegocio tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public ValorDominioNegocio getDominioRetencion() {
		return dominioRetencion;
	}

	public void setDominioRetencion(ValorDominioNegocio dominioRetencion) {
		this.dominioRetencion = dominioRetencion;
	}

	public List<DistribucionRetenciones> getDistribucionRetenciones() {
		return distribucionRetenciones;
	}

	public void setDistribucionRetenciones(List<DistribucionRetenciones> distribucionRetenciones) {
		this.distribucionRetenciones = distribucionRetenciones;
	}

	@Override
	public int hashCode() {
		return Objects.hash(retencionTipoNegocioPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetencionTipoNegocio other = (RetencionTipoNegocio) obj;
		return Objects.equals(retencionTipoNegocioPK, other.retencionTipoNegocioPK);
	}

	@Override
	public String toString() {
		return "RetencionTipoNegocio [retencionTipoNegocioPK=" + retencionTipoNegocioPK + "]";
	}
}
