/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "AUT_USUARIO")
@XmlRootElement
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LOGIN")
    private String login;
    @Basic(optional = false)
    @Column(name = "FECHA_ULTIMO_ACCESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimoAcceso;
    @Basic(optional = false)
    @Column(name = "TOKEN")
    private String token;
    @Basic(optional = false)
    @Column(name = "REFRESH_TOKEN")
    private String refreshToken;
    @Column(name = "ULTIMA_IP")
    private String ultimaIP;
    @Column(name = "ULTIMO_ROL")
    private String ultimoRol;
    @JoinColumns({
        @JoinColumn(name = "NUMERO_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO")
        , @JoinColumn(name = "TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO")})
    @OneToOne(optional = false)
    private Persona persona;

    public Usuario() {
    }

    public Usuario(String login) {
        this.login = login;
    }

    public Usuario(String login, Date fechaUltimoAcceso, String token, String refreshToken) {
        this.login = login;
        this.fechaUltimoAcceso = fechaUltimoAcceso;
        this.token = token;
        this.refreshToken = refreshToken;
    }    
    
    /**
	 * @param login
	 * @param fechaUltimoAcceso
	 * @param token
	 * @param refreshToken
	 * @param ultimaIP
	 * @param ultimoRol
	 */
	public Usuario(String login, Date fechaUltimoAcceso, String token, String refreshToken, String ultimaIP,
			String ultimoRol) {
		this.login = login;
		this.fechaUltimoAcceso = fechaUltimoAcceso;
		this.token = token;
		this.refreshToken = refreshToken;
		this.ultimaIP = ultimaIP;
		this.ultimoRol = ultimoRol;
	}

	public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getFechaUltimoAcceso() {
        return fechaUltimoAcceso;
    }

    public void setFechaUltimoAcceso(Date fechaUltimoAcceso) {
        this.fechaUltimoAcceso = fechaUltimoAcceso;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getUltimaIP() {
		return ultimaIP;
	}

	public void setUltimaIP(String ultimaIP) {
		this.ultimaIP = ultimaIP;
	}

	public String getUltimoRol() {
		return ultimoRol;
	}

	public void setUltimoRol(String ultimoRol) {
		this.ultimoRol = ultimoRol;
	}

	@Override
	public int hashCode() {
		return Objects.hash(login);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		return Objects.equals(login, other.login);
	}

	@Override
    public String toString() {
        return "modelos.AutUsuario[ login=" + login + " ]";
    }
    
}
