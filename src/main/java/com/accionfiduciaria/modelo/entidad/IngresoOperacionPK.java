/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author efarias
 *
 */
@Embeddable
public class IngresoOperacionPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "PERIODO")
	private String periodo;

	@Basic(optional = false)
	@Column(name = "TIPO_NEGOCIO")
	private Integer tipoNegocio;

	@Basic(optional = false)
	@Column(name = "COD_SFC")
	private String codSfc;

	@Basic(optional = false)
	@Column(name = "RADICADO")
	private String radicado;

	public IngresoOperacionPK() {

	}

	/**
	 * @param periodo
	 * @param tipoNegocio
	 * @param codSfc
	 * @param radicado
	 */
	public IngresoOperacionPK(String periodo, Integer tipoNegocio, String codSfc, String radicado) {
		this.periodo = periodo;
		this.tipoNegocio = tipoNegocio;
		this.codSfc = codSfc;
		this.radicado = radicado;
	}

	@Override
	public String toString() {
		return "IngresoOperacionPK [periodo=" + periodo + ", tipoNegocio=" + tipoNegocio + ", codSfc=" + codSfc
				+ ", radicado=" + radicado + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, periodo, tipoNegocio, radicado);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		IngresoOperacionPK other = (IngresoOperacionPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(periodo, other.periodo)
				&& Objects.equals(tipoNegocio, other.tipoNegocio) && Objects.equals(radicado, other.radicado);

	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Integer getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(Integer tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public String getRadicado() {
		return radicado;
	}

	public void setRadicado(String radicado) {
		this.radicado = radicado;
	}

}
