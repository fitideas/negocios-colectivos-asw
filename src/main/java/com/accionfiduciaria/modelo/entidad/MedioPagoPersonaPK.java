package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class MedioPagoPersonaPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -214482024381936813L;
	@Basic(optional = false)
    @Column(name = "CUENTA")
    private String cuenta;
    @Basic(optional = false)
    @Column(name = "NUMERO_DOCUMENTO")
    private String numeroDocumento;
    @Basic(optional = false)
    @Column(name = "TIPO_DOCUMENTO")
    private String tipoDocumento;
    @Basic(optional = false)
    @Column(name = "BANCO")
    private Integer banco;
    @Basic(optional = false)
    @Column(name = "TIPO_CUENTA")
    private Integer tipoCuenta;

    public MedioPagoPersonaPK() {
    }

    public MedioPagoPersonaPK(String cuenta, String numeroDocumento, String tipoDocumento, Integer banco, Integer tipoCuenta) {
        this.cuenta = cuenta;
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumento = tipoDocumento;
        this.banco = banco;
        this.tipoCuenta = tipoCuenta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getBanco() {
        return banco;
    }

    public void setBanco(Integer banco) {
        this.banco = banco;
    }

    public Integer getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(Integer tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    

    @Override
	public int hashCode() {
		return Objects.hash(banco, cuenta, numeroDocumento, tipoCuenta, tipoDocumento);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedioPagoPersonaPK other = (MedioPagoPersonaPK) obj;
		return Objects.equals(banco, other.banco) && Objects.equals(cuenta, other.cuenta)
				&& Objects.equals(numeroDocumento, other.numeroDocumento)
				&& Objects.equals(tipoCuenta, other.tipoCuenta) && Objects.equals(tipoDocumento, other.tipoDocumento);
	}

	@Override
    public String toString() {
        return "modelos.NegMedioPagoPersonaPK[ cuenta=" + cuenta + ", numeroDocumento=" + numeroDocumento + ", tipoDocumento=" + tipoDocumento + ", banco=" + banco + ", tipoCuenta=" + tipoCuenta + " ]";
    }
    
}
