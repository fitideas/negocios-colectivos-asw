/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_DISTRI_RETENCIONES_TITULAR")
@XmlRootElement
public class DistribucionRetencionesTitular implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DistribucionRetencionesTitularPK distribucionRetencionesTitularPK;
	@Basic(optional = false)
	@Column(name = "UNIDAD")
	private String unidad;
	@Basic(optional = false)
	@Column(name = "VALOR_CONFIGURADO_IMPUESTO")
	private BigDecimal valorConfiguradoImpuesto;
	@Basic(optional = false)
	@Column(name = "VALOR_BASE")
	private BigDecimal valorBase;
	@Basic(optional = false)
	@Column(name = "VALOR_IMPUESTO")
	private BigDecimal valorImpuesto;
	@JoinColumns({
        @JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_RETENCION", insertable = false, updatable = false)
        , @JoinColumn(name = "TITULAR_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
        , @JoinColumn(name = "TITULAR_NUMERO_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TITULAR_TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TITULAR_TIPO_NAT_NEG", referencedColumnName = "TIPO_NATURALEZA_NEGOCIO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
	private RetencionTitular retencionTitular;
	@JoinColumns({
			@JoinColumn(name = "NEG_DIST_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "PERIODO", referencedColumnName = "PERIODO", insertable = false, updatable = false),
			@JoinColumn(name = "FECHA_HORA", referencedColumnName = "FECHA_HORA", insertable = false, updatable = false), })
	@ManyToOne(optional = false)
	private Distribucion distribucion;
	@JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioRetencion;

	public DistribucionRetencionesTitular() {

	}

	public DistribucionRetencionesTitular(RetencionTitularPK retencionTitularPK, DistribucionPK distribucionPK) {
		this.distribucionRetencionesTitularPK = new DistribucionRetencionesTitularPK(retencionTitularPK,
				distribucionPK);
	}

	public DistribucionRetencionesTitular(RetencionTitularPK retencionTitularPK, DistribucionPK distribucionPK,
			String unidad, BigDecimal valorConfiguradoImpuesto, BigDecimal valorBase, BigDecimal valorImpuesto) {
		this.distribucionRetencionesTitularPK = new DistribucionRetencionesTitularPK(retencionTitularPK,
				distribucionPK);
		this.unidad = unidad;
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
		this.valorBase = valorBase;
		this.valorImpuesto = valorImpuesto;
	}

	public DistribucionRetencionesTitular(DistribucionRetencionesTitularPK distribucionRetencionesTitularPK,
			String unidad, BigDecimal valorConfiguradoImpuesto, BigDecimal valorBase, BigDecimal valorImpuesto) {
		this.distribucionRetencionesTitularPK = distribucionRetencionesTitularPK;
		this.unidad = unidad;
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
		this.valorBase = valorBase;
		this.valorImpuesto = valorImpuesto;
	}

	public DistribucionRetencionesTitularPK getDistribucionRetencionesTitularPK() {
		return distribucionRetencionesTitularPK;
	}

	public void setDistribucionRetencionesTitularPK(DistribucionRetencionesTitularPK distribucionRetencionesTitularPK) {
		this.distribucionRetencionesTitularPK = distribucionRetencionesTitularPK;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public BigDecimal getValorConfiguradoImpuesto() {
		return valorConfiguradoImpuesto;
	}

	public void setValorConfiguradoImpuesto(BigDecimal valorConfiguradoImpuesto) {
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public BigDecimal getValorImpuesto() {
		return valorImpuesto;
	}

	public void setValorImpuesto(BigDecimal valorImpuesto) {
		this.valorImpuesto = valorImpuesto;
	}

	public RetencionTitular getRetencionTitular() {
		return retencionTitular;
	}

	public Distribucion getDistribucion() {
		return distribucion;
	}

	public ValorDominioNegocio getDominioRetencion() {
		return dominioRetencion;
	}

}
