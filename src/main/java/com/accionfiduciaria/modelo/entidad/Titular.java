package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author José Polo - jpolo@asesoftware.com
 *
 */
@Entity
@Table(name = "NEG_TITULAR")
@XmlRootElement
public class Titular extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TitularPK titularPK;
    @Basic(optional = false)
    @Column(name = "PORCENTAJE")
    private BigDecimal porcentaje;
    @Column(name = "ASESOR")
    private String asesor;
    @Column(name = "RADICADO")
    private String radicado;
    @Basic(optional = false)
    @Column(name = "COTITULAR")
    private Boolean esCotitular;
    @JoinColumns({
        @JoinColumn(name = "NUMERO_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Persona persona;
    @JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Negocio negocio;
    @OneToMany(mappedBy = "titular", fetch = FetchType.LAZY)
    private List<RetencionTitular> retencionTitularList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "titular", fetch = FetchType.LAZY)
    private List<Beneficiario> beneficiarios;
    @JoinColumn(name = "ID_PARTICIPACION", referencedColumnName = "ID_PARTICIPACION")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ParticipacionesNegocio participacion;
    @Basic(optional = false)
    @Column(name = "NUMERO_DERECHOS")
    private Integer numeroDerechos;
    @Column(name = "ACTIVO")
    private Boolean activo;
    @Column(name = "ID_TIPNATJUR")
    private Integer idTipoNaturalezaJuridica;
    
    public Titular() {
    }

    public Titular(TitularPK titularPK) {
        this.titularPK = titularPK;
    }

    public Titular(TitularPK titularPK, BigDecimal porcentaje) {
        this.titularPK = titularPK;
        this.porcentaje = porcentaje;
    }

    public Titular(String codSfc, String tipocDocumento, String numeroDocumento) {
        this.titularPK = new TitularPK(codSfc, tipocDocumento, numeroDocumento);
    }

    public TitularPK getTitularPK() {
        return titularPK;
    }

    public void setTitularPK(TitularPK titularPK) {
        this.titularPK = titularPK;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }

    /**
	 * @return the esCotitular
	 */
	public Boolean getEsCotitular() {
		return esCotitular;
	}

	/**
	 * @param esCotitular the esCotitular to set
	 */
	public void setEsCotitular(Boolean esCotitular) {
		this.esCotitular = esCotitular;
	}
	
	public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Negocio getNegocio() {
        return negocio;
    }

    public void setNegocio(Negocio negocio) {
        this.negocio = negocio;
    }

    /**
	 * @return the asesor
	 */
	public String getAsesor() {
		return asesor;
	}

	/**
	 * @param asesor the asesor to set
	 */
	public void setAsesor(String asesor) {
		this.asesor = asesor;
	}

	/**
	 * @return the radicado
	 */
	public String getRadicado() {
		return radicado;
	}

	/**
	 * @param radicado the radicado to set
	 */
	public void setRadicado(String radicado) {
		this.radicado = radicado;
	}

	@XmlTransient
    public List<RetencionTitular> getRetencionTitularList() {
        return retencionTitularList;
    }

    public void setRetencionTitularList(List<RetencionTitular> retencionTitularList) {
        this.retencionTitularList = retencionTitularList;
    }

    /**
	 * @return the beneficiarios
	 */
	public List<Beneficiario> getBeneficiarios() {
		return beneficiarios;
	}

	/**
	 * @param beneficiarios the beneficiarios to set
	 */
	public void setBeneficiarios(List<Beneficiario> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
	
	/**
	 * @return the participacion
	 */
	public ParticipacionesNegocio getParticipacion() {
		return participacion;
	}

	/**
	 * @param participacion the participacion to set
	 */
	public void setParticipacion(ParticipacionesNegocio participacion) {
		this.participacion = participacion;
	}

	/**
	 * @return the numeroDerechos
	 */
	public Integer getNumeroDerechos() {
		return numeroDerechos;
	}

	/**
	 * @param numeroDerechos the numeroDerechos to set
	 */
	public void setNumeroDerechos(Integer numeroDerechos) {
		this.numeroDerechos = numeroDerechos;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getIdTipoNaturalezaJuridica() {
		return idTipoNaturalezaJuridica;
	}

	public void setIdTipoNaturalezaJuridica(Integer idTipoNaturalezaJuridica) {
		this.idTipoNaturalezaJuridica = idTipoNaturalezaJuridica;
	}

	@Override
	public int hashCode() {
		return Objects.hash(titularPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Titular other = (Titular) obj;
		return Objects.equals(titularPK, other.titularPK);
	}

	@Override
    public String toString() {
        return "modelos.NegTitular[ negTitularPK=" + titularPK + " ]";
    }
    
}
