package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "GEN_PERSONA")
@XmlRootElement
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PersonaPK personaPK;
    @Basic(optional = false)
    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "persona")
    private Usuario usuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaUsuario", fetch = FetchType.LAZY)
    private List<SolicitudCambiosDatos> solicitudCambiosDatosUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaVinculado", fetch = FetchType.LAZY)
    private List<SolicitudCambiosDatos> solicitudCambiosDatosVinculado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona", fetch = FetchType.LAZY)
    private List<Titular> titulares;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona", fetch = FetchType.LAZY)
    private List<MedioPagoPersona> mediosPagoPersona;
    @OneToMany(mappedBy = "fideicomitente")
    private List<Negocio> negocios;

    public Persona() {
    }

    public Persona(PersonaPK genPersonaPK) {
        this.personaPK = genPersonaPK;
    }

    public Persona(PersonaPK genPersonaPK, String nombreCompleto) {
        this.personaPK = genPersonaPK;
        this.nombreCompleto = nombreCompleto;
    }

    public Persona(String tipocDocumento, String numeroDocumento) {
        this.personaPK = new PersonaPK(tipocDocumento, numeroDocumento);
    }

    public Persona(String tipoDocumento, String numeroDocumento, String nombreCompleto) {
    	this.personaPK = new PersonaPK(tipoDocumento, numeroDocumento);
    	this.nombreCompleto = nombreCompleto;
	}

	public PersonaPK getPersonaPK() {
        return personaPK;
    }

    public void setPersonaPK(PersonaPK personaPK) {
        this.personaPK = personaPK;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @XmlTransient
    public List<SolicitudCambiosDatos> getSolicitudCambiosDatosUsuario() {
        return solicitudCambiosDatosUsuario;
    }

    public void setSolicitudCambiosDatosUsuario(List<SolicitudCambiosDatos> solicitudCambiosDatosUsuario) {
        this.solicitudCambiosDatosUsuario = solicitudCambiosDatosUsuario;
    }

    @XmlTransient
    public List<SolicitudCambiosDatos> getSolicitudCambiosDatosVinculado() {
        return solicitudCambiosDatosVinculado;
    }

    public void setSolicitudCambiosDatosVinculado(List<SolicitudCambiosDatos> solicitudCambiosDatosVinculado) {
        this.solicitudCambiosDatosVinculado = solicitudCambiosDatosVinculado;
    }
    
    /**
	 * @return the titulares
	 */
	public List<Titular> getTitulares() {
		return titulares;
	}

	/**
	 * @param titulares the titulares to set
	 */
	public void setTitulares(List<Titular> titulares) {
		this.titulares = titulares;
	}
	
	/**
	 * @return the mediosPagoPersona
	 */
	public List<MedioPagoPersona> getMediosPagoPersona() {
		return mediosPagoPersona;
	}

	/**
	 * @param mediosPagoPersona the mediosPagoPersona to set
	 */
	public void setMediosPagoPersona(List<MedioPagoPersona> mediosPagoPersona) {
		this.mediosPagoPersona = mediosPagoPersona;
	}

	@XmlTransient
	public List<Negocio> getNegocios() {
		return negocios;
	}

	public void setNegocios(List<Negocio> negocios) {
		this.negocios = negocios;
	}

	@Override
	public int hashCode() {
		return Objects.hash(personaPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		return Objects.equals(personaPK, other.personaPK);
	}

	@Override
    public String toString() {
        return "modelos.GenPersona[ genPersonaPK=" + personaPK + " ]";
    }
    
}
