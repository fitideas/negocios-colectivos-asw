/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_INGRESO_OPERACION")
@XmlRootElement
public class IngresoOperacion extends Auditoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected IngresoOperacionPK ingresoOperacionPK;
	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	@Column(name = "VALOR")
	private BigDecimal valor;
	@Column(name = "FECHA_SISTEMA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaSistema;
	@Column(name = "ESTADO")
	private String estado;
	@JoinColumns({
        @JoinColumn(name = "TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false)
        ,@JoinColumn(name = "PERIODO", referencedColumnName = "PERIODO", insertable = false, updatable = false)
        ,@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private PagoNegocio pagoNegocio;

	public IngresoOperacion() {

	}

	public IngresoOperacion(String periodo, Integer tipoNegocio, String codSfc, String radicado) {
		this.ingresoOperacionPK = new IngresoOperacionPK(periodo, tipoNegocio, codSfc, radicado);
	}

	public IngresoOperacion(IngresoOperacionPK ingresoOperacionPK) {
		this.ingresoOperacionPK = ingresoOperacionPK;
	}

	public IngresoOperacion(IngresoOperacionPK ingresoOperacionPK, Date fecha, BigDecimal valor, Date fechaSistema,
			String estado) {
		this.ingresoOperacionPK = ingresoOperacionPK;
		this.fecha = fecha;
		this.valor = valor;
		this.fechaSistema = fechaSistema;
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ingresoOperacionPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngresoOperacion other = (IngresoOperacion) obj;
		return Objects.equals(ingresoOperacionPK, other.ingresoOperacionPK);
	}

	@Override
	public String toString() {
		return "IngresoOperacion [ingresoOperacionPK=" + ingresoOperacionPK + ", fecha=" + fecha + ", valor=" + valor
				+ ", fechaSistema=" + fechaSistema + ", estado=" + estado + "]";
	}

	public IngresoOperacionPK getIngresoOperacionPK() {
		return ingresoOperacionPK;
	}

	public void setIngresoOperacionPK(IngresoOperacionPK ingresoOperacionPK) {
		this.ingresoOperacionPK = ingresoOperacionPK;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Date getFechaSistema() {
		return fechaSistema;
	}

	public void setFechaSistema(Date fechaSistema) {
		this.fechaSistema = fechaSistema;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
