package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author efarias
 *
 */
@Embeddable
public class HistorialNotificacionesPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "COD_SFC")
	private String codSfc;

	@Basic(optional = false)
	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	@Basic(optional = false)
	@Column(name = "FECHA_NOTIFICACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaNotificacion;

	public HistorialNotificacionesPK() {

	}

	public HistorialNotificacionesPK(String codSfc, Date fecha, Date fechaNotificacion) {
		this.codSfc = codSfc;
		this.fecha = fecha;
		this.fechaNotificacion = fechaNotificacion;
	}

	public HistorialNotificacionesPK(FechaClavePK fechaClavePK, Date fechaNotificacion) {
		this.codSfc = fechaClavePK.getCodSfc();
		this.fecha = fechaClavePK.getFecha();
		this.fechaNotificacion = fechaNotificacion;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, fecha, fechaNotificacion);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		HistorialNotificacionesPK other = (HistorialNotificacionesPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(fecha, other.fecha)
				&& Objects.equals(fechaNotificacion, other.fechaNotificacion);
	}

	@Override
	public String toString() {
		return "HistorialNotificacionesPK [codSfc=" + codSfc + ", fecha=" + fecha + ", fechaNotificacion="
				+ fechaNotificacion + "]";
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}

	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}

}
