package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ComisionAprobadoraComitePK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column (name ="ID_DECISION")
	private Long idDecision;
	@Column(name="TIPO_DOCUMENTO")
	private String tipoDocumento;
	@Column(name ="NUMERO_DOCUMENTO") 
	private String numeroDocumento;
	
	
	
	public ComisionAprobadoraComitePK() {
	}

	public ComisionAprobadoraComitePK(Long idDecision, String tipoDocumento, String numeroDocumento) {
		super();
		this.idDecision = idDecision;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
	}
	
	public ComisionAprobadoraComitePK( String tipoDocumento, String numeroDocumento) {
	
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
	}
	/**
	 * @return the idDecision
	 */
	public Long getIdDecision() {
		return idDecision;
	}
	/**
	 * @param idDecision the idDecision to set
	 */
	public void setIdDecision(Long idDecision) {
		this.idDecision = idDecision;
	}
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	

}
