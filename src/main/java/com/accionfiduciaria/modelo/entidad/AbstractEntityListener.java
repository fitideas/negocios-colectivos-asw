/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

/**
 * @author efarias
 *
 */
@PropertySource("classpath:propiedades.properties")
public class AbstractEntityListener {

	@Value("${header.title.authorization}")
	private String headerAuthorization;

	@Autowired
	private HttpServletRequest request;

	// Constantes
	private static final String EMPTY = "";

	/**
	 * Este método setea el campo loginAuditoria tras cada actualización o inserción
	 * en la base de datos
	 * 
	 * @author efarias
	 */
	@PrePersist
	public void prePersist(Auditoria auditoria) {
		auditoria.setLoginAuditoria(obtenerToken());
	}

	/**
	 * Este método setea el campo loginAuditoria tras cada actualización o inserción
	 * en la base de datos
	 * 
	 * @author efarias
	 */
	@PreUpdate
	public void preUpdate(Auditoria auditoria) {
		auditoria.setLoginAuditoria(obtenerToken());
	}

	/**
	 * Este método obtiene el token del sesión para efectos de auditoria.
	 * 
	 * @author efarias
	 * @return token - valor del token de sesión
	 */
	private String obtenerToken() {
		String token = request.getHeader(headerAuthorization);
		if (token == null || token.equals(EMPTY)) {
			token = " ";
		}
		return token;
	}

}
