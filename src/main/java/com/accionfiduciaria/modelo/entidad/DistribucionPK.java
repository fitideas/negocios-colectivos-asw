/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author efarias
 *
 */
@Embeddable
public class DistribucionPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "COD_SFC")
	private String codSfc;

	@Basic(optional = false)
	@Column(name = "TIPO_NEGOCIO")
	private Integer tipoNegocio;

	@Basic(optional = false)
	@Column(name = "PERIODO")
	private String periodo;

	@Basic(optional = false)
	@Column(name = "FECHA_HORA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHora;

	public DistribucionPK() {
	}

	/**
	 * @param codSfc
	 * @param tipoNegocio
	 * @param periodo
	 * @param fechaLiqNegProm
	 */
	public DistribucionPK(String codSfc, Integer tipoNegocio, String periodo, Date fechaHora) {
		this.codSfc = codSfc;
		this.tipoNegocio = tipoNegocio;
		this.periodo = periodo;
		this.fechaHora = fechaHora;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, tipoNegocio, periodo, fechaHora);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		DistribucionPK other = (DistribucionPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(tipoNegocio, other.tipoNegocio)
				&& Objects.equals(periodo, other.periodo) && Objects.equals(fechaHora, other.fechaHora);
	}

	@Override
	public String toString() {
		return "DistribucionPK [codSfc=" + codSfc + ", tipoNegocio=" + tipoNegocio + ", periodo=" + periodo
				+ ", fechaHora=" + fechaHora + "]";
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public Integer getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(Integer tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

}
