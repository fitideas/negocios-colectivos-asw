package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "CONF_TABLAS_DOMINIOS")
@XmlRootElement
public class TablasDominios implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TablasDominiosPK tablasDominiosPK;

    public TablasDominios() {
    }

    public TablasDominios(TablasDominiosPK tablasDominiosPK) {
        this.tablasDominiosPK = tablasDominiosPK;
    }

    public TablasDominios(String nombreTabla, String codDominio) {
        this.tablasDominiosPK = new TablasDominiosPK(nombreTabla, codDominio);
    }

    public TablasDominiosPK getTablasDominiosPK() {
        return tablasDominiosPK;
    }

    public void setTablasDominiosPK(TablasDominiosPK tablasDominiosPK) {
        this.tablasDominiosPK = tablasDominiosPK;
    }

    @Override
	public int hashCode() {
		return Objects.hash(tablasDominiosPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TablasDominios other = (TablasDominios) obj;
		return Objects.equals(tablasDominiosPK, other.tablasDominiosPK);
	}

	@Override
    public String toString() {
        return "modelos.ConfTablasDominios[ tablasDominiosPK=" + tablasDominiosPK + " ]";
    }
    
}
