package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "AUT_PERMISO")
@XmlRootElement
public class Permiso implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDPERMISO")
    private Integer idPermiso;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "PUEDEEDITAR")
    private Boolean puedeEditar;
    @Column(name = "URLPERMISO")
    private String urlPermiso;
    @Column(name = "ESTADO")
    private Boolean estado;
    @OneToMany(mappedBy = "permisoPadre", fetch = FetchType.LAZY)
    private List<Permiso> permisosHijos;
    @JoinColumn(name = "AUT_PERMISO_IDPERMISO", referencedColumnName = "IDPERMISO")
    @ManyToOne
    private Permiso permisoPadre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "permiso", fetch = FetchType.LAZY)
    private List<RolPermiso> roles;

    public Permiso() {
    }

    public Permiso(Integer idpermiso) {
        this.idPermiso = idpermiso;
    }

    public Permiso(Integer idpermiso, String nombre, Boolean puedeeditar) {
        this.idPermiso = idpermiso;
        this.nombre = nombre;
        this.puedeEditar = puedeeditar;
    }

    public Integer getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(Integer idPermiso) {
        this.idPermiso = idPermiso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getPuedeEditar() {
        return puedeEditar;
    }

    public void setPuedeEditar(Boolean puedeEditar) {
        this.puedeEditar = puedeEditar;
    }

    public String getUrlPermiso() {
        return urlPermiso;
    }

    public void setUrlPermiso(String urlPermiso) {
        this.urlPermiso = urlPermiso;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Permiso> getPermisosHijos() {
        return permisosHijos;
    }

    public void setPermisosHijos(List<Permiso> permisosHijos) {
        this.permisosHijos = permisosHijos;
    }

    public Permiso getPermisoPadre() {
        return permisoPadre;
    }

    public void setPermisoPadre(Permiso permisoPadre) {
        this.permisoPadre = permisoPadre;
    }

    @XmlTransient
    public List<RolPermiso> getRoles() {
        return roles;
    }

    public void setRoles(List<RolPermiso> roles) {
        this.roles = roles;
    }   

    @Override
	public int hashCode() {
		return Objects.hash(idPermiso);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permiso other = (Permiso) obj;
		return Objects.equals(idPermiso, other.idPermiso);
	}

	@Override
    public String toString() {
        return "modelos.AutPermiso[ idpermiso=" + idPermiso + " ]";
    }
    
}
