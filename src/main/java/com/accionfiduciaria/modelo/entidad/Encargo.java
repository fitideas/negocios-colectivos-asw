package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_ENCARGO")
@XmlRootElement
public class Encargo implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8548059503242366047L;
	
	@EmbeddedId
    protected EncargoPK encargoPK;
    @Basic(optional = false)
    @Column(name = "MOVIMIENTO_FONDO")
    private String movimientoFondo;
    @Basic(optional = false)
    @Column(name = "NUMERO_PAGO_ORDINARIO")
    private String numeroPagoOrdinario;
    @Basic(optional = false)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumns({
        @JoinColumn(name = "CUENTA", referencedColumnName = "CUENTA", insertable = false, updatable = false)
        , @JoinColumn(name = "BANCO", referencedColumnName = "BANCO", insertable = false, updatable = false)
        , @JoinColumn(name = "TIPO_CUENTA", referencedColumnName = "TIPO_CUENTA", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private MedioPago medioPago;
    @JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Negocio negocio;

    @Column(name = "TIPO_CUENTA")
    private Integer tipoCuenta;
    
    @Column(name = "BANCO")
    private Integer banco;

    /**
	 * 
	 */
	public Encargo() {
	}

	public Encargo(EncargoPK encargoPK) {
        this.encargoPK = encargoPK;
    }

    public Encargo(EncargoPK encargoPK, String movimientoFondo, String numeroPagoOrdinario, String estado) {
        this.encargoPK = encargoPK;
        this.movimientoFondo = movimientoFondo;
        this.numeroPagoOrdinario = numeroPagoOrdinario;
        this.estado = estado;
    }

    public Encargo(String codSfc ,String cuenta) {
        this.encargoPK = new EncargoPK(codSfc, cuenta);
    }

    public Encargo(String codSfc, String cuenta, String estado) {
    	this.encargoPK = new EncargoPK(codSfc, cuenta);
    	this.estado = estado;
    }
    
    public EncargoPK getEncargoPK() {
        return encargoPK;
    }

    public void setEncargoPK(EncargoPK encargoPK) {
        this.encargoPK = encargoPK;
    }

    public String getMovimientoFondo() {
        return movimientoFondo;
    }

    public void setMovimientoFondo(String movimientoFondo) {
        this.movimientoFondo = movimientoFondo;
    }

    public String getNumeroPagoOrdinario() {
        return numeroPagoOrdinario;
    }

    public void setNumeroPagoOrdinario(String numeroPagoOrdinario) {
        this.numeroPagoOrdinario = numeroPagoOrdinario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

    public Negocio getNegocio() {
        return negocio;
    }

    public void setNegocio(Negocio negocio) {
        this.negocio = negocio;
    }
    
	public Integer getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(Integer tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public Integer getBanco() {
		return banco;
	}

	public void setBanco(Integer banco) {
		this.banco = banco;
	}

	@Override
	public int hashCode() {
		return Objects.hash(encargoPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Encargo other = (Encargo) obj;
		return Objects.equals(encargoPK, other.encargoPK);
	}

	@Override
	public String toString() {
		return "Encargo [encargoPK=" + encargoPK + "]";
	}
}
