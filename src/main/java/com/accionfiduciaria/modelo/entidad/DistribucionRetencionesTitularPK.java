/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author efarias
 *
 */
@Embeddable
public class DistribucionRetencionesTitularPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "ID_RETENCION")
	private Integer idRetencion;
	@Basic(optional = false)
	@Column(name = "TITULAR_COD_SFC")
	private String retTitularCodSfc;
	@Basic(optional = false)
	@Column(name = "TITULAR_NUMERO_DOCUMENTO")
	private String retNumDocTitular;
	@Basic(optional = false)
	@Column(name = "TITULAR_TIPO_DOCUMENTO")
	private String retTipoDocTitular;
	@Basic(optional = false)
	@Column(name = "NEG_DIST_COD_SFC")
	private String distCodSfc;
	@Basic(optional = false)
	@Column(name = "TIPO_NEGOCIO")
	private Integer distTipoNegocio;
	@Basic(optional = false)
	@Column(name = "PERIODO")
	private String periodo;
	@Basic(optional = false)
	@Column(name = "FECHA_HORA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHora;
	@Basic(optional = false)
	@Column(name = "TITULAR_TIPO_NAT_NEG")
	private Integer titularTipoNatNeg;
	
	public DistribucionRetencionesTitularPK() {

	}

	public DistribucionRetencionesTitularPK(RetencionTitularPK retencionTitularPK, DistribucionPK distribucionPK) {
		this.idRetencion = retencionTitularPK.getIdRetencion();
		this.retTitularCodSfc = retencionTitularPK.getCodSfc();
		this.retNumDocTitular = retencionTitularPK.getNumeroDocumento();
		this.retTipoDocTitular = retencionTitularPK.getTipoDocumento();
		this.distCodSfc = distribucionPK.getCodSfc();
		this.distTipoNegocio = distribucionPK.getTipoNegocio();
		this.periodo = distribucionPK.getPeriodo();
		this.fechaHora = distribucionPK.getFechaHora();
		this.titularTipoNatNeg = retencionTitularPK.getTipoNaturalezaNegocio();
	}

	public Integer getIdRetencion() {
		return idRetencion;
	}

	public void setIdRetencion(Integer idRetencion) {
		this.idRetencion = idRetencion;
	}

	public String getRetTitularCodSfc() {
		return retTitularCodSfc;
	}

	public void setRetTitularCodSfc(String retTitularCodSfc) {
		this.retTitularCodSfc = retTitularCodSfc;
	}

	public String getRetNumDocTitular() {
		return retNumDocTitular;
	}

	public void setRetNumDocTitular(String retNumDocTitular) {
		this.retNumDocTitular = retNumDocTitular;
	}

	public String getRetTipoDocTitular() {
		return retTipoDocTitular;
	}

	public void setRetTipoDocTitular(String retTipoDocTitular) {
		this.retTipoDocTitular = retTipoDocTitular;
	}

	public String getDistCodSfc() {
		return distCodSfc;
	}

	public void setDistCodSfc(String distCodSfc) {
		this.distCodSfc = distCodSfc;
	}

	public Integer getDistTipoNegocio() {
		return distTipoNegocio;
	}

	public void setDistTipoNegocio(Integer distTipoNegocio) {
		this.distTipoNegocio = distTipoNegocio;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Integer getTitularTipoNatNeg() {
		return titularTipoNatNeg;
	}

	public void setTitularTipoNatNeg(Integer titularTipoNatNeg) {
		this.titularTipoNatNeg = titularTipoNatNeg;
	}

	@Override
	public int hashCode() {
		return Objects.hash(distCodSfc, distTipoNegocio, fechaHora, idRetencion, periodo, retNumDocTitular,
				retTipoDocTitular, retTitularCodSfc, titularTipoNatNeg);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistribucionRetencionesTitularPK other = (DistribucionRetencionesTitularPK) obj;
		return Objects.equals(distCodSfc, other.distCodSfc) && Objects.equals(distTipoNegocio, other.distTipoNegocio)
				&& Objects.equals(fechaHora, other.fechaHora) && Objects.equals(idRetencion, other.idRetencion)
				&& Objects.equals(periodo, other.periodo) && Objects.equals(retNumDocTitular, other.retNumDocTitular)
				&& Objects.equals(retTipoDocTitular, other.retTipoDocTitular)
				&& Objects.equals(retTitularCodSfc, other.retTitularCodSfc)
				&& Objects.equals(titularTipoNatNeg, other.titularTipoNatNeg);
	}

	@Override
	public String toString() {
		return "DistribucionRetencionesTitularPK [idRetencion=" + idRetencion + ", retTitularCodSfc=" + retTitularCodSfc
				+ ", retNumDocTitular=" + retNumDocTitular + ", retTipoDocTitular=" + retTipoDocTitular
				+ ", distCodSfc=" + distCodSfc + ", distTipoNegocio=" + distTipoNegocio + ", periodo=" + periodo
				+ ", fechaHora=" + fechaHora + ", titularTipoNatNeg=" + titularTipoNatNeg + "]";
	}
}
