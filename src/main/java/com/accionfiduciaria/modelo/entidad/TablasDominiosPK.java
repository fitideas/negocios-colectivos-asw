package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class TablasDominiosPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1305711796967321426L;
	
	@Basic(optional = false)
    @Column(name = "NOMBRE_TABLA")
    private String nombreTabla;
    @Basic(optional = false)
    @Column(name = "COD_DOMINIO")
    private String codDominio;

    public TablasDominiosPK() {
    }

    public TablasDominiosPK(String nombreTabla, String codDominio) {
        this.nombreTabla = nombreTabla;
        this.codDominio = codDominio;
    }

    public String getNombreTabla() {
        return nombreTabla;
    }

    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public String getCodDominio() {
        return codDominio;
    }

    public void setCodDominio(String codDominio) {
        this.codDominio = codDominio;
    }

    @Override
	public int hashCode() {
		return Objects.hash(codDominio, nombreTabla);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TablasDominiosPK other = (TablasDominiosPK) obj;
		return Objects.equals(codDominio, other.codDominio) && Objects.equals(nombreTabla, other.nombreTabla);
	}

	@Override
    public String toString() {
        return "modelos.ConfTablasDominiosPK[ nombreTabla=" + nombreTabla + ", codDominio=" + codDominio + " ]";
    }
    
}
