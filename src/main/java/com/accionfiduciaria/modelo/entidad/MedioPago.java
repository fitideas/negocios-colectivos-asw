package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_MEDIO_PAGO")
@XmlRootElement
public class MedioPago extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MedioPagoPK medioPagoPK;
    
    @Column(name="OBSERVACION")
    private String observacion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medioPago")
    private List<MedioPagoPersona> mediosPagoPersona;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medioPago")
    private List<Encargo> encargos;

    public MedioPago() {
    }

    public MedioPago(MedioPagoPK medioPagoPK) {
        this.medioPagoPK = medioPagoPK;
    }

    public MedioPago(Integer banco, String cuenta, Integer tipoCuenta) {
        this.medioPagoPK = new MedioPagoPK(banco, cuenta, tipoCuenta);
    }

    public MedioPagoPK getMedioPagoPK() {
        return medioPagoPK;
    }

    public void setMedioPagoPK(MedioPagoPK medioPagoPK) {
        this.medioPagoPK = medioPagoPK;
    }

	/**
	 * @return the observacion
	 */
	public String getObservacion() {
		return observacion;
	}

	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@XmlTransient
    public List<MedioPagoPersona> getMediosPagoPersona() {
        return mediosPagoPersona;
    }

    public void setMediosPagoPersona(List<MedioPagoPersona> mediosPagoPersona) {
        this.mediosPagoPersona = mediosPagoPersona;
    }

	@XmlTransient
	public List<Encargo> getEncargos() {
		return encargos;
	}

	public void setEncargos(List<Encargo> encargos) {
		this.encargos = encargos;
	}

	@Override
	public int hashCode() {
		return Objects.hash(medioPagoPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedioPago other = (MedioPago) obj;
		return Objects.equals(medioPagoPK, other.medioPagoPK);
	}

	@Override
    public String toString() {
        return "modelos.NegMedioPago[ negMedioPagoPK=" + medioPagoPK + " ]";
    }
    
}
