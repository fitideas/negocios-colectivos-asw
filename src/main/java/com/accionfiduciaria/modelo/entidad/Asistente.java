package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="NEG_ASISTENCIA")
@XmlRootElement
public class Asistente  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	AsistentePK asistentePK;
	
	@Column(name ="ASISTIO")
	private int asistio;
	
	@JoinColumns({
        @JoinColumn(name = "FECHA", referencedColumnName = "FECHA", insertable = false, updatable = false)
        , @JoinColumn(name = "REUNION_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)})
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
	private Reunion reunion;

	public Asistente() {
		
	}	
	
	/**
	 * @return the asistentePK
	 */
	public AsistentePK getAsistentePK() {
		return asistentePK;
	}

	/**
	 * @param asistentePK the asistentePK to set
	 */
	public void setAsistentePK(AsistentePK asistentePK) {
		this.asistentePK = asistentePK;
	}
	
	/**
	 * @return the asistio
	 */
	public int getAsistio() {
		return asistio;
	}
	/**
	 * @param asistio the asistio to set
	 */
	public void setAsistio(int asistio) {
		this.asistio = asistio;
	}

	public Reunion getReunion() {
		return reunion;
	}

	public void setReunion(Reunion reunion) {
		this.reunion = reunion;
	}

	@Override
	public String toString() {
		return "Asistente [asistentePK=" + asistentePK + ", asistio=" + asistio + ", reunion="  + "]";
	}

}
