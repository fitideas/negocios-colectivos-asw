package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AsistentePK implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	@Basic(optional = false)
	@Column(name ="COD_SFC")
	private String codSFC;
	@Basic(optional = false)
	@Column(name ="TIPO_DOCUMENTO")
	private String tipoDocumento;
	@Basic(optional = false)
	@Column(name ="NUMERO_DOCUMENTO")
	private String numeroDocumento;
	@Basic(optional = false)
	@Column(name ="REUNION_COD_SFC")
	private String reunionCodSFC;
	@Basic(optional = false)
	@Column(name ="FECHA",columnDefinition = "TIMESTAMP")
	private LocalDateTime fecha;
	
	
	
	public AsistentePK() {
		super();
	}


	public AsistentePK(String codSFC, String tipoDocumento, String numeroDocumento, String reunionCodSFC,
			LocalDateTime fecha) {
		
		this.codSFC = codSFC;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.reunionCodSFC = reunionCodSFC;
		this.fecha = fecha;
	}
	
	
	/**
	 * @return the codSFC
	 */
	public String getCodSFC() {
		return codSFC;
	}
	/**
	 * @param codSFC the codSFC to set
	 */
	public void setCodSFC(String codSFC) {
		this.codSFC = codSFC;
	}
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/**
	 * @return the reunionCodSFC
	 */
	public String getReunionCodSFC() {
		return reunionCodSFC;
	}
	/**
	 * @param reunionCodSFC the reunionCodSFC to set
	 */
	public void setReunionCodSFC(String reunionCodSFC) {
		this.reunionCodSFC = reunionCodSFC;
	}
	/**
	 * @return the fecha
	 */
	public LocalDateTime getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codSFC == null) ? 0 : codSFC.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((numeroDocumento == null) ? 0 : numeroDocumento.hashCode());
		result = prime * result + ((reunionCodSFC == null) ? 0 : reunionCodSFC.hashCode());
		result = prime * result + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsistentePK other = (AsistentePK) obj;
		if (codSFC == null) {
			if (other.codSFC != null)
				return false;
		} else if (!codSFC.equals(other.codSFC))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (numeroDocumento == null) {
			if (other.numeroDocumento != null)
				return false;
		} else if (!numeroDocumento.equals(other.numeroDocumento))
			return false;
		if (reunionCodSFC == null) {
			if (other.reunionCodSFC != null)
				return false;
		} else if (!reunionCodSFC.equals(other.reunionCodSFC))
			return false;
		if (tipoDocumento == null) {
			if (other.tipoDocumento != null)
				return false;
		} else if (!tipoDocumento.equals(other.tipoDocumento))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AsistentePK [codSFC=" + codSFC + ", tipoDocumento=" + tipoDocumento + ", numeroDocumento="
				+ numeroDocumento + ", reunionCodSFC=" + reunionCodSFC + ", fecha=" + fecha + "]";
	}
	
	
	
}
