package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_RETENCION_TITULAR")
@XmlRootElement
public class RetencionTitular extends Auditoria implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected RetencionTitularPK retencionTitularPK;
	@Basic(optional = false)
	@Column(name = "ACTIVO")
	private Boolean activo;
	@JoinColumns({
			@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "NUMERO_DOCUMENTO", referencedColumnName = "NUMERO_DOCUMENTO", insertable = false, updatable = false),
			@JoinColumn(name = "TIPO_DOCUMENTO", referencedColumnName = "TIPO_DOCUMENTO", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private Titular titular;
	@JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioRetencion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "retencionTitular")
	private List<RetencionImpuestoBeneficiario> retencionImpuestoBeneficiarios;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "retencionTitular")
	private List<DistribucionRetencionesTitular> distribucionRetencionesTitular;

	public RetencionTitular() {
	}

	public RetencionTitular(RetencionTitularPK retencionTitularPK) {
		this.retencionTitularPK = retencionTitularPK;
	}

	public RetencionTitular(RetencionTitularPK retencionTitularPK, Boolean activo) {
		this.retencionTitularPK = retencionTitularPK;
		this.activo = activo;
	}

	public RetencionTitular(Integer idRetencion, String codSfc, String numeroDocumento, String tipoDocumento, Integer tipoNegocio) {
		this.retencionTitularPK = new RetencionTitularPK(idRetencion, codSfc, numeroDocumento, tipoDocumento, tipoNegocio);
	}

	public RetencionTitular(Integer idRetencion, String codSfc, String numeroDocumento, String tipoDocumento,
			Boolean activo, Integer tipoNegocio) {
		this.retencionTitularPK = new RetencionTitularPK(idRetencion, codSfc, numeroDocumento, tipoDocumento, tipoNegocio);
		this.activo = activo;
	}

	public RetencionTitularPK getRetencionTitularPK() {
		return retencionTitularPK;
	}

	public void setRetencionTitularPK(RetencionTitularPK retencionTitularPK) {
		this.retencionTitularPK = retencionTitularPK;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Titular getTitular() {
		return titular;
	}

	public void setTitular(Titular titular) {
		this.titular = titular;
	}

	public ValorDominioNegocio getDominioRetencion() {
		return dominioRetencion;
	}

	public void setDominioRetencion(ValorDominioNegocio dominioRetencion) {
		this.dominioRetencion = dominioRetencion;
	}

	public List<RetencionImpuestoBeneficiario> getRetencionImpuestoBeneficiarios() {
		return retencionImpuestoBeneficiarios;
	}

	public void setRetencionImpuestoBeneficiarios(List<RetencionImpuestoBeneficiario> retencionImpuestoBeneficiarios) {
		this.retencionImpuestoBeneficiarios = retencionImpuestoBeneficiarios;
	}

	public List<DistribucionRetencionesTitular> getDistribucionRetencionesTitular() {
		return distribucionRetencionesTitular;
	}

	public void setDistribucionRetencionesTitular(List<DistribucionRetencionesTitular> distribucionRetencionesTitular) {
		this.distribucionRetencionesTitular = distribucionRetencionesTitular;
	}

	@Override
	public int hashCode() {
		return Objects.hash(retencionTitularPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetencionTitular other = (RetencionTitular) obj;
		return Objects.equals(retencionTitularPK, other.retencionTitularPK);
	}

	@Override
	public String toString() {
		return "modelos.NegRetencionTitular[ negRetencionTitularPK=" + retencionTitularPK + " ]";
	}

}
