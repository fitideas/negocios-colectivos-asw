package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jpolo
 */
@Embeddable
public class FechaClavePK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
    @Column(name = "COD_SFC")
    private String codSfc;
    @Basic(optional = false)
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public FechaClavePK() {
    }

    public FechaClavePK(String codSfc, Date fecha) {
        this.codSfc = codSfc;
        this.fecha = fecha;
    }

    public String getCodSfc() {
        return codSfc;
    }

    public void setCodSfc(String codSfc) {
        this.codSfc = codSfc;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, fecha);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FechaClavePK other = (FechaClavePK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(fecha, other.fecha);
	}

	@Override
	public String toString() {
		return "FechaClavePK [codSfc=" + codSfc + ", fecha=" + fecha + "]";
	}
}
