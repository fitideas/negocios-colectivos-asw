/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */

@Entity
@Table(name = "NEG_PAGO_NEGOCIO")
@XmlRootElement
public class PagoNegocio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected PagoNegocioPK pagoNegocioPK;
	@Basic(optional = false)
	@Column(name = "DISTRIBUCION_EXITOSA")
	private String distribucionExitosa;
	@Basic(optional = false)
	@Column(name = "ENCARGO_COD_SFC")
	private String encargoCodSfc;
	@Basic(optional = false)
	@Column(name = "CUENTA")
	private String cuenta;
	@JoinColumns({
			@JoinColumn(name = "ENCARGO_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "CUENTA", referencedColumnName = "CUENTA", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private Encargo encargo;
	@JoinColumns({
			@JoinColumn(name = "TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private TipoNegocio tipoNegocio;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pagoNegocio")
	private List<IngresoOperacion> ingresosOperacion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pagoNegocio")
	private List<Distribucion> distribuciones;

	public PagoNegocio() {

	}

	public PagoNegocio(PagoNegocioPK pagoNegocioPK) {
		this.pagoNegocioPK = pagoNegocioPK;
	}

	public PagoNegocio(String periodo, Integer tipoNegocio, String codSfc) {
		this.pagoNegocioPK = new PagoNegocioPK(periodo, tipoNegocio, codSfc);
	}
	
	public PagoNegocio(PagoNegocioPK pagoNegocioPK, String distribucionExitosa, String encargoCodSfc, String cuenta) {
		this.pagoNegocioPK = pagoNegocioPK;
		this.distribucionExitosa = distribucionExitosa;
		this.encargoCodSfc = encargoCodSfc;
		this.cuenta = cuenta;
	}
	
	public PagoNegocio(PagoNegocioPK pagoNegocioPK, String distribucionExitosa, EncargoPK encargoPK) {
		this.pagoNegocioPK = pagoNegocioPK;
		this.distribucionExitosa = distribucionExitosa;
		this.encargoCodSfc = encargoPK.getCodSfc();
		this.cuenta = encargoPK.getCuenta();
	}

	@Override
	public int hashCode() {
		return Objects.hash(pagoNegocioPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PagoNegocio other = (PagoNegocio) obj;
		return Objects.equals(pagoNegocioPK, other.pagoNegocioPK);
	}

	@Override
	public String toString() {
		return "modelos.PagoNegocio [pagoNegocioPK=" + pagoNegocioPK + "]";
	}

	public PagoNegocioPK getPagoNegocioPK() {
		return pagoNegocioPK;
	}

	public void setPagoNegocioPK(PagoNegocioPK pagoNegocioPK) {
		this.pagoNegocioPK = pagoNegocioPK;
	}

	public String getDistribucionExitosa() {
		return distribucionExitosa;
	}

	public void setDistribucionExitosa(String distribucionExitosa) {
		this.distribucionExitosa = distribucionExitosa;
	}

	public String getEncargoCodSfc() {
		return encargoCodSfc;
	}

	public void setEncargoCodSfc(String encargoCodSfc) {
		this.encargoCodSfc = encargoCodSfc;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public Encargo getEncargo() {
		return encargo;
	}

	public void setEncargo(Encargo encargo) {
		this.encargo = encargo;
	}

	public List<IngresoOperacion> getIngresosOperacion() {
		return ingresosOperacion;
	}

	public void setIngresosOperacion(List<IngresoOperacion> ingresosOperacion) {
		this.ingresosOperacion = ingresosOperacion;
	}

	public TipoNegocio getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(TipoNegocio tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public List<Distribucion> getDistribuciones() {
		return distribuciones;
	}

	public void setDistribuciones(List<Distribucion> distribuciones) {
		this.distribuciones = distribuciones;
	}

}
