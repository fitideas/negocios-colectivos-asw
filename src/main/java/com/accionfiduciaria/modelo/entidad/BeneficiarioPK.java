package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class BeneficiarioPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8597146451779542913L;
	@Basic(optional = false)
    @Column(name = "COD_SFC")
    private String codSfc;
    @Basic(optional = false)
    @Column(name = "TIPO_DOC_TITULAR")
    private String tipoDocumentoTitular;
    @Basic(optional = false)
    @Column(name = "NUM_DOC_TITULAR")
    private String numeroDocumentoTitular;
    @Basic(optional = false)
    @Column(name = "TIPO_DOC_BENEFICIARIO")
    private String tipoDocumentoBeneficiario;
    @Basic(optional = false)
    @Column(name = "NUM_DOC_BENEFICIARIO")
    private String numeroDocumentoBeneficiario;
    @Basic(optional = false)
    @Column(name = "CUENTA")
    private String cuenta;
    @Basic(optional = false)
    @Column(name = "BANCO")
    private Integer banco;
    @Basic(optional = false)
    @Column(name = "TIPO_CUENTA")
    private Integer tipoCuenta;
    
    public BeneficiarioPK() {
    	
    }
    
	public BeneficiarioPK(MedioPagoPersonaPK medioPagoPersonaPK, TitularPK titularPK) {
		this.codSfc = titularPK.getCodSfc();
		this.tipoDocumentoTitular = titularPK.getTipoDocumento();
		this.numeroDocumentoTitular = titularPK.getNumeroDocumento();
		this.tipoDocumentoBeneficiario = medioPagoPersonaPK.getTipoDocumento();
		this.numeroDocumentoBeneficiario = medioPagoPersonaPK.getNumeroDocumento();
		this.cuenta = medioPagoPersonaPK.getCuenta();
		this.banco = medioPagoPersonaPK.getBanco();
		this.tipoCuenta = medioPagoPersonaPK.getTipoCuenta();
	}

	public String getCodSfc() {
        return codSfc;
    }

    public void setCodSfc(String codSfc) {
        this.codSfc = codSfc;
    }

    public String getTipoDocumentoTitular() {
        return tipoDocumentoTitular;
    }

    public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
        this.tipoDocumentoTitular = tipoDocumentoTitular;
    }

    public String getNumeroDocumentoTitular() {
        return numeroDocumentoTitular;
    }

    public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
        this.numeroDocumentoTitular = numeroDocumentoTitular;
    }

    public String getTipoDocumentoBeneficiario() {
        return tipoDocumentoBeneficiario;
    }

    public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
        this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
    }

    public String getNumeroDocumentoBeneficiario() {
        return numeroDocumentoBeneficiario;
    }

    public void setNumeroDocumentoBeneficiario(String numeroDocumentoBeneficiario) {
        this.numeroDocumentoBeneficiario = numeroDocumentoBeneficiario;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getBanco() {
        return banco;
    }

    public void setBanco(Integer banco) {
        this.banco = banco;
    }

    public Integer getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(Integer tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

	@Override
	public int hashCode() {
		return Objects.hash(banco, codSfc, cuenta, numeroDocumentoBeneficiario, numeroDocumentoTitular, tipoCuenta,
				tipoDocumentoBeneficiario, tipoDocumentoTitular);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BeneficiarioPK other = (BeneficiarioPK) obj;
		return Objects.equals(banco, other.banco) && Objects.equals(codSfc, other.codSfc)
				&& Objects.equals(cuenta, other.cuenta)
				&& Objects.equals(numeroDocumentoBeneficiario, other.numeroDocumentoBeneficiario)
				&& Objects.equals(numeroDocumentoTitular, other.numeroDocumentoTitular)
				&& Objects.equals(tipoCuenta, other.tipoCuenta)
				&& Objects.equals(tipoDocumentoBeneficiario, other.tipoDocumentoBeneficiario)
				&& Objects.equals(tipoDocumentoTitular, other.tipoDocumentoTitular);
	}
}
