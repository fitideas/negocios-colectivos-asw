/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_DISTRIBUCION_RETENCIONES")
@XmlRootElement
public class DistribucionRetenciones implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DistribucionRetencionesPK distribucionRetencionesPK;
	@Basic(optional = false)
	@Column(name = "UNIDAD")
	private String unidad;
	@Basic(optional = false)
	@Column(name = "VALOR_CONFIGURADO_IMPUESTO")
	private BigDecimal valorConfiguradoImpuesto;
	@Basic(optional = false)
	@Column(name = "VALOR_BASE")
	private BigDecimal valorBase;
	@Basic(optional = false)
	@Column(name = "VALOR_IMPUESTO")
	private BigDecimal valorImpuesto;
	@JoinColumns({
			@JoinColumn(name = "NEG_DIST_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "NEG_DIST_TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "FECHA_HORA", referencedColumnName = "FECHA_HORA", insertable = false, updatable = false),
			@JoinColumn(name = "PERIODO", referencedColumnName = "PERIODO", insertable = false, updatable = false), })
	@ManyToOne(optional = false)
	private Distribucion distribucion;
	@JoinColumns({
			@JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_RETENCION", insertable = false, updatable = false),
			@JoinColumn(name = "NEG_RET_TIPO_NEGOCIO", referencedColumnName = "TIPO_NEGOCIO", insertable = false, updatable = false),
			@JoinColumn(name = "NEG_RET_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false) })
	@ManyToOne(optional = false)
	private RetencionTipoNegocio retencionTipoNegocio;
	@JoinColumn(name = "ID_RETENCION", referencedColumnName = "ID_VALOR_DOMINIO", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ValorDominioNegocio dominioRetencion;

	public DistribucionRetenciones() {

	}

	public DistribucionRetenciones(DistribucionPK distribucionPK, RetencionTipoNegocioPK retencionTipoNegocioPK) {
		this.distribucionRetencionesPK = new DistribucionRetencionesPK(retencionTipoNegocioPK, distribucionPK);
	}

	public DistribucionRetenciones(DistribucionPK distribucionPK, RetencionTipoNegocioPK retencionTipoNegocioPK,
			String unidad, BigDecimal valorConfiguradoImpuesto, BigDecimal valorBase, BigDecimal valorImpuesto) {
		this.distribucionRetencionesPK = new DistribucionRetencionesPK(retencionTipoNegocioPK, distribucionPK);
		this.unidad = unidad;
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
		this.valorBase = valorBase;
		this.valorImpuesto = valorImpuesto;
	}

	public DistribucionRetenciones(DistribucionRetencionesPK distribucionRetencionesPK, String unidad,
			BigDecimal valorConfiguradoImpuesto, BigDecimal valorBase, BigDecimal valorImpuesto) {
		this.distribucionRetencionesPK = distribucionRetencionesPK;
		this.unidad = unidad;
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
		this.valorBase = valorBase;
		this.valorImpuesto = valorImpuesto;
	}

	@Override
	public String toString() {
		return "DistribucionRetenciones [distribucionRetencionesPK=" + distribucionRetencionesPK + ", unidad=" + unidad
				+ ", valorConfiguradoImpuesto=" + valorConfiguradoImpuesto + ", valorBase=" + valorBase
				+ ", valorImpuesto=" + valorImpuesto + "]";
	}

	public DistribucionRetencionesPK getDistribucionRetencionesPK() {
		return distribucionRetencionesPK;
	}

	public void setDistribucionRetencionesPK(DistribucionRetencionesPK distribucionRetencionesPK) {
		this.distribucionRetencionesPK = distribucionRetencionesPK;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public BigDecimal getValorConfiguradoImpuesto() {
		return valorConfiguradoImpuesto;
	}

	public void setValorConfiguradoImpuesto(BigDecimal valorConfiguradoImpuesto) {
		this.valorConfiguradoImpuesto = valorConfiguradoImpuesto;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public BigDecimal getValorImpuesto() {
		return valorImpuesto;
	}

	public void setValorImpuesto(BigDecimal valorImpuesto) {
		this.valorImpuesto = valorImpuesto;
	}

	public Distribucion getDistribucion() {
		return distribucion;
	}

	public RetencionTipoNegocio getRetencionTipoNegocio() {
		return retencionTipoNegocio;
	}

	public ValorDominioNegocio getDominioRetencion() {
		return dominioRetencion;
	}

}
