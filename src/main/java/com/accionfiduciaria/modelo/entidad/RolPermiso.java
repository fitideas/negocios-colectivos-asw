package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "AUT_ROL_PERMISO")
@XmlRootElement
public class RolPermiso extends Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RolPermisoPK pk;
    @Basic(optional = false)
    @Column(name = "PUEDEEDITAR")
    private Boolean puedeEditar;
    @JoinColumn(name = "IDPERMISO", referencedColumnName = "IDPERMISO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Permiso permiso;
    @JoinColumn(name = "IDROL", referencedColumnName = "IDROL", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Rol rol;

    public RolPermiso() {
    }

    public RolPermiso(RolPermisoPK autRolPermisoPK) {
        this.pk = autRolPermisoPK;
    }

    public RolPermiso(RolPermisoPK autRolPermisoPK, Boolean puedeeditar) {
        this.pk = autRolPermisoPK;
        this.puedeEditar = puedeeditar;
    }

    public RolPermiso(Integer autRolIdrol, Integer autPermisoIdpermiso) {
        this.pk = new RolPermisoPK(autRolIdrol, autPermisoIdpermiso);
    }

    public RolPermisoPK getPk() {
        return pk;
    }

    public void setPk(RolPermisoPK rolPermisoPK) {
        this.pk = rolPermisoPK;
    }

    public Boolean getPuedeEditar() {
        return puedeEditar;
    }

    public void setPuedeEditar(Boolean puedeEditar) {
        this.puedeEditar = puedeEditar;
    }

    public Permiso getPermiso() {
        return permiso;
    }

    public void setPermiso(Permiso permiso) {
        this.permiso = permiso;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
	public int hashCode() {
		return Objects.hash(pk);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolPermiso other = (RolPermiso) obj;
		return Objects.equals(pk, other.pk);
	}

	@Override
    public String toString() {
        return "modelos.AutRolPermiso[ autRolPermisoPK=" + pk + " ]";
    }
    
}
