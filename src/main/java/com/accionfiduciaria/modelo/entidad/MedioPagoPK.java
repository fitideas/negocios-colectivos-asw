package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jpolo
 */
@Embeddable
public class MedioPagoPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4215264994656040773L;
	@Basic(optional = false)
    @Column(name = "BANCO")
    private Integer banco;
    @Basic(optional = false)
    @Column(name = "CUENTA")
    private String cuenta;
    @Basic(optional = false)
    @Column(name = "TIPO_CUENTA")
    private Integer tipoCuenta;

    public MedioPagoPK() {
    }

    public MedioPagoPK(Integer banco, String cuenta, Integer tipoCuenta) {
        this.banco = banco;
        this.cuenta = cuenta;
        this.tipoCuenta = tipoCuenta;
    }

    public Integer getBanco() {
        return banco;
    }

    public void setBanco(Integer banco) {
        this.banco = banco;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(Integer tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

	@Override
	public int hashCode() {
		return Objects.hash(banco, cuenta, tipoCuenta);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedioPagoPK other = (MedioPagoPK) obj;
		return Objects.equals(banco, other.banco) && Objects.equals(cuenta, other.cuenta)
				&& Objects.equals(tipoCuenta, other.tipoCuenta);
	}
    
}
