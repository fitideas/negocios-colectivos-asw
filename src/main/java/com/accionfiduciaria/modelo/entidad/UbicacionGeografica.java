/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_UBICACION_GEOGRAFICA")
@XmlRootElement
public class UbicacionGeografica implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "CODIGO")
	private String codigo;
	@Basic(optional = false)
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "ISO2")
	private String iso2;
	@Basic(optional = false)
	@Column(name = "TIPO")
	private String tipo;
	@Column(name = "CODIGO_PADRE")
	private String codigoPadre;
	@Basic(optional = false)
	@Column(name = "ACTIVO")
	private Boolean activo;

	public UbicacionGeografica() {

	}

	public UbicacionGeografica(String codigo) {
		this.codigo = codigo;
	}

	public UbicacionGeografica(String codigo, String nombre, String iso2, String tipo, String codigoPadre,
			Boolean activo) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.iso2 = iso2;
		this.tipo = tipo;
		this.codigoPadre = codigoPadre;
		this.activo = activo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIso2() {
		return iso2;
	}

	public void setIso2(String iso2) {
		this.iso2 = iso2;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigoPadre() {
		return codigoPadre;
	}

	public void setCodigoPadre(String codigoPadre) {
		this.codigoPadre = codigoPadre;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UbicacionGeografica other = (UbicacionGeografica) obj;
		return Objects.equals(codigo, other.codigo);
	}

	@Override
	public String toString() {
		return "UbicacionGeografica [codigo=" + codigo + ", nombre=" + nombre + ", iso2=" + iso2 + ", tipo=" + tipo
				+ ", codigoPadre=" + codigoPadre + ", activo=" + activo + "]";
	}

}
