/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "NEG_RESPONSABLES_NOTIFICACION")
@XmlRootElement
public class ResponsableNotificacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected ResponsableNotificacionPK responsablesNotificacionPK;

	@Basic(optional = false)
	@Column(name = "NOMBRE")
	private String nombre;

	@Basic(optional = false)
	@Column(name = "TIPO_RESPONSABLE")
	private String tipoResponsable;

	@Basic(optional = false)
	@Column(name = "TELEFONO")
	private String telefono;

	@Basic(optional = false)
	@Column(name = "CORREO")
	private String correo;

	@Basic(optional = false)
	@Column(name = "ACTIVO")
	private boolean activo;

	@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
	@OneToOne(optional = false)
	private Negocio negocio;

	public ResponsableNotificacion() {
	}

	public ResponsableNotificacion(ResponsableNotificacionPK responsablesNotificacionPK, String nombre,
			String tipoResponsable, String telefono, String correo, boolean activo) {
		this.responsablesNotificacionPK = responsablesNotificacionPK;
		this.nombre = nombre;
		this.tipoResponsable = tipoResponsable;
		this.telefono = telefono;
		this.correo = correo;
		this.activo = activo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(responsablesNotificacionPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponsableNotificacion other = (ResponsableNotificacion) obj;
		return Objects.equals(responsablesNotificacionPK, other.responsablesNotificacionPK);
	}

	@Override
	public String toString() {
		return "ResponsablesNotificacion [responsablesNotificacionPK=" + responsablesNotificacionPK + ", nombre="
				+ nombre + ", tipoResponsable=" + tipoResponsable + ", telefono=" + telefono + ", correo=" + correo
				+ ", activo=" + activo + ", negocio=" + negocio + "]";
	}

	public ResponsableNotificacionPK getResponsablesNotificacionPK() {
		return responsablesNotificacionPK;
	}

	public void setResponsablesNotificacionPK(ResponsableNotificacionPK responsablesNotificacionPK) {
		this.responsablesNotificacionPK = responsablesNotificacionPK;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoResponsable() {
		return tipoResponsable;
	}

	public void setTipoResponsable(String tipoResponsable) {
		this.tipoResponsable = tipoResponsable;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

}
