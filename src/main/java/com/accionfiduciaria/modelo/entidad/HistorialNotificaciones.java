/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author efarias
 *
 */
@Entity
@Table(name = "AUD_HISTORIAL_NOTIFICACIONES")
@XmlRootElement
public class HistorialNotificaciones implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected HistorialNotificacionesPK historialNotificacionesPK;
	@Basic(optional = false)
	@Column(name = "DOCUMENTO_RESPONSABLE")
	private String documentoResponsable;
	@Basic(optional = false)
	@Column(name = "NOMBRE_RESPONSABLE")
	private String nombreResponsable;
	@Basic(optional = false)
	@Column(name = "TIPO_RESPONSABLE")
	private String tipoResponsable;
	@Column(name = "CORREO_RESPONSABLE")
	private String correoResponsable;
	@Column(name = "MENSAJE")
	private String mensaje;
	@Basic(optional = false)
	@Column(name = "ENVIO_NOTIFICACION")
	private boolean envioNotificacion;
	@JoinColumns({
			@JoinColumn(name = "COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false),
			@JoinColumn(name = "FECHA", referencedColumnName = "FECHA", insertable = false, updatable = false) })
	@OneToOne(optional = false)
	private FechaClave fechaClave;

	public HistorialNotificaciones() {

	}

	public HistorialNotificaciones(HistorialNotificacionesPK historialNotificacionesPK) {
		this.historialNotificacionesPK = historialNotificacionesPK;
	}

	public HistorialNotificaciones(FechaClavePK fechaClavePK, Date fechaNotificacion, String documentoResponsable,
			String nombreResponsable, String tipoResponsable, String correoResponsable, String mensaje,
			boolean envioNotificacion) {
		this.historialNotificacionesPK = new HistorialNotificacionesPK(fechaClavePK, fechaNotificacion);
		this.documentoResponsable = documentoResponsable;
		this.nombreResponsable = nombreResponsable;
		this.tipoResponsable = tipoResponsable;
		this.correoResponsable = correoResponsable;
		this.mensaje = mensaje;
		this.envioNotificacion = envioNotificacion;
	}

	@Override
	public int hashCode() {
		return Objects.hash(historialNotificacionesPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistorialNotificaciones other = (HistorialNotificaciones) obj;
		return Objects.equals(historialNotificacionesPK, other.historialNotificacionesPK);
	}

	public HistorialNotificacionesPK getHistorialNotificacionesPK() {
		return historialNotificacionesPK;
	}

	public void setHistorialNotificacionesPK(HistorialNotificacionesPK historialNotificacionesPK) {
		this.historialNotificacionesPK = historialNotificacionesPK;
	}

	public String getDocumentoResponsable() {
		return documentoResponsable;
	}

	public void setDocumentoResponsable(String documentoResponsable) {
		this.documentoResponsable = documentoResponsable;
	}

	public String getNombreResponsable() {
		return nombreResponsable;
	}

	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}

	public String getTipoResponsable() {
		return tipoResponsable;
	}

	public void setTipoResponsable(String tipoResponsable) {
		this.tipoResponsable = tipoResponsable;
	}

	public String getCorreoResponsable() {
		return correoResponsable;
	}

	public void setCorreoResponsable(String correoResponsable) {
		this.correoResponsable = correoResponsable;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean isEnvioNotificacion() {
		return envioNotificacion;
	}

	public void setEnvioNotificacion(boolean envioNotificacion) {
		this.envioNotificacion = envioNotificacion;
	}

	public FechaClave getFechaClave() {
		return fechaClave;
	}

	public void setFechaClave(FechaClave fechaClave) {
		this.fechaClave = fechaClave;
	}

}
