package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="AUD_HISTORIAL_DETALLE")
public class AudHistorialDetalle implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AudHistorialDetallePK audHistoriaDetallePK;

    @Column(name="DATO_ANTERIOR")            
    private String datoAnterior;
    
    @Column(name="DATO_NUEVO")
    private String datoNuevo;
    
    @JoinColumns({
        @JoinColumn(name = "AUD_HISTORIAL_COD_SFC", referencedColumnName = "COD_SFC", insertable = false, updatable = false)
        ,@JoinColumn(name = "AUD_HISTORIAL_NOMBRE_TABLA", referencedColumnName = "NOMBRE_TABLA", insertable = false, updatable = false)
        ,@JoinColumn(name = "AUD_HISTORIAL_FECHA", referencedColumnName = "FECHA", insertable = false, updatable = false)
        , @JoinColumn(name = "AUD_HISTORIAL_OPERACION", referencedColumnName = "OPERACION", insertable = false, updatable = false)})
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private AudHistorial audHistorial;

	public AudHistorialDetalle() {
		super();
	}

	public AudHistorialDetallePK getAudHistoriaDetallePK() {
		return audHistoriaDetallePK;
	}

	public void setAudHistoriaDetallePK(AudHistorialDetallePK audHistoriaDetallePK) {
		this.audHistoriaDetallePK = audHistoriaDetallePK;
	}



	public String getDatoAnterior() {
		return datoAnterior;
	}

	public void setDatoAnterior(String datoAnterior) {
		this.datoAnterior = datoAnterior;
	}

	public String getDatoNuevo() {
		return datoNuevo;
	}

	public void setDatoNuevo(String datoNuevo) {
		this.datoNuevo = datoNuevo;
	}

	public void setAudHistorial(AudHistorial audHistorial) {
		this.audHistorial = audHistorial;
	}

	@Override
	public int hashCode() {
		return Objects.hash(audHistoriaDetallePK, audHistorial, datoAnterior, datoNuevo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AudHistorialDetalle other = (AudHistorialDetalle) obj;
		return Objects.equals(audHistoriaDetallePK, other.audHistoriaDetallePK)
				&& Objects.equals(audHistorial, other.audHistorial) && Objects.equals(datoAnterior, other.datoAnterior)
				&& Objects.equals(datoNuevo, other.datoNuevo);
	}

	@Override
	public String toString() {
		return this.getAudHistoriaDetallePK().getOperacion()+" "+ audHistoriaDetallePK.getCodSFC() +" " + audHistoriaDetallePK.getFecha()+ ","
				+ ", datoAnterior=" + datoAnterior + ", datoNuevo=" + datoNuevo + ", campo=" +audHistoriaDetallePK.getCampo() 
				+ "]";
	}

	public AudHistorial getAudHistorial() {
		return audHistorial;
	}

    
    
}
