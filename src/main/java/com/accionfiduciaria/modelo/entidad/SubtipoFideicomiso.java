package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jpolo
 */
@Entity
@Table(name = "NEG_SUBTIPO_FIDEICOMISO")
@XmlRootElement
public class SubtipoFideicomiso implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SubtipoFideicomisoPK subtipoFideicomisoPK;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @JoinColumn(name = "CODIGO_TIPO_FIDEICOMISO", referencedColumnName = "CODIGO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoFideicomiso tipoFideicomiso;

    public SubtipoFideicomiso() {
    }

    public SubtipoFideicomiso(SubtipoFideicomisoPK subtipoFideicomisoPK) {
        this.subtipoFideicomisoPK = subtipoFideicomisoPK;
    }

    public SubtipoFideicomiso(SubtipoFideicomisoPK subtipoFideicomisoPK, String nombre) {
        this.subtipoFideicomisoPK = subtipoFideicomisoPK;
        this.nombre = nombre;
    }

    public SubtipoFideicomiso(String codigo, String codigoTipoFideicomiso) {
        this.subtipoFideicomisoPK = new SubtipoFideicomisoPK(codigo, codigoTipoFideicomiso);
    }

    public SubtipoFideicomisoPK getSubtipoFideicomisoPK() {
        return subtipoFideicomisoPK;
    }

    public void setSubtipoFideicomisoPK(SubtipoFideicomisoPK subtipoFideicomisoPK) {
        this.subtipoFideicomisoPK = subtipoFideicomisoPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoFideicomiso getTipoFideicomiso() {
        return tipoFideicomiso;
    }

    public void setTipoFideicomiso(TipoFideicomiso tipoFideicomiso) {
        this.tipoFideicomiso = tipoFideicomiso;
    }

	@Override
	public int hashCode() {
		return Objects.hash(subtipoFideicomisoPK);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubtipoFideicomiso other = (SubtipoFideicomiso) obj;
		return Objects.equals(subtipoFideicomisoPK, other.subtipoFideicomisoPK);
	}

	@Override
	public String toString() {
		return "SubtipoFideicomiso [subtipoFideicomisoPK=" + subtipoFideicomisoPK + ", nombre=" + nombre
				+ ", tipoFideicomiso=" + tipoFideicomiso + "]";
	}
}
