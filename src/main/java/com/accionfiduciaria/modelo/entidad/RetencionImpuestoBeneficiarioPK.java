/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author efarias
 *
 */
@Embeddable
public class RetencionImpuestoBeneficiarioPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "ID_RETENCION")
	private Integer idRetencion;
	@Basic(optional = false)
	@Column(name = "NEG_RET_TITU_COD_SFC")
	private String retTitularCodSfc;
	@Basic(optional = false)
	@Column(name = "NEG_RET_TITU_NUM_DOC")
	private String retNumDocTitular;
	@Basic(optional = false)
	@Column(name = "NEG_RET_TITU_TIPO_DOC")
	private String retTipoDocTitular;
	@Basic(optional = false)
	@Column(name = "NEG_DIS_BEN_COD_SFC")
	private String disBenCodigoSfc;
	@Basic(optional = false)
	@Column(name = "TIPO_DOC_TITULAR")
	private String disBenTipoDocTitular;
	@Basic(optional = false)
	@Column(name = "NUM_DOC_TITULAR")
	private String disBenNumDocTitular;
	@Basic(optional = false)
	@Column(name = "NUM_DOC_BENEFICIARIO")
	private String numDocBeneficiario;
	@Basic(optional = false)
	@Column(name = "TIPO_DOC_BENEFICIARIO")
	private String tipoDocBeneficiario;
	@Basic(optional = false)
	@Column(name = "BANCO")
	private Integer banco;
	@Basic(optional = false)
	@Column(name = "CUENTA")
	private String cuenta;
	@Basic(optional = false)
	@Column(name = "TIPO_CUENTA")
	private Integer tipoCuenta;
	@Basic(optional = false)
	@Column(name = "NEG_RET_TITU_TIPO_NAT_NEG")
	private Integer retencionTitularTipoNaturaleza;
	

	public RetencionImpuestoBeneficiarioPK() {

	}
	
	public RetencionImpuestoBeneficiarioPK(Integer idRetencion, String retTitularCodSfc, String retNumDocTitular,
			String retTipoDocTitular, DistribucionBeneficiarioPK distribucionBeneficiarioPK, Integer retencionTitularTipoNaturaleza) {
		this.idRetencion = idRetencion;
		this.retTitularCodSfc = retTitularCodSfc;
		this.retNumDocTitular = retNumDocTitular;
		this.retTipoDocTitular = retTipoDocTitular;
		this.disBenCodigoSfc = distribucionBeneficiarioPK.getCodigoSFC();
		this.disBenTipoDocTitular = distribucionBeneficiarioPK.getTipoDocumentoTitular();
		this.disBenNumDocTitular = distribucionBeneficiarioPK.getNumeroDocumentoTitular();
		this.numDocBeneficiario = distribucionBeneficiarioPK.getNumeroDocumentoBeneficiario();
		this.tipoDocBeneficiario = distribucionBeneficiarioPK.getTipoDocumentoBeneficiario();
		this.banco = distribucionBeneficiarioPK.getBanco();
		this.cuenta = distribucionBeneficiarioPK.getCuenta();
		this.tipoCuenta = distribucionBeneficiarioPK.getTipoCuenta();
		this.retencionTitularTipoNaturaleza = retencionTitularTipoNaturaleza;
	}

	public RetencionImpuestoBeneficiarioPK(RetencionTitularPK retencionTitularPK,
			DistribucionBeneficiarioPK distribucionBeneficiarioPK) {
		this.idRetencion = retencionTitularPK.getIdRetencion();
		this.retTitularCodSfc = retencionTitularPK.getCodSfc();
		this.retNumDocTitular = retencionTitularPK.getNumeroDocumento();
		this.retTipoDocTitular = retencionTitularPK.getTipoDocumento();
		this.disBenCodigoSfc = distribucionBeneficiarioPK.getCodigoSFC();
		this.disBenTipoDocTitular = distribucionBeneficiarioPK.getTipoDocumentoTitular();
		this.disBenNumDocTitular = distribucionBeneficiarioPK.getNumeroDocumentoTitular();
		this.numDocBeneficiario = distribucionBeneficiarioPK.getNumeroDocumentoBeneficiario();
		this.tipoDocBeneficiario = distribucionBeneficiarioPK.getTipoDocumentoBeneficiario();
		this.banco = distribucionBeneficiarioPK.getBanco();
		this.cuenta = distribucionBeneficiarioPK.getCuenta();
		this.tipoCuenta = distribucionBeneficiarioPK.getTipoCuenta();
		this.retencionTitularTipoNaturaleza = retencionTitularPK.getTipoNaturalezaNegocio();
	}

	public Integer getIdRetencion() {
		return idRetencion;
	}

	public void setIdRetencion(Integer idRetencion) {
		this.idRetencion = idRetencion;
	}

	public String getRetTitularCodSfc() {
		return retTitularCodSfc;
	}

	public void setRetTitularCodSfc(String retTitularCodSfc) {
		this.retTitularCodSfc = retTitularCodSfc;
	}

	public String getRetNumDocTitular() {
		return retNumDocTitular;
	}

	public void setRetNumDocTitular(String retNumDocTitular) {
		this.retNumDocTitular = retNumDocTitular;
	}

	public String getRetTipoDocTitular() {
		return retTipoDocTitular;
	}

	public void setRetTipoDocTitular(String retTipoDocTitular) {
		this.retTipoDocTitular = retTipoDocTitular;
	}

	public String getDisBenCodigoSfc() {
		return disBenCodigoSfc;
	}

	public void setDisBenCodigoSfc(String disBenCodigoSfc) {
		this.disBenCodigoSfc = disBenCodigoSfc;
	}

	public String getDisBenTipoDocTitular() {
		return disBenTipoDocTitular;
	}

	public void setDisBenTipoDocTitular(String disBenTipoDocTitular) {
		this.disBenTipoDocTitular = disBenTipoDocTitular;
	}

	public String getDisBenNumDocTitular() {
		return disBenNumDocTitular;
	}

	public void setDisBenNumDocTitular(String disBenNumDocTitular) {
		this.disBenNumDocTitular = disBenNumDocTitular;
	}

	public String getNumDocBeneficiario() {
		return numDocBeneficiario;
	}

	public void setNumDocBeneficiario(String numDocBeneficiario) {
		this.numDocBeneficiario = numDocBeneficiario;
	}

	public String getTipoDocBeneficiario() {
		return tipoDocBeneficiario;
	}

	public void setTipoDocBeneficiario(String tipoDocBeneficiario) {
		this.tipoDocBeneficiario = tipoDocBeneficiario;
	}

	public Integer getBanco() {
		return banco;
	}

	public void setBanco(Integer banco) {
		this.banco = banco;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public Integer getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(Integer tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public Integer getRetencionTitularTipoNaturaleza() {
		return retencionTitularTipoNaturaleza;
	}

	public void setRetencionTitularTipoNaturaleza(Integer retencionTitularTipoNaturaleza) {
		this.retencionTitularTipoNaturaleza = retencionTitularTipoNaturaleza;
	}

	@Override
	public int hashCode() {
		return Objects.hash(banco, cuenta, disBenCodigoSfc, disBenNumDocTitular, disBenTipoDocTitular, idRetencion,
				numDocBeneficiario, retNumDocTitular, retTipoDocTitular, retTitularCodSfc,
				retencionTitularTipoNaturaleza, tipoCuenta, tipoDocBeneficiario);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetencionImpuestoBeneficiarioPK other = (RetencionImpuestoBeneficiarioPK) obj;
		return Objects.equals(banco, other.banco) && Objects.equals(cuenta, other.cuenta)
				&& Objects.equals(disBenCodigoSfc, other.disBenCodigoSfc)
				&& Objects.equals(disBenNumDocTitular, other.disBenNumDocTitular)
				&& Objects.equals(disBenTipoDocTitular, other.disBenTipoDocTitular)
				&& Objects.equals(idRetencion, other.idRetencion)
				&& Objects.equals(numDocBeneficiario, other.numDocBeneficiario)
				&& Objects.equals(retNumDocTitular, other.retNumDocTitular)
				&& Objects.equals(retTipoDocTitular, other.retTipoDocTitular)
				&& Objects.equals(retTitularCodSfc, other.retTitularCodSfc)
				&& Objects.equals(retencionTitularTipoNaturaleza, other.retencionTitularTipoNaturaleza)
				&& Objects.equals(tipoCuenta, other.tipoCuenta)
				&& Objects.equals(tipoDocBeneficiario, other.tipoDocBeneficiario);
	}

	@Override
	public String toString() {
		return "RetencionImpuestoBeneficiarioPK [idRetencion=" + idRetencion + ", retTitularCodSfc=" + retTitularCodSfc
				+ ", retNumDocTitular=" + retNumDocTitular + ", retTipoDocTitular=" + retTipoDocTitular
				+ ", disBenCodigoSfc=" + disBenCodigoSfc + ", disBenTipoDocTitular=" + disBenTipoDocTitular
				+ ", disBenNumDocTitular=" + disBenNumDocTitular + ", numDocBeneficiario=" + numDocBeneficiario
				+ ", tipoDocBeneficiario=" + tipoDocBeneficiario + ", banco=" + banco + ", cuenta=" + cuenta
				+ ", tipoCuenta=" + tipoCuenta + ", retencionTitularTipoNaturaleza=" + retencionTitularTipoNaturaleza
				+ "]";
	}
}
