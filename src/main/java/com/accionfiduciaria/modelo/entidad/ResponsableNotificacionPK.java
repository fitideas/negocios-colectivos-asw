/**
 * 
 */
package com.accionfiduciaria.modelo.entidad;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author efarias
 *
 */
@Embeddable
public class ResponsableNotificacionPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "COD_SFC")
	private String codSfc;

	@Basic(optional = false)
	@Column(name = "TIPO_DOCUMENTO")
	private String tipoDocumento;

	@Basic(optional = false)
	@Column(name = "DOCUMENTO")
	private String documento;

	public ResponsableNotificacionPK() {
	}

	public ResponsableNotificacionPK(String codSfc, String tipoDocumento, String documento) {
		this.codSfc = codSfc;
		this.tipoDocumento = tipoDocumento;
		this.documento = documento;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codSfc, documento, tipoDocumento);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponsableNotificacionPK other = (ResponsableNotificacionPK) obj;
		return Objects.equals(codSfc, other.codSfc) && Objects.equals(documento, other.documento)
				&& Objects.equals(tipoDocumento, other.tipoDocumento);
	}

	@Override
	public String toString() {
		return "ResponsableNotificacionPK [codSfc=" + codSfc + ", tipoDocumento=" + tipoDocumento + ", documento="
				+ documento + "]";
	}

	public String getCodSfc() {
		return codSfc;
	}

	public void setCodSfc(String codSfc) {
		this.codSfc = codSfc;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

}
