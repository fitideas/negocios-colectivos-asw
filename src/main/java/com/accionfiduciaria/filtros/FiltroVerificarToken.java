package com.accionfiduciaria.filtros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.accionfiduciaria.negocio.servicios.autenticacion.IServicioAutenticacion;
import org.springframework.web.bind.annotation.RequestMethod;

@Component
@PropertySource("classpath:mensajes.properties")
@PropertySource("classpath:propiedades.properties")
public class FiltroVerificarToken implements Filter{
	
	@Value("${sesion.caducada}")
	private String mensajeSesionCaducada;
	
	@Value("${header.title.authorization}")
	private String headerAuthorization;
	
	
	@Autowired
	private IServicioAutenticacion servicioAutenticacion;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		if( !RequestMethod.OPTIONS.name().equals(req.getMethod())
				&& !servicioAutenticacion.validarToken(req.getHeader(headerAuthorization))) {
			res.sendError(HttpStatus.UNAUTHORIZED.value(), mensajeSesionCaducada);
		}

        chain.doFilter(request, response);
	}
	
	@Bean
	public FilterRegistrationBean<FiltroVerificarToken> filtroAutenticacion() {
	    FilterRegistrationBean<FiltroVerificarToken> registrationBean = new FilterRegistrationBean<>();
	    registrationBean.setFilter(this);
	    registrationBean.addUrlPatterns(
	    		"/roles/*",
	    		"/parametros/*",
	    		"/beneficiario/obtenersolicitudes",
	    		"/beneficiario/enviarsolicitud",
	    		"/beneficiario/guardardatosdistribucion",
	    		"/beneficiario/obtenerinformacionfinanciera",
	    		"/beneficiario/obtenercotitular",
	    		"/beneficiario/guardarinformacionfinanciera",
	    		"/beneficiario/administrarcotitular",
	    		"/util/obtenerbancos",
	    		"/util/obtenertiposdocumento",
	    		"/negocio/obtenerinformacionnegocio",
	    		"/negocio/guardarinformacionnegocio",
	    		"/negocio/obtenertiposnegocio",
	    		"/negocio/obtenertipofideicomiso"
	    		);
	    return registrationBean;    
	}

}
