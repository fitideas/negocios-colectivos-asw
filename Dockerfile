FROM openjdk:8-jdk-alpine

RUN ln -sfn /usr/share/zoneinfo/America/Bogota /etc/localtime

COPY target/*.jar /app.jar

CMD ["java","-jar","/app.jar"]
